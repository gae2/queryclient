package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.QuerySession;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "querySessions")
@XmlType(propOrder = {"querySession", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuerySessionListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QuerySessionListStub.class.getName());

    private List<QuerySessionStub> querySessions = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public QuerySessionListStub()
    {
        this(new ArrayList<QuerySessionStub>());
    }
    public QuerySessionListStub(List<QuerySessionStub> querySessions)
    {
        this(querySessions, null);
    }
    public QuerySessionListStub(List<QuerySessionStub> querySessions, String forwardCursor)
    {
        this.querySessions = querySessions;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(querySessions == null) {
            return true;
        } else {
            return querySessions.isEmpty();
        }
    }
    public int getSize()
    {
        if(querySessions == null) {
            return 0;
        } else {
            return querySessions.size();
        }
    }


    @XmlElement(name = "querySession")
    public List<QuerySessionStub> getQuerySession()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<QuerySessionStub> getList()
    {
        return querySessions;
    }
    public void setList(List<QuerySessionStub> querySessions)
    {
        this.querySessions = querySessions;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<QuerySessionStub> it = this.querySessions.iterator();
        while(it.hasNext()) {
            QuerySessionStub querySession = it.next();
            sb.append(querySession.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static QuerySessionListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of QuerySessionListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write QuerySessionListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write QuerySessionListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write QuerySessionListStub object as a string.", e);
        }
        
        return null;
    }
    public static QuerySessionListStub fromJsonString(String jsonStr)
    {
        try {
            QuerySessionListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, QuerySessionListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into QuerySessionListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into QuerySessionListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into QuerySessionListStub object.", e);
        }
        
        return null;
    }

}
