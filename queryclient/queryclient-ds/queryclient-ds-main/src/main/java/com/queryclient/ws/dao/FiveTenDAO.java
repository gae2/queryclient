package com.queryclient.ws.dao;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.data.FiveTenDataObject;


// TBD: Add offset/count to getAllFiveTens() and findFiveTens(), etc.
public interface FiveTenDAO
{
    FiveTenDataObject getFiveTen(String guid) throws BaseException;
    List<FiveTenDataObject> getFiveTens(List<String> guids) throws BaseException;
    List<FiveTenDataObject> getAllFiveTens() throws BaseException;
    /* @Deprecated */ List<FiveTenDataObject> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException;
    List<FiveTenDataObject> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createFiveTen(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return FiveTenDataObject?)
    String createFiveTen(FiveTenDataObject fiveTen) throws BaseException;          // Returns Guid.  (Return FiveTenDataObject?)
    //Boolean updateFiveTen(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateFiveTen(FiveTenDataObject fiveTen) throws BaseException;
    Boolean deleteFiveTen(String guid) throws BaseException;
    Boolean deleteFiveTen(FiveTenDataObject fiveTen) throws BaseException;
    Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException;
}
