package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "gaeUserStruct")
@XmlType(propOrder = {"authDomain", "federatedIdentity", "nickname", "userId", "email", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GaeUserStructStub implements GaeUserStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GaeUserStructStub.class.getName());

    private String authDomain;
    private String federatedIdentity;
    private String nickname;
    private String userId;
    private String email;
    private String note;

    public GaeUserStructStub()
    {
        this(null);
    }
    public GaeUserStructStub(GaeUserStruct bean)
    {
        if(bean != null) {
            this.authDomain = bean.getAuthDomain();
            this.federatedIdentity = bean.getFederatedIdentity();
            this.nickname = bean.getNickname();
            this.userId = bean.getUserId();
            this.email = bean.getEmail();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getAuthDomain()
    {
        return this.authDomain;
    }
    public void setAuthDomain(String authDomain)
    {
        this.authDomain = authDomain;
    }

    @XmlElement
    public String getFederatedIdentity()
    {
        return this.federatedIdentity;
    }
    public void setFederatedIdentity(String federatedIdentity)
    {
        this.federatedIdentity = federatedIdentity;
    }

    @XmlElement
    public String getNickname()
    {
        return this.nickname;
    }
    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    @XmlElement
    public String getUserId()
    {
        return this.userId;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    @XmlElement
    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getAuthDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFederatedIdentity() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNickname() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUserId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmail() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("authDomain", this.authDomain);
        dataMap.put("federatedIdentity", this.federatedIdentity);
        dataMap.put("nickname", this.nickname);
        dataMap.put("userId", this.userId);
        dataMap.put("email", this.email);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = authDomain == null ? 0 : authDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = federatedIdentity == null ? 0 : federatedIdentity.hashCode();
        _hash = 31 * _hash + delta;
        delta = nickname == null ? 0 : nickname.hashCode();
        _hash = 31 * _hash + delta;
        delta = userId == null ? 0 : userId.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static GaeUserStructStub convertBeanToStub(GaeUserStruct bean)
    {
        GaeUserStructStub stub = null;
        if(bean instanceof GaeUserStructStub) {
            stub = (GaeUserStructStub) bean;
        } else {
            if(bean != null) {
                stub = new GaeUserStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static GaeUserStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of GaeUserStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write GaeUserStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write GaeUserStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write GaeUserStructStub object as a string.", e);
        }
        
        return null;
    }
    public static GaeUserStructStub fromJsonString(String jsonStr)
    {
        try {
            GaeUserStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, GaeUserStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeUserStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeUserStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeUserStructStub object.", e);
        }
        
        return null;
    }

}
