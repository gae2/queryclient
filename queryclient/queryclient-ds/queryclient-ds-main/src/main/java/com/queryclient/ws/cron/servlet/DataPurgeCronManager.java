package com.queryclient.ws.cron.servlet;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.service.ApiConsumerService;
import com.queryclient.ws.service.UserService;
import com.queryclient.ws.service.UserPasswordService;
import com.queryclient.ws.service.ExternalUserAuthService;
import com.queryclient.ws.service.UserAuthStateService;
import com.queryclient.ws.service.DataServiceService;
import com.queryclient.ws.service.ServiceEndpointService;
import com.queryclient.ws.service.QuerySessionService;
import com.queryclient.ws.service.QueryRecordService;
import com.queryclient.ws.service.DummyEntityService;
import com.queryclient.ws.service.ServiceInfoService;
import com.queryclient.ws.service.FiveTenService;
import com.queryclient.ws.service.impl.ApiConsumerServiceImpl;
import com.queryclient.ws.service.impl.UserServiceImpl;
import com.queryclient.ws.service.impl.UserPasswordServiceImpl;
import com.queryclient.ws.service.impl.ExternalUserAuthServiceImpl;
import com.queryclient.ws.service.impl.UserAuthStateServiceImpl;
import com.queryclient.ws.service.impl.DataServiceServiceImpl;
import com.queryclient.ws.service.impl.ServiceEndpointServiceImpl;
import com.queryclient.ws.service.impl.QuerySessionServiceImpl;
import com.queryclient.ws.service.impl.QueryRecordServiceImpl;
import com.queryclient.ws.service.impl.DummyEntityServiceImpl;
import com.queryclient.ws.service.impl.ServiceInfoServiceImpl;
import com.queryclient.ws.service.impl.FiveTenServiceImpl;


public final class DataPurgeCronManager
{
    private static final Logger log = Logger.getLogger(DataPurgeCronManager.class.getName());   

    // TBD...
    // Count is per entity, not per cron run.
    public static final int DEFAULT_MAX_COUNT = 10;
    public static final int MAXIMUM_MAX_COUNT = 1000;   // ????
    // ...
    public static final int DEFAULT_DELTA_HOURS = 168;    // 1 week.
    public static final int MAXIMUM_DELTA_HOURS = 8760;   // 1 year.
    // ...
    

    // TBD: Is this safe for concurrent calls??
    private ApiConsumerService apiConsumerService = null;
    private UserService userService = null;
    private UserPasswordService userPasswordService = null;
    private ExternalUserAuthService externalUserAuthService = null;
    private UserAuthStateService userAuthStateService = null;
    private DataServiceService dataServiceService = null;
    private ServiceEndpointService serviceEndpointService = null;
    private QuerySessionService querySessionService = null;
    private QueryRecordService queryRecordService = null;
    private DummyEntityService dummyEntityService = null;
    private ServiceInfoService serviceInfoService = null;
    private FiveTenService fiveTenService = null;
    // etc...

    // Lazy initialized.
    private ApiConsumerService getApiConsumerService()
    {
        if(apiConsumerService == null) {
            apiConsumerService = new ApiConsumerServiceImpl();
        }
        return apiConsumerService;
    }
    private UserService getUserService()
    {
        if(userService == null) {
            userService = new UserServiceImpl();
        }
        return userService;
    }
    private UserPasswordService getUserPasswordService()
    {
        if(userPasswordService == null) {
            userPasswordService = new UserPasswordServiceImpl();
        }
        return userPasswordService;
    }
    private ExternalUserAuthService getExternalUserAuthService()
    {
        if(externalUserAuthService == null) {
            externalUserAuthService = new ExternalUserAuthServiceImpl();
        }
        return externalUserAuthService;
    }
    private UserAuthStateService getUserAuthStateService()
    {
        if(userAuthStateService == null) {
            userAuthStateService = new UserAuthStateServiceImpl();
        }
        return userAuthStateService;
    }
    private DataServiceService getDataServiceService()
    {
        if(dataServiceService == null) {
            dataServiceService = new DataServiceServiceImpl();
        }
        return dataServiceService;
    }
    private ServiceEndpointService getServiceEndpointService()
    {
        if(serviceEndpointService == null) {
            serviceEndpointService = new ServiceEndpointServiceImpl();
        }
        return serviceEndpointService;
    }
    private QuerySessionService getQuerySessionService()
    {
        if(querySessionService == null) {
            querySessionService = new QuerySessionServiceImpl();
        }
        return querySessionService;
    }
    private QueryRecordService getQueryRecordService()
    {
        if(queryRecordService == null) {
            queryRecordService = new QueryRecordServiceImpl();
        }
        return queryRecordService;
    }
    private DummyEntityService getDummyEntityService()
    {
        if(dummyEntityService == null) {
            dummyEntityService = new DummyEntityServiceImpl();
        }
        return dummyEntityService;
    }
    private ServiceInfoService getServiceInfoService()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new ServiceInfoServiceImpl();
        }
        return serviceInfoService;
    }
    private FiveTenService getFiveTenService()
    {
        if(fiveTenService == null) {
            fiveTenService = new FiveTenServiceImpl();
        }
        return fiveTenService;
    }
    // etc. ...

    
    private DataPurgeCronManager()
    {
        init();
    }
    private void init()
    {
        // TBD: ...
    }

    // Initialization-on-demand holder.
    private static class RefreshCronManagerHolder
    {
        private static final DataPurgeCronManager INSTANCE = new DataPurgeCronManager();
    }
    // Singleton method
    public static DataPurgeCronManager getInstance()
    {
        return RefreshCronManagerHolder.INSTANCE;
    }
    
    
    public int processDeletion(String entity, int deltaHours, int maxCount)
    {
        if(deltaHours <= 0) {
            deltaHours = DEFAULT_DELTA_HOURS;
        } else if(deltaHours > MAXIMUM_DELTA_HOURS) {
            deltaHours = MAXIMUM_DELTA_HOURS;
        }
        if(maxCount <= 0) {
            maxCount = DEFAULT_MAX_COUNT;            
        } else if(maxCount > MAXIMUM_MAX_COUNT) {
            maxCount = MAXIMUM_MAX_COUNT;            
        }

        // temporary
        int cnt = 0;
        switch(entity) {
        case "ApiConsumer":
            cnt = deleteApiConsumers(entity, deltaHours, maxCount);
            break;
        case "User":
            cnt = deleteUsers(entity, deltaHours, maxCount);
            break;
        case "UserPassword":
            cnt = deleteUserPasswords(entity, deltaHours, maxCount);
            break;
        case "ExternalUserAuth":
            cnt = deleteExternalUserAuths(entity, deltaHours, maxCount);
            break;
        case "UserAuthState":
            cnt = deleteUserAuthStates(entity, deltaHours, maxCount);
            break;
        case "DataService":
            cnt = deleteDataServices(entity, deltaHours, maxCount);
            break;
        case "ServiceEndpoint":
            cnt = deleteServiceEndpoints(entity, deltaHours, maxCount);
            break;
        case "QuerySession":
            cnt = deleteQuerySessions(entity, deltaHours, maxCount);
            break;
        case "QueryRecord":
            cnt = deleteQueryRecords(entity, deltaHours, maxCount);
            break;
        case "DummyEntity":
            cnt = deleteDummyEntities(entity, deltaHours, maxCount);
            break;
        case "ServiceInfo":
            cnt = deleteServiceInfos(entity, deltaHours, maxCount);
            break;
        case "FiveTen":
            cnt = deleteFiveTens(entity, deltaHours, maxCount);
            break;
        default:
            log.warning("Unsupported entity type for bulk deletion: " + entity);
        }
        return cnt;
    }


    // Purge based on createdTime.
    // Note that for many entity types, createdTime may not be the best choice.
    // TBD: Pass order/filtering as args?
    

    public int deleteApiConsumers(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getApiConsumerService().deleteApiConsumers(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getApiConsumerService().findApiConsumerKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getApiConsumerService().deleteApiConsumer(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete ApiConsumer: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete ApiConsumers.", e);
        }
        return cnt;
    }

    public int deleteUsers(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getUserService().deleteUsers(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getUserService().findUserKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getUserService().deleteUser(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete User: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete Users.", e);
        }
        return cnt;
    }

    public int deleteUserPasswords(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getUserPasswordService().deleteUserPasswords(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getUserPasswordService().findUserPasswordKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getUserPasswordService().deleteUserPassword(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete UserPassword: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete UserPasswords.", e);
        }
        return cnt;
    }

    public int deleteExternalUserAuths(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getExternalUserAuthService().deleteExternalUserAuths(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getExternalUserAuthService().findExternalUserAuthKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getExternalUserAuthService().deleteExternalUserAuth(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete ExternalUserAuth: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete ExternalUserAuths.", e);
        }
        return cnt;
    }

    public int deleteUserAuthStates(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getUserAuthStateService().deleteUserAuthStates(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getUserAuthStateService().findUserAuthStateKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getUserAuthStateService().deleteUserAuthState(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete UserAuthState: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete UserAuthStates.", e);
        }
        return cnt;
    }

    public int deleteDataServices(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getDataServiceService().deleteDataServices(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getDataServiceService().findDataServiceKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getDataServiceService().deleteDataService(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete DataService: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete DataServices.", e);
        }
        return cnt;
    }

    public int deleteServiceEndpoints(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getServiceEndpointService().deleteServiceEndpoints(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getServiceEndpointService().findServiceEndpointKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getServiceEndpointService().deleteServiceEndpoint(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete ServiceEndpoint: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete ServiceEndpoints.", e);
        }
        return cnt;
    }

    public int deleteQuerySessions(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getQuerySessionService().deleteQuerySessions(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getQuerySessionService().findQuerySessionKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getQuerySessionService().deleteQuerySession(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete QuerySession: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete QuerySessions.", e);
        }
        return cnt;
    }

    public int deleteQueryRecords(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getQueryRecordService().deleteQueryRecords(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getQueryRecordService().findQueryRecordKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getQueryRecordService().deleteQueryRecord(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete QueryRecord: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete QueryRecords.", e);
        }
        return cnt;
    }

    public int deleteDummyEntities(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getDummyEntityService().deleteDummyEntities(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getDummyEntityService().findDummyEntityKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getDummyEntityService().deleteDummyEntity(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete DummyEntity: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete DummyEntities.", e);
        }
        return cnt;
    }

    public int deleteServiceInfos(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getServiceInfoService().deleteServiceInfos(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getServiceInfoService().findServiceInfoKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getServiceInfoService().deleteServiceInfo(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete ServiceInfo: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete ServiceInfos.", e);
        }
        return cnt;
    }

    public int deleteFiveTens(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getFiveTenService().deleteFiveTens(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getFiveTenService().findFiveTenKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getFiveTenService().deleteFiveTen(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete FiveTen: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete FiveTens.", e);
        }
        return cnt;
    }


}
