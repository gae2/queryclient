package com.queryclient.ws.resource.exception;

import com.queryclient.ws.exception.resource.BaseResourceException;


public class UnauthorizedRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public UnauthorizedRsException() 
    {
        super();
    }
    public UnauthorizedRsException(String message) 
    {
        super(message);
    }
    public UnauthorizedRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public UnauthorizedRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public UnauthorizedRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public UnauthorizedRsException(Throwable cause) 
    {
        super(cause);
    }
    public UnauthorizedRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
