package com.queryclient.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.FiveTen;
import com.queryclient.ws.bean.FiveTenBean;
import com.queryclient.ws.stub.FiveTenListStub;
import com.queryclient.ws.stub.FiveTenStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.FiveTenResource;

// MockFiveTenResource is a decorator.
// It can be used as a base class to mock FiveTenResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/fiveTens/")
public abstract class MockFiveTenResource implements FiveTenResource
{
    private static final Logger log = Logger.getLogger(MockFiveTenResource.class.getName());

    // MockFiveTenResource uses the decorator design pattern.
    private FiveTenResource decoratedResource;

    public MockFiveTenResource(FiveTenResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected FiveTenResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(FiveTenResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllFiveTens(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllFiveTenKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getFiveTenKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getFiveTenKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getFiveTen(String guid) throws BaseResourceException
    {
        return decoratedResource.getFiveTen(guid);
    }

    @Override
    public Response getFiveTen(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getFiveTen(guid, field);
    }

    @Override
    public Response createFiveTen(FiveTenStub fiveTen) throws BaseResourceException
    {
        return decoratedResource.createFiveTen(fiveTen);
    }

    @Override
    public Response updateFiveTen(String guid, FiveTenStub fiveTen) throws BaseResourceException
    {
        return decoratedResource.updateFiveTen(guid, fiveTen);
    }

    @Override
    public Response updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseResourceException
    {
        return decoratedResource.updateFiveTen(guid, counter, requesterIpAddress);
    }

    @Override
    public Response deleteFiveTen(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteFiveTen(guid);
    }

    @Override
    public Response deleteFiveTens(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteFiveTens(filter, params, values);
    }


}
