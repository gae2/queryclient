package com.queryclient.ws.platform.gae;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.appidentity.PublicCertificate;
import com.google.apphosting.api.ApiProxy;

import com.queryclient.ws.platform.AppIdentityPlatformService;
import com.queryclient.ws.platform.SignedKey;
import com.queryclient.ws.platform.X509PublicCertificate;


// GAE Platform service.
public class GaeAppIdentityPlatformService extends AppIdentityPlatformService
{
    private static final Logger log = Logger.getLogger(GaeAppIdentityPlatformService.class.getName());

    private GaeAppIdentityPlatformService()
    {
    }

    // Initialization-on-demand holder.
    private static class GaeAppIdentityPlatformServiceHolder
    {
        private static final GaeAppIdentityPlatformService INSTANCE = new GaeAppIdentityPlatformService();
    }

    // Singleton method
    public static GaeAppIdentityPlatformService getInstance()
    {
        return GaeAppIdentityPlatformServiceHolder.INSTANCE;
    }


    @Override
    public String getAppId()
    {
        return ApiProxy.getCurrentEnvironment().getAppId();
    }

    @Override
    public List<X509PublicCertificate> getPublicCertificatesForApp()
    {
        AppIdentityService appIdentity = AppIdentityServiceFactory.getAppIdentityService();
        java.util.Collection<PublicCertificate> publicCertificates = appIdentity.getPublicCertificatesForApp();
        
        List<X509PublicCertificate> certficates = new ArrayList<X509PublicCertificate>();
        for(PublicCertificate publicCertificate : publicCertificates) {
            certficates.add(new GaeX509PublicCertificate(publicCertificate));
        }
        
        return certficates;
    }

    @Override
    public SignedKey signForApp(byte[] signBlob) 
    {
        AppIdentityService appIdentity = AppIdentityServiceFactory.getAppIdentityService();
        AppIdentityService.SigningResult signingResult = appIdentity.signForApp(signBlob);
        return new GaeSignedKey(signingResult);
    }

        
    // temporary
    public String getGaeServiceAccountName()
    {
        AppIdentityService appIdentity = AppIdentityServiceFactory.getAppIdentityService();
        return appIdentity.getServiceAccountName();
    }

    // temporary
    public GoogleApiAccessToken getAccessToken(List<String> scopes)
    {
        AppIdentityService appIdentity = AppIdentityServiceFactory.getAppIdentityService();
        AppIdentityService.GetAccessTokenResult accessToken = appIdentity.getAccessToken(scopes);
        return new GoogleApiAccessToken(accessToken);
    }

}
