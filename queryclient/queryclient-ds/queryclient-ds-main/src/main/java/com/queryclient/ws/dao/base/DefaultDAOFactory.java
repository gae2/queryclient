package com.queryclient.ws.dao.base;

import java.util.logging.Logger;

import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.dao.ApiConsumerDAO;
import com.queryclient.ws.dao.UserDAO;
import com.queryclient.ws.dao.UserPasswordDAO;
import com.queryclient.ws.dao.ExternalUserAuthDAO;
import com.queryclient.ws.dao.UserAuthStateDAO;
import com.queryclient.ws.dao.DataServiceDAO;
import com.queryclient.ws.dao.ServiceEndpointDAO;
import com.queryclient.ws.dao.QuerySessionDAO;
import com.queryclient.ws.dao.QueryRecordDAO;
import com.queryclient.ws.dao.DummyEntityDAO;
import com.queryclient.ws.dao.ServiceInfoDAO;
import com.queryclient.ws.dao.FiveTenDAO;

// Default DAO factory uses JDO on Google AppEngine.
public class DefaultDAOFactory extends DAOFactory
{
    private static final Logger log = Logger.getLogger(DefaultDAOFactory.class.getName());

    // Ctor. Prevents instantiation.
    private DefaultDAOFactory()
    {
        // ...
    }

    // Initialization-on-demand holder.
    private static class DefaultDAOFactoryHolder
    {
        private static final DefaultDAOFactory INSTANCE = new DefaultDAOFactory();
    }

    // Singleton method
    public static DefaultDAOFactory getInstance()
    {
        return DefaultDAOFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerDAO getApiConsumerDAO()
    {
        return new DefaultApiConsumerDAO();
    }

    @Override
    public UserDAO getUserDAO()
    {
        return new DefaultUserDAO();
    }

    @Override
    public UserPasswordDAO getUserPasswordDAO()
    {
        return new DefaultUserPasswordDAO();
    }

    @Override
    public ExternalUserAuthDAO getExternalUserAuthDAO()
    {
        return new DefaultExternalUserAuthDAO();
    }

    @Override
    public UserAuthStateDAO getUserAuthStateDAO()
    {
        return new DefaultUserAuthStateDAO();
    }

    @Override
    public DataServiceDAO getDataServiceDAO()
    {
        return new DefaultDataServiceDAO();
    }

    @Override
    public ServiceEndpointDAO getServiceEndpointDAO()
    {
        return new DefaultServiceEndpointDAO();
    }

    @Override
    public QuerySessionDAO getQuerySessionDAO()
    {
        return new DefaultQuerySessionDAO();
    }

    @Override
    public QueryRecordDAO getQueryRecordDAO()
    {
        return new DefaultQueryRecordDAO();
    }

    @Override
    public DummyEntityDAO getDummyEntityDAO()
    {
        return new DefaultDummyEntityDAO();
    }

    @Override
    public ServiceInfoDAO getServiceInfoDAO()
    {
        return new DefaultServiceInfoDAO();
    }

    @Override
    public FiveTenDAO getFiveTenDAO()
    {
        return new DefaultFiveTenDAO();
    }

}
