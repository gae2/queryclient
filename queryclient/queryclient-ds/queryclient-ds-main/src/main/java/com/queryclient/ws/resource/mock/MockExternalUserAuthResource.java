package com.queryclient.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.bean.ExternalUserAuthBean;
import com.queryclient.ws.stub.ExternalUserAuthListStub;
import com.queryclient.ws.stub.ExternalUserAuthStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.ExternalUserAuthResource;
import com.queryclient.ws.resource.util.GaeAppStructResourceUtil;
import com.queryclient.ws.resource.util.ExternalUserIdStructResourceUtil;

// MockExternalUserAuthResource is a decorator.
// It can be used as a base class to mock ExternalUserAuthResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/externalUserAuths/")
public abstract class MockExternalUserAuthResource implements ExternalUserAuthResource
{
    private static final Logger log = Logger.getLogger(MockExternalUserAuthResource.class.getName());

    // MockExternalUserAuthResource uses the decorator design pattern.
    private ExternalUserAuthResource decoratedResource;

    public MockExternalUserAuthResource(ExternalUserAuthResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected ExternalUserAuthResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(ExternalUserAuthResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllExternalUserAuths(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllExternalUserAuths(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllExternalUserAuthKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllExternalUserAuthKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getExternalUserAuthKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getExternalUserAuthKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getExternalUserAuth(String guid) throws BaseResourceException
    {
        return decoratedResource.getExternalUserAuth(guid);
    }

    @Override
    public Response getExternalUserAuth(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getExternalUserAuth(guid, field);
    }

    @Override
    public Response createExternalUserAuth(ExternalUserAuthStub externalUserAuth) throws BaseResourceException
    {
        return decoratedResource.createExternalUserAuth(externalUserAuth);
    }

    @Override
    public Response updateExternalUserAuth(String guid, ExternalUserAuthStub externalUserAuth) throws BaseResourceException
    {
        return decoratedResource.updateExternalUserAuth(guid, externalUserAuth);
    }

    @Override
    public Response updateExternalUserAuth(String guid, String managerApp, Long appAcl, String gaeApp, String ownerUser, Long userAcl, String user, String providerId, String externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseResourceException
    {
        return decoratedResource.updateExternalUserAuth(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, providerId, externalUserId, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
    }

    @Override
    public Response deleteExternalUserAuth(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteExternalUserAuth(guid);
    }

    @Override
    public Response deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteExternalUserAuths(filter, params, values);
    }


}
