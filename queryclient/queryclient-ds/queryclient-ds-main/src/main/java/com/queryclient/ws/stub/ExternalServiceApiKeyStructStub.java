package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ExternalServiceApiKeyStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "externalServiceApiKeyStruct")
@XmlType(propOrder = {"uuid", "service", "key", "secret", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalServiceApiKeyStructStub implements ExternalServiceApiKeyStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExternalServiceApiKeyStructStub.class.getName());

    private String uuid;
    private String service;
    private String key;
    private String secret;
    private String note;

    public ExternalServiceApiKeyStructStub()
    {
        this(null);
    }
    public ExternalServiceApiKeyStructStub(ExternalServiceApiKeyStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.service = bean.getService();
            this.key = bean.getKey();
            this.secret = bean.getSecret();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getService()
    {
        return this.service;
    }
    public void setService(String service)
    {
        this.service = service;
    }

    @XmlElement
    public String getKey()
    {
        return this.key;
    }
    public void setKey(String key)
    {
        this.key = key;
    }

    @XmlElement
    public String getSecret()
    {
        return this.secret;
    }
    public void setSecret(String secret)
    {
        this.secret = secret;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getService() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecret() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("service", this.service);
        dataMap.put("key", this.key);
        dataMap.put("secret", this.secret);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = service == null ? 0 : service.hashCode();
        _hash = 31 * _hash + delta;
        delta = key == null ? 0 : key.hashCode();
        _hash = 31 * _hash + delta;
        delta = secret == null ? 0 : secret.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ExternalServiceApiKeyStructStub convertBeanToStub(ExternalServiceApiKeyStruct bean)
    {
        ExternalServiceApiKeyStructStub stub = null;
        if(bean instanceof ExternalServiceApiKeyStructStub) {
            stub = (ExternalServiceApiKeyStructStub) bean;
        } else {
            if(bean != null) {
                stub = new ExternalServiceApiKeyStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ExternalServiceApiKeyStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of ExternalServiceApiKeyStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ExternalServiceApiKeyStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ExternalServiceApiKeyStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ExternalServiceApiKeyStructStub object as a string.", e);
        }
        
        return null;
    }
    public static ExternalServiceApiKeyStructStub fromJsonString(String jsonStr)
    {
        try {
            ExternalServiceApiKeyStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ExternalServiceApiKeyStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalServiceApiKeyStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalServiceApiKeyStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalServiceApiKeyStructStub object.", e);
        }
        
        return null;
    }

}
