package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.data.ConsumerKeySecretPairDataObject;

public class ConsumerKeySecretPairBean implements ConsumerKeySecretPair
{
    private static final Logger log = Logger.getLogger(ConsumerKeySecretPairBean.class.getName());

    // Embedded data object.
    private ConsumerKeySecretPairDataObject dobj = null;

    public ConsumerKeySecretPairBean()
    {
        this(new ConsumerKeySecretPairDataObject());
    }
    public ConsumerKeySecretPairBean(ConsumerKeySecretPairDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public ConsumerKeySecretPairDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getConsumerKey()
    {
        if(getDataObject() != null) {
            return getDataObject().getConsumerKey();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ConsumerKeySecretPairDataObject is null!");
            return null;   // ???
        }
    }
    public void setConsumerKey(String consumerKey)
    {
        if(getDataObject() != null) {
            getDataObject().setConsumerKey(consumerKey);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ConsumerKeySecretPairDataObject is null!");
        }
    }

    public String getConsumerSecret()
    {
        if(getDataObject() != null) {
            return getDataObject().getConsumerSecret();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ConsumerKeySecretPairDataObject is null!");
            return null;   // ???
        }
    }
    public void setConsumerSecret(String consumerSecret)
    {
        if(getDataObject() != null) {
            getDataObject().setConsumerSecret(consumerSecret);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ConsumerKeySecretPairDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ConsumerKeySecretPairDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getConsumerKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getConsumerSecret() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public ConsumerKeySecretPairDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
