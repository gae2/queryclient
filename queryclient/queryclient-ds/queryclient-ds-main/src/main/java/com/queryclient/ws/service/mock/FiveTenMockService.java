package com.queryclient.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.FiveTen;
import com.queryclient.ws.bean.FiveTenBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.FiveTenDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.FiveTenService;


// FiveTenMockService is a decorator.
// It can be used as a base class to mock FiveTenService objects.
public abstract class FiveTenMockService implements FiveTenService
{
    private static final Logger log = Logger.getLogger(FiveTenMockService.class.getName());

    // FiveTenMockService uses the decorator design pattern.
    private FiveTenService decoratedService;

    public FiveTenMockService(FiveTenService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected FiveTenService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(FiveTenService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // FiveTen related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFiveTen(): guid = " + guid);
        FiveTen bean = decoratedService.getFiveTen(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getFiveTen(guid, field);
        return obj;
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        log.fine("getFiveTens()");
        List<FiveTen> fiveTens = decoratedService.getFiveTens(guids);
        log.finer("END");
        return fiveTens;
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return getAllFiveTens(null, null, null);
    }


    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFiveTens(ordering, offset, count, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFiveTens(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<FiveTen> fiveTens = decoratedService.getAllFiveTens(ordering, offset, count, forwardCursor);
        log.finer("END");
        return fiveTens;
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFiveTenKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFiveTenKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllFiveTenKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FiveTenMockService.findFiveTens(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<FiveTen> fiveTens = decoratedService.findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return fiveTens;
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FiveTenMockService.findFiveTenKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FiveTenMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException
    {
        return decoratedService.createFiveTen(counter, requesterIpAddress);
    }

    @Override
    public String createFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createFiveTen(fiveTen);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException
    {
        return decoratedService.updateFiveTen(guid, counter, requesterIpAddress);
    }
        
    @Override
    public Boolean updateFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateFiveTen(fiveTen);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteFiveTen(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteFiveTen(fiveTen);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteFiveTens(filter, params, values);
        return count;
    }

}
