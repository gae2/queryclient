package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class QuerySessionDataObject extends KeyedDataObject implements QuerySession
{
    private static final Logger log = Logger.getLogger(QuerySessionDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(QuerySessionDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(QuerySessionDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String dataService;

    @Persistent(defaultFetchGroup = "true")
    private String serviceUrl;

    @Persistent(defaultFetchGroup = "true")
    private String inputFormat;

    @Persistent(defaultFetchGroup = "true")
    private String outputFormat;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="referer", columns=@Column(name="referrerInforeferer")),
        @Persistent(name="userAgent", columns=@Column(name="referrerInfouserAgent")),
        @Persistent(name="language", columns=@Column(name="referrerInfolanguage")),
        @Persistent(name="hostname", columns=@Column(name="referrerInfohostname")),
        @Persistent(name="ipAddress", columns=@Column(name="referrerInfoipAddress")),
        @Persistent(name="note", columns=@Column(name="referrerInfonote")),
    })
    private ReferrerInfoStructDataObject referrerInfo;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public QuerySessionDataObject()
    {
        this(null);
    }
    public QuerySessionDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public QuerySessionDataObject(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note)
    {
        this(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note, null, null);
    }
    public QuerySessionDataObject(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.dataService = dataService;
        this.serviceUrl = serviceUrl;
        this.inputFormat = inputFormat;
        this.outputFormat = outputFormat;
        if(referrerInfo != null) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = null;
        }
        this.status = status;
        this.note = note;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return QuerySessionDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return QuerySessionDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getDataService()
    {
        return this.dataService;
    }
    public void setDataService(String dataService)
    {
        this.dataService = dataService;
    }

    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    public String getInputFormat()
    {
        return this.inputFormat;
    }
    public void setInputFormat(String inputFormat)
    {
        this.inputFormat = inputFormat;
    }

    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(referrerInfo == null) {
            this.referrerInfo = null;
            log.log(Level.INFO, "QuerySessionDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is null.");            
        } else if(referrerInfo instanceof ReferrerInfoStructDataObject) {
            this.referrerInfo = (ReferrerInfoStructDataObject) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = new ReferrerInfoStructDataObject();   // ????
            log.log(Level.WARNING, "QuerySessionDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is of an invalid type.");
        }
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("dataService", this.dataService);
        dataMap.put("serviceUrl", this.serviceUrl);
        dataMap.put("inputFormat", this.inputFormat);
        dataMap.put("outputFormat", this.outputFormat);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        QuerySession thatObj = (QuerySession) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.dataService == null && thatObj.getDataService() != null)
            || (this.dataService != null && thatObj.getDataService() == null)
            || !this.dataService.equals(thatObj.getDataService()) ) {
            return false;
        }
        if( (this.serviceUrl == null && thatObj.getServiceUrl() != null)
            || (this.serviceUrl != null && thatObj.getServiceUrl() == null)
            || !this.serviceUrl.equals(thatObj.getServiceUrl()) ) {
            return false;
        }
        if( (this.inputFormat == null && thatObj.getInputFormat() != null)
            || (this.inputFormat != null && thatObj.getInputFormat() == null)
            || !this.inputFormat.equals(thatObj.getInputFormat()) ) {
            return false;
        }
        if( (this.outputFormat == null && thatObj.getOutputFormat() != null)
            || (this.outputFormat != null && thatObj.getOutputFormat() == null)
            || !this.outputFormat.equals(thatObj.getOutputFormat()) ) {
            return false;
        }
        if( (this.referrerInfo == null && thatObj.getReferrerInfo() != null)
            || (this.referrerInfo != null && thatObj.getReferrerInfo() == null)
            || !this.referrerInfo.equals(thatObj.getReferrerInfo()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = dataService == null ? 0 : dataService.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputFormat == null ? 0 : inputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFormat == null ? 0 : outputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
