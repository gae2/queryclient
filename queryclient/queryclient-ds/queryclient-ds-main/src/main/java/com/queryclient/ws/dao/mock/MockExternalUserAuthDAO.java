package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.ExternalUserAuthDAO;
import com.queryclient.ws.data.ExternalUserAuthDataObject;


// MockExternalUserAuthDAO is a decorator.
// It can be used as a base class to mock ExternalUserAuthDAO objects.
public abstract class MockExternalUserAuthDAO implements ExternalUserAuthDAO
{
    private static final Logger log = Logger.getLogger(MockExternalUserAuthDAO.class.getName()); 

    // MockExternalUserAuthDAO uses the decorator design pattern.
    private ExternalUserAuthDAO decoratedDAO;

    public MockExternalUserAuthDAO(ExternalUserAuthDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected ExternalUserAuthDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(ExternalUserAuthDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public ExternalUserAuthDataObject getExternalUserAuth(String guid) throws BaseException
    {
        return decoratedDAO.getExternalUserAuth(guid);
	}

    @Override
    public List<ExternalUserAuthDataObject> getExternalUserAuths(List<String> guids) throws BaseException
    {
        return decoratedDAO.getExternalUserAuths(guids);
    }

    @Override
    public List<ExternalUserAuthDataObject> getAllExternalUserAuths() throws BaseException
	{
	    return getAllExternalUserAuths(null, null, null);
    }


    @Override
    public List<ExternalUserAuthDataObject> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllExternalUserAuths(ordering, offset, count, null);
    }

    @Override
    public List<ExternalUserAuthDataObject> getAllExternalUserAuths(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllExternalUserAuths(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllExternalUserAuthKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllExternalUserAuthKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<ExternalUserAuthDataObject> findExternalUserAuths(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findExternalUserAuths(filter, ordering, params, values, null, null);
    }

    @Override
	public List<ExternalUserAuthDataObject> findExternalUserAuths(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findExternalUserAuths(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<ExternalUserAuthDataObject> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<ExternalUserAuthDataObject> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createExternalUserAuth(ExternalUserAuthDataObject externalUserAuth) throws BaseException
    {
        return decoratedDAO.createExternalUserAuth( externalUserAuth);
    }

    @Override
	public Boolean updateExternalUserAuth(ExternalUserAuthDataObject externalUserAuth) throws BaseException
	{
        return decoratedDAO.updateExternalUserAuth(externalUserAuth);
	}
	
    @Override
    public Boolean deleteExternalUserAuth(ExternalUserAuthDataObject externalUserAuth) throws BaseException
    {
        return decoratedDAO.deleteExternalUserAuth(externalUserAuth);
    }

    @Override
    public Boolean deleteExternalUserAuth(String guid) throws BaseException
    {
        return decoratedDAO.deleteExternalUserAuth(guid);
	}

    @Override
    public Long deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteExternalUserAuths(filter, params, values);
    }

}
