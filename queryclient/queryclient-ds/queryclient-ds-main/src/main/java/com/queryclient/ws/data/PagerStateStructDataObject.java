package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.PagerStateStruct;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class PagerStateStructDataObject implements PagerStateStruct, Serializable
{
    private static final Logger log = Logger.getLogger(PagerStateStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _pagerstatestruct_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String pagerMode;

    @Persistent(defaultFetchGroup = "true")
    private String primaryOrdering;

    @Persistent(defaultFetchGroup = "true")
    private String secondaryOrdering;

    @Persistent(defaultFetchGroup = "true")
    private Long currentOffset;

    @Persistent(defaultFetchGroup = "true")
    private Long currentPage;

    @Persistent(defaultFetchGroup = "true")
    private Integer pageSize;

    @Persistent(defaultFetchGroup = "true")
    private Long totalCount;

    @Persistent(defaultFetchGroup = "true")
    private Long lowerBoundTotalCount;

    @Persistent(defaultFetchGroup = "true")
    private Long previousPageOffset;

    @Persistent(defaultFetchGroup = "true")
    private Long nextPageOffset;

    @Persistent(defaultFetchGroup = "true")
    private Long lastPageOffset;

    @Persistent(defaultFetchGroup = "true")
    private Long lastPageIndex;

    @Persistent(defaultFetchGroup = "true")
    private Boolean firstActionEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean previousActionEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean nextActionEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean lastActionEnabled;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public PagerStateStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public PagerStateStructDataObject(String pagerMode, String primaryOrdering, String secondaryOrdering, Long currentOffset, Long currentPage, Integer pageSize, Long totalCount, Long lowerBoundTotalCount, Long previousPageOffset, Long nextPageOffset, Long lastPageOffset, Long lastPageIndex, Boolean firstActionEnabled, Boolean previousActionEnabled, Boolean nextActionEnabled, Boolean lastActionEnabled, String note)
    {
        setPagerMode(pagerMode);
        setPrimaryOrdering(primaryOrdering);
        setSecondaryOrdering(secondaryOrdering);
        setCurrentOffset(currentOffset);
        setCurrentPage(currentPage);
        setPageSize(pageSize);
        setTotalCount(totalCount);
        setLowerBoundTotalCount(lowerBoundTotalCount);
        setPreviousPageOffset(previousPageOffset);
        setNextPageOffset(nextPageOffset);
        setLastPageOffset(lastPageOffset);
        setLastPageIndex(lastPageIndex);
        setFirstActionEnabled(firstActionEnabled);
        setPreviousActionEnabled(previousActionEnabled);
        setNextActionEnabled(nextActionEnabled);
        setLastActionEnabled(lastActionEnabled);
        setNote(note);
    }

    private void resetEncodedKey()
    {
    }

    public String getPagerMode()
    {
        return this.pagerMode;
    }
    public void setPagerMode(String pagerMode)
    {
        this.pagerMode = pagerMode;
        if(this.pagerMode != null) {
            resetEncodedKey();
        }
    }

    public String getPrimaryOrdering()
    {
        return this.primaryOrdering;
    }
    public void setPrimaryOrdering(String primaryOrdering)
    {
        this.primaryOrdering = primaryOrdering;
        if(this.primaryOrdering != null) {
            resetEncodedKey();
        }
    }

    public String getSecondaryOrdering()
    {
        return this.secondaryOrdering;
    }
    public void setSecondaryOrdering(String secondaryOrdering)
    {
        this.secondaryOrdering = secondaryOrdering;
        if(this.secondaryOrdering != null) {
            resetEncodedKey();
        }
    }

    public Long getCurrentOffset()
    {
        return this.currentOffset;
    }
    public void setCurrentOffset(Long currentOffset)
    {
        this.currentOffset = currentOffset;
        if(this.currentOffset != null) {
            resetEncodedKey();
        }
    }

    public Long getCurrentPage()
    {
        return this.currentPage;
    }
    public void setCurrentPage(Long currentPage)
    {
        this.currentPage = currentPage;
        if(this.currentPage != null) {
            resetEncodedKey();
        }
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }
    public void setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;
        if(this.pageSize != null) {
            resetEncodedKey();
        }
    }

    public Long getTotalCount()
    {
        return this.totalCount;
    }
    public void setTotalCount(Long totalCount)
    {
        this.totalCount = totalCount;
        if(this.totalCount != null) {
            resetEncodedKey();
        }
    }

    public Long getLowerBoundTotalCount()
    {
        return this.lowerBoundTotalCount;
    }
    public void setLowerBoundTotalCount(Long lowerBoundTotalCount)
    {
        this.lowerBoundTotalCount = lowerBoundTotalCount;
        if(this.lowerBoundTotalCount != null) {
            resetEncodedKey();
        }
    }

    public Long getPreviousPageOffset()
    {
        return this.previousPageOffset;
    }
    public void setPreviousPageOffset(Long previousPageOffset)
    {
        this.previousPageOffset = previousPageOffset;
        if(this.previousPageOffset != null) {
            resetEncodedKey();
        }
    }

    public Long getNextPageOffset()
    {
        return this.nextPageOffset;
    }
    public void setNextPageOffset(Long nextPageOffset)
    {
        this.nextPageOffset = nextPageOffset;
        if(this.nextPageOffset != null) {
            resetEncodedKey();
        }
    }

    public Long getLastPageOffset()
    {
        return this.lastPageOffset;
    }
    public void setLastPageOffset(Long lastPageOffset)
    {
        this.lastPageOffset = lastPageOffset;
        if(this.lastPageOffset != null) {
            resetEncodedKey();
        }
    }

    public Long getLastPageIndex()
    {
        return this.lastPageIndex;
    }
    public void setLastPageIndex(Long lastPageIndex)
    {
        this.lastPageIndex = lastPageIndex;
        if(this.lastPageIndex != null) {
            resetEncodedKey();
        }
    }

    public Boolean isFirstActionEnabled()
    {
        return this.firstActionEnabled;
    }
    public void setFirstActionEnabled(Boolean firstActionEnabled)
    {
        this.firstActionEnabled = firstActionEnabled;
        if(this.firstActionEnabled != null) {
            resetEncodedKey();
        }
    }

    public Boolean isPreviousActionEnabled()
    {
        return this.previousActionEnabled;
    }
    public void setPreviousActionEnabled(Boolean previousActionEnabled)
    {
        this.previousActionEnabled = previousActionEnabled;
        if(this.previousActionEnabled != null) {
            resetEncodedKey();
        }
    }

    public Boolean isNextActionEnabled()
    {
        return this.nextActionEnabled;
    }
    public void setNextActionEnabled(Boolean nextActionEnabled)
    {
        this.nextActionEnabled = nextActionEnabled;
        if(this.nextActionEnabled != null) {
            resetEncodedKey();
        }
    }

    public Boolean isLastActionEnabled()
    {
        return this.lastActionEnabled;
    }
    public void setLastActionEnabled(Boolean lastActionEnabled)
    {
        this.lastActionEnabled = lastActionEnabled;
        if(this.lastActionEnabled != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPagerMode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPrimaryOrdering() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecondaryOrdering() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCurrentOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCurrentPage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPageSize() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTotalCount() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLowerBoundTotalCount() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPreviousPageOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNextPageOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastPageOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastPageIndex() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isFirstActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isPreviousActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isNextActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isLastActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("pagerMode", this.pagerMode);
        dataMap.put("primaryOrdering", this.primaryOrdering);
        dataMap.put("secondaryOrdering", this.secondaryOrdering);
        dataMap.put("currentOffset", this.currentOffset);
        dataMap.put("currentPage", this.currentPage);
        dataMap.put("pageSize", this.pageSize);
        dataMap.put("totalCount", this.totalCount);
        dataMap.put("lowerBoundTotalCount", this.lowerBoundTotalCount);
        dataMap.put("previousPageOffset", this.previousPageOffset);
        dataMap.put("nextPageOffset", this.nextPageOffset);
        dataMap.put("lastPageOffset", this.lastPageOffset);
        dataMap.put("lastPageIndex", this.lastPageIndex);
        dataMap.put("firstActionEnabled", this.firstActionEnabled);
        dataMap.put("previousActionEnabled", this.previousActionEnabled);
        dataMap.put("nextActionEnabled", this.nextActionEnabled);
        dataMap.put("lastActionEnabled", this.lastActionEnabled);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        PagerStateStruct thatObj = (PagerStateStruct) obj;
        if( (this.pagerMode == null && thatObj.getPagerMode() != null)
            || (this.pagerMode != null && thatObj.getPagerMode() == null)
            || !this.pagerMode.equals(thatObj.getPagerMode()) ) {
            return false;
        }
        if( (this.primaryOrdering == null && thatObj.getPrimaryOrdering() != null)
            || (this.primaryOrdering != null && thatObj.getPrimaryOrdering() == null)
            || !this.primaryOrdering.equals(thatObj.getPrimaryOrdering()) ) {
            return false;
        }
        if( (this.secondaryOrdering == null && thatObj.getSecondaryOrdering() != null)
            || (this.secondaryOrdering != null && thatObj.getSecondaryOrdering() == null)
            || !this.secondaryOrdering.equals(thatObj.getSecondaryOrdering()) ) {
            return false;
        }
        if( (this.currentOffset == null && thatObj.getCurrentOffset() != null)
            || (this.currentOffset != null && thatObj.getCurrentOffset() == null)
            || !this.currentOffset.equals(thatObj.getCurrentOffset()) ) {
            return false;
        }
        if( (this.currentPage == null && thatObj.getCurrentPage() != null)
            || (this.currentPage != null && thatObj.getCurrentPage() == null)
            || !this.currentPage.equals(thatObj.getCurrentPage()) ) {
            return false;
        }
        if( (this.pageSize == null && thatObj.getPageSize() != null)
            || (this.pageSize != null && thatObj.getPageSize() == null)
            || !this.pageSize.equals(thatObj.getPageSize()) ) {
            return false;
        }
        if( (this.totalCount == null && thatObj.getTotalCount() != null)
            || (this.totalCount != null && thatObj.getTotalCount() == null)
            || !this.totalCount.equals(thatObj.getTotalCount()) ) {
            return false;
        }
        if( (this.lowerBoundTotalCount == null && thatObj.getLowerBoundTotalCount() != null)
            || (this.lowerBoundTotalCount != null && thatObj.getLowerBoundTotalCount() == null)
            || !this.lowerBoundTotalCount.equals(thatObj.getLowerBoundTotalCount()) ) {
            return false;
        }
        if( (this.previousPageOffset == null && thatObj.getPreviousPageOffset() != null)
            || (this.previousPageOffset != null && thatObj.getPreviousPageOffset() == null)
            || !this.previousPageOffset.equals(thatObj.getPreviousPageOffset()) ) {
            return false;
        }
        if( (this.nextPageOffset == null && thatObj.getNextPageOffset() != null)
            || (this.nextPageOffset != null && thatObj.getNextPageOffset() == null)
            || !this.nextPageOffset.equals(thatObj.getNextPageOffset()) ) {
            return false;
        }
        if( (this.lastPageOffset == null && thatObj.getLastPageOffset() != null)
            || (this.lastPageOffset != null && thatObj.getLastPageOffset() == null)
            || !this.lastPageOffset.equals(thatObj.getLastPageOffset()) ) {
            return false;
        }
        if( (this.lastPageIndex == null && thatObj.getLastPageIndex() != null)
            || (this.lastPageIndex != null && thatObj.getLastPageIndex() == null)
            || !this.lastPageIndex.equals(thatObj.getLastPageIndex()) ) {
            return false;
        }
        if( (this.firstActionEnabled == null && thatObj.isFirstActionEnabled() != null)
            || (this.firstActionEnabled != null && thatObj.isFirstActionEnabled() == null)
            || !this.firstActionEnabled.equals(thatObj.isFirstActionEnabled()) ) {
            return false;
        }
        if( (this.previousActionEnabled == null && thatObj.isPreviousActionEnabled() != null)
            || (this.previousActionEnabled != null && thatObj.isPreviousActionEnabled() == null)
            || !this.previousActionEnabled.equals(thatObj.isPreviousActionEnabled()) ) {
            return false;
        }
        if( (this.nextActionEnabled == null && thatObj.isNextActionEnabled() != null)
            || (this.nextActionEnabled != null && thatObj.isNextActionEnabled() == null)
            || !this.nextActionEnabled.equals(thatObj.isNextActionEnabled()) ) {
            return false;
        }
        if( (this.lastActionEnabled == null && thatObj.isLastActionEnabled() != null)
            || (this.lastActionEnabled != null && thatObj.isLastActionEnabled() == null)
            || !this.lastActionEnabled.equals(thatObj.isLastActionEnabled()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = pagerMode == null ? 0 : pagerMode.hashCode();
        _hash = 31 * _hash + delta;
        delta = primaryOrdering == null ? 0 : primaryOrdering.hashCode();
        _hash = 31 * _hash + delta;
        delta = secondaryOrdering == null ? 0 : secondaryOrdering.hashCode();
        _hash = 31 * _hash + delta;
        delta = currentOffset == null ? 0 : currentOffset.hashCode();
        _hash = 31 * _hash + delta;
        delta = currentPage == null ? 0 : currentPage.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageSize == null ? 0 : pageSize.hashCode();
        _hash = 31 * _hash + delta;
        delta = totalCount == null ? 0 : totalCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = lowerBoundTotalCount == null ? 0 : lowerBoundTotalCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = previousPageOffset == null ? 0 : previousPageOffset.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextPageOffset == null ? 0 : nextPageOffset.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastPageOffset == null ? 0 : lastPageOffset.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastPageIndex == null ? 0 : lastPageIndex.hashCode();
        _hash = 31 * _hash + delta;
        delta = firstActionEnabled == null ? 0 : firstActionEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = previousActionEnabled == null ? 0 : previousActionEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextActionEnabled == null ? 0 : nextActionEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastActionEnabled == null ? 0 : lastActionEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
