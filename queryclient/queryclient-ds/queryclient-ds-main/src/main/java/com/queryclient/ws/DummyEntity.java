package com.queryclient.ws;



public interface DummyEntity 
{
    String  getGuid();
    String  getUser();
    String  getName();
    String  getContent();
    Integer  getMaxLength();
    Boolean  isExpired();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
