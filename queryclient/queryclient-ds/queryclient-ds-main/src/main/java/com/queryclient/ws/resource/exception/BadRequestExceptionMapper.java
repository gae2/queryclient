package com.queryclient.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.queryclient.ws.stub.ErrorStub;

@Provider
public class BadRequestExceptionMapper implements ExceptionMapper<BadRequestRsException>
{
    public Response toResponse(BadRequestRsException ex) {
        return Response.status(Status.BAD_REQUEST)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
