package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.data.GaeAppStructDataObject;

public class GaeAppStructBean implements GaeAppStruct
{
    private static final Logger log = Logger.getLogger(GaeAppStructBean.class.getName());

    // Embedded data object.
    private GaeAppStructDataObject dobj = null;

    public GaeAppStructBean()
    {
        this(new GaeAppStructDataObject());
    }
    public GaeAppStructBean(GaeAppStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public GaeAppStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGroupId()
    {
        if(getDataObject() != null) {
            return getDataObject().getGroupId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setGroupId(String groupId)
    {
        if(getDataObject() != null) {
            getDataObject().setGroupId(groupId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
        }
    }

    public String getAppId()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppId(String appId)
    {
        if(getDataObject() != null) {
            getDataObject().setAppId(appId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
        }
    }

    public String getAppDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppDomain(String appDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setAppDomain(appDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
        }
    }

    public String getNamespace()
    {
        if(getDataObject() != null) {
            return getDataObject().getNamespace();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNamespace(String namespace)
    {
        if(getDataObject() != null) {
            getDataObject().setNamespace(namespace);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
        }
    }

    public Long getAcl()
    {
        if(getDataObject() != null) {
            return getDataObject().getAcl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAcl(Long acl)
    {
        if(getDataObject() != null) {
            getDataObject().setAcl(acl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeAppStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getGroupId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAppId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAppDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNamespace() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAcl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public GaeAppStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
