package com.queryclient.ws.resource;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.service.ApiConsumerService;
import com.queryclient.ws.service.impl.ApiConsumerServiceImpl;
import com.queryclient.ws.service.UserService;
import com.queryclient.ws.service.impl.UserServiceImpl;
import com.queryclient.ws.service.UserPasswordService;
import com.queryclient.ws.service.impl.UserPasswordServiceImpl;
import com.queryclient.ws.service.ExternalUserAuthService;
import com.queryclient.ws.service.impl.ExternalUserAuthServiceImpl;
import com.queryclient.ws.service.UserAuthStateService;
import com.queryclient.ws.service.impl.UserAuthStateServiceImpl;
import com.queryclient.ws.service.DataServiceService;
import com.queryclient.ws.service.impl.DataServiceServiceImpl;
import com.queryclient.ws.service.ServiceEndpointService;
import com.queryclient.ws.service.impl.ServiceEndpointServiceImpl;
import com.queryclient.ws.service.QuerySessionService;
import com.queryclient.ws.service.impl.QuerySessionServiceImpl;
import com.queryclient.ws.service.QueryRecordService;
import com.queryclient.ws.service.impl.QueryRecordServiceImpl;
import com.queryclient.ws.service.DummyEntityService;
import com.queryclient.ws.service.impl.DummyEntityServiceImpl;
import com.queryclient.ws.service.ServiceInfoService;
import com.queryclient.ws.service.impl.ServiceInfoServiceImpl;
import com.queryclient.ws.service.FiveTenService;
import com.queryclient.ws.service.impl.FiveTenServiceImpl;


// TBD:
// Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

	private static ApiConsumerService apiConsumerService = null;
	private static UserService userService = null;
	private static UserPasswordService userPasswordService = null;
	private static ExternalUserAuthService externalUserAuthService = null;
	private static UserAuthStateService userAuthStateService = null;
	private static DataServiceService dataServiceService = null;
	private static ServiceEndpointService serviceEndpointService = null;
	private static QuerySessionService querySessionService = null;
	private static QueryRecordService queryRecordService = null;
	private static DummyEntityService dummyEntityService = null;
	private static ServiceInfoService serviceInfoService = null;
	private static FiveTenService fiveTenService = null;

    // Prevents instantiation.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(ServiceManager.apiConsumerService == null) {
            ServiceManager.apiConsumerService = new ApiConsumerServiceImpl();
        }
        return ServiceManager.apiConsumerService;
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(ServiceManager.userService == null) {
            ServiceManager.userService = new UserServiceImpl();
        }
        return ServiceManager.userService;
    }

    // Returns a UserPasswordService instance.
	public static UserPasswordService getUserPasswordService() 
    {
        if(ServiceManager.userPasswordService == null) {
            ServiceManager.userPasswordService = new UserPasswordServiceImpl();
        }
        return ServiceManager.userPasswordService;
    }

    // Returns a ExternalUserAuthService instance.
	public static ExternalUserAuthService getExternalUserAuthService() 
    {
        if(ServiceManager.externalUserAuthService == null) {
            ServiceManager.externalUserAuthService = new ExternalUserAuthServiceImpl();
        }
        return ServiceManager.externalUserAuthService;
    }

    // Returns a UserAuthStateService instance.
	public static UserAuthStateService getUserAuthStateService() 
    {
        if(ServiceManager.userAuthStateService == null) {
            ServiceManager.userAuthStateService = new UserAuthStateServiceImpl();
        }
        return ServiceManager.userAuthStateService;
    }

    // Returns a DataServiceService instance.
	public static DataServiceService getDataServiceService() 
    {
        if(ServiceManager.dataServiceService == null) {
            ServiceManager.dataServiceService = new DataServiceServiceImpl();
        }
        return ServiceManager.dataServiceService;
    }

    // Returns a ServiceEndpointService instance.
	public static ServiceEndpointService getServiceEndpointService() 
    {
        if(ServiceManager.serviceEndpointService == null) {
            ServiceManager.serviceEndpointService = new ServiceEndpointServiceImpl();
        }
        return ServiceManager.serviceEndpointService;
    }

    // Returns a QuerySessionService instance.
	public static QuerySessionService getQuerySessionService() 
    {
        if(ServiceManager.querySessionService == null) {
            ServiceManager.querySessionService = new QuerySessionServiceImpl();
        }
        return ServiceManager.querySessionService;
    }

    // Returns a QueryRecordService instance.
	public static QueryRecordService getQueryRecordService() 
    {
        if(ServiceManager.queryRecordService == null) {
            ServiceManager.queryRecordService = new QueryRecordServiceImpl();
        }
        return ServiceManager.queryRecordService;
    }

    // Returns a DummyEntityService instance.
	public static DummyEntityService getDummyEntityService() 
    {
        if(ServiceManager.dummyEntityService == null) {
            ServiceManager.dummyEntityService = new DummyEntityServiceImpl();
        }
        return ServiceManager.dummyEntityService;
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(ServiceManager.serviceInfoService == null) {
            ServiceManager.serviceInfoService = new ServiceInfoServiceImpl();
        }
        return ServiceManager.serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(ServiceManager.fiveTenService == null) {
            ServiceManager.fiveTenService = new FiveTenServiceImpl();
        }
        return ServiceManager.fiveTenService;
    }

}
