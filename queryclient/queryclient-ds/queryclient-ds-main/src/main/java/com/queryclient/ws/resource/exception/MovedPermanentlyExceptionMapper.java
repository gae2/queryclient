package com.queryclient.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.stub.ErrorStub;

@Provider
public class MovedPermanentlyExceptionMapper implements ExceptionMapper<MovedPermanentlyRsException>
{
    public Response toResponse(MovedPermanentlyRsException ex) {
        return Response.status(StatusCode.MOVED_PERMANENTLY)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
