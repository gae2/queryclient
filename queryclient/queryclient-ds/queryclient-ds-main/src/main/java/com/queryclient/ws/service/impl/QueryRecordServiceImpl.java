package com.queryclient.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.bean.ReferrerInfoStructBean;
import com.queryclient.ws.bean.QueryRecordBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.ReferrerInfoStructDataObject;
import com.queryclient.ws.data.QueryRecordDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.QueryRecordService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class QueryRecordServiceImpl implements QueryRecordService
{
    private static final Logger log = Logger.getLogger(QueryRecordServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // QueryRecord related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public QueryRecord getQueryRecord(String guid) throws BaseException
    {
        log.finer("BEGIN");

        QueryRecordDataObject dataObj = getDAOFactory().getQueryRecordDAO().getQueryRecord(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve QueryRecordDataObject for guid = " + guid);
            return null;  // ????
        }
        QueryRecordBean bean = new QueryRecordBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getQueryRecord(String guid, String field) throws BaseException
    {
        QueryRecordDataObject dataObj = getDAOFactory().getQueryRecordDAO().getQueryRecord(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve QueryRecordDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("querySession")) {
            return dataObj.getQuerySession();
        } else if(field.equals("queryId")) {
            return dataObj.getQueryId();
        } else if(field.equals("dataService")) {
            return dataObj.getDataService();
        } else if(field.equals("serviceUrl")) {
            return dataObj.getServiceUrl();
        } else if(field.equals("delayed")) {
            return dataObj.isDelayed();
        } else if(field.equals("query")) {
            return dataObj.getQuery();
        } else if(field.equals("inputFormat")) {
            return dataObj.getInputFormat();
        } else if(field.equals("inputFile")) {
            return dataObj.getInputFile();
        } else if(field.equals("inputContent")) {
            return dataObj.getInputContent();
        } else if(field.equals("targetOutputFormat")) {
            return dataObj.getTargetOutputFormat();
        } else if(field.equals("outputFormat")) {
            return dataObj.getOutputFormat();
        } else if(field.equals("outputFile")) {
            return dataObj.getOutputFile();
        } else if(field.equals("outputContent")) {
            return dataObj.getOutputContent();
        } else if(field.equals("responseCode")) {
            return dataObj.getResponseCode();
        } else if(field.equals("result")) {
            return dataObj.getResult();
        } else if(field.equals("referrerInfo")) {
            return dataObj.getReferrerInfo();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("extra")) {
            return dataObj.getExtra();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("scheduledTime")) {
            return dataObj.getScheduledTime();
        } else if(field.equals("processedTime")) {
            return dataObj.getProcessedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<QueryRecord> getQueryRecords(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<QueryRecord> list = new ArrayList<QueryRecord>();
        List<QueryRecordDataObject> dataObjs = getDAOFactory().getQueryRecordDAO().getQueryRecords(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve QueryRecordDataObject list.");
        } else {
            Iterator<QueryRecordDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                QueryRecordDataObject dataObj = (QueryRecordDataObject) it.next();
                list.add(new QueryRecordBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<QueryRecord> getAllQueryRecords() throws BaseException
    {
        return getAllQueryRecords(null, null, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecords(ordering, offset, count, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQueryRecords(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<QueryRecord> list = new ArrayList<QueryRecord>();
        List<QueryRecordDataObject> dataObjs = getDAOFactory().getQueryRecordDAO().getAllQueryRecords(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve QueryRecordDataObject list.");
        } else {
            Iterator<QueryRecordDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                QueryRecordDataObject dataObj = (QueryRecordDataObject) it.next();
                list.add(new QueryRecordBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllQueryRecordKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getQueryRecordDAO().getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve QueryRecord key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("QueryRecordServiceImpl.findQueryRecords(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<QueryRecord> list = new ArrayList<QueryRecord>();
        List<QueryRecordDataObject> dataObjs = getDAOFactory().getQueryRecordDAO().findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find queryRecords for the given criterion.");
        } else {
            Iterator<QueryRecordDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                QueryRecordDataObject dataObj = (QueryRecordDataObject) it.next();
                list.add(new QueryRecordBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("QueryRecordServiceImpl.findQueryRecordKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getQueryRecordDAO().findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find QueryRecord keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("QueryRecordServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getQueryRecordDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createQueryRecord(String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        
        QueryRecordDataObject dataObj = new QueryRecordDataObject(null, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfoDobj, status, extra, note, scheduledTime, processedTime);
        return createQueryRecord(dataObj);
    }

    @Override
    public String createQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");

        // Param queryRecord cannot be null.....
        if(queryRecord == null) {
            log.log(Level.INFO, "Param queryRecord is null!");
            throw new BadRequestException("Param queryRecord object is null!");
        }
        QueryRecordDataObject dataObj = null;
        if(queryRecord instanceof QueryRecordDataObject) {
            dataObj = (QueryRecordDataObject) queryRecord;
        } else if(queryRecord instanceof QueryRecordBean) {
            dataObj = ((QueryRecordBean) queryRecord).toDataObject();
        } else {  // if(queryRecord instanceof QueryRecord)
            //dataObj = new QueryRecordDataObject(null, queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), (ReferrerInfoStructDataObject) queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new QueryRecordDataObject(queryRecord.getGuid(), queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), (ReferrerInfoStructDataObject) queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
        }
        String guid = getDAOFactory().getQueryRecordDAO().createQueryRecord(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();            
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        QueryRecordDataObject dataObj = new QueryRecordDataObject(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfoDobj, status, extra, note, scheduledTime, processedTime);
        return updateQueryRecord(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");

        // Param queryRecord cannot be null.....
        if(queryRecord == null || queryRecord.getGuid() == null) {
            log.log(Level.INFO, "Param queryRecord or its guid is null!");
            throw new BadRequestException("Param queryRecord object or its guid is null!");
        }
        QueryRecordDataObject dataObj = null;
        if(queryRecord instanceof QueryRecordDataObject) {
            dataObj = (QueryRecordDataObject) queryRecord;
        } else if(queryRecord instanceof QueryRecordBean) {
            dataObj = ((QueryRecordBean) queryRecord).toDataObject();
        } else {  // if(queryRecord instanceof QueryRecord)
            dataObj = new QueryRecordDataObject(queryRecord.getGuid(), queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
        }
        Boolean suc = getDAOFactory().getQueryRecordDAO().updateQueryRecord(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteQueryRecord(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getQueryRecordDAO().deleteQueryRecord(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");

        // Param queryRecord cannot be null.....
        if(queryRecord == null || queryRecord.getGuid() == null) {
            log.log(Level.INFO, "Param queryRecord or its guid is null!");
            throw new BadRequestException("Param queryRecord object or its guid is null!");
        }
        QueryRecordDataObject dataObj = null;
        if(queryRecord instanceof QueryRecordDataObject) {
            dataObj = (QueryRecordDataObject) queryRecord;
        } else if(queryRecord instanceof QueryRecordBean) {
            dataObj = ((QueryRecordBean) queryRecord).toDataObject();
        } else {  // if(queryRecord instanceof QueryRecord)
            dataObj = new QueryRecordDataObject(queryRecord.getGuid(), queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
        }
        Boolean suc = getDAOFactory().getQueryRecordDAO().deleteQueryRecord(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteQueryRecords(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getQueryRecordDAO().deleteQueryRecords(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
