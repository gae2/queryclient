package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "externalUserAuths")
@XmlType(propOrder = {"externalUserAuth", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalUserAuthListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExternalUserAuthListStub.class.getName());

    private List<ExternalUserAuthStub> externalUserAuths = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public ExternalUserAuthListStub()
    {
        this(new ArrayList<ExternalUserAuthStub>());
    }
    public ExternalUserAuthListStub(List<ExternalUserAuthStub> externalUserAuths)
    {
        this(externalUserAuths, null);
    }
    public ExternalUserAuthListStub(List<ExternalUserAuthStub> externalUserAuths, String forwardCursor)
    {
        this.externalUserAuths = externalUserAuths;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(externalUserAuths == null) {
            return true;
        } else {
            return externalUserAuths.isEmpty();
        }
    }
    public int getSize()
    {
        if(externalUserAuths == null) {
            return 0;
        } else {
            return externalUserAuths.size();
        }
    }


    @XmlElement(name = "externalUserAuth")
    public List<ExternalUserAuthStub> getExternalUserAuth()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ExternalUserAuthStub> getList()
    {
        return externalUserAuths;
    }
    public void setList(List<ExternalUserAuthStub> externalUserAuths)
    {
        this.externalUserAuths = externalUserAuths;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<ExternalUserAuthStub> it = this.externalUserAuths.iterator();
        while(it.hasNext()) {
            ExternalUserAuthStub externalUserAuth = it.next();
            sb.append(externalUserAuth.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ExternalUserAuthListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ExternalUserAuthListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ExternalUserAuthListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ExternalUserAuthListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ExternalUserAuthListStub object as a string.", e);
        }
        
        return null;
    }
    public static ExternalUserAuthListStub fromJsonString(String jsonStr)
    {
        try {
            ExternalUserAuthListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ExternalUserAuthListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalUserAuthListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalUserAuthListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalUserAuthListStub object.", e);
        }
        
        return null;
    }

}
