package com.queryclient.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.bean.GaeAppStructBean;
import com.queryclient.ws.bean.ExternalUserIdStructBean;
import com.queryclient.ws.bean.ExternalUserAuthBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.GaeAppStructDataObject;
import com.queryclient.ws.data.ExternalUserIdStructDataObject;
import com.queryclient.ws.data.ExternalUserAuthDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.ExternalUserAuthService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ExternalUserAuthServiceImpl implements ExternalUserAuthService
{
    private static final Logger log = Logger.getLogger(ExternalUserAuthServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ExternalUserAuth related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ExternalUserAuth getExternalUserAuth(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ExternalUserAuthDataObject dataObj = getDAOFactory().getExternalUserAuthDAO().getExternalUserAuth(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ExternalUserAuthDataObject for guid = " + guid);
            return null;  // ????
        }
        ExternalUserAuthBean bean = new ExternalUserAuthBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getExternalUserAuth(String guid, String field) throws BaseException
    {
        ExternalUserAuthDataObject dataObj = getDAOFactory().getExternalUserAuthDAO().getExternalUserAuth(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ExternalUserAuthDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return dataObj.getManagerApp();
        } else if(field.equals("appAcl")) {
            return dataObj.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return dataObj.getGaeApp();
        } else if(field.equals("ownerUser")) {
            return dataObj.getOwnerUser();
        } else if(field.equals("userAcl")) {
            return dataObj.getUserAcl();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("providerId")) {
            return dataObj.getProviderId();
        } else if(field.equals("externalUserId")) {
            return dataObj.getExternalUserId();
        } else if(field.equals("requestToken")) {
            return dataObj.getRequestToken();
        } else if(field.equals("accessToken")) {
            return dataObj.getAccessToken();
        } else if(field.equals("accessTokenSecret")) {
            return dataObj.getAccessTokenSecret();
        } else if(field.equals("email")) {
            return dataObj.getEmail();
        } else if(field.equals("firstName")) {
            return dataObj.getFirstName();
        } else if(field.equals("lastName")) {
            return dataObj.getLastName();
        } else if(field.equals("fullName")) {
            return dataObj.getFullName();
        } else if(field.equals("displayName")) {
            return dataObj.getDisplayName();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("gender")) {
            return dataObj.getGender();
        } else if(field.equals("dateOfBirth")) {
            return dataObj.getDateOfBirth();
        } else if(field.equals("profileImageUrl")) {
            return dataObj.getProfileImageUrl();
        } else if(field.equals("timeZone")) {
            return dataObj.getTimeZone();
        } else if(field.equals("postalCode")) {
            return dataObj.getPostalCode();
        } else if(field.equals("location")) {
            return dataObj.getLocation();
        } else if(field.equals("country")) {
            return dataObj.getCountry();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("authTime")) {
            return dataObj.getAuthTime();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ExternalUserAuth> getExternalUserAuths(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ExternalUserAuth> list = new ArrayList<ExternalUserAuth>();
        List<ExternalUserAuthDataObject> dataObjs = getDAOFactory().getExternalUserAuthDAO().getExternalUserAuths(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ExternalUserAuthDataObject list.");
        } else {
            Iterator<ExternalUserAuthDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ExternalUserAuthDataObject dataObj = (ExternalUserAuthDataObject) it.next();
                list.add(new ExternalUserAuthBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths() throws BaseException
    {
        return getAllExternalUserAuths(null, null, null);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllExternalUserAuths(ordering, offset, count, null);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllExternalUserAuths(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ExternalUserAuth> list = new ArrayList<ExternalUserAuth>();
        List<ExternalUserAuthDataObject> dataObjs = getDAOFactory().getExternalUserAuthDAO().getAllExternalUserAuths(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ExternalUserAuthDataObject list.");
        } else {
            Iterator<ExternalUserAuthDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ExternalUserAuthDataObject dataObj = (ExternalUserAuthDataObject) it.next();
                list.add(new ExternalUserAuthBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllExternalUserAuthKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllExternalUserAuthKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getExternalUserAuthDAO().getAllExternalUserAuthKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ExternalUserAuth key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findExternalUserAuths(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ExternalUserAuthServiceImpl.findExternalUserAuths(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ExternalUserAuth> list = new ArrayList<ExternalUserAuth>();
        List<ExternalUserAuthDataObject> dataObjs = getDAOFactory().getExternalUserAuthDAO().findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find externalUserAuths for the given criterion.");
        } else {
            Iterator<ExternalUserAuthDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ExternalUserAuthDataObject dataObj = (ExternalUserAuthDataObject) it.next();
                list.add(new ExternalUserAuthBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ExternalUserAuthServiceImpl.findExternalUserAuthKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getExternalUserAuthDAO().findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ExternalUserAuth keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ExternalUserAuthServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getExternalUserAuthDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createExternalUserAuth(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        ExternalUserIdStructDataObject externalUserIdDobj = null;
        if(externalUserId instanceof ExternalUserIdStructBean) {
            externalUserIdDobj = ((ExternalUserIdStructBean) externalUserId).toDataObject();
        } else if(externalUserId instanceof ExternalUserIdStruct) {
            externalUserIdDobj = new ExternalUserIdStructDataObject(externalUserId.getUuid(), externalUserId.getId(), externalUserId.getName(), externalUserId.getEmail(), externalUserId.getUsername(), externalUserId.getOpenId(), externalUserId.getNote());
        } else {
            externalUserIdDobj = null;   // ????
        }
        
        ExternalUserAuthDataObject dataObj = new ExternalUserAuthDataObject(null, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, user, providerId, externalUserIdDobj, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
        return createExternalUserAuth(dataObj);
    }

    @Override
    public String createExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("BEGIN");

        // Param externalUserAuth cannot be null.....
        if(externalUserAuth == null) {
            log.log(Level.INFO, "Param externalUserAuth is null!");
            throw new BadRequestException("Param externalUserAuth object is null!");
        }
        ExternalUserAuthDataObject dataObj = null;
        if(externalUserAuth instanceof ExternalUserAuthDataObject) {
            dataObj = (ExternalUserAuthDataObject) externalUserAuth;
        } else if(externalUserAuth instanceof ExternalUserAuthBean) {
            dataObj = ((ExternalUserAuthBean) externalUserAuth).toDataObject();
        } else {  // if(externalUserAuth instanceof ExternalUserAuth)
            //dataObj = new ExternalUserAuthDataObject(null, externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), (GaeAppStructDataObject) externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), (ExternalUserIdStructDataObject) externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ExternalUserAuthDataObject(externalUserAuth.getGuid(), externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), (GaeAppStructDataObject) externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), (ExternalUserIdStructDataObject) externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
        }
        String guid = getDAOFactory().getExternalUserAuthDAO().createExternalUserAuth(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateExternalUserAuth(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        ExternalUserIdStructDataObject externalUserIdDobj = null;
        if(externalUserId instanceof ExternalUserIdStructBean) {
            externalUserIdDobj = ((ExternalUserIdStructBean) externalUserId).toDataObject();            
        } else if(externalUserId instanceof ExternalUserIdStruct) {
            externalUserIdDobj = new ExternalUserIdStructDataObject(externalUserId.getUuid(), externalUserId.getId(), externalUserId.getName(), externalUserId.getEmail(), externalUserId.getUsername(), externalUserId.getOpenId(), externalUserId.getNote());
        } else {
            externalUserIdDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ExternalUserAuthDataObject dataObj = new ExternalUserAuthDataObject(guid, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, user, providerId, externalUserIdDobj, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
        return updateExternalUserAuth(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("BEGIN");

        // Param externalUserAuth cannot be null.....
        if(externalUserAuth == null || externalUserAuth.getGuid() == null) {
            log.log(Level.INFO, "Param externalUserAuth or its guid is null!");
            throw new BadRequestException("Param externalUserAuth object or its guid is null!");
        }
        ExternalUserAuthDataObject dataObj = null;
        if(externalUserAuth instanceof ExternalUserAuthDataObject) {
            dataObj = (ExternalUserAuthDataObject) externalUserAuth;
        } else if(externalUserAuth instanceof ExternalUserAuthBean) {
            dataObj = ((ExternalUserAuthBean) externalUserAuth).toDataObject();
        } else {  // if(externalUserAuth instanceof ExternalUserAuth)
            dataObj = new ExternalUserAuthDataObject(externalUserAuth.getGuid(), externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getExternalUserAuthDAO().updateExternalUserAuth(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteExternalUserAuth(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getExternalUserAuthDAO().deleteExternalUserAuth(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("BEGIN");

        // Param externalUserAuth cannot be null.....
        if(externalUserAuth == null || externalUserAuth.getGuid() == null) {
            log.log(Level.INFO, "Param externalUserAuth or its guid is null!");
            throw new BadRequestException("Param externalUserAuth object or its guid is null!");
        }
        ExternalUserAuthDataObject dataObj = null;
        if(externalUserAuth instanceof ExternalUserAuthDataObject) {
            dataObj = (ExternalUserAuthDataObject) externalUserAuth;
        } else if(externalUserAuth instanceof ExternalUserAuthBean) {
            dataObj = ((ExternalUserAuthBean) externalUserAuth).toDataObject();
        } else {  // if(externalUserAuth instanceof ExternalUserAuth)
            dataObj = new ExternalUserAuthDataObject(externalUserAuth.getGuid(), externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getExternalUserAuthDAO().deleteExternalUserAuth(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getExternalUserAuthDAO().deleteExternalUserAuths(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
