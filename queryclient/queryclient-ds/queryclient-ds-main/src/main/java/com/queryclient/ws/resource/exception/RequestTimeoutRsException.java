package com.queryclient.ws.resource.exception;

import com.queryclient.ws.exception.resource.BaseResourceException;


public class RequestTimeoutRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public RequestTimeoutRsException() 
    {
        super();
    }
    public RequestTimeoutRsException(String message) 
    {
        super(message);
    }
    public RequestTimeoutRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public RequestTimeoutRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public RequestTimeoutRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public RequestTimeoutRsException(Throwable cause) 
    {
        super(cause);
    }
    public RequestTimeoutRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
