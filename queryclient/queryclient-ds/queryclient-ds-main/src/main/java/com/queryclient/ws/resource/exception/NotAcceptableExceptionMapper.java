package com.queryclient.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.queryclient.ws.stub.ErrorStub;

@Provider
public class NotAcceptableExceptionMapper implements ExceptionMapper<NotAcceptableRsException>
{
    public Response toResponse(NotAcceptableRsException ex) {
        return Response.status(Status.NOT_ACCEPTABLE)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
