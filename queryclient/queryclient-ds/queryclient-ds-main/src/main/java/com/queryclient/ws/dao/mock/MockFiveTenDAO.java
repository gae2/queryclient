package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.FiveTenDAO;
import com.queryclient.ws.data.FiveTenDataObject;


// MockFiveTenDAO is a decorator.
// It can be used as a base class to mock FiveTenDAO objects.
public abstract class MockFiveTenDAO implements FiveTenDAO
{
    private static final Logger log = Logger.getLogger(MockFiveTenDAO.class.getName()); 

    // MockFiveTenDAO uses the decorator design pattern.
    private FiveTenDAO decoratedDAO;

    public MockFiveTenDAO(FiveTenDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected FiveTenDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(FiveTenDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public FiveTenDataObject getFiveTen(String guid) throws BaseException
    {
        return decoratedDAO.getFiveTen(guid);
	}

    @Override
    public List<FiveTenDataObject> getFiveTens(List<String> guids) throws BaseException
    {
        return decoratedDAO.getFiveTens(guids);
    }

    @Override
    public List<FiveTenDataObject> getAllFiveTens() throws BaseException
	{
	    return getAllFiveTens(null, null, null);
    }


    @Override
    public List<FiveTenDataObject> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllFiveTens(ordering, offset, count, null);
    }

    @Override
    public List<FiveTenDataObject> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllFiveTens(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFiveTenKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllFiveTenKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findFiveTens(filter, ordering, params, values, null, null);
    }

    @Override
	public List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findFiveTens(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createFiveTen(FiveTenDataObject fiveTen) throws BaseException
    {
        return decoratedDAO.createFiveTen( fiveTen);
    }

    @Override
	public Boolean updateFiveTen(FiveTenDataObject fiveTen) throws BaseException
	{
        return decoratedDAO.updateFiveTen(fiveTen);
	}
	
    @Override
    public Boolean deleteFiveTen(FiveTenDataObject fiveTen) throws BaseException
    {
        return decoratedDAO.deleteFiveTen(fiveTen);
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        return decoratedDAO.deleteFiveTen(guid);
	}

    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteFiveTens(filter, params, values);
    }

}
