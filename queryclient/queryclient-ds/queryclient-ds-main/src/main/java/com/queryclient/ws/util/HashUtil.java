package com.queryclient.ws.util;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class HashUtil
{
    private static final Logger log = Logger.getLogger(HashUtil.class.getName());
    // temporary
    // private static final String SALT_SEPARATOR = ",";   // Do we need this? Or, can we use this?
    private static final String SALT_SEPARATOR = "";   // Empty string. This is safer. Note that if change the separator, all hashing methods (using salt) will return different values, which may not be acceptable... 
    // ....

    // Static methods only.
    private HashUtil() {}


    public static String generateMd2Hash(String content)
    {
        return generateMd2Hash(content, null);
    }
    public static String generateMd2Hash(String content, String salt)
    {
        String hashed = null;
        try {
            String source = content;
            if(salt != null && !salt.isEmpty()) {
                source += SALT_SEPARATOR + salt;   // ??? 
            }
            hashed = CommonUtil.MD2(source);
        } catch (NoSuchAlgorithmException e) {
            log.log(Level.WARNING, "Failed to generate md2 hash", e);
        } catch (UnsupportedEncodingException e) {
            log.log(Level.WARNING, "Failed to generate md2 hash", e);
        } catch (Exception e) {
            // Null pointer exception, etc...
            log.log(Level.WARNING, "Failed to generate md2 hash", e);
        }        
        return hashed;
    }

    public static String generateMd5Hash(String content)
    {
        return generateMd5Hash(content, null);
    }
    public static String generateMd5Hash(String content, String salt)
    {
        String hashed = null;
        try {
            String source = content;
            if(salt != null && !salt.isEmpty()) {
                source += SALT_SEPARATOR + salt;   // ??? 
            }
            hashed = CommonUtil.MD5(source);
        } catch (NoSuchAlgorithmException e) {
            log.log(Level.WARNING, "Failed to generate md5 hash", e);
        } catch (UnsupportedEncodingException e) {
            log.log(Level.WARNING, "Failed to generate md5 hash", e);
        } catch (Exception e) {
            // Null pointer exception, etc...
            log.log(Level.WARNING, "Failed to generate md5 hash", e);
        }        
        return hashed;
    }

    
    public static String generateSha1Hash(String content)
    {
        return generateSha1Hash(content, null);
    }
    public static String generateSha1Hash(String content, String salt)
    {
        String hashed = null;
        try {
            String source = content;
            if(salt != null && !salt.isEmpty()) {
                source += SALT_SEPARATOR + salt;   // ??? 
            }
            hashed = CommonUtil.SHA1(source);
        } catch (NoSuchAlgorithmException e) {
            log.log(Level.WARNING, "Failed to generate sha1 hash", e);
        } catch (UnsupportedEncodingException e) {
            log.log(Level.WARNING, "Failed to generate sha1 hash", e);
        } catch (Exception e) {
            // Null pointer exception, etc...
            log.log(Level.WARNING, "Failed to generate sha1 hash", e);
        }        
        return hashed;
    }

    public static String generateSha256Hash(String content)
    {
        return generateSha256Hash(content, null);
    }
    public static String generateSha256Hash(String content, String salt)
    {
        String hashed = null;
        try {
            String source = content;
            if(salt != null && !salt.isEmpty()) {
                source += SALT_SEPARATOR + salt;   // ??? 
            }
            hashed = CommonUtil.SHA256(source);
        } catch (NoSuchAlgorithmException e) {
            log.log(Level.WARNING, "Failed to generate sha256 hash", e);
        } catch (UnsupportedEncodingException e) {
            log.log(Level.WARNING, "Failed to generate sha256 hash", e);
        } catch (Exception e) {
            // Null pointer exception, etc...
            log.log(Level.WARNING, "Failed to generate sha256 hash", e);
        }        
        return hashed;
    }

    public static String generateSha384Hash(String content)
    {
        return generateSha384Hash(content, null);
    }
    public static String generateSha384Hash(String content, String salt)
    {
        String hashed = null;
        try {
            String source = content;
            if(salt != null && !salt.isEmpty()) {
                source += SALT_SEPARATOR + salt;   // ??? 
            }
            hashed = CommonUtil.SHA384(source);
        } catch (NoSuchAlgorithmException e) {
            log.log(Level.WARNING, "Failed to generate sha384 hash", e);
        } catch (UnsupportedEncodingException e) {
            log.log(Level.WARNING, "Failed to generate sha384 hash", e);
        } catch (Exception e) {
            // Null pointer exception, etc...
            log.log(Level.WARNING, "Failed to generate sha384 hash", e);
        }        
        return hashed;
    }

    public static String generateSha512Hash(String content)
    {
        return generateSha512Hash(content, null);
    }
    public static String generateSha512Hash(String content, String salt)
    {
        String hashed = null;
        try {
            String source = content;
            if(salt != null && !salt.isEmpty()) {
                source += SALT_SEPARATOR + salt;   // ??? 
            }
            hashed = CommonUtil.SHA512(source);
        } catch (NoSuchAlgorithmException e) {
            log.log(Level.WARNING, "Failed to generate sha512 hash", e);
        } catch (UnsupportedEncodingException e) {
            log.log(Level.WARNING, "Failed to generate sha512 hash", e);
        } catch (Exception e) {
            // Null pointer exception, etc...
            log.log(Level.WARNING, "Failed to generate sha512 hash", e);
        }        
        return hashed;
    }


}
