package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ServiceInfoBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ServiceInfoBasePermission.class.getName());

    public ServiceInfoBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "ServiceInfo::" + action;
    }


}
