package com.queryclient.ws.resource.impl;

import java.io.StringWriter;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import com.sun.jersey.api.core.HttpContext;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.auth.TwoLeggedOAuthProvider;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;
import com.queryclient.ws.resource.exception.UnauthorizedRsException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.bean.ExternalUserAuthBean;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.ExternalUserAuthListStub;
import com.queryclient.ws.stub.ExternalUserAuthStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.ExternalUserAuthResource;
import com.queryclient.ws.resource.util.GaeAppStructResourceUtil;
import com.queryclient.ws.resource.util.ExternalUserIdStructResourceUtil;


@Path("/externalUserAuths/")
public class ExternalUserAuthResourceImpl extends BaseResourceImpl implements ExternalUserAuthResource
{
    private static final Logger log = Logger.getLogger(ExternalUserAuthResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public ExternalUserAuthResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;
    }

    private Response getExternalUserAuthList(List<ExternalUserAuth> beans, StringCursor forwardCursor) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }

        Response response = null;
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            ExternalUserAuthListStub listStub = new ExternalUserAuthListStub();
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            response = Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } else {
            long lastModifiedTime = 0L;
            List<ExternalUserAuthStub> stubs = new ArrayList<ExternalUserAuthStub>();
            Iterator<ExternalUserAuth> it = beans.iterator();
            while(it.hasNext()) {
                ExternalUserAuth bean = (ExternalUserAuth) it.next();
                stubs.add(ExternalUserAuthStub.convertBeanToStub(bean));
                if(bean.getModifiedTime() != null) {
                    if(bean.getModifiedTime() > lastModifiedTime) {
                        lastModifiedTime = bean.getModifiedTime();
                    }
                } else {
                    if(bean.getCreatedTime() > lastModifiedTime) {
                        lastModifiedTime = bean.getCreatedTime();
                    }
                }
            }
            ExternalUserAuthListStub listStub = new ExternalUserAuthListStub(stubs);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            response = Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).lastModified(new Date(lastModifiedTime)).build();
        }
        return response;
    }

    @Override
    public Response getAllExternalUserAuths(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<ExternalUserAuth> beans = ServiceManager.getExternalUserAuthService().getAllExternalUserAuths(ordering, offset, count, forwardCursor);
            return getExternalUserAuthList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllExternalUserAuthKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<String> keys = ServiceManager.getExternalUserAuthService().getAllExternalUserAuthKeys(ordering, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<ExternalUserAuth> beans = ServiceManager.getExternalUserAuthService().findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return getExternalUserAuthList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<String> keys = ServiceManager.getExternalUserAuthService().findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getExternalUserAuthKeys(List<String> guids) throws BaseResourceException
    {
        // TBD:
        throw new NotImplementedRsException("To be implemented", resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            Long count = ServiceManager.getExternalUserAuthService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getExternalUserAuth(String guid) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            ExternalUserAuth bean = ServiceManager.getExternalUserAuthService().getExternalUserAuth(guid);

            EntityTag eTag = new EntityTag(Integer.toString(bean.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("ExternalUserAuth object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full ExternalUserAuth object.");

            ExternalUserAuthStub stub = ExternalUserAuthStub.convertBeanToStub(bean);
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).cacheControl(CacheControl.valueOf("private")).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).cacheControl(CacheControl.valueOf("private")).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getExternalUserAuth(String guid, String field) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            if(field == null || field.trim().length() == 0) {
                return getExternalUserAuth(guid);
            }
            ExternalUserAuth bean = ServiceManager.getExternalUserAuthService().getExternalUserAuth(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("managerApp")) {
                    String fval = bean.getManagerApp();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("appAcl")) {
                    Long fval = bean.getAppAcl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("gaeApp")) {
                    GaeAppStruct fval = bean.getGaeApp();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("ownerUser")) {
                    String fval = bean.getOwnerUser();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("userAcl")) {
                    Long fval = bean.getUserAcl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("user")) {
                    String fval = bean.getUser();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("providerId")) {
                    String fval = bean.getProviderId();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("externalUserId")) {
                    ExternalUserIdStruct fval = bean.getExternalUserId();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("requestToken")) {
                    String fval = bean.getRequestToken();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("accessToken")) {
                    String fval = bean.getAccessToken();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("accessTokenSecret")) {
                    String fval = bean.getAccessTokenSecret();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("email")) {
                    String fval = bean.getEmail();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("firstName")) {
                    String fval = bean.getFirstName();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("lastName")) {
                    String fval = bean.getLastName();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("fullName")) {
                    String fval = bean.getFullName();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("displayName")) {
                    String fval = bean.getDisplayName();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("description")) {
                    String fval = bean.getDescription();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("gender")) {
                    String fval = bean.getGender();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("dateOfBirth")) {
                    String fval = bean.getDateOfBirth();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("profileImageUrl")) {
                    String fval = bean.getProfileImageUrl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("timeZone")) {
                    String fval = bean.getTimeZone();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("postalCode")) {
                    String fval = bean.getPostalCode();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("location")) {
                    String fval = bean.getLocation();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("country")) {
                    String fval = bean.getCountry();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("language")) {
                    String fval = bean.getLanguage();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("status")) {
                    String fval = bean.getStatus();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("authTime")) {
                    Long fval = bean.getAuthTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("expirationTime")) {
                    Long fval = bean.getExpirationTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("private")).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("private"));
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    @Override
    public Response createExternalUserAuth(ExternalUserAuthStub externalUserAuth) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        if(useAsyncService()) {
            log.log(Level.INFO, "createExternalUserAuth(): Invoking an async call.");
            String guid = externalUserAuth.getGuid();
            if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
                guid = GUID.generate();
                externalUserAuth.setGuid(guid);
            }
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsCreateExternalUserAuth-" + guid;
            String taskName = "WsCreateExternalUserAuth-" + guid + "-" + (new Date()).getTime();
            TaskOptions taskOpt = null;
            try {
                final JAXBContext jAXBContext = JAXBContext.newInstance(ExternalUserAuthStub.class);
                Marshaller marshaller = null;
                StringWriter writer = null;

                // Note that the actual size will be bigger than this in xml or json format...
                int approxPayloadSize = externalUserAuth.toString().length() * 2;  // 2 bytes per char
                if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
                if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                    if(getCache() != null) {
                        getCache().put(taskName, externalUserAuth);
                    
                        ExternalUserAuthStub dummyStub = new ExternalUserAuthStub();
                        dummyStub.setGuid(externalUserAuth.getGuid());
                        marshaller = jAXBContext.createMarshaller();
                        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                        writer = new StringWriter();
                        marshaller.marshal(dummyStub, writer);
                        String dummyPayload = writer.toString();
                        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createExternalUserAuth(): dummyPayload = " + dummyPayload);

                        // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                        taskOpt = com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl(TASK_URIPATH_PREFIX + "externalUserAuths/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                    } else {
                        throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                    }
                } else {
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(externalUserAuth, writer);
                    String payload = writer.toString();
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createExternalUserAuth(): payload = " + payload);

                    // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                    taskOpt = com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl(TASK_URIPATH_PREFIX + "externalUserAuths/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
                }
            } catch (JAXBException e) {
                log.log(Level.WARNING, "Marshaling failed during task enqueing.");
                throw new BaseResourceException("Marshaling failed during task enqueing.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Payload generation failed during task enqueing.");
                throw new BaseResourceException("Payload generation failed during task enqueing.", e);
            }

            queue.add(taskOpt);
            URI createdUri = URI.create(resourceUri + "/" + guid);  // The resource hasn't been created yet, but the client still might expect the location header!
            return Response.status(Status.ACCEPTED).location(createdUri).entity(guid).build();
        }
        // else

        try {
            ExternalUserAuthBean bean = convertExternalUserAuthStubToBean(externalUserAuth);
            String guid = ServiceManager.getExternalUserAuthService().createExternalUserAuth(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ResourceAlreadyPresentException ex) {
            throw new ResourceAlreadyPresentRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateExternalUserAuth(String guid, ExternalUserAuthStub externalUserAuth) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        //guid = GUID.normalize(guid);    // TBD: Validate guid?
        if(externalUserAuth == null || !guid.equals(externalUserAuth.getGuid())) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from externalUserAuth guid = " + externalUserAuth.getGuid());
            throw new RequestForbiddenRsException("Failed to update the externalUserAuth with guid = " + guid);
        }

        if(useAsyncService()) {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateExternalUserAuth(): Invoking an async call: guid = " + guid);
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsUpdateExternalUserAuth-" + guid;
            String taskName = "WsUpdateExternalUserAuth-" + guid + "-" + (new Date()).getTime();
            TaskOptions taskOpt = null;
            try {
                final JAXBContext jAXBContext = JAXBContext.newInstance(ExternalUserAuthStub.class);
                Marshaller marshaller = null;
                StringWriter writer = null;

                // Note that the actual size will be bigger than this in xml or json format...
                int approxPayloadSize = externalUserAuth.toString().length() * 2;  // 2 bytes per char
                if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
                if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                    if(getCache() != null) {
                        getCache().put(taskName, externalUserAuth);

                        ExternalUserAuthStub dummyStub = new ExternalUserAuthStub();
                        dummyStub.setGuid(externalUserAuth.getGuid());
                        marshaller = jAXBContext.createMarshaller();
                        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                        writer = new StringWriter();
                        marshaller.marshal(dummyStub, writer);
                        String dummyPayload = writer.toString();
                        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateExternalUserAuth(): dummyPayload = " + dummyPayload);

                        // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                        taskOpt = com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl(TASK_URIPATH_PREFIX + "externalUserAuths/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                    } else {
                        throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                    }
                } else {
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(externalUserAuth, writer);
                    String payload = writer.toString();
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateExternalUserAuth(): payload = " + payload);

                    // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                    taskOpt = com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl(TASK_URIPATH_PREFIX + "externalUserAuths/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
                }
            } catch (JAXBException e) {
                log.log(Level.WARNING, "Marshaling failed during task enqueing.");
                throw new BaseResourceException("Marshaling failed during task enqueing.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Payload generation failed during task enqueing.");
                throw new BaseResourceException("Payload generation failed during task enqueing.", e);
            }

            queue.add(taskOpt);
            return Response.status(Status.ACCEPTED).build();
        }
        // else

        try {
            ExternalUserAuthBean bean = convertExternalUserAuthStubToBean(externalUserAuth);
            boolean suc = ServiceManager.getExternalUserAuthService().updateExternalUserAuth(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the externalUserAuth with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the externalUserAuth with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateExternalUserAuth(String guid, String managerApp, Long appAcl, String gaeApp, String ownerUser, Long userAcl, String user, String providerId, String externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            /*
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            boolean suc = ServiceManager.getExternalUserAuthService().updateExternalUserAuth(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, providerId, externalUserId, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the externalUserAuth with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the externalUserAuth with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
//        } catch(BadRequestException ex) {
//            throw new BadRequestRsException(ex, resourceUri);
//        } catch(ResourceNotFoundException ex) {
//            throw new ResourceNotFoundRsException(ex, resourceUri);
//        } catch(ResourceGoneException ex) {
//            throw new ResourceGoneRsException(ex, resourceUri);
//        } catch(RequestForbiddenException ex) {
//            throw new RequestForbiddenRsException(ex, resourceUri);
//        } catch(RequestConflictException ex) {
//            throw new RequestConflictRsException(ex, resourceUri);
//        } catch(DataStoreException ex) {
//            throw new DataStoreRsException(ex, resourceUri);
//        } catch(ServiceUnavailableException ex) {
//            throw new ServiceUnavailableRsException(ex, resourceUri);
//        } catch(InternalServerErrorException ex) {
//            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteExternalUserAuth(String guid) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        //guid = GUID.normalize(guid);    // TBD: Validate guid?
        if(useAsyncService()) {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteExternalUserAuth(): Invoking an async call: guid = " + guid);
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsDeleteExternalUserAuth-" + guid;
            String taskName = "WsDeleteExternalUserAuth-" + guid + "-" + (new Date()).getTime();

            // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
            TaskOptions taskOpt = com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl(TASK_URIPATH_PREFIX + "externalUserAuths/" + guid).method(Method.DELETE).taskName(taskName);
            queue.add(taskOpt);
            return Response.status(Status.ACCEPTED).build();
        }
        // else

        try {
            boolean suc = ServiceManager.getExternalUserAuthService().deleteExternalUserAuth(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the externalUserAuth with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the externalUserAuth with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            Long count = ServiceManager.getExternalUserAuthService().deleteExternalUserAuths(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    public static ExternalUserAuthBean convertExternalUserAuthStubToBean(ExternalUserAuth stub)
    {
        ExternalUserAuthBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new ExternalUserAuthBean();
            bean.setGuid(stub.getGuid());
            bean.setManagerApp(stub.getManagerApp());
            bean.setAppAcl(stub.getAppAcl());
            bean.setGaeApp(GaeAppStructResourceUtil.convertGaeAppStructStubToBean(stub.getGaeApp()));
            bean.setOwnerUser(stub.getOwnerUser());
            bean.setUserAcl(stub.getUserAcl());
            bean.setUser(stub.getUser());
            bean.setProviderId(stub.getProviderId());
            bean.setExternalUserId(ExternalUserIdStructResourceUtil.convertExternalUserIdStructStubToBean(stub.getExternalUserId()));
            bean.setRequestToken(stub.getRequestToken());
            bean.setAccessToken(stub.getAccessToken());
            bean.setAccessTokenSecret(stub.getAccessTokenSecret());
            bean.setEmail(stub.getEmail());
            bean.setFirstName(stub.getFirstName());
            bean.setLastName(stub.getLastName());
            bean.setFullName(stub.getFullName());
            bean.setDisplayName(stub.getDisplayName());
            bean.setDescription(stub.getDescription());
            bean.setGender(stub.getGender());
            bean.setDateOfBirth(stub.getDateOfBirth());
            bean.setProfileImageUrl(stub.getProfileImageUrl());
            bean.setTimeZone(stub.getTimeZone());
            bean.setPostalCode(stub.getPostalCode());
            bean.setLocation(stub.getLocation());
            bean.setCountry(stub.getCountry());
            bean.setLanguage(stub.getLanguage());
            bean.setStatus(stub.getStatus());
            bean.setAuthTime(stub.getAuthTime());
            bean.setExpirationTime(stub.getExpirationTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<ExternalUserAuthBean> convertExternalUserAuthListStubToBeanList(ExternalUserAuthListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<ExternalUserAuthStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<ExternalUserAuthBean> beanList = new ArrayList<ExternalUserAuthBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(ExternalUserAuthStub stub : stubList) {
                    ExternalUserAuthBean bean = convertExternalUserAuthStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }

}
