package com.queryclient.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.ws.bean.ConsumerKeySecretPairBean;
import com.queryclient.ws.bean.ReferrerInfoStructBean;
import com.queryclient.ws.bean.DataServiceBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.ConsumerKeySecretPairDataObject;
import com.queryclient.ws.data.ReferrerInfoStructDataObject;
import com.queryclient.ws.data.DataServiceDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.DataServiceService;


// DataServiceMockService is a decorator.
// It can be used as a base class to mock DataServiceService objects.
public abstract class DataServiceMockService implements DataServiceService
{
    private static final Logger log = Logger.getLogger(DataServiceMockService.class.getName());

    // DataServiceMockService uses the decorator design pattern.
    private DataServiceService decoratedService;

    public DataServiceMockService(DataServiceService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected DataServiceService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(DataServiceService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // DataService related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DataService getDataService(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDataService(): guid = " + guid);
        DataService bean = decoratedService.getDataService(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getDataService(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getDataService(guid, field);
        return obj;
    }

    @Override
    public List<DataService> getDataServices(List<String> guids) throws BaseException
    {
        log.fine("getDataServices()");
        List<DataService> dataServices = decoratedService.getDataServices(guids);
        log.finer("END");
        return dataServices;
    }

    @Override
    public List<DataService> getAllDataServices() throws BaseException
    {
        return getAllDataServices(null, null, null);
    }


    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServices(ordering, offset, count, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDataServices(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<DataService> dataServices = decoratedService.getAllDataServices(ordering, offset, count, forwardCursor);
        log.finer("END");
        return dataServices;
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDataServiceKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllDataServiceKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DataServiceMockService.findDataServices(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<DataService> dataServices = decoratedService.findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return dataServices;
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DataServiceMockService.findDataServiceKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DataServiceMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDataService(String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        return decoratedService.createDataService(user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status);
    }

    @Override
    public String createDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createDataService(dataService);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        return decoratedService.updateDataService(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status);
    }
        
    @Override
    public Boolean updateDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateDataService(dataService);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDataService(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDataService(dataService);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDataServices(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteDataServices(filter, params, values);
        return count;
    }

}
