package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class DummyEntityBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DummyEntityBasePermission.class.getName());

    public DummyEntityBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "DummyEntity::" + action;
    }


}
