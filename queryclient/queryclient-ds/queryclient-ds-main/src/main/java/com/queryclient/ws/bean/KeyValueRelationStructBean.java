package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.KeyValueRelationStruct;
import com.queryclient.ws.data.KeyValueRelationStructDataObject;

public class KeyValueRelationStructBean implements KeyValueRelationStruct
{
    private static final Logger log = Logger.getLogger(KeyValueRelationStructBean.class.getName());

    // Embedded data object.
    private KeyValueRelationStructDataObject dobj = null;

    public KeyValueRelationStructBean()
    {
        this(new KeyValueRelationStructDataObject());
    }
    public KeyValueRelationStructBean(KeyValueRelationStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public KeyValueRelationStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
        }
    }

    public String getKey()
    {
        if(getDataObject() != null) {
            return getDataObject().getKey();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setKey(String key)
    {
        if(getDataObject() != null) {
            getDataObject().setKey(key);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
        }
    }

    public String getValue()
    {
        if(getDataObject() != null) {
            return getDataObject().getValue();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setValue(String value)
    {
        if(getDataObject() != null) {
            getDataObject().setValue(value);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
        }
    }

    public String getRelation()
    {
        if(getDataObject() != null) {
            return getDataObject().getRelation();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRelation(String relation)
    {
        if(getDataObject() != null) {
            getDataObject().setRelation(relation);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValueRelationStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getValue() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRelation() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public KeyValueRelationStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
