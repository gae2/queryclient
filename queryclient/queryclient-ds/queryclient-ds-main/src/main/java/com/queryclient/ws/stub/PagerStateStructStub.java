package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.PagerStateStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "pagerStateStruct")
@XmlType(propOrder = {"pagerMode", "primaryOrdering", "secondaryOrdering", "currentOffset", "currentPage", "pageSize", "totalCount", "lowerBoundTotalCount", "previousPageOffset", "nextPageOffset", "lastPageOffset", "lastPageIndex", "firstActionEnabled", "previousActionEnabled", "nextActionEnabled", "lastActionEnabled", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PagerStateStructStub implements PagerStateStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PagerStateStructStub.class.getName());

    private String pagerMode;
    private String primaryOrdering;
    private String secondaryOrdering;
    private Long currentOffset;
    private Long currentPage;
    private Integer pageSize;
    private Long totalCount;
    private Long lowerBoundTotalCount;
    private Long previousPageOffset;
    private Long nextPageOffset;
    private Long lastPageOffset;
    private Long lastPageIndex;
    private Boolean firstActionEnabled;
    private Boolean previousActionEnabled;
    private Boolean nextActionEnabled;
    private Boolean lastActionEnabled;
    private String note;

    public PagerStateStructStub()
    {
        this(null);
    }
    public PagerStateStructStub(PagerStateStruct bean)
    {
        if(bean != null) {
            this.pagerMode = bean.getPagerMode();
            this.primaryOrdering = bean.getPrimaryOrdering();
            this.secondaryOrdering = bean.getSecondaryOrdering();
            this.currentOffset = bean.getCurrentOffset();
            this.currentPage = bean.getCurrentPage();
            this.pageSize = bean.getPageSize();
            this.totalCount = bean.getTotalCount();
            this.lowerBoundTotalCount = bean.getLowerBoundTotalCount();
            this.previousPageOffset = bean.getPreviousPageOffset();
            this.nextPageOffset = bean.getNextPageOffset();
            this.lastPageOffset = bean.getLastPageOffset();
            this.lastPageIndex = bean.getLastPageIndex();
            this.firstActionEnabled = bean.isFirstActionEnabled();
            this.previousActionEnabled = bean.isPreviousActionEnabled();
            this.nextActionEnabled = bean.isNextActionEnabled();
            this.lastActionEnabled = bean.isLastActionEnabled();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getPagerMode()
    {
        return this.pagerMode;
    }
    public void setPagerMode(String pagerMode)
    {
        this.pagerMode = pagerMode;
    }

    @XmlElement
    public String getPrimaryOrdering()
    {
        return this.primaryOrdering;
    }
    public void setPrimaryOrdering(String primaryOrdering)
    {
        this.primaryOrdering = primaryOrdering;
    }

    @XmlElement
    public String getSecondaryOrdering()
    {
        return this.secondaryOrdering;
    }
    public void setSecondaryOrdering(String secondaryOrdering)
    {
        this.secondaryOrdering = secondaryOrdering;
    }

    @XmlElement
    public Long getCurrentOffset()
    {
        return this.currentOffset;
    }
    public void setCurrentOffset(Long currentOffset)
    {
        this.currentOffset = currentOffset;
    }

    @XmlElement
    public Long getCurrentPage()
    {
        return this.currentPage;
    }
    public void setCurrentPage(Long currentPage)
    {
        this.currentPage = currentPage;
    }

    @XmlElement
    public Integer getPageSize()
    {
        return this.pageSize;
    }
    public void setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;
    }

    @XmlElement
    public Long getTotalCount()
    {
        return this.totalCount;
    }
    public void setTotalCount(Long totalCount)
    {
        this.totalCount = totalCount;
    }

    @XmlElement
    public Long getLowerBoundTotalCount()
    {
        return this.lowerBoundTotalCount;
    }
    public void setLowerBoundTotalCount(Long lowerBoundTotalCount)
    {
        this.lowerBoundTotalCount = lowerBoundTotalCount;
    }

    @XmlElement
    public Long getPreviousPageOffset()
    {
        return this.previousPageOffset;
    }
    public void setPreviousPageOffset(Long previousPageOffset)
    {
        this.previousPageOffset = previousPageOffset;
    }

    @XmlElement
    public Long getNextPageOffset()
    {
        return this.nextPageOffset;
    }
    public void setNextPageOffset(Long nextPageOffset)
    {
        this.nextPageOffset = nextPageOffset;
    }

    @XmlElement
    public Long getLastPageOffset()
    {
        return this.lastPageOffset;
    }
    public void setLastPageOffset(Long lastPageOffset)
    {
        this.lastPageOffset = lastPageOffset;
    }

    @XmlElement
    public Long getLastPageIndex()
    {
        return this.lastPageIndex;
    }
    public void setLastPageIndex(Long lastPageIndex)
    {
        this.lastPageIndex = lastPageIndex;
    }

    @XmlElement
    public Boolean isFirstActionEnabled()
    {
        return this.firstActionEnabled;
    }
    public void setFirstActionEnabled(Boolean firstActionEnabled)
    {
        this.firstActionEnabled = firstActionEnabled;
    }

    @XmlElement
    public Boolean isPreviousActionEnabled()
    {
        return this.previousActionEnabled;
    }
    public void setPreviousActionEnabled(Boolean previousActionEnabled)
    {
        this.previousActionEnabled = previousActionEnabled;
    }

    @XmlElement
    public Boolean isNextActionEnabled()
    {
        return this.nextActionEnabled;
    }
    public void setNextActionEnabled(Boolean nextActionEnabled)
    {
        this.nextActionEnabled = nextActionEnabled;
    }

    @XmlElement
    public Boolean isLastActionEnabled()
    {
        return this.lastActionEnabled;
    }
    public void setLastActionEnabled(Boolean lastActionEnabled)
    {
        this.lastActionEnabled = lastActionEnabled;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPagerMode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPrimaryOrdering() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecondaryOrdering() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCurrentOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCurrentPage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPageSize() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTotalCount() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLowerBoundTotalCount() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPreviousPageOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNextPageOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastPageOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastPageIndex() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isFirstActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isPreviousActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isNextActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isLastActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("pagerMode", this.pagerMode);
        dataMap.put("primaryOrdering", this.primaryOrdering);
        dataMap.put("secondaryOrdering", this.secondaryOrdering);
        dataMap.put("currentOffset", this.currentOffset);
        dataMap.put("currentPage", this.currentPage);
        dataMap.put("pageSize", this.pageSize);
        dataMap.put("totalCount", this.totalCount);
        dataMap.put("lowerBoundTotalCount", this.lowerBoundTotalCount);
        dataMap.put("previousPageOffset", this.previousPageOffset);
        dataMap.put("nextPageOffset", this.nextPageOffset);
        dataMap.put("lastPageOffset", this.lastPageOffset);
        dataMap.put("lastPageIndex", this.lastPageIndex);
        dataMap.put("firstActionEnabled", this.firstActionEnabled);
        dataMap.put("previousActionEnabled", this.previousActionEnabled);
        dataMap.put("nextActionEnabled", this.nextActionEnabled);
        dataMap.put("lastActionEnabled", this.lastActionEnabled);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = pagerMode == null ? 0 : pagerMode.hashCode();
        _hash = 31 * _hash + delta;
        delta = primaryOrdering == null ? 0 : primaryOrdering.hashCode();
        _hash = 31 * _hash + delta;
        delta = secondaryOrdering == null ? 0 : secondaryOrdering.hashCode();
        _hash = 31 * _hash + delta;
        delta = currentOffset == null ? 0 : currentOffset.hashCode();
        _hash = 31 * _hash + delta;
        delta = currentPage == null ? 0 : currentPage.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageSize == null ? 0 : pageSize.hashCode();
        _hash = 31 * _hash + delta;
        delta = totalCount == null ? 0 : totalCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = lowerBoundTotalCount == null ? 0 : lowerBoundTotalCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = previousPageOffset == null ? 0 : previousPageOffset.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextPageOffset == null ? 0 : nextPageOffset.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastPageOffset == null ? 0 : lastPageOffset.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastPageIndex == null ? 0 : lastPageIndex.hashCode();
        _hash = 31 * _hash + delta;
        delta = firstActionEnabled == null ? 0 : firstActionEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = previousActionEnabled == null ? 0 : previousActionEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextActionEnabled == null ? 0 : nextActionEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastActionEnabled == null ? 0 : lastActionEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static PagerStateStructStub convertBeanToStub(PagerStateStruct bean)
    {
        PagerStateStructStub stub = null;
        if(bean instanceof PagerStateStructStub) {
            stub = (PagerStateStructStub) bean;
        } else {
            if(bean != null) {
                stub = new PagerStateStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static PagerStateStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of PagerStateStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write PagerStateStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write PagerStateStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write PagerStateStructStub object as a string.", e);
        }
        
        return null;
    }
    public static PagerStateStructStub fromJsonString(String jsonStr)
    {
        try {
            PagerStateStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, PagerStateStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into PagerStateStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into PagerStateStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into PagerStateStructStub object.", e);
        }
        
        return null;
    }

}
