package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.UserAuthStateDAO;
import com.queryclient.ws.data.UserAuthStateDataObject;


// MockUserAuthStateDAO is a decorator.
// It can be used as a base class to mock UserAuthStateDAO objects.
public abstract class MockUserAuthStateDAO implements UserAuthStateDAO
{
    private static final Logger log = Logger.getLogger(MockUserAuthStateDAO.class.getName()); 

    // MockUserAuthStateDAO uses the decorator design pattern.
    private UserAuthStateDAO decoratedDAO;

    public MockUserAuthStateDAO(UserAuthStateDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected UserAuthStateDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(UserAuthStateDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public UserAuthStateDataObject getUserAuthState(String guid) throws BaseException
    {
        return decoratedDAO.getUserAuthState(guid);
	}

    @Override
    public List<UserAuthStateDataObject> getUserAuthStates(List<String> guids) throws BaseException
    {
        return decoratedDAO.getUserAuthStates(guids);
    }

    @Override
    public List<UserAuthStateDataObject> getAllUserAuthStates() throws BaseException
	{
	    return getAllUserAuthStates(null, null, null);
    }


    @Override
    public List<UserAuthStateDataObject> getAllUserAuthStates(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllUserAuthStates(ordering, offset, count, null);
    }

    @Override
    public List<UserAuthStateDataObject> getAllUserAuthStates(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllUserAuthStates(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserAuthStateKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllUserAuthStateKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<UserAuthStateDataObject> findUserAuthStates(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findUserAuthStates(filter, ordering, params, values, null, null);
    }

    @Override
	public List<UserAuthStateDataObject> findUserAuthStates(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findUserAuthStates(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<UserAuthStateDataObject> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<UserAuthStateDataObject> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUserAuthState(UserAuthStateDataObject userAuthState) throws BaseException
    {
        return decoratedDAO.createUserAuthState( userAuthState);
    }

    @Override
	public Boolean updateUserAuthState(UserAuthStateDataObject userAuthState) throws BaseException
	{
        return decoratedDAO.updateUserAuthState(userAuthState);
	}
	
    @Override
    public Boolean deleteUserAuthState(UserAuthStateDataObject userAuthState) throws BaseException
    {
        return decoratedDAO.deleteUserAuthState(userAuthState);
    }

    @Override
    public Boolean deleteUserAuthState(String guid) throws BaseException
    {
        return decoratedDAO.deleteUserAuthState(guid);
	}

    @Override
    public Long deleteUserAuthStates(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteUserAuthStates(filter, params, values);
    }

}
