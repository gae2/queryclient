package com.queryclient.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.bean.ServiceEndpointBean;
import com.queryclient.ws.stub.ServiceEndpointListStub;
import com.queryclient.ws.stub.ServiceEndpointStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.ServiceEndpointResource;
import com.queryclient.ws.resource.util.ConsumerKeySecretPairResourceUtil;

// MockServiceEndpointResource is a decorator.
// It can be used as a base class to mock ServiceEndpointResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/serviceEndpoints/")
public abstract class MockServiceEndpointResource implements ServiceEndpointResource
{
    private static final Logger log = Logger.getLogger(MockServiceEndpointResource.class.getName());

    // MockServiceEndpointResource uses the decorator design pattern.
    private ServiceEndpointResource decoratedResource;

    public MockServiceEndpointResource(ServiceEndpointResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected ServiceEndpointResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(ServiceEndpointResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllServiceEndpoints(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getServiceEndpointKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getServiceEndpointKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getServiceEndpoint(String guid) throws BaseResourceException
    {
        return decoratedResource.getServiceEndpoint(guid);
    }

    @Override
    public Response getServiceEndpoint(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getServiceEndpoint(guid, field);
    }

    @Override
    public Response createServiceEndpoint(ServiceEndpointStub serviceEndpoint) throws BaseResourceException
    {
        return decoratedResource.createServiceEndpoint(serviceEndpoint);
    }

    @Override
    public Response updateServiceEndpoint(String guid, ServiceEndpointStub serviceEndpoint) throws BaseResourceException
    {
        return decoratedResource.updateServiceEndpoint(guid, serviceEndpoint);
    }

    @Override
    public Response updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, String authCredential, String status) throws BaseResourceException
    {
        return decoratedResource.updateServiceEndpoint(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredential, status);
    }

    @Override
    public Response deleteServiceEndpoint(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteServiceEndpoint(guid);
    }

    @Override
    public Response deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteServiceEndpoints(filter, params, values);
    }


}
