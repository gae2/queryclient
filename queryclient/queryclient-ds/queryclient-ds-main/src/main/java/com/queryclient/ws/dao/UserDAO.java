package com.queryclient.ws.dao;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.data.UserDataObject;


// TBD: Add offset/count to getAllUsers() and findUsers(), etc.
public interface UserDAO
{
    UserDataObject getUser(String guid) throws BaseException;
    List<UserDataObject> getUsers(List<String> guids) throws BaseException;
    List<UserDataObject> getAllUsers() throws BaseException;
    /* @Deprecated */ List<UserDataObject> getAllUsers(String ordering, Long offset, Integer count) throws BaseException;
    List<UserDataObject> getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUser(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserDataObject?)
    String createUser(UserDataObject user) throws BaseException;          // Returns Guid.  (Return UserDataObject?)
    //Boolean updateUser(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUser(UserDataObject user) throws BaseException;
    Boolean deleteUser(String guid) throws BaseException;
    Boolean deleteUser(UserDataObject user) throws BaseException;
    Long deleteUsers(String filter, String params, List<String> values) throws BaseException;
}
