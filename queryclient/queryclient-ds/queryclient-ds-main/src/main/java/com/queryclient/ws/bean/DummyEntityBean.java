package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.data.DummyEntityDataObject;

public class DummyEntityBean extends BeanBase implements DummyEntity
{
    private static final Logger log = Logger.getLogger(DummyEntityBean.class.getName());

    // Embedded data object.
    private DummyEntityDataObject dobj = null;

    public DummyEntityBean()
    {
        this(new DummyEntityDataObject());
    }
    public DummyEntityBean(String guid)
    {
        this(new DummyEntityDataObject(guid));
    }
    public DummyEntityBean(DummyEntityDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public DummyEntityDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
        }
    }

    public String getName()
    {
        if(getDataObject() != null) {
            return getDataObject().getName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
            return null;   // ???
        }
    }
    public void setName(String name)
    {
        if(getDataObject() != null) {
            getDataObject().setName(name);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
        }
    }

    public String getContent()
    {
        if(getDataObject() != null) {
            return getDataObject().getContent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
            return null;   // ???
        }
    }
    public void setContent(String content)
    {
        if(getDataObject() != null) {
            getDataObject().setContent(content);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
        }
    }

    public Integer getMaxLength()
    {
        if(getDataObject() != null) {
            return getDataObject().getMaxLength();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
            return null;   // ???
        }
    }
    public void setMaxLength(Integer maxLength)
    {
        if(getDataObject() != null) {
            getDataObject().setMaxLength(maxLength);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
        }
    }

    public Boolean isExpired()
    {
        if(getDataObject() != null) {
            return getDataObject().isExpired();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpired(Boolean expired)
    {
        if(getDataObject() != null) {
            getDataObject().setExpired(expired);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DummyEntityDataObject is null!");
        }
    }


    // TBD
    public DummyEntityDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
