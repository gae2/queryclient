package com.queryclient.ws;



public interface UserPassword 
{
    String  getGuid();
    String  getManagerApp();
    Long  getAppAcl();
    GaeAppStruct  getGaeApp();
    String  getOwnerUser();
    Long  getUserAcl();
    String  getAdmin();
    String  getUser();
    String  getUsername();
    String  getEmail();
    String  getOpenId();
    String  getPlainPassword();
    String  getHashedPassword();
    String  getSalt();
    String  getHashMethod();
    Boolean  isResetRequired();
    String  getChallengeQuestion();
    String  getChallengeAnswer();
    String  getStatus();
    Long  getLastResetTime();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
