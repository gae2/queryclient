package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.UserPasswordDAO;
import com.queryclient.ws.data.UserPasswordDataObject;


// MockUserPasswordDAO is a decorator.
// It can be used as a base class to mock UserPasswordDAO objects.
public abstract class MockUserPasswordDAO implements UserPasswordDAO
{
    private static final Logger log = Logger.getLogger(MockUserPasswordDAO.class.getName()); 

    // MockUserPasswordDAO uses the decorator design pattern.
    private UserPasswordDAO decoratedDAO;

    public MockUserPasswordDAO(UserPasswordDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected UserPasswordDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(UserPasswordDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public UserPasswordDataObject getUserPassword(String guid) throws BaseException
    {
        return decoratedDAO.getUserPassword(guid);
	}

    @Override
    public List<UserPasswordDataObject> getUserPasswords(List<String> guids) throws BaseException
    {
        return decoratedDAO.getUserPasswords(guids);
    }

    @Override
    public List<UserPasswordDataObject> getAllUserPasswords() throws BaseException
	{
	    return getAllUserPasswords(null, null, null);
    }


    @Override
    public List<UserPasswordDataObject> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllUserPasswords(ordering, offset, count, null);
    }

    @Override
    public List<UserPasswordDataObject> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllUserPasswords(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<UserPasswordDataObject> findUserPasswords(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findUserPasswords(filter, ordering, params, values, null, null);
    }

    @Override
	public List<UserPasswordDataObject> findUserPasswords(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findUserPasswords(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<UserPasswordDataObject> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<UserPasswordDataObject> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUserPassword(UserPasswordDataObject userPassword) throws BaseException
    {
        return decoratedDAO.createUserPassword( userPassword);
    }

    @Override
	public Boolean updateUserPassword(UserPasswordDataObject userPassword) throws BaseException
	{
        return decoratedDAO.updateUserPassword(userPassword);
	}
	
    @Override
    public Boolean deleteUserPassword(UserPasswordDataObject userPassword) throws BaseException
    {
        return decoratedDAO.deleteUserPassword(userPassword);
    }

    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        return decoratedDAO.deleteUserPassword(guid);
	}

    @Override
    public Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteUserPasswords(filter, params, values);
    }

}
