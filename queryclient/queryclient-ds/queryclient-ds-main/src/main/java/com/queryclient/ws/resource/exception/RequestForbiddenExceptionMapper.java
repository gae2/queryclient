package com.queryclient.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.queryclient.ws.stub.ErrorStub;

@Provider
public class RequestForbiddenExceptionMapper implements ExceptionMapper<RequestForbiddenRsException>
{
    public Response toResponse(RequestForbiddenRsException ex) {
        return Response.status(Status.FORBIDDEN)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
