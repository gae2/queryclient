package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.data.GaeUserStructDataObject;

public class GaeUserStructBean implements GaeUserStruct
{
    private static final Logger log = Logger.getLogger(GaeUserStructBean.class.getName());

    // Embedded data object.
    private GaeUserStructDataObject dobj = null;

    public GaeUserStructBean()
    {
        this(new GaeUserStructDataObject());
    }
    public GaeUserStructBean(GaeUserStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public GaeUserStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getAuthDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getAuthDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthDomain(String authDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthDomain(authDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
        }
    }

    public String getFederatedIdentity()
    {
        if(getDataObject() != null) {
            return getDataObject().getFederatedIdentity();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setFederatedIdentity(String federatedIdentity)
    {
        if(getDataObject() != null) {
            getDataObject().setFederatedIdentity(federatedIdentity);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
        }
    }

    public String getNickname()
    {
        if(getDataObject() != null) {
            return getDataObject().getNickname();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNickname(String nickname)
    {
        if(getDataObject() != null) {
            getDataObject().setNickname(nickname);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
        }
    }

    public String getUserId()
    {
        if(getDataObject() != null) {
            return getDataObject().getUserId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUserId(String userId)
    {
        if(getDataObject() != null) {
            getDataObject().setUserId(userId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
        }
    }

    public String getEmail()
    {
        if(getDataObject() != null) {
            return getDataObject().getEmail();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setEmail(String email)
    {
        if(getDataObject() != null) {
            getDataObject().setEmail(email);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GaeUserStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getAuthDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFederatedIdentity() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNickname() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUserId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmail() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public GaeUserStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
