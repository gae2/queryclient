package com.queryclient.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.DataServiceDAO;
import com.queryclient.ws.data.DataServiceDataObject;


public class DefaultDataServiceDAO extends DefaultDAOBase implements DataServiceDAO
{
    private static final Logger log = Logger.getLogger(DefaultDataServiceDAO.class.getName()); 

    // Returns the dataService for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public DataServiceDataObject getDataService(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        DataServiceDataObject dataService = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = DataServiceDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = DataServiceDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                dataService = pm.getObjectById(DataServiceDataObject.class, key);
                dataService.getAuthCredential();  // "Touch". Otherwise this field will not be fetched by default.
                dataService.getReferrerInfo();  // "Touch". Otherwise this field will not be fetched by default.
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve dataService for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return dataService;
	}

    @Override
    public List<DataServiceDataObject> getDataServices(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<DataServiceDataObject> dataServices = null;
        if(guids != null && !guids.isEmpty()) {
            dataServices = new ArrayList<DataServiceDataObject>();
            for(String guid : guids) {
                DataServiceDataObject obj = getDataService(guid); 
                dataServices.add(obj);
            }
	    }

        log.finer("END");
        return dataServices;
    }

    @Override
    public List<DataServiceDataObject> getAllDataServices() throws BaseException
	{
	    return getAllDataServices(null, null, null);
    }


    @Override
    public List<DataServiceDataObject> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllDataServices(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<DataServiceDataObject> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<DataServiceDataObject> dataServices = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(DataServiceDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            dataServices = (List<DataServiceDataObject>) q.execute();
            if(dataServices != null) {
                dataServices.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(dataServices);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            /*
            // ???
            Collection<DataServiceDataObject> rs_dataServices = (Collection<DataServiceDataObject>) q.execute();
            if(rs_dataServices == null) {
                log.log(Level.WARNING, "Failed to retrieve all dataServices.");
                dataServices = new ArrayList<DataServiceDataObject>();  // ???           
            } else {
                dataServices = new ArrayList<DataServiceDataObject>(pm.detachCopyAll(rs_dataServices));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all dataServices.", ex);
            //dataServices = new ArrayList<DataServiceDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all dataServices.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return dataServices;
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + DataServiceDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all DataService keys.", ex);
            throw new DataStoreException("Failed to retrieve all DataService keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findDataServices(filter, ordering, params, values, null, null);
    }

    @Override
	public List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findDataServices(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultDataServiceDAO.findDataServices(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findDataServices() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<DataServiceDataObject> dataServices = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(DataServiceDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                dataServices = (List<DataServiceDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                dataServices = (List<DataServiceDataObject>) q.execute();
            }
            if(dataServices != null) {
                dataServices.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(dataServices);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(DataServiceDataObject dobj : dataServices) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find dataServices because index is missing.", ex);
            //dataServices = new ArrayList<DataServiceDataObject>();  // ???
            throw new DataStoreException("Failed to find dataServices because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find dataServices meeting the criterion.", ex);
            //dataServices = new ArrayList<DataServiceDataObject>();  // ???
            throw new DataStoreException("Failed to find dataServices meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return dataServices;
	}

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultDataServiceDAO.findDataServiceKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + DataServiceDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find DataService keys because index is missing.", ex);
            throw new DataStoreException("Failed to find DataService keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find DataService keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find DataService keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultDataServiceDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(DataServiceDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get dataService count because index is missing.", ex);
            throw new DataStoreException("Failed to get dataService count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get dataService count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get dataService count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the dataService in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeDataService(DataServiceDataObject dataService) throws BaseException
    {
        log.fine("storeDataService() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = dataService.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                dataService.setCreatedTime(createdTime);
            }
            Long modifiedTime = dataService.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                dataService.setModifiedTime(createdTime);
            }
            pm.makePersistent(dataService); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = dataService.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store dataService because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store dataService because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store dataService.", ex);
            throw new DataStoreException("Failed to store dataService.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeDataService(): guid = " + guid);
        return guid;
    }

    @Override
    public String createDataService(DataServiceDataObject dataService) throws BaseException
    {
        // The createdTime field will be automatically set in storeDataService().
        //Long createdTime = dataService.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    dataService.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = dataService.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    dataService.setModifiedTime(createdTime);
        //}
        return storeDataService(dataService);
    }

    @Override
	public Boolean updateDataService(DataServiceDataObject dataService) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeDataService()
	    // (in which case modifiedTime might be updated again).
	    dataService.setModifiedTime(System.currentTimeMillis());
	    String guid = storeDataService(dataService);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteDataService(DataServiceDataObject dataService) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(dataService);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete dataService because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete dataService because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete dataService.", ex);
            throw new DataStoreException("Failed to delete dataService.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = DataServiceDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = DataServiceDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                DataServiceDataObject dataService = pm.getObjectById(DataServiceDataObject.class, key);
                pm.deletePersistent(dataService);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete dataService because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete dataService because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete dataService for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete dataService for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteDataServices(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultDataServiceDAO.deleteDataServices(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(DataServiceDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletedataServices because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete dataServices because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete dataServices because index is missing", ex);
            throw new DataStoreException("Failed to delete dataServices because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete dataServices", ex);
            throw new DataStoreException("Failed to delete dataServices", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
