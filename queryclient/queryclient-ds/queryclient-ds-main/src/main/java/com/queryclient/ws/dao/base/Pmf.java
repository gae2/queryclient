package com.queryclient.ws.dao.base;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

public final class Pmf
{
    private Pmf() {}

    // Initialization-on-demand holder.
    private static class PersistenceManagerFactoryHolder
    {
        private static final PersistenceManagerFactory INSTANCE = 
            JDOHelper.getPersistenceManagerFactory("transactions-optional");
        // Etc.
    }

    public static PersistenceManagerFactory get() 
    {
        return PersistenceManagerFactoryHolder.INSTANCE;
    }

    // TBD: Return different types of PMFs.
    public static PersistenceManagerFactory getDefaultPMF() 
    {
        return PersistenceManagerFactoryHolder.INSTANCE;
    }

}
