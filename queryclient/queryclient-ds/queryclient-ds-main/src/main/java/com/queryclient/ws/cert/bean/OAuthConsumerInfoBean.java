package com.queryclient.ws.cert.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.cert.OAuthConsumerInfo;
import com.queryclient.ws.cert.data.OAuthConsumerInfoDataObject;


public class OAuthConsumerInfoBean implements OAuthConsumerInfo
{
    private static final Logger log = Logger.getLogger(OAuthConsumerInfoBean.class.getName());

    // Embedded data object.
    private OAuthConsumerInfoDataObject dobj = null;

    public OAuthConsumerInfoBean()
    {
        this(new OAuthConsumerInfoDataObject());
    }
    public OAuthConsumerInfoBean(String guid)
    {
        this(new OAuthConsumerInfoDataObject(guid));
    }
    public OAuthConsumerInfoBean(OAuthConsumerInfoDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public OAuthConsumerInfoDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
        }
    }

    public Long getCreatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getCreatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
            return null;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getDataObject() != null) {
            getDataObject().setCreatedTime(createdTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
        }
    }

    public Long getModifiedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getModifiedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
            return null;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setModifiedTime(modifiedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
        }
    }

    public String getServiceName()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceName(String serviceName)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceName(serviceName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
        }
    }

    public String getAppId()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppId(String appId)
    {
        if(getDataObject() != null) {
            getDataObject().setAppId(appId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
        }
    }

    public String getAppUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppUrl(String appUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setAppUrl(appUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
        }
    }

    public String getConsumerKey()
    {
        if(getDataObject() != null) {
            return getDataObject().getConsumerKey();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setConsumerKey(String consumerKey)
    {
        if(getDataObject() != null) {
            getDataObject().setConsumerKey(consumerKey);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
        }
    }

    public String getConsumerSecret()
    {
        if(getDataObject() != null) {
            return getDataObject().getConsumerSecret();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setConsumerSecret(String consumerSecret)
    {
        if(getDataObject() != null) {
            getDataObject().setConsumerSecret(consumerSecret);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
        }
    }

    public Long getExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationTime(expirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OAuthConsumerInfoDataObject is null!");
        }
    }

    // TBD
    public OAuthConsumerInfoDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
