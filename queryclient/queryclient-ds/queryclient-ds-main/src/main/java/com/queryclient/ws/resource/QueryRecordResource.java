package com.queryclient.ws.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.stub.QueryRecordStub;
import com.queryclient.ws.stub.QueryRecordListStub;


// TBD: Partial update/overwrite?
// TBD: Field-based filtering in getQueryRecord(guid). (e.g., ?field1=x&field2=y)
// Note: Jersey (possibly, new version 1.9.1) seems to have a weird bug and
//       it throws exception with the format @Path("{guid : [0-9a-fA-F\\-]+}") (and, other variations)
//       (which somehow translates into 405 error).
// --> Workaround. Use this format: @Path("{guid: [a-zA-Z0-9\\-_]+}") across all guid path params...
public interface QueryRecordResource
{
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllQueryRecords(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("allkeys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllQueryRecordKeys(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("keys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findQueryRecordKeys(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("subset")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getQueryRecordKeys(@QueryParam("guids") List<String> guids) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

    @GET
    // @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Support both guid and key ???  (Note: We have to be consistent!)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getQueryRecord(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    @Path("{guid: [a-f0-9\\-]+}/{field: [a-zA-Z_][a-zA-Z0-9_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getQueryRecord(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findQueryRecords(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createQueryRecord(QueryRecordStub queryRecord) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateQueryRecord(@PathParam("guid") String guid, QueryRecordStub queryRecord) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateQueryRecord(@PathParam("guid") String guid, @QueryParam("querySession") String querySession, @QueryParam("queryId") Integer queryId, @QueryParam("dataService") String dataService, @QueryParam("serviceUrl") String serviceUrl, @QueryParam("delayed") Boolean delayed, @QueryParam("query") String query, @QueryParam("inputFormat") String inputFormat, @QueryParam("inputFile") String inputFile, @QueryParam("inputContent") String inputContent, @QueryParam("targetOutputFormat") String targetOutputFormat, @QueryParam("outputFormat") String outputFormat, @QueryParam("outputFile") String outputFile, @QueryParam("outputContent") String outputContent, @QueryParam("responseCode") Integer responseCode, @QueryParam("result") String result, @QueryParam("referrerInfo") String referrerInfo, @QueryParam("status") String status, @QueryParam("extra") String extra, @QueryParam("note") String note, @QueryParam("scheduledTime") Long scheduledTime, @QueryParam("processedTime") Long processedTime) throws BaseResourceException;

    @DELETE
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    Response deleteQueryRecord(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deleteQueryRecords(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

//    @POST
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response createQueryRecords(QueryRecordListStub queryRecords) throws BaseResourceException;

//    @PUT
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response updateeQueryRecords(QueryRecordListStub queryRecords) throws BaseResourceException;

}
