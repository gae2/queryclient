package com.queryclient.ws;



public interface ExternalUserAuth 
{
    String  getGuid();
    String  getManagerApp();
    Long  getAppAcl();
    GaeAppStruct  getGaeApp();
    String  getOwnerUser();
    Long  getUserAcl();
    String  getUser();
    String  getProviderId();
    ExternalUserIdStruct  getExternalUserId();
    String  getRequestToken();
    String  getAccessToken();
    String  getAccessTokenSecret();
    String  getEmail();
    String  getFirstName();
    String  getLastName();
    String  getFullName();
    String  getDisplayName();
    String  getDescription();
    String  getGender();
    String  getDateOfBirth();
    String  getProfileImageUrl();
    String  getTimeZone();
    String  getPostalCode();
    String  getLocation();
    String  getCountry();
    String  getLanguage();
    String  getStatus();
    Long  getAuthTime();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
