package com.queryclient.ws.dao;

// Abstract factory.
public abstract class DAOFactory
{
    public abstract ApiConsumerDAO getApiConsumerDAO();
    public abstract UserDAO getUserDAO();
    public abstract UserPasswordDAO getUserPasswordDAO();
    public abstract ExternalUserAuthDAO getExternalUserAuthDAO();
    public abstract UserAuthStateDAO getUserAuthStateDAO();
    public abstract DataServiceDAO getDataServiceDAO();
    public abstract ServiceEndpointDAO getServiceEndpointDAO();
    public abstract QuerySessionDAO getQuerySessionDAO();
    public abstract QueryRecordDAO getQueryRecordDAO();
    public abstract DummyEntityDAO getDummyEntityDAO();
    public abstract ServiceInfoDAO getServiceInfoDAO();
    public abstract FiveTenDAO getFiveTenDAO();
}
