package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.PagerStateStruct;
import com.queryclient.ws.data.PagerStateStructDataObject;

public class PagerStateStructBean implements PagerStateStruct
{
    private static final Logger log = Logger.getLogger(PagerStateStructBean.class.getName());

    // Embedded data object.
    private PagerStateStructDataObject dobj = null;

    public PagerStateStructBean()
    {
        this(new PagerStateStructDataObject());
    }
    public PagerStateStructBean(PagerStateStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public PagerStateStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getPagerMode()
    {
        if(getDataObject() != null) {
            return getDataObject().getPagerMode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPagerMode(String pagerMode)
    {
        if(getDataObject() != null) {
            getDataObject().setPagerMode(pagerMode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public String getPrimaryOrdering()
    {
        if(getDataObject() != null) {
            return getDataObject().getPrimaryOrdering();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPrimaryOrdering(String primaryOrdering)
    {
        if(getDataObject() != null) {
            getDataObject().setPrimaryOrdering(primaryOrdering);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public String getSecondaryOrdering()
    {
        if(getDataObject() != null) {
            return getDataObject().getSecondaryOrdering();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSecondaryOrdering(String secondaryOrdering)
    {
        if(getDataObject() != null) {
            getDataObject().setSecondaryOrdering(secondaryOrdering);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Long getCurrentOffset()
    {
        if(getDataObject() != null) {
            return getDataObject().getCurrentOffset();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setCurrentOffset(Long currentOffset)
    {
        if(getDataObject() != null) {
            getDataObject().setCurrentOffset(currentOffset);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Long getCurrentPage()
    {
        if(getDataObject() != null) {
            return getDataObject().getCurrentPage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setCurrentPage(Long currentPage)
    {
        if(getDataObject() != null) {
            getDataObject().setCurrentPage(currentPage);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Integer getPageSize()
    {
        if(getDataObject() != null) {
            return getDataObject().getPageSize();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPageSize(Integer pageSize)
    {
        if(getDataObject() != null) {
            getDataObject().setPageSize(pageSize);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Long getTotalCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getTotalCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setTotalCount(Long totalCount)
    {
        if(getDataObject() != null) {
            getDataObject().setTotalCount(totalCount);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Long getLowerBoundTotalCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getLowerBoundTotalCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLowerBoundTotalCount(Long lowerBoundTotalCount)
    {
        if(getDataObject() != null) {
            getDataObject().setLowerBoundTotalCount(lowerBoundTotalCount);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Long getPreviousPageOffset()
    {
        if(getDataObject() != null) {
            return getDataObject().getPreviousPageOffset();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPreviousPageOffset(Long previousPageOffset)
    {
        if(getDataObject() != null) {
            getDataObject().setPreviousPageOffset(previousPageOffset);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Long getNextPageOffset()
    {
        if(getDataObject() != null) {
            return getDataObject().getNextPageOffset();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNextPageOffset(Long nextPageOffset)
    {
        if(getDataObject() != null) {
            getDataObject().setNextPageOffset(nextPageOffset);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Long getLastPageOffset()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastPageOffset();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastPageOffset(Long lastPageOffset)
    {
        if(getDataObject() != null) {
            getDataObject().setLastPageOffset(lastPageOffset);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Long getLastPageIndex()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastPageIndex();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastPageIndex(Long lastPageIndex)
    {
        if(getDataObject() != null) {
            getDataObject().setLastPageIndex(lastPageIndex);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Boolean isFirstActionEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isFirstActionEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setFirstActionEnabled(Boolean firstActionEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setFirstActionEnabled(firstActionEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Boolean isPreviousActionEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isPreviousActionEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPreviousActionEnabled(Boolean previousActionEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setPreviousActionEnabled(previousActionEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Boolean isNextActionEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isNextActionEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNextActionEnabled(Boolean nextActionEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setNextActionEnabled(nextActionEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public Boolean isLastActionEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isLastActionEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastActionEnabled(Boolean lastActionEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setLastActionEnabled(lastActionEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PagerStateStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPagerMode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPrimaryOrdering() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecondaryOrdering() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCurrentOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCurrentPage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPageSize() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTotalCount() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLowerBoundTotalCount() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPreviousPageOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNextPageOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastPageOffset() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastPageIndex() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isFirstActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isPreviousActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isNextActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isLastActionEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public PagerStateStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
