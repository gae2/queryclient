package com.queryclient.ws;



public interface ServiceInfo 
{
    String  getGuid();
    String  getTitle();
    String  getContent();
    String  getType();
    String  getStatus();
    Long  getScheduledTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
