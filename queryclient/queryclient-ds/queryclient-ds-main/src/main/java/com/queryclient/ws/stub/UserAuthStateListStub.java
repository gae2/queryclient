package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.UserAuthState;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "userAuthStates")
@XmlType(propOrder = {"userAuthState", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAuthStateListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserAuthStateListStub.class.getName());

    private List<UserAuthStateStub> userAuthStates = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public UserAuthStateListStub()
    {
        this(new ArrayList<UserAuthStateStub>());
    }
    public UserAuthStateListStub(List<UserAuthStateStub> userAuthStates)
    {
        this(userAuthStates, null);
    }
    public UserAuthStateListStub(List<UserAuthStateStub> userAuthStates, String forwardCursor)
    {
        this.userAuthStates = userAuthStates;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(userAuthStates == null) {
            return true;
        } else {
            return userAuthStates.isEmpty();
        }
    }
    public int getSize()
    {
        if(userAuthStates == null) {
            return 0;
        } else {
            return userAuthStates.size();
        }
    }


    @XmlElement(name = "userAuthState")
    public List<UserAuthStateStub> getUserAuthState()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserAuthStateStub> getList()
    {
        return userAuthStates;
    }
    public void setList(List<UserAuthStateStub> userAuthStates)
    {
        this.userAuthStates = userAuthStates;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<UserAuthStateStub> it = this.userAuthStates.iterator();
        while(it.hasNext()) {
            UserAuthStateStub userAuthState = it.next();
            sb.append(userAuthState.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserAuthStateListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserAuthStateListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserAuthStateListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserAuthStateListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserAuthStateListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserAuthStateListStub fromJsonString(String jsonStr)
    {
        try {
            UserAuthStateListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserAuthStateListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserAuthStateListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserAuthStateListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserAuthStateListStub object.", e);
        }
        
        return null;
    }

}
