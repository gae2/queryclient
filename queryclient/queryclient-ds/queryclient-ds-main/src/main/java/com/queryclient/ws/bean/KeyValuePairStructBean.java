package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.KeyValuePairStruct;
import com.queryclient.ws.data.KeyValuePairStructDataObject;

public class KeyValuePairStructBean implements KeyValuePairStruct
{
    private static final Logger log = Logger.getLogger(KeyValuePairStructBean.class.getName());

    // Embedded data object.
    private KeyValuePairStructDataObject dobj = null;

    public KeyValuePairStructBean()
    {
        this(new KeyValuePairStructDataObject());
    }
    public KeyValuePairStructBean(KeyValuePairStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public KeyValuePairStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValuePairStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValuePairStructDataObject is null!");
        }
    }

    public String getKey()
    {
        if(getDataObject() != null) {
            return getDataObject().getKey();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValuePairStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setKey(String key)
    {
        if(getDataObject() != null) {
            getDataObject().setKey(key);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValuePairStructDataObject is null!");
        }
    }

    public String getValue()
    {
        if(getDataObject() != null) {
            return getDataObject().getValue();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValuePairStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setValue(String value)
    {
        if(getDataObject() != null) {
            getDataObject().setValue(value);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValuePairStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValuePairStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValuePairStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeyValuePairStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getValue() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public KeyValuePairStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
