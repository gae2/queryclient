package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "serviceEndpoints")
@XmlType(propOrder = {"serviceEndpoint", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceEndpointListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ServiceEndpointListStub.class.getName());

    private List<ServiceEndpointStub> serviceEndpoints = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public ServiceEndpointListStub()
    {
        this(new ArrayList<ServiceEndpointStub>());
    }
    public ServiceEndpointListStub(List<ServiceEndpointStub> serviceEndpoints)
    {
        this(serviceEndpoints, null);
    }
    public ServiceEndpointListStub(List<ServiceEndpointStub> serviceEndpoints, String forwardCursor)
    {
        this.serviceEndpoints = serviceEndpoints;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(serviceEndpoints == null) {
            return true;
        } else {
            return serviceEndpoints.isEmpty();
        }
    }
    public int getSize()
    {
        if(serviceEndpoints == null) {
            return 0;
        } else {
            return serviceEndpoints.size();
        }
    }


    @XmlElement(name = "serviceEndpoint")
    public List<ServiceEndpointStub> getServiceEndpoint()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ServiceEndpointStub> getList()
    {
        return serviceEndpoints;
    }
    public void setList(List<ServiceEndpointStub> serviceEndpoints)
    {
        this.serviceEndpoints = serviceEndpoints;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<ServiceEndpointStub> it = this.serviceEndpoints.iterator();
        while(it.hasNext()) {
            ServiceEndpointStub serviceEndpoint = it.next();
            sb.append(serviceEndpoint.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ServiceEndpointListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ServiceEndpointListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ServiceEndpointListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ServiceEndpointListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ServiceEndpointListStub object as a string.", e);
        }
        
        return null;
    }
    public static ServiceEndpointListStub fromJsonString(String jsonStr)
    {
        try {
            ServiceEndpointListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ServiceEndpointListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ServiceEndpointListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ServiceEndpointListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ServiceEndpointListStub object.", e);
        }
        
        return null;
    }

}
