package com.queryclient.cert.ws;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseConsumerRegistry.class.getName());

    // Consumer key-secret map.
    // TBD: Use a better data structure which reflects OAuthConsumerInfo.
    private Map<String, String> consumerSecretMap;
    
    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD...
        consumerSecretMap.put("2f1eef17-7ed5-4ab9-9890-804979df8403", "be5350ab-664d-4d90-a8dc-1aae7b4acc8f");  // QueryClient + QueryClientApp
        consumerSecretMap.put("39576d83-6b08-48ff-9590-ffd5a2bb151b", "02c216cc-b8ae-4cc8-8351-6478aac5f9e8");  // QueryClient + QueryClient
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
