package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ApiConsumer;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "apiConsumer")
@XmlType(propOrder = {"guid", "aeryId", "name", "description", "appKey", "appSecret", "status", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiConsumerStub implements ApiConsumer, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ApiConsumerStub.class.getName());

    private String guid;
    private String aeryId;
    private String name;
    private String description;
    private String appKey;
    private String appSecret;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    public ApiConsumerStub()
    {
        this(null);
    }
    public ApiConsumerStub(ApiConsumer bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.aeryId = bean.getAeryId();
            this.name = bean.getName();
            this.description = bean.getDescription();
            this.appKey = bean.getAppKey();
            this.appSecret = bean.getAppSecret();
            this.status = bean.getStatus();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getAeryId()
    {
        return this.aeryId;
    }
    public void setAeryId(String aeryId)
    {
        this.aeryId = aeryId;
    }

    @XmlElement
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    @XmlElement
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    @XmlElement
    public String getAppKey()
    {
        return this.appKey;
    }
    public void setAppKey(String appKey)
    {
        this.appKey = appKey;
    }

    @XmlElement
    public String getAppSecret()
    {
        return this.appSecret;
    }
    public void setAppSecret(String appSecret)
    {
        this.appSecret = appSecret;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("aeryId", this.aeryId);
        dataMap.put("name", this.name);
        dataMap.put("description", this.description);
        dataMap.put("appKey", this.appKey);
        dataMap.put("appSecret", this.appSecret);
        dataMap.put("status", this.status);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = aeryId == null ? 0 : aeryId.hashCode();
        _hash = 31 * _hash + delta;
        delta = name == null ? 0 : name.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = appKey == null ? 0 : appKey.hashCode();
        _hash = 31 * _hash + delta;
        delta = appSecret == null ? 0 : appSecret.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ApiConsumerStub convertBeanToStub(ApiConsumer bean)
    {
        ApiConsumerStub stub = null;
        if(bean instanceof ApiConsumerStub) {
            stub = (ApiConsumerStub) bean;
        } else {
            if(bean != null) {
                stub = new ApiConsumerStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ApiConsumerStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of ApiConsumerStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ApiConsumerStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ApiConsumerStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ApiConsumerStub object as a string.", e);
        }
        
        return null;
    }
    public static ApiConsumerStub fromJsonString(String jsonStr)
    {
        try {
            ApiConsumerStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ApiConsumerStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ApiConsumerStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ApiConsumerStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ApiConsumerStub object.", e);
        }
        
        return null;
    }

}
