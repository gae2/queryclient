package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class QuerySessionBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QuerySessionBasePermission.class.getName());

    public QuerySessionBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "QuerySession::" + action;
    }


}
