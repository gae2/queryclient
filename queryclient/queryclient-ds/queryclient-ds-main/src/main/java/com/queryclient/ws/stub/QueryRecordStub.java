package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "queryRecord")
@XmlType(propOrder = {"guid", "querySession", "queryId", "dataService", "serviceUrl", "delayed", "query", "inputFormat", "inputFile", "inputContent", "targetOutputFormat", "outputFormat", "outputFile", "outputContent", "responseCode", "result", "referrerInfoStub", "status", "extra", "note", "scheduledTime", "processedTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryRecordStub implements QueryRecord, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QueryRecordStub.class.getName());

    private String guid;
    private String querySession;
    private Integer queryId;
    private String dataService;
    private String serviceUrl;
    private Boolean delayed;
    private String query;
    private String inputFormat;
    private String inputFile;
    private String inputContent;
    private String targetOutputFormat;
    private String outputFormat;
    private String outputFile;
    private String outputContent;
    private Integer responseCode;
    private String result;
    private ReferrerInfoStructStub referrerInfo;
    private String status;
    private String extra;
    private String note;
    private Long scheduledTime;
    private Long processedTime;
    private Long createdTime;
    private Long modifiedTime;

    public QueryRecordStub()
    {
        this(null);
    }
    public QueryRecordStub(QueryRecord bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.querySession = bean.getQuerySession();
            this.queryId = bean.getQueryId();
            this.dataService = bean.getDataService();
            this.serviceUrl = bean.getServiceUrl();
            this.delayed = bean.isDelayed();
            this.query = bean.getQuery();
            this.inputFormat = bean.getInputFormat();
            this.inputFile = bean.getInputFile();
            this.inputContent = bean.getInputContent();
            this.targetOutputFormat = bean.getTargetOutputFormat();
            this.outputFormat = bean.getOutputFormat();
            this.outputFile = bean.getOutputFile();
            this.outputContent = bean.getOutputContent();
            this.responseCode = bean.getResponseCode();
            this.result = bean.getResult();
            this.referrerInfo = ReferrerInfoStructStub.convertBeanToStub(bean.getReferrerInfo());
            this.status = bean.getStatus();
            this.extra = bean.getExtra();
            this.note = bean.getNote();
            this.scheduledTime = bean.getScheduledTime();
            this.processedTime = bean.getProcessedTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getQuerySession()
    {
        return this.querySession;
    }
    public void setQuerySession(String querySession)
    {
        this.querySession = querySession;
    }

    @XmlElement
    public Integer getQueryId()
    {
        return this.queryId;
    }
    public void setQueryId(Integer queryId)
    {
        this.queryId = queryId;
    }

    @XmlElement
    public String getDataService()
    {
        return this.dataService;
    }
    public void setDataService(String dataService)
    {
        this.dataService = dataService;
    }

    @XmlElement
    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    @XmlElement
    public Boolean isDelayed()
    {
        return this.delayed;
    }
    public void setDelayed(Boolean delayed)
    {
        this.delayed = delayed;
    }

    @XmlElement
    public String getQuery()
    {
        return this.query;
    }
    public void setQuery(String query)
    {
        this.query = query;
    }

    @XmlElement
    public String getInputFormat()
    {
        return this.inputFormat;
    }
    public void setInputFormat(String inputFormat)
    {
        this.inputFormat = inputFormat;
    }

    @XmlElement
    public String getInputFile()
    {
        return this.inputFile;
    }
    public void setInputFile(String inputFile)
    {
        this.inputFile = inputFile;
    }

    @XmlElement
    public String getInputContent()
    {
        return this.inputContent;
    }
    public void setInputContent(String inputContent)
    {
        this.inputContent = inputContent;
    }

    @XmlElement
    public String getTargetOutputFormat()
    {
        return this.targetOutputFormat;
    }
    public void setTargetOutputFormat(String targetOutputFormat)
    {
        this.targetOutputFormat = targetOutputFormat;
    }

    @XmlElement
    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    @XmlElement
    public String getOutputFile()
    {
        return this.outputFile;
    }
    public void setOutputFile(String outputFile)
    {
        this.outputFile = outputFile;
    }

    @XmlElement
    public String getOutputContent()
    {
        return this.outputContent;
    }
    public void setOutputContent(String outputContent)
    {
        this.outputContent = outputContent;
    }

    @XmlElement
    public Integer getResponseCode()
    {
        return this.responseCode;
    }
    public void setResponseCode(Integer responseCode)
    {
        this.responseCode = responseCode;
    }

    @XmlElement
    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    @XmlElement(name = "referrerInfo")
    @JsonIgnore
    public ReferrerInfoStructStub getReferrerInfoStub()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfoStub(ReferrerInfoStructStub referrerInfo)
    {
        this.referrerInfo = referrerInfo;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ReferrerInfoStructStub.class)
    public ReferrerInfoStruct getReferrerInfo()
    {  
        return getReferrerInfoStub();
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if((referrerInfo == null) || (referrerInfo instanceof ReferrerInfoStructStub)) {
            setReferrerInfoStub((ReferrerInfoStructStub) referrerInfo);
        } else {
            // TBD
            setReferrerInfoStub(ReferrerInfoStructStub.convertBeanToStub(referrerInfo));
        }
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getExtra()
    {
        return this.extra;
    }
    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public Long getScheduledTime()
    {
        return this.scheduledTime;
    }
    public void setScheduledTime(Long scheduledTime)
    {
        this.scheduledTime = scheduledTime;
    }

    @XmlElement
    public Long getProcessedTime()
    {
        return this.processedTime;
    }
    public void setProcessedTime(Long processedTime)
    {
        this.processedTime = processedTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("querySession", this.querySession);
        dataMap.put("queryId", this.queryId);
        dataMap.put("dataService", this.dataService);
        dataMap.put("serviceUrl", this.serviceUrl);
        dataMap.put("delayed", this.delayed);
        dataMap.put("query", this.query);
        dataMap.put("inputFormat", this.inputFormat);
        dataMap.put("inputFile", this.inputFile);
        dataMap.put("inputContent", this.inputContent);
        dataMap.put("targetOutputFormat", this.targetOutputFormat);
        dataMap.put("outputFormat", this.outputFormat);
        dataMap.put("outputFile", this.outputFile);
        dataMap.put("outputContent", this.outputContent);
        dataMap.put("responseCode", this.responseCode);
        dataMap.put("result", this.result);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("status", this.status);
        dataMap.put("extra", this.extra);
        dataMap.put("note", this.note);
        dataMap.put("scheduledTime", this.scheduledTime);
        dataMap.put("processedTime", this.processedTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = querySession == null ? 0 : querySession.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryId == null ? 0 : queryId.hashCode();
        _hash = 31 * _hash + delta;
        delta = dataService == null ? 0 : dataService.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = delayed == null ? 0 : delayed.hashCode();
        _hash = 31 * _hash + delta;
        delta = query == null ? 0 : query.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputFormat == null ? 0 : inputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputFile == null ? 0 : inputFile.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputContent == null ? 0 : inputContent.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetOutputFormat == null ? 0 : targetOutputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFormat == null ? 0 : outputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFile == null ? 0 : outputFile.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputContent == null ? 0 : outputContent.hashCode();
        _hash = 31 * _hash + delta;
        delta = responseCode == null ? 0 : responseCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = result == null ? 0 : result.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = extra == null ? 0 : extra.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = scheduledTime == null ? 0 : scheduledTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = processedTime == null ? 0 : processedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static QueryRecordStub convertBeanToStub(QueryRecord bean)
    {
        QueryRecordStub stub = null;
        if(bean instanceof QueryRecordStub) {
            stub = (QueryRecordStub) bean;
        } else {
            if(bean != null) {
                stub = new QueryRecordStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static QueryRecordStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of QueryRecordStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write QueryRecordStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write QueryRecordStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write QueryRecordStub object as a string.", e);
        }
        
        return null;
    }
    public static QueryRecordStub fromJsonString(String jsonStr)
    {
        try {
            QueryRecordStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, QueryRecordStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into QueryRecordStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into QueryRecordStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into QueryRecordStub object.", e);
        }
        
        return null;
    }

}
