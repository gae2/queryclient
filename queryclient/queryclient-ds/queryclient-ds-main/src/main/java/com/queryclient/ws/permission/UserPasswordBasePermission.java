package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class UserPasswordBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserPasswordBasePermission.class.getName());

    public UserPasswordBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "UserPassword::" + action;
    }


}
