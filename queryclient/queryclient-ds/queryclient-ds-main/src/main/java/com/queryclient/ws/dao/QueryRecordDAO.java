package com.queryclient.ws.dao;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.data.QueryRecordDataObject;


// TBD: Add offset/count to getAllQueryRecords() and findQueryRecords(), etc.
public interface QueryRecordDAO
{
    QueryRecordDataObject getQueryRecord(String guid) throws BaseException;
    List<QueryRecordDataObject> getQueryRecords(List<String> guids) throws BaseException;
    List<QueryRecordDataObject> getAllQueryRecords() throws BaseException;
    /* @Deprecated */ List<QueryRecordDataObject> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException;
    List<QueryRecordDataObject> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<QueryRecordDataObject> findQueryRecords(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<QueryRecordDataObject> findQueryRecords(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<QueryRecordDataObject> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<QueryRecordDataObject> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createQueryRecord(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return QueryRecordDataObject?)
    String createQueryRecord(QueryRecordDataObject queryRecord) throws BaseException;          // Returns Guid.  (Return QueryRecordDataObject?)
    //Boolean updateQueryRecord(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateQueryRecord(QueryRecordDataObject queryRecord) throws BaseException;
    Boolean deleteQueryRecord(String guid) throws BaseException;
    Boolean deleteQueryRecord(QueryRecordDataObject queryRecord) throws BaseException;
    Long deleteQueryRecords(String filter, String params, List<String> values) throws BaseException;
}
