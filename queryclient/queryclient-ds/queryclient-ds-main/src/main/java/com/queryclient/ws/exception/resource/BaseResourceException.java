package com.queryclient.ws.exception.resource;

// Base exception to be used through web services methods.
public class BaseResourceException extends RuntimeException  // extends BaseException ???
{
    private static final long serialVersionUID = 1L;
    
    // Resource URL (or, URL path segment).
    private String resource = null; // request

    public BaseResourceException() 
    {
        super();
    }
    public BaseResourceException(String message) 
    {
        super(message);
    }
    public BaseResourceException(String message, String resource) 
    {
        super(message);
        this.resource = resource;
    }
    public BaseResourceException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public BaseResourceException(String message, Throwable cause, String resource) 
    {
        super(message, cause);
        this.resource = resource;
    }
    public BaseResourceException(Throwable cause) 
    {
        super(cause);
    }
    public BaseResourceException(Throwable cause, String resource) 
    {
        super(cause);
        this.resource = resource;
    }
    
    public String getResource()
    {
        return this.resource;
    }

}
