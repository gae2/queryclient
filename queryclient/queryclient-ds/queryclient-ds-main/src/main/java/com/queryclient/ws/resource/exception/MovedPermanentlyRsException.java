package com.queryclient.ws.resource.exception;

import com.queryclient.ws.exception.resource.BaseResourceException;


public class MovedPermanentlyRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public MovedPermanentlyRsException() 
    {
        super();
    }
    public MovedPermanentlyRsException(String message) 
    {
        super(message);
    }
    public MovedPermanentlyRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public MovedPermanentlyRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public MovedPermanentlyRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public MovedPermanentlyRsException(Throwable cause) 
    {
        super(cause);
    }
    public MovedPermanentlyRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
