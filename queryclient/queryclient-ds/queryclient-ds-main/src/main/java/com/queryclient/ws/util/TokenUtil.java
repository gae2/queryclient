package com.queryclient.ws.util;

import java.util.logging.Logger;
import java.util.logging.Level;


///////////////////////////////////////
//
//78^3 =       474 552
//78^4 =    37 015 056
//78^5 = 2 887 174 368
//
//74^3 =       405 224  --> tiny/3
//74^4 =    29 986 576
//74^5 = 2 219 006 624
//
//72^3 =       373 248
//72^4 =    26 873 856
//72^5 = 1 934 917 632
//
//------------------------------------
//
//64^4 =    16 777 216
//64^5 = 1 073 741 824
//
//62^4 =    14 775 336  --> short/4
//62^5 =   916 132 832
//52^4 =     7 311 616
//52^5 =   380 204 032
//
//------------------------------------
//
//36^4 =     1 679 616
//36^5 =    60 466 176  --> medium/5
//36^6 = 2 176 782 336
//
//34^4 =     1 336 336
//34^5 =    45 435 424
//34^6 = 1 544 804 416
//32^4 =     1 048 576
//32^5 =    33 554 432
//32^6 = 1 073 742 824
//
//------------------------------------
//
//26^5 =    11 881 376
//26^6 =   308 915 776  --> long/6
//26^7 = 8 031 810 176
//
//------------------------------------
//
//12^6 =     2 985 984
//12^7 =    35 831 808
//12^8 =   429 981 696
//
//10^7 =    10 000 000  --> digit/7
//10^8 =   100 000 000
//10^9 = 1 000 000 000
//
//------------------------------------
//
//
///////////////////////////////////////


public final class TokenUtil
{
    private static final Logger log = Logger.getLogger(TokenUtil.class.getName());
    
    private TokenUtil() {}

    

    // TBD:
    // Better algorithm in case the base is an exponent of 2, e.g., 64, 32, etc. ???
    // ????
    
    
    // TBD:
    // 72 chars: a-z, A-Z, 0-9, and - $ _ . + ! * , ( )
    // also   ; / ? : @ = &   in certain cases  --> up to 79 chars...
    // ...
    
    
    // 12 chars in addition to the 62 alphanumeric characters.
    // Note: '@' and '~' cannot be the first char of a token (in our design),
    //       and '/' cannot be the first or second char (or, the last char?).
    // ';', '#', and '?' may not be allowed due to URL parsing....
    private static final char[] SpecialChars = new char[]{'-', '_', '.', '$', '+', '!', '*', ',', '(', ')', ':', '=', '&'};
    private static char getBase75Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        } else if(i >= 52 && i < 62) {
            c = (char) (i - 52 + 48);   // 0 == 48
        } else {  // if(i >= 62 && i < 75) {
            c = SpecialChars[i - 62];   // Note if i>=75 error!, but this should not happen....
        }
        return c;
    }
    private static int getBase75Int(char c)
    {
        int i;
        for(int k=0; k<SpecialChars.length; k++) {
            if(SpecialChars[k] == c) {
                return k + 62;
            }
        }
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else if(chi >= 48) {
            i = chi - 48 + 52;
        } else {
            // ????
            i = 0;
        }
        return i;
    }
    private static char getBase74Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        } else if(i >= 52 && i < 62) {
            c = (char) (i - 52 + 48);   // 0 == 48
        } else {  // if(i >= 62 && i < 74) {
            c = SpecialChars[i - 62];   // Note if i>=74 error!, but this should not happen....
        }
        return c;
    }
    private static int getBase74Int(char c)
    {
        int i;
        for(int k=0; k<SpecialChars.length; k++) {
            if(SpecialChars[k] == c) {
                return k + 62;
            }
        }
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else if(chi >= 48) {
            i = chi - 48 + 52;
        } else {
            // ????
            i = 0;
        }
        return i;
    }
    private static char getBase72Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        } else if(i >= 52 && i < 62) {
            c = (char) (i - 52 + 48);   // 0 == 48
        } else {  // if(i >= 62 && i < 72) {
            c = SpecialChars[i - 62];   // Note if i>=72 error!, but this should not happen....
        }
        return c;
    }
    private static int getBase72Int(char c)
    {
        int i;
        for(int k=0; k<SpecialChars.length; k++) {
            if(SpecialChars[k] == c) {
                return k + 62;
            }
        }
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else if(chi >= 48) {
            i = chi - 48 + 52;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    
    // Alphabets + digits + "_" + "-"
    private static char getBase64Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        } else if(i >= 52 && i < 62) {
            c = (char) (i - 52 + 48);   // 0 == 48
        } else { // 62 <= i < 64
            if(i == 62) {
                c = '-';
            } else {
                c = '_';
            }
        }
        return c;
    }
    private static int getBase64Int(char c)
    {
        int i;
        for(int k=0; k<SpecialChars.length; k++) {
            if(SpecialChars[k] == c) {
                return k + 62;
            }
        }
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else if(chi >= 48) {
            i = chi - 48 + 52;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // Alphabets + digits
    private static char getBase62Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        } else {  // if(i >= 52 && i < 62) {
            c = (char) (i - 52 + 48);   // 0 == 48
        }
        return c;
    }
    private static int getBase62Int(char c)
    {
        int i;
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else if(chi >= 48) {
            i = chi - 48 + 52;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // alphabets only.
    private static char getBase52Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else {  // if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        }
        return c;
    }
    private static int getBase52Int(char c)
    {
        int i;
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // lower-case alphabets + digits
    private static char getBase36Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 97);   // a == 97
        } else {  // if(i >= 26 && i < 36) {
            c = (char) (i - 26 + 48);   // 0 == 48
        }
        return c;
    }
    private static int getBase36Int(char c)
    {
        int i;
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97;
        } else if(chi >= 48) {
            i = chi - 48 + 26;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // lower-case alphabets + digits (excluding l/1 and o/0)
    private static char getBase34Char(int i)
    {
        char c;
        
//        // [1] Exclude alphabets 1 and o
//        if(i >= 0 && i < 10) {
//            c = (char) (i + 48);        // 0 == 48
//        } else if(i >= 10 && i < 21) {
//            c = (char) (i - 10 + 97);   // a == 97
//        } else if(i >= 21 && i < 23) {  // exclude L.
//            c = (char) (i - 21 + 109);  // m == 109
//        } else {                        // exclude O
//            c = (char) (i - 23 + 112);  // p == 112
//        }

        // [2] Exclude digits 0 and 1
        if(i >= 0 && i < 8) {
            c = (char) (i + 50);        // 2 == 50
        } else {  // if(i >= 8 && i < 34) {
            c = (char) (i - 8 + 97);    // a == 97
        }

        return c;
    }
    private static int getBase34Int(char c)
    {
        int i;
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 8;
        } else if(chi >= 50) {
            i = chi - 50;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // lower-case alphabets + digits excluding alphabets l, o, y, and z
    private static char getBase32Char(int i)
    {
        char c;
        if(i >= 0 && i < 10) {
            c = (char) (i + 48);        // 0 == 48
        } else if(i >= 10 && i < 21) {
            c = (char) (i - 10 + 97);   // a == 97
        } else if(i >= 21 && i < 23) {  // exclude L.
            c = (char) (i - 21 + 109);  // m == 109
        } else {                        // exclude O, Y, Z.
            c = (char) (i - 23 + 112);  // p == 112
        }
        return c;
    }
    private static int getBase32Int(char c)
    {
        int i;
        int chi = (int) c;
        if(chi >= 112) {
            i = chi - 112 + 23;
        } else if(chi >= 109) {
            i = chi - 109 + 21;
        } else if(chi >= 97) {
            i = chi - 97 + 10;
        } else if(chi >= 48) {
            i = chi - 48;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // lower-case alphabets only
    private static char getBase26Char(int i)
    {
        char c;
        // c = (char) (i + 97);  // a == 97
        c = (char) ((i % 26) + 97);  // a == 97   // ????
        return c;
    }
    private static int getBase26Int(char c)
    {
        int i;
        int chi = (int) c;
        // if(chi >= 97) {
        if(chi >= 97 && chi < 123) {   // ???
            i = chi - 97;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // Digits + "_" + "-"
    private static char getBase12Char(int i)
    {
        char c;
        
        if(i >= 0 && i < 10) {
            c = (char) (i + 48);        // 0 == 48
        } else {
            if(i == 10) {
                c = '-';
            } else {
                c = '_';
            }
        }

        return c;
    }
    private static int getBase12Int(char c)
    {
        int i;
        for(int k=0; k<SpecialChars.length; k++) {
            if(SpecialChars[k] == c) {
                return k + 10;
            }
        }
        int chi = (int) c;
        // if(chi >= 48) {
        if(chi >= 48 && chi < 58) {   // ???
            i = chi - 48;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // digits only
    private static char getBase10Char(int i)
    {
        char c;
        // c = (char) (i + 48);  // 0 == 48
        c = (char) ((i % 10) + 48);  // 0 == 48   // ????
        return c;
    }
    private static int getBase10Int(char c)
    {
        int i;
        int chi = (int) c;
        // if(chi >= 48) {
        if(chi >= 48 && chi < 58) {   // ???
            i = chi - 48;
        } else {
            // ????
            i = 0;
        }
        return i;
    }


    // vowels only.
    private static char getBase5Char(int i)
    {
        char c;
        // switch(i) {
        switch(i % 5) {    // ???
        case 0:
        default:  // ???
            c = 'a';
            break;
        case 1:
            c = 'e';
            break;
        case 2:
            c = 'i';
            break;
        case 3:
            c = 'o';
            break;
        case 4:
            c = 'u';
            break;
        }
        return c;
    }
    private static int getBase5Int(char c)
    {
        int i;
        switch(c) {
        case 'a':
        default:  // ???
            i = 0;
            break;
        case 'e':
            i = 1;
            break;
        case 'i':
            i = 2;
            break;
        case 'o':
            i = 3;
            break;
        case 'u':
            i = 4;
            break;
        }
        return i;
    }

    // abc only.
    private static char getBase3Char(int i)
    {
        char c;
        // switch(i) {
        switch(i % 3) {   // ????
        case 0:
        default:  // ???
            c = 'a';
            break;
        case 1:
            c = 'b';
            break;
        case 2:
            c = 'c';
            break;
        }
        return c;
    }
    private static int getBase3Int(char c)
    {
        int i;
        switch(c) {
        case 'a':
        default:  // ???
            i = 0;
            break;
        case 'b':
            i = 1;
            break;
        case 'c':
            i = 2;
            break;
        }
        return i;
    }

    // 0/1 only
    private static char getBase2Char(int i)
    {
        char c;
        // switch(i) {
        switch(i % 2) {    // ???
        case 0:
        default:  // ???
            c = '0';
            break;
        case 1:
            c = '1';
            break;
        }
        return c;
    }
    private static int getBase2Int(char c)
    {
        int i;
        switch(c) {
        case '0':
        default:  // ???
            i = 0;
            break;
        case '1':
            i = 1;
            break;
        }
        return i;
    }
    
    
    
    public static String generateBase75Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase75Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase75Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 75L);
            int rem = (int) (base - tmp * 75L);
            base = tmp;
            sb.append(getBase75Char(rem));
        }
        String tinyUriToken = sb.reverse().toString();
        return tinyUriToken;
    }


    public static String generateBase74Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase74Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase74Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 74L);
            int rem = (int) (base - tmp * 74L);
            base = tmp;
            sb.append(getBase74Char(rem));
        }
        String tinyUriToken = sb.reverse().toString();
        return tinyUriToken;
    }


    public static String generateBase72Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase72Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase72Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 72L);
            int rem = (int) (base - tmp * 72L);
            base = tmp;
            sb.append(getBase72Char(rem));
        }
        String tinyUriToken = sb.reverse().toString();
        return tinyUriToken;
    }

    
    public static String generateBase64Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase64Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase64Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 64L);
            int rem = (int) (base - tmp * 64L);
            base = tmp;
            sb.append(getBase64Char(rem));
        }
        String tinyUriToken = sb.reverse().toString();
        return tinyUriToken;
    }

    
    public static String generateBase62Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase62Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase62Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 62L);
            int rem = (int) (base - tmp * 62L);
            base = tmp;
            sb.append(getBase62Char(rem));
        }
        String shortUriToken = sb.reverse().toString();
        return shortUriToken;
    }
    

    public static String generateBase52Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase52Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase52Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 52L);
            int rem = (int) (base - tmp * 52L);
            base = tmp;
            sb.append(getBase52Char(rem));
        }
        String tinyUriToken = sb.reverse().toString();
        return tinyUriToken;
    }


    public static String generateBase36Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase36Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase36Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 36L);
            int rem = (int) (base - tmp * 36L);
            base = tmp;
            sb.append(getBase36Char(rem));
        }
        String mediumUriToken = sb.reverse().toString();
        return mediumUriToken;
    }


    public static String generateBase34Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase34Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase34Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 34L);
            int rem = (int) (base - tmp * 34L);
            base = tmp;
            sb.append(getBase34Char(rem));
        }
        String tinyUriToken = sb.reverse().toString();
        return tinyUriToken;
    }


    public static String generateBase32Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase32Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase32Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 32L);
            int rem = (int) (base - tmp * 32L);
            base = tmp;
            sb.append(getBase32Char(rem));
        }
        String tinyUriToken = sb.reverse().toString();
        return tinyUriToken;
    }


    public static String generateBase26Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase26Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase26Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 26L);
            int rem = (int) (base - tmp * 26L);
            base = tmp;
            sb.append(getBase26Char(rem));
        }
        String longUriToken = sb.reverse().toString();
        return longUriToken;
    }


    public static String generateBase12Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase12Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase12Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 12L);
            int rem = (int) (base - tmp * 12L);
            base = tmp;
            sb.append(getBase12Char(rem));
        }
        String tinyUriToken = sb.reverse().toString();
        return tinyUriToken;
    }


    public static String generateBase10Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase10Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase10Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 10L);
            int rem = (int) (base - tmp * 10L);
            base = tmp;
            sb.append(getBase10Char(rem));
        }
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }
    

    public static String generateBase05Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase05Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase05Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 5L);
            int rem = (int) (base - tmp * 5L);
            base = tmp;
            sb.append(getBase5Char(rem));
        }
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }


    public static String generateBase03Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase03Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase03Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 3L);
            int rem = (int) (base - tmp * 3L);
            base = tmp;
            sb.append(getBase3Char(rem));
        }
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }


    public static String generateBase02Token(String hexStr)
    {
        if(hexStr == null || hexStr.isEmpty()) {
            return hexStr;
        } else {
            int len = hexStr.length();
            if(len > 31) {
                hexStr = hexStr.substring(len - 31, len);
            }
        }
        try {
            long base = Long.parseLong(hexStr, 16);
            return generateBase02Token(base);
        } catch(NumberFormatException e) {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Invalid hexStr = " + hexStr, e);
            return null;
        }
    }
    public static String generateBase02Token(long base)
    {
        StringBuilder sb = new StringBuilder();
        while(base > 0) {
            long tmp = (long) (base / 2L);
            int rem = (int) (base - tmp * 2L);
            base = tmp;
            sb.append(getBase2Char(rem));
        }
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }
    

}
