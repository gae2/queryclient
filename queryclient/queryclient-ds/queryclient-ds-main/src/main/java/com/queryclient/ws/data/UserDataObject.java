package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.User;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class UserDataObject extends KeyedDataObject implements User
{
    private static final Logger log = Logger.getLogger(UserDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(UserDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(UserDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String managerApp;

    @Persistent(defaultFetchGroup = "true")
    private Long appAcl;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="groupId", columns=@Column(name="gaeAppgroupId")),
        @Persistent(name="appId", columns=@Column(name="gaeAppappId")),
        @Persistent(name="appDomain", columns=@Column(name="gaeAppappDomain")),
        @Persistent(name="namespace", columns=@Column(name="gaeAppnamespace")),
        @Persistent(name="acl", columns=@Column(name="gaeAppacl")),
        @Persistent(name="note", columns=@Column(name="gaeAppnote")),
    })
    private GaeAppStructDataObject gaeApp;

    @Persistent(defaultFetchGroup = "true")
    private String aeryId;

    @Persistent(defaultFetchGroup = "true")
    private String sessionId;

    @Persistent(defaultFetchGroup = "true")
    private String username;

    @Persistent(defaultFetchGroup = "true")
    private String nickname;

    @Persistent(defaultFetchGroup = "true")
    private String avatar;

    @Persistent(defaultFetchGroup = "true")
    private String email;

    @Persistent(defaultFetchGroup = "true")
    private String openId;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="authDomain", columns=@Column(name="gaeUserauthDomain")),
        @Persistent(name="federatedIdentity", columns=@Column(name="gaeUserfederatedIdentity")),
        @Persistent(name="nickname", columns=@Column(name="gaeUsernickname")),
        @Persistent(name="userId", columns=@Column(name="gaeUseruserId")),
        @Persistent(name="email", columns=@Column(name="gaeUseremail")),
        @Persistent(name="note", columns=@Column(name="gaeUsernote")),
    })
    private GaeUserStructDataObject gaeUser;

    @Persistent(defaultFetchGroup = "true")
    private String timeZone;

    @Persistent(defaultFetchGroup = "true")
    private String address;

    @Persistent(defaultFetchGroup = "true")
    private String location;

    @Persistent(defaultFetchGroup = "true")
    private String ipAddress;

    @Persistent(defaultFetchGroup = "true")
    private String referer;

    @Persistent(defaultFetchGroup = "true")
    private Boolean obsolete;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Long emailVerifiedTime;

    @Persistent(defaultFetchGroup = "true")
    private Long openIdVerifiedTime;

    @Persistent(defaultFetchGroup = "true")
    private Long authenticatedTime;

    public UserDataObject()
    {
        this(null);
    }
    public UserDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String address, String location, String ipAddress, String referer, Boolean obsolete, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime)
    {
        this(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, address, location, ipAddress, referer, obsolete, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime, null, null);
    }
    public UserDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String address, String location, String ipAddress, String referer, Boolean obsolete, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        if(gaeApp != null) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = null;
        }
        this.aeryId = aeryId;
        this.sessionId = sessionId;
        this.username = username;
        this.nickname = nickname;
        this.avatar = avatar;
        this.email = email;
        this.openId = openId;
        if(gaeUser != null) {
            this.gaeUser = new GaeUserStructDataObject(gaeUser.getAuthDomain(), gaeUser.getFederatedIdentity(), gaeUser.getNickname(), gaeUser.getUserId(), gaeUser.getEmail(), gaeUser.getNote());
        } else {
            this.gaeUser = null;
        }
        this.timeZone = timeZone;
        this.address = address;
        this.location = location;
        this.ipAddress = ipAddress;
        this.referer = referer;
        this.obsolete = obsolete;
        this.status = status;
        this.emailVerifiedTime = emailVerifiedTime;
        this.openIdVerifiedTime = openIdVerifiedTime;
        this.authenticatedTime = authenticatedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return UserDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return UserDataObject.composeKey(getGuid());
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStruct getGaeApp()
    {
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(gaeApp == null) {
            this.gaeApp = null;
            log.log(Level.INFO, "UserDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is null.");            
        } else if(gaeApp instanceof GaeAppStructDataObject) {
            this.gaeApp = (GaeAppStructDataObject) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = new GaeAppStructDataObject();   // ????
            log.log(Level.WARNING, "UserDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is of an invalid type.");
        }
    }

    public String getAeryId()
    {
        return this.aeryId;
    }
    public void setAeryId(String aeryId)
    {
        this.aeryId = aeryId;
    }

    public String getSessionId()
    {
        return this.sessionId;
    }
    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getNickname()
    {
        return this.nickname;
    }
    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getAvatar()
    {
        return this.avatar;
    }
    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getOpenId()
    {
        return this.openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    public GaeUserStruct getGaeUser()
    {
        return this.gaeUser;
    }
    public void setGaeUser(GaeUserStruct gaeUser)
    {
        if(gaeUser == null) {
            this.gaeUser = null;
            log.log(Level.INFO, "UserDataObject.setGaeUser(GaeUserStruct gaeUser): Arg gaeUser is null.");            
        } else if(gaeUser instanceof GaeUserStructDataObject) {
            this.gaeUser = (GaeUserStructDataObject) gaeUser;
        } else if(gaeUser instanceof GaeUserStruct) {
            this.gaeUser = new GaeUserStructDataObject(gaeUser.getAuthDomain(), gaeUser.getFederatedIdentity(), gaeUser.getNickname(), gaeUser.getUserId(), gaeUser.getEmail(), gaeUser.getNote());
        } else {
            this.gaeUser = new GaeUserStructDataObject();   // ????
            log.log(Level.WARNING, "UserDataObject.setGaeUser(GaeUserStruct gaeUser): Arg gaeUser is of an invalid type.");
        }
    }

    public String getTimeZone()
    {
        return this.timeZone;
    }
    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }

    public String getAddress()
    {
        return this.address;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public String getReferer()
    {
        return this.referer;
    }
    public void setReferer(String referer)
    {
        this.referer = referer;
    }

    public Boolean isObsolete()
    {
        return this.obsolete;
    }
    public void setObsolete(Boolean obsolete)
    {
        this.obsolete = obsolete;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getEmailVerifiedTime()
    {
        return this.emailVerifiedTime;
    }
    public void setEmailVerifiedTime(Long emailVerifiedTime)
    {
        this.emailVerifiedTime = emailVerifiedTime;
    }

    public Long getOpenIdVerifiedTime()
    {
        return this.openIdVerifiedTime;
    }
    public void setOpenIdVerifiedTime(Long openIdVerifiedTime)
    {
        this.openIdVerifiedTime = openIdVerifiedTime;
    }

    public Long getAuthenticatedTime()
    {
        return this.authenticatedTime;
    }
    public void setAuthenticatedTime(Long authenticatedTime)
    {
        this.authenticatedTime = authenticatedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("aeryId", this.aeryId);
        dataMap.put("sessionId", this.sessionId);
        dataMap.put("username", this.username);
        dataMap.put("nickname", this.nickname);
        dataMap.put("avatar", this.avatar);
        dataMap.put("email", this.email);
        dataMap.put("openId", this.openId);
        dataMap.put("gaeUser", this.gaeUser);
        dataMap.put("timeZone", this.timeZone);
        dataMap.put("address", this.address);
        dataMap.put("location", this.location);
        dataMap.put("ipAddress", this.ipAddress);
        dataMap.put("referer", this.referer);
        dataMap.put("obsolete", this.obsolete);
        dataMap.put("status", this.status);
        dataMap.put("emailVerifiedTime", this.emailVerifiedTime);
        dataMap.put("openIdVerifiedTime", this.openIdVerifiedTime);
        dataMap.put("authenticatedTime", this.authenticatedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        User thatObj = (User) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.managerApp == null && thatObj.getManagerApp() != null)
            || (this.managerApp != null && thatObj.getManagerApp() == null)
            || !this.managerApp.equals(thatObj.getManagerApp()) ) {
            return false;
        }
        if( (this.appAcl == null && thatObj.getAppAcl() != null)
            || (this.appAcl != null && thatObj.getAppAcl() == null)
            || !this.appAcl.equals(thatObj.getAppAcl()) ) {
            return false;
        }
        if( (this.gaeApp == null && thatObj.getGaeApp() != null)
            || (this.gaeApp != null && thatObj.getGaeApp() == null)
            || !this.gaeApp.equals(thatObj.getGaeApp()) ) {
            return false;
        }
        if( (this.aeryId == null && thatObj.getAeryId() != null)
            || (this.aeryId != null && thatObj.getAeryId() == null)
            || !this.aeryId.equals(thatObj.getAeryId()) ) {
            return false;
        }
        if( (this.sessionId == null && thatObj.getSessionId() != null)
            || (this.sessionId != null && thatObj.getSessionId() == null)
            || !this.sessionId.equals(thatObj.getSessionId()) ) {
            return false;
        }
        if( (this.username == null && thatObj.getUsername() != null)
            || (this.username != null && thatObj.getUsername() == null)
            || !this.username.equals(thatObj.getUsername()) ) {
            return false;
        }
        if( (this.nickname == null && thatObj.getNickname() != null)
            || (this.nickname != null && thatObj.getNickname() == null)
            || !this.nickname.equals(thatObj.getNickname()) ) {
            return false;
        }
        if( (this.avatar == null && thatObj.getAvatar() != null)
            || (this.avatar != null && thatObj.getAvatar() == null)
            || !this.avatar.equals(thatObj.getAvatar()) ) {
            return false;
        }
        if( (this.email == null && thatObj.getEmail() != null)
            || (this.email != null && thatObj.getEmail() == null)
            || !this.email.equals(thatObj.getEmail()) ) {
            return false;
        }
        if( (this.openId == null && thatObj.getOpenId() != null)
            || (this.openId != null && thatObj.getOpenId() == null)
            || !this.openId.equals(thatObj.getOpenId()) ) {
            return false;
        }
        if( (this.gaeUser == null && thatObj.getGaeUser() != null)
            || (this.gaeUser != null && thatObj.getGaeUser() == null)
            || !this.gaeUser.equals(thatObj.getGaeUser()) ) {
            return false;
        }
        if( (this.timeZone == null && thatObj.getTimeZone() != null)
            || (this.timeZone != null && thatObj.getTimeZone() == null)
            || !this.timeZone.equals(thatObj.getTimeZone()) ) {
            return false;
        }
        if( (this.address == null && thatObj.getAddress() != null)
            || (this.address != null && thatObj.getAddress() == null)
            || !this.address.equals(thatObj.getAddress()) ) {
            return false;
        }
        if( (this.location == null && thatObj.getLocation() != null)
            || (this.location != null && thatObj.getLocation() == null)
            || !this.location.equals(thatObj.getLocation()) ) {
            return false;
        }
        if( (this.ipAddress == null && thatObj.getIpAddress() != null)
            || (this.ipAddress != null && thatObj.getIpAddress() == null)
            || !this.ipAddress.equals(thatObj.getIpAddress()) ) {
            return false;
        }
        if( (this.referer == null && thatObj.getReferer() != null)
            || (this.referer != null && thatObj.getReferer() == null)
            || !this.referer.equals(thatObj.getReferer()) ) {
            return false;
        }
        if( (this.obsolete == null && thatObj.isObsolete() != null)
            || (this.obsolete != null && thatObj.isObsolete() == null)
            || !this.obsolete.equals(thatObj.isObsolete()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.emailVerifiedTime == null && thatObj.getEmailVerifiedTime() != null)
            || (this.emailVerifiedTime != null && thatObj.getEmailVerifiedTime() == null)
            || !this.emailVerifiedTime.equals(thatObj.getEmailVerifiedTime()) ) {
            return false;
        }
        if( (this.openIdVerifiedTime == null && thatObj.getOpenIdVerifiedTime() != null)
            || (this.openIdVerifiedTime != null && thatObj.getOpenIdVerifiedTime() == null)
            || !this.openIdVerifiedTime.equals(thatObj.getOpenIdVerifiedTime()) ) {
            return false;
        }
        if( (this.authenticatedTime == null && thatObj.getAuthenticatedTime() != null)
            || (this.authenticatedTime != null && thatObj.getAuthenticatedTime() == null)
            || !this.authenticatedTime.equals(thatObj.getAuthenticatedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = aeryId == null ? 0 : aeryId.hashCode();
        _hash = 31 * _hash + delta;
        delta = sessionId == null ? 0 : sessionId.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = nickname == null ? 0 : nickname.hashCode();
        _hash = 31 * _hash + delta;
        delta = avatar == null ? 0 : avatar.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = openId == null ? 0 : openId.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeUser == null ? 0 : gaeUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = timeZone == null ? 0 : timeZone.hashCode();
        _hash = 31 * _hash + delta;
        delta = address == null ? 0 : address.hashCode();
        _hash = 31 * _hash + delta;
        delta = location == null ? 0 : location.hashCode();
        _hash = 31 * _hash + delta;
        delta = ipAddress == null ? 0 : ipAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = referer == null ? 0 : referer.hashCode();
        _hash = 31 * _hash + delta;
        delta = obsolete == null ? 0 : obsolete.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = emailVerifiedTime == null ? 0 : emailVerifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = openIdVerifiedTime == null ? 0 : openIdVerifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = authenticatedTime == null ? 0 : authenticatedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
