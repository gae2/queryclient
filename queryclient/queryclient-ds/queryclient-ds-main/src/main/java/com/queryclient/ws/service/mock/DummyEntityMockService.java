package com.queryclient.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.bean.DummyEntityBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.DummyEntityDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.DummyEntityService;


// DummyEntityMockService is a decorator.
// It can be used as a base class to mock DummyEntityService objects.
public abstract class DummyEntityMockService implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(DummyEntityMockService.class.getName());

    // DummyEntityMockService uses the decorator design pattern.
    private DummyEntityService decoratedService;

    public DummyEntityMockService(DummyEntityService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected DummyEntityService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(DummyEntityService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // DummyEntity related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DummyEntity getDummyEntity(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDummyEntity(): guid = " + guid);
        DummyEntity bean = decoratedService.getDummyEntity(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getDummyEntity(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getDummyEntity(guid, field);
        return obj;
    }

    @Override
    public List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException
    {
        log.fine("getDummyEntities()");
        List<DummyEntity> dummyEntities = decoratedService.getDummyEntities(guids);
        log.finer("END");
        return dummyEntities;
    }

    @Override
    public List<DummyEntity> getAllDummyEntities() throws BaseException
    {
        return getAllDummyEntities(null, null, null);
    }


    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDummyEntities(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<DummyEntity> dummyEntities = decoratedService.getAllDummyEntities(ordering, offset, count, forwardCursor);
        log.finer("END");
        return dummyEntities;
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDummyEntityKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityMockService.findDummyEntities(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<DummyEntity> dummyEntities = decoratedService.findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return dummyEntities;
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityMockService.findDummyEntityKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        return decoratedService.createDummyEntity(user, name, content, maxLength, expired, status);
    }

    @Override
    public String createDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createDummyEntity(dummyEntity);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        return decoratedService.updateDummyEntity(guid, user, name, content, maxLength, expired, status);
    }
        
    @Override
    public Boolean updateDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateDummyEntity(dummyEntity);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDummyEntity(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDummyEntity(dummyEntity);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteDummyEntities(filter, params, values);
        return count;
    }

}
