package com.queryclient.ws.resource.exception;

import com.queryclient.ws.exception.resource.BaseResourceException;


public class RequestForbiddenRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public RequestForbiddenRsException() 
    {
        super();
    }
    public RequestForbiddenRsException(String message) 
    {
        super(message);
    }
    public RequestForbiddenRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public RequestForbiddenRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public RequestForbiddenRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public RequestForbiddenRsException(Throwable cause) 
    {
        super(cause);
    }
    public RequestForbiddenRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
