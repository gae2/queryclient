package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.KeyValueRelationStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "keyValueRelationStructs")
@XmlType(propOrder = {"keyValueRelationStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeyValueRelationStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeyValueRelationStructListStub.class.getName());

    private List<KeyValueRelationStructStub> keyValueRelationStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public KeyValueRelationStructListStub()
    {
        this(new ArrayList<KeyValueRelationStructStub>());
    }
    public KeyValueRelationStructListStub(List<KeyValueRelationStructStub> keyValueRelationStructs)
    {
        this(keyValueRelationStructs, null);
    }
    public KeyValueRelationStructListStub(List<KeyValueRelationStructStub> keyValueRelationStructs, String forwardCursor)
    {
        this.keyValueRelationStructs = keyValueRelationStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(keyValueRelationStructs == null) {
            return true;
        } else {
            return keyValueRelationStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(keyValueRelationStructs == null) {
            return 0;
        } else {
            return keyValueRelationStructs.size();
        }
    }


    @XmlElement(name = "keyValueRelationStruct")
    public List<KeyValueRelationStructStub> getKeyValueRelationStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<KeyValueRelationStructStub> getList()
    {
        return keyValueRelationStructs;
    }
    public void setList(List<KeyValueRelationStructStub> keyValueRelationStructs)
    {
        this.keyValueRelationStructs = keyValueRelationStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<KeyValueRelationStructStub> it = this.keyValueRelationStructs.iterator();
        while(it.hasNext()) {
            KeyValueRelationStructStub keyValueRelationStruct = it.next();
            sb.append(keyValueRelationStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static KeyValueRelationStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of KeyValueRelationStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write KeyValueRelationStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write KeyValueRelationStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write KeyValueRelationStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static KeyValueRelationStructListStub fromJsonString(String jsonStr)
    {
        try {
            KeyValueRelationStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, KeyValueRelationStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into KeyValueRelationStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into KeyValueRelationStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into KeyValueRelationStructListStub object.", e);
        }
        
        return null;
    }

}
