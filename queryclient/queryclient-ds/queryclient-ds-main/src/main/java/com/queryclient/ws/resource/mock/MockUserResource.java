package com.queryclient.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.User;
import com.queryclient.ws.bean.UserBean;
import com.queryclient.ws.stub.UserListStub;
import com.queryclient.ws.stub.UserStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.UserResource;
import com.queryclient.ws.resource.util.GaeAppStructResourceUtil;
import com.queryclient.ws.resource.util.GaeUserStructResourceUtil;

// MockUserResource is a decorator.
// It can be used as a base class to mock UserResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/users/")
public abstract class MockUserResource implements UserResource
{
    private static final Logger log = Logger.getLogger(MockUserResource.class.getName());

    // MockUserResource uses the decorator design pattern.
    private UserResource decoratedResource;

    public MockUserResource(UserResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected UserResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(UserResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUsers(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUserKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getUserKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getUserKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getUser(String guid) throws BaseResourceException
    {
        return decoratedResource.getUser(guid);
    }

    @Override
    public Response getUser(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getUser(guid, field);
    }

    @Override
    public Response createUser(UserStub user) throws BaseResourceException
    {
        return decoratedResource.createUser(user);
    }

    @Override
    public Response updateUser(String guid, UserStub user) throws BaseResourceException
    {
        return decoratedResource.updateUser(guid, user);
    }

    @Override
    public Response updateUser(String guid, String managerApp, Long appAcl, String gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, String gaeUser, String timeZone, String address, String location, String ipAddress, String referer, Boolean obsolete, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseResourceException
    {
        return decoratedResource.updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, address, location, ipAddress, referer, obsolete, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public Response deleteUser(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteUser(guid);
    }

    @Override
    public Response deleteUsers(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteUsers(filter, params, values);
    }


}
