package com.queryclient.ws;



public interface QueryRecord 
{
    String  getGuid();
    String  getQuerySession();
    Integer  getQueryId();
    String  getDataService();
    String  getServiceUrl();
    Boolean  isDelayed();
    String  getQuery();
    String  getInputFormat();
    String  getInputFile();
    String  getInputContent();
    String  getTargetOutputFormat();
    String  getOutputFormat();
    String  getOutputFile();
    String  getOutputContent();
    Integer  getResponseCode();
    String  getResult();
    ReferrerInfoStruct  getReferrerInfo();
    String  getStatus();
    String  getExtra();
    String  getNote();
    Long  getScheduledTime();
    Long  getProcessedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
