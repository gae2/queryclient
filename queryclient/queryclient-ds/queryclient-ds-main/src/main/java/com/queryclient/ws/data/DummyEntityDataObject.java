package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class DummyEntityDataObject extends KeyedDataObject implements DummyEntity
{
    private static final Logger log = Logger.getLogger(DummyEntityDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(DummyEntityDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(DummyEntityDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String name;

    @Persistent(defaultFetchGroup = "false")
    private Text content;

    @Persistent(defaultFetchGroup = "true")
    private Integer maxLength;

    @Persistent(defaultFetchGroup = "true")
    private Boolean expired;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public DummyEntityDataObject()
    {
        this(null);
    }
    public DummyEntityDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public DummyEntityDataObject(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status)
    {
        this(guid, user, name, content, maxLength, expired, status, null, null);
    }
    public DummyEntityDataObject(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.name = name;
        setContent(content);
        this.maxLength = maxLength;
        this.expired = expired;
        this.status = status;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return DummyEntityDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return DummyEntityDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getContent()
    {
        if(this.content == null) {
            return null;
        }    
        return this.content.getValue();
    }
    public void setContent(String content)
    {
        if(content == null) {
            this.content = null;
        } else {
            this.content = new Text(content);
        }
    }

    public Integer getMaxLength()
    {
        return this.maxLength;
    }
    public void setMaxLength(Integer maxLength)
    {
        this.maxLength = maxLength;
    }

    public Boolean isExpired()
    {
        return this.expired;
    }
    public void setExpired(Boolean expired)
    {
        this.expired = expired;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("name", this.name);
        dataMap.put("content", this.content);
        dataMap.put("maxLength", this.maxLength);
        dataMap.put("expired", this.expired);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        DummyEntity thatObj = (DummyEntity) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.name == null && thatObj.getName() != null)
            || (this.name != null && thatObj.getName() == null)
            || !this.name.equals(thatObj.getName()) ) {
            return false;
        }
        if( (this.content == null && thatObj.getContent() != null)
            || (this.content != null && thatObj.getContent() == null)
            || !this.content.equals(thatObj.getContent()) ) {
            return false;
        }
        if( (this.maxLength == null && thatObj.getMaxLength() != null)
            || (this.maxLength != null && thatObj.getMaxLength() == null)
            || !this.maxLength.equals(thatObj.getMaxLength()) ) {
            return false;
        }
        if( (this.expired == null && thatObj.isExpired() != null)
            || (this.expired != null && thatObj.isExpired() == null)
            || !this.expired.equals(thatObj.isExpired()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = name == null ? 0 : name.hashCode();
        _hash = 31 * _hash + delta;
        delta = content == null ? 0 : content.hashCode();
        _hash = 31 * _hash + delta;
        delta = maxLength == null ? 0 : maxLength.hashCode();
        _hash = 31 * _hash + delta;
        delta = expired == null ? 0 : expired.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
