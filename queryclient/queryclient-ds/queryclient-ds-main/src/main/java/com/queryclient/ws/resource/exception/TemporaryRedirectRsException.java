package com.queryclient.ws.resource.exception;

import com.queryclient.ws.exception.resource.BaseResourceException;


public class TemporaryRedirectRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public TemporaryRedirectRsException() 
    {
        super();
    }
    public TemporaryRedirectRsException(String message) 
    {
        super(message);
    }
    public TemporaryRedirectRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public TemporaryRedirectRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public TemporaryRedirectRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public TemporaryRedirectRsException(Throwable cause) 
    {
        super(cause);
    }
    public TemporaryRedirectRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
