package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class UserPasswordDataObject extends KeyedDataObject implements UserPassword
{
    private static final Logger log = Logger.getLogger(UserPasswordDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(UserPasswordDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(UserPasswordDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String managerApp;

    @Persistent(defaultFetchGroup = "true")
    private Long appAcl;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="groupId", columns=@Column(name="gaeAppgroupId")),
        @Persistent(name="appId", columns=@Column(name="gaeAppappId")),
        @Persistent(name="appDomain", columns=@Column(name="gaeAppappDomain")),
        @Persistent(name="namespace", columns=@Column(name="gaeAppnamespace")),
        @Persistent(name="acl", columns=@Column(name="gaeAppacl")),
        @Persistent(name="note", columns=@Column(name="gaeAppnote")),
    })
    private GaeAppStructDataObject gaeApp;

    @Persistent(defaultFetchGroup = "true")
    private String ownerUser;

    @Persistent(defaultFetchGroup = "true")
    private Long userAcl;

    @Persistent(defaultFetchGroup = "true")
    private String admin;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String username;

    @Persistent(defaultFetchGroup = "true")
    private String email;

    @Persistent(defaultFetchGroup = "true")
    private String openId;

    @Persistent(defaultFetchGroup = "true")
    private String plainPassword;

    @Persistent(defaultFetchGroup = "true")
    private String hashedPassword;

    @Persistent(defaultFetchGroup = "true")
    private String salt;

    @Persistent(defaultFetchGroup = "true")
    private String hashMethod;

    @Persistent(defaultFetchGroup = "true")
    private Boolean resetRequired;

    @Persistent(defaultFetchGroup = "true")
    private String challengeQuestion;

    @Persistent(defaultFetchGroup = "true")
    private String challengeAnswer;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Long lastResetTime;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    public UserPasswordDataObject()
    {
        this(null);
    }
    public UserPasswordDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserPasswordDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime, null, null);
    }
    public UserPasswordDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        if(gaeApp != null) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = null;
        }
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.admin = admin;
        this.user = user;
        this.username = username;
        this.email = email;
        this.openId = openId;
        this.plainPassword = plainPassword;
        this.hashedPassword = hashedPassword;
        this.salt = salt;
        this.hashMethod = hashMethod;
        this.resetRequired = resetRequired;
        this.challengeQuestion = challengeQuestion;
        this.challengeAnswer = challengeAnswer;
        this.status = status;
        this.lastResetTime = lastResetTime;
        this.expirationTime = expirationTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return UserPasswordDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return UserPasswordDataObject.composeKey(getGuid());
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStruct getGaeApp()
    {
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(gaeApp == null) {
            this.gaeApp = null;
            log.log(Level.INFO, "UserPasswordDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is null.");            
        } else if(gaeApp instanceof GaeAppStructDataObject) {
            this.gaeApp = (GaeAppStructDataObject) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = new GaeAppStructDataObject();   // ????
            log.log(Level.WARNING, "UserPasswordDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is of an invalid type.");
        }
    }

    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    public String getAdmin()
    {
        return this.admin;
    }
    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getOpenId()
    {
        return this.openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    public String getPlainPassword()
    {
        return this.plainPassword;
    }
    public void setPlainPassword(String plainPassword)
    {
        this.plainPassword = plainPassword;
    }

    public String getHashedPassword()
    {
        return this.hashedPassword;
    }
    public void setHashedPassword(String hashedPassword)
    {
        this.hashedPassword = hashedPassword;
    }

    public String getSalt()
    {
        return this.salt;
    }
    public void setSalt(String salt)
    {
        this.salt = salt;
    }

    public String getHashMethod()
    {
        return this.hashMethod;
    }
    public void setHashMethod(String hashMethod)
    {
        this.hashMethod = hashMethod;
    }

    public Boolean isResetRequired()
    {
        return this.resetRequired;
    }
    public void setResetRequired(Boolean resetRequired)
    {
        this.resetRequired = resetRequired;
    }

    public String getChallengeQuestion()
    {
        return this.challengeQuestion;
    }
    public void setChallengeQuestion(String challengeQuestion)
    {
        this.challengeQuestion = challengeQuestion;
    }

    public String getChallengeAnswer()
    {
        return this.challengeAnswer;
    }
    public void setChallengeAnswer(String challengeAnswer)
    {
        this.challengeAnswer = challengeAnswer;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getLastResetTime()
    {
        return this.lastResetTime;
    }
    public void setLastResetTime(Long lastResetTime)
    {
        this.lastResetTime = lastResetTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("ownerUser", this.ownerUser);
        dataMap.put("userAcl", this.userAcl);
        dataMap.put("admin", this.admin);
        dataMap.put("user", this.user);
        dataMap.put("username", this.username);
        dataMap.put("email", this.email);
        dataMap.put("openId", this.openId);
        dataMap.put("plainPassword", this.plainPassword);
        dataMap.put("hashedPassword", this.hashedPassword);
        dataMap.put("salt", this.salt);
        dataMap.put("hashMethod", this.hashMethod);
        dataMap.put("resetRequired", this.resetRequired);
        dataMap.put("challengeQuestion", this.challengeQuestion);
        dataMap.put("challengeAnswer", this.challengeAnswer);
        dataMap.put("status", this.status);
        dataMap.put("lastResetTime", this.lastResetTime);
        dataMap.put("expirationTime", this.expirationTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        UserPassword thatObj = (UserPassword) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.managerApp == null && thatObj.getManagerApp() != null)
            || (this.managerApp != null && thatObj.getManagerApp() == null)
            || !this.managerApp.equals(thatObj.getManagerApp()) ) {
            return false;
        }
        if( (this.appAcl == null && thatObj.getAppAcl() != null)
            || (this.appAcl != null && thatObj.getAppAcl() == null)
            || !this.appAcl.equals(thatObj.getAppAcl()) ) {
            return false;
        }
        if( (this.gaeApp == null && thatObj.getGaeApp() != null)
            || (this.gaeApp != null && thatObj.getGaeApp() == null)
            || !this.gaeApp.equals(thatObj.getGaeApp()) ) {
            return false;
        }
        if( (this.ownerUser == null && thatObj.getOwnerUser() != null)
            || (this.ownerUser != null && thatObj.getOwnerUser() == null)
            || !this.ownerUser.equals(thatObj.getOwnerUser()) ) {
            return false;
        }
        if( (this.userAcl == null && thatObj.getUserAcl() != null)
            || (this.userAcl != null && thatObj.getUserAcl() == null)
            || !this.userAcl.equals(thatObj.getUserAcl()) ) {
            return false;
        }
        if( (this.admin == null && thatObj.getAdmin() != null)
            || (this.admin != null && thatObj.getAdmin() == null)
            || !this.admin.equals(thatObj.getAdmin()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.username == null && thatObj.getUsername() != null)
            || (this.username != null && thatObj.getUsername() == null)
            || !this.username.equals(thatObj.getUsername()) ) {
            return false;
        }
        if( (this.email == null && thatObj.getEmail() != null)
            || (this.email != null && thatObj.getEmail() == null)
            || !this.email.equals(thatObj.getEmail()) ) {
            return false;
        }
        if( (this.openId == null && thatObj.getOpenId() != null)
            || (this.openId != null && thatObj.getOpenId() == null)
            || !this.openId.equals(thatObj.getOpenId()) ) {
            return false;
        }
        if( (this.plainPassword == null && thatObj.getPlainPassword() != null)
            || (this.plainPassword != null && thatObj.getPlainPassword() == null)
            || !this.plainPassword.equals(thatObj.getPlainPassword()) ) {
            return false;
        }
        if( (this.hashedPassword == null && thatObj.getHashedPassword() != null)
            || (this.hashedPassword != null && thatObj.getHashedPassword() == null)
            || !this.hashedPassword.equals(thatObj.getHashedPassword()) ) {
            return false;
        }
        if( (this.salt == null && thatObj.getSalt() != null)
            || (this.salt != null && thatObj.getSalt() == null)
            || !this.salt.equals(thatObj.getSalt()) ) {
            return false;
        }
        if( (this.hashMethod == null && thatObj.getHashMethod() != null)
            || (this.hashMethod != null && thatObj.getHashMethod() == null)
            || !this.hashMethod.equals(thatObj.getHashMethod()) ) {
            return false;
        }
        if( (this.resetRequired == null && thatObj.isResetRequired() != null)
            || (this.resetRequired != null && thatObj.isResetRequired() == null)
            || !this.resetRequired.equals(thatObj.isResetRequired()) ) {
            return false;
        }
        if( (this.challengeQuestion == null && thatObj.getChallengeQuestion() != null)
            || (this.challengeQuestion != null && thatObj.getChallengeQuestion() == null)
            || !this.challengeQuestion.equals(thatObj.getChallengeQuestion()) ) {
            return false;
        }
        if( (this.challengeAnswer == null && thatObj.getChallengeAnswer() != null)
            || (this.challengeAnswer != null && thatObj.getChallengeAnswer() == null)
            || !this.challengeAnswer.equals(thatObj.getChallengeAnswer()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.lastResetTime == null && thatObj.getLastResetTime() != null)
            || (this.lastResetTime != null && thatObj.getLastResetTime() == null)
            || !this.lastResetTime.equals(thatObj.getLastResetTime()) ) {
            return false;
        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = ownerUser == null ? 0 : ownerUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAcl == null ? 0 : userAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = admin == null ? 0 : admin.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = openId == null ? 0 : openId.hashCode();
        _hash = 31 * _hash + delta;
        delta = plainPassword == null ? 0 : plainPassword.hashCode();
        _hash = 31 * _hash + delta;
        delta = hashedPassword == null ? 0 : hashedPassword.hashCode();
        _hash = 31 * _hash + delta;
        delta = salt == null ? 0 : salt.hashCode();
        _hash = 31 * _hash + delta;
        delta = hashMethod == null ? 0 : hashMethod.hashCode();
        _hash = 31 * _hash + delta;
        delta = resetRequired == null ? 0 : resetRequired.hashCode();
        _hash = 31 * _hash + delta;
        delta = challengeQuestion == null ? 0 : challengeQuestion.hashCode();
        _hash = 31 * _hash + delta;
        delta = challengeAnswer == null ? 0 : challengeAnswer.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastResetTime == null ? 0 : lastResetTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
