package com.queryclient.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.bean.QuerySessionBean;
import com.queryclient.ws.stub.QuerySessionListStub;
import com.queryclient.ws.stub.QuerySessionStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.QuerySessionResource;
import com.queryclient.ws.resource.util.ReferrerInfoStructResourceUtil;

// MockQuerySessionResource is a decorator.
// It can be used as a base class to mock QuerySessionResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/querySessions/")
public abstract class MockQuerySessionResource implements QuerySessionResource
{
    private static final Logger log = Logger.getLogger(MockQuerySessionResource.class.getName());

    // MockQuerySessionResource uses the decorator design pattern.
    private QuerySessionResource decoratedResource;

    public MockQuerySessionResource(QuerySessionResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected QuerySessionResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(QuerySessionResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllQuerySessions(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getQuerySessionKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getQuerySessionKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getQuerySession(String guid) throws BaseResourceException
    {
        return decoratedResource.getQuerySession(guid);
    }

    @Override
    public Response getQuerySession(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getQuerySession(guid, field);
    }

    @Override
    public Response createQuerySession(QuerySessionStub querySession) throws BaseResourceException
    {
        return decoratedResource.createQuerySession(querySession);
    }

    @Override
    public Response updateQuerySession(String guid, QuerySessionStub querySession) throws BaseResourceException
    {
        return decoratedResource.updateQuerySession(guid, querySession);
    }

    @Override
    public Response updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, String referrerInfo, String status, String note) throws BaseResourceException
    {
        return decoratedResource.updateQuerySession(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note);
    }

    @Override
    public Response deleteQuerySession(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteQuerySession(guid);
    }

    @Override
    public Response deleteQuerySessions(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteQuerySessions(filter, params, values);
    }


}
