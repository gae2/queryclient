package com.queryclient.ws.search.core;

import java.util.logging.Logger;
import java.util.logging.Level;

// import com.google.appengine.api.search.Cursor;


// TBD:
// Not specific to search (in the current implementation).
// Just use core.StringCursor...
public class StringQueryCursor
{
    private static final Logger log = Logger.getLogger(StringQueryCursor.class.getName());

    private String webSafeString = null;


    public StringQueryCursor(String webSafeString)
    {
        this.webSafeString = webSafeString;
    }


    public boolean isEmpty()
    {
        if(webSafeString == null || webSafeString.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public String getWebSafeString()
    {
        return webSafeString;
    }
    public void setWebSafeString(String currentWebSafeString)
    {
        this.webSafeString = currentWebSafeString;
    }


}
