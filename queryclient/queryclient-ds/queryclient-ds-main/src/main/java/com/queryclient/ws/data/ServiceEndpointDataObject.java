package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class ServiceEndpointDataObject extends KeyedDataObject implements ServiceEndpoint
{
    private static final Logger log = Logger.getLogger(ServiceEndpointDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ServiceEndpointDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ServiceEndpointDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String dataService;

    @Persistent(defaultFetchGroup = "true")
    private String serviceName;

    @Persistent(defaultFetchGroup = "true")
    private String serviceUrl;

    @Persistent(defaultFetchGroup = "true")
    private Boolean authRequired;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="consumerKey", columns=@Column(name="authCredentialconsumerKey")),
        @Persistent(name="consumerSecret", columns=@Column(name="authCredentialconsumerSecret")),
    })
    private ConsumerKeySecretPairDataObject authCredential;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public ServiceEndpointDataObject()
    {
        this(null);
    }
    public ServiceEndpointDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public ServiceEndpointDataObject(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status)
    {
        this(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredential, status, null, null);
    }
    public ServiceEndpointDataObject(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.dataService = dataService;
        this.serviceName = serviceName;
        this.serviceUrl = serviceUrl;
        this.authRequired = authRequired;
        if(authCredential != null) {
            this.authCredential = new ConsumerKeySecretPairDataObject(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            this.authCredential = null;
        }
        this.status = status;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return ServiceEndpointDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ServiceEndpointDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getDataService()
    {
        return this.dataService;
    }
    public void setDataService(String dataService)
    {
        this.dataService = dataService;
    }

    public String getServiceName()
    {
        return this.serviceName;
    }
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    public Boolean isAuthRequired()
    {
        return this.authRequired;
    }
    public void setAuthRequired(Boolean authRequired)
    {
        this.authRequired = authRequired;
    }

    public ConsumerKeySecretPair getAuthCredential()
    {
        return this.authCredential;
    }
    public void setAuthCredential(ConsumerKeySecretPair authCredential)
    {
        if(authCredential == null) {
            this.authCredential = null;
            log.log(Level.INFO, "ServiceEndpointDataObject.setAuthCredential(ConsumerKeySecretPair authCredential): Arg authCredential is null.");            
        } else if(authCredential instanceof ConsumerKeySecretPairDataObject) {
            this.authCredential = (ConsumerKeySecretPairDataObject) authCredential;
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            this.authCredential = new ConsumerKeySecretPairDataObject(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            this.authCredential = new ConsumerKeySecretPairDataObject();   // ????
            log.log(Level.WARNING, "ServiceEndpointDataObject.setAuthCredential(ConsumerKeySecretPair authCredential): Arg authCredential is of an invalid type.");
        }
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("dataService", this.dataService);
        dataMap.put("serviceName", this.serviceName);
        dataMap.put("serviceUrl", this.serviceUrl);
        dataMap.put("authRequired", this.authRequired);
        dataMap.put("authCredential", this.authCredential);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ServiceEndpoint thatObj = (ServiceEndpoint) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.dataService == null && thatObj.getDataService() != null)
            || (this.dataService != null && thatObj.getDataService() == null)
            || !this.dataService.equals(thatObj.getDataService()) ) {
            return false;
        }
        if( (this.serviceName == null && thatObj.getServiceName() != null)
            || (this.serviceName != null && thatObj.getServiceName() == null)
            || !this.serviceName.equals(thatObj.getServiceName()) ) {
            return false;
        }
        if( (this.serviceUrl == null && thatObj.getServiceUrl() != null)
            || (this.serviceUrl != null && thatObj.getServiceUrl() == null)
            || !this.serviceUrl.equals(thatObj.getServiceUrl()) ) {
            return false;
        }
        if( (this.authRequired == null && thatObj.isAuthRequired() != null)
            || (this.authRequired != null && thatObj.isAuthRequired() == null)
            || !this.authRequired.equals(thatObj.isAuthRequired()) ) {
            return false;
        }
        if( (this.authCredential == null && thatObj.getAuthCredential() != null)
            || (this.authCredential != null && thatObj.getAuthCredential() == null)
            || !this.authCredential.equals(thatObj.getAuthCredential()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = dataService == null ? 0 : dataService.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceName == null ? 0 : serviceName.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = authRequired == null ? 0 : authRequired.hashCode();
        _hash = 31 * _hash + delta;
        delta = authCredential == null ? 0 : authCredential.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
