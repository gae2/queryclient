package com.queryclient.ws.dao;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.data.ServiceEndpointDataObject;


// TBD: Add offset/count to getAllServiceEndpoints() and findServiceEndpoints(), etc.
public interface ServiceEndpointDAO
{
    ServiceEndpointDataObject getServiceEndpoint(String guid) throws BaseException;
    List<ServiceEndpointDataObject> getServiceEndpoints(List<String> guids) throws BaseException;
    List<ServiceEndpointDataObject> getAllServiceEndpoints() throws BaseException;
    /* @Deprecated */ List<ServiceEndpointDataObject> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException;
    List<ServiceEndpointDataObject> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<ServiceEndpointDataObject> findServiceEndpoints(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<ServiceEndpointDataObject> findServiceEndpoints(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<ServiceEndpointDataObject> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<ServiceEndpointDataObject> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createServiceEndpoint(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ServiceEndpointDataObject?)
    String createServiceEndpoint(ServiceEndpointDataObject serviceEndpoint) throws BaseException;          // Returns Guid.  (Return ServiceEndpointDataObject?)
    //Boolean updateServiceEndpoint(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateServiceEndpoint(ServiceEndpointDataObject serviceEndpoint) throws BaseException;
    Boolean deleteServiceEndpoint(String guid) throws BaseException;
    Boolean deleteServiceEndpoint(ServiceEndpointDataObject serviceEndpoint) throws BaseException;
    Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException;
}
