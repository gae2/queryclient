package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.ServiceEndpointDAO;
import com.queryclient.ws.data.ServiceEndpointDataObject;


// MockServiceEndpointDAO is a decorator.
// It can be used as a base class to mock ServiceEndpointDAO objects.
public abstract class MockServiceEndpointDAO implements ServiceEndpointDAO
{
    private static final Logger log = Logger.getLogger(MockServiceEndpointDAO.class.getName()); 

    // MockServiceEndpointDAO uses the decorator design pattern.
    private ServiceEndpointDAO decoratedDAO;

    public MockServiceEndpointDAO(ServiceEndpointDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected ServiceEndpointDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(ServiceEndpointDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public ServiceEndpointDataObject getServiceEndpoint(String guid) throws BaseException
    {
        return decoratedDAO.getServiceEndpoint(guid);
	}

    @Override
    public List<ServiceEndpointDataObject> getServiceEndpoints(List<String> guids) throws BaseException
    {
        return decoratedDAO.getServiceEndpoints(guids);
    }

    @Override
    public List<ServiceEndpointDataObject> getAllServiceEndpoints() throws BaseException
	{
	    return getAllServiceEndpoints(null, null, null);
    }


    @Override
    public List<ServiceEndpointDataObject> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllServiceEndpoints(ordering, offset, count, null);
    }

    @Override
    public List<ServiceEndpointDataObject> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllServiceEndpoints(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<ServiceEndpointDataObject> findServiceEndpoints(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findServiceEndpoints(filter, ordering, params, values, null, null);
    }

    @Override
	public List<ServiceEndpointDataObject> findServiceEndpoints(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findServiceEndpoints(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<ServiceEndpointDataObject> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<ServiceEndpointDataObject> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createServiceEndpoint(ServiceEndpointDataObject serviceEndpoint) throws BaseException
    {
        return decoratedDAO.createServiceEndpoint( serviceEndpoint);
    }

    @Override
	public Boolean updateServiceEndpoint(ServiceEndpointDataObject serviceEndpoint) throws BaseException
	{
        return decoratedDAO.updateServiceEndpoint(serviceEndpoint);
	}
	
    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpointDataObject serviceEndpoint) throws BaseException
    {
        return decoratedDAO.deleteServiceEndpoint(serviceEndpoint);
    }

    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        return decoratedDAO.deleteServiceEndpoint(guid);
	}

    @Override
    public Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteServiceEndpoints(filter, params, values);
    }

}
