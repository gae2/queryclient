package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "dummyEntities")
@XmlType(propOrder = {"dummyEntity", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DummyEntityListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DummyEntityListStub.class.getName());

    private List<DummyEntityStub> dummyEntities = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public DummyEntityListStub()
    {
        this(new ArrayList<DummyEntityStub>());
    }
    public DummyEntityListStub(List<DummyEntityStub> dummyEntities)
    {
        this(dummyEntities, null);
    }
    public DummyEntityListStub(List<DummyEntityStub> dummyEntities, String forwardCursor)
    {
        this.dummyEntities = dummyEntities;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(dummyEntities == null) {
            return true;
        } else {
            return dummyEntities.isEmpty();
        }
    }
    public int getSize()
    {
        if(dummyEntities == null) {
            return 0;
        } else {
            return dummyEntities.size();
        }
    }


    @XmlElement(name = "dummyEntity")
    public List<DummyEntityStub> getDummyEntity()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<DummyEntityStub> getList()
    {
        return dummyEntities;
    }
    public void setList(List<DummyEntityStub> dummyEntities)
    {
        this.dummyEntities = dummyEntities;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<DummyEntityStub> it = this.dummyEntities.iterator();
        while(it.hasNext()) {
            DummyEntityStub dummyEntity = it.next();
            sb.append(dummyEntity.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static DummyEntityListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of DummyEntityListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write DummyEntityListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write DummyEntityListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write DummyEntityListStub object as a string.", e);
        }
        
        return null;
    }
    public static DummyEntityListStub fromJsonString(String jsonStr)
    {
        try {
            DummyEntityListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, DummyEntityListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into DummyEntityListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into DummyEntityListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into DummyEntityListStub object.", e);
        }
        
        return null;
    }

}
