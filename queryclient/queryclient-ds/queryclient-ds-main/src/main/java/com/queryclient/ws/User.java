package com.queryclient.ws;



public interface User 
{
    String  getGuid();
    String  getManagerApp();
    Long  getAppAcl();
    GaeAppStruct  getGaeApp();
    String  getAeryId();
    String  getSessionId();
    String  getUsername();
    String  getNickname();
    String  getAvatar();
    String  getEmail();
    String  getOpenId();
    GaeUserStruct  getGaeUser();
    String  getTimeZone();
    String  getAddress();
    String  getLocation();
    String  getIpAddress();
    String  getReferer();
    Boolean  isObsolete();
    String  getStatus();
    Long  getEmailVerifiedTime();
    Long  getOpenIdVerifiedTime();
    Long  getAuthenticatedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
