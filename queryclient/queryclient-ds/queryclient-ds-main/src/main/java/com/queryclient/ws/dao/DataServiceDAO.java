package com.queryclient.ws.dao;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.data.DataServiceDataObject;


// TBD: Add offset/count to getAllDataServices() and findDataServices(), etc.
public interface DataServiceDAO
{
    DataServiceDataObject getDataService(String guid) throws BaseException;
    List<DataServiceDataObject> getDataServices(List<String> guids) throws BaseException;
    List<DataServiceDataObject> getAllDataServices() throws BaseException;
    /* @Deprecated */ List<DataServiceDataObject> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException;
    List<DataServiceDataObject> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createDataService(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return DataServiceDataObject?)
    String createDataService(DataServiceDataObject dataService) throws BaseException;          // Returns Guid.  (Return DataServiceDataObject?)
    //Boolean updateDataService(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateDataService(DataServiceDataObject dataService) throws BaseException;
    Boolean deleteDataService(String guid) throws BaseException;
    Boolean deleteDataService(DataServiceDataObject dataService) throws BaseException;
    Long deleteDataServices(String filter, String params, List<String> values) throws BaseException;
}
