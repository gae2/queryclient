package com.queryclient.ws.resource.exception;

import com.queryclient.ws.exception.resource.BaseResourceException;


public class MethodNotAllowedRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public MethodNotAllowedRsException() 
    {
        super();
    }
    public MethodNotAllowedRsException(String message) 
    {
        super(message);
    }
    public MethodNotAllowedRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public MethodNotAllowedRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public MethodNotAllowedRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public MethodNotAllowedRsException(Throwable cause) 
    {
        super(cause);
    }
    public MethodNotAllowedRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
