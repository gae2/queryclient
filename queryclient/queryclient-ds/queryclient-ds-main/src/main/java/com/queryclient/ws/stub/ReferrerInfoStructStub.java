package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "referrerInfoStruct")
@XmlType(propOrder = {"referer", "userAgent", "language", "hostname", "ipAddress", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferrerInfoStructStub implements ReferrerInfoStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ReferrerInfoStructStub.class.getName());

    private String referer;
    private String userAgent;
    private String language;
    private String hostname;
    private String ipAddress;
    private String note;

    public ReferrerInfoStructStub()
    {
        this(null);
    }
    public ReferrerInfoStructStub(ReferrerInfoStruct bean)
    {
        if(bean != null) {
            this.referer = bean.getReferer();
            this.userAgent = bean.getUserAgent();
            this.language = bean.getLanguage();
            this.hostname = bean.getHostname();
            this.ipAddress = bean.getIpAddress();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getReferer()
    {
        return this.referer;
    }
    public void setReferer(String referer)
    {
        this.referer = referer;
    }

    @XmlElement
    public String getUserAgent()
    {
        return this.userAgent;
    }
    public void setUserAgent(String userAgent)
    {
        this.userAgent = userAgent;
    }

    @XmlElement
    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    @XmlElement
    public String getHostname()
    {
        return this.hostname;
    }
    public void setHostname(String hostname)
    {
        this.hostname = hostname;
    }

    @XmlElement
    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getReferer() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUserAgent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLanguage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHostname() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getIpAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("referer", this.referer);
        dataMap.put("userAgent", this.userAgent);
        dataMap.put("language", this.language);
        dataMap.put("hostname", this.hostname);
        dataMap.put("ipAddress", this.ipAddress);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = referer == null ? 0 : referer.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAgent == null ? 0 : userAgent.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = hostname == null ? 0 : hostname.hashCode();
        _hash = 31 * _hash + delta;
        delta = ipAddress == null ? 0 : ipAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ReferrerInfoStructStub convertBeanToStub(ReferrerInfoStruct bean)
    {
        ReferrerInfoStructStub stub = null;
        if(bean instanceof ReferrerInfoStructStub) {
            stub = (ReferrerInfoStructStub) bean;
        } else {
            if(bean != null) {
                stub = new ReferrerInfoStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ReferrerInfoStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of ReferrerInfoStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ReferrerInfoStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ReferrerInfoStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ReferrerInfoStructStub object as a string.", e);
        }
        
        return null;
    }
    public static ReferrerInfoStructStub fromJsonString(String jsonStr)
    {
        try {
            ReferrerInfoStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ReferrerInfoStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ReferrerInfoStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ReferrerInfoStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ReferrerInfoStructStub object.", e);
        }
        
        return null;
    }

}
