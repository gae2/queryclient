package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class FiveTenBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FiveTenBasePermission.class.getName());

    public FiveTenBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "FiveTen::" + action;
    }


}
