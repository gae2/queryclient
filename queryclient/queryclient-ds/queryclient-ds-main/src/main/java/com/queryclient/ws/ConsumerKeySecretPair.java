package com.queryclient.ws;



public interface ConsumerKeySecretPair 
{
    String  getConsumerKey();
    String  getConsumerSecret();
    boolean isEmpty();
}
