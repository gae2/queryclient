package com.queryclient.ws.cert.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.cert.OAuthConsumerInfo;


@PersistenceCapable(detachable="true")
public class OAuthConsumerInfoDataObject implements OAuthConsumerInfo
{
    private static final Logger log = Logger.getLogger(OAuthConsumerInfoDataObject.class.getName());

    public static Key composeKey(String guid)
    {
        Key key = KeyFactory.createKey(OAuthConsumerInfoDataObject.class.getSimpleName(), guid);
        return key; 
    }

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;

    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private Long createdTime;

    @Persistent(defaultFetchGroup = "true")
    private Long modifiedTime;

    @Persistent(defaultFetchGroup = "true")
    private String serviceName;

    @Persistent(defaultFetchGroup = "true")
    private String appId;

    @Persistent(defaultFetchGroup = "true")
    private String appUrl;

    @Persistent(defaultFetchGroup = "true")
    private String consumerKey;

    @Persistent(defaultFetchGroup = "true")
    private String consumerSecret;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    public OAuthConsumerInfoDataObject()
    {
        this(null);
    }
    public OAuthConsumerInfoDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public OAuthConsumerInfoDataObject(String guid, String serviceName, String appId, String appUrl, String consumerKey, String consumerSecret, String note, String status, Long expirationTime)
    {
        this(guid, serviceName, appId, appUrl, consumerKey, consumerSecret, note, status, expirationTime, null, null);
    }
    public OAuthConsumerInfoDataObject(String guid, String serviceName, String appId, String appUrl, String consumerKey, String consumerSecret, String note, String status, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        generatePK(guid);
    	this.createdTime = createdTime;
    	this.modifiedTime = modifiedTime;

        this.serviceName = serviceName;
        this.appId = appId;
        this.appUrl = appUrl;
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
        this.note = note;
        this.status = status;
        this.expirationTime = expirationTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

    protected Key createKey()
    {
        return OAuthConsumerInfoDataObject.composeKey(getGuid());
    }

    public Key getKey() 
    {
        return this.key;
    }
    private void resetKey(Key key)
    {
        this.key = key;
    }
    private void setKey(Key key)
    {
        // Key can be set only one????
        if(this.key == null) {
            resetKey(key);
        } else {
            log.info("Key is already set. New key will be ignored.");
        }
    }

    protected void rebuildKey()
    {
        resetKey(createKey());
    }
    protected void buildKey()
    {
        setKey(createKey());
    }

    public Long getCreatedTime() 
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime() 
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime) 
    {
        this.modifiedTime = modifiedTime;
    }

    public String getServiceName()
    {
        return this.serviceName;
    }
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getAppId()
    {
        return this.appId;
    }
    public void setAppId(String appId)
    {
        this.appId = appId;
    }

    public String getAppUrl()
    {
        return this.appUrl;
    }
    public void setAppUrl(String appUrl)
    {
        this.appUrl = appUrl;
    }

    public String getConsumerKey()
    {
        return this.consumerKey;
    }
    public void setConsumerKey(String consumerKey)
    {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret()
    {
        return this.consumerSecret;
    }
    public void setConsumerSecret(String consumerSecret)
    {
        this.consumerSecret = consumerSecret;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("key", this.key);
        dataMap.put("guid", this.guid);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);
        dataMap.put("serviceName", this.serviceName);
        dataMap.put("appId", this.appId);
        dataMap.put("appUrl", this.appUrl);
        dataMap.put("consumerKey", this.consumerKey);
        dataMap.put("consumerSecret", this.consumerSecret);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);
        dataMap.put("expirationTime", this.expirationTime);

        return dataMap;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        OAuthConsumerInfo thatObj = (OAuthConsumerInfo) obj;
//        if( (this.key == null && thatObj.getKey() != null)
//            || (this.key != null && thatObj.getKey() == null)
//            || !this.key.equals(thatObj.getKey()) ) {
//            return false;
//        }
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.serviceName == null && thatObj.getServiceName() != null)
            || (this.serviceName != null && thatObj.getServiceName() == null)
            || !this.serviceName.equals(thatObj.getServiceName()) ) {
            return false;
        }
        if( (this.appId == null && thatObj.getAppId() != null)
            || (this.appId != null && thatObj.getAppId() == null)
            || !this.appId.equals(thatObj.getAppId()) ) {
            return false;
        }
        if( (this.appUrl == null && thatObj.getAppUrl() != null)
            || (this.appUrl != null && thatObj.getAppUrl() == null)
            || !this.appUrl.equals(thatObj.getAppUrl()) ) {
            return false;
        }
        if( (this.consumerKey == null && thatObj.getConsumerKey() != null)
            || (this.consumerKey != null && thatObj.getConsumerKey() == null)
            || !this.consumerKey.equals(thatObj.getConsumerKey()) ) {
            return false;
        }
        if( (this.consumerSecret == null && thatObj.getConsumerSecret() != null)
            || (this.consumerSecret != null && thatObj.getConsumerSecret() == null)
            || !this.consumerSecret.equals(thatObj.getConsumerSecret()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
//        if( (this.createdTime == null && thatObj.getCreatedTime() != null)
//            || (this.createdTime != null && thatObj.getCreatedTime() == null)
//            || !this.createdTime.equals(thatObj.getCreatedTime()) ) {
//            return false;
//        }
//        if( (this.modifiedTime == null && thatObj.getModifiedTime() != null)
//            || (this.modifiedTime != null && thatObj.getModifiedTime() == null)
//            || !this.modifiedTime.equals(thatObj.getModifiedTime()) ) {
//            return false;
//        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        hash = 31 * hash + delta;
        delta = serviceName == null ? 0 : serviceName.hashCode();
        hash = 31 * hash + delta;
        delta = appId == null ? 0 : appId.hashCode();
        hash = 31 * hash + delta;
        delta = appUrl == null ? 0 : appUrl.hashCode();
        hash = 31 * hash + delta;
        delta = consumerKey == null ? 0 : consumerKey.hashCode();
        hash = 31 * hash + delta;
        delta = consumerSecret == null ? 0 : consumerSecret.hashCode();
        hash = 31 * hash + delta;
        delta = note == null ? 0 : note.hashCode();
        hash = 31 * hash + delta;
        delta = status == null ? 0 : status.hashCode();
        hash = 31 * hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        hash = 31 * hash + delta;
        return hash;
    }

}
