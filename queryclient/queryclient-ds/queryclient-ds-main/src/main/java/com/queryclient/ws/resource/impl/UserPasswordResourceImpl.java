package com.queryclient.ws.resource.impl;

import java.io.StringWriter;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import com.sun.jersey.api.core.HttpContext;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.auth.TwoLeggedOAuthProvider;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;
import com.queryclient.ws.resource.exception.UnauthorizedRsException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.bean.UserPasswordBean;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.UserPasswordListStub;
import com.queryclient.ws.stub.UserPasswordStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.UserPasswordResource;
import com.queryclient.ws.resource.util.GaeAppStructResourceUtil;


@Path("/userPasswords/")
public class UserPasswordResourceImpl extends BaseResourceImpl implements UserPasswordResource
{
    private static final Logger log = Logger.getLogger(UserPasswordResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public UserPasswordResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;
    }

    private Response getUserPasswordList(List<UserPassword> beans, StringCursor forwardCursor) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }

        Response response = null;
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            UserPasswordListStub listStub = new UserPasswordListStub();
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            response = Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } else {
            long lastModifiedTime = 0L;
            List<UserPasswordStub> stubs = new ArrayList<UserPasswordStub>();
            Iterator<UserPassword> it = beans.iterator();
            while(it.hasNext()) {
                UserPassword bean = (UserPassword) it.next();
                stubs.add(UserPasswordStub.convertBeanToStub(bean));
                if(bean.getModifiedTime() != null) {
                    if(bean.getModifiedTime() > lastModifiedTime) {
                        lastModifiedTime = bean.getModifiedTime();
                    }
                } else {
                    if(bean.getCreatedTime() > lastModifiedTime) {
                        lastModifiedTime = bean.getCreatedTime();
                    }
                }
            }
            UserPasswordListStub listStub = new UserPasswordListStub(stubs);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            response = Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).lastModified(new Date(lastModifiedTime)).build();
        }
        return response;
    }

    @Override
    public Response getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<UserPassword> beans = ServiceManager.getUserPasswordService().getAllUserPasswords(ordering, offset, count, forwardCursor);
            return getUserPasswordList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<String> keys = ServiceManager.getUserPasswordService().getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<UserPassword> beans = ServiceManager.getUserPasswordService().findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return getUserPasswordList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<String> keys = ServiceManager.getUserPasswordService().findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getUserPasswordKeys(List<String> guids) throws BaseResourceException
    {
        // TBD:
        throw new NotImplementedRsException("To be implemented", resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            Long count = ServiceManager.getUserPasswordService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getUserPassword(String guid) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            UserPassword bean = ServiceManager.getUserPasswordService().getUserPassword(guid);

            EntityTag eTag = new EntityTag(Integer.toString(bean.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("UserPassword object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full UserPassword object.");

            UserPasswordStub stub = UserPasswordStub.convertBeanToStub(bean);
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).cacheControl(CacheControl.valueOf("private")).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).cacheControl(CacheControl.valueOf("private")).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getUserPassword(String guid, String field) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            if(field == null || field.trim().length() == 0) {
                return getUserPassword(guid);
            }
            UserPassword bean = ServiceManager.getUserPasswordService().getUserPassword(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("managerApp")) {
                    String fval = bean.getManagerApp();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("appAcl")) {
                    Long fval = bean.getAppAcl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("gaeApp")) {
                    GaeAppStruct fval = bean.getGaeApp();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("ownerUser")) {
                    String fval = bean.getOwnerUser();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("userAcl")) {
                    Long fval = bean.getUserAcl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("admin")) {
                    String fval = bean.getAdmin();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("user")) {
                    String fval = bean.getUser();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("username")) {
                    String fval = bean.getUsername();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("email")) {
                    String fval = bean.getEmail();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("openId")) {
                    String fval = bean.getOpenId();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("plainPassword")) {
                    String fval = bean.getPlainPassword();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("hashedPassword")) {
                    String fval = bean.getHashedPassword();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("salt")) {
                    String fval = bean.getSalt();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("hashMethod")) {
                    String fval = bean.getHashMethod();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("resetRequired")) {
                    Boolean fval = bean.isResetRequired();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("challengeQuestion")) {
                    String fval = bean.getChallengeQuestion();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("challengeAnswer")) {
                    String fval = bean.getChallengeAnswer();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("status")) {
                    String fval = bean.getStatus();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("lastResetTime")) {
                    Long fval = bean.getLastResetTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("expirationTime")) {
                    Long fval = bean.getExpirationTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("private")).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("private"));
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    @Override
    public Response createUserPassword(UserPasswordStub userPassword) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        if(useAsyncService()) {
            log.log(Level.INFO, "createUserPassword(): Invoking an async call.");
            String guid = userPassword.getGuid();
            if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
                guid = GUID.generate();
                userPassword.setGuid(guid);
            }
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsCreateUserPassword-" + guid;
            String taskName = "WsCreateUserPassword-" + guid + "-" + (new Date()).getTime();
            TaskOptions taskOpt = null;
            try {
                final JAXBContext jAXBContext = JAXBContext.newInstance(UserPasswordStub.class);
                Marshaller marshaller = null;
                StringWriter writer = null;

                // Note that the actual size will be bigger than this in xml or json format...
                int approxPayloadSize = userPassword.toString().length() * 2;  // 2 bytes per char
                if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
                if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                    if(getCache() != null) {
                        getCache().put(taskName, userPassword);
                    
                        UserPasswordStub dummyStub = new UserPasswordStub();
                        dummyStub.setGuid(userPassword.getGuid());
                        marshaller = jAXBContext.createMarshaller();
                        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                        writer = new StringWriter();
                        marshaller.marshal(dummyStub, writer);
                        String dummyPayload = writer.toString();
                        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createUserPassword(): dummyPayload = " + dummyPayload);

                        // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                        taskOpt = com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl(TASK_URIPATH_PREFIX + "userPasswords/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                    } else {
                        throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                    }
                } else {
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(userPassword, writer);
                    String payload = writer.toString();
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createUserPassword(): payload = " + payload);

                    // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                    taskOpt = com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl(TASK_URIPATH_PREFIX + "userPasswords/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
                }
            } catch (JAXBException e) {
                log.log(Level.WARNING, "Marshaling failed during task enqueing.");
                throw new BaseResourceException("Marshaling failed during task enqueing.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Payload generation failed during task enqueing.");
                throw new BaseResourceException("Payload generation failed during task enqueing.", e);
            }

            queue.add(taskOpt);
            URI createdUri = URI.create(resourceUri + "/" + guid);  // The resource hasn't been created yet, but the client still might expect the location header!
            return Response.status(Status.ACCEPTED).location(createdUri).entity(guid).build();
        }
        // else

        try {
            UserPasswordBean bean = convertUserPasswordStubToBean(userPassword);
            String guid = ServiceManager.getUserPasswordService().createUserPassword(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ResourceAlreadyPresentException ex) {
            throw new ResourceAlreadyPresentRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateUserPassword(String guid, UserPasswordStub userPassword) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        //guid = GUID.normalize(guid);    // TBD: Validate guid?
        if(userPassword == null || !guid.equals(userPassword.getGuid())) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from userPassword guid = " + userPassword.getGuid());
            throw new RequestForbiddenRsException("Failed to update the userPassword with guid = " + guid);
        }

        if(useAsyncService()) {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateUserPassword(): Invoking an async call: guid = " + guid);
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsUpdateUserPassword-" + guid;
            String taskName = "WsUpdateUserPassword-" + guid + "-" + (new Date()).getTime();
            TaskOptions taskOpt = null;
            try {
                final JAXBContext jAXBContext = JAXBContext.newInstance(UserPasswordStub.class);
                Marshaller marshaller = null;
                StringWriter writer = null;

                // Note that the actual size will be bigger than this in xml or json format...
                int approxPayloadSize = userPassword.toString().length() * 2;  // 2 bytes per char
                if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
                if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                    if(getCache() != null) {
                        getCache().put(taskName, userPassword);

                        UserPasswordStub dummyStub = new UserPasswordStub();
                        dummyStub.setGuid(userPassword.getGuid());
                        marshaller = jAXBContext.createMarshaller();
                        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                        writer = new StringWriter();
                        marshaller.marshal(dummyStub, writer);
                        String dummyPayload = writer.toString();
                        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateUserPassword(): dummyPayload = " + dummyPayload);

                        // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                        taskOpt = com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl(TASK_URIPATH_PREFIX + "userPasswords/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                    } else {
                        throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                    }
                } else {
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(userPassword, writer);
                    String payload = writer.toString();
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateUserPassword(): payload = " + payload);

                    // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                    taskOpt = com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl(TASK_URIPATH_PREFIX + "userPasswords/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
                }
            } catch (JAXBException e) {
                log.log(Level.WARNING, "Marshaling failed during task enqueing.");
                throw new BaseResourceException("Marshaling failed during task enqueing.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Payload generation failed during task enqueing.");
                throw new BaseResourceException("Payload generation failed during task enqueing.", e);
            }

            queue.add(taskOpt);
            return Response.status(Status.ACCEPTED).build();
        }
        // else

        try {
            UserPasswordBean bean = convertUserPasswordStubToBean(userPassword);
            boolean suc = ServiceManager.getUserPasswordService().updateUserPassword(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the userPassword with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the userPassword with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateUserPassword(String guid, String managerApp, Long appAcl, String gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            /*
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            boolean suc = ServiceManager.getUserPasswordService().updateUserPassword(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the userPassword with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the userPassword with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
//        } catch(BadRequestException ex) {
//            throw new BadRequestRsException(ex, resourceUri);
//        } catch(ResourceNotFoundException ex) {
//            throw new ResourceNotFoundRsException(ex, resourceUri);
//        } catch(ResourceGoneException ex) {
//            throw new ResourceGoneRsException(ex, resourceUri);
//        } catch(RequestForbiddenException ex) {
//            throw new RequestForbiddenRsException(ex, resourceUri);
//        } catch(RequestConflictException ex) {
//            throw new RequestConflictRsException(ex, resourceUri);
//        } catch(DataStoreException ex) {
//            throw new DataStoreRsException(ex, resourceUri);
//        } catch(ServiceUnavailableException ex) {
//            throw new ServiceUnavailableRsException(ex, resourceUri);
//        } catch(InternalServerErrorException ex) {
//            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteUserPassword(String guid) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        //guid = GUID.normalize(guid);    // TBD: Validate guid?
        if(useAsyncService()) {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteUserPassword(): Invoking an async call: guid = " + guid);
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsDeleteUserPassword-" + guid;
            String taskName = "WsDeleteUserPassword-" + guid + "-" + (new Date()).getTime();

            // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
            TaskOptions taskOpt = com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl(TASK_URIPATH_PREFIX + "userPasswords/" + guid).method(Method.DELETE).taskName(taskName);
            queue.add(taskOpt);
            return Response.status(Status.ACCEPTED).build();
        }
        // else

        try {
            boolean suc = ServiceManager.getUserPasswordService().deleteUserPassword(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the userPassword with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the userPassword with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteUserPasswords(String filter, String params, List<String> values) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            Long count = ServiceManager.getUserPasswordService().deleteUserPasswords(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    public static UserPasswordBean convertUserPasswordStubToBean(UserPassword stub)
    {
        UserPasswordBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new UserPasswordBean();
            bean.setGuid(stub.getGuid());
            bean.setManagerApp(stub.getManagerApp());
            bean.setAppAcl(stub.getAppAcl());
            bean.setGaeApp(GaeAppStructResourceUtil.convertGaeAppStructStubToBean(stub.getGaeApp()));
            bean.setOwnerUser(stub.getOwnerUser());
            bean.setUserAcl(stub.getUserAcl());
            bean.setAdmin(stub.getAdmin());
            bean.setUser(stub.getUser());
            bean.setUsername(stub.getUsername());
            bean.setEmail(stub.getEmail());
            bean.setOpenId(stub.getOpenId());
            bean.setPlainPassword(stub.getPlainPassword());
            bean.setHashedPassword(stub.getHashedPassword());
            bean.setSalt(stub.getSalt());
            bean.setHashMethod(stub.getHashMethod());
            bean.setResetRequired(stub.isResetRequired());
            bean.setChallengeQuestion(stub.getChallengeQuestion());
            bean.setChallengeAnswer(stub.getChallengeAnswer());
            bean.setStatus(stub.getStatus());
            bean.setLastResetTime(stub.getLastResetTime());
            bean.setExpirationTime(stub.getExpirationTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<UserPasswordBean> convertUserPasswordListStubToBeanList(UserPasswordListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<UserPasswordStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<UserPasswordBean> beanList = new ArrayList<UserPasswordBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(UserPasswordStub stub : stubList) {
                    UserPasswordBean bean = convertUserPasswordStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }

}
