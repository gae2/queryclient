package com.queryclient.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.stub.ErrorStub;

@Provider
public class MultipleChoiceExceptionMapper implements ExceptionMapper<MultipleChoiceRsException>
{
    public Response toResponse(MultipleChoiceRsException ex) {
        return Response.status(StatusCode.MULTIPLE_CHOICES)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
