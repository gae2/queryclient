package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "consumerKeySecretPairs")
@XmlType(propOrder = {"consumerKeySecretPair", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerKeySecretPairListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ConsumerKeySecretPairListStub.class.getName());

    private List<ConsumerKeySecretPairStub> consumerKeySecretPairs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public ConsumerKeySecretPairListStub()
    {
        this(new ArrayList<ConsumerKeySecretPairStub>());
    }
    public ConsumerKeySecretPairListStub(List<ConsumerKeySecretPairStub> consumerKeySecretPairs)
    {
        this(consumerKeySecretPairs, null);
    }
    public ConsumerKeySecretPairListStub(List<ConsumerKeySecretPairStub> consumerKeySecretPairs, String forwardCursor)
    {
        this.consumerKeySecretPairs = consumerKeySecretPairs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(consumerKeySecretPairs == null) {
            return true;
        } else {
            return consumerKeySecretPairs.isEmpty();
        }
    }
    public int getSize()
    {
        if(consumerKeySecretPairs == null) {
            return 0;
        } else {
            return consumerKeySecretPairs.size();
        }
    }


    @XmlElement(name = "consumerKeySecretPair")
    public List<ConsumerKeySecretPairStub> getConsumerKeySecretPair()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ConsumerKeySecretPairStub> getList()
    {
        return consumerKeySecretPairs;
    }
    public void setList(List<ConsumerKeySecretPairStub> consumerKeySecretPairs)
    {
        this.consumerKeySecretPairs = consumerKeySecretPairs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<ConsumerKeySecretPairStub> it = this.consumerKeySecretPairs.iterator();
        while(it.hasNext()) {
            ConsumerKeySecretPairStub consumerKeySecretPair = it.next();
            sb.append(consumerKeySecretPair.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ConsumerKeySecretPairListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ConsumerKeySecretPairListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ConsumerKeySecretPairListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ConsumerKeySecretPairListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ConsumerKeySecretPairListStub object as a string.", e);
        }
        
        return null;
    }
    public static ConsumerKeySecretPairListStub fromJsonString(String jsonStr)
    {
        try {
            ConsumerKeySecretPairListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ConsumerKeySecretPairListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ConsumerKeySecretPairListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ConsumerKeySecretPairListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ConsumerKeySecretPairListStub object.", e);
        }
        
        return null;
    }

}
