package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.QuerySessionDAO;
import com.queryclient.ws.data.QuerySessionDataObject;


// MockQuerySessionDAO is a decorator.
// It can be used as a base class to mock QuerySessionDAO objects.
public abstract class MockQuerySessionDAO implements QuerySessionDAO
{
    private static final Logger log = Logger.getLogger(MockQuerySessionDAO.class.getName()); 

    // MockQuerySessionDAO uses the decorator design pattern.
    private QuerySessionDAO decoratedDAO;

    public MockQuerySessionDAO(QuerySessionDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected QuerySessionDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(QuerySessionDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public QuerySessionDataObject getQuerySession(String guid) throws BaseException
    {
        return decoratedDAO.getQuerySession(guid);
	}

    @Override
    public List<QuerySessionDataObject> getQuerySessions(List<String> guids) throws BaseException
    {
        return decoratedDAO.getQuerySessions(guids);
    }

    @Override
    public List<QuerySessionDataObject> getAllQuerySessions() throws BaseException
	{
	    return getAllQuerySessions(null, null, null);
    }


    @Override
    public List<QuerySessionDataObject> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySessionDataObject> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllQuerySessions(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<QuerySessionDataObject> findQuerySessions(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findQuerySessions(filter, ordering, params, values, null, null);
    }

    @Override
	public List<QuerySessionDataObject> findQuerySessions(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findQuerySessions(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<QuerySessionDataObject> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<QuerySessionDataObject> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createQuerySession(QuerySessionDataObject querySession) throws BaseException
    {
        return decoratedDAO.createQuerySession( querySession);
    }

    @Override
	public Boolean updateQuerySession(QuerySessionDataObject querySession) throws BaseException
	{
        return decoratedDAO.updateQuerySession(querySession);
	}
	
    @Override
    public Boolean deleteQuerySession(QuerySessionDataObject querySession) throws BaseException
    {
        return decoratedDAO.deleteQuerySession(querySession);
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        return decoratedDAO.deleteQuerySession(guid);
	}

    @Override
    public Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteQuerySessions(filter, params, values);
    }

}
