package com.queryclient.ws.exception;

import com.queryclient.ws.BaseException;


public class TemporaryRedirectException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public TemporaryRedirectException() 
    {
        super();
    }
    public TemporaryRedirectException(String message) 
    {
        super(message);
    }
   public TemporaryRedirectException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public TemporaryRedirectException(Throwable cause) 
    {
        super(cause);
    }

}
