package com.queryclient.ws.service;

import java.util.List;

import com.queryclient.ws.FiveTen;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface FiveTenService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    FiveTen getFiveTen(String guid) throws BaseException;
    Object getFiveTen(String guid, String field) throws BaseException;
    List<FiveTen> getFiveTens(List<String> guids) throws BaseException;
    List<FiveTen> getAllFiveTens() throws BaseException;
    /* @Deprecated */ List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException;
    List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException;
    //String createFiveTen(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return FiveTen?)
    String createFiveTen(FiveTen fiveTen) throws BaseException;          // Returns Guid.  (Return FiveTen?)
    Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException;
    //Boolean updateFiveTen(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateFiveTen(FiveTen fiveTen) throws BaseException;
    Boolean deleteFiveTen(String guid) throws BaseException;
    Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException;
    Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException;

//    Integer createFiveTens(List<FiveTen> fiveTens) throws BaseException;
//    Boolean updateeFiveTens(List<FiveTen> fiveTens) throws BaseException;

}
