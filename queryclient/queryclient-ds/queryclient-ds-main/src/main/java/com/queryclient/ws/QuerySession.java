package com.queryclient.ws;



public interface QuerySession 
{
    String  getGuid();
    String  getUser();
    String  getDataService();
    String  getServiceUrl();
    String  getInputFormat();
    String  getOutputFormat();
    ReferrerInfoStruct  getReferrerInfo();
    String  getStatus();
    String  getNote();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
