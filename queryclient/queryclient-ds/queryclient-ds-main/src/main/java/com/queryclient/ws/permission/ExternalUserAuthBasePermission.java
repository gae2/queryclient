package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ExternalUserAuthBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExternalUserAuthBasePermission.class.getName());

    public ExternalUserAuthBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "ExternalUserAuth::" + action;
    }


}
