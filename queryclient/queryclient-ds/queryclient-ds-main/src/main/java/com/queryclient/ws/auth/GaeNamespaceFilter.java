package com.queryclient.ws.auth;

import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.google.appengine.api.NamespaceManager;


// Place holder for now...
public class GaeNamespaceFilter implements Filter
{
    private static final Logger log = Logger.getLogger(GaeNamespaceFilter.class.getName());


    @Override
    public void init(FilterConfig config) throws ServletException
    {
        log.fine("init() called.");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException
    {
        log.fine("doFilter() called.");

        // TBD: ....
        if (NamespaceManager.get() == null) {
            NamespaceManager.set(NamespaceManager.getGoogleAppsNamespace());
        }
    }

    @Override
    public void destroy()
    {
        log.fine("destroy() called.");
    }

}
