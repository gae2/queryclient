package com.queryclient.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.ServiceInfo;
import com.queryclient.ws.bean.ServiceInfoBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.ServiceInfoDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.ServiceInfoService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ServiceInfoServiceImpl implements ServiceInfoService
{
    private static final Logger log = Logger.getLogger(ServiceInfoServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ServiceInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ServiceInfo getServiceInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ServiceInfoDataObject dataObj = getDAOFactory().getServiceInfoDAO().getServiceInfo(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ServiceInfoDataObject for guid = " + guid);
            return null;  // ????
        }
        ServiceInfoBean bean = new ServiceInfoBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getServiceInfo(String guid, String field) throws BaseException
    {
        ServiceInfoDataObject dataObj = getDAOFactory().getServiceInfoDAO().getServiceInfo(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ServiceInfoDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("content")) {
            return dataObj.getContent();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("scheduledTime")) {
            return dataObj.getScheduledTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ServiceInfo> getServiceInfos(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ServiceInfo> list = new ArrayList<ServiceInfo>();
        List<ServiceInfoDataObject> dataObjs = getDAOFactory().getServiceInfoDAO().getServiceInfos(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceInfoDataObject list.");
        } else {
            Iterator<ServiceInfoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ServiceInfoDataObject dataObj = (ServiceInfoDataObject) it.next();
                list.add(new ServiceInfoBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos() throws BaseException
    {
        return getAllServiceInfos(null, null, null);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceInfos(ordering, offset, count, null);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllServiceInfos(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ServiceInfo> list = new ArrayList<ServiceInfo>();
        List<ServiceInfoDataObject> dataObjs = getDAOFactory().getServiceInfoDAO().getAllServiceInfos(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceInfoDataObject list.");
        } else {
            Iterator<ServiceInfoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ServiceInfoDataObject dataObj = (ServiceInfoDataObject) it.next();
                list.add(new ServiceInfoBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllServiceInfoKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getServiceInfoDAO().getAllServiceInfoKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceInfo key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findServiceInfos(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ServiceInfoServiceImpl.findServiceInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ServiceInfo> list = new ArrayList<ServiceInfo>();
        List<ServiceInfoDataObject> dataObjs = getDAOFactory().getServiceInfoDAO().findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find serviceInfos for the given criterion.");
        } else {
            Iterator<ServiceInfoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ServiceInfoDataObject dataObj = (ServiceInfoDataObject) it.next();
                list.add(new ServiceInfoBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ServiceInfoServiceImpl.findServiceInfoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getServiceInfoDAO().findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ServiceInfo keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ServiceInfoServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getServiceInfoDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createServiceInfo(String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        ServiceInfoDataObject dataObj = new ServiceInfoDataObject(null, title, content, type, status, scheduledTime);
        return createServiceInfo(dataObj);
    }

    @Override
    public String createServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceInfo cannot be null.....
        if(serviceInfo == null) {
            log.log(Level.INFO, "Param serviceInfo is null!");
            throw new BadRequestException("Param serviceInfo object is null!");
        }
        ServiceInfoDataObject dataObj = null;
        if(serviceInfo instanceof ServiceInfoDataObject) {
            dataObj = (ServiceInfoDataObject) serviceInfo;
        } else if(serviceInfo instanceof ServiceInfoBean) {
            dataObj = ((ServiceInfoBean) serviceInfo).toDataObject();
        } else {  // if(serviceInfo instanceof ServiceInfo)
            //dataObj = new ServiceInfoDataObject(null, serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ServiceInfoDataObject(serviceInfo.getGuid(), serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
        }
        String guid = getDAOFactory().getServiceInfoDAO().createServiceInfo(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ServiceInfoDataObject dataObj = new ServiceInfoDataObject(guid, title, content, type, status, scheduledTime);
        return updateServiceInfo(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceInfo cannot be null.....
        if(serviceInfo == null || serviceInfo.getGuid() == null) {
            log.log(Level.INFO, "Param serviceInfo or its guid is null!");
            throw new BadRequestException("Param serviceInfo object or its guid is null!");
        }
        ServiceInfoDataObject dataObj = null;
        if(serviceInfo instanceof ServiceInfoDataObject) {
            dataObj = (ServiceInfoDataObject) serviceInfo;
        } else if(serviceInfo instanceof ServiceInfoBean) {
            dataObj = ((ServiceInfoBean) serviceInfo).toDataObject();
        } else {  // if(serviceInfo instanceof ServiceInfo)
            dataObj = new ServiceInfoDataObject(serviceInfo.getGuid(), serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
        }
        Boolean suc = getDAOFactory().getServiceInfoDAO().updateServiceInfo(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteServiceInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getServiceInfoDAO().deleteServiceInfo(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceInfo cannot be null.....
        if(serviceInfo == null || serviceInfo.getGuid() == null) {
            log.log(Level.INFO, "Param serviceInfo or its guid is null!");
            throw new BadRequestException("Param serviceInfo object or its guid is null!");
        }
        ServiceInfoDataObject dataObj = null;
        if(serviceInfo instanceof ServiceInfoDataObject) {
            dataObj = (ServiceInfoDataObject) serviceInfo;
        } else if(serviceInfo instanceof ServiceInfoBean) {
            dataObj = ((ServiceInfoBean) serviceInfo).toDataObject();
        } else {  // if(serviceInfo instanceof ServiceInfo)
            dataObj = new ServiceInfoDataObject(serviceInfo.getGuid(), serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
        }
        Boolean suc = getDAOFactory().getServiceInfoDAO().deleteServiceInfo(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteServiceInfos(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getServiceInfoDAO().deleteServiceInfos(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
