package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "gaeAppStructs")
@XmlType(propOrder = {"gaeAppStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class GaeAppStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GaeAppStructListStub.class.getName());

    private List<GaeAppStructStub> gaeAppStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public GaeAppStructListStub()
    {
        this(new ArrayList<GaeAppStructStub>());
    }
    public GaeAppStructListStub(List<GaeAppStructStub> gaeAppStructs)
    {
        this(gaeAppStructs, null);
    }
    public GaeAppStructListStub(List<GaeAppStructStub> gaeAppStructs, String forwardCursor)
    {
        this.gaeAppStructs = gaeAppStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(gaeAppStructs == null) {
            return true;
        } else {
            return gaeAppStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(gaeAppStructs == null) {
            return 0;
        } else {
            return gaeAppStructs.size();
        }
    }


    @XmlElement(name = "gaeAppStruct")
    public List<GaeAppStructStub> getGaeAppStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<GaeAppStructStub> getList()
    {
        return gaeAppStructs;
    }
    public void setList(List<GaeAppStructStub> gaeAppStructs)
    {
        this.gaeAppStructs = gaeAppStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<GaeAppStructStub> it = this.gaeAppStructs.iterator();
        while(it.hasNext()) {
            GaeAppStructStub gaeAppStruct = it.next();
            sb.append(gaeAppStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static GaeAppStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of GaeAppStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write GaeAppStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write GaeAppStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write GaeAppStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static GaeAppStructListStub fromJsonString(String jsonStr)
    {
        try {
            GaeAppStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, GaeAppStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeAppStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeAppStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeAppStructListStub object.", e);
        }
        
        return null;
    }

}
