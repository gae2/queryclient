package com.queryclient.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.stub.ErrorStub;

@Provider
public class BaseExceptionMapper implements ExceptionMapper<BaseException>
{
    public Response toResponse(BaseException ex) 
    {
        return Response.status(Status.INTERNAL_SERVER_ERROR)
        .entity(new ErrorStub(ex.getMessage()))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
