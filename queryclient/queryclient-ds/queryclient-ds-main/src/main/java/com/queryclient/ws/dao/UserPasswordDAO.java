package com.queryclient.ws.dao;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.data.UserPasswordDataObject;


// TBD: Add offset/count to getAllUserPasswords() and findUserPasswords(), etc.
public interface UserPasswordDAO
{
    UserPasswordDataObject getUserPassword(String guid) throws BaseException;
    List<UserPasswordDataObject> getUserPasswords(List<String> guids) throws BaseException;
    List<UserPasswordDataObject> getAllUserPasswords() throws BaseException;
    /* @Deprecated */ List<UserPasswordDataObject> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException;
    List<UserPasswordDataObject> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<UserPasswordDataObject> findUserPasswords(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<UserPasswordDataObject> findUserPasswords(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<UserPasswordDataObject> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<UserPasswordDataObject> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUserPassword(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserPasswordDataObject?)
    String createUserPassword(UserPasswordDataObject userPassword) throws BaseException;          // Returns Guid.  (Return UserPasswordDataObject?)
    //Boolean updateUserPassword(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserPassword(UserPasswordDataObject userPassword) throws BaseException;
    Boolean deleteUserPassword(String guid) throws BaseException;
    Boolean deleteUserPassword(UserPasswordDataObject userPassword) throws BaseException;
    Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException;
}
