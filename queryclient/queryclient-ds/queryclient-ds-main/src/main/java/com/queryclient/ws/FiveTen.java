package com.queryclient.ws;



public interface FiveTen 
{
    String  getGuid();
    Integer  getCounter();
    String  getRequesterIpAddress();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
