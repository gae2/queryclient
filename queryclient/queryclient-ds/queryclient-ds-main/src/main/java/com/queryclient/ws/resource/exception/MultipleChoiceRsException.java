package com.queryclient.ws.resource.exception;

import com.queryclient.ws.exception.resource.BaseResourceException;


public class MultipleChoiceRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public MultipleChoiceRsException() 
    {
        super();
    }
    public MultipleChoiceRsException(String message) 
    {
        super(message);
    }
    public MultipleChoiceRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public MultipleChoiceRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public MultipleChoiceRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public MultipleChoiceRsException(Throwable cause) 
    {
        super(cause);
    }
    public MultipleChoiceRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
