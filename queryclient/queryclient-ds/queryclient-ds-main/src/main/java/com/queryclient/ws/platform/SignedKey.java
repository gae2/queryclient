package com.queryclient.ws.platform;

public class SignedKey
{
    private String keyName;
    private byte[] signature;

    public SignedKey(String keyName, byte[] signature)
    {
        super();
        this.keyName = keyName;
        this.signature = signature;
    }

    public String getKeyName()
    {
        return keyName;
    }
    public void setKeyName(String keyName)
    {
        this.keyName = keyName;
    }

    public byte[] getSignature()
    {
        return signature;
    }
    public void setSignature(byte[] signature)
    {
        this.signature = signature;
    }
    
}
