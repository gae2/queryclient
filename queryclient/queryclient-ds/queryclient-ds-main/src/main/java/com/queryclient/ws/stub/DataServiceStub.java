package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "dataService")
@XmlType(propOrder = {"guid", "user", "name", "description", "type", "mode", "serviceUrl", "authRequired", "authCredentialStub", "referrerInfoStub", "status", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataServiceStub implements DataService, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DataServiceStub.class.getName());

    private String guid;
    private String user;
    private String name;
    private String description;
    private String type;
    private String mode;
    private String serviceUrl;
    private Boolean authRequired;
    private ConsumerKeySecretPairStub authCredential;
    private ReferrerInfoStructStub referrerInfo;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    public DataServiceStub()
    {
        this(null);
    }
    public DataServiceStub(DataService bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.name = bean.getName();
            this.description = bean.getDescription();
            this.type = bean.getType();
            this.mode = bean.getMode();
            this.serviceUrl = bean.getServiceUrl();
            this.authRequired = bean.isAuthRequired();
            this.authCredential = ConsumerKeySecretPairStub.convertBeanToStub(bean.getAuthCredential());
            this.referrerInfo = ReferrerInfoStructStub.convertBeanToStub(bean.getReferrerInfo());
            this.status = bean.getStatus();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    @XmlElement
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    @XmlElement
    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    @XmlElement
    public String getMode()
    {
        return this.mode;
    }
    public void setMode(String mode)
    {
        this.mode = mode;
    }

    @XmlElement
    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    @XmlElement
    public Boolean isAuthRequired()
    {
        return this.authRequired;
    }
    public void setAuthRequired(Boolean authRequired)
    {
        this.authRequired = authRequired;
    }

    @XmlElement(name = "authCredential")
    @JsonIgnore
    public ConsumerKeySecretPairStub getAuthCredentialStub()
    {
        return this.authCredential;
    }
    public void setAuthCredentialStub(ConsumerKeySecretPairStub authCredential)
    {
        this.authCredential = authCredential;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ConsumerKeySecretPairStub.class)
    public ConsumerKeySecretPair getAuthCredential()
    {  
        return getAuthCredentialStub();
    }
    public void setAuthCredential(ConsumerKeySecretPair authCredential)
    {
        if((authCredential == null) || (authCredential instanceof ConsumerKeySecretPairStub)) {
            setAuthCredentialStub((ConsumerKeySecretPairStub) authCredential);
        } else {
            // TBD
            setAuthCredentialStub(ConsumerKeySecretPairStub.convertBeanToStub(authCredential));
        }
    }

    @XmlElement(name = "referrerInfo")
    @JsonIgnore
    public ReferrerInfoStructStub getReferrerInfoStub()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfoStub(ReferrerInfoStructStub referrerInfo)
    {
        this.referrerInfo = referrerInfo;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ReferrerInfoStructStub.class)
    public ReferrerInfoStruct getReferrerInfo()
    {  
        return getReferrerInfoStub();
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if((referrerInfo == null) || (referrerInfo instanceof ReferrerInfoStructStub)) {
            setReferrerInfoStub((ReferrerInfoStructStub) referrerInfo);
        } else {
            // TBD
            setReferrerInfoStub(ReferrerInfoStructStub.convertBeanToStub(referrerInfo));
        }
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("name", this.name);
        dataMap.put("description", this.description);
        dataMap.put("type", this.type);
        dataMap.put("mode", this.mode);
        dataMap.put("serviceUrl", this.serviceUrl);
        dataMap.put("authRequired", this.authRequired);
        dataMap.put("authCredential", this.authCredential);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("status", this.status);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = name == null ? 0 : name.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = mode == null ? 0 : mode.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = authRequired == null ? 0 : authRequired.hashCode();
        _hash = 31 * _hash + delta;
        delta = authCredential == null ? 0 : authCredential.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static DataServiceStub convertBeanToStub(DataService bean)
    {
        DataServiceStub stub = null;
        if(bean instanceof DataServiceStub) {
            stub = (DataServiceStub) bean;
        } else {
            if(bean != null) {
                stub = new DataServiceStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static DataServiceStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of DataServiceStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write DataServiceStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write DataServiceStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write DataServiceStub object as a string.", e);
        }
        
        return null;
    }
    public static DataServiceStub fromJsonString(String jsonStr)
    {
        try {
            DataServiceStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, DataServiceStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into DataServiceStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into DataServiceStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into DataServiceStub object.", e);
        }
        
        return null;
    }

}
