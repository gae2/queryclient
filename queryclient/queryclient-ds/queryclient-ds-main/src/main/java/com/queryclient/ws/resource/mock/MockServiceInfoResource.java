package com.queryclient.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.ServiceInfo;
import com.queryclient.ws.bean.ServiceInfoBean;
import com.queryclient.ws.stub.ServiceInfoListStub;
import com.queryclient.ws.stub.ServiceInfoStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.ServiceInfoResource;

// MockServiceInfoResource is a decorator.
// It can be used as a base class to mock ServiceInfoResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/serviceInfos/")
public abstract class MockServiceInfoResource implements ServiceInfoResource
{
    private static final Logger log = Logger.getLogger(MockServiceInfoResource.class.getName());

    // MockServiceInfoResource uses the decorator design pattern.
    private ServiceInfoResource decoratedResource;

    public MockServiceInfoResource(ServiceInfoResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected ServiceInfoResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(ServiceInfoResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllServiceInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllServiceInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllServiceInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllServiceInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getServiceInfoKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getServiceInfoKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getServiceInfo(String guid) throws BaseResourceException
    {
        return decoratedResource.getServiceInfo(guid);
    }

    @Override
    public Response getServiceInfo(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getServiceInfo(guid, field);
    }

    @Override
    public Response createServiceInfo(ServiceInfoStub serviceInfo) throws BaseResourceException
    {
        return decoratedResource.createServiceInfo(serviceInfo);
    }

    @Override
    public Response updateServiceInfo(String guid, ServiceInfoStub serviceInfo) throws BaseResourceException
    {
        return decoratedResource.updateServiceInfo(guid, serviceInfo);
    }

    @Override
    public Response updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime) throws BaseResourceException
    {
        return decoratedResource.updateServiceInfo(guid, title, content, type, status, scheduledTime);
    }

    @Override
    public Response deleteServiceInfo(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteServiceInfo(guid);
    }

    @Override
    public Response deleteServiceInfos(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteServiceInfos(filter, params, values);
    }


}
