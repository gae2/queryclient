package com.queryclient.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.ws.bean.ConsumerKeySecretPairBean;
import com.queryclient.ws.bean.ReferrerInfoStructBean;
import com.queryclient.ws.bean.DataServiceBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.ConsumerKeySecretPairDataObject;
import com.queryclient.ws.data.ReferrerInfoStructDataObject;
import com.queryclient.ws.data.DataServiceDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.DataServiceService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DataServiceServiceImpl implements DataServiceService
{
    private static final Logger log = Logger.getLogger(DataServiceServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // DataService related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DataService getDataService(String guid) throws BaseException
    {
        log.finer("BEGIN");

        DataServiceDataObject dataObj = getDAOFactory().getDataServiceDAO().getDataService(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DataServiceDataObject for guid = " + guid);
            return null;  // ????
        }
        DataServiceBean bean = new DataServiceBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getDataService(String guid, String field) throws BaseException
    {
        DataServiceDataObject dataObj = getDAOFactory().getDataServiceDAO().getDataService(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DataServiceDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("name")) {
            return dataObj.getName();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("mode")) {
            return dataObj.getMode();
        } else if(field.equals("serviceUrl")) {
            return dataObj.getServiceUrl();
        } else if(field.equals("authRequired")) {
            return dataObj.isAuthRequired();
        } else if(field.equals("authCredential")) {
            return dataObj.getAuthCredential();
        } else if(field.equals("referrerInfo")) {
            return dataObj.getReferrerInfo();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<DataService> getDataServices(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<DataService> list = new ArrayList<DataService>();
        List<DataServiceDataObject> dataObjs = getDAOFactory().getDataServiceDAO().getDataServices(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve DataServiceDataObject list.");
        } else {
            Iterator<DataServiceDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DataServiceDataObject dataObj = (DataServiceDataObject) it.next();
                list.add(new DataServiceBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<DataService> getAllDataServices() throws BaseException
    {
        return getAllDataServices(null, null, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServices(ordering, offset, count, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDataServices(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DataService> list = new ArrayList<DataService>();
        List<DataServiceDataObject> dataObjs = getDAOFactory().getDataServiceDAO().getAllDataServices(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve DataServiceDataObject list.");
        } else {
            Iterator<DataServiceDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DataServiceDataObject dataObj = (DataServiceDataObject) it.next();
                list.add(new DataServiceBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllDataServiceKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getDataServiceDAO().getAllDataServiceKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve DataService key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DataServiceServiceImpl.findDataServices(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DataService> list = new ArrayList<DataService>();
        List<DataServiceDataObject> dataObjs = getDAOFactory().getDataServiceDAO().findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find dataServices for the given criterion.");
        } else {
            Iterator<DataServiceDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DataServiceDataObject dataObj = (DataServiceDataObject) it.next();
                list.add(new DataServiceBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DataServiceServiceImpl.findDataServiceKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getDataServiceDAO().findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find DataService keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DataServiceServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getDataServiceDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createDataService(String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        ConsumerKeySecretPairDataObject authCredentialDobj = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialDobj = ((ConsumerKeySecretPairBean) authCredential).toDataObject();
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialDobj = new ConsumerKeySecretPairDataObject(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialDobj = null;   // ????
        }
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        
        DataServiceDataObject dataObj = new DataServiceDataObject(null, user, name, description, type, mode, serviceUrl, authRequired, authCredentialDobj, referrerInfoDobj, status);
        return createDataService(dataObj);
    }

    @Override
    public String createDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");

        // Param dataService cannot be null.....
        if(dataService == null) {
            log.log(Level.INFO, "Param dataService is null!");
            throw new BadRequestException("Param dataService object is null!");
        }
        DataServiceDataObject dataObj = null;
        if(dataService instanceof DataServiceDataObject) {
            dataObj = (DataServiceDataObject) dataService;
        } else if(dataService instanceof DataServiceBean) {
            dataObj = ((DataServiceBean) dataService).toDataObject();
        } else {  // if(dataService instanceof DataService)
            //dataObj = new DataServiceDataObject(null, dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), (ConsumerKeySecretPairDataObject) dataService.getAuthCredential(), (ReferrerInfoStructDataObject) dataService.getReferrerInfo(), dataService.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new DataServiceDataObject(dataService.getGuid(), dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), (ConsumerKeySecretPairDataObject) dataService.getAuthCredential(), (ReferrerInfoStructDataObject) dataService.getReferrerInfo(), dataService.getStatus());
        }
        String guid = getDAOFactory().getDataServiceDAO().createDataService(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        ConsumerKeySecretPairDataObject authCredentialDobj = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialDobj = ((ConsumerKeySecretPairBean) authCredential).toDataObject();            
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialDobj = new ConsumerKeySecretPairDataObject(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialDobj = null;   // ????
        }
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();            
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        DataServiceDataObject dataObj = new DataServiceDataObject(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredentialDobj, referrerInfoDobj, status);
        return updateDataService(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");

        // Param dataService cannot be null.....
        if(dataService == null || dataService.getGuid() == null) {
            log.log(Level.INFO, "Param dataService or its guid is null!");
            throw new BadRequestException("Param dataService object or its guid is null!");
        }
        DataServiceDataObject dataObj = null;
        if(dataService instanceof DataServiceDataObject) {
            dataObj = (DataServiceDataObject) dataService;
        } else if(dataService instanceof DataServiceBean) {
            dataObj = ((DataServiceBean) dataService).toDataObject();
        } else {  // if(dataService instanceof DataService)
            dataObj = new DataServiceDataObject(dataService.getGuid(), dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), dataService.getAuthCredential(), dataService.getReferrerInfo(), dataService.getStatus());
        }
        Boolean suc = getDAOFactory().getDataServiceDAO().updateDataService(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getDataServiceDAO().deleteDataService(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");

        // Param dataService cannot be null.....
        if(dataService == null || dataService.getGuid() == null) {
            log.log(Level.INFO, "Param dataService or its guid is null!");
            throw new BadRequestException("Param dataService object or its guid is null!");
        }
        DataServiceDataObject dataObj = null;
        if(dataService instanceof DataServiceDataObject) {
            dataObj = (DataServiceDataObject) dataService;
        } else if(dataService instanceof DataServiceBean) {
            dataObj = ((DataServiceBean) dataService).toDataObject();
        } else {  // if(dataService instanceof DataService)
            dataObj = new DataServiceDataObject(dataService.getGuid(), dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), dataService.getAuthCredential(), dataService.getReferrerInfo(), dataService.getStatus());
        }
        Boolean suc = getDAOFactory().getDataServiceDAO().deleteDataService(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteDataServices(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getDataServiceDAO().deleteDataServices(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
