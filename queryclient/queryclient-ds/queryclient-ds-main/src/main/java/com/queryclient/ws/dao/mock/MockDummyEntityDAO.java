package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.DummyEntityDAO;
import com.queryclient.ws.data.DummyEntityDataObject;


// MockDummyEntityDAO is a decorator.
// It can be used as a base class to mock DummyEntityDAO objects.
public abstract class MockDummyEntityDAO implements DummyEntityDAO
{
    private static final Logger log = Logger.getLogger(MockDummyEntityDAO.class.getName()); 

    // MockDummyEntityDAO uses the decorator design pattern.
    private DummyEntityDAO decoratedDAO;

    public MockDummyEntityDAO(DummyEntityDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected DummyEntityDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(DummyEntityDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public DummyEntityDataObject getDummyEntity(String guid) throws BaseException
    {
        return decoratedDAO.getDummyEntity(guid);
	}

    @Override
    public List<DummyEntityDataObject> getDummyEntities(List<String> guids) throws BaseException
    {
        return decoratedDAO.getDummyEntities(guids);
    }

    @Override
    public List<DummyEntityDataObject> getAllDummyEntities() throws BaseException
	{
	    return getAllDummyEntities(null, null, null);
    }


    @Override
    public List<DummyEntityDataObject> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntityDataObject> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllDummyEntities(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<DummyEntityDataObject> findDummyEntities(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findDummyEntities(filter, ordering, params, values, null, null);
    }

    @Override
	public List<DummyEntityDataObject> findDummyEntities(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findDummyEntities(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<DummyEntityDataObject> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<DummyEntityDataObject> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createDummyEntity(DummyEntityDataObject dummyEntity) throws BaseException
    {
        return decoratedDAO.createDummyEntity( dummyEntity);
    }

    @Override
	public Boolean updateDummyEntity(DummyEntityDataObject dummyEntity) throws BaseException
	{
        return decoratedDAO.updateDummyEntity(dummyEntity);
	}
	
    @Override
    public Boolean deleteDummyEntity(DummyEntityDataObject dummyEntity) throws BaseException
    {
        return decoratedDAO.deleteDummyEntity(dummyEntity);
    }

    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        return decoratedDAO.deleteDummyEntity(guid);
	}

    @Override
    public Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteDummyEntities(filter, params, values);
    }

}
