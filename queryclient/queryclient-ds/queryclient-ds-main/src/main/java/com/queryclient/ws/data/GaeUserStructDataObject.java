package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class GaeUserStructDataObject implements GaeUserStruct, Serializable
{
    private static final Logger log = Logger.getLogger(GaeUserStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _gaeuserstruct_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String authDomain;

    @Persistent(defaultFetchGroup = "true")
    private String federatedIdentity;

    @Persistent(defaultFetchGroup = "true")
    private String nickname;

    @Persistent(defaultFetchGroup = "true")
    private String userId;

    @Persistent(defaultFetchGroup = "true")
    private String email;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public GaeUserStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null);
    }
    public GaeUserStructDataObject(String authDomain, String federatedIdentity, String nickname, String userId, String email, String note)
    {
        setAuthDomain(authDomain);
        setFederatedIdentity(federatedIdentity);
        setNickname(nickname);
        setUserId(userId);
        setEmail(email);
        setNote(note);
    }

    private void resetEncodedKey()
    {
    }

    public String getAuthDomain()
    {
        return this.authDomain;
    }
    public void setAuthDomain(String authDomain)
    {
        this.authDomain = authDomain;
        if(this.authDomain != null) {
            resetEncodedKey();
        }
    }

    public String getFederatedIdentity()
    {
        return this.federatedIdentity;
    }
    public void setFederatedIdentity(String federatedIdentity)
    {
        this.federatedIdentity = federatedIdentity;
        if(this.federatedIdentity != null) {
            resetEncodedKey();
        }
    }

    public String getNickname()
    {
        return this.nickname;
    }
    public void setNickname(String nickname)
    {
        this.nickname = nickname;
        if(this.nickname != null) {
            resetEncodedKey();
        }
    }

    public String getUserId()
    {
        return this.userId;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
        if(this.userId != null) {
            resetEncodedKey();
        }
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
        if(this.email != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getAuthDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFederatedIdentity() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNickname() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUserId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmail() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("authDomain", this.authDomain);
        dataMap.put("federatedIdentity", this.federatedIdentity);
        dataMap.put("nickname", this.nickname);
        dataMap.put("userId", this.userId);
        dataMap.put("email", this.email);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        GaeUserStruct thatObj = (GaeUserStruct) obj;
        if( (this.authDomain == null && thatObj.getAuthDomain() != null)
            || (this.authDomain != null && thatObj.getAuthDomain() == null)
            || !this.authDomain.equals(thatObj.getAuthDomain()) ) {
            return false;
        }
        if( (this.federatedIdentity == null && thatObj.getFederatedIdentity() != null)
            || (this.federatedIdentity != null && thatObj.getFederatedIdentity() == null)
            || !this.federatedIdentity.equals(thatObj.getFederatedIdentity()) ) {
            return false;
        }
        if( (this.nickname == null && thatObj.getNickname() != null)
            || (this.nickname != null && thatObj.getNickname() == null)
            || !this.nickname.equals(thatObj.getNickname()) ) {
            return false;
        }
        if( (this.userId == null && thatObj.getUserId() != null)
            || (this.userId != null && thatObj.getUserId() == null)
            || !this.userId.equals(thatObj.getUserId()) ) {
            return false;
        }
        if( (this.email == null && thatObj.getEmail() != null)
            || (this.email != null && thatObj.getEmail() == null)
            || !this.email.equals(thatObj.getEmail()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = authDomain == null ? 0 : authDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = federatedIdentity == null ? 0 : federatedIdentity.hashCode();
        _hash = 31 * _hash + delta;
        delta = nickname == null ? 0 : nickname.hashCode();
        _hash = 31 * _hash + delta;
        delta = userId == null ? 0 : userId.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
