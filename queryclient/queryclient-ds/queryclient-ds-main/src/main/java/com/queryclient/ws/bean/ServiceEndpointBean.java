package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.data.ConsumerKeySecretPairDataObject;
import com.queryclient.ws.data.ServiceEndpointDataObject;

public class ServiceEndpointBean extends BeanBase implements ServiceEndpoint
{
    private static final Logger log = Logger.getLogger(ServiceEndpointBean.class.getName());

    // Embedded data object.
    private ServiceEndpointDataObject dobj = null;

    public ServiceEndpointBean()
    {
        this(new ServiceEndpointDataObject());
    }
    public ServiceEndpointBean(String guid)
    {
        this(new ServiceEndpointDataObject(guid));
    }
    public ServiceEndpointBean(ServiceEndpointDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public ServiceEndpointDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
        }
    }

    public String getDataService()
    {
        if(getDataObject() != null) {
            return getDataObject().getDataService();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
            return null;   // ???
        }
    }
    public void setDataService(String dataService)
    {
        if(getDataObject() != null) {
            getDataObject().setDataService(dataService);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
        }
    }

    public String getServiceName()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceName(String serviceName)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceName(serviceName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
        }
    }

    public String getServiceUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceUrl(serviceUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
        }
    }

    public Boolean isAuthRequired()
    {
        if(getDataObject() != null) {
            return getDataObject().isAuthRequired();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthRequired(Boolean authRequired)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthRequired(authRequired);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
        }
    }

    public ConsumerKeySecretPair getAuthCredential()
    {
        if(getDataObject() != null) {
            ConsumerKeySecretPair _field = getDataObject().getAuthCredential();
            if(_field == null) {
                log.log(Level.INFO, "authCredential is null.");
                return null;
            } else {
                return new ConsumerKeySecretPairBean((ConsumerKeySecretPairDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthCredential(ConsumerKeySecretPair authCredential)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthCredential(
                (authCredential instanceof ConsumerKeySecretPairBean) ?
                ((ConsumerKeySecretPairBean) authCredential).toDataObject() :
                ((authCredential instanceof ConsumerKeySecretPairDataObject) ? authCredential : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ServiceEndpointDataObject is null!");
        }
    }


    // TBD
    public ServiceEndpointDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
