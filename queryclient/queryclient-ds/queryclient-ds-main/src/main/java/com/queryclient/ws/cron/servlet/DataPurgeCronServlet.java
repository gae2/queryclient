package com.queryclient.ws.cron.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.queryclient.ws.core.StatusCode;


// For old data purging.
public final class DataPurgeCronServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DataPurgeCronServlet.class.getName());

    
    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        //JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singleton instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        if(log.isLoggable(Level.INFO)) log.info("RefreshCronServlet::doGet() called.");
        
        // TBD:
        // Check the header X-AppEngine-Cron: true
        // ...
        boolean isGaeCron = false;
        String headerXGAECron = req.getHeader("X-AppEngine-Cron");
        if(headerXGAECron != null) {
            isGaeCron = Boolean.parseBoolean(headerXGAECron);
        }
        if(log.isLoggable(Level.FINE)) log.fine("isGaeCron = " + isGaeCron);

        
        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        String requestUrl = req.getRequestURL().toString();
        if(log.isLoggable(Level.FINE)) {
            log.fine("contextPath = " + contextPath);
            log.fine("servletPath = " + servletPath);
            log.fine("pathInfo = " + pathInfo);
            log.fine("queryString = " + queryString);
            log.fine("requestUrl = " + requestUrl);
        }
        

        
        // ???
        // /<entity1,entit2>/deltaHours/maxCount
        // ???? /<entity1,entit2>/deltaHours/maxCount?filter=<filter>
        // (If filter includes "createdTime < X", then it overwrites deltaHours.. ???)
        // ....
        
        
        // TBD:
        // ...
        List<String> entities = new ArrayList<>();
        Integer deltaHours = null;
        Integer maxCount = null;
        if(pathInfo != null && !pathInfo.isEmpty()) {
            if(pathInfo.startsWith("/")) {
                pathInfo = pathInfo.substring(1);
            }
            if(pathInfo == null || pathInfo.isEmpty()) {
                // ????
            } else {
                String entityListStr = null;
                if(pathInfo.contains("/")) {
                    String[] parts = pathInfo.split("/", 5);
                    int len = parts.length;
                    if(len > 0) {
                        entityListStr = parts[0];
                        if(len > 1) {
                            try {
                                int cnt = Integer.parseInt(parts[1]);
                                if(cnt > 0) {
                                    deltaHours = cnt;
                                } else {
                                    // ignore 
                                }
                            } catch(NumberFormatException e) {
                                // ignore...
                                if(log.isLoggable(Level.INFO)) log.info("Invalid pathInfo = " + pathInfo);
                            }
                        }
                        if(len > 2) {
                            try {
                                int mode = Integer.parseInt(parts[2]);
                                if(mode > 0) {
                                    maxCount = mode;
                                } else {
                                    // ignore 
                                }
                            } catch(NumberFormatException e) {
                                // ignore...
                                if(log.isLoggable(Level.INFO)) log.info("Invalid pathInfo = " + pathInfo);
                            }
                        }
                    }
                } else {
                    entityListStr = pathInfo;
                }
                if(entityListStr != null && !entityListStr.isEmpty()) {
                    String[] es = entityListStr.split(",");
                    entities = Arrays.asList(es);
                }
            }
        }
        // TBD: validate input values ???
        
        if(deltaHours == null || deltaHours <= 0) {
            deltaHours = DataPurgeCronManager.DEFAULT_DELTA_HOURS;
        } else if(deltaHours > DataPurgeCronManager.MAXIMUM_DELTA_HOURS) {
            deltaHours = DataPurgeCronManager.MAXIMUM_DELTA_HOURS;
        }
        if(maxCount == null || maxCount <= 0) {
            maxCount = DataPurgeCronManager.DEFAULT_MAX_COUNT;            
        } else if(maxCount > DataPurgeCronManager.MAXIMUM_MAX_COUNT) {
            maxCount = DataPurgeCronManager.MAXIMUM_MAX_COUNT;            
        }
        if(log.isLoggable(Level.INFO)) {
            log.info("entities = " + entities);
            log.info("deltaHours = " + deltaHours);
            log.info("maxCount = " + maxCount);
        }
        
        // temporary
        // for now, only for notifications...
        Set<String> deletableEntities = new HashSet<>(Arrays.asList(new String[]{
            "ApiConsumer", 
            "User", 
            "UserPassword", 
            "ExternalUserAuth", 
            "UserAuthState", 
            "DataService", 
            "ServiceEndpoint", 
            "QuerySession", 
            "QueryRecord", 
            "DummyEntity", 
            "ServiceInfo", 
            "FiveTen", 
        }));
        // ....

        for(String entity : entities) {
            if(deletableEntities.contains(entity)) {
                int cnt = DataPurgeCronManager.getInstance().processDeletion(entity, deltaHours, maxCount);
                if(log.isLoggable(Level.INFO)) log.info("Deleted " + cnt + " records at " + System.currentTimeMillis());
            } else {
                log.warning("Entity not deletable: " + entity);
            }
        }

        // Always return 200 ????
        // return 200 only if cnt > 0 ????
        resp.setStatus(StatusCode.OK);
    }

    
    // TBD:
    // depending on mail.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
}
