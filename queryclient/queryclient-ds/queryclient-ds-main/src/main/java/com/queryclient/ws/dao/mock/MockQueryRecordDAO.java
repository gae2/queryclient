package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.QueryRecordDAO;
import com.queryclient.ws.data.QueryRecordDataObject;


// MockQueryRecordDAO is a decorator.
// It can be used as a base class to mock QueryRecordDAO objects.
public abstract class MockQueryRecordDAO implements QueryRecordDAO
{
    private static final Logger log = Logger.getLogger(MockQueryRecordDAO.class.getName()); 

    // MockQueryRecordDAO uses the decorator design pattern.
    private QueryRecordDAO decoratedDAO;

    public MockQueryRecordDAO(QueryRecordDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected QueryRecordDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(QueryRecordDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public QueryRecordDataObject getQueryRecord(String guid) throws BaseException
    {
        return decoratedDAO.getQueryRecord(guid);
	}

    @Override
    public List<QueryRecordDataObject> getQueryRecords(List<String> guids) throws BaseException
    {
        return decoratedDAO.getQueryRecords(guids);
    }

    @Override
    public List<QueryRecordDataObject> getAllQueryRecords() throws BaseException
	{
	    return getAllQueryRecords(null, null, null);
    }


    @Override
    public List<QueryRecordDataObject> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllQueryRecords(ordering, offset, count, null);
    }

    @Override
    public List<QueryRecordDataObject> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllQueryRecords(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<QueryRecordDataObject> findQueryRecords(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findQueryRecords(filter, ordering, params, values, null, null);
    }

    @Override
	public List<QueryRecordDataObject> findQueryRecords(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findQueryRecords(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<QueryRecordDataObject> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<QueryRecordDataObject> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createQueryRecord(QueryRecordDataObject queryRecord) throws BaseException
    {
        return decoratedDAO.createQueryRecord( queryRecord);
    }

    @Override
	public Boolean updateQueryRecord(QueryRecordDataObject queryRecord) throws BaseException
	{
        return decoratedDAO.updateQueryRecord(queryRecord);
	}
	
    @Override
    public Boolean deleteQueryRecord(QueryRecordDataObject queryRecord) throws BaseException
    {
        return decoratedDAO.deleteQueryRecord(queryRecord);
    }

    @Override
    public Boolean deleteQueryRecord(String guid) throws BaseException
    {
        return decoratedDAO.deleteQueryRecord(guid);
	}

    @Override
    public Long deleteQueryRecords(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteQueryRecords(filter, params, values);
    }

}
