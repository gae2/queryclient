package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.data.ReferrerInfoStructDataObject;
import com.queryclient.ws.data.QuerySessionDataObject;

public class QuerySessionBean extends BeanBase implements QuerySession
{
    private static final Logger log = Logger.getLogger(QuerySessionBean.class.getName());

    // Embedded data object.
    private QuerySessionDataObject dobj = null;

    public QuerySessionBean()
    {
        this(new QuerySessionDataObject());
    }
    public QuerySessionBean(String guid)
    {
        this(new QuerySessionDataObject(guid));
    }
    public QuerySessionBean(QuerySessionDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public QuerySessionDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
        }
    }

    public String getDataService()
    {
        if(getDataObject() != null) {
            return getDataObject().getDataService();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
            return null;   // ???
        }
    }
    public void setDataService(String dataService)
    {
        if(getDataObject() != null) {
            getDataObject().setDataService(dataService);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
        }
    }

    public String getServiceUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceUrl(serviceUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
        }
    }

    public String getInputFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getInputFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
            return null;   // ???
        }
    }
    public void setInputFormat(String inputFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setInputFormat(inputFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
        }
    }

    public String getOutputFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputFormat(String outputFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputFormat(outputFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        if(getDataObject() != null) {
            ReferrerInfoStruct _field = getDataObject().getReferrerInfo();
            if(_field == null) {
                log.log(Level.INFO, "referrerInfo is null.");
                return null;
            } else {
                return new ReferrerInfoStructBean((ReferrerInfoStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getDataObject() != null) {
            getDataObject().setReferrerInfo(
                (referrerInfo instanceof ReferrerInfoStructBean) ?
                ((ReferrerInfoStructBean) referrerInfo).toDataObject() :
                ((referrerInfo instanceof ReferrerInfoStructDataObject) ? referrerInfo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QuerySessionDataObject is null!");
        }
    }


    // TBD
    public QuerySessionDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
