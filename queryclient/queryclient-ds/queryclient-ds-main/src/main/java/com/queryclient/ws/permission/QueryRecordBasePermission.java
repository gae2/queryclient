package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class QueryRecordBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QueryRecordBasePermission.class.getName());

    public QueryRecordBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "QueryRecord::" + action;
    }


}
