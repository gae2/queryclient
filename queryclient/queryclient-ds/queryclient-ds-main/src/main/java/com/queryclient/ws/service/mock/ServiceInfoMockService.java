package com.queryclient.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.ServiceInfo;
import com.queryclient.ws.bean.ServiceInfoBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.ServiceInfoDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.ServiceInfoService;


// ServiceInfoMockService is a decorator.
// It can be used as a base class to mock ServiceInfoService objects.
public abstract class ServiceInfoMockService implements ServiceInfoService
{
    private static final Logger log = Logger.getLogger(ServiceInfoMockService.class.getName());

    // ServiceInfoMockService uses the decorator design pattern.
    private ServiceInfoService decoratedService;

    public ServiceInfoMockService(ServiceInfoService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected ServiceInfoService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(ServiceInfoService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // ServiceInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ServiceInfo getServiceInfo(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getServiceInfo(): guid = " + guid);
        ServiceInfo bean = decoratedService.getServiceInfo(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getServiceInfo(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getServiceInfo(guid, field);
        return obj;
    }

    @Override
    public List<ServiceInfo> getServiceInfos(List<String> guids) throws BaseException
    {
        log.fine("getServiceInfos()");
        List<ServiceInfo> serviceInfos = decoratedService.getServiceInfos(guids);
        log.finer("END");
        return serviceInfos;
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos() throws BaseException
    {
        return getAllServiceInfos(null, null, null);
    }


    @Override
    public List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceInfos(ordering, offset, count, null);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllServiceInfos(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<ServiceInfo> serviceInfos = decoratedService.getAllServiceInfos(ordering, offset, count, forwardCursor);
        log.finer("END");
        return serviceInfos;
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllServiceInfoKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllServiceInfoKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findServiceInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ServiceInfoMockService.findServiceInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<ServiceInfo> serviceInfos = decoratedService.findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return serviceInfos;
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ServiceInfoMockService.findServiceInfoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ServiceInfoMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createServiceInfo(String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        return decoratedService.createServiceInfo(title, content, type, status, scheduledTime);
    }

    @Override
    public String createServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createServiceInfo(serviceInfo);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        return decoratedService.updateServiceInfo(guid, title, content, type, status, scheduledTime);
    }
        
    @Override
    public Boolean updateServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateServiceInfo(serviceInfo);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteServiceInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteServiceInfo(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteServiceInfo(serviceInfo);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteServiceInfos(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteServiceInfos(filter, params, values);
        return count;
    }

}
