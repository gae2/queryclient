package com.queryclient.ws;



public interface PagerStateStruct 
{
    String  getPagerMode();
    String  getPrimaryOrdering();
    String  getSecondaryOrdering();
    Long  getCurrentOffset();
    Long  getCurrentPage();
    Integer  getPageSize();
    Long  getTotalCount();
    Long  getLowerBoundTotalCount();
    Long  getPreviousPageOffset();
    Long  getNextPageOffset();
    Long  getLastPageOffset();
    Long  getLastPageIndex();
    Boolean  isFirstActionEnabled();
    Boolean  isPreviousActionEnabled();
    Boolean  isNextActionEnabled();
    Boolean  isLastActionEnabled();
    String  getNote();
    boolean isEmpty();
}
