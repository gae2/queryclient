package com.queryclient.ws.cert.dao;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

public final class Pmf
{
    private Pmf() {}

    // Initialization-on-demand holder.
    private static class PersistenceManagerFactoryHolder
    {
        private static final PersistenceManagerFactory INSTANCE = 
            JDOHelper.getPersistenceManagerFactory("transactions-optional-cert");
        // Etc.
    }

    public static PersistenceManagerFactory get() 
    {
        return PersistenceManagerFactoryHolder.INSTANCE;
    }

}
