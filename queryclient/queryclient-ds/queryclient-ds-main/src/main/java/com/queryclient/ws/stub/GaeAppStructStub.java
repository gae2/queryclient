package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "gaeAppStruct")
@XmlType(propOrder = {"groupId", "appId", "appDomain", "namespace", "acl", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GaeAppStructStub implements GaeAppStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GaeAppStructStub.class.getName());

    private String groupId;
    private String appId;
    private String appDomain;
    private String namespace;
    private Long acl;
    private String note;

    public GaeAppStructStub()
    {
        this(null);
    }
    public GaeAppStructStub(GaeAppStruct bean)
    {
        if(bean != null) {
            this.groupId = bean.getGroupId();
            this.appId = bean.getAppId();
            this.appDomain = bean.getAppDomain();
            this.namespace = bean.getNamespace();
            this.acl = bean.getAcl();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getGroupId()
    {
        return this.groupId;
    }
    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    @XmlElement
    public String getAppId()
    {
        return this.appId;
    }
    public void setAppId(String appId)
    {
        this.appId = appId;
    }

    @XmlElement
    public String getAppDomain()
    {
        return this.appDomain;
    }
    public void setAppDomain(String appDomain)
    {
        this.appDomain = appDomain;
    }

    @XmlElement
    public String getNamespace()
    {
        return this.namespace;
    }
    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    @XmlElement
    public Long getAcl()
    {
        return this.acl;
    }
    public void setAcl(Long acl)
    {
        this.acl = acl;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getGroupId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAppId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAppDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNamespace() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAcl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("groupId", this.groupId);
        dataMap.put("appId", this.appId);
        dataMap.put("appDomain", this.appDomain);
        dataMap.put("namespace", this.namespace);
        dataMap.put("acl", this.acl);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = groupId == null ? 0 : groupId.hashCode();
        _hash = 31 * _hash + delta;
        delta = appId == null ? 0 : appId.hashCode();
        _hash = 31 * _hash + delta;
        delta = appDomain == null ? 0 : appDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = namespace == null ? 0 : namespace.hashCode();
        _hash = 31 * _hash + delta;
        delta = acl == null ? 0 : acl.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static GaeAppStructStub convertBeanToStub(GaeAppStruct bean)
    {
        GaeAppStructStub stub = null;
        if(bean instanceof GaeAppStructStub) {
            stub = (GaeAppStructStub) bean;
        } else {
            if(bean != null) {
                stub = new GaeAppStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static GaeAppStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of GaeAppStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write GaeAppStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write GaeAppStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write GaeAppStructStub object as a string.", e);
        }
        
        return null;
    }
    public static GaeAppStructStub fromJsonString(String jsonStr)
    {
        try {
            GaeAppStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, GaeAppStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeAppStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeAppStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeAppStructStub object.", e);
        }
        
        return null;
    }

}
