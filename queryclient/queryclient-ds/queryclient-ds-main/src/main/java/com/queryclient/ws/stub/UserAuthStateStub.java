package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "userAuthState")
@XmlType(propOrder = {"guid", "managerApp", "appAcl", "gaeAppStub", "ownerUser", "userAcl", "providerId", "user", "username", "email", "openId", "deviceId", "sessionId", "authToken", "authStatus", "externalAuth", "externalIdStub", "status", "firstAuthTime", "lastAuthTime", "expirationTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAuthStateStub implements UserAuthState, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserAuthStateStub.class.getName());

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructStub gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String providerId;
    private String user;
    private String username;
    private String email;
    private String openId;
    private String deviceId;
    private String sessionId;
    private String authToken;
    private String authStatus;
    private String externalAuth;
    private ExternalUserIdStructStub externalId;
    private String status;
    private Long firstAuthTime;
    private Long lastAuthTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    public UserAuthStateStub()
    {
        this(null);
    }
    public UserAuthStateStub(UserAuthState bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.managerApp = bean.getManagerApp();
            this.appAcl = bean.getAppAcl();
            this.gaeApp = GaeAppStructStub.convertBeanToStub(bean.getGaeApp());
            this.ownerUser = bean.getOwnerUser();
            this.userAcl = bean.getUserAcl();
            this.providerId = bean.getProviderId();
            this.user = bean.getUser();
            this.username = bean.getUsername();
            this.email = bean.getEmail();
            this.openId = bean.getOpenId();
            this.deviceId = bean.getDeviceId();
            this.sessionId = bean.getSessionId();
            this.authToken = bean.getAuthToken();
            this.authStatus = bean.getAuthStatus();
            this.externalAuth = bean.getExternalAuth();
            this.externalId = ExternalUserIdStructStub.convertBeanToStub(bean.getExternalId());
            this.status = bean.getStatus();
            this.firstAuthTime = bean.getFirstAuthTime();
            this.lastAuthTime = bean.getLastAuthTime();
            this.expirationTime = bean.getExpirationTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    @XmlElement
    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    @XmlElement(name = "gaeApp")
    @JsonIgnore
    public GaeAppStructStub getGaeAppStub()
    {
        return this.gaeApp;
    }
    public void setGaeAppStub(GaeAppStructStub gaeApp)
    {
        this.gaeApp = gaeApp;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=GaeAppStructStub.class)
    public GaeAppStruct getGaeApp()
    {  
        return getGaeAppStub();
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if((gaeApp == null) || (gaeApp instanceof GaeAppStructStub)) {
            setGaeAppStub((GaeAppStructStub) gaeApp);
        } else {
            // TBD
            setGaeAppStub(GaeAppStructStub.convertBeanToStub(gaeApp));
        }
    }

    @XmlElement
    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    @XmlElement
    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    @XmlElement
    public String getProviderId()
    {
        return this.providerId;
    }
    public void setProviderId(String providerId)
    {
        this.providerId = providerId;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    @XmlElement
    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    @XmlElement
    public String getOpenId()
    {
        return this.openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    @XmlElement
    public String getDeviceId()
    {
        return this.deviceId;
    }
    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    @XmlElement
    public String getSessionId()
    {
        return this.sessionId;
    }
    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    @XmlElement
    public String getAuthToken()
    {
        return this.authToken;
    }
    public void setAuthToken(String authToken)
    {
        this.authToken = authToken;
    }

    @XmlElement
    public String getAuthStatus()
    {
        return this.authStatus;
    }
    public void setAuthStatus(String authStatus)
    {
        this.authStatus = authStatus;
    }

    @XmlElement
    public String getExternalAuth()
    {
        return this.externalAuth;
    }
    public void setExternalAuth(String externalAuth)
    {
        this.externalAuth = externalAuth;
    }

    @XmlElement(name = "externalId")
    @JsonIgnore
    public ExternalUserIdStructStub getExternalIdStub()
    {
        return this.externalId;
    }
    public void setExternalIdStub(ExternalUserIdStructStub externalId)
    {
        this.externalId = externalId;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ExternalUserIdStructStub.class)
    public ExternalUserIdStruct getExternalId()
    {  
        return getExternalIdStub();
    }
    public void setExternalId(ExternalUserIdStruct externalId)
    {
        if((externalId == null) || (externalId instanceof ExternalUserIdStructStub)) {
            setExternalIdStub((ExternalUserIdStructStub) externalId);
        } else {
            // TBD
            setExternalIdStub(ExternalUserIdStructStub.convertBeanToStub(externalId));
        }
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getFirstAuthTime()
    {
        return this.firstAuthTime;
    }
    public void setFirstAuthTime(Long firstAuthTime)
    {
        this.firstAuthTime = firstAuthTime;
    }

    @XmlElement
    public Long getLastAuthTime()
    {
        return this.lastAuthTime;
    }
    public void setLastAuthTime(Long lastAuthTime)
    {
        this.lastAuthTime = lastAuthTime;
    }

    @XmlElement
    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("ownerUser", this.ownerUser);
        dataMap.put("userAcl", this.userAcl);
        dataMap.put("providerId", this.providerId);
        dataMap.put("user", this.user);
        dataMap.put("username", this.username);
        dataMap.put("email", this.email);
        dataMap.put("openId", this.openId);
        dataMap.put("deviceId", this.deviceId);
        dataMap.put("sessionId", this.sessionId);
        dataMap.put("authToken", this.authToken);
        dataMap.put("authStatus", this.authStatus);
        dataMap.put("externalAuth", this.externalAuth);
        dataMap.put("externalId", this.externalId);
        dataMap.put("status", this.status);
        dataMap.put("firstAuthTime", this.firstAuthTime);
        dataMap.put("lastAuthTime", this.lastAuthTime);
        dataMap.put("expirationTime", this.expirationTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = ownerUser == null ? 0 : ownerUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAcl == null ? 0 : userAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = providerId == null ? 0 : providerId.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = openId == null ? 0 : openId.hashCode();
        _hash = 31 * _hash + delta;
        delta = deviceId == null ? 0 : deviceId.hashCode();
        _hash = 31 * _hash + delta;
        delta = sessionId == null ? 0 : sessionId.hashCode();
        _hash = 31 * _hash + delta;
        delta = authToken == null ? 0 : authToken.hashCode();
        _hash = 31 * _hash + delta;
        delta = authStatus == null ? 0 : authStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = externalAuth == null ? 0 : externalAuth.hashCode();
        _hash = 31 * _hash + delta;
        delta = externalId == null ? 0 : externalId.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = firstAuthTime == null ? 0 : firstAuthTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastAuthTime == null ? 0 : lastAuthTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static UserAuthStateStub convertBeanToStub(UserAuthState bean)
    {
        UserAuthStateStub stub = null;
        if(bean instanceof UserAuthStateStub) {
            stub = (UserAuthStateStub) bean;
        } else {
            if(bean != null) {
                stub = new UserAuthStateStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserAuthStateStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of UserAuthStateStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserAuthStateStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserAuthStateStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserAuthStateStub object as a string.", e);
        }
        
        return null;
    }
    public static UserAuthStateStub fromJsonString(String jsonStr)
    {
        try {
            UserAuthStateStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserAuthStateStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserAuthStateStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserAuthStateStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserAuthStateStub object.", e);
        }
        
        return null;
    }

}
