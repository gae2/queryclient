package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "referrerInfoStructs")
@XmlType(propOrder = {"referrerInfoStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferrerInfoStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ReferrerInfoStructListStub.class.getName());

    private List<ReferrerInfoStructStub> referrerInfoStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public ReferrerInfoStructListStub()
    {
        this(new ArrayList<ReferrerInfoStructStub>());
    }
    public ReferrerInfoStructListStub(List<ReferrerInfoStructStub> referrerInfoStructs)
    {
        this(referrerInfoStructs, null);
    }
    public ReferrerInfoStructListStub(List<ReferrerInfoStructStub> referrerInfoStructs, String forwardCursor)
    {
        this.referrerInfoStructs = referrerInfoStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(referrerInfoStructs == null) {
            return true;
        } else {
            return referrerInfoStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(referrerInfoStructs == null) {
            return 0;
        } else {
            return referrerInfoStructs.size();
        }
    }


    @XmlElement(name = "referrerInfoStruct")
    public List<ReferrerInfoStructStub> getReferrerInfoStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ReferrerInfoStructStub> getList()
    {
        return referrerInfoStructs;
    }
    public void setList(List<ReferrerInfoStructStub> referrerInfoStructs)
    {
        this.referrerInfoStructs = referrerInfoStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<ReferrerInfoStructStub> it = this.referrerInfoStructs.iterator();
        while(it.hasNext()) {
            ReferrerInfoStructStub referrerInfoStruct = it.next();
            sb.append(referrerInfoStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ReferrerInfoStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ReferrerInfoStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ReferrerInfoStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ReferrerInfoStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ReferrerInfoStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static ReferrerInfoStructListStub fromJsonString(String jsonStr)
    {
        try {
            ReferrerInfoStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ReferrerInfoStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ReferrerInfoStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ReferrerInfoStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ReferrerInfoStructListStub object.", e);
        }
        
        return null;
    }

}
