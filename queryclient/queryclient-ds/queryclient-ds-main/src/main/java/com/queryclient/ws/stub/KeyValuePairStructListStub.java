package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.KeyValuePairStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "keyValuePairStructs")
@XmlType(propOrder = {"keyValuePairStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeyValuePairStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeyValuePairStructListStub.class.getName());

    private List<KeyValuePairStructStub> keyValuePairStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public KeyValuePairStructListStub()
    {
        this(new ArrayList<KeyValuePairStructStub>());
    }
    public KeyValuePairStructListStub(List<KeyValuePairStructStub> keyValuePairStructs)
    {
        this(keyValuePairStructs, null);
    }
    public KeyValuePairStructListStub(List<KeyValuePairStructStub> keyValuePairStructs, String forwardCursor)
    {
        this.keyValuePairStructs = keyValuePairStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(keyValuePairStructs == null) {
            return true;
        } else {
            return keyValuePairStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(keyValuePairStructs == null) {
            return 0;
        } else {
            return keyValuePairStructs.size();
        }
    }


    @XmlElement(name = "keyValuePairStruct")
    public List<KeyValuePairStructStub> getKeyValuePairStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<KeyValuePairStructStub> getList()
    {
        return keyValuePairStructs;
    }
    public void setList(List<KeyValuePairStructStub> keyValuePairStructs)
    {
        this.keyValuePairStructs = keyValuePairStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<KeyValuePairStructStub> it = this.keyValuePairStructs.iterator();
        while(it.hasNext()) {
            KeyValuePairStructStub keyValuePairStruct = it.next();
            sb.append(keyValuePairStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static KeyValuePairStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of KeyValuePairStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write KeyValuePairStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write KeyValuePairStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write KeyValuePairStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static KeyValuePairStructListStub fromJsonString(String jsonStr)
    {
        try {
            KeyValuePairStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, KeyValuePairStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into KeyValuePairStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into KeyValuePairStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into KeyValuePairStructListStub object.", e);
        }
        
        return null;
    }

}
