package com.queryclient.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.bean.GaeAppStructBean;
import com.queryclient.ws.bean.UserPasswordBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.GaeAppStructDataObject;
import com.queryclient.ws.data.UserPasswordDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.UserPasswordService;


// UserPasswordMockService is a decorator.
// It can be used as a base class to mock UserPasswordService objects.
public abstract class UserPasswordMockService implements UserPasswordService
{
    private static final Logger log = Logger.getLogger(UserPasswordMockService.class.getName());

    // UserPasswordMockService uses the decorator design pattern.
    private UserPasswordService decoratedService;

    public UserPasswordMockService(UserPasswordService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected UserPasswordService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(UserPasswordService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // UserPassword related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserPassword getUserPassword(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserPassword(): guid = " + guid);
        UserPassword bean = decoratedService.getUserPassword(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserPassword(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getUserPassword(guid, field);
        return obj;
    }

    @Override
    public List<UserPassword> getUserPasswords(List<String> guids) throws BaseException
    {
        log.fine("getUserPasswords()");
        List<UserPassword> userPasswords = decoratedService.getUserPasswords(guids);
        log.finer("END");
        return userPasswords;
    }

    @Override
    public List<UserPassword> getAllUserPasswords() throws BaseException
    {
        return getAllUserPasswords(null, null, null);
    }


    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswords(ordering, offset, count, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserPasswords(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<UserPassword> userPasswords = decoratedService.getAllUserPasswords(ordering, offset, count, forwardCursor);
        log.finer("END");
        return userPasswords;
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserPasswordKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserPasswordMockService.findUserPasswords(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<UserPassword> userPasswords = decoratedService.findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return userPasswords;
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserPasswordMockService.findUserPasswordKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserPasswordMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUserPassword(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        return decoratedService.createUserPassword(managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

    @Override
    public String createUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createUserPassword(userPassword);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUserPassword(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        return decoratedService.updateUserPassword(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }
        
    @Override
    public Boolean updateUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateUserPassword(userPassword);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteUserPassword(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteUserPassword(userPassword);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteUserPasswords(filter, params, values);
        return count;
    }

}
