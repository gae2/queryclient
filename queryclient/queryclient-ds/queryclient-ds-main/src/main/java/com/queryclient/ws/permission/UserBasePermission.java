package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class UserBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserBasePermission.class.getName());

    public UserBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "User::" + action;
    }


}
