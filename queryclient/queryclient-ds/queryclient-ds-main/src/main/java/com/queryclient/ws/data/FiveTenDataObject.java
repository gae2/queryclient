package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.FiveTen;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class FiveTenDataObject extends KeyedDataObject implements FiveTen
{
    private static final Logger log = Logger.getLogger(FiveTenDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(FiveTenDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(FiveTenDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private Integer counter;

    @Persistent(defaultFetchGroup = "true")
    private String requesterIpAddress;

    public FiveTenDataObject()
    {
        this(null);
    }
    public FiveTenDataObject(String guid)
    {
        this(guid, null, null, null, null);
    }
    public FiveTenDataObject(String guid, Integer counter, String requesterIpAddress)
    {
        this(guid, counter, requesterIpAddress, null, null);
    }
    public FiveTenDataObject(String guid, Integer counter, String requesterIpAddress, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.counter = counter;
        this.requesterIpAddress = requesterIpAddress;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return FiveTenDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return FiveTenDataObject.composeKey(getGuid());
    }

    public Integer getCounter()
    {
        return this.counter;
    }
    public void setCounter(Integer counter)
    {
        this.counter = counter;
    }

    public String getRequesterIpAddress()
    {
        return this.requesterIpAddress;
    }
    public void setRequesterIpAddress(String requesterIpAddress)
    {
        this.requesterIpAddress = requesterIpAddress;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("counter", this.counter);
        dataMap.put("requesterIpAddress", this.requesterIpAddress);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        FiveTen thatObj = (FiveTen) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.counter == null && thatObj.getCounter() != null)
            || (this.counter != null && thatObj.getCounter() == null)
            || !this.counter.equals(thatObj.getCounter()) ) {
            return false;
        }
        if( (this.requesterIpAddress == null && thatObj.getRequesterIpAddress() != null)
            || (this.requesterIpAddress != null && thatObj.getRequesterIpAddress() == null)
            || !this.requesterIpAddress.equals(thatObj.getRequesterIpAddress()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = counter == null ? 0 : counter.hashCode();
        _hash = 31 * _hash + delta;
        delta = requesterIpAddress == null ? 0 : requesterIpAddress.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
