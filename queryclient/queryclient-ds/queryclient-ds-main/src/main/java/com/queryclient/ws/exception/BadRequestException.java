package com.queryclient.ws.exception;

import com.queryclient.ws.BaseException;


public class BadRequestException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public BadRequestException() 
    {
        super();
    }
    public BadRequestException(String message) 
    {
        super(message);
    }
   public BadRequestException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public BadRequestException(Throwable cause) 
    {
        super(cause);
    }

}
