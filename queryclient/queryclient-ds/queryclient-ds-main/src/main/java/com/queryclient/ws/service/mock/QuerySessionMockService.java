package com.queryclient.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.bean.ReferrerInfoStructBean;
import com.queryclient.ws.bean.QuerySessionBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.ReferrerInfoStructDataObject;
import com.queryclient.ws.data.QuerySessionDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.QuerySessionService;


// QuerySessionMockService is a decorator.
// It can be used as a base class to mock QuerySessionService objects.
public abstract class QuerySessionMockService implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(QuerySessionMockService.class.getName());

    // QuerySessionMockService uses the decorator design pattern.
    private QuerySessionService decoratedService;

    public QuerySessionMockService(QuerySessionService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected QuerySessionService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(QuerySessionService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // QuerySession related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getQuerySession(): guid = " + guid);
        QuerySession bean = decoratedService.getQuerySession(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getQuerySession(guid, field);
        return obj;
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        log.fine("getQuerySessions()");
        List<QuerySession> querySessions = decoratedService.getQuerySessions(guids);
        log.finer("END");
        return querySessions;
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return getAllQuerySessions(null, null, null);
    }


    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQuerySessions(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<QuerySession> querySessions = decoratedService.getAllQuerySessions(ordering, offset, count, forwardCursor);
        log.finer("END");
        return querySessions;
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQuerySessionKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QuerySessionMockService.findQuerySessions(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<QuerySession> querySessions = decoratedService.findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return querySessions;
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QuerySessionMockService.findQuerySessionKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QuerySessionMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        return decoratedService.createQuerySession(user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note);
    }

    @Override
    public String createQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createQuerySession(querySession);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        return decoratedService.updateQuerySession(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note);
    }
        
    @Override
    public Boolean updateQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateQuerySession(querySession);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteQuerySession(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteQuerySession(querySession);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteQuerySessions(filter, params, values);
        return count;
    }

}
