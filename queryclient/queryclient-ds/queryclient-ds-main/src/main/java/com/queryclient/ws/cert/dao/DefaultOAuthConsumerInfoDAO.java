package com.queryclient.ws.cert.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import com.google.appengine.api.datastore.Key;

import com.queryclient.ws.core.GUID;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.cert.dao.OAuthConsumerInfoDAO;
import com.queryclient.ws.cert.data.OAuthConsumerInfoDataObject;


public class DefaultOAuthConsumerInfoDAO implements OAuthConsumerInfoDAO
{
    private static final Logger log = Logger.getLogger(DefaultOAuthConsumerInfoDAO.class.getName()); 

    protected PersistenceManager getPersistenceManager()
    {
        // TBD: Use different types of PMFs (e.g., with different configs, etc.).
        PersistenceManager pm = Pmf.get().getPersistenceManager();

        // TBD: Set these in jdoconfig file????
        pm.setCopyOnAttach(false);
        pm.setDetachAllOnCommit(true);
        pm.getFetchPlan().setMaxFetchDepth(3);

        return pm;
    }


    // Returns the oAuthConsumerInfo for the given guid.
    // Returns null if none is found in the datastore.
    @Override
    public OAuthConsumerInfoDataObject getOAuthConsumerInfo(String guid) throws BaseException
    {
        OAuthConsumerInfoDataObject oAuthConsumerInfo = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                Key key = OAuthConsumerInfoDataObject.composeKey(guid);
                oAuthConsumerInfo = pm.getObjectById(OAuthConsumerInfoDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve oAuthConsumerInfo for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }
	    return oAuthConsumerInfo;
	}

    @Override
    public List<OAuthConsumerInfoDataObject> getAllOAuthConsumerInfos() throws BaseException
	{
	    return getAllOAuthConsumerInfos(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<OAuthConsumerInfoDataObject> getAllOAuthConsumerInfos(String ordering, Long offset, Integer count) throws BaseException
	{
    	List<OAuthConsumerInfoDataObject> oAuthConsumerInfos = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(OAuthConsumerInfoDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            oAuthConsumerInfos = (List<OAuthConsumerInfoDataObject>) q.execute();
            oAuthConsumerInfos.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            /*
            // ???
            Collection<OAuthConsumerInfoDataObject> rs_oAuthConsumerInfos = (Collection<OAuthConsumerInfoDataObject>) q.execute();
            if(rs_oAuthConsumerInfos == null) {
                log.log(Level.WARNING, "Failed to retrieve all oAuthConsumerInfos.");
                oAuthConsumerInfos = new ArrayList<OAuthConsumerInfoDataObject>();  // ???           
            } else {
                oAuthConsumerInfos = new ArrayList<OAuthConsumerInfoDataObject>(pm.detachCopyAll(rs_oAuthConsumerInfos));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all oAuthConsumerInfos.", ex);
            //oAuthConsumerInfos = new ArrayList<OAuthConsumerInfoDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all oAuthConsumerInfos.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return oAuthConsumerInfos;
    }

    @Override
	public List<OAuthConsumerInfoDataObject> findOAuthConsumerInfos(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOAuthConsumerInfos(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OAuthConsumerInfoDataObject> findOAuthConsumerInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOAuthConsumerInfos(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<OAuthConsumerInfoDataObject> findOAuthConsumerInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultOAuthConsumerInfoDAO.findOAuthConsumerInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findOAuthConsumerInfos() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<OAuthConsumerInfoDataObject> oAuthConsumerInfos = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(OAuthConsumerInfoDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                oAuthConsumerInfos = (List<OAuthConsumerInfoDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                oAuthConsumerInfos = (List<OAuthConsumerInfoDataObject>) q.execute();
            }
            oAuthConsumerInfos.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(OAuthConsumerInfoDataObject dobj : oAuthConsumerInfos) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find oAuthConsumerInfos because index is missing.", ex);
            //oAuthConsumerInfos = new ArrayList<OAuthConsumerInfoDataObject>();  // ???
            throw new DataStoreException("Failed to find oAuthConsumerInfos because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find oAuthConsumerInfos meeting the criterion.", ex);
            //oAuthConsumerInfos = new ArrayList<OAuthConsumerInfoDataObject>();  // ???
            throw new DataStoreException("Failed to find oAuthConsumerInfos meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return oAuthConsumerInfos;
	}

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultOAuthConsumerInfoDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(OAuthConsumerInfoDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = new Long((Integer) q.executeWithArray(values.toArray(new Object[0])));
            } else {
                count = new Long((Integer) q.execute());
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get oAuthConsumerInfo count because index is missing.", ex);
            throw new DataStoreException("Failed to get oAuthConsumerInfo count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get oAuthConsumerInfo count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get oAuthConsumerInfo count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return count;
    }

	// Stores the oAuthConsumerInfo in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeOAuthConsumerInfo(OAuthConsumerInfoDataObject oAuthConsumerInfo) throws BaseException
    {
        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
	    try {
            //tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = oAuthConsumerInfo.getCreatedTime();
            if(createdTime == null) {
                createdTime = (new Date()).getTime();
                oAuthConsumerInfo.setCreatedTime(createdTime);
            }
            Long modifiedTime = oAuthConsumerInfo.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                oAuthConsumerInfo.setModifiedTime(createdTime);
            }
            pm.makePersistent(oAuthConsumerInfo); 
            //tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = oAuthConsumerInfo.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store oAuthConsumerInfo because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store oAuthConsumerInfo because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store oAuthConsumerInfo.", ex);
            throw new DataStoreException("Failed to store oAuthConsumerInfo.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return guid;
    }
	
    @Override
    public String createOAuthConsumerInfo(OAuthConsumerInfoDataObject oAuthConsumerInfo) throws BaseException
    {
        // The createdTime field will be automatically set in storeOAuthConsumerInfo().
        //Long createdTime = oAuthConsumerInfo.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = (new Date()).getTime();
        //    oAuthConsumerInfo.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = oAuthConsumerInfo.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    oAuthConsumerInfo.setModifiedTime(createdTime);
        //}
        return storeOAuthConsumerInfo(oAuthConsumerInfo);
    }

    @Override
	public Boolean updateOAuthConsumerInfo(OAuthConsumerInfoDataObject oAuthConsumerInfo) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeOAuthConsumerInfo()
	    // (in which case modifiedTime might be updated again).
	    oAuthConsumerInfo.setModifiedTime((new Date()).getTime());
	    String guid = storeOAuthConsumerInfo(oAuthConsumerInfo);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteOAuthConsumerInfo(OAuthConsumerInfoDataObject oAuthConsumerInfo) throws BaseException
    {
        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            pm.deletePersistent(oAuthConsumerInfo);
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete oAuthConsumerInfo because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete oAuthConsumerInfo because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete oAuthConsumerInfo.", ex);
            throw new DataStoreException("Failed to delete oAuthConsumerInfo.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return suc;
    }

    @Override
    public Boolean deleteOAuthConsumerInfo(String guid) throws BaseException
    {
        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                Key key = OAuthConsumerInfoDataObject.composeKey(guid);
                OAuthConsumerInfoDataObject oAuthConsumerInfo = pm.getObjectById(OAuthConsumerInfoDataObject.class, key);
                pm.deletePersistent(oAuthConsumerInfo);
                //tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                log.log(Level.WARNING, "Failed to delete oAuthConsumerInfo because the datastore is currently read-only.", ex);
                throw new ServiceUnavailableException("Failed to delete oAuthConsumerInfo because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete oAuthConsumerInfo for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete oAuthConsumerInfo for guid = " + guid, ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }
        return suc;
	}

    @Override
    public Long deleteOAuthConsumerInfos(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultOAuthConsumerInfoDAO.deleteOAuthConsumerInfos(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(OAuthConsumerInfoDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteoAuthConsumerInfos because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete oAuthConsumerInfos because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete oAuthConsumerInfos because index is missing", ex);
            throw new DataStoreException("Failed to delete oAuthConsumerInfos because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete oAuthConsumerInfos", ex);
            throw new DataStoreException("Failed to delete oAuthConsumerInfos", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        return count;
    }

}
