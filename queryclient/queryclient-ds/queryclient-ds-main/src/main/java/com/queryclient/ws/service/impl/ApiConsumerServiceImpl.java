package com.queryclient.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.ApiConsumer;
import com.queryclient.ws.bean.ApiConsumerBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.ApiConsumerDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.ApiConsumerService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ApiConsumerServiceImpl implements ApiConsumerService
{
    private static final Logger log = Logger.getLogger(ApiConsumerServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ApiConsumer related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ApiConsumer getApiConsumer(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ApiConsumerDataObject dataObj = getDAOFactory().getApiConsumerDAO().getApiConsumer(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ApiConsumerDataObject for guid = " + guid);
            return null;  // ????
        }
        ApiConsumerBean bean = new ApiConsumerBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getApiConsumer(String guid, String field) throws BaseException
    {
        ApiConsumerDataObject dataObj = getDAOFactory().getApiConsumerDAO().getApiConsumer(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ApiConsumerDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("aeryId")) {
            return dataObj.getAeryId();
        } else if(field.equals("name")) {
            return dataObj.getName();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("appKey")) {
            return dataObj.getAppKey();
        } else if(field.equals("appSecret")) {
            return dataObj.getAppSecret();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ApiConsumer> getApiConsumers(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ApiConsumer> list = new ArrayList<ApiConsumer>();
        List<ApiConsumerDataObject> dataObjs = getDAOFactory().getApiConsumerDAO().getApiConsumers(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ApiConsumerDataObject list.");
        } else {
            Iterator<ApiConsumerDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ApiConsumerDataObject dataObj = (ApiConsumerDataObject) it.next();
                list.add(new ApiConsumerBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers() throws BaseException
    {
        return getAllApiConsumers(null, null, null);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllApiConsumers(ordering, offset, count, null);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllApiConsumers(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ApiConsumer> list = new ArrayList<ApiConsumer>();
        List<ApiConsumerDataObject> dataObjs = getDAOFactory().getApiConsumerDAO().getAllApiConsumers(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ApiConsumerDataObject list.");
        } else {
            Iterator<ApiConsumerDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ApiConsumerDataObject dataObj = (ApiConsumerDataObject) it.next();
                list.add(new ApiConsumerBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllApiConsumerKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllApiConsumerKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getApiConsumerDAO().getAllApiConsumerKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ApiConsumer key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findApiConsumers(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ApiConsumerServiceImpl.findApiConsumers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ApiConsumer> list = new ArrayList<ApiConsumer>();
        List<ApiConsumerDataObject> dataObjs = getDAOFactory().getApiConsumerDAO().findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find apiConsumers for the given criterion.");
        } else {
            Iterator<ApiConsumerDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ApiConsumerDataObject dataObj = (ApiConsumerDataObject) it.next();
                list.add(new ApiConsumerBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ApiConsumerServiceImpl.findApiConsumerKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getApiConsumerDAO().findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ApiConsumer keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ApiConsumerServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getApiConsumerDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createApiConsumer(String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        ApiConsumerDataObject dataObj = new ApiConsumerDataObject(null, aeryId, name, description, appKey, appSecret, status);
        return createApiConsumer(dataObj);
    }

    @Override
    public String createApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("BEGIN");

        // Param apiConsumer cannot be null.....
        if(apiConsumer == null) {
            log.log(Level.INFO, "Param apiConsumer is null!");
            throw new BadRequestException("Param apiConsumer object is null!");
        }
        ApiConsumerDataObject dataObj = null;
        if(apiConsumer instanceof ApiConsumerDataObject) {
            dataObj = (ApiConsumerDataObject) apiConsumer;
        } else if(apiConsumer instanceof ApiConsumerBean) {
            dataObj = ((ApiConsumerBean) apiConsumer).toDataObject();
        } else {  // if(apiConsumer instanceof ApiConsumer)
            //dataObj = new ApiConsumerDataObject(null, apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ApiConsumerDataObject(apiConsumer.getGuid(), apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
        }
        String guid = getDAOFactory().getApiConsumerDAO().createApiConsumer(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateApiConsumer(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ApiConsumerDataObject dataObj = new ApiConsumerDataObject(guid, aeryId, name, description, appKey, appSecret, status);
        return updateApiConsumer(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("BEGIN");

        // Param apiConsumer cannot be null.....
        if(apiConsumer == null || apiConsumer.getGuid() == null) {
            log.log(Level.INFO, "Param apiConsumer or its guid is null!");
            throw new BadRequestException("Param apiConsumer object or its guid is null!");
        }
        ApiConsumerDataObject dataObj = null;
        if(apiConsumer instanceof ApiConsumerDataObject) {
            dataObj = (ApiConsumerDataObject) apiConsumer;
        } else if(apiConsumer instanceof ApiConsumerBean) {
            dataObj = ((ApiConsumerBean) apiConsumer).toDataObject();
        } else {  // if(apiConsumer instanceof ApiConsumer)
            dataObj = new ApiConsumerDataObject(apiConsumer.getGuid(), apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
        }
        Boolean suc = getDAOFactory().getApiConsumerDAO().updateApiConsumer(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteApiConsumer(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getApiConsumerDAO().deleteApiConsumer(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("BEGIN");

        // Param apiConsumer cannot be null.....
        if(apiConsumer == null || apiConsumer.getGuid() == null) {
            log.log(Level.INFO, "Param apiConsumer or its guid is null!");
            throw new BadRequestException("Param apiConsumer object or its guid is null!");
        }
        ApiConsumerDataObject dataObj = null;
        if(apiConsumer instanceof ApiConsumerDataObject) {
            dataObj = (ApiConsumerDataObject) apiConsumer;
        } else if(apiConsumer instanceof ApiConsumerBean) {
            dataObj = ((ApiConsumerBean) apiConsumer).toDataObject();
        } else {  // if(apiConsumer instanceof ApiConsumer)
            dataObj = new ApiConsumerDataObject(apiConsumer.getGuid(), apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
        }
        Boolean suc = getDAOFactory().getApiConsumerDAO().deleteApiConsumer(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteApiConsumers(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getApiConsumerDAO().deleteApiConsumers(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
