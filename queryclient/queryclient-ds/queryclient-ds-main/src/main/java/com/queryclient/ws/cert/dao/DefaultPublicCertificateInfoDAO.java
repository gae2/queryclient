package com.queryclient.ws.cert.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import com.google.appengine.api.datastore.Key;

import com.queryclient.ws.core.GUID;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.cert.dao.PublicCertificateInfoDAO;
import com.queryclient.ws.cert.data.PublicCertificateInfoDataObject;


public class DefaultPublicCertificateInfoDAO implements PublicCertificateInfoDAO
{
    private static final Logger log = Logger.getLogger(DefaultPublicCertificateInfoDAO.class.getName()); 

    protected PersistenceManager getPersistenceManager()
    {
        // TBD: Use different types of PMFs (e.g., with different configs, etc.).
        PersistenceManager pm = Pmf.get().getPersistenceManager();

        // TBD: Set these in jdoconfig file????
        pm.setCopyOnAttach(false);
        pm.setDetachAllOnCommit(true);
        pm.getFetchPlan().setMaxFetchDepth(3);

        return pm;
    }


    // Returns the publicCertificateInfo for the given guid.
    // Returns null if none is found in the datastore.
    @Override
    public PublicCertificateInfoDataObject getPublicCertificateInfo(String guid) throws BaseException
    {
        PublicCertificateInfoDataObject publicCertificateInfo = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                Key key = PublicCertificateInfoDataObject.composeKey(guid);
                publicCertificateInfo = pm.getObjectById(PublicCertificateInfoDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve publicCertificateInfo for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }
	    return publicCertificateInfo;
	}

    @Override
    public List<PublicCertificateInfoDataObject> getAllPublicCertificateInfos() throws BaseException
	{
	    return getAllPublicCertificateInfos(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<PublicCertificateInfoDataObject> getAllPublicCertificateInfos(String ordering, Long offset, Integer count) throws BaseException
	{
    	List<PublicCertificateInfoDataObject> publicCertificateInfos = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(PublicCertificateInfoDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            publicCertificateInfos = (List<PublicCertificateInfoDataObject>) q.execute();
            publicCertificateInfos.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            /*
            // ???
            Collection<PublicCertificateInfoDataObject> rs_publicCertificateInfos = (Collection<PublicCertificateInfoDataObject>) q.execute();
            if(rs_publicCertificateInfos == null) {
                log.log(Level.WARNING, "Failed to retrieve all publicCertificateInfos.");
                publicCertificateInfos = new ArrayList<PublicCertificateInfoDataObject>();  // ???           
            } else {
                publicCertificateInfos = new ArrayList<PublicCertificateInfoDataObject>(pm.detachCopyAll(rs_publicCertificateInfos));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all publicCertificateInfos.", ex);
            //publicCertificateInfos = new ArrayList<PublicCertificateInfoDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all publicCertificateInfos.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return publicCertificateInfos;
    }

    @Override
	public List<PublicCertificateInfoDataObject> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findPublicCertificateInfos(filter, ordering, params, values, null, null);
    }

    @Override
	public List<PublicCertificateInfoDataObject> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findPublicCertificateInfos(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<PublicCertificateInfoDataObject> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultPublicCertificateInfoDAO.findPublicCertificateInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findPublicCertificateInfos() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<PublicCertificateInfoDataObject> publicCertificateInfos = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(PublicCertificateInfoDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                publicCertificateInfos = (List<PublicCertificateInfoDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                publicCertificateInfos = (List<PublicCertificateInfoDataObject>) q.execute();
            }
            publicCertificateInfos.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(PublicCertificateInfoDataObject dobj : publicCertificateInfos) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find publicCertificateInfos because index is missing.", ex);
            //publicCertificateInfos = new ArrayList<PublicCertificateInfoDataObject>();  // ???
            throw new DataStoreException("Failed to find publicCertificateInfos because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find publicCertificateInfos meeting the criterion.", ex);
            //publicCertificateInfos = new ArrayList<PublicCertificateInfoDataObject>();  // ???
            throw new DataStoreException("Failed to find publicCertificateInfos meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return publicCertificateInfos;
	}

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultPublicCertificateInfoDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(PublicCertificateInfoDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = new Long((Integer) q.executeWithArray(values.toArray(new Object[0])));
            } else {
                count = new Long((Integer) q.execute());
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get publicCertificateInfo count because index is missing.", ex);
            throw new DataStoreException("Failed to get publicCertificateInfo count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get publicCertificateInfo count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get publicCertificateInfo count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return count;
    }

	// Stores the publicCertificateInfo in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storePublicCertificateInfo(PublicCertificateInfoDataObject publicCertificateInfo) throws BaseException
    {
        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
	    try {
            //tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = publicCertificateInfo.getCreatedTime();
            if(createdTime == null) {
                createdTime = (new Date()).getTime();
                publicCertificateInfo.setCreatedTime(createdTime);
            }
            Long modifiedTime = publicCertificateInfo.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                publicCertificateInfo.setModifiedTime(createdTime);
            }
            pm.makePersistent(publicCertificateInfo); 
            //tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = publicCertificateInfo.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store publicCertificateInfo because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store publicCertificateInfo because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store publicCertificateInfo.", ex);
            throw new DataStoreException("Failed to store publicCertificateInfo.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return guid;
    }
	
    @Override
    public String createPublicCertificateInfo(PublicCertificateInfoDataObject publicCertificateInfo) throws BaseException
    {
        // The createdTime field will be automatically set in storePublicCertificateInfo().
        //Long createdTime = publicCertificateInfo.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = (new Date()).getTime();
        //    publicCertificateInfo.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = publicCertificateInfo.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    publicCertificateInfo.setModifiedTime(createdTime);
        //}
        return storePublicCertificateInfo(publicCertificateInfo);
    }

    @Override
	public Boolean updatePublicCertificateInfo(PublicCertificateInfoDataObject publicCertificateInfo) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storePublicCertificateInfo()
	    // (in which case modifiedTime might be updated again).
	    publicCertificateInfo.setModifiedTime((new Date()).getTime());
	    String guid = storePublicCertificateInfo(publicCertificateInfo);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deletePublicCertificateInfo(PublicCertificateInfoDataObject publicCertificateInfo) throws BaseException
    {
        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            pm.deletePersistent(publicCertificateInfo);
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete publicCertificateInfo because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete publicCertificateInfo because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete publicCertificateInfo.", ex);
            throw new DataStoreException("Failed to delete publicCertificateInfo.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return suc;
    }

    @Override
    public Boolean deletePublicCertificateInfo(String guid) throws BaseException
    {
        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                Key key = PublicCertificateInfoDataObject.composeKey(guid);
                PublicCertificateInfoDataObject publicCertificateInfo = pm.getObjectById(PublicCertificateInfoDataObject.class, key);
                pm.deletePersistent(publicCertificateInfo);
                //tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                log.log(Level.WARNING, "Failed to delete publicCertificateInfo because the datastore is currently read-only.", ex);
                throw new ServiceUnavailableException("Failed to delete publicCertificateInfo because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete publicCertificateInfo for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete publicCertificateInfo for guid = " + guid, ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }
        return suc;
	}

    @Override
    public Long deletePublicCertificateInfos(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultPublicCertificateInfoDAO.deletePublicCertificateInfos(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(PublicCertificateInfoDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletepublicCertificateInfos because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete publicCertificateInfos because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete publicCertificateInfos because index is missing", ex);
            throw new DataStoreException("Failed to delete publicCertificateInfos because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete publicCertificateInfos", ex);
            throw new DataStoreException("Failed to delete publicCertificateInfos", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        return count;
    }

}
