package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "keys")
@XmlType(propOrder = {"key", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeyListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeyListStub.class.getName());

    private List<String> keys = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public KeyListStub()
    {
        this(new ArrayList<String>());
    }
    public KeyListStub(List<String> keys)
    {
        this(keys, null);
    }
    public KeyListStub(List<String> keys, String forwardCursor)
    {
        this.keys = keys;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(keys == null) {
            return true;
        } else {
            return keys.isEmpty();
        }
    }
    public int getSize()
    {
        if(keys == null) {
            return 0;
        } else {
            return keys.size();
        }
    }


    @XmlElement(name = "key")
    public List<String> getKey()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<String> getList()
    {
        return keys;
    }
    public void setList(List<String> keys)
    {
        this.keys = keys;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<String> it = this.keys.iterator();
        while(it.hasNext()) {
            String key = it.next();
            sb.append(key.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static KeyListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of KeyListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write KeyListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write KeyListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write KeyListStub object as a string.", e);
        }
        
        return null;
    }
    public static KeyListStub fromJsonString(String jsonStr)
    {
        try {
            KeyListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, KeyListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into KeyListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into KeyListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into KeyListStub object.", e);
        }
        
        return null;
    }

}
