package com.queryclient.ws.service;

import java.util.List;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UserPasswordService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UserPassword getUserPassword(String guid) throws BaseException;
    Object getUserPassword(String guid, String field) throws BaseException;
    List<UserPassword> getUserPasswords(List<String> guids) throws BaseException;
    List<UserPassword> getAllUserPasswords() throws BaseException;
    /* @Deprecated */ List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException;
    List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUserPassword(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException;
    //String createUserPassword(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserPassword?)
    String createUserPassword(UserPassword userPassword) throws BaseException;          // Returns Guid.  (Return UserPassword?)
    Boolean updateUserPassword(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException;
    //Boolean updateUserPassword(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserPassword(UserPassword userPassword) throws BaseException;
    Boolean deleteUserPassword(String guid) throws BaseException;
    Boolean deleteUserPassword(UserPassword userPassword) throws BaseException;
    Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException;

//    Integer createUserPasswords(List<UserPassword> userPasswords) throws BaseException;
//    Boolean updateeUserPasswords(List<UserPassword> userPasswords) throws BaseException;

}
