package com.queryclient.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.bean.DummyEntityBean;
import com.queryclient.ws.stub.DummyEntityListStub;
import com.queryclient.ws.stub.DummyEntityStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.DummyEntityResource;

// MockDummyEntityResource is a decorator.
// It can be used as a base class to mock DummyEntityResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/dummyEntities/")
public abstract class MockDummyEntityResource implements DummyEntityResource
{
    private static final Logger log = Logger.getLogger(MockDummyEntityResource.class.getName());

    // MockDummyEntityResource uses the decorator design pattern.
    private DummyEntityResource decoratedResource;

    public MockDummyEntityResource(DummyEntityResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected DummyEntityResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(DummyEntityResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDummyEntities(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getDummyEntityKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getDummyEntityKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getDummyEntity(String guid) throws BaseResourceException
    {
        return decoratedResource.getDummyEntity(guid);
    }

    @Override
    public Response getDummyEntity(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getDummyEntity(guid, field);
    }

    @Override
    public Response createDummyEntity(DummyEntityStub dummyEntity) throws BaseResourceException
    {
        return decoratedResource.createDummyEntity(dummyEntity);
    }

    @Override
    public Response updateDummyEntity(String guid, DummyEntityStub dummyEntity) throws BaseResourceException
    {
        return decoratedResource.updateDummyEntity(guid, dummyEntity);
    }

    @Override
    public Response updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseResourceException
    {
        return decoratedResource.updateDummyEntity(guid, user, name, content, maxLength, expired, status);
    }

    @Override
    public Response deleteDummyEntity(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteDummyEntity(guid);
    }

    @Override
    public Response deleteDummyEntities(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteDummyEntities(filter, params, values);
    }


}
