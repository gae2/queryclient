package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ServiceInfo;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "serviceInfo")
@XmlType(propOrder = {"guid", "title", "content", "type", "status", "scheduledTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceInfoStub implements ServiceInfo, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ServiceInfoStub.class.getName());

    private String guid;
    private String title;
    private String content;
    private String type;
    private String status;
    private Long scheduledTime;
    private Long createdTime;
    private Long modifiedTime;

    public ServiceInfoStub()
    {
        this(null);
    }
    public ServiceInfoStub(ServiceInfo bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.title = bean.getTitle();
            this.content = bean.getContent();
            this.type = bean.getType();
            this.status = bean.getStatus();
            this.scheduledTime = bean.getScheduledTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    @XmlElement
    public String getContent()
    {
        return this.content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    @XmlElement
    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getScheduledTime()
    {
        return this.scheduledTime;
    }
    public void setScheduledTime(Long scheduledTime)
    {
        this.scheduledTime = scheduledTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("title", this.title);
        dataMap.put("content", this.content);
        dataMap.put("type", this.type);
        dataMap.put("status", this.status);
        dataMap.put("scheduledTime", this.scheduledTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = content == null ? 0 : content.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = scheduledTime == null ? 0 : scheduledTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ServiceInfoStub convertBeanToStub(ServiceInfo bean)
    {
        ServiceInfoStub stub = null;
        if(bean instanceof ServiceInfoStub) {
            stub = (ServiceInfoStub) bean;
        } else {
            if(bean != null) {
                stub = new ServiceInfoStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ServiceInfoStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of ServiceInfoStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ServiceInfoStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ServiceInfoStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ServiceInfoStub object as a string.", e);
        }
        
        return null;
    }
    public static ServiceInfoStub fromJsonString(String jsonStr)
    {
        try {
            ServiceInfoStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ServiceInfoStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ServiceInfoStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ServiceInfoStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ServiceInfoStub object.", e);
        }
        
        return null;
    }

}
