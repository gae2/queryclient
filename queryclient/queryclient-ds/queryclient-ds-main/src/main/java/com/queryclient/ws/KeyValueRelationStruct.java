package com.queryclient.ws;



public interface KeyValueRelationStruct extends KeyValuePairStruct
{
    String  getRelation();
    boolean isEmpty();
}
