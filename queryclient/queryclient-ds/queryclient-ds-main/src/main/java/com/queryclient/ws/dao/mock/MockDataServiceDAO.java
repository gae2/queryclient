package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.DataServiceDAO;
import com.queryclient.ws.data.DataServiceDataObject;


// MockDataServiceDAO is a decorator.
// It can be used as a base class to mock DataServiceDAO objects.
public abstract class MockDataServiceDAO implements DataServiceDAO
{
    private static final Logger log = Logger.getLogger(MockDataServiceDAO.class.getName()); 

    // MockDataServiceDAO uses the decorator design pattern.
    private DataServiceDAO decoratedDAO;

    public MockDataServiceDAO(DataServiceDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected DataServiceDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(DataServiceDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public DataServiceDataObject getDataService(String guid) throws BaseException
    {
        return decoratedDAO.getDataService(guid);
	}

    @Override
    public List<DataServiceDataObject> getDataServices(List<String> guids) throws BaseException
    {
        return decoratedDAO.getDataServices(guids);
    }

    @Override
    public List<DataServiceDataObject> getAllDataServices() throws BaseException
	{
	    return getAllDataServices(null, null, null);
    }


    @Override
    public List<DataServiceDataObject> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllDataServices(ordering, offset, count, null);
    }

    @Override
    public List<DataServiceDataObject> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllDataServices(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllDataServiceKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findDataServices(filter, ordering, params, values, null, null);
    }

    @Override
	public List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findDataServices(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<DataServiceDataObject> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createDataService(DataServiceDataObject dataService) throws BaseException
    {
        return decoratedDAO.createDataService( dataService);
    }

    @Override
	public Boolean updateDataService(DataServiceDataObject dataService) throws BaseException
	{
        return decoratedDAO.updateDataService(dataService);
	}
	
    @Override
    public Boolean deleteDataService(DataServiceDataObject dataService) throws BaseException
    {
        return decoratedDAO.deleteDataService(dataService);
    }

    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        return decoratedDAO.deleteDataService(guid);
	}

    @Override
    public Long deleteDataServices(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteDataServices(filter, params, values);
    }

}
