package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.User;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "users")
@XmlType(propOrder = {"user", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserListStub.class.getName());

    private List<UserStub> users = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public UserListStub()
    {
        this(new ArrayList<UserStub>());
    }
    public UserListStub(List<UserStub> users)
    {
        this(users, null);
    }
    public UserListStub(List<UserStub> users, String forwardCursor)
    {
        this.users = users;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(users == null) {
            return true;
        } else {
            return users.isEmpty();
        }
    }
    public int getSize()
    {
        if(users == null) {
            return 0;
        } else {
            return users.size();
        }
    }


    @XmlElement(name = "user")
    public List<UserStub> getUser()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserStub> getList()
    {
        return users;
    }
    public void setList(List<UserStub> users)
    {
        this.users = users;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<UserStub> it = this.users.iterator();
        while(it.hasNext()) {
            UserStub user = it.next();
            sb.append(user.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserListStub fromJsonString(String jsonStr)
    {
        try {
            UserListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserListStub object.", e);
        }
        
        return null;
    }

}
