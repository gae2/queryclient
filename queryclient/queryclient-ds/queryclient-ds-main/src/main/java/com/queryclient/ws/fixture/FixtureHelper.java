package com.queryclient.ws.fixture;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.config.Config;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.bean.ApiConsumerBean;
import com.queryclient.ws.bean.UserBean;
import com.queryclient.ws.bean.UserPasswordBean;
import com.queryclient.ws.bean.ExternalUserAuthBean;
import com.queryclient.ws.bean.UserAuthStateBean;
import com.queryclient.ws.bean.DataServiceBean;
import com.queryclient.ws.bean.ServiceEndpointBean;
import com.queryclient.ws.bean.QuerySessionBean;
import com.queryclient.ws.bean.QueryRecordBean;
import com.queryclient.ws.bean.DummyEntityBean;
import com.queryclient.ws.bean.ServiceInfoBean;
import com.queryclient.ws.bean.FiveTenBean;
import com.queryclient.ws.stub.ApiConsumerStub;
import com.queryclient.ws.stub.UserStub;
import com.queryclient.ws.stub.UserPasswordStub;
import com.queryclient.ws.stub.ExternalUserAuthStub;
import com.queryclient.ws.stub.UserAuthStateStub;
import com.queryclient.ws.stub.DataServiceStub;
import com.queryclient.ws.stub.ServiceEndpointStub;
import com.queryclient.ws.stub.QuerySessionStub;
import com.queryclient.ws.stub.QueryRecordStub;
import com.queryclient.ws.stub.DummyEntityStub;
import com.queryclient.ws.stub.ServiceInfoStub;
import com.queryclient.ws.stub.FiveTenStub;
import com.queryclient.ws.stub.ApiConsumerListStub;
import com.queryclient.ws.stub.UserListStub;
import com.queryclient.ws.stub.UserPasswordListStub;
import com.queryclient.ws.stub.ExternalUserAuthListStub;
import com.queryclient.ws.stub.UserAuthStateListStub;
import com.queryclient.ws.stub.DataServiceListStub;
import com.queryclient.ws.stub.ServiceEndpointListStub;
import com.queryclient.ws.stub.QuerySessionListStub;
import com.queryclient.ws.stub.QueryRecordListStub;
import com.queryclient.ws.stub.DummyEntityListStub;
import com.queryclient.ws.stub.ServiceInfoListStub;
import com.queryclient.ws.stub.FiveTenListStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.impl.ApiConsumerResourceImpl;
import com.queryclient.ws.resource.impl.UserResourceImpl;
import com.queryclient.ws.resource.impl.UserPasswordResourceImpl;
import com.queryclient.ws.resource.impl.ExternalUserAuthResourceImpl;
import com.queryclient.ws.resource.impl.UserAuthStateResourceImpl;
import com.queryclient.ws.resource.impl.DataServiceResourceImpl;
import com.queryclient.ws.resource.impl.ServiceEndpointResourceImpl;
import com.queryclient.ws.resource.impl.QuerySessionResourceImpl;
import com.queryclient.ws.resource.impl.QueryRecordResourceImpl;
import com.queryclient.ws.resource.impl.DummyEntityResourceImpl;
import com.queryclient.ws.resource.impl.ServiceInfoResourceImpl;
import com.queryclient.ws.resource.impl.FiveTenResourceImpl;


// "Helper" functions...
// Get the list of fixture files,
// Read them, and
// Load the data into the datastore, etc....
// See the note in FixtureUtil...
public class FixtureHelper
{
    private static final Logger log = Logger.getLogger(FixtureHelper.class.getName());

    // ???
    private static final String CONFIG_KEY_FIXTURE_LOAD = "queryclient.fixture.load";
    private static final String CONFIG_KEY_FIXTURE_DIR = "queryclient.fixture.directory";
    // ...

    // Dummy var.
    protected boolean dummyFalse1 = false;
    // ...

    
    private FixtureHelper()
    {
        // ...
    }
    
    // Initialization-on-demand holder.
    private static class FixtureHelperHolder
    {
        private static final FixtureHelper INSTANCE = new FixtureHelper();
    }

    // Singleton method
    public static FixtureHelper getInstance()
    {
        return FixtureHelperHolder.INSTANCE;
    }


    public boolean isFixtureLoad()
    {
        // temporary
        return Config.getInstance().getBoolean(CONFIG_KEY_FIXTURE_LOAD, FixtureUtil.getDefaultFixtureLoad());
    }

    public String getFixtureDirName()
    {
        // temporary
        return Config.getInstance().getString(CONFIG_KEY_FIXTURE_DIR, FixtureUtil.getDefaultFixtureDir());
    }

    
    // TBD
    private List<FixtureFile> buildFixtureFileList()
    {
        List<FixtureFile> list = new ArrayList<FixtureFile>();

        String fixtureDirName = getFixtureDirName();
        File fixtureDir = new File(fixtureDirName);
        if(!fixtureDir.exists() || !fixtureDir.isDirectory()) {
            // error. what to do????
            if(log.isLoggable(Level.WARNING)) log.warning("Fixture file directory does not exist. fixtureDirName = " + fixtureDirName);
        } else {
            File[] files = fixtureDir.listFiles();
            for(File f : files) {
                if(f.isFile()) {
                    String filePath = f.getPath();  // ???
                    FixtureFile ff = new FixtureFile(filePath);
                    list.add(ff);
                } else {
                    // This should not happen. Ignore...                    
                }
            }
        }
        
        return list;
    }
    
    public int processFixtureFiles()
    {
        List<FixtureFile> list = buildFixtureFileList();
        if(log.isLoggable(Level.FINE)) {
            for(FixtureFile f : list) {
                log.fine("FixtureFile f = " + f);
            }
        }
        
        int count = 0;
        for(FixtureFile f : list) {  // list cannot be null.
            // [1] Read the file
            // [2] Convert the file content to objects
            // [3] Then, save it. (We use update/overwrite rather than create to ensure "idempotency".)
            String filePath = f.getFilePath();
            String objectType = f.getObjectType();
            String mediaType = f.getMediaType();
            
            // JSON only, for now....
            if(! FixtureUtil.MEDIA_TYPE_JSON.equals(mediaType)) {
                if(log.isLoggable(Level.WARNING)) log.warning("Currently supports only Json fixture files. Skipping filePath = " + filePath);
                continue;
            }

            BufferedReader reader = null;
            File inputFile = new File(filePath);
            if(inputFile.canRead()) {
                try {
                    reader = new BufferedReader(new FileReader(inputFile));
                    StringBuilder sb = new StringBuilder(0x1000);
                    final char[] buf = new char[0x1000];
                    int read = -1;
                    do {
                        read = reader.read(buf, 0, buf.length);
                        if (read>0) {
                            sb.append(buf, 0, read);
                        }
                    } while (read>=0);

                    if(dummyFalse1) {
                    } else if("ApiConsumer".equals(objectType)) {
                        ApiConsumerStub stub = ApiConsumerStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ApiConsumerBean bean = ApiConsumerResourceImpl.convertApiConsumerStubToBean(stub);
                        boolean suc = ServiceManager.getApiConsumerService().updateApiConsumer(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ApiConsumerList".equals(objectType)) {
                        ApiConsumerListStub listStub = ApiConsumerListStub.fromJsonString(sb.toString());
                        List<ApiConsumerBean> beanList = ApiConsumerResourceImpl.convertApiConsumerListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ApiConsumerBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getApiConsumerService().updateApiConsumer(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("User".equals(objectType)) {
                        UserStub stub = UserStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserBean bean = UserResourceImpl.convertUserStubToBean(stub);
                        boolean suc = ServiceManager.getUserService().updateUser(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserList".equals(objectType)) {
                        UserListStub listStub = UserListStub.fromJsonString(sb.toString());
                        List<UserBean> beanList = UserResourceImpl.convertUserListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserService().updateUser(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserPassword".equals(objectType)) {
                        UserPasswordStub stub = UserPasswordStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserPasswordBean bean = UserPasswordResourceImpl.convertUserPasswordStubToBean(stub);
                        boolean suc = ServiceManager.getUserPasswordService().updateUserPassword(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserPasswordList".equals(objectType)) {
                        UserPasswordListStub listStub = UserPasswordListStub.fromJsonString(sb.toString());
                        List<UserPasswordBean> beanList = UserPasswordResourceImpl.convertUserPasswordListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserPasswordBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserPasswordService().updateUserPassword(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ExternalUserAuth".equals(objectType)) {
                        ExternalUserAuthStub stub = ExternalUserAuthStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ExternalUserAuthBean bean = ExternalUserAuthResourceImpl.convertExternalUserAuthStubToBean(stub);
                        boolean suc = ServiceManager.getExternalUserAuthService().updateExternalUserAuth(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ExternalUserAuthList".equals(objectType)) {
                        ExternalUserAuthListStub listStub = ExternalUserAuthListStub.fromJsonString(sb.toString());
                        List<ExternalUserAuthBean> beanList = ExternalUserAuthResourceImpl.convertExternalUserAuthListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ExternalUserAuthBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getExternalUserAuthService().updateExternalUserAuth(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserAuthState".equals(objectType)) {
                        UserAuthStateStub stub = UserAuthStateStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserAuthStateBean bean = UserAuthStateResourceImpl.convertUserAuthStateStubToBean(stub);
                        boolean suc = ServiceManager.getUserAuthStateService().updateUserAuthState(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserAuthStateList".equals(objectType)) {
                        UserAuthStateListStub listStub = UserAuthStateListStub.fromJsonString(sb.toString());
                        List<UserAuthStateBean> beanList = UserAuthStateResourceImpl.convertUserAuthStateListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserAuthStateBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserAuthStateService().updateUserAuthState(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("DataService".equals(objectType)) {
                        DataServiceStub stub = DataServiceStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        DataServiceBean bean = DataServiceResourceImpl.convertDataServiceStubToBean(stub);
                        boolean suc = ServiceManager.getDataServiceService().updateDataService(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("DataServiceList".equals(objectType)) {
                        DataServiceListStub listStub = DataServiceListStub.fromJsonString(sb.toString());
                        List<DataServiceBean> beanList = DataServiceResourceImpl.convertDataServiceListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(DataServiceBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getDataServiceService().updateDataService(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceEndpoint".equals(objectType)) {
                        ServiceEndpointStub stub = ServiceEndpointStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ServiceEndpointBean bean = ServiceEndpointResourceImpl.convertServiceEndpointStubToBean(stub);
                        boolean suc = ServiceManager.getServiceEndpointService().updateServiceEndpoint(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceEndpointList".equals(objectType)) {
                        ServiceEndpointListStub listStub = ServiceEndpointListStub.fromJsonString(sb.toString());
                        List<ServiceEndpointBean> beanList = ServiceEndpointResourceImpl.convertServiceEndpointListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ServiceEndpointBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getServiceEndpointService().updateServiceEndpoint(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("QuerySession".equals(objectType)) {
                        QuerySessionStub stub = QuerySessionStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        QuerySessionBean bean = QuerySessionResourceImpl.convertQuerySessionStubToBean(stub);
                        boolean suc = ServiceManager.getQuerySessionService().updateQuerySession(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("QuerySessionList".equals(objectType)) {
                        QuerySessionListStub listStub = QuerySessionListStub.fromJsonString(sb.toString());
                        List<QuerySessionBean> beanList = QuerySessionResourceImpl.convertQuerySessionListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(QuerySessionBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getQuerySessionService().updateQuerySession(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("QueryRecord".equals(objectType)) {
                        QueryRecordStub stub = QueryRecordStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        QueryRecordBean bean = QueryRecordResourceImpl.convertQueryRecordStubToBean(stub);
                        boolean suc = ServiceManager.getQueryRecordService().updateQueryRecord(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("QueryRecordList".equals(objectType)) {
                        QueryRecordListStub listStub = QueryRecordListStub.fromJsonString(sb.toString());
                        List<QueryRecordBean> beanList = QueryRecordResourceImpl.convertQueryRecordListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(QueryRecordBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getQueryRecordService().updateQueryRecord(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("DummyEntity".equals(objectType)) {
                        DummyEntityStub stub = DummyEntityStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        DummyEntityBean bean = DummyEntityResourceImpl.convertDummyEntityStubToBean(stub);
                        boolean suc = ServiceManager.getDummyEntityService().updateDummyEntity(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("DummyEntityList".equals(objectType)) {
                        DummyEntityListStub listStub = DummyEntityListStub.fromJsonString(sb.toString());
                        List<DummyEntityBean> beanList = DummyEntityResourceImpl.convertDummyEntityListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(DummyEntityBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getDummyEntityService().updateDummyEntity(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceInfo".equals(objectType)) {
                        ServiceInfoStub stub = ServiceInfoStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ServiceInfoBean bean = ServiceInfoResourceImpl.convertServiceInfoStubToBean(stub);
                        boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceInfoList".equals(objectType)) {
                        ServiceInfoListStub listStub = ServiceInfoListStub.fromJsonString(sb.toString());
                        List<ServiceInfoBean> beanList = ServiceInfoResourceImpl.convertServiceInfoListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ServiceInfoBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FiveTen".equals(objectType)) {
                        FiveTenStub stub = FiveTenStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        FiveTenBean bean = FiveTenResourceImpl.convertFiveTenStubToBean(stub);
                        boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FiveTenList".equals(objectType)) {
                        FiveTenListStub listStub = FiveTenListStub.fromJsonString(sb.toString());
                        List<FiveTenBean> beanList = FiveTenResourceImpl.convertFiveTenListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(FiveTenBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else {
                        // This cannot happen
                    }
                } catch (FileNotFoundException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (IOException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (BaseException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process a fixture file: filePath = " + filePath, e);
                } catch (Exception e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected error while processing a fixture file: filePath = " + filePath, e);
                } finally {
                    if(reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Error while closing the fixture file: filePath = " + filePath, e);
                        }
                    }
                }                
            } else {
                if(log.isLoggable(Level.WARNING)) log.warning("Skipping a fixture file because it is not readable: filePath = " + filePath);
            }
        }
                
        return count;
    }
    
}
