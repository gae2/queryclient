package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.ApiConsumerDAO;
import com.queryclient.ws.data.ApiConsumerDataObject;


// MockApiConsumerDAO is a decorator.
// It can be used as a base class to mock ApiConsumerDAO objects.
public abstract class MockApiConsumerDAO implements ApiConsumerDAO
{
    private static final Logger log = Logger.getLogger(MockApiConsumerDAO.class.getName()); 

    // MockApiConsumerDAO uses the decorator design pattern.
    private ApiConsumerDAO decoratedDAO;

    public MockApiConsumerDAO(ApiConsumerDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected ApiConsumerDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(ApiConsumerDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public ApiConsumerDataObject getApiConsumer(String guid) throws BaseException
    {
        return decoratedDAO.getApiConsumer(guid);
	}

    @Override
    public List<ApiConsumerDataObject> getApiConsumers(List<String> guids) throws BaseException
    {
        return decoratedDAO.getApiConsumers(guids);
    }

    @Override
    public List<ApiConsumerDataObject> getAllApiConsumers() throws BaseException
	{
	    return getAllApiConsumers(null, null, null);
    }


    @Override
    public List<ApiConsumerDataObject> getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllApiConsumers(ordering, offset, count, null);
    }

    @Override
    public List<ApiConsumerDataObject> getAllApiConsumers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllApiConsumers(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllApiConsumerKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllApiConsumerKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<ApiConsumerDataObject> findApiConsumers(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findApiConsumers(filter, ordering, params, values, null, null);
    }

    @Override
	public List<ApiConsumerDataObject> findApiConsumers(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findApiConsumers(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<ApiConsumerDataObject> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<ApiConsumerDataObject> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createApiConsumer(ApiConsumerDataObject apiConsumer) throws BaseException
    {
        return decoratedDAO.createApiConsumer( apiConsumer);
    }

    @Override
	public Boolean updateApiConsumer(ApiConsumerDataObject apiConsumer) throws BaseException
	{
        return decoratedDAO.updateApiConsumer(apiConsumer);
	}
	
    @Override
    public Boolean deleteApiConsumer(ApiConsumerDataObject apiConsumer) throws BaseException
    {
        return decoratedDAO.deleteApiConsumer(apiConsumer);
    }

    @Override
    public Boolean deleteApiConsumer(String guid) throws BaseException
    {
        return decoratedDAO.deleteApiConsumer(guid);
	}

    @Override
    public Long deleteApiConsumers(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteApiConsumers(filter, params, values);
    }

}
