package com.queryclient.ws.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;


// Utility functions for combining/decomposing into json string segments.
public final class JsonUtil
{
    private static final Logger log = Logger.getLogger(JsonUtil.class.getName());
    private static ObjectMapper jsonObjectMapper = null;

    private JsonUtil() {}

    public static ObjectMapper getJsonObjectMapper()
    {
        if(jsonObjectMapper == null) {
            jsonObjectMapper = new ObjectMapper();
        }
        return jsonObjectMapper;
    }

    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> objJsonString
    public static Map<String, String> parseJsonObjectString(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, String> jsonObjectMap = new HashMap<String, String>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            ObjectMapper om = new ObjectMapper();  // ????
            factory.setCodec(om);
            JsonParser parser = factory.createJsonParser(jsonStr);

            JsonNode topNode = parser.readValueAsTree();
            Iterator<String> fieldNames = topNode.getFieldNames();
            
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                String value = topNode.get(name).asText();
                // Note: value might be a sensitive data which should not be logged....
                // if(log.isLoggable(Level.FINE)) log.fine("jsonObjectMap: name = " + name + "; value" + value);
                if(log.isLoggable(Level.FINE)) log.fine("jsonObjectMap: name = " + name);
                jsonObjectMap.put(name, value);
            }
        
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    public static String generateJsonObjectString(Map<String, String> jsonObjectMap)
    {
        StringBuilder sb = new StringBuilder();

        for(String key : jsonObjectMap.keySet()) {
            String json = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(json == null) {
                sb.append("null");  // ????
            } else {
                sb.append(json);
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectString(): jsonStr = " + jsonStr);

        return jsonStr;
    }
    
    
}
