package com.queryclient.ws;



public interface ReferrerInfoStruct 
{
    String  getReferer();
    String  getUserAgent();
    String  getLanguage();
    String  getHostname();
    String  getIpAddress();
    String  getNote();
    boolean isEmpty();
}
