package com.queryclient.ws.exception;

import com.queryclient.ws.BaseException;


public class FoundElsewhereException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public FoundElsewhereException() 
    {
        super();
    }
    public FoundElsewhereException(String message) 
    {
        super(message);
    }
   public FoundElsewhereException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public FoundElsewhereException(Throwable cause) 
    {
        super(cause);
    }

}
