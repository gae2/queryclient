package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ExternalServiceApiKeyStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "externalServiceApiKeyStructs")
@XmlType(propOrder = {"externalServiceApiKeyStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalServiceApiKeyStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExternalServiceApiKeyStructListStub.class.getName());

    private List<ExternalServiceApiKeyStructStub> externalServiceApiKeyStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public ExternalServiceApiKeyStructListStub()
    {
        this(new ArrayList<ExternalServiceApiKeyStructStub>());
    }
    public ExternalServiceApiKeyStructListStub(List<ExternalServiceApiKeyStructStub> externalServiceApiKeyStructs)
    {
        this(externalServiceApiKeyStructs, null);
    }
    public ExternalServiceApiKeyStructListStub(List<ExternalServiceApiKeyStructStub> externalServiceApiKeyStructs, String forwardCursor)
    {
        this.externalServiceApiKeyStructs = externalServiceApiKeyStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(externalServiceApiKeyStructs == null) {
            return true;
        } else {
            return externalServiceApiKeyStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(externalServiceApiKeyStructs == null) {
            return 0;
        } else {
            return externalServiceApiKeyStructs.size();
        }
    }


    @XmlElement(name = "externalServiceApiKeyStruct")
    public List<ExternalServiceApiKeyStructStub> getExternalServiceApiKeyStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ExternalServiceApiKeyStructStub> getList()
    {
        return externalServiceApiKeyStructs;
    }
    public void setList(List<ExternalServiceApiKeyStructStub> externalServiceApiKeyStructs)
    {
        this.externalServiceApiKeyStructs = externalServiceApiKeyStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<ExternalServiceApiKeyStructStub> it = this.externalServiceApiKeyStructs.iterator();
        while(it.hasNext()) {
            ExternalServiceApiKeyStructStub externalServiceApiKeyStruct = it.next();
            sb.append(externalServiceApiKeyStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ExternalServiceApiKeyStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ExternalServiceApiKeyStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ExternalServiceApiKeyStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ExternalServiceApiKeyStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ExternalServiceApiKeyStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static ExternalServiceApiKeyStructListStub fromJsonString(String jsonStr)
    {
        try {
            ExternalServiceApiKeyStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ExternalServiceApiKeyStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalServiceApiKeyStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalServiceApiKeyStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalServiceApiKeyStructListStub object.", e);
        }
        
        return null;
    }

}
