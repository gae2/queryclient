package com.queryclient.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.bean.ConsumerKeySecretPairBean;
import com.queryclient.ws.bean.ServiceEndpointBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.ConsumerKeySecretPairDataObject;
import com.queryclient.ws.data.ServiceEndpointDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.ServiceEndpointService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ServiceEndpointServiceImpl implements ServiceEndpointService
{
    private static final Logger log = Logger.getLogger(ServiceEndpointServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ServiceEndpoint related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ServiceEndpoint getServiceEndpoint(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ServiceEndpointDataObject dataObj = getDAOFactory().getServiceEndpointDAO().getServiceEndpoint(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ServiceEndpointDataObject for guid = " + guid);
            return null;  // ????
        }
        ServiceEndpointBean bean = new ServiceEndpointBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getServiceEndpoint(String guid, String field) throws BaseException
    {
        ServiceEndpointDataObject dataObj = getDAOFactory().getServiceEndpointDAO().getServiceEndpoint(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ServiceEndpointDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("dataService")) {
            return dataObj.getDataService();
        } else if(field.equals("serviceName")) {
            return dataObj.getServiceName();
        } else if(field.equals("serviceUrl")) {
            return dataObj.getServiceUrl();
        } else if(field.equals("authRequired")) {
            return dataObj.isAuthRequired();
        } else if(field.equals("authCredential")) {
            return dataObj.getAuthCredential();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ServiceEndpoint> list = new ArrayList<ServiceEndpoint>();
        List<ServiceEndpointDataObject> dataObjs = getDAOFactory().getServiceEndpointDAO().getServiceEndpoints(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceEndpointDataObject list.");
        } else {
            Iterator<ServiceEndpointDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ServiceEndpointDataObject dataObj = (ServiceEndpointDataObject) it.next();
                list.add(new ServiceEndpointBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException
    {
        return getAllServiceEndpoints(null, null, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpoints(ordering, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllServiceEndpoints(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ServiceEndpoint> list = new ArrayList<ServiceEndpoint>();
        List<ServiceEndpointDataObject> dataObjs = getDAOFactory().getServiceEndpointDAO().getAllServiceEndpoints(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceEndpointDataObject list.");
        } else {
            Iterator<ServiceEndpointDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ServiceEndpointDataObject dataObj = (ServiceEndpointDataObject) it.next();
                list.add(new ServiceEndpointBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllServiceEndpointKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getServiceEndpointDAO().getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceEndpoint key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ServiceEndpointServiceImpl.findServiceEndpoints(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ServiceEndpoint> list = new ArrayList<ServiceEndpoint>();
        List<ServiceEndpointDataObject> dataObjs = getDAOFactory().getServiceEndpointDAO().findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find serviceEndpoints for the given criterion.");
        } else {
            Iterator<ServiceEndpointDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ServiceEndpointDataObject dataObj = (ServiceEndpointDataObject) it.next();
                list.add(new ServiceEndpointBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ServiceEndpointServiceImpl.findServiceEndpointKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getServiceEndpointDAO().findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ServiceEndpoint keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ServiceEndpointServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getServiceEndpointDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createServiceEndpoint(String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        ConsumerKeySecretPairDataObject authCredentialDobj = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialDobj = ((ConsumerKeySecretPairBean) authCredential).toDataObject();
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialDobj = new ConsumerKeySecretPairDataObject(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialDobj = null;   // ????
        }
        
        ServiceEndpointDataObject dataObj = new ServiceEndpointDataObject(null, user, dataService, serviceName, serviceUrl, authRequired, authCredentialDobj, status);
        return createServiceEndpoint(dataObj);
    }

    @Override
    public String createServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceEndpoint cannot be null.....
        if(serviceEndpoint == null) {
            log.log(Level.INFO, "Param serviceEndpoint is null!");
            throw new BadRequestException("Param serviceEndpoint object is null!");
        }
        ServiceEndpointDataObject dataObj = null;
        if(serviceEndpoint instanceof ServiceEndpointDataObject) {
            dataObj = (ServiceEndpointDataObject) serviceEndpoint;
        } else if(serviceEndpoint instanceof ServiceEndpointBean) {
            dataObj = ((ServiceEndpointBean) serviceEndpoint).toDataObject();
        } else {  // if(serviceEndpoint instanceof ServiceEndpoint)
            //dataObj = new ServiceEndpointDataObject(null, serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), (ConsumerKeySecretPairDataObject) serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ServiceEndpointDataObject(serviceEndpoint.getGuid(), serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), (ConsumerKeySecretPairDataObject) serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
        }
        String guid = getDAOFactory().getServiceEndpointDAO().createServiceEndpoint(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        ConsumerKeySecretPairDataObject authCredentialDobj = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialDobj = ((ConsumerKeySecretPairBean) authCredential).toDataObject();            
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialDobj = new ConsumerKeySecretPairDataObject(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ServiceEndpointDataObject dataObj = new ServiceEndpointDataObject(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredentialDobj, status);
        return updateServiceEndpoint(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceEndpoint cannot be null.....
        if(serviceEndpoint == null || serviceEndpoint.getGuid() == null) {
            log.log(Level.INFO, "Param serviceEndpoint or its guid is null!");
            throw new BadRequestException("Param serviceEndpoint object or its guid is null!");
        }
        ServiceEndpointDataObject dataObj = null;
        if(serviceEndpoint instanceof ServiceEndpointDataObject) {
            dataObj = (ServiceEndpointDataObject) serviceEndpoint;
        } else if(serviceEndpoint instanceof ServiceEndpointBean) {
            dataObj = ((ServiceEndpointBean) serviceEndpoint).toDataObject();
        } else {  // if(serviceEndpoint instanceof ServiceEndpoint)
            dataObj = new ServiceEndpointDataObject(serviceEndpoint.getGuid(), serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
        }
        Boolean suc = getDAOFactory().getServiceEndpointDAO().updateServiceEndpoint(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getServiceEndpointDAO().deleteServiceEndpoint(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceEndpoint cannot be null.....
        if(serviceEndpoint == null || serviceEndpoint.getGuid() == null) {
            log.log(Level.INFO, "Param serviceEndpoint or its guid is null!");
            throw new BadRequestException("Param serviceEndpoint object or its guid is null!");
        }
        ServiceEndpointDataObject dataObj = null;
        if(serviceEndpoint instanceof ServiceEndpointDataObject) {
            dataObj = (ServiceEndpointDataObject) serviceEndpoint;
        } else if(serviceEndpoint instanceof ServiceEndpointBean) {
            dataObj = ((ServiceEndpointBean) serviceEndpoint).toDataObject();
        } else {  // if(serviceEndpoint instanceof ServiceEndpoint)
            dataObj = new ServiceEndpointDataObject(serviceEndpoint.getGuid(), serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
        }
        Boolean suc = getDAOFactory().getServiceEndpointDAO().deleteServiceEndpoint(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getServiceEndpointDAO().deleteServiceEndpoints(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
