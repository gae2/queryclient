package com.queryclient.ws.resource.exception;

import com.queryclient.ws.exception.resource.BaseResourceException;


public class FoundElsewhereRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public FoundElsewhereRsException() 
    {
        super();
    }
    public FoundElsewhereRsException(String message) 
    {
        super(message);
    }
    public FoundElsewhereRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public FoundElsewhereRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public FoundElsewhereRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public FoundElsewhereRsException(Throwable cause) 
    {
        super(cause);
    }
    public FoundElsewhereRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
