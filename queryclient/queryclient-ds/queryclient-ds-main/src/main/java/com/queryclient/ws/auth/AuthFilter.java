package com.queryclient.ws.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class AuthFilter implements Filter
{
    private static final Logger log = Logger.getLogger(AuthFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    
    public AuthFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
        // TBD: 
        // Initialize connections to AeryID/CommonAuth servers.
        // etc.
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // TBD
        boolean accessAllowed = false;  // Use access control enum???
        String authToken = req.getParameter(AuthConstants.PARAM_AUTH_TOKEN);
        String clientKey = req.getParameter(AuthConstants.PARAM_CLIENT_KEY);
        String userKey = req.getParameter(AuthConstants.PARAM_USER_KEY);
        
        // TBD
        boolean isClientRequest = false;
        boolean isUserRequest = false;
        if(clientKey != null && clientKey.length() > 0) {  // TBD: Validate...
            isClientRequest = true;
        }
        if(userKey != null && userKey.length() > 0) {      // TBD: Validate...
            isUserRequest = true;
        }
        if(isClientRequest == false && isUserRequest == false) {
            // Error!
            // ...
        }
//        if(isClientRequest == true && isUserRequest == true) {
//            // Error! ????
//        }
        
        // TBD
        if(authToken != null) {
            // TBD:
            // Verify the token. (Need clientKey as well???)
            // Check the access control
            // TBD: Where to store the auth token? here in the local app? or, in the AeryId service???
            // Maybe, just keep it locally in the memory?
            // set the accessAllowed flag.
            
            // temporary
            if(isClientRequest == true) {
                if(AuthRegistry.getInstance().isValid(clientKey, authToken)) {
                    accessAllowed = true;
                }
            }
            // temporary
        }
        
        // TBD
        if (accessAllowed == false) {
            // TBD:
            String clientSecret = req.getParameter(AuthConstants.PARAM_CLIENT_SECRET);
            String userSecret = req.getParameter(AuthConstants.PARAM_USER_SECRET);
            
            if(isClientRequest == true && (clientSecret != null && clientSecret.length() > 0)) {
                // TBD:
                // Verify the client key/secret...
                // Then set the authToken in the response....
                // ...
                
                // set the flag, if authentication successful.
                //accessAllowed = true;
            } else if(isUserRequest == true && (userSecret != null && userSecret.length() > 0)) {
                // TBD:
                // Verify the user key/secret...
                // Then set the authToken in the response....
                // ...
                
                // set the flag, if authentication successful.
                //accessAllowed = true;
            } else {
                // TBD: Send the authentication request...
                // ...
            }
            
        }

        // TBD
        if (accessAllowed == true && authToken != null) {  // or, if it is valid ...
            // TBD:Add the (updated) authToken in the response
            // ...
            // And, "cache it"???
//            if(clientKey != null) {
//                // TBD: Update the attributes as well, ....
//                AuthRegistry.getInstance().put(clientKey, authToken);
//            }
            
            // ???
            //req.setAttribute(AuthConstants.ATTR_AUTHORIZED, authToken);
        }
        
        // TBD
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AuthFilter.doFilter(): accessAllowed = " + accessAllowed);
        if (accessAllowed == true) {
            // Continue through filter chain.
            chain.doFilter(req, res);
        } else {
            // Redirect to the error or login page. ???
            // ... ???
            config.getServletContext().getRequestDispatcher("/").forward(req, res);
            return;
        }
    }
    
}
