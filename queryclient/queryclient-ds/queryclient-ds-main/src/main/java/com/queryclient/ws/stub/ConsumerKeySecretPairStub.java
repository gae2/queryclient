package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "consumerKeySecretPair")
@XmlType(propOrder = {"consumerKey", "consumerSecret"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerKeySecretPairStub implements ConsumerKeySecretPair, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ConsumerKeySecretPairStub.class.getName());

    private String consumerKey;
    private String consumerSecret;

    public ConsumerKeySecretPairStub()
    {
        this(null);
    }
    public ConsumerKeySecretPairStub(ConsumerKeySecretPair bean)
    {
        if(bean != null) {
            this.consumerKey = bean.getConsumerKey();
            this.consumerSecret = bean.getConsumerSecret();
        }
    }


    @XmlElement
    public String getConsumerKey()
    {
        return this.consumerKey;
    }
    public void setConsumerKey(String consumerKey)
    {
        this.consumerKey = consumerKey;
    }

    @XmlElement
    public String getConsumerSecret()
    {
        return this.consumerSecret;
    }
    public void setConsumerSecret(String consumerSecret)
    {
        this.consumerSecret = consumerSecret;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getConsumerKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getConsumerSecret() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("consumerKey", this.consumerKey);
        dataMap.put("consumerSecret", this.consumerSecret);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = consumerKey == null ? 0 : consumerKey.hashCode();
        _hash = 31 * _hash + delta;
        delta = consumerSecret == null ? 0 : consumerSecret.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ConsumerKeySecretPairStub convertBeanToStub(ConsumerKeySecretPair bean)
    {
        ConsumerKeySecretPairStub stub = null;
        if(bean instanceof ConsumerKeySecretPairStub) {
            stub = (ConsumerKeySecretPairStub) bean;
        } else {
            if(bean != null) {
                stub = new ConsumerKeySecretPairStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ConsumerKeySecretPairStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of ConsumerKeySecretPairStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ConsumerKeySecretPairStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ConsumerKeySecretPairStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ConsumerKeySecretPairStub object as a string.", e);
        }
        
        return null;
    }
    public static ConsumerKeySecretPairStub fromJsonString(String jsonStr)
    {
        try {
            ConsumerKeySecretPairStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ConsumerKeySecretPairStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ConsumerKeySecretPairStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ConsumerKeySecretPairStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ConsumerKeySecretPairStub object.", e);
        }
        
        return null;
    }

}
