package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ServiceEndpointBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ServiceEndpointBasePermission.class.getName());

    public ServiceEndpointBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "ServiceEndpoint::" + action;
    }


}
