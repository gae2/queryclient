package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class ExternalUserAuthDataObject extends KeyedDataObject implements ExternalUserAuth
{
    private static final Logger log = Logger.getLogger(ExternalUserAuthDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ExternalUserAuthDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ExternalUserAuthDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String managerApp;

    @Persistent(defaultFetchGroup = "true")
    private Long appAcl;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="groupId", columns=@Column(name="gaeAppgroupId")),
        @Persistent(name="appId", columns=@Column(name="gaeAppappId")),
        @Persistent(name="appDomain", columns=@Column(name="gaeAppappDomain")),
        @Persistent(name="namespace", columns=@Column(name="gaeAppnamespace")),
        @Persistent(name="acl", columns=@Column(name="gaeAppacl")),
        @Persistent(name="note", columns=@Column(name="gaeAppnote")),
    })
    private GaeAppStructDataObject gaeApp;

    @Persistent(defaultFetchGroup = "true")
    private String ownerUser;

    @Persistent(defaultFetchGroup = "true")
    private Long userAcl;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String providerId;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="uuid", columns=@Column(name="externalUserIduuid")),
        @Persistent(name="id", columns=@Column(name="externalUserIdid")),
        @Persistent(name="name", columns=@Column(name="externalUserIdname")),
        @Persistent(name="email", columns=@Column(name="externalUserIdemail")),
        @Persistent(name="username", columns=@Column(name="externalUserIdusername")),
        @Persistent(name="openId", columns=@Column(name="externalUserIdopenId")),
        @Persistent(name="note", columns=@Column(name="externalUserIdnote")),
    })
    private ExternalUserIdStructDataObject externalUserId;

    @Persistent(defaultFetchGroup = "true")
    private String requestToken;

    @Persistent(defaultFetchGroup = "true")
    private String accessToken;

    @Persistent(defaultFetchGroup = "true")
    private String accessTokenSecret;

    @Persistent(defaultFetchGroup = "true")
    private String email;

    @Persistent(defaultFetchGroup = "true")
    private String firstName;

    @Persistent(defaultFetchGroup = "true")
    private String lastName;

    @Persistent(defaultFetchGroup = "true")
    private String fullName;

    @Persistent(defaultFetchGroup = "true")
    private String displayName;

    @Persistent(defaultFetchGroup = "true")
    private String description;

    @Persistent(defaultFetchGroup = "true")
    private String gender;

    @Persistent(defaultFetchGroup = "true")
    private String dateOfBirth;

    @Persistent(defaultFetchGroup = "true")
    private String profileImageUrl;

    @Persistent(defaultFetchGroup = "true")
    private String timeZone;

    @Persistent(defaultFetchGroup = "true")
    private String postalCode;

    @Persistent(defaultFetchGroup = "true")
    private String location;

    @Persistent(defaultFetchGroup = "true")
    private String country;

    @Persistent(defaultFetchGroup = "true")
    private String language;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Long authTime;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    public ExternalUserAuthDataObject()
    {
        this(null);
    }
    public ExternalUserAuthDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ExternalUserAuthDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, providerId, externalUserId, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime, null, null);
    }
    public ExternalUserAuthDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        if(gaeApp != null) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = null;
        }
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.providerId = providerId;
        if(externalUserId != null) {
            this.externalUserId = new ExternalUserIdStructDataObject(externalUserId.getUuid(), externalUserId.getId(), externalUserId.getName(), externalUserId.getEmail(), externalUserId.getUsername(), externalUserId.getOpenId(), externalUserId.getNote());
        } else {
            this.externalUserId = null;
        }
        this.requestToken = requestToken;
        this.accessToken = accessToken;
        this.accessTokenSecret = accessTokenSecret;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
        this.displayName = displayName;
        this.description = description;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.profileImageUrl = profileImageUrl;
        this.timeZone = timeZone;
        this.postalCode = postalCode;
        this.location = location;
        this.country = country;
        this.language = language;
        this.status = status;
        this.authTime = authTime;
        this.expirationTime = expirationTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return ExternalUserAuthDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ExternalUserAuthDataObject.composeKey(getGuid());
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStruct getGaeApp()
    {
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(gaeApp == null) {
            this.gaeApp = null;
            log.log(Level.INFO, "ExternalUserAuthDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is null.");            
        } else if(gaeApp instanceof GaeAppStructDataObject) {
            this.gaeApp = (GaeAppStructDataObject) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = new GaeAppStructDataObject();   // ????
            log.log(Level.WARNING, "ExternalUserAuthDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is of an invalid type.");
        }
    }

    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getProviderId()
    {
        return this.providerId;
    }
    public void setProviderId(String providerId)
    {
        this.providerId = providerId;
    }

    public ExternalUserIdStruct getExternalUserId()
    {
        return this.externalUserId;
    }
    public void setExternalUserId(ExternalUserIdStruct externalUserId)
    {
        if(externalUserId == null) {
            this.externalUserId = null;
            log.log(Level.INFO, "ExternalUserAuthDataObject.setExternalUserId(ExternalUserIdStruct externalUserId): Arg externalUserId is null.");            
        } else if(externalUserId instanceof ExternalUserIdStructDataObject) {
            this.externalUserId = (ExternalUserIdStructDataObject) externalUserId;
        } else if(externalUserId instanceof ExternalUserIdStruct) {
            this.externalUserId = new ExternalUserIdStructDataObject(externalUserId.getUuid(), externalUserId.getId(), externalUserId.getName(), externalUserId.getEmail(), externalUserId.getUsername(), externalUserId.getOpenId(), externalUserId.getNote());
        } else {
            this.externalUserId = new ExternalUserIdStructDataObject();   // ????
            log.log(Level.WARNING, "ExternalUserAuthDataObject.setExternalUserId(ExternalUserIdStruct externalUserId): Arg externalUserId is of an invalid type.");
        }
    }

    public String getRequestToken()
    {
        return this.requestToken;
    }
    public void setRequestToken(String requestToken)
    {
        this.requestToken = requestToken;
    }

    public String getAccessToken()
    {
        return this.accessToken;
    }
    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getAccessTokenSecret()
    {
        return this.accessTokenSecret;
    }
    public void setAccessTokenSecret(String accessTokenSecret)
    {
        this.accessTokenSecret = accessTokenSecret;
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFirstName()
    {
        return this.firstName;
    }
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return this.lastName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getFullName()
    {
        return this.fullName;
    }
    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getDisplayName()
    {
        return this.displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getGender()
    {
        return this.gender;
    }
    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public String getDateOfBirth()
    {
        return this.dateOfBirth;
    }
    public void setDateOfBirth(String dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }

    public String getProfileImageUrl()
    {
        return this.profileImageUrl;
    }
    public void setProfileImageUrl(String profileImageUrl)
    {
        this.profileImageUrl = profileImageUrl;
    }

    public String getTimeZone()
    {
        return this.timeZone;
    }
    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }

    public String getPostalCode()
    {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getCountry()
    {
        return this.country;
    }
    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getAuthTime()
    {
        return this.authTime;
    }
    public void setAuthTime(Long authTime)
    {
        this.authTime = authTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("ownerUser", this.ownerUser);
        dataMap.put("userAcl", this.userAcl);
        dataMap.put("user", this.user);
        dataMap.put("providerId", this.providerId);
        dataMap.put("externalUserId", this.externalUserId);
        dataMap.put("requestToken", this.requestToken);
        dataMap.put("accessToken", this.accessToken);
        dataMap.put("accessTokenSecret", this.accessTokenSecret);
        dataMap.put("email", this.email);
        dataMap.put("firstName", this.firstName);
        dataMap.put("lastName", this.lastName);
        dataMap.put("fullName", this.fullName);
        dataMap.put("displayName", this.displayName);
        dataMap.put("description", this.description);
        dataMap.put("gender", this.gender);
        dataMap.put("dateOfBirth", this.dateOfBirth);
        dataMap.put("profileImageUrl", this.profileImageUrl);
        dataMap.put("timeZone", this.timeZone);
        dataMap.put("postalCode", this.postalCode);
        dataMap.put("location", this.location);
        dataMap.put("country", this.country);
        dataMap.put("language", this.language);
        dataMap.put("status", this.status);
        dataMap.put("authTime", this.authTime);
        dataMap.put("expirationTime", this.expirationTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ExternalUserAuth thatObj = (ExternalUserAuth) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.managerApp == null && thatObj.getManagerApp() != null)
            || (this.managerApp != null && thatObj.getManagerApp() == null)
            || !this.managerApp.equals(thatObj.getManagerApp()) ) {
            return false;
        }
        if( (this.appAcl == null && thatObj.getAppAcl() != null)
            || (this.appAcl != null && thatObj.getAppAcl() == null)
            || !this.appAcl.equals(thatObj.getAppAcl()) ) {
            return false;
        }
        if( (this.gaeApp == null && thatObj.getGaeApp() != null)
            || (this.gaeApp != null && thatObj.getGaeApp() == null)
            || !this.gaeApp.equals(thatObj.getGaeApp()) ) {
            return false;
        }
        if( (this.ownerUser == null && thatObj.getOwnerUser() != null)
            || (this.ownerUser != null && thatObj.getOwnerUser() == null)
            || !this.ownerUser.equals(thatObj.getOwnerUser()) ) {
            return false;
        }
        if( (this.userAcl == null && thatObj.getUserAcl() != null)
            || (this.userAcl != null && thatObj.getUserAcl() == null)
            || !this.userAcl.equals(thatObj.getUserAcl()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.providerId == null && thatObj.getProviderId() != null)
            || (this.providerId != null && thatObj.getProviderId() == null)
            || !this.providerId.equals(thatObj.getProviderId()) ) {
            return false;
        }
        if( (this.externalUserId == null && thatObj.getExternalUserId() != null)
            || (this.externalUserId != null && thatObj.getExternalUserId() == null)
            || !this.externalUserId.equals(thatObj.getExternalUserId()) ) {
            return false;
        }
        if( (this.requestToken == null && thatObj.getRequestToken() != null)
            || (this.requestToken != null && thatObj.getRequestToken() == null)
            || !this.requestToken.equals(thatObj.getRequestToken()) ) {
            return false;
        }
        if( (this.accessToken == null && thatObj.getAccessToken() != null)
            || (this.accessToken != null && thatObj.getAccessToken() == null)
            || !this.accessToken.equals(thatObj.getAccessToken()) ) {
            return false;
        }
        if( (this.accessTokenSecret == null && thatObj.getAccessTokenSecret() != null)
            || (this.accessTokenSecret != null && thatObj.getAccessTokenSecret() == null)
            || !this.accessTokenSecret.equals(thatObj.getAccessTokenSecret()) ) {
            return false;
        }
        if( (this.email == null && thatObj.getEmail() != null)
            || (this.email != null && thatObj.getEmail() == null)
            || !this.email.equals(thatObj.getEmail()) ) {
            return false;
        }
        if( (this.firstName == null && thatObj.getFirstName() != null)
            || (this.firstName != null && thatObj.getFirstName() == null)
            || !this.firstName.equals(thatObj.getFirstName()) ) {
            return false;
        }
        if( (this.lastName == null && thatObj.getLastName() != null)
            || (this.lastName != null && thatObj.getLastName() == null)
            || !this.lastName.equals(thatObj.getLastName()) ) {
            return false;
        }
        if( (this.fullName == null && thatObj.getFullName() != null)
            || (this.fullName != null && thatObj.getFullName() == null)
            || !this.fullName.equals(thatObj.getFullName()) ) {
            return false;
        }
        if( (this.displayName == null && thatObj.getDisplayName() != null)
            || (this.displayName != null && thatObj.getDisplayName() == null)
            || !this.displayName.equals(thatObj.getDisplayName()) ) {
            return false;
        }
        if( (this.description == null && thatObj.getDescription() != null)
            || (this.description != null && thatObj.getDescription() == null)
            || !this.description.equals(thatObj.getDescription()) ) {
            return false;
        }
        if( (this.gender == null && thatObj.getGender() != null)
            || (this.gender != null && thatObj.getGender() == null)
            || !this.gender.equals(thatObj.getGender()) ) {
            return false;
        }
        if( (this.dateOfBirth == null && thatObj.getDateOfBirth() != null)
            || (this.dateOfBirth != null && thatObj.getDateOfBirth() == null)
            || !this.dateOfBirth.equals(thatObj.getDateOfBirth()) ) {
            return false;
        }
        if( (this.profileImageUrl == null && thatObj.getProfileImageUrl() != null)
            || (this.profileImageUrl != null && thatObj.getProfileImageUrl() == null)
            || !this.profileImageUrl.equals(thatObj.getProfileImageUrl()) ) {
            return false;
        }
        if( (this.timeZone == null && thatObj.getTimeZone() != null)
            || (this.timeZone != null && thatObj.getTimeZone() == null)
            || !this.timeZone.equals(thatObj.getTimeZone()) ) {
            return false;
        }
        if( (this.postalCode == null && thatObj.getPostalCode() != null)
            || (this.postalCode != null && thatObj.getPostalCode() == null)
            || !this.postalCode.equals(thatObj.getPostalCode()) ) {
            return false;
        }
        if( (this.location == null && thatObj.getLocation() != null)
            || (this.location != null && thatObj.getLocation() == null)
            || !this.location.equals(thatObj.getLocation()) ) {
            return false;
        }
        if( (this.country == null && thatObj.getCountry() != null)
            || (this.country != null && thatObj.getCountry() == null)
            || !this.country.equals(thatObj.getCountry()) ) {
            return false;
        }
        if( (this.language == null && thatObj.getLanguage() != null)
            || (this.language != null && thatObj.getLanguage() == null)
            || !this.language.equals(thatObj.getLanguage()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.authTime == null && thatObj.getAuthTime() != null)
            || (this.authTime != null && thatObj.getAuthTime() == null)
            || !this.authTime.equals(thatObj.getAuthTime()) ) {
            return false;
        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = ownerUser == null ? 0 : ownerUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAcl == null ? 0 : userAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = providerId == null ? 0 : providerId.hashCode();
        _hash = 31 * _hash + delta;
        delta = externalUserId == null ? 0 : externalUserId.hashCode();
        _hash = 31 * _hash + delta;
        delta = requestToken == null ? 0 : requestToken.hashCode();
        _hash = 31 * _hash + delta;
        delta = accessToken == null ? 0 : accessToken.hashCode();
        _hash = 31 * _hash + delta;
        delta = accessTokenSecret == null ? 0 : accessTokenSecret.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = firstName == null ? 0 : firstName.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastName == null ? 0 : lastName.hashCode();
        _hash = 31 * _hash + delta;
        delta = fullName == null ? 0 : fullName.hashCode();
        _hash = 31 * _hash + delta;
        delta = displayName == null ? 0 : displayName.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = gender == null ? 0 : gender.hashCode();
        _hash = 31 * _hash + delta;
        delta = dateOfBirth == null ? 0 : dateOfBirth.hashCode();
        _hash = 31 * _hash + delta;
        delta = profileImageUrl == null ? 0 : profileImageUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = timeZone == null ? 0 : timeZone.hashCode();
        _hash = 31 * _hash + delta;
        delta = postalCode == null ? 0 : postalCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = location == null ? 0 : location.hashCode();
        _hash = 31 * _hash + delta;
        delta = country == null ? 0 : country.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = authTime == null ? 0 : authTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
