package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.PagerStateStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "pagerStateStructs")
@XmlType(propOrder = {"pagerStateStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PagerStateStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PagerStateStructListStub.class.getName());

    private List<PagerStateStructStub> pagerStateStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public PagerStateStructListStub()
    {
        this(new ArrayList<PagerStateStructStub>());
    }
    public PagerStateStructListStub(List<PagerStateStructStub> pagerStateStructs)
    {
        this(pagerStateStructs, null);
    }
    public PagerStateStructListStub(List<PagerStateStructStub> pagerStateStructs, String forwardCursor)
    {
        this.pagerStateStructs = pagerStateStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(pagerStateStructs == null) {
            return true;
        } else {
            return pagerStateStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(pagerStateStructs == null) {
            return 0;
        } else {
            return pagerStateStructs.size();
        }
    }


    @XmlElement(name = "pagerStateStruct")
    public List<PagerStateStructStub> getPagerStateStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<PagerStateStructStub> getList()
    {
        return pagerStateStructs;
    }
    public void setList(List<PagerStateStructStub> pagerStateStructs)
    {
        this.pagerStateStructs = pagerStateStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<PagerStateStructStub> it = this.pagerStateStructs.iterator();
        while(it.hasNext()) {
            PagerStateStructStub pagerStateStruct = it.next();
            sb.append(pagerStateStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static PagerStateStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of PagerStateStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write PagerStateStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write PagerStateStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write PagerStateStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static PagerStateStructListStub fromJsonString(String jsonStr)
    {
        try {
            PagerStateStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, PagerStateStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into PagerStateStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into PagerStateStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into PagerStateStructListStub object.", e);
        }
        
        return null;
    }

}
