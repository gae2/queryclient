package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.DataService;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "dataServices")
@XmlType(propOrder = {"dataService", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataServiceListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DataServiceListStub.class.getName());

    private List<DataServiceStub> dataServices = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public DataServiceListStub()
    {
        this(new ArrayList<DataServiceStub>());
    }
    public DataServiceListStub(List<DataServiceStub> dataServices)
    {
        this(dataServices, null);
    }
    public DataServiceListStub(List<DataServiceStub> dataServices, String forwardCursor)
    {
        this.dataServices = dataServices;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(dataServices == null) {
            return true;
        } else {
            return dataServices.isEmpty();
        }
    }
    public int getSize()
    {
        if(dataServices == null) {
            return 0;
        } else {
            return dataServices.size();
        }
    }


    @XmlElement(name = "dataService")
    public List<DataServiceStub> getDataService()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<DataServiceStub> getList()
    {
        return dataServices;
    }
    public void setList(List<DataServiceStub> dataServices)
    {
        this.dataServices = dataServices;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<DataServiceStub> it = this.dataServices.iterator();
        while(it.hasNext()) {
            DataServiceStub dataService = it.next();
            sb.append(dataService.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static DataServiceListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of DataServiceListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write DataServiceListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write DataServiceListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write DataServiceListStub object as a string.", e);
        }
        
        return null;
    }
    public static DataServiceListStub fromJsonString(String jsonStr)
    {
        try {
            DataServiceListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, DataServiceListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into DataServiceListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into DataServiceListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into DataServiceListStub object.", e);
        }
        
        return null;
    }

}
