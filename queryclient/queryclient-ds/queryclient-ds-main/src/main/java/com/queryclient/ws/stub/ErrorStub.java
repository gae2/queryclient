package com.queryclient.ws.stub;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.queryclient.ws.Error;
import com.queryclient.ws.exception.resource.BaseResourceException;

@XmlRootElement(name = "error")
@XmlType(propOrder = {"message", "resource", "cause"})
public class ErrorStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ErrorStub.class.getName()); 

	//private String code;
	private String message = null;
	//private String detail; 
	private String resource = null;
    private String cause = null;
	//private Date time;
    
    public ErrorStub() 
    {
    }
    public ErrorStub(String message) 
    {
        this(message, null);
    }
    public ErrorStub(String message, String resource) 
    {
        this(message, resource, null);
    }
    public ErrorStub(String message, String resource, String cause)
    {
        this.message = message;
        this.resource = resource;
        this.cause = cause;
    }
    public ErrorStub(Error bean) 
    {
        if(bean != null) {
            this.message = bean.getMessage();
            this.resource = bean.getResource();
            this.cause = bean.getCause();
        }
    }
    public ErrorStub(BaseResourceException ex) 
    {
        this.message = ex.getMessage();
        this.resource = ex.getResource();
        Throwable t = ex.getCause();
        if(t != null) {
            this.cause = t.getMessage();
        }
    }

	
    @XmlElement(name = "message")
    public String getMessage()
    {
        return this.message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }

    @XmlElement(name = "resource")
    public String getResource()
    {
        return this.resource;
    }
    public void setResource(String resource)
    {
        this.resource = resource;
    }

    @XmlElement(name = "cause")
    public String getCause()
    {
        return this.cause;
    }
    public void setCause(String cause)
    {
        this.cause = cause;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("message = ");
        sb.append(this.message);
        sb.append(";");
        sb.append("resource = ");
        sb.append(this.resource);
        sb.append(";");
        sb.append("cause = ");
        sb.append(this.cause);
        return sb.toString();
    }

    // temporary
    public static ErrorStub convertBeanToStub(Error bean)
    {
        ErrorStub stub;
        if(bean instanceof ErrorStub) {
            // Just a precaution. This shouldn't really happen.
            stub = (ErrorStub) bean;
        } else {
            stub = new ErrorStub(bean);
        }        
        return stub;
    }

}
