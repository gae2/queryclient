package com.queryclient.ws;



public interface ExternalUserIdStruct 
{
    String  getUuid();
    String  getId();
    String  getName();
    String  getEmail();
    String  getUsername();
    String  getOpenId();
    String  getNote();
    boolean isEmpty();
}
