package com.queryclient.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.bean.GaeAppStructBean;
import com.queryclient.ws.bean.UserPasswordBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.GaeAppStructDataObject;
import com.queryclient.ws.data.UserPasswordDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.UserPasswordService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserPasswordServiceImpl implements UserPasswordService
{
    private static final Logger log = Logger.getLogger(UserPasswordServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UserPassword related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserPassword getUserPassword(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserPasswordDataObject dataObj = getDAOFactory().getUserPasswordDAO().getUserPassword(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserPasswordDataObject for guid = " + guid);
            return null;  // ????
        }
        UserPasswordBean bean = new UserPasswordBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserPassword(String guid, String field) throws BaseException
    {
        UserPasswordDataObject dataObj = getDAOFactory().getUserPasswordDAO().getUserPassword(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserPasswordDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return dataObj.getManagerApp();
        } else if(field.equals("appAcl")) {
            return dataObj.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return dataObj.getGaeApp();
        } else if(field.equals("ownerUser")) {
            return dataObj.getOwnerUser();
        } else if(field.equals("userAcl")) {
            return dataObj.getUserAcl();
        } else if(field.equals("admin")) {
            return dataObj.getAdmin();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("username")) {
            return dataObj.getUsername();
        } else if(field.equals("email")) {
            return dataObj.getEmail();
        } else if(field.equals("openId")) {
            return dataObj.getOpenId();
        } else if(field.equals("plainPassword")) {
            return dataObj.getPlainPassword();
        } else if(field.equals("hashedPassword")) {
            return dataObj.getHashedPassword();
        } else if(field.equals("salt")) {
            return dataObj.getSalt();
        } else if(field.equals("hashMethod")) {
            return dataObj.getHashMethod();
        } else if(field.equals("resetRequired")) {
            return dataObj.isResetRequired();
        } else if(field.equals("challengeQuestion")) {
            return dataObj.getChallengeQuestion();
        } else if(field.equals("challengeAnswer")) {
            return dataObj.getChallengeAnswer();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("lastResetTime")) {
            return dataObj.getLastResetTime();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserPassword> getUserPasswords(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserPassword> list = new ArrayList<UserPassword>();
        List<UserPasswordDataObject> dataObjs = getDAOFactory().getUserPasswordDAO().getUserPasswords(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserPasswordDataObject list.");
        } else {
            Iterator<UserPasswordDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserPasswordDataObject dataObj = (UserPasswordDataObject) it.next();
                list.add(new UserPasswordBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<UserPassword> getAllUserPasswords() throws BaseException
    {
        return getAllUserPasswords(null, null, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswords(ordering, offset, count, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserPasswords(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<UserPassword> list = new ArrayList<UserPassword>();
        List<UserPasswordDataObject> dataObjs = getDAOFactory().getUserPasswordDAO().getAllUserPasswords(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserPasswordDataObject list.");
        } else {
            Iterator<UserPasswordDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserPasswordDataObject dataObj = (UserPasswordDataObject) it.next();
                list.add(new UserPasswordBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllUserPasswordKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getUserPasswordDAO().getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserPassword key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserPasswordServiceImpl.findUserPasswords(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<UserPassword> list = new ArrayList<UserPassword>();
        List<UserPasswordDataObject> dataObjs = getDAOFactory().getUserPasswordDAO().findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find userPasswords for the given criterion.");
        } else {
            Iterator<UserPasswordDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserPasswordDataObject dataObj = (UserPasswordDataObject) it.next();
                list.add(new UserPasswordBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserPasswordServiceImpl.findUserPasswordKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getUserPasswordDAO().findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserPassword keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserPasswordServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserPasswordDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUserPassword(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        
        UserPasswordDataObject dataObj = new UserPasswordDataObject(null, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
        return createUserPassword(dataObj);
    }

    @Override
    public String createUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");

        // Param userPassword cannot be null.....
        if(userPassword == null) {
            log.log(Level.INFO, "Param userPassword is null!");
            throw new BadRequestException("Param userPassword object is null!");
        }
        UserPasswordDataObject dataObj = null;
        if(userPassword instanceof UserPasswordDataObject) {
            dataObj = (UserPasswordDataObject) userPassword;
        } else if(userPassword instanceof UserPasswordBean) {
            dataObj = ((UserPasswordBean) userPassword).toDataObject();
        } else {  // if(userPassword instanceof UserPassword)
            //dataObj = new UserPasswordDataObject(null, userPassword.getManagerApp(), userPassword.getAppAcl(), (GaeAppStructDataObject) userPassword.getGaeApp(), userPassword.getOwnerUser(), userPassword.getUserAcl(), userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), userPassword.getPlainPassword(), userPassword.getHashedPassword(), userPassword.getSalt(), userPassword.getHashMethod(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserPasswordDataObject(userPassword.getGuid(), userPassword.getManagerApp(), userPassword.getAppAcl(), (GaeAppStructDataObject) userPassword.getGaeApp(), userPassword.getOwnerUser(), userPassword.getUserAcl(), userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), userPassword.getPlainPassword(), userPassword.getHashedPassword(), userPassword.getSalt(), userPassword.getHashMethod(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
        }
        String guid = getDAOFactory().getUserPasswordDAO().createUserPassword(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUserPassword(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserPasswordDataObject dataObj = new UserPasswordDataObject(guid, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
        return updateUserPassword(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");

        // Param userPassword cannot be null.....
        if(userPassword == null || userPassword.getGuid() == null) {
            log.log(Level.INFO, "Param userPassword or its guid is null!");
            throw new BadRequestException("Param userPassword object or its guid is null!");
        }
        UserPasswordDataObject dataObj = null;
        if(userPassword instanceof UserPasswordDataObject) {
            dataObj = (UserPasswordDataObject) userPassword;
        } else if(userPassword instanceof UserPasswordBean) {
            dataObj = ((UserPasswordBean) userPassword).toDataObject();
        } else {  // if(userPassword instanceof UserPassword)
            dataObj = new UserPasswordDataObject(userPassword.getGuid(), userPassword.getManagerApp(), userPassword.getAppAcl(), userPassword.getGaeApp(), userPassword.getOwnerUser(), userPassword.getUserAcl(), userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), userPassword.getPlainPassword(), userPassword.getHashedPassword(), userPassword.getSalt(), userPassword.getHashMethod(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getUserPasswordDAO().updateUserPassword(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserPasswordDAO().deleteUserPassword(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");

        // Param userPassword cannot be null.....
        if(userPassword == null || userPassword.getGuid() == null) {
            log.log(Level.INFO, "Param userPassword or its guid is null!");
            throw new BadRequestException("Param userPassword object or its guid is null!");
        }
        UserPasswordDataObject dataObj = null;
        if(userPassword instanceof UserPasswordDataObject) {
            dataObj = (UserPasswordDataObject) userPassword;
        } else if(userPassword instanceof UserPasswordBean) {
            dataObj = ((UserPasswordBean) userPassword).toDataObject();
        } else {  // if(userPassword instanceof UserPassword)
            dataObj = new UserPasswordDataObject(userPassword.getGuid(), userPassword.getManagerApp(), userPassword.getAppAcl(), userPassword.getGaeApp(), userPassword.getOwnerUser(), userPassword.getUserAcl(), userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), userPassword.getPlainPassword(), userPassword.getHashedPassword(), userPassword.getSalt(), userPassword.getHashMethod(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getUserPasswordDAO().deleteUserPassword(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserPasswordDAO().deleteUserPasswords(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
