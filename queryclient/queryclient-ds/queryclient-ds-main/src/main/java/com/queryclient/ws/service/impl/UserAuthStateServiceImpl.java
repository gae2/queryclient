package com.queryclient.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;
import com.queryclient.ws.bean.GaeAppStructBean;
import com.queryclient.ws.bean.ExternalUserIdStructBean;
import com.queryclient.ws.bean.UserAuthStateBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.GaeAppStructDataObject;
import com.queryclient.ws.data.ExternalUserIdStructDataObject;
import com.queryclient.ws.data.UserAuthStateDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.UserAuthStateService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserAuthStateServiceImpl implements UserAuthStateService
{
    private static final Logger log = Logger.getLogger(UserAuthStateServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UserAuthState related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserAuthState getUserAuthState(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserAuthStateDataObject dataObj = getDAOFactory().getUserAuthStateDAO().getUserAuthState(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserAuthStateDataObject for guid = " + guid);
            return null;  // ????
        }
        UserAuthStateBean bean = new UserAuthStateBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserAuthState(String guid, String field) throws BaseException
    {
        UserAuthStateDataObject dataObj = getDAOFactory().getUserAuthStateDAO().getUserAuthState(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserAuthStateDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return dataObj.getManagerApp();
        } else if(field.equals("appAcl")) {
            return dataObj.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return dataObj.getGaeApp();
        } else if(field.equals("ownerUser")) {
            return dataObj.getOwnerUser();
        } else if(field.equals("userAcl")) {
            return dataObj.getUserAcl();
        } else if(field.equals("providerId")) {
            return dataObj.getProviderId();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("username")) {
            return dataObj.getUsername();
        } else if(field.equals("email")) {
            return dataObj.getEmail();
        } else if(field.equals("openId")) {
            return dataObj.getOpenId();
        } else if(field.equals("deviceId")) {
            return dataObj.getDeviceId();
        } else if(field.equals("sessionId")) {
            return dataObj.getSessionId();
        } else if(field.equals("authToken")) {
            return dataObj.getAuthToken();
        } else if(field.equals("authStatus")) {
            return dataObj.getAuthStatus();
        } else if(field.equals("externalAuth")) {
            return dataObj.getExternalAuth();
        } else if(field.equals("externalId")) {
            return dataObj.getExternalId();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("firstAuthTime")) {
            return dataObj.getFirstAuthTime();
        } else if(field.equals("lastAuthTime")) {
            return dataObj.getLastAuthTime();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserAuthState> getUserAuthStates(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserAuthState> list = new ArrayList<UserAuthState>();
        List<UserAuthStateDataObject> dataObjs = getDAOFactory().getUserAuthStateDAO().getUserAuthStates(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserAuthStateDataObject list.");
        } else {
            Iterator<UserAuthStateDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserAuthStateDataObject dataObj = (UserAuthStateDataObject) it.next();
                list.add(new UserAuthStateBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates() throws BaseException
    {
        return getAllUserAuthStates(null, null, null);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserAuthStates(ordering, offset, count, null);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserAuthStates(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<UserAuthState> list = new ArrayList<UserAuthState>();
        List<UserAuthStateDataObject> dataObjs = getDAOFactory().getUserAuthStateDAO().getAllUserAuthStates(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserAuthStateDataObject list.");
        } else {
            Iterator<UserAuthStateDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserAuthStateDataObject dataObj = (UserAuthStateDataObject) it.next();
                list.add(new UserAuthStateBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserAuthStateKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllUserAuthStateKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getUserAuthStateDAO().getAllUserAuthStateKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserAuthState key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserAuthStates(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserAuthStateServiceImpl.findUserAuthStates(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<UserAuthState> list = new ArrayList<UserAuthState>();
        List<UserAuthStateDataObject> dataObjs = getDAOFactory().getUserAuthStateDAO().findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find userAuthStates for the given criterion.");
        } else {
            Iterator<UserAuthStateDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserAuthStateDataObject dataObj = (UserAuthStateDataObject) it.next();
                list.add(new UserAuthStateBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserAuthStateServiceImpl.findUserAuthStateKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getUserAuthStateDAO().findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserAuthState keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserAuthStateServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserAuthStateDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUserAuthState(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        ExternalUserIdStructDataObject externalIdDobj = null;
        if(externalId instanceof ExternalUserIdStructBean) {
            externalIdDobj = ((ExternalUserIdStructBean) externalId).toDataObject();
        } else if(externalId instanceof ExternalUserIdStruct) {
            externalIdDobj = new ExternalUserIdStructDataObject(externalId.getUuid(), externalId.getId(), externalId.getName(), externalId.getEmail(), externalId.getUsername(), externalId.getOpenId(), externalId.getNote());
        } else {
            externalIdDobj = null;   // ????
        }
        
        UserAuthStateDataObject dataObj = new UserAuthStateDataObject(null, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalIdDobj, status, firstAuthTime, lastAuthTime, expirationTime);
        return createUserAuthState(dataObj);
    }

    @Override
    public String createUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");

        // Param userAuthState cannot be null.....
        if(userAuthState == null) {
            log.log(Level.INFO, "Param userAuthState is null!");
            throw new BadRequestException("Param userAuthState object is null!");
        }
        UserAuthStateDataObject dataObj = null;
        if(userAuthState instanceof UserAuthStateDataObject) {
            dataObj = (UserAuthStateDataObject) userAuthState;
        } else if(userAuthState instanceof UserAuthStateBean) {
            dataObj = ((UserAuthStateBean) userAuthState).toDataObject();
        } else {  // if(userAuthState instanceof UserAuthState)
            //dataObj = new UserAuthStateDataObject(null, userAuthState.getManagerApp(), userAuthState.getAppAcl(), (GaeAppStructDataObject) userAuthState.getGaeApp(), userAuthState.getOwnerUser(), userAuthState.getUserAcl(), userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), (ExternalUserIdStructDataObject) userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserAuthStateDataObject(userAuthState.getGuid(), userAuthState.getManagerApp(), userAuthState.getAppAcl(), (GaeAppStructDataObject) userAuthState.getGaeApp(), userAuthState.getOwnerUser(), userAuthState.getUserAcl(), userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), (ExternalUserIdStructDataObject) userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
        }
        String guid = getDAOFactory().getUserAuthStateDAO().createUserAuthState(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUserAuthState(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        ExternalUserIdStructDataObject externalIdDobj = null;
        if(externalId instanceof ExternalUserIdStructBean) {
            externalIdDobj = ((ExternalUserIdStructBean) externalId).toDataObject();            
        } else if(externalId instanceof ExternalUserIdStruct) {
            externalIdDobj = new ExternalUserIdStructDataObject(externalId.getUuid(), externalId.getId(), externalId.getName(), externalId.getEmail(), externalId.getUsername(), externalId.getOpenId(), externalId.getNote());
        } else {
            externalIdDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserAuthStateDataObject dataObj = new UserAuthStateDataObject(guid, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalIdDobj, status, firstAuthTime, lastAuthTime, expirationTime);
        return updateUserAuthState(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");

        // Param userAuthState cannot be null.....
        if(userAuthState == null || userAuthState.getGuid() == null) {
            log.log(Level.INFO, "Param userAuthState or its guid is null!");
            throw new BadRequestException("Param userAuthState object or its guid is null!");
        }
        UserAuthStateDataObject dataObj = null;
        if(userAuthState instanceof UserAuthStateDataObject) {
            dataObj = (UserAuthStateDataObject) userAuthState;
        } else if(userAuthState instanceof UserAuthStateBean) {
            dataObj = ((UserAuthStateBean) userAuthState).toDataObject();
        } else {  // if(userAuthState instanceof UserAuthState)
            dataObj = new UserAuthStateDataObject(userAuthState.getGuid(), userAuthState.getManagerApp(), userAuthState.getAppAcl(), userAuthState.getGaeApp(), userAuthState.getOwnerUser(), userAuthState.getUserAcl(), userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getUserAuthStateDAO().updateUserAuthState(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUserAuthState(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserAuthStateDAO().deleteUserAuthState(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");

        // Param userAuthState cannot be null.....
        if(userAuthState == null || userAuthState.getGuid() == null) {
            log.log(Level.INFO, "Param userAuthState or its guid is null!");
            throw new BadRequestException("Param userAuthState object or its guid is null!");
        }
        UserAuthStateDataObject dataObj = null;
        if(userAuthState instanceof UserAuthStateDataObject) {
            dataObj = (UserAuthStateDataObject) userAuthState;
        } else if(userAuthState instanceof UserAuthStateBean) {
            dataObj = ((UserAuthStateBean) userAuthState).toDataObject();
        } else {  // if(userAuthState instanceof UserAuthState)
            dataObj = new UserAuthStateDataObject(userAuthState.getGuid(), userAuthState.getManagerApp(), userAuthState.getAppAcl(), userAuthState.getGaeApp(), userAuthState.getOwnerUser(), userAuthState.getUserAcl(), userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getUserAuthStateDAO().deleteUserAuthState(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUserAuthStates(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserAuthStateDAO().deleteUserAuthStates(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
