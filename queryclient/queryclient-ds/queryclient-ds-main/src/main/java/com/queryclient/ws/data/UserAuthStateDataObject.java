package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class UserAuthStateDataObject extends KeyedDataObject implements UserAuthState
{
    private static final Logger log = Logger.getLogger(UserAuthStateDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(UserAuthStateDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(UserAuthStateDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String managerApp;

    @Persistent(defaultFetchGroup = "true")
    private Long appAcl;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="groupId", columns=@Column(name="gaeAppgroupId")),
        @Persistent(name="appId", columns=@Column(name="gaeAppappId")),
        @Persistent(name="appDomain", columns=@Column(name="gaeAppappDomain")),
        @Persistent(name="namespace", columns=@Column(name="gaeAppnamespace")),
        @Persistent(name="acl", columns=@Column(name="gaeAppacl")),
        @Persistent(name="note", columns=@Column(name="gaeAppnote")),
    })
    private GaeAppStructDataObject gaeApp;

    @Persistent(defaultFetchGroup = "true")
    private String ownerUser;

    @Persistent(defaultFetchGroup = "true")
    private Long userAcl;

    @Persistent(defaultFetchGroup = "true")
    private String providerId;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String username;

    @Persistent(defaultFetchGroup = "true")
    private String email;

    @Persistent(defaultFetchGroup = "true")
    private String openId;

    @Persistent(defaultFetchGroup = "true")
    private String deviceId;

    @Persistent(defaultFetchGroup = "true")
    private String sessionId;

    @Persistent(defaultFetchGroup = "true")
    private String authToken;

    @Persistent(defaultFetchGroup = "true")
    private String authStatus;

    @Persistent(defaultFetchGroup = "true")
    private String externalAuth;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="uuid", columns=@Column(name="externalIduuid")),
        @Persistent(name="id", columns=@Column(name="externalIdid")),
        @Persistent(name="name", columns=@Column(name="externalIdname")),
        @Persistent(name="email", columns=@Column(name="externalIdemail")),
        @Persistent(name="username", columns=@Column(name="externalIdusername")),
        @Persistent(name="openId", columns=@Column(name="externalIdopenId")),
        @Persistent(name="note", columns=@Column(name="externalIdnote")),
    })
    private ExternalUserIdStructDataObject externalId;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Long firstAuthTime;

    @Persistent(defaultFetchGroup = "true")
    private Long lastAuthTime;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    public UserAuthStateDataObject()
    {
        this(null);
    }
    public UserAuthStateDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserAuthStateDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime, null, null);
    }
    public UserAuthStateDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        if(gaeApp != null) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = null;
        }
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.providerId = providerId;
        this.user = user;
        this.username = username;
        this.email = email;
        this.openId = openId;
        this.deviceId = deviceId;
        this.sessionId = sessionId;
        this.authToken = authToken;
        this.authStatus = authStatus;
        this.externalAuth = externalAuth;
        if(externalId != null) {
            this.externalId = new ExternalUserIdStructDataObject(externalId.getUuid(), externalId.getId(), externalId.getName(), externalId.getEmail(), externalId.getUsername(), externalId.getOpenId(), externalId.getNote());
        } else {
            this.externalId = null;
        }
        this.status = status;
        this.firstAuthTime = firstAuthTime;
        this.lastAuthTime = lastAuthTime;
        this.expirationTime = expirationTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return UserAuthStateDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return UserAuthStateDataObject.composeKey(getGuid());
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStruct getGaeApp()
    {
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(gaeApp == null) {
            this.gaeApp = null;
            log.log(Level.INFO, "UserAuthStateDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is null.");            
        } else if(gaeApp instanceof GaeAppStructDataObject) {
            this.gaeApp = (GaeAppStructDataObject) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = new GaeAppStructDataObject();   // ????
            log.log(Level.WARNING, "UserAuthStateDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is of an invalid type.");
        }
    }

    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    public String getProviderId()
    {
        return this.providerId;
    }
    public void setProviderId(String providerId)
    {
        this.providerId = providerId;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getOpenId()
    {
        return this.openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    public String getDeviceId()
    {
        return this.deviceId;
    }
    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getSessionId()
    {
        return this.sessionId;
    }
    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getAuthToken()
    {
        return this.authToken;
    }
    public void setAuthToken(String authToken)
    {
        this.authToken = authToken;
    }

    public String getAuthStatus()
    {
        return this.authStatus;
    }
    public void setAuthStatus(String authStatus)
    {
        this.authStatus = authStatus;
    }

    public String getExternalAuth()
    {
        return this.externalAuth;
    }
    public void setExternalAuth(String externalAuth)
    {
        this.externalAuth = externalAuth;
    }

    public ExternalUserIdStruct getExternalId()
    {
        return this.externalId;
    }
    public void setExternalId(ExternalUserIdStruct externalId)
    {
        if(externalId == null) {
            this.externalId = null;
            log.log(Level.INFO, "UserAuthStateDataObject.setExternalId(ExternalUserIdStruct externalId): Arg externalId is null.");            
        } else if(externalId instanceof ExternalUserIdStructDataObject) {
            this.externalId = (ExternalUserIdStructDataObject) externalId;
        } else if(externalId instanceof ExternalUserIdStruct) {
            this.externalId = new ExternalUserIdStructDataObject(externalId.getUuid(), externalId.getId(), externalId.getName(), externalId.getEmail(), externalId.getUsername(), externalId.getOpenId(), externalId.getNote());
        } else {
            this.externalId = new ExternalUserIdStructDataObject();   // ????
            log.log(Level.WARNING, "UserAuthStateDataObject.setExternalId(ExternalUserIdStruct externalId): Arg externalId is of an invalid type.");
        }
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getFirstAuthTime()
    {
        return this.firstAuthTime;
    }
    public void setFirstAuthTime(Long firstAuthTime)
    {
        this.firstAuthTime = firstAuthTime;
    }

    public Long getLastAuthTime()
    {
        return this.lastAuthTime;
    }
    public void setLastAuthTime(Long lastAuthTime)
    {
        this.lastAuthTime = lastAuthTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("ownerUser", this.ownerUser);
        dataMap.put("userAcl", this.userAcl);
        dataMap.put("providerId", this.providerId);
        dataMap.put("user", this.user);
        dataMap.put("username", this.username);
        dataMap.put("email", this.email);
        dataMap.put("openId", this.openId);
        dataMap.put("deviceId", this.deviceId);
        dataMap.put("sessionId", this.sessionId);
        dataMap.put("authToken", this.authToken);
        dataMap.put("authStatus", this.authStatus);
        dataMap.put("externalAuth", this.externalAuth);
        dataMap.put("externalId", this.externalId);
        dataMap.put("status", this.status);
        dataMap.put("firstAuthTime", this.firstAuthTime);
        dataMap.put("lastAuthTime", this.lastAuthTime);
        dataMap.put("expirationTime", this.expirationTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        UserAuthState thatObj = (UserAuthState) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.managerApp == null && thatObj.getManagerApp() != null)
            || (this.managerApp != null && thatObj.getManagerApp() == null)
            || !this.managerApp.equals(thatObj.getManagerApp()) ) {
            return false;
        }
        if( (this.appAcl == null && thatObj.getAppAcl() != null)
            || (this.appAcl != null && thatObj.getAppAcl() == null)
            || !this.appAcl.equals(thatObj.getAppAcl()) ) {
            return false;
        }
        if( (this.gaeApp == null && thatObj.getGaeApp() != null)
            || (this.gaeApp != null && thatObj.getGaeApp() == null)
            || !this.gaeApp.equals(thatObj.getGaeApp()) ) {
            return false;
        }
        if( (this.ownerUser == null && thatObj.getOwnerUser() != null)
            || (this.ownerUser != null && thatObj.getOwnerUser() == null)
            || !this.ownerUser.equals(thatObj.getOwnerUser()) ) {
            return false;
        }
        if( (this.userAcl == null && thatObj.getUserAcl() != null)
            || (this.userAcl != null && thatObj.getUserAcl() == null)
            || !this.userAcl.equals(thatObj.getUserAcl()) ) {
            return false;
        }
        if( (this.providerId == null && thatObj.getProviderId() != null)
            || (this.providerId != null && thatObj.getProviderId() == null)
            || !this.providerId.equals(thatObj.getProviderId()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.username == null && thatObj.getUsername() != null)
            || (this.username != null && thatObj.getUsername() == null)
            || !this.username.equals(thatObj.getUsername()) ) {
            return false;
        }
        if( (this.email == null && thatObj.getEmail() != null)
            || (this.email != null && thatObj.getEmail() == null)
            || !this.email.equals(thatObj.getEmail()) ) {
            return false;
        }
        if( (this.openId == null && thatObj.getOpenId() != null)
            || (this.openId != null && thatObj.getOpenId() == null)
            || !this.openId.equals(thatObj.getOpenId()) ) {
            return false;
        }
        if( (this.deviceId == null && thatObj.getDeviceId() != null)
            || (this.deviceId != null && thatObj.getDeviceId() == null)
            || !this.deviceId.equals(thatObj.getDeviceId()) ) {
            return false;
        }
        if( (this.sessionId == null && thatObj.getSessionId() != null)
            || (this.sessionId != null && thatObj.getSessionId() == null)
            || !this.sessionId.equals(thatObj.getSessionId()) ) {
            return false;
        }
        if( (this.authToken == null && thatObj.getAuthToken() != null)
            || (this.authToken != null && thatObj.getAuthToken() == null)
            || !this.authToken.equals(thatObj.getAuthToken()) ) {
            return false;
        }
        if( (this.authStatus == null && thatObj.getAuthStatus() != null)
            || (this.authStatus != null && thatObj.getAuthStatus() == null)
            || !this.authStatus.equals(thatObj.getAuthStatus()) ) {
            return false;
        }
        if( (this.externalAuth == null && thatObj.getExternalAuth() != null)
            || (this.externalAuth != null && thatObj.getExternalAuth() == null)
            || !this.externalAuth.equals(thatObj.getExternalAuth()) ) {
            return false;
        }
        if( (this.externalId == null && thatObj.getExternalId() != null)
            || (this.externalId != null && thatObj.getExternalId() == null)
            || !this.externalId.equals(thatObj.getExternalId()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.firstAuthTime == null && thatObj.getFirstAuthTime() != null)
            || (this.firstAuthTime != null && thatObj.getFirstAuthTime() == null)
            || !this.firstAuthTime.equals(thatObj.getFirstAuthTime()) ) {
            return false;
        }
        if( (this.lastAuthTime == null && thatObj.getLastAuthTime() != null)
            || (this.lastAuthTime != null && thatObj.getLastAuthTime() == null)
            || !this.lastAuthTime.equals(thatObj.getLastAuthTime()) ) {
            return false;
        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = ownerUser == null ? 0 : ownerUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAcl == null ? 0 : userAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = providerId == null ? 0 : providerId.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = openId == null ? 0 : openId.hashCode();
        _hash = 31 * _hash + delta;
        delta = deviceId == null ? 0 : deviceId.hashCode();
        _hash = 31 * _hash + delta;
        delta = sessionId == null ? 0 : sessionId.hashCode();
        _hash = 31 * _hash + delta;
        delta = authToken == null ? 0 : authToken.hashCode();
        _hash = 31 * _hash + delta;
        delta = authStatus == null ? 0 : authStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = externalAuth == null ? 0 : externalAuth.hashCode();
        _hash = 31 * _hash + delta;
        delta = externalId == null ? 0 : externalId.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = firstAuthTime == null ? 0 : firstAuthTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastAuthTime == null ? 0 : lastAuthTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
