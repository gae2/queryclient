package com.queryclient.ws;



public interface ServiceEndpoint 
{
    String  getGuid();
    String  getUser();
    String  getDataService();
    String  getServiceName();
    String  getServiceUrl();
    Boolean  isAuthRequired();
    ConsumerKeySecretPair  getAuthCredential();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
