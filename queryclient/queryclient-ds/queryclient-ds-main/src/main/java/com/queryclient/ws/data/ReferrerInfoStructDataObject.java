package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class ReferrerInfoStructDataObject implements ReferrerInfoStruct, Serializable
{
    private static final Logger log = Logger.getLogger(ReferrerInfoStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _referrerinfostruct_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String referer;

    @Persistent(defaultFetchGroup = "true")
    private String userAgent;

    @Persistent(defaultFetchGroup = "true")
    private String language;

    @Persistent(defaultFetchGroup = "true")
    private String hostname;

    @Persistent(defaultFetchGroup = "true")
    private String ipAddress;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public ReferrerInfoStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null);
    }
    public ReferrerInfoStructDataObject(String referer, String userAgent, String language, String hostname, String ipAddress, String note)
    {
        setReferer(referer);
        setUserAgent(userAgent);
        setLanguage(language);
        setHostname(hostname);
        setIpAddress(ipAddress);
        setNote(note);
    }

    private void resetEncodedKey()
    {
    }

    public String getReferer()
    {
        return this.referer;
    }
    public void setReferer(String referer)
    {
        this.referer = referer;
        if(this.referer != null) {
            resetEncodedKey();
        }
    }

    public String getUserAgent()
    {
        return this.userAgent;
    }
    public void setUserAgent(String userAgent)
    {
        this.userAgent = userAgent;
        if(this.userAgent != null) {
            resetEncodedKey();
        }
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
        if(this.language != null) {
            resetEncodedKey();
        }
    }

    public String getHostname()
    {
        return this.hostname;
    }
    public void setHostname(String hostname)
    {
        this.hostname = hostname;
        if(this.hostname != null) {
            resetEncodedKey();
        }
    }

    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
        if(this.ipAddress != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getReferer() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUserAgent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLanguage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHostname() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getIpAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("referer", this.referer);
        dataMap.put("userAgent", this.userAgent);
        dataMap.put("language", this.language);
        dataMap.put("hostname", this.hostname);
        dataMap.put("ipAddress", this.ipAddress);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ReferrerInfoStruct thatObj = (ReferrerInfoStruct) obj;
        if( (this.referer == null && thatObj.getReferer() != null)
            || (this.referer != null && thatObj.getReferer() == null)
            || !this.referer.equals(thatObj.getReferer()) ) {
            return false;
        }
        if( (this.userAgent == null && thatObj.getUserAgent() != null)
            || (this.userAgent != null && thatObj.getUserAgent() == null)
            || !this.userAgent.equals(thatObj.getUserAgent()) ) {
            return false;
        }
        if( (this.language == null && thatObj.getLanguage() != null)
            || (this.language != null && thatObj.getLanguage() == null)
            || !this.language.equals(thatObj.getLanguage()) ) {
            return false;
        }
        if( (this.hostname == null && thatObj.getHostname() != null)
            || (this.hostname != null && thatObj.getHostname() == null)
            || !this.hostname.equals(thatObj.getHostname()) ) {
            return false;
        }
        if( (this.ipAddress == null && thatObj.getIpAddress() != null)
            || (this.ipAddress != null && thatObj.getIpAddress() == null)
            || !this.ipAddress.equals(thatObj.getIpAddress()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = referer == null ? 0 : referer.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAgent == null ? 0 : userAgent.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = hostname == null ? 0 : hostname.hashCode();
        _hash = 31 * _hash + delta;
        delta = ipAddress == null ? 0 : ipAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
