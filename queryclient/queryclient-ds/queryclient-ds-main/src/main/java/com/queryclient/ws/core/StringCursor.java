package com.queryclient.ws.core;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.Serializable;


// Sort of a wrapper of a cursor, or more precisely, the "web safe string" of a cursor.
// This is needed so that, primarily, it can be used as an "in-out" param.
public class StringCursor implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(StringCursor.class.getName());


    // "Web safe string" for a given DB cursor.
    private String webSafeString = null;
    // (Optionally) To be used as an external param value (instead of webSafeString).
    // Normally, but not necessarily, based on the hash value of webSafeString.
    // (Note that this is not automatically set by this class (e.g., in the ctor/setter).
    // The caller should, if deemed necessary, set/unset this value according to its own hash algo, etc.) 
    private String token = null;


    public StringCursor()
    {
        this(null);
    }
    // Note that one-String-arg constructor is necessary for this to be usable as a JAX_RS query parameter.
    // (Or, alternatively, we can implement static valueOf(String) or fromString(String).)
    public StringCursor(String webSafeString)
    {
        this(webSafeString, null);
    }
    public StringCursor(String webSafeString, String token)
    {
        this.webSafeString = webSafeString;
        this.token = token;
    }


    // null/empty webSafeString may means that there is no more data,
    //     that is, the cursor is at the end of the associated list.
    public boolean isEmpty()
    {
        if(webSafeString == null || webSafeString.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public String getWebSafeString()
    {
        return webSafeString;
    }
    public void setWebSafeString(String currentWebSafeString)
    {
        this.webSafeString = currentWebSafeString;
    }

    public String getToken()
    {
        return token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }


}
