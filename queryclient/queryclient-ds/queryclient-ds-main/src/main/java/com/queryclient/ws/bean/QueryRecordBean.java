package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.data.ReferrerInfoStructDataObject;
import com.queryclient.ws.data.QueryRecordDataObject;

public class QueryRecordBean extends BeanBase implements QueryRecord
{
    private static final Logger log = Logger.getLogger(QueryRecordBean.class.getName());

    // Embedded data object.
    private QueryRecordDataObject dobj = null;

    public QueryRecordBean()
    {
        this(new QueryRecordDataObject());
    }
    public QueryRecordBean(String guid)
    {
        this(new QueryRecordDataObject(guid));
    }
    public QueryRecordBean(QueryRecordDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public QueryRecordDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getQuerySession()
    {
        if(getDataObject() != null) {
            return getDataObject().getQuerySession();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setQuerySession(String querySession)
    {
        if(getDataObject() != null) {
            getDataObject().setQuerySession(querySession);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public Integer getQueryId()
    {
        if(getDataObject() != null) {
            return getDataObject().getQueryId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setQueryId(Integer queryId)
    {
        if(getDataObject() != null) {
            getDataObject().setQueryId(queryId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getDataService()
    {
        if(getDataObject() != null) {
            return getDataObject().getDataService();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setDataService(String dataService)
    {
        if(getDataObject() != null) {
            getDataObject().setDataService(dataService);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getServiceUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceUrl(serviceUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public Boolean isDelayed()
    {
        if(getDataObject() != null) {
            return getDataObject().isDelayed();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setDelayed(Boolean delayed)
    {
        if(getDataObject() != null) {
            getDataObject().setDelayed(delayed);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getQuery()
    {
        if(getDataObject() != null) {
            return getDataObject().getQuery();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setQuery(String query)
    {
        if(getDataObject() != null) {
            getDataObject().setQuery(query);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getInputFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getInputFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setInputFormat(String inputFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setInputFormat(inputFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getInputFile()
    {
        if(getDataObject() != null) {
            return getDataObject().getInputFile();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setInputFile(String inputFile)
    {
        if(getDataObject() != null) {
            getDataObject().setInputFile(inputFile);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getInputContent()
    {
        if(getDataObject() != null) {
            return getDataObject().getInputContent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setInputContent(String inputContent)
    {
        if(getDataObject() != null) {
            getDataObject().setInputContent(inputContent);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getTargetOutputFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getTargetOutputFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setTargetOutputFormat(String targetOutputFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setTargetOutputFormat(targetOutputFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getOutputFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputFormat(String outputFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputFormat(outputFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getOutputFile()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputFile();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputFile(String outputFile)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputFile(outputFile);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getOutputContent()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputContent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputContent(String outputContent)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputContent(outputContent);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public Integer getResponseCode()
    {
        if(getDataObject() != null) {
            return getDataObject().getResponseCode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setResponseCode(Integer responseCode)
    {
        if(getDataObject() != null) {
            getDataObject().setResponseCode(responseCode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getResult()
    {
        if(getDataObject() != null) {
            return getDataObject().getResult();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setResult(String result)
    {
        if(getDataObject() != null) {
            getDataObject().setResult(result);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        if(getDataObject() != null) {
            ReferrerInfoStruct _field = getDataObject().getReferrerInfo();
            if(_field == null) {
                log.log(Level.INFO, "referrerInfo is null.");
                return null;
            } else {
                return new ReferrerInfoStructBean((ReferrerInfoStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getDataObject() != null) {
            getDataObject().setReferrerInfo(
                (referrerInfo instanceof ReferrerInfoStructBean) ?
                ((ReferrerInfoStructBean) referrerInfo).toDataObject() :
                ((referrerInfo instanceof ReferrerInfoStructDataObject) ? referrerInfo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getExtra()
    {
        if(getDataObject() != null) {
            return getDataObject().getExtra();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setExtra(String extra)
    {
        if(getDataObject() != null) {
            getDataObject().setExtra(extra);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public Long getScheduledTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getScheduledTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setScheduledTime(Long scheduledTime)
    {
        if(getDataObject() != null) {
            getDataObject().setScheduledTime(scheduledTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }

    public Long getProcessedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getProcessedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
            return null;   // ???
        }
    }
    public void setProcessedTime(Long processedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setProcessedTime(processedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QueryRecordDataObject is null!");
        }
    }


    // TBD
    public QueryRecordDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
