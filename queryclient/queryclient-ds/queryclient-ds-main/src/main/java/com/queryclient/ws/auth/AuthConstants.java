package com.queryclient.ws.auth;

public class AuthConstants
{
    private AuthConstants() {}

    // TBD
    public static String PARAM_VERSION = "aeryid_version";
    public static String PARAM_AUTH_TOKEN = "aeryid_auth_token";
    public static String PARAM_CLIENT_KEY = "aeryid_client_key";
    public static String PARAM_CLIENT_SECRET = "aeryid_client_secret";
    public static String PARAM_USER_KEY = "aeryid_user_key";
    public static String PARAM_USER_SECRET = "aeryid_user_secret";
    public static String PARAM_TARGET_CLIENT = "aeryid_target_client";
    public static String PARAM_TARGET_USER = "aeryid_target_user";
    public static String PARAM_TIMESTAMP = "aeryid_timestamp";
    public static String PARAM_REQUEST_GUID = "aeryid_request_guid";
    public static String PARAM_SIGNATURE = "aeryid_signature";
    public static String PARAM_SIGNATURE_METHOD = "aeryid_signature_method";
    // ...
    public static String ATTR_AUTHORIZED = "attr_authorized";
    // ...

}
