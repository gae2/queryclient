package com.queryclient.ws.resource.exception;

import com.queryclient.ws.exception.resource.BaseResourceException;


public class NotAcceptableRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public NotAcceptableRsException() 
    {
        super();
    }
    public NotAcceptableRsException(String message) 
    {
        super(message);
    }
    public NotAcceptableRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public NotAcceptableRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public NotAcceptableRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public NotAcceptableRsException(Throwable cause) 
    {
        super(cause);
    }
    public NotAcceptableRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
