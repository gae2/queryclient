package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.ws.data.ConsumerKeySecretPairDataObject;
import com.queryclient.ws.data.ReferrerInfoStructDataObject;
import com.queryclient.ws.data.DataServiceDataObject;

public class DataServiceBean extends BeanBase implements DataService
{
    private static final Logger log = Logger.getLogger(DataServiceBean.class.getName());

    // Embedded data object.
    private DataServiceDataObject dobj = null;

    public DataServiceBean()
    {
        this(new DataServiceDataObject());
    }
    public DataServiceBean(String guid)
    {
        this(new DataServiceDataObject(guid));
    }
    public DataServiceBean(DataServiceDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public DataServiceDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }

    public String getName()
    {
        if(getDataObject() != null) {
            return getDataObject().getName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;   // ???
        }
    }
    public void setName(String name)
    {
        if(getDataObject() != null) {
            getDataObject().setName(name);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }

    public String getMode()
    {
        if(getDataObject() != null) {
            return getDataObject().getMode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;   // ???
        }
    }
    public void setMode(String mode)
    {
        if(getDataObject() != null) {
            getDataObject().setMode(mode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }

    public String getServiceUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceUrl(serviceUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }

    public Boolean isAuthRequired()
    {
        if(getDataObject() != null) {
            return getDataObject().isAuthRequired();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthRequired(Boolean authRequired)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthRequired(authRequired);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }

    public ConsumerKeySecretPair getAuthCredential()
    {
        if(getDataObject() != null) {
            ConsumerKeySecretPair _field = getDataObject().getAuthCredential();
            if(_field == null) {
                log.log(Level.INFO, "authCredential is null.");
                return null;
            } else {
                return new ConsumerKeySecretPairBean((ConsumerKeySecretPairDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthCredential(ConsumerKeySecretPair authCredential)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthCredential(
                (authCredential instanceof ConsumerKeySecretPairBean) ?
                ((ConsumerKeySecretPairBean) authCredential).toDataObject() :
                ((authCredential instanceof ConsumerKeySecretPairDataObject) ? authCredential : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        if(getDataObject() != null) {
            ReferrerInfoStruct _field = getDataObject().getReferrerInfo();
            if(_field == null) {
                log.log(Level.INFO, "referrerInfo is null.");
                return null;
            } else {
                return new ReferrerInfoStructBean((ReferrerInfoStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getDataObject() != null) {
            getDataObject().setReferrerInfo(
                (referrerInfo instanceof ReferrerInfoStructBean) ?
                ((ReferrerInfoStructBean) referrerInfo).toDataObject() :
                ((referrerInfo instanceof ReferrerInfoStructDataObject) ? referrerInfo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataServiceDataObject is null!");
        }
    }


    // TBD
    public DataServiceDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
