package com.queryclient.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.bean.ExternalUserIdStructBean;
import com.queryclient.ws.stub.ExternalUserIdStructStub;


public class ExternalUserIdStructResourceUtil
{
    private static final Logger log = Logger.getLogger(ExternalUserIdStructResourceUtil.class.getName());

    // Static methods only.
    private ExternalUserIdStructResourceUtil() {}

    public static ExternalUserIdStructBean convertExternalUserIdStructStubToBean(ExternalUserIdStruct stub)
    {
        ExternalUserIdStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null ExternalUserIdStructBean is returned.");
        } else {
            bean = new ExternalUserIdStructBean();
            bean.setUuid(stub.getUuid());
            bean.setId(stub.getId());
            bean.setName(stub.getName());
            bean.setEmail(stub.getEmail());
            bean.setUsername(stub.getUsername());
            bean.setOpenId(stub.getOpenId());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
