package com.queryclient.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.bean.ReferrerInfoStructBean;
import com.queryclient.ws.bean.QuerySessionBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.ReferrerInfoStructDataObject;
import com.queryclient.ws.data.QuerySessionDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.QuerySessionService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class QuerySessionServiceImpl implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(QuerySessionServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // QuerySession related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        log.finer("BEGIN");

        QuerySessionDataObject dataObj = getDAOFactory().getQuerySessionDAO().getQuerySession(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve QuerySessionDataObject for guid = " + guid);
            return null;  // ????
        }
        QuerySessionBean bean = new QuerySessionBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        QuerySessionDataObject dataObj = getDAOFactory().getQuerySessionDAO().getQuerySession(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve QuerySessionDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("dataService")) {
            return dataObj.getDataService();
        } else if(field.equals("serviceUrl")) {
            return dataObj.getServiceUrl();
        } else if(field.equals("inputFormat")) {
            return dataObj.getInputFormat();
        } else if(field.equals("outputFormat")) {
            return dataObj.getOutputFormat();
        } else if(field.equals("referrerInfo")) {
            return dataObj.getReferrerInfo();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<QuerySession> list = new ArrayList<QuerySession>();
        List<QuerySessionDataObject> dataObjs = getDAOFactory().getQuerySessionDAO().getQuerySessions(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve QuerySessionDataObject list.");
        } else {
            Iterator<QuerySessionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                QuerySessionDataObject dataObj = (QuerySessionDataObject) it.next();
                list.add(new QuerySessionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return getAllQuerySessions(null, null, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQuerySessions(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<QuerySession> list = new ArrayList<QuerySession>();
        List<QuerySessionDataObject> dataObjs = getDAOFactory().getQuerySessionDAO().getAllQuerySessions(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve QuerySessionDataObject list.");
        } else {
            Iterator<QuerySessionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                QuerySessionDataObject dataObj = (QuerySessionDataObject) it.next();
                list.add(new QuerySessionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllQuerySessionKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getQuerySessionDAO().getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve QuerySession key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("QuerySessionServiceImpl.findQuerySessions(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<QuerySession> list = new ArrayList<QuerySession>();
        List<QuerySessionDataObject> dataObjs = getDAOFactory().getQuerySessionDAO().findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find querySessions for the given criterion.");
        } else {
            Iterator<QuerySessionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                QuerySessionDataObject dataObj = (QuerySessionDataObject) it.next();
                list.add(new QuerySessionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("QuerySessionServiceImpl.findQuerySessionKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getQuerySessionDAO().findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find QuerySession keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("QuerySessionServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getQuerySessionDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        
        QuerySessionDataObject dataObj = new QuerySessionDataObject(null, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfoDobj, status, note);
        return createQuerySession(dataObj);
    }

    @Override
    public String createQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");

        // Param querySession cannot be null.....
        if(querySession == null) {
            log.log(Level.INFO, "Param querySession is null!");
            throw new BadRequestException("Param querySession object is null!");
        }
        QuerySessionDataObject dataObj = null;
        if(querySession instanceof QuerySessionDataObject) {
            dataObj = (QuerySessionDataObject) querySession;
        } else if(querySession instanceof QuerySessionBean) {
            dataObj = ((QuerySessionBean) querySession).toDataObject();
        } else {  // if(querySession instanceof QuerySession)
            //dataObj = new QuerySessionDataObject(null, querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), (ReferrerInfoStructDataObject) querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new QuerySessionDataObject(querySession.getGuid(), querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), (ReferrerInfoStructDataObject) querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
        }
        String guid = getDAOFactory().getQuerySessionDAO().createQuerySession(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();            
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        QuerySessionDataObject dataObj = new QuerySessionDataObject(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfoDobj, status, note);
        return updateQuerySession(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");

        // Param querySession cannot be null.....
        if(querySession == null || querySession.getGuid() == null) {
            log.log(Level.INFO, "Param querySession or its guid is null!");
            throw new BadRequestException("Param querySession object or its guid is null!");
        }
        QuerySessionDataObject dataObj = null;
        if(querySession instanceof QuerySessionDataObject) {
            dataObj = (QuerySessionDataObject) querySession;
        } else if(querySession instanceof QuerySessionBean) {
            dataObj = ((QuerySessionBean) querySession).toDataObject();
        } else {  // if(querySession instanceof QuerySession)
            dataObj = new QuerySessionDataObject(querySession.getGuid(), querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
        }
        Boolean suc = getDAOFactory().getQuerySessionDAO().updateQuerySession(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getQuerySessionDAO().deleteQuerySession(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");

        // Param querySession cannot be null.....
        if(querySession == null || querySession.getGuid() == null) {
            log.log(Level.INFO, "Param querySession or its guid is null!");
            throw new BadRequestException("Param querySession object or its guid is null!");
        }
        QuerySessionDataObject dataObj = null;
        if(querySession instanceof QuerySessionDataObject) {
            dataObj = (QuerySessionDataObject) querySession;
        } else if(querySession instanceof QuerySessionBean) {
            dataObj = ((QuerySessionBean) querySession).toDataObject();
        } else {  // if(querySession instanceof QuerySession)
            dataObj = new QuerySessionDataObject(querySession.getGuid(), querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
        }
        Boolean suc = getDAOFactory().getQuerySessionDAO().deleteQuerySession(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getQuerySessionDAO().deleteQuerySessions(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
