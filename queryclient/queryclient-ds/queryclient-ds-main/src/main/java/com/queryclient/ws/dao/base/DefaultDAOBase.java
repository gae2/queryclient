package com.queryclient.ws.dao.base;

import javax.jdo.PersistenceManager;

// PersistenceManager can be injected to DAO objects.
// For now, we just use a global singleton.
// DefaultDAOBase is a simple super class providing a set of convenience methods.  
public abstract class DefaultDAOBase
{
    // Temporary
    public final int MAX_FETCH_COUNT = 1000;
    // ...

    protected PersistenceManager getPersistenceManager()
    {
        // TBD: Use different types of PMFs (e.g., with different configs, etc.).
        PersistenceManager pm = Pmf.get().getPersistenceManager();

        // TBD: Set these in jdoconfig file????
        pm.setCopyOnAttach(false);
        pm.setDetachAllOnCommit(true);
        pm.getFetchPlan().setMaxFetchDepth(3);

        return pm;
    }

}
