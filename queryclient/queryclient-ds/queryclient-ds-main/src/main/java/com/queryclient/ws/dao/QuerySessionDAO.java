package com.queryclient.ws.dao;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.data.QuerySessionDataObject;


// TBD: Add offset/count to getAllQuerySessions() and findQuerySessions(), etc.
public interface QuerySessionDAO
{
    QuerySessionDataObject getQuerySession(String guid) throws BaseException;
    List<QuerySessionDataObject> getQuerySessions(List<String> guids) throws BaseException;
    List<QuerySessionDataObject> getAllQuerySessions() throws BaseException;
    /* @Deprecated */ List<QuerySessionDataObject> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException;
    List<QuerySessionDataObject> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<QuerySessionDataObject> findQuerySessions(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<QuerySessionDataObject> findQuerySessions(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<QuerySessionDataObject> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<QuerySessionDataObject> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createQuerySession(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return QuerySessionDataObject?)
    String createQuerySession(QuerySessionDataObject querySession) throws BaseException;          // Returns Guid.  (Return QuerySessionDataObject?)
    //Boolean updateQuerySession(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateQuerySession(QuerySessionDataObject querySession) throws BaseException;
    Boolean deleteQuerySession(String guid) throws BaseException;
    Boolean deleteQuerySession(QuerySessionDataObject querySession) throws BaseException;
    Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException;
}
