package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class QueryRecordDataObject extends KeyedDataObject implements QueryRecord
{
    private static final Logger log = Logger.getLogger(QueryRecordDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(QueryRecordDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(QueryRecordDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String querySession;

    @Persistent(defaultFetchGroup = "true")
    private Integer queryId;

    @Persistent(defaultFetchGroup = "true")
    private String dataService;

    @Persistent(defaultFetchGroup = "true")
    private String serviceUrl;

    @Persistent(defaultFetchGroup = "true")
    private Boolean delayed;

    @Persistent(defaultFetchGroup = "false")
    private Text query;

    @Persistent(defaultFetchGroup = "true")
    private String inputFormat;

    @Persistent(defaultFetchGroup = "true")
    private String inputFile;

    @Persistent(defaultFetchGroup = "false")
    private Text inputContent;

    @Persistent(defaultFetchGroup = "true")
    private String targetOutputFormat;

    @Persistent(defaultFetchGroup = "true")
    private String outputFormat;

    @Persistent(defaultFetchGroup = "true")
    private String outputFile;

    @Persistent(defaultFetchGroup = "false")
    private Text outputContent;

    @Persistent(defaultFetchGroup = "true")
    private Integer responseCode;

    @Persistent(defaultFetchGroup = "true")
    private String result;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="referer", columns=@Column(name="referrerInforeferer")),
        @Persistent(name="userAgent", columns=@Column(name="referrerInfouserAgent")),
        @Persistent(name="language", columns=@Column(name="referrerInfolanguage")),
        @Persistent(name="hostname", columns=@Column(name="referrerInfohostname")),
        @Persistent(name="ipAddress", columns=@Column(name="referrerInfoipAddress")),
        @Persistent(name="note", columns=@Column(name="referrerInfonote")),
    })
    private ReferrerInfoStructDataObject referrerInfo;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String extra;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private Long scheduledTime;

    @Persistent(defaultFetchGroup = "true")
    private Long processedTime;

    public QueryRecordDataObject()
    {
        this(null);
    }
    public QueryRecordDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public QueryRecordDataObject(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime)
    {
        this(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime, null, null);
    }
    public QueryRecordDataObject(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.querySession = querySession;
        this.queryId = queryId;
        this.dataService = dataService;
        this.serviceUrl = serviceUrl;
        this.delayed = delayed;
        setQuery(query);
        this.inputFormat = inputFormat;
        this.inputFile = inputFile;
        setInputContent(inputContent);
        this.targetOutputFormat = targetOutputFormat;
        this.outputFormat = outputFormat;
        this.outputFile = outputFile;
        setOutputContent(outputContent);
        this.responseCode = responseCode;
        this.result = result;
        if(referrerInfo != null) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = null;
        }
        this.status = status;
        this.extra = extra;
        this.note = note;
        this.scheduledTime = scheduledTime;
        this.processedTime = processedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return QueryRecordDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return QueryRecordDataObject.composeKey(getGuid());
    }

    public String getQuerySession()
    {
        return this.querySession;
    }
    public void setQuerySession(String querySession)
    {
        this.querySession = querySession;
    }

    public Integer getQueryId()
    {
        return this.queryId;
    }
    public void setQueryId(Integer queryId)
    {
        this.queryId = queryId;
    }

    public String getDataService()
    {
        return this.dataService;
    }
    public void setDataService(String dataService)
    {
        this.dataService = dataService;
    }

    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    public Boolean isDelayed()
    {
        return this.delayed;
    }
    public void setDelayed(Boolean delayed)
    {
        this.delayed = delayed;
    }

    public String getQuery()
    {
        if(this.query == null) {
            return null;
        }    
        return this.query.getValue();
    }
    public void setQuery(String query)
    {
        if(query == null) {
            this.query = null;
        } else {
            this.query = new Text(query);
        }
    }

    public String getInputFormat()
    {
        return this.inputFormat;
    }
    public void setInputFormat(String inputFormat)
    {
        this.inputFormat = inputFormat;
    }

    public String getInputFile()
    {
        return this.inputFile;
    }
    public void setInputFile(String inputFile)
    {
        this.inputFile = inputFile;
    }

    public String getInputContent()
    {
        if(this.inputContent == null) {
            return null;
        }    
        return this.inputContent.getValue();
    }
    public void setInputContent(String inputContent)
    {
        if(inputContent == null) {
            this.inputContent = null;
        } else {
            this.inputContent = new Text(inputContent);
        }
    }

    public String getTargetOutputFormat()
    {
        return this.targetOutputFormat;
    }
    public void setTargetOutputFormat(String targetOutputFormat)
    {
        this.targetOutputFormat = targetOutputFormat;
    }

    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    public String getOutputFile()
    {
        return this.outputFile;
    }
    public void setOutputFile(String outputFile)
    {
        this.outputFile = outputFile;
    }

    public String getOutputContent()
    {
        if(this.outputContent == null) {
            return null;
        }    
        return this.outputContent.getValue();
    }
    public void setOutputContent(String outputContent)
    {
        if(outputContent == null) {
            this.outputContent = null;
        } else {
            this.outputContent = new Text(outputContent);
        }
    }

    public Integer getResponseCode()
    {
        return this.responseCode;
    }
    public void setResponseCode(Integer responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(referrerInfo == null) {
            this.referrerInfo = null;
            log.log(Level.INFO, "QueryRecordDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is null.");            
        } else if(referrerInfo instanceof ReferrerInfoStructDataObject) {
            this.referrerInfo = (ReferrerInfoStructDataObject) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = new ReferrerInfoStructDataObject();   // ????
            log.log(Level.WARNING, "QueryRecordDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is of an invalid type.");
        }
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getExtra()
    {
        return this.extra;
    }
    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getScheduledTime()
    {
        return this.scheduledTime;
    }
    public void setScheduledTime(Long scheduledTime)
    {
        this.scheduledTime = scheduledTime;
    }

    public Long getProcessedTime()
    {
        return this.processedTime;
    }
    public void setProcessedTime(Long processedTime)
    {
        this.processedTime = processedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("querySession", this.querySession);
        dataMap.put("queryId", this.queryId);
        dataMap.put("dataService", this.dataService);
        dataMap.put("serviceUrl", this.serviceUrl);
        dataMap.put("delayed", this.delayed);
        dataMap.put("query", this.query);
        dataMap.put("inputFormat", this.inputFormat);
        dataMap.put("inputFile", this.inputFile);
        dataMap.put("inputContent", this.inputContent);
        dataMap.put("targetOutputFormat", this.targetOutputFormat);
        dataMap.put("outputFormat", this.outputFormat);
        dataMap.put("outputFile", this.outputFile);
        dataMap.put("outputContent", this.outputContent);
        dataMap.put("responseCode", this.responseCode);
        dataMap.put("result", this.result);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("status", this.status);
        dataMap.put("extra", this.extra);
        dataMap.put("note", this.note);
        dataMap.put("scheduledTime", this.scheduledTime);
        dataMap.put("processedTime", this.processedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        QueryRecord thatObj = (QueryRecord) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.querySession == null && thatObj.getQuerySession() != null)
            || (this.querySession != null && thatObj.getQuerySession() == null)
            || !this.querySession.equals(thatObj.getQuerySession()) ) {
            return false;
        }
        if( (this.queryId == null && thatObj.getQueryId() != null)
            || (this.queryId != null && thatObj.getQueryId() == null)
            || !this.queryId.equals(thatObj.getQueryId()) ) {
            return false;
        }
        if( (this.dataService == null && thatObj.getDataService() != null)
            || (this.dataService != null && thatObj.getDataService() == null)
            || !this.dataService.equals(thatObj.getDataService()) ) {
            return false;
        }
        if( (this.serviceUrl == null && thatObj.getServiceUrl() != null)
            || (this.serviceUrl != null && thatObj.getServiceUrl() == null)
            || !this.serviceUrl.equals(thatObj.getServiceUrl()) ) {
            return false;
        }
        if( (this.delayed == null && thatObj.isDelayed() != null)
            || (this.delayed != null && thatObj.isDelayed() == null)
            || !this.delayed.equals(thatObj.isDelayed()) ) {
            return false;
        }
        if( (this.query == null && thatObj.getQuery() != null)
            || (this.query != null && thatObj.getQuery() == null)
            || !this.query.equals(thatObj.getQuery()) ) {
            return false;
        }
        if( (this.inputFormat == null && thatObj.getInputFormat() != null)
            || (this.inputFormat != null && thatObj.getInputFormat() == null)
            || !this.inputFormat.equals(thatObj.getInputFormat()) ) {
            return false;
        }
        if( (this.inputFile == null && thatObj.getInputFile() != null)
            || (this.inputFile != null && thatObj.getInputFile() == null)
            || !this.inputFile.equals(thatObj.getInputFile()) ) {
            return false;
        }
        if( (this.inputContent == null && thatObj.getInputContent() != null)
            || (this.inputContent != null && thatObj.getInputContent() == null)
            || !this.inputContent.equals(thatObj.getInputContent()) ) {
            return false;
        }
        if( (this.targetOutputFormat == null && thatObj.getTargetOutputFormat() != null)
            || (this.targetOutputFormat != null && thatObj.getTargetOutputFormat() == null)
            || !this.targetOutputFormat.equals(thatObj.getTargetOutputFormat()) ) {
            return false;
        }
        if( (this.outputFormat == null && thatObj.getOutputFormat() != null)
            || (this.outputFormat != null && thatObj.getOutputFormat() == null)
            || !this.outputFormat.equals(thatObj.getOutputFormat()) ) {
            return false;
        }
        if( (this.outputFile == null && thatObj.getOutputFile() != null)
            || (this.outputFile != null && thatObj.getOutputFile() == null)
            || !this.outputFile.equals(thatObj.getOutputFile()) ) {
            return false;
        }
        if( (this.outputContent == null && thatObj.getOutputContent() != null)
            || (this.outputContent != null && thatObj.getOutputContent() == null)
            || !this.outputContent.equals(thatObj.getOutputContent()) ) {
            return false;
        }
        if( (this.responseCode == null && thatObj.getResponseCode() != null)
            || (this.responseCode != null && thatObj.getResponseCode() == null)
            || !this.responseCode.equals(thatObj.getResponseCode()) ) {
            return false;
        }
        if( (this.result == null && thatObj.getResult() != null)
            || (this.result != null && thatObj.getResult() == null)
            || !this.result.equals(thatObj.getResult()) ) {
            return false;
        }
        if( (this.referrerInfo == null && thatObj.getReferrerInfo() != null)
            || (this.referrerInfo != null && thatObj.getReferrerInfo() == null)
            || !this.referrerInfo.equals(thatObj.getReferrerInfo()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.extra == null && thatObj.getExtra() != null)
            || (this.extra != null && thatObj.getExtra() == null)
            || !this.extra.equals(thatObj.getExtra()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.scheduledTime == null && thatObj.getScheduledTime() != null)
            || (this.scheduledTime != null && thatObj.getScheduledTime() == null)
            || !this.scheduledTime.equals(thatObj.getScheduledTime()) ) {
            return false;
        }
        if( (this.processedTime == null && thatObj.getProcessedTime() != null)
            || (this.processedTime != null && thatObj.getProcessedTime() == null)
            || !this.processedTime.equals(thatObj.getProcessedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = querySession == null ? 0 : querySession.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryId == null ? 0 : queryId.hashCode();
        _hash = 31 * _hash + delta;
        delta = dataService == null ? 0 : dataService.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = delayed == null ? 0 : delayed.hashCode();
        _hash = 31 * _hash + delta;
        delta = query == null ? 0 : query.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputFormat == null ? 0 : inputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputFile == null ? 0 : inputFile.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputContent == null ? 0 : inputContent.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetOutputFormat == null ? 0 : targetOutputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFormat == null ? 0 : outputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFile == null ? 0 : outputFile.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputContent == null ? 0 : outputContent.hashCode();
        _hash = 31 * _hash + delta;
        delta = responseCode == null ? 0 : responseCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = result == null ? 0 : result.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = extra == null ? 0 : extra.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = scheduledTime == null ? 0 : scheduledTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = processedTime == null ? 0 : processedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
