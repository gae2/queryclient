package com.queryclient.ws.cert.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.cert.PublicCertificateInfo;


@PersistenceCapable(detachable="true")
public class PublicCertificateInfoDataObject implements PublicCertificateInfo
{
    private static final Logger log = Logger.getLogger(PublicCertificateInfoDataObject.class.getName());

    public static Key composeKey(String guid)
    {
        Key key = KeyFactory.createKey(PublicCertificateInfoDataObject.class.getSimpleName(), guid);
        return key; 
    }

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;

    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private Long createdTime;

    @Persistent(defaultFetchGroup = "true")
    private Long modifiedTime;

    @Persistent(defaultFetchGroup = "true")
    private String appId;

    @Persistent(defaultFetchGroup = "true")
    private String appUrl;

    @Persistent(defaultFetchGroup = "true")
    private String certName;

    @Persistent(defaultFetchGroup = "true")
    private List<String> certInPemFormat;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    public PublicCertificateInfoDataObject()
    {
        this(null);
    }
    public PublicCertificateInfoDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public PublicCertificateInfoDataObject(String guid, String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime)
    {
        this(guid, appId, appUrl, certName, certInPemFormat, note, status, expirationTime, null, null);
    }
    public PublicCertificateInfoDataObject(String guid, String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        generatePK(guid);
    	this.createdTime = createdTime;
    	this.modifiedTime = modifiedTime;

        this.appId = appId;
        this.appUrl = appUrl;
        this.certName = certName;
        this.setCertInPemFormat(certInPemFormat);
        this.note = note;
        this.status = status;
        this.expirationTime = expirationTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

    protected Key createKey()
    {
        return PublicCertificateInfoDataObject.composeKey(getGuid());
    }

    public Key getKey() 
    {
        return this.key;
    }
    private void resetKey(Key key)
    {
        this.key = key;
    }
    private void setKey(Key key)
    {
        // Key can be set only one????
        if(this.key == null) {
            resetKey(key);
        } else {
            log.info("Key is already set. New key will be ignored.");
        }
    }

    protected void rebuildKey()
    {
        resetKey(createKey());
    }
    protected void buildKey()
    {
        setKey(createKey());
    }

    public Long getCreatedTime() 
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime() 
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime) 
    {
        this.modifiedTime = modifiedTime;
    }

    public String getAppId()
    {
        return this.appId;
    }
    public void setAppId(String appId)
    {
        this.appId = appId;
    }

    public String getAppUrl()
    {
        return this.appUrl;
    }
    public void setAppUrl(String appUrl)
    {
        this.appUrl = appUrl;
    }

    public String getCertName()
    {
        return this.certName;
    }
    public void setCertName(String certName)
    {
        this.certName = certName;
    }

    public String getCertInPemFormat()
    {
        if(this.certInPemFormat == null) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for(String v : this.certInPemFormat) {
            sb.append(v);
        }
        return sb.toString(); 
    }
    public void setCertInPemFormat(String certInPemFormat)
    {
        if(certInPemFormat == null) {
            this.certInPemFormat = null;
        } else {
            List<String> _list = new ArrayList<String>();
            int _beg = 0;
            int _rem = certInPemFormat.length();
            while(_rem > 0) {
                int _sz;
                if((_rem - 500) >= 0) {
                    _sz = 500;
                    _rem -= 500;
                } else {
                    _sz = _rem;
                    _rem = 0;
                }
                _list.add(certInPemFormat.substring(_beg, _beg+_sz));
                _beg += _sz;
            }
            this.certInPemFormat = _list;
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("key", this.key);
        dataMap.put("guid", this.guid);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);
        dataMap.put("appId", this.appId);
        dataMap.put("appUrl", this.appUrl);
        dataMap.put("certName", this.certName);
        dataMap.put("certInPemFormat", this.certInPemFormat);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);
        dataMap.put("expirationTime", this.expirationTime);

        return dataMap;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        PublicCertificateInfo thatObj = (PublicCertificateInfo) obj;
//        if( (this.key == null && thatObj.getKey() != null)
//            || (this.key != null && thatObj.getKey() == null)
//            || !this.key.equals(thatObj.getKey()) ) {
//            return false;
//        }
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.appId == null && thatObj.getAppId() != null)
            || (this.appId != null && thatObj.getAppId() == null)
            || !this.appId.equals(thatObj.getAppId()) ) {
            return false;
        }
        if( (this.appUrl == null && thatObj.getAppUrl() != null)
            || (this.appUrl != null && thatObj.getAppUrl() == null)
            || !this.appUrl.equals(thatObj.getAppUrl()) ) {
            return false;
        }
        if( (this.certName == null && thatObj.getCertName() != null)
            || (this.certName != null && thatObj.getCertName() == null)
            || !this.certName.equals(thatObj.getCertName()) ) {
            return false;
        }
        if( (this.certInPemFormat == null && thatObj.getCertInPemFormat() != null)
            || (this.certInPemFormat != null && thatObj.getCertInPemFormat() == null)
            || !this.certInPemFormat.equals(thatObj.getCertInPemFormat()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
//        if( (this.createdTime == null && thatObj.getCreatedTime() != null)
//            || (this.createdTime != null && thatObj.getCreatedTime() == null)
//            || !this.createdTime.equals(thatObj.getCreatedTime()) ) {
//            return false;
//        }
//        if( (this.modifiedTime == null && thatObj.getModifiedTime() != null)
//            || (this.modifiedTime != null && thatObj.getModifiedTime() == null)
//            || !this.modifiedTime.equals(thatObj.getModifiedTime()) ) {
//            return false;
//        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        hash = 31 * hash + delta;
        delta = appId == null ? 0 : appId.hashCode();
        hash = 31 * hash + delta;
        delta = appUrl == null ? 0 : appUrl.hashCode();
        hash = 31 * hash + delta;
        delta = certName == null ? 0 : certName.hashCode();
        hash = 31 * hash + delta;
        delta = certInPemFormat == null ? 0 : certInPemFormat.hashCode();
        hash = 31 * hash + delta;
        delta = note == null ? 0 : note.hashCode();
        hash = 31 * hash + delta;
        delta = status == null ? 0 : status.hashCode();
        hash = 31 * hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        hash = 31 * hash + delta;
        return hash;
    }

}
