package com.queryclient.ws;



public interface DataService 
{
    String  getGuid();
    String  getUser();
    String  getName();
    String  getDescription();
    String  getType();
    String  getMode();
    String  getServiceUrl();
    Boolean  isAuthRequired();
    ConsumerKeySecretPair  getAuthCredential();
    ReferrerInfoStruct  getReferrerInfo();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
