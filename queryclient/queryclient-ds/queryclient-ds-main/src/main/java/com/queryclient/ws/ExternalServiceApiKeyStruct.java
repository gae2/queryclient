package com.queryclient.ws;



public interface ExternalServiceApiKeyStruct 
{
    String  getUuid();
    String  getService();
    String  getKey();
    String  getSecret();
    String  getNote();
    boolean isEmpty();
}
