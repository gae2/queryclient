package com.queryclient.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.bean.UserPasswordBean;
import com.queryclient.ws.stub.UserPasswordListStub;
import com.queryclient.ws.stub.UserPasswordStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.UserPasswordResource;
import com.queryclient.ws.resource.util.GaeAppStructResourceUtil;

// MockUserPasswordResource is a decorator.
// It can be used as a base class to mock UserPasswordResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/userPasswords/")
public abstract class MockUserPasswordResource implements UserPasswordResource
{
    private static final Logger log = Logger.getLogger(MockUserPasswordResource.class.getName());

    // MockUserPasswordResource uses the decorator design pattern.
    private UserPasswordResource decoratedResource;

    public MockUserPasswordResource(UserPasswordResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected UserPasswordResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(UserPasswordResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUserPasswords(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getUserPasswordKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getUserPasswordKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getUserPassword(String guid) throws BaseResourceException
    {
        return decoratedResource.getUserPassword(guid);
    }

    @Override
    public Response getUserPassword(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getUserPassword(guid, field);
    }

    @Override
    public Response createUserPassword(UserPasswordStub userPassword) throws BaseResourceException
    {
        return decoratedResource.createUserPassword(userPassword);
    }

    @Override
    public Response updateUserPassword(String guid, UserPasswordStub userPassword) throws BaseResourceException
    {
        return decoratedResource.updateUserPassword(guid, userPassword);
    }

    @Override
    public Response updateUserPassword(String guid, String managerApp, Long appAcl, String gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseResourceException
    {
        return decoratedResource.updateUserPassword(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

    @Override
    public Response deleteUserPassword(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteUserPassword(guid);
    }

    @Override
    public Response deleteUserPasswords(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteUserPasswords(filter, params, values);
    }


}
