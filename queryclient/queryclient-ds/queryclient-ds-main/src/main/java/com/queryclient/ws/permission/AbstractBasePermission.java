package com.queryclient.ws.permission;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;


public abstract class AbstractBasePermission implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AbstractBasePermission.class.getName());

    public static final String ACTION_CREATE = "create";
    public static final String ACTION_READ = "read";
    public static final String ACTION_UPDATE = "update";
    public static final String ACTION_DELETE = "delete";

    private List<String> mActionList = null;
    private List<String> mPermissibleActionList = null;
    private List<String> mPermissionRequiredActionList = null;

    // Place-holder
    public AbstractBasePermission()
    {
    }


    ///////////////////////////////////////////////////////////
    // To be implemented/overridden by subclasses...


    // Permission name format: "object_name:<optional_instance_guid>:action_name<:optional_field_name>"
    public abstract String getPermissionName(String action);

    public String getCreatePermissionName()
    {
        return getPermissionName(ACTION_CREATE);
    }
    public String getReadPermissionName()
    {
        return getPermissionName(ACTION_READ);
    }
    public String getUpdatePermissionName()
    {
        return getPermissionName(ACTION_UPDATE);
    }
    public String getDeletePermissionName()
    {
        return getPermissionName(ACTION_DELETE);
    }

    public boolean isCreatePermissionRequired()
    {
        return true;
    }
    public boolean isReadPermissionRequired()
    {
        return true;
    }
    public boolean isUpdatePermissionRequired()
    {
        return true;
    }
    public boolean isDeletePermissionRequired()
    {
        return true;
    }

    public boolean isPermissionRequired(String action)
    {
        if(ACTION_CREATE.equals(action)) {
            return isCreatePermissionRequired();
        } else if(ACTION_READ.equals(action)) {
            return isReadPermissionRequired();
        } else if(ACTION_UPDATE.equals(action)) {
            return isUpdatePermissionRequired();
        } else if(ACTION_DELETE.equals(action)) {
            return isDeletePermissionRequired();
        } else {
            // ????
            log.warning("Unrecognized action = " + action);
            return true;
        }
    }

    public List<String> getAllActions()
    {
        if(mActionList == null) {
            synchronized(this) {
                mActionList = new ArrayList<String>();
                mActionList.add(ACTION_CREATE);
                mActionList.add(ACTION_READ);
                mActionList.add(ACTION_UPDATE);
                mActionList.add(ACTION_DELETE);
            }       
        }
        return mActionList;
    }

    public List<String> getAllPermissibleActions()
    {
        if(mPermissibleActionList == null) {
            synchronized(this) {
                mPermissibleActionList = new ArrayList<String>();
                if(isCreatePermissionRequired() == false) {
                    mPermissibleActionList.add(ACTION_CREATE);
                }
                if(isReadPermissionRequired() == false) {
                    mPermissibleActionList.add(ACTION_READ);
                }
                if(isUpdatePermissionRequired() == false) {
                    mPermissibleActionList.add(ACTION_UPDATE);
                }
                if(isDeletePermissionRequired() == false) {
                    mPermissibleActionList.add(ACTION_DELETE);
                }
            }       
        }
        return mPermissibleActionList;
    }

    public List<String> getAllPermissionRequiredActions()
    {
        if(mPermissionRequiredActionList == null) {
            synchronized(this) {
                mPermissionRequiredActionList = new ArrayList<String>();
                if(isCreatePermissionRequired()) {
                    mPermissionRequiredActionList.add(ACTION_CREATE);
                }
                if(isReadPermissionRequired()) {
                    mPermissionRequiredActionList.add(ACTION_READ);
                }
                if(isUpdatePermissionRequired()) {
                    mPermissionRequiredActionList.add(ACTION_UPDATE);
                }
                if(isDeletePermissionRequired()) {
                    mPermissionRequiredActionList.add(ACTION_DELETE);
                }
            }       
        }
        return mPermissionRequiredActionList;
    }

}
