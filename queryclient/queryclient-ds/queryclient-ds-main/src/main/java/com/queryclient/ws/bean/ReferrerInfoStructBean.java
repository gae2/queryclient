package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.data.ReferrerInfoStructDataObject;

public class ReferrerInfoStructBean implements ReferrerInfoStruct
{
    private static final Logger log = Logger.getLogger(ReferrerInfoStructBean.class.getName());

    // Embedded data object.
    private ReferrerInfoStructDataObject dobj = null;

    public ReferrerInfoStructBean()
    {
        this(new ReferrerInfoStructDataObject());
    }
    public ReferrerInfoStructBean(ReferrerInfoStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public ReferrerInfoStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getReferer()
    {
        if(getDataObject() != null) {
            return getDataObject().getReferer();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferer(String referer)
    {
        if(getDataObject() != null) {
            getDataObject().setReferer(referer);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
        }
    }

    public String getUserAgent()
    {
        if(getDataObject() != null) {
            return getDataObject().getUserAgent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUserAgent(String userAgent)
    {
        if(getDataObject() != null) {
            getDataObject().setUserAgent(userAgent);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
        }
    }

    public String getLanguage()
    {
        if(getDataObject() != null) {
            return getDataObject().getLanguage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLanguage(String language)
    {
        if(getDataObject() != null) {
            getDataObject().setLanguage(language);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
        }
    }

    public String getHostname()
    {
        if(getDataObject() != null) {
            return getDataObject().getHostname();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHostname(String hostname)
    {
        if(getDataObject() != null) {
            getDataObject().setHostname(hostname);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
        }
    }

    public String getIpAddress()
    {
        if(getDataObject() != null) {
            return getDataObject().getIpAddress();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setIpAddress(String ipAddress)
    {
        if(getDataObject() != null) {
            getDataObject().setIpAddress(ipAddress);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ReferrerInfoStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getReferer() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUserAgent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLanguage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHostname() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getIpAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public ReferrerInfoStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
