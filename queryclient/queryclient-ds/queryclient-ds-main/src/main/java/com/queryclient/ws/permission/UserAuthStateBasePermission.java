package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class UserAuthStateBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserAuthStateBasePermission.class.getName());

    public UserAuthStateBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "UserAuthState::" + action;
    }


}
