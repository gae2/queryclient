package com.queryclient.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ResourceAlreadyPresentException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.DataStoreRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.ws.bean.DataServiceBean;
import com.queryclient.ws.stub.DataServiceListStub;
import com.queryclient.ws.stub.DataServiceStub;
import com.queryclient.ws.resource.ServiceManager;
import com.queryclient.ws.resource.DataServiceResource;
import com.queryclient.ws.resource.util.ConsumerKeySecretPairResourceUtil;
import com.queryclient.ws.resource.util.ReferrerInfoStructResourceUtil;

// MockDataServiceResource is a decorator.
// It can be used as a base class to mock DataServiceResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/dataServices/")
public abstract class MockDataServiceResource implements DataServiceResource
{
    private static final Logger log = Logger.getLogger(MockDataServiceResource.class.getName());

    // MockDataServiceResource uses the decorator design pattern.
    private DataServiceResource decoratedResource;

    public MockDataServiceResource(DataServiceResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected DataServiceResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(DataServiceResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDataServices(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDataServiceKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getDataServiceKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getDataServiceKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getDataService(String guid) throws BaseResourceException
    {
        return decoratedResource.getDataService(guid);
    }

    @Override
    public Response getDataService(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getDataService(guid, field);
    }

    @Override
    public Response createDataService(DataServiceStub dataService) throws BaseResourceException
    {
        return decoratedResource.createDataService(dataService);
    }

    @Override
    public Response updateDataService(String guid, DataServiceStub dataService) throws BaseResourceException
    {
        return decoratedResource.updateDataService(guid, dataService);
    }

    @Override
    public Response updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, String authCredential, String referrerInfo, String status) throws BaseResourceException
    {
        return decoratedResource.updateDataService(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status);
    }

    @Override
    public Response deleteDataService(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteDataService(guid);
    }

    @Override
    public Response deleteDataServices(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteDataServices(filter, params, values);
    }


}
