package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "gaeUserStructs")
@XmlType(propOrder = {"gaeUserStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class GaeUserStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GaeUserStructListStub.class.getName());

    private List<GaeUserStructStub> gaeUserStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public GaeUserStructListStub()
    {
        this(new ArrayList<GaeUserStructStub>());
    }
    public GaeUserStructListStub(List<GaeUserStructStub> gaeUserStructs)
    {
        this(gaeUserStructs, null);
    }
    public GaeUserStructListStub(List<GaeUserStructStub> gaeUserStructs, String forwardCursor)
    {
        this.gaeUserStructs = gaeUserStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(gaeUserStructs == null) {
            return true;
        } else {
            return gaeUserStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(gaeUserStructs == null) {
            return 0;
        } else {
            return gaeUserStructs.size();
        }
    }


    @XmlElement(name = "gaeUserStruct")
    public List<GaeUserStructStub> getGaeUserStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<GaeUserStructStub> getList()
    {
        return gaeUserStructs;
    }
    public void setList(List<GaeUserStructStub> gaeUserStructs)
    {
        this.gaeUserStructs = gaeUserStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<GaeUserStructStub> it = this.gaeUserStructs.iterator();
        while(it.hasNext()) {
            GaeUserStructStub gaeUserStruct = it.next();
            sb.append(gaeUserStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static GaeUserStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of GaeUserStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write GaeUserStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write GaeUserStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write GaeUserStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static GaeUserStructListStub fromJsonString(String jsonStr)
    {
        try {
            GaeUserStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, GaeUserStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeUserStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeUserStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into GaeUserStructListStub object.", e);
        }
        
        return null;
    }

}
