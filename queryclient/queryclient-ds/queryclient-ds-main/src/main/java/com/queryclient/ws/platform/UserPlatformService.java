package com.queryclient.ws.platform;

import java.util.Set;
import java.util.List;


// Platform service.
public abstract class UserPlatformService
{
    // ...
    public abstract Patron getCurrentPatron();
    //public abstract boolean setDefaultAuthMode(String authMode);
    public abstract Patron authenticate();
    public abstract Patron authenticateThroughApp();
    public abstract Patron authenticateThroughGoogle();
    public abstract Patron authenticateThroughOpenID();
    public abstract Patron authenticateThroughOpenID(String federatedIdentity, Set<String> attributesRequest);
    //public abstract Patron authenticateThroughGoopleApp(String appDomain);
    // ...

    // Modeled after GAE UserService.
    public abstract String createLoginURL(String authMode, String destinationURL);
    public abstract String createLoginURL(String authMode, String destinationURL, String authDomain);
    public abstract String createLoginURL(String authMode, String destinationURL, String authDomain, String federatedIdentity, Set<String> attributesRequest);
    public abstract String createLogoutURL(String destinationURL);
    public abstract String createLogoutURL(String destinationURL, String authDomain);
    // ...

    // Returns true if the current user is authenticated...
    public abstract boolean isPatronAuthenticated();

    // Returns the list of roles associated with a current (authenticated) user.
    public abstract List<String> getPatronRoles();

    // Returns true if the current patron is "admin".
    // Amdin is a (predefined) special role (for GAE compatibility).
    public abstract boolean isPatronAdmin();

    // ...

}
