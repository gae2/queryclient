package com.queryclient.ws.cert.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.cert.PublicCertificateInfo;
import com.queryclient.ws.cert.dao.PublicCertificateInfoDAO;
import com.queryclient.ws.cert.dao.DefaultPublicCertificateInfoDAO;
import com.queryclient.ws.cert.bean.PublicCertificateInfoBean;
import com.queryclient.ws.cert.data.PublicCertificateInfoDataObject;
import com.queryclient.ws.cert.service.PublicCertificateInfoService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class PublicCertificateInfoServiceImpl implements PublicCertificateInfoService
{
    private static final Logger log = Logger.getLogger(PublicCertificateInfoServiceImpl.class.getName());

    private PublicCertificateInfoDAO dao = null;

    private PublicCertificateInfoDAO getPublicCertificateInfoDAO()
    {
        if(dao == null) {
            dao = new DefaultPublicCertificateInfoDAO();
        }
        return dao;
    }
    
    //////////////////////////////////////////////////////////////////////////
    // PublicCertificateInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public PublicCertificateInfo getPublicCertificateInfo(String guid) throws BaseException
    {
        PublicCertificateInfoDataObject dataObj = getPublicCertificateInfoDAO().getPublicCertificateInfo(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve PublicCertificateInfoDataObject for guid = " + guid);
            return null;  // ????
        }
        PublicCertificateInfoBean bean = new PublicCertificateInfoBean(dataObj);
        return bean;
    }

    @Override
    public Object getPublicCertificateInfo(String guid, String field) throws BaseException
    {
        PublicCertificateInfoDataObject dataObj = getPublicCertificateInfoDAO().getPublicCertificateInfo(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve PublicCertificateInfoDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appId")) {
            return dataObj.getAppId();
        } else if(field.equals("appUrl")) {
            return dataObj.getAppUrl();
        } else if(field.equals("certName")) {
            return dataObj.getCertName();
        } else if(field.equals("certInPemFormat")) {
            return dataObj.getCertInPemFormat();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }
    
    @Override
    public List<PublicCertificateInfo> getAllPublicCertificateInfos() throws BaseException
    {
        return getAllPublicCertificateInfos(null, null, null);
    }

    @Override
    public List<PublicCertificateInfo> getAllPublicCertificateInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        // TBD: Is there a better way????
        List<PublicCertificateInfo> list = new ArrayList<PublicCertificateInfo>();
        List<PublicCertificateInfoDataObject> dataObjs = getPublicCertificateInfoDAO().getAllPublicCertificateInfos(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve PublicCertificateInfoDataObject list.");
        } else {
            Iterator<PublicCertificateInfoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                PublicCertificateInfoDataObject dataObj = (PublicCertificateInfoDataObject) it.next();
                list.add(new PublicCertificateInfoBean(dataObj));
            }
        }
        return list;
    }

    @Override
    public List<PublicCertificateInfo> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findPublicCertificateInfos(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<PublicCertificateInfo> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("PublicCertificateInfoServiceImpl.findPublicCertificateInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<PublicCertificateInfo> list = new ArrayList<PublicCertificateInfo>();
        List<PublicCertificateInfoDataObject> dataObjs = getPublicCertificateInfoDAO().findPublicCertificateInfos(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find publicCertificateInfos for the given criterion.");
        } else {
            Iterator<PublicCertificateInfoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                PublicCertificateInfoDataObject dataObj = (PublicCertificateInfoDataObject) it.next();
                list.add(new PublicCertificateInfoBean(dataObj));
            }
        }
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("PublicCertificateInfoServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = getPublicCertificateInfoDAO().getCount(filter, params, values, aggregate);
        return count;
    }

    @Override
    public String createPublicCertificateInfo(String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        PublicCertificateInfoDataObject dataObj = new PublicCertificateInfoDataObject(null, appId, appUrl, certName, certInPemFormat, note, status, expirationTime);
        return createPublicCertificateInfo(dataObj);
    }

    @Override
    public String createPublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        // Param publicCertificateInfo cannot be null.....
        if(publicCertificateInfo == null) {
            log.log(Level.INFO, "Param publicCertificateInfo is null!");
            throw new BadRequestException("Param publicCertificateInfo object is null!");
        }
        PublicCertificateInfoDataObject dataObj = null;
        if(publicCertificateInfo instanceof PublicCertificateInfoDataObject) {
            dataObj = (PublicCertificateInfoDataObject) publicCertificateInfo;
        } else if(publicCertificateInfo instanceof PublicCertificateInfoBean) {
            dataObj = ((PublicCertificateInfoBean) publicCertificateInfo).toDataObject();
        } else {  // if(publicCertificateInfo instanceof PublicCertificateInfo)
            //dataObj = new PublicCertificateInfoDataObject(null, publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new PublicCertificateInfoDataObject(publicCertificateInfo.getGuid(), publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
        }
        String guid = getPublicCertificateInfoDAO().createPublicCertificateInfo(dataObj);
        return guid;
    }

    @Override
    public Boolean updatePublicCertificateInfo(String guid, String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        PublicCertificateInfoDataObject dataObj = new PublicCertificateInfoDataObject(guid, appId, appUrl, certName, certInPemFormat, note, status, expirationTime);
        return updatePublicCertificateInfo(dataObj);
    }
        
    // ???
    @Override
    public Boolean updatePublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        // Param publicCertificateInfo cannot be null.....
        if(publicCertificateInfo == null || publicCertificateInfo.getGuid() == null) {
            log.log(Level.INFO, "Param publicCertificateInfo or its guid is null!");
            throw new BadRequestException("Param publicCertificateInfo object or its guid is null!");
        }
        PublicCertificateInfoDataObject dataObj = null;
        if(publicCertificateInfo instanceof PublicCertificateInfoDataObject) {
            dataObj = (PublicCertificateInfoDataObject) publicCertificateInfo;
        } else if(publicCertificateInfo instanceof PublicCertificateInfoBean) {
            dataObj = ((PublicCertificateInfoBean) publicCertificateInfo).toDataObject();
        } else {  // if(publicCertificateInfo instanceof PublicCertificateInfo)
            dataObj = new PublicCertificateInfoDataObject(publicCertificateInfo.getGuid(), publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
        }
        Boolean suc = getPublicCertificateInfoDAO().updatePublicCertificateInfo(dataObj);
        return suc;
    }
    
    @Override
    public Boolean deletePublicCertificateInfo(String guid) throws BaseException
    {
        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getPublicCertificateInfoDAO().deletePublicCertificateInfo(guid);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deletePublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        // Param publicCertificateInfo cannot be null.....
        if(publicCertificateInfo == null || publicCertificateInfo.getGuid() == null) {
            log.log(Level.INFO, "Param publicCertificateInfo or its guid is null!");
            throw new BadRequestException("Param publicCertificateInfo object or its guid is null!");
        }
        PublicCertificateInfoDataObject dataObj = null;
        if(publicCertificateInfo instanceof PublicCertificateInfoDataObject) {
            dataObj = (PublicCertificateInfoDataObject) publicCertificateInfo;
        } else if(publicCertificateInfo instanceof PublicCertificateInfoBean) {
            dataObj = ((PublicCertificateInfoBean) publicCertificateInfo).toDataObject();
        } else {  // if(publicCertificateInfo instanceof PublicCertificateInfo)
            dataObj = new PublicCertificateInfoDataObject(publicCertificateInfo.getGuid(), publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
        }
        Boolean suc = getPublicCertificateInfoDAO().deletePublicCertificateInfo(dataObj);
        return suc;
    }

    @Override
    public Long deletePublicCertificateInfos(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getPublicCertificateInfoDAO().deletePublicCertificateInfos(filter, params, values);
        return count;
    }

}
