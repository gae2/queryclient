package com.queryclient.ws.service;

import java.util.List;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface QueryRecordService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    QueryRecord getQueryRecord(String guid) throws BaseException;
    Object getQueryRecord(String guid, String field) throws BaseException;
    List<QueryRecord> getQueryRecords(List<String> guids) throws BaseException;
    List<QueryRecord> getAllQueryRecords() throws BaseException;
    /* @Deprecated */ List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException;
    List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createQueryRecord(String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException;
    //String createQueryRecord(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return QueryRecord?)
    String createQueryRecord(QueryRecord queryRecord) throws BaseException;          // Returns Guid.  (Return QueryRecord?)
    Boolean updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException;
    //Boolean updateQueryRecord(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateQueryRecord(QueryRecord queryRecord) throws BaseException;
    Boolean deleteQueryRecord(String guid) throws BaseException;
    Boolean deleteQueryRecord(QueryRecord queryRecord) throws BaseException;
    Long deleteQueryRecords(String filter, String params, List<String> values) throws BaseException;

//    Integer createQueryRecords(List<QueryRecord> queryRecords) throws BaseException;
//    Boolean updateeQueryRecords(List<QueryRecord> queryRecords) throws BaseException;

}
