package com.queryclient.ws.dao;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.data.DummyEntityDataObject;


// TBD: Add offset/count to getAllDummyEntities() and findDummyEntities(), etc.
public interface DummyEntityDAO
{
    DummyEntityDataObject getDummyEntity(String guid) throws BaseException;
    List<DummyEntityDataObject> getDummyEntities(List<String> guids) throws BaseException;
    List<DummyEntityDataObject> getAllDummyEntities() throws BaseException;
    /* @Deprecated */ List<DummyEntityDataObject> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException;
    List<DummyEntityDataObject> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<DummyEntityDataObject> findDummyEntities(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<DummyEntityDataObject> findDummyEntities(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<DummyEntityDataObject> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<DummyEntityDataObject> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createDummyEntity(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return DummyEntityDataObject?)
    String createDummyEntity(DummyEntityDataObject dummyEntity) throws BaseException;          // Returns Guid.  (Return DummyEntityDataObject?)
    //Boolean updateDummyEntity(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateDummyEntity(DummyEntityDataObject dummyEntity) throws BaseException;
    Boolean deleteDummyEntity(String guid) throws BaseException;
    Boolean deleteDummyEntity(DummyEntityDataObject dummyEntity) throws BaseException;
    Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException;
}
