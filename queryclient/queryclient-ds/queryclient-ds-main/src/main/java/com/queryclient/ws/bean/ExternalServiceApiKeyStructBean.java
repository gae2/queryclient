package com.queryclient.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ExternalServiceApiKeyStruct;
import com.queryclient.ws.data.ExternalServiceApiKeyStructDataObject;

public class ExternalServiceApiKeyStructBean implements ExternalServiceApiKeyStruct
{
    private static final Logger log = Logger.getLogger(ExternalServiceApiKeyStructBean.class.getName());

    // Embedded data object.
    private ExternalServiceApiKeyStructDataObject dobj = null;

    public ExternalServiceApiKeyStructBean()
    {
        this(new ExternalServiceApiKeyStructDataObject());
    }
    public ExternalServiceApiKeyStructBean(ExternalServiceApiKeyStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public ExternalServiceApiKeyStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
        }
    }

    public String getService()
    {
        if(getDataObject() != null) {
            return getDataObject().getService();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setService(String service)
    {
        if(getDataObject() != null) {
            getDataObject().setService(service);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
        }
    }

    public String getKey()
    {
        if(getDataObject() != null) {
            return getDataObject().getKey();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setKey(String key)
    {
        if(getDataObject() != null) {
            getDataObject().setKey(key);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
        }
    }

    public String getSecret()
    {
        if(getDataObject() != null) {
            return getDataObject().getSecret();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSecret(String secret)
    {
        if(getDataObject() != null) {
            getDataObject().setSecret(secret);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalServiceApiKeyStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getService() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecret() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public ExternalServiceApiKeyStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
