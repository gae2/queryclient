package com.queryclient.ws.core;

import java.util.LinkedHashMap;
import java.util.Map;


public class CacheMap<K,V> extends LinkedHashMap<K, V>
{
    private static final long serialVersionUID = 1L;
    private static final int DEFAULT_MAX_SIZE = Integer.MAX_VALUE;  // ???

    // Maximum capacity
    private int mMaxSize = -1;

    
    // TBD:
    // Modify ctors to accept maxSize arg???
    // Also, use maxSize to optimize initialCapacity and loadFactor???
    // ...
    
    public CacheMap()
    {
        initialize();
    }

    public CacheMap(int initialCapacity)
    {
        super(initialCapacity);
        initialize();
    }

    public CacheMap(Map<K,V> m)
    {
        super(m);
        if(m instanceof CacheMap<?,?>) {
            mMaxSize = ((CacheMap<K,V>) m).getMaxSize();
        }
        initialize();
    }

    public CacheMap(int initialCapacity, float loadFactor)
    {
        super(initialCapacity, loadFactor);
        initialize();
    }

    public CacheMap(int initialCapacity, float loadFactor, boolean accessOrder)
    {
        super(initialCapacity, loadFactor, accessOrder);
        initialize();
    }
    
    private void initialize()
    {
        if(mMaxSize <= 0) {
            mMaxSize = DEFAULT_MAX_SIZE;  // ????
        }
    }

    public int getMaxSize()
    {
        return mMaxSize;
    }

    public void setMaxSize(int maxSize)
    {
        if(maxSize <= 0) {
            maxSize = DEFAULT_MAX_SIZE;  // ????
        }
        this.mMaxSize = maxSize;
    }

    @Override
    protected boolean removeEldestEntry(java.util.Map.Entry<K, V> eldest)
    {
        //return super.removeEldestEntry(eldest);
        if(size() >= mMaxSize) {
            return true;
        } else {
            return false;
        }
    }

    
    
}
