package com.queryclient.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.bean.DummyEntityBean;
import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.data.DummyEntityDataObject;
import com.queryclient.ws.service.DAOFactoryManager;
import com.queryclient.ws.service.DummyEntityService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DummyEntityServiceImpl implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(DummyEntityServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // DummyEntity related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DummyEntity getDummyEntity(String guid) throws BaseException
    {
        log.finer("BEGIN");

        DummyEntityDataObject dataObj = getDAOFactory().getDummyEntityDAO().getDummyEntity(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DummyEntityDataObject for guid = " + guid);
            return null;  // ????
        }
        DummyEntityBean bean = new DummyEntityBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getDummyEntity(String guid, String field) throws BaseException
    {
        DummyEntityDataObject dataObj = getDAOFactory().getDummyEntityDAO().getDummyEntity(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DummyEntityDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("name")) {
            return dataObj.getName();
        } else if(field.equals("content")) {
            return dataObj.getContent();
        } else if(field.equals("maxLength")) {
            return dataObj.getMaxLength();
        } else if(field.equals("expired")) {
            return dataObj.isExpired();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<DummyEntity> list = new ArrayList<DummyEntity>();
        List<DummyEntityDataObject> dataObjs = getDAOFactory().getDummyEntityDAO().getDummyEntities(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve DummyEntityDataObject list.");
        } else {
            Iterator<DummyEntityDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DummyEntityDataObject dataObj = (DummyEntityDataObject) it.next();
                list.add(new DummyEntityBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<DummyEntity> getAllDummyEntities() throws BaseException
    {
        return getAllDummyEntities(null, null, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDummyEntities(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DummyEntity> list = new ArrayList<DummyEntity>();
        List<DummyEntityDataObject> dataObjs = getDAOFactory().getDummyEntityDAO().getAllDummyEntities(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve DummyEntityDataObject list.");
        } else {
            Iterator<DummyEntityDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DummyEntityDataObject dataObj = (DummyEntityDataObject) it.next();
                list.add(new DummyEntityBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllDummyEntityKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getDummyEntityDAO().getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve DummyEntity key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DummyEntityServiceImpl.findDummyEntities(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DummyEntity> list = new ArrayList<DummyEntity>();
        List<DummyEntityDataObject> dataObjs = getDAOFactory().getDummyEntityDAO().findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find dummyEntities for the given criterion.");
        } else {
            Iterator<DummyEntityDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DummyEntityDataObject dataObj = (DummyEntityDataObject) it.next();
                list.add(new DummyEntityBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DummyEntityServiceImpl.findDummyEntityKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getDummyEntityDAO().findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find DummyEntity keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DummyEntityServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getDummyEntityDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        DummyEntityDataObject dataObj = new DummyEntityDataObject(null, user, name, content, maxLength, expired, status);
        return createDummyEntity(dataObj);
    }

    @Override
    public String createDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");

        // Param dummyEntity cannot be null.....
        if(dummyEntity == null) {
            log.log(Level.INFO, "Param dummyEntity is null!");
            throw new BadRequestException("Param dummyEntity object is null!");
        }
        DummyEntityDataObject dataObj = null;
        if(dummyEntity instanceof DummyEntityDataObject) {
            dataObj = (DummyEntityDataObject) dummyEntity;
        } else if(dummyEntity instanceof DummyEntityBean) {
            dataObj = ((DummyEntityBean) dummyEntity).toDataObject();
        } else {  // if(dummyEntity instanceof DummyEntity)
            //dataObj = new DummyEntityDataObject(null, dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new DummyEntityDataObject(dummyEntity.getGuid(), dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
        }
        String guid = getDAOFactory().getDummyEntityDAO().createDummyEntity(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        DummyEntityDataObject dataObj = new DummyEntityDataObject(guid, user, name, content, maxLength, expired, status);
        return updateDummyEntity(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");

        // Param dummyEntity cannot be null.....
        if(dummyEntity == null || dummyEntity.getGuid() == null) {
            log.log(Level.INFO, "Param dummyEntity or its guid is null!");
            throw new BadRequestException("Param dummyEntity object or its guid is null!");
        }
        DummyEntityDataObject dataObj = null;
        if(dummyEntity instanceof DummyEntityDataObject) {
            dataObj = (DummyEntityDataObject) dummyEntity;
        } else if(dummyEntity instanceof DummyEntityBean) {
            dataObj = ((DummyEntityBean) dummyEntity).toDataObject();
        } else {  // if(dummyEntity instanceof DummyEntity)
            dataObj = new DummyEntityDataObject(dummyEntity.getGuid(), dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
        }
        Boolean suc = getDAOFactory().getDummyEntityDAO().updateDummyEntity(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getDummyEntityDAO().deleteDummyEntity(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");

        // Param dummyEntity cannot be null.....
        if(dummyEntity == null || dummyEntity.getGuid() == null) {
            log.log(Level.INFO, "Param dummyEntity or its guid is null!");
            throw new BadRequestException("Param dummyEntity object or its guid is null!");
        }
        DummyEntityDataObject dataObj = null;
        if(dummyEntity instanceof DummyEntityDataObject) {
            dataObj = (DummyEntityDataObject) dummyEntity;
        } else if(dummyEntity instanceof DummyEntityBean) {
            dataObj = ((DummyEntityBean) dummyEntity).toDataObject();
        } else {  // if(dummyEntity instanceof DummyEntity)
            dataObj = new DummyEntityDataObject(dummyEntity.getGuid(), dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
        }
        Boolean suc = getDAOFactory().getDummyEntityDAO().deleteDummyEntity(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getDummyEntityDAO().deleteDummyEntities(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
