package com.queryclient.ws.service;

import java.util.logging.Logger;

import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.dao.base.DefaultDAOFactory;

// We use Abstract Factory pattern.
// This "manager" class provides a way to choose a concrete factory.
public final class DAOFactoryManager
{
    private static final Logger log = Logger.getLogger(DAOFactoryManager.class.getName());

    // Prevents instantiation.
    private DAOFactoryManager() {}

    // Returns a DAO factory.
	public static DAOFactory getDAOFactory() 
    {
        // For now, hard-coded.
	    return DefaultDAOFactory.getInstance();
    }

}
