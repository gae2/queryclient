package com.queryclient.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class DataServiceBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DataServiceBasePermission.class.getName());

    public DataServiceBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "DataService::" + action;
    }


}
