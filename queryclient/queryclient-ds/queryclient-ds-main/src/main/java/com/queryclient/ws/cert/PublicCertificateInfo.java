package com.queryclient.ws.cert;


public interface PublicCertificateInfo 
{
    String  getGuid();
    String  getAppId();
    String  getAppUrl();
    String  getCertName();
    String  getCertInPemFormat();
    String  getNote();
    String  getStatus();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
