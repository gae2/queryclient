package com.queryclient.ws.dao.mock;

import java.util.logging.Logger;

import com.queryclient.ws.dao.DAOFactory;
import com.queryclient.ws.dao.ApiConsumerDAO;
import com.queryclient.ws.dao.UserDAO;
import com.queryclient.ws.dao.UserPasswordDAO;
import com.queryclient.ws.dao.ExternalUserAuthDAO;
import com.queryclient.ws.dao.UserAuthStateDAO;
import com.queryclient.ws.dao.DataServiceDAO;
import com.queryclient.ws.dao.ServiceEndpointDAO;
import com.queryclient.ws.dao.QuerySessionDAO;
import com.queryclient.ws.dao.QueryRecordDAO;
import com.queryclient.ws.dao.DummyEntityDAO;
import com.queryclient.ws.dao.ServiceInfoDAO;
import com.queryclient.ws.dao.FiveTenDAO;


// Create your own mock object factory using MockDAOFactory as a template.
public class MockDAOFactory extends DAOFactory
{
    private static final Logger log = Logger.getLogger(MockDAOFactory.class.getName());

    // Using the Decorator pattern.
    private MockDAOFactory decoratedDAOFactory;
    private MockDAOFactory()
    {
        this(null);   // ????
    }
    private MockDAOFactory(MockDAOFactory decoratedDAOFactory)
    {
        this.decoratedDAOFactory = decoratedDAOFactory;
    }

    // Initialization-on-demand holder.
    private static class MockDAOFactoryHolder
    {
        private static final MockDAOFactory INSTANCE = new MockDAOFactory();
    }

    // Singleton method
    public static MockDAOFactory getInstance()
    {
        return MockDAOFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public MockDAOFactory getDecoratedDAOFactory()
    {
        return decoratedDAOFactory;
    }
    public void setDecoratedDAOFactory(MockDAOFactory decoratedDAOFactory)
    {
        this.decoratedDAOFactory = decoratedDAOFactory;
    }


    @Override
    public ApiConsumerDAO getApiConsumerDAO()
    {
        return new MockApiConsumerDAO(decoratedDAOFactory.getApiConsumerDAO()) {};
    }

    @Override
    public UserDAO getUserDAO()
    {
        return new MockUserDAO(decoratedDAOFactory.getUserDAO()) {};
    }

    @Override
    public UserPasswordDAO getUserPasswordDAO()
    {
        return new MockUserPasswordDAO(decoratedDAOFactory.getUserPasswordDAO()) {};
    }

    @Override
    public ExternalUserAuthDAO getExternalUserAuthDAO()
    {
        return new MockExternalUserAuthDAO(decoratedDAOFactory.getExternalUserAuthDAO()) {};
    }

    @Override
    public UserAuthStateDAO getUserAuthStateDAO()
    {
        return new MockUserAuthStateDAO(decoratedDAOFactory.getUserAuthStateDAO()) {};
    }

    @Override
    public DataServiceDAO getDataServiceDAO()
    {
        return new MockDataServiceDAO(decoratedDAOFactory.getDataServiceDAO()) {};
    }

    @Override
    public ServiceEndpointDAO getServiceEndpointDAO()
    {
        return new MockServiceEndpointDAO(decoratedDAOFactory.getServiceEndpointDAO()) {};
    }

    @Override
    public QuerySessionDAO getQuerySessionDAO()
    {
        return new MockQuerySessionDAO(decoratedDAOFactory.getQuerySessionDAO()) {};
    }

    @Override
    public QueryRecordDAO getQueryRecordDAO()
    {
        return new MockQueryRecordDAO(decoratedDAOFactory.getQueryRecordDAO()) {};
    }

    @Override
    public DummyEntityDAO getDummyEntityDAO()
    {
        return new MockDummyEntityDAO(decoratedDAOFactory.getDummyEntityDAO()) {};
    }

    @Override
    public ServiceInfoDAO getServiceInfoDAO()
    {
        return new MockServiceInfoDAO(decoratedDAOFactory.getServiceInfoDAO()) {};
    }

    @Override
    public FiveTenDAO getFiveTenDAO()
    {
        return new MockFiveTenDAO(decoratedDAOFactory.getFiveTenDAO()) {};
    }

}
