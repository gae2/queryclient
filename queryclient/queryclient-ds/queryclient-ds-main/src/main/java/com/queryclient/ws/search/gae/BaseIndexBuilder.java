package com.queryclient.ws.search.gae;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.GetRequest;
import com.google.appengine.api.search.GetResponse;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.IndexSpec;
import com.google.appengine.api.search.PutException;
import com.google.appengine.api.search.PutResponse;
import com.google.appengine.api.search.SearchServiceFactory;
import com.google.appengine.api.search.StatusCode;


public class BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(BaseIndexBuilder.class.getName());
    
    private final String mIndexName;
    public BaseIndexBuilder(String indexName)
    {
        mIndexName = indexName;
    }


    public Index getIndex() 
    {
        IndexSpec indexSpec = IndexSpec.newBuilder()
            .setName(mIndexName)
             // .setConsistency(Consistency.PER_DOCUMENT)
            .build();
        return SearchServiceFactory.getSearchService().getIndex(indexSpec);
    }

    public boolean addDocument(Document document)
    {
        boolean suc = false;
        try {
            getIndex().put(document);
            PutResponse response = getIndex().put(document);
            // results = response.iterator();
            // TBD
            suc = true;
        } catch (PutException e) {
            if (StatusCode.TRANSIENT_ERROR.equals(e.getOperationResult().getCode())) {
                // TBD: retry putting document
            }
            log.log(Level.WARNING, "Failed to put document.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to put document due to unknown error.", e);
        }
        return suc;
    }

    public boolean removeDocument(String docId)
    {
        boolean suc = false;
        try {
            List<String> docIds = new ArrayList<String>();
            docIds.add(docId);
            getIndex().delete(docIds);
            suc = true;  // ???
        } catch (RuntimeException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to remove document with docuId = " + docId, e);
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to remove document due to unknown error. docuId = " + docId, e);
        }
        return suc;
    }

    public boolean removeDocuments(List<String> docIds)
    {
        boolean suc = false;
        try {
            if(docIds != null && !docIds.isEmpty()) {
                getIndex().delete(docIds);
                suc = true;   // ???
            }
        } catch (RuntimeException e) {
            log.log(Level.WARNING, "Failed to remove documents", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to remove documents due to unknown error", e);
        }
        return suc;
    }

    public boolean removeAllDocuments()
    {
        boolean suc = false;
        try {
            // temporary
            int MAX_COUNT = 100;
            int cnt = 0;
            while (++cnt < MAX_COUNT) {
                List<String> docIds = new ArrayList<String>();
                // Return a set of document IDs.
                GetRequest request = GetRequest.newBuilder().setReturningIdsOnly(true).build();
                GetResponse<Document> response = getIndex().getRange(request);
                if (response.getResults().isEmpty()) {
                    suc = true;
                    break;
                }
                for (Document doc : response) {
                    docIds.add(doc.getId());
                }
                getIndex().delete(docIds);
            }
        } catch (RuntimeException e) {
            log.log(Level.WARNING, "Failed to remove documents", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to remove documents due to unknown error", e);
        }
        return suc;
    }

    public static Date parseDate(String strDate)
    {
        if(strDate == null) {
            return null;  // ???
        }
        Date date = null;
        try {
            // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            // TimeZone timeZone = TimeZone.getDefault();
            // //TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
            // dateFormat.setTimeZone(timeZone);
            date = dateFormat.parse(strDate);
        } catch (ParseException e1) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse strDate: " + strDate, e1);
        } catch (Exception e2) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse Date: strDate = " + strDate, e2);
        }
        return date;
    }
    public static Date parseDate(String strDate, String timeZone)
    {
        if(strDate == null) {
            return null;  // ???
        }
        Date date = null;
        try {
            // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy z");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd z");
            TimeZone tz = TimeZone.getTimeZone(timeZone);
            dateFormat.setTimeZone(tz);
            date = dateFormat.parse(strDate);
        } catch (ParseException e1) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse strDate: " + strDate, e1);
        } catch (Exception e2) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse Date: strDate = " + strDate, e2);
        }
        return date;
    }

    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        // TimeZone timeZone = TimeZone.getDefault();
        // //TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
        // dateFormat.setTimeZone(timeZone);
        String date = dateFormat.format(new Date(time));
        return date;
    }
    public static String formatDate(Long time, String timeZone)
    {
        return formatDate(time, timeZone, true);
    }
    public static String formatDate(Long time, String timeZone, boolean showTimeZone)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = null;
        if(showTimeZone) {
            //dateFormat = new SimpleDateFormat("MM/dd/yyyy z");
            dateFormat = new SimpleDateFormat("yyyy-MM-dd z");
        } else {
            //dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        }
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        dateFormat.setTimeZone(tz);
        String date = dateFormat.format(new Date(time));
        return date;
    }

}
