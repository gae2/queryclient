package com.queryclient.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.config.Config;
import com.queryclient.ws.exception.DataStoreException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.dao.UserDAO;
import com.queryclient.ws.data.UserDataObject;


// MockUserDAO is a decorator.
// It can be used as a base class to mock UserDAO objects.
public abstract class MockUserDAO implements UserDAO
{
    private static final Logger log = Logger.getLogger(MockUserDAO.class.getName()); 

    // MockUserDAO uses the decorator design pattern.
    private UserDAO decoratedDAO;

    public MockUserDAO(UserDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected UserDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(UserDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public UserDataObject getUser(String guid) throws BaseException
    {
        return decoratedDAO.getUser(guid);
	}

    @Override
    public List<UserDataObject> getUsers(List<String> guids) throws BaseException
    {
        return decoratedDAO.getUsers(guids);
    }

    @Override
    public List<UserDataObject> getAllUsers() throws BaseException
	{
	    return getAllUsers(null, null, null);
    }


    @Override
    public List<UserDataObject> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllUsers(ordering, offset, count, null);
    }

    @Override
    public List<UserDataObject> getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllUsers(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllUserKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findUsers(filter, ordering, params, values, null, null);
    }

    @Override
	public List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findUsers(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findUsers(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUser(UserDataObject user) throws BaseException
    {
        return decoratedDAO.createUser( user);
    }

    @Override
	public Boolean updateUser(UserDataObject user) throws BaseException
	{
        return decoratedDAO.updateUser(user);
	}
	
    @Override
    public Boolean deleteUser(UserDataObject user) throws BaseException
    {
        return decoratedDAO.deleteUser(user);
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        return decoratedDAO.deleteUser(guid);
	}

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteUsers(filter, params, values);
    }

}
