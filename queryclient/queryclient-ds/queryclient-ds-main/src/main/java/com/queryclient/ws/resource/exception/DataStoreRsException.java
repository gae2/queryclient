package com.queryclient.ws.resource.exception;

import com.queryclient.ws.exception.resource.BaseResourceException;


public class DataStoreRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public DataStoreRsException() 
    {
        super();
    }
    public DataStoreRsException(String message) 
    {
        super(message);
    }
    public DataStoreRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public DataStoreRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public DataStoreRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public DataStoreRsException(Throwable cause) 
    {
        super(cause);
    }
    public DataStoreRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
