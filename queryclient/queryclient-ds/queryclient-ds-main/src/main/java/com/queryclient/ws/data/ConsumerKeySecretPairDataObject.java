package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class ConsumerKeySecretPairDataObject implements ConsumerKeySecretPair, Serializable
{
    private static final Logger log = Logger.getLogger(ConsumerKeySecretPairDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _consumerkeysecretpair_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String consumerKey;

    @Persistent(defaultFetchGroup = "true")
    private String consumerSecret;

    public ConsumerKeySecretPairDataObject()
    {
        // ???
        // this(null, null);
    }
    public ConsumerKeySecretPairDataObject(String consumerKey, String consumerSecret)
    {
        setConsumerKey(consumerKey);
        setConsumerSecret(consumerSecret);
    }

    private void resetEncodedKey()
    {
    }

    public String getConsumerKey()
    {
        return this.consumerKey;
    }
    public void setConsumerKey(String consumerKey)
    {
        this.consumerKey = consumerKey;
        if(this.consumerKey != null) {
            resetEncodedKey();
        }
    }

    public String getConsumerSecret()
    {
        return this.consumerSecret;
    }
    public void setConsumerSecret(String consumerSecret)
    {
        this.consumerSecret = consumerSecret;
        if(this.consumerSecret != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getConsumerKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getConsumerSecret() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("consumerKey", this.consumerKey);
        dataMap.put("consumerSecret", this.consumerSecret);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ConsumerKeySecretPair thatObj = (ConsumerKeySecretPair) obj;
        if( (this.consumerKey == null && thatObj.getConsumerKey() != null)
            || (this.consumerKey != null && thatObj.getConsumerKey() == null)
            || !this.consumerKey.equals(thatObj.getConsumerKey()) ) {
            return false;
        }
        if( (this.consumerSecret == null && thatObj.getConsumerSecret() != null)
            || (this.consumerSecret != null && thatObj.getConsumerSecret() == null)
            || !this.consumerSecret.equals(thatObj.getConsumerSecret()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = consumerKey == null ? 0 : consumerKey.hashCode();
        _hash = 31 * _hash + delta;
        delta = consumerSecret == null ? 0 : consumerSecret.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
