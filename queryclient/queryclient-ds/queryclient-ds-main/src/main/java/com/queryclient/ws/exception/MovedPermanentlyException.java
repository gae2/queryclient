package com.queryclient.ws.exception;

import com.queryclient.ws.BaseException;


public class MovedPermanentlyException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public MovedPermanentlyException() 
    {
        super();
    }
    public MovedPermanentlyException(String message) 
    {
        super(message);
    }
   public MovedPermanentlyException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public MovedPermanentlyException(Throwable cause) 
    {
        super(cause);
    }

}
