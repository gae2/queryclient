package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "externalUserAuth")
@XmlType(propOrder = {"guid", "managerApp", "appAcl", "gaeAppStub", "ownerUser", "userAcl", "user", "providerId", "externalUserIdStub", "requestToken", "accessToken", "accessTokenSecret", "email", "firstName", "lastName", "fullName", "displayName", "description", "gender", "dateOfBirth", "profileImageUrl", "timeZone", "postalCode", "location", "country", "language", "status", "authTime", "expirationTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalUserAuthStub implements ExternalUserAuth, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExternalUserAuthStub.class.getName());

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructStub gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private String providerId;
    private ExternalUserIdStructStub externalUserId;
    private String requestToken;
    private String accessToken;
    private String accessTokenSecret;
    private String email;
    private String firstName;
    private String lastName;
    private String fullName;
    private String displayName;
    private String description;
    private String gender;
    private String dateOfBirth;
    private String profileImageUrl;
    private String timeZone;
    private String postalCode;
    private String location;
    private String country;
    private String language;
    private String status;
    private Long authTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    public ExternalUserAuthStub()
    {
        this(null);
    }
    public ExternalUserAuthStub(ExternalUserAuth bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.managerApp = bean.getManagerApp();
            this.appAcl = bean.getAppAcl();
            this.gaeApp = GaeAppStructStub.convertBeanToStub(bean.getGaeApp());
            this.ownerUser = bean.getOwnerUser();
            this.userAcl = bean.getUserAcl();
            this.user = bean.getUser();
            this.providerId = bean.getProviderId();
            this.externalUserId = ExternalUserIdStructStub.convertBeanToStub(bean.getExternalUserId());
            this.requestToken = bean.getRequestToken();
            this.accessToken = bean.getAccessToken();
            this.accessTokenSecret = bean.getAccessTokenSecret();
            this.email = bean.getEmail();
            this.firstName = bean.getFirstName();
            this.lastName = bean.getLastName();
            this.fullName = bean.getFullName();
            this.displayName = bean.getDisplayName();
            this.description = bean.getDescription();
            this.gender = bean.getGender();
            this.dateOfBirth = bean.getDateOfBirth();
            this.profileImageUrl = bean.getProfileImageUrl();
            this.timeZone = bean.getTimeZone();
            this.postalCode = bean.getPostalCode();
            this.location = bean.getLocation();
            this.country = bean.getCountry();
            this.language = bean.getLanguage();
            this.status = bean.getStatus();
            this.authTime = bean.getAuthTime();
            this.expirationTime = bean.getExpirationTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    @XmlElement
    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    @XmlElement(name = "gaeApp")
    @JsonIgnore
    public GaeAppStructStub getGaeAppStub()
    {
        return this.gaeApp;
    }
    public void setGaeAppStub(GaeAppStructStub gaeApp)
    {
        this.gaeApp = gaeApp;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=GaeAppStructStub.class)
    public GaeAppStruct getGaeApp()
    {  
        return getGaeAppStub();
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if((gaeApp == null) || (gaeApp instanceof GaeAppStructStub)) {
            setGaeAppStub((GaeAppStructStub) gaeApp);
        } else {
            // TBD
            setGaeAppStub(GaeAppStructStub.convertBeanToStub(gaeApp));
        }
    }

    @XmlElement
    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    @XmlElement
    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getProviderId()
    {
        return this.providerId;
    }
    public void setProviderId(String providerId)
    {
        this.providerId = providerId;
    }

    @XmlElement(name = "externalUserId")
    @JsonIgnore
    public ExternalUserIdStructStub getExternalUserIdStub()
    {
        return this.externalUserId;
    }
    public void setExternalUserIdStub(ExternalUserIdStructStub externalUserId)
    {
        this.externalUserId = externalUserId;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ExternalUserIdStructStub.class)
    public ExternalUserIdStruct getExternalUserId()
    {  
        return getExternalUserIdStub();
    }
    public void setExternalUserId(ExternalUserIdStruct externalUserId)
    {
        if((externalUserId == null) || (externalUserId instanceof ExternalUserIdStructStub)) {
            setExternalUserIdStub((ExternalUserIdStructStub) externalUserId);
        } else {
            // TBD
            setExternalUserIdStub(ExternalUserIdStructStub.convertBeanToStub(externalUserId));
        }
    }

    @XmlElement
    public String getRequestToken()
    {
        return this.requestToken;
    }
    public void setRequestToken(String requestToken)
    {
        this.requestToken = requestToken;
    }

    @XmlElement
    public String getAccessToken()
    {
        return this.accessToken;
    }
    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    @XmlElement
    public String getAccessTokenSecret()
    {
        return this.accessTokenSecret;
    }
    public void setAccessTokenSecret(String accessTokenSecret)
    {
        this.accessTokenSecret = accessTokenSecret;
    }

    @XmlElement
    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    @XmlElement
    public String getFirstName()
    {
        return this.firstName;
    }
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    @XmlElement
    public String getLastName()
    {
        return this.lastName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    @XmlElement
    public String getFullName()
    {
        return this.fullName;
    }
    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    @XmlElement
    public String getDisplayName()
    {
        return this.displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    @XmlElement
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    @XmlElement
    public String getGender()
    {
        return this.gender;
    }
    public void setGender(String gender)
    {
        this.gender = gender;
    }

    @XmlElement
    public String getDateOfBirth()
    {
        return this.dateOfBirth;
    }
    public void setDateOfBirth(String dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }

    @XmlElement
    public String getProfileImageUrl()
    {
        return this.profileImageUrl;
    }
    public void setProfileImageUrl(String profileImageUrl)
    {
        this.profileImageUrl = profileImageUrl;
    }

    @XmlElement
    public String getTimeZone()
    {
        return this.timeZone;
    }
    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }

    @XmlElement
    public String getPostalCode()
    {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    @XmlElement
    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    @XmlElement
    public String getCountry()
    {
        return this.country;
    }
    public void setCountry(String country)
    {
        this.country = country;
    }

    @XmlElement
    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getAuthTime()
    {
        return this.authTime;
    }
    public void setAuthTime(Long authTime)
    {
        this.authTime = authTime;
    }

    @XmlElement
    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("ownerUser", this.ownerUser);
        dataMap.put("userAcl", this.userAcl);
        dataMap.put("user", this.user);
        dataMap.put("providerId", this.providerId);
        dataMap.put("externalUserId", this.externalUserId);
        dataMap.put("requestToken", this.requestToken);
        dataMap.put("accessToken", this.accessToken);
        dataMap.put("accessTokenSecret", this.accessTokenSecret);
        dataMap.put("email", this.email);
        dataMap.put("firstName", this.firstName);
        dataMap.put("lastName", this.lastName);
        dataMap.put("fullName", this.fullName);
        dataMap.put("displayName", this.displayName);
        dataMap.put("description", this.description);
        dataMap.put("gender", this.gender);
        dataMap.put("dateOfBirth", this.dateOfBirth);
        dataMap.put("profileImageUrl", this.profileImageUrl);
        dataMap.put("timeZone", this.timeZone);
        dataMap.put("postalCode", this.postalCode);
        dataMap.put("location", this.location);
        dataMap.put("country", this.country);
        dataMap.put("language", this.language);
        dataMap.put("status", this.status);
        dataMap.put("authTime", this.authTime);
        dataMap.put("expirationTime", this.expirationTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = ownerUser == null ? 0 : ownerUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAcl == null ? 0 : userAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = providerId == null ? 0 : providerId.hashCode();
        _hash = 31 * _hash + delta;
        delta = externalUserId == null ? 0 : externalUserId.hashCode();
        _hash = 31 * _hash + delta;
        delta = requestToken == null ? 0 : requestToken.hashCode();
        _hash = 31 * _hash + delta;
        delta = accessToken == null ? 0 : accessToken.hashCode();
        _hash = 31 * _hash + delta;
        delta = accessTokenSecret == null ? 0 : accessTokenSecret.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = firstName == null ? 0 : firstName.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastName == null ? 0 : lastName.hashCode();
        _hash = 31 * _hash + delta;
        delta = fullName == null ? 0 : fullName.hashCode();
        _hash = 31 * _hash + delta;
        delta = displayName == null ? 0 : displayName.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = gender == null ? 0 : gender.hashCode();
        _hash = 31 * _hash + delta;
        delta = dateOfBirth == null ? 0 : dateOfBirth.hashCode();
        _hash = 31 * _hash + delta;
        delta = profileImageUrl == null ? 0 : profileImageUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = timeZone == null ? 0 : timeZone.hashCode();
        _hash = 31 * _hash + delta;
        delta = postalCode == null ? 0 : postalCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = location == null ? 0 : location.hashCode();
        _hash = 31 * _hash + delta;
        delta = country == null ? 0 : country.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = authTime == null ? 0 : authTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ExternalUserAuthStub convertBeanToStub(ExternalUserAuth bean)
    {
        ExternalUserAuthStub stub = null;
        if(bean instanceof ExternalUserAuthStub) {
            stub = (ExternalUserAuthStub) bean;
        } else {
            if(bean != null) {
                stub = new ExternalUserAuthStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ExternalUserAuthStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of ExternalUserAuthStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ExternalUserAuthStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ExternalUserAuthStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ExternalUserAuthStub object as a string.", e);
        }
        
        return null;
    }
    public static ExternalUserAuthStub fromJsonString(String jsonStr)
    {
        try {
            ExternalUserAuthStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ExternalUserAuthStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalUserAuthStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalUserAuthStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalUserAuthStub object.", e);
        }
        
        return null;
    }

}
