package com.queryclient.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.queryclient.ws.ApiConsumer;
import com.queryclient.ws.util.CommonUtil;
import com.queryclient.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class ApiConsumerDataObject extends KeyedDataObject implements ApiConsumer
{
    private static final Logger log = Logger.getLogger(ApiConsumerDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ApiConsumerDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ApiConsumerDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String aeryId;

    @Persistent(defaultFetchGroup = "true")
    private String name;

    @Persistent(defaultFetchGroup = "true")
    private String description;

    @Persistent(defaultFetchGroup = "true")
    private String appKey;

    @Persistent(defaultFetchGroup = "true")
    private String appSecret;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public ApiConsumerDataObject()
    {
        this(null);
    }
    public ApiConsumerDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public ApiConsumerDataObject(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status)
    {
        this(guid, aeryId, name, description, appKey, appSecret, status, null, null);
    }
    public ApiConsumerDataObject(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.aeryId = aeryId;
        this.name = name;
        this.description = description;
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.status = status;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return ApiConsumerDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ApiConsumerDataObject.composeKey(getGuid());
    }

    public String getAeryId()
    {
        return this.aeryId;
    }
    public void setAeryId(String aeryId)
    {
        this.aeryId = aeryId;
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAppKey()
    {
        return this.appKey;
    }
    public void setAppKey(String appKey)
    {
        this.appKey = appKey;
    }

    public String getAppSecret()
    {
        return this.appSecret;
    }
    public void setAppSecret(String appSecret)
    {
        this.appSecret = appSecret;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("aeryId", this.aeryId);
        dataMap.put("name", this.name);
        dataMap.put("description", this.description);
        dataMap.put("appKey", this.appKey);
        dataMap.put("appSecret", this.appSecret);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ApiConsumer thatObj = (ApiConsumer) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.aeryId == null && thatObj.getAeryId() != null)
            || (this.aeryId != null && thatObj.getAeryId() == null)
            || !this.aeryId.equals(thatObj.getAeryId()) ) {
            return false;
        }
        if( (this.name == null && thatObj.getName() != null)
            || (this.name != null && thatObj.getName() == null)
            || !this.name.equals(thatObj.getName()) ) {
            return false;
        }
        if( (this.description == null && thatObj.getDescription() != null)
            || (this.description != null && thatObj.getDescription() == null)
            || !this.description.equals(thatObj.getDescription()) ) {
            return false;
        }
        if( (this.appKey == null && thatObj.getAppKey() != null)
            || (this.appKey != null && thatObj.getAppKey() == null)
            || !this.appKey.equals(thatObj.getAppKey()) ) {
            return false;
        }
        if( (this.appSecret == null && thatObj.getAppSecret() != null)
            || (this.appSecret != null && thatObj.getAppSecret() == null)
            || !this.appSecret.equals(thatObj.getAppSecret()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = aeryId == null ? 0 : aeryId.hashCode();
        _hash = 31 * _hash + delta;
        delta = name == null ? 0 : name.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = appKey == null ? 0 : appKey.hashCode();
        _hash = 31 * _hash + delta;
        delta = appSecret == null ? 0 : appSecret.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
