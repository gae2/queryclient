package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "queryRecords")
@XmlType(propOrder = {"queryRecord", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryRecordListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QueryRecordListStub.class.getName());

    private List<QueryRecordStub> queryRecords = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public QueryRecordListStub()
    {
        this(new ArrayList<QueryRecordStub>());
    }
    public QueryRecordListStub(List<QueryRecordStub> queryRecords)
    {
        this(queryRecords, null);
    }
    public QueryRecordListStub(List<QueryRecordStub> queryRecords, String forwardCursor)
    {
        this.queryRecords = queryRecords;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(queryRecords == null) {
            return true;
        } else {
            return queryRecords.isEmpty();
        }
    }
    public int getSize()
    {
        if(queryRecords == null) {
            return 0;
        } else {
            return queryRecords.size();
        }
    }


    @XmlElement(name = "queryRecord")
    public List<QueryRecordStub> getQueryRecord()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<QueryRecordStub> getList()
    {
        return queryRecords;
    }
    public void setList(List<QueryRecordStub> queryRecords)
    {
        this.queryRecords = queryRecords;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<QueryRecordStub> it = this.queryRecords.iterator();
        while(it.hasNext()) {
            QueryRecordStub queryRecord = it.next();
            sb.append(queryRecord.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static QueryRecordListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of QueryRecordListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write QueryRecordListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write QueryRecordListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write QueryRecordListStub object as a string.", e);
        }
        
        return null;
    }
    public static QueryRecordListStub fromJsonString(String jsonStr)
    {
        try {
            QueryRecordListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, QueryRecordListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into QueryRecordListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into QueryRecordListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into QueryRecordListStub object.", e);
        }
        
        return null;
    }

}
