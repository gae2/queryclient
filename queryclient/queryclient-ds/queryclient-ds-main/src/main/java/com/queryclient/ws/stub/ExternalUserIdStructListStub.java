package com.queryclient.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.util.JsonUtil;


@XmlRootElement(name = "externalUserIdStructs")
@XmlType(propOrder = {"externalUserIdStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalUserIdStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExternalUserIdStructListStub.class.getName());

    private List<ExternalUserIdStructStub> externalUserIdStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public ExternalUserIdStructListStub()
    {
        this(new ArrayList<ExternalUserIdStructStub>());
    }
    public ExternalUserIdStructListStub(List<ExternalUserIdStructStub> externalUserIdStructs)
    {
        this(externalUserIdStructs, null);
    }
    public ExternalUserIdStructListStub(List<ExternalUserIdStructStub> externalUserIdStructs, String forwardCursor)
    {
        this.externalUserIdStructs = externalUserIdStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(externalUserIdStructs == null) {
            return true;
        } else {
            return externalUserIdStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(externalUserIdStructs == null) {
            return 0;
        } else {
            return externalUserIdStructs.size();
        }
    }


    @XmlElement(name = "externalUserIdStruct")
    public List<ExternalUserIdStructStub> getExternalUserIdStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ExternalUserIdStructStub> getList()
    {
        return externalUserIdStructs;
    }
    public void setList(List<ExternalUserIdStructStub> externalUserIdStructs)
    {
        this.externalUserIdStructs = externalUserIdStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<ExternalUserIdStructStub> it = this.externalUserIdStructs.iterator();
        while(it.hasNext()) {
            ExternalUserIdStructStub externalUserIdStruct = it.next();
            sb.append(externalUserIdStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ExternalUserIdStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ExternalUserIdStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ExternalUserIdStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ExternalUserIdStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ExternalUserIdStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static ExternalUserIdStructListStub fromJsonString(String jsonStr)
    {
        try {
            ExternalUserIdStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ExternalUserIdStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalUserIdStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalUserIdStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ExternalUserIdStructListStub object.", e);
        }
        
        return null;
    }

}
