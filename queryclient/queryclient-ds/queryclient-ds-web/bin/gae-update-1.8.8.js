//////////////////////////////////////////
// Note: Need to authenticate through --oauth2 first
//       Otherwise, it'll prompt for password, and we have no way to input it from the script...
//////////////////////////////////////////

var fs = require("fs");
var path = require("path");
// var child_process = require("child_process");
var exec = require("child_process").exec;

var gaeSdkBinDir = "C:\\Apps\\appengine-java-sdk-1.8.8\\bin";
// var warDir = "..\\war";

var version = "0.1.0-SNAPSHOT";
if(process.argv.length > 2) {
    version = process.argv[2];
} else {
    console.log("Version not specified. Usage:");
    console.log("        node gae-update-xxx.js 1.0.0");
    return;
}

var warDir = "..\\target\\queryclient-ds-web-" + version;
var webInfDir = warDir + "\\WEB-INF";
// var webInfDir = "..\\tmp";
fs.exists(webInfDir, function(exists) {
    if(exists) {
        fs.readdir(webInfDir, function(err, files) {
            var fileList = [];

            // [1] Use all appengine files in the dir
            if(files) {
                files.forEach(function(f) {
                    if(path.extname(f) == ".xml" 
                        && f.indexOf("appengine-web") == 0 
                        && ( f.indexOf("-alpha.xml") == -1 && f.indexOf("-saved.xml") == -1 && f.indexOf("-tmp.xml") == -1 && f.indexOf("-tbd.xml") == -1 )
                        && f != "appengine-web.xml") {
                        console.log("appengine-web xml file: f = " + f);
                        fileList.push(f);
                    }
                });
            }
            // [2] Or, hardcode the list
            // fileList = [...];
            // ....

            // Deploy the projects one at a time...
            deployProject(fileList, 0); 
        });
    } else {
        console.log("The directory, " + webInfDir + ", does not exist!");
    }
});


// Recursive - async calls...
var deployProject = function(fileList, idx) 
{
    if(fileList) {
        var len = fileList.length;
        if(idx < 0 || idx >= len) {
            return;
        }

        var f = fileList[idx];
        var srcFile = webInfDir + '\\' + f;
        var destFile = webInfDir + '\\appengine-web.xml';
        console.log("Start processing: " + idx + ": " + f);

        // var that = this;
        var copyCmd = 'copy ' + srcFile + ' ' + destFile;
        var copyProc = exec(copyCmd, function(copyError, copyStdout, copyStderr) {
            console.log('copyStdout: ' + copyStdout);
            console.log('copyStderr: ' + copyStderr);
            if (copyError !== null) {
                console.log('Copy exec error: ' + copyError);
                return;
            } else {
                var deployCmd = gaeSdkBinDir + '\\appcfg --oauth2 --use_java7 --enable_jar_splitting  update ' + warDir;
                var deployProc = exec(deployCmd, function(deployError, deployStdout, deployStderr) {
                    console.log('deployStdout: ' + deployStdout);
                    console.log('deployStderr: ' + deployStderr);
                    if (deployError !== null) {
                        console.log('Deploy exec error: ' + deployError);
                        return;
                    } else {
                        console.log('Sucessfully deployed: f = ' + f);
                        deployProject(fileList, idx + 1);
                        return;
                    }
                });
            }
        });
    } else {
        console.log('fileList is empty/null.');
        return;
    }
};

