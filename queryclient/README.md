# Query Client

Cloud-based query client app for WSQL data storage services.
WSQL (Web structured query language) is a SQL-like DSL
specifically designed to query data storage Web services API 
conformant with my (proprietary) "microservices framework".



The project consists of three Web services and their client SDKs.

* `queryclient-ds:` Data storage/access web services and its client SDKs.
* `queryclient-fe:` Web app/api and its client SDKs. Includes the "business logic".
* `queryclient-wc:` Web-based client app for `queryclient-fe`.




_Note: This was mostly developed ca 2010 as part of the "microservices framework" on GAE._

