package com.queryclient.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairListStub;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.ws.stub.ReferrerInfoStructListStub;
import com.queryclient.ws.stub.DataServiceStub;
import com.queryclient.ws.stub.DataServiceListStub;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.ws.service.DataServiceService;
import com.queryclient.af.proxy.DataServiceServiceProxy;
import com.queryclient.af.proxy.remote.RemoteDataServiceServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncDataServiceServiceProxy extends BaseAsyncServiceProxy implements DataServiceServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncDataServiceServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteDataServiceServiceProxy remoteProxy;

    public AsyncDataServiceServiceProxy()
    {
        remoteProxy = new RemoteDataServiceServiceProxy();
    }

    @Override
    public DataService getDataService(String guid) throws BaseException
    {
        return remoteProxy.getDataService(guid);
    }

    @Override
    public Object getDataService(String guid, String field) throws BaseException
    {
        return remoteProxy.getDataService(guid, field);       
    }

    @Override
    public List<DataService> getDataServices(List<String> guids) throws BaseException
    {
        return remoteProxy.getDataServices(guids);
    }

    @Override
    public List<DataService> getAllDataServices() throws BaseException
    {
        return getAllDataServices(null, null, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllDataServices(ordering, offset, count);
        return getAllDataServices(ordering, offset, count, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllDataServices(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllDataServiceKeys(ordering, offset, count);
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllDataServiceKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findDataServices(filter, ordering, params, values, grouping, unique, offset, count);
        return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDataService(String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        DataServiceBean bean = new DataServiceBean(null, user, name, description, type, mode, serviceUrl, authRequired, MarshalHelper.convertConsumerKeySecretPairToBean(authCredential), MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), status);
        return createDataService(bean);        
    }

    @Override
    public String createDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");

        String guid = dataService.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((DataServiceBean) dataService).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateDataService-" + guid;
        String taskName = "RsCreateDataService-" + guid + "-" + (new Date()).getTime();
        DataServiceStub stub = MarshalHelper.convertDataServiceToStub(dataService);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(DataServiceStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = dataService.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    DataServiceStub dummyStub = new DataServiceStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createDataService(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "dataServices/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createDataService(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "dataServices/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "DataService guid is invalid.");
        	throw new BaseException("DataService guid is invalid.");
        }
        DataServiceBean bean = new DataServiceBean(guid, user, name, description, type, mode, serviceUrl, authRequired, MarshalHelper.convertConsumerKeySecretPairToBean(authCredential), MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), status);
        return updateDataService(bean);        
    }

    @Override
    public Boolean updateDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");

        String guid = dataService.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "DataService object is invalid.");
        	throw new BaseException("DataService object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateDataService-" + guid;
        String taskName = "RsUpdateDataService-" + guid + "-" + (new Date()).getTime();
        DataServiceStub stub = MarshalHelper.convertDataServiceToStub(dataService);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(DataServiceStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = dataService.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    DataServiceStub dummyStub = new DataServiceStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateDataService(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "dataServices/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateDataService(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "dataServices/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteDataService-" + guid;
        String taskName = "RsDeleteDataService-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "dataServices/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteDataService(DataService dataService) throws BaseException
    {
        String guid = dataService.getGuid();
        return deleteDataService(guid);
    }

    @Override
    public Long deleteDataServices(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteDataServices(filter, params, values);
    }

}
