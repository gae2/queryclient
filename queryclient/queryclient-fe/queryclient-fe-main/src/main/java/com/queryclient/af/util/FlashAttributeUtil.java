package com.queryclient.af.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


// "Flash" attribute is a short-lived session attribute.
// Normally, a flash attribute is used to pass messages between different requests.
// The receiving request deletes the flash attribute once it has processed the message.
public class FlashAttributeUtil
{
    private static final Logger log = Logger.getLogger(FlashAttributeUtil.class.getName());

    // Supports multiple flash messages, using a map.
    private static final String SESSION_ATTR_FLASH_MAP = "session_flash_map";
    // Default flash message key
    private static final String DEAULT_FLASH_KEY = "_default_flash_key";
    // ....
    

    private FlashAttributeUtil() {}


    public static String getFlash(HttpServletRequest request)
    {
        String flash = getNamedFlash(request, DEAULT_FLASH_KEY);
        return flash;
    }
    public static String getNamedFlash(HttpServletRequest request, String key)
    {
        String flash = null;
        if(request != null) {
            HttpSession session = request.getSession();
            if(session != null) {
                flash = getNamedFlash(session, key);
            }
        }
        return flash;
    }
    public static String getFlash(HttpSession session)
    {
        String flash = getNamedFlash(session, DEAULT_FLASH_KEY);
        return flash;
    }
    public static String getNamedFlash(HttpSession session, String key)
    {
        String flash = null;
        Map<String, String> flashMap = getFlashMapAttribute(session);
        if(flashMap != null) {
            flash = flashMap.get(key);
        }
        if(log.isLoggable(Level.FINER)) log.finer("key = " + key + "; flash = " + flash);
        return flash;
    }

    public static String setFlash(HttpServletRequest request, String flash)
    {
        flash = setNamedFlash(request, DEAULT_FLASH_KEY, flash);
        return flash;
    }
    public static String setNamedFlash(HttpServletRequest request, String key, String flash)
    {
        String oldFlash = null;
        if(request != null) {
            HttpSession session = request.getSession();
            if(session != null) {
                oldFlash = setNamedFlash(session, key, flash);
            }
        }
        return oldFlash;
    }
    public static String setFlash(HttpSession session, String flash)
    {
        flash = setNamedFlash(session, DEAULT_FLASH_KEY, flash);
        return flash;
    }
    public static String setNamedFlash(HttpSession session, String key, String flash)
    {
        if(key == null || key.isEmpty()) {
            return null;
        }
        Map<String, String> flashMap = getFlashMapAttribute(session);
        if(flashMap == null) {
            flashMap = createNewFlashMap();
        }
        String oldFlash = flashMap.put(key, flash);
        flashMap = setFlashMapAttribute(session, flashMap);   // Is this necessary??
        if(flashMap == null) {
            return null;   // ???
        }
        if(log.isLoggable(Level.FINER)) log.finer("key = " + key + "; flash = " + flash + "; oldFlash = " + oldFlash);
        return oldFlash;
    }

    public static String removeFlash(HttpServletRequest request)
    {
        String flash = removeNamedFlash(request, DEAULT_FLASH_KEY);
        return flash;
    }
    public static String removeNamedFlash(HttpServletRequest request, String key)
    {
        String oldFlash = null;
        if(request != null) {
            HttpSession session = request.getSession();
            if(session != null) {
                oldFlash = removeNamedFlash(session, key);
            }
        }
        return oldFlash;
    }
    public static String removeFlash(HttpSession session)
    {
        String flash = removeNamedFlash(session, DEAULT_FLASH_KEY);
        return flash;
    }
    public static String removeNamedFlash(HttpSession session, String key)
    {
        if(key == null || key.isEmpty()) {
            return null;
        }
        Map<String, String> flashMap = getFlashMapAttribute(session);
        if(flashMap == null) {
            return null;
        }
        String flash = flashMap.remove(key);
        flashMap = setFlashMapAttribute(session, flashMap);   // Is this necessary??
        if(flashMap == null) {
            return null;   // ???
        }
        if(log.isLoggable(Level.FINER)) log.finer("key = " + key + "; flash = " + flash);
        return flash;
    }

    public static void clearAllFlashes(HttpServletRequest request)
    {
        if(request != null) {
            HttpSession session = request.getSession();
            if(session != null) {
                clearAllFlashes(session);
            }
        }
    }
    public static void clearAllFlashes(HttpSession session)
    {
        Map<String, String> flashMap = getFlashMapAttribute(session);
        if(flashMap == null) {
            return;
        }
        // TBD:
        // Remove the SESSION_ATTR_FLASH_MAP attr?
        // Or, just clear the flashMap?
        flashMap.clear();
        setFlashMapAttribute(session, flashMap);
        // removeFlashMapAttribute(session);
    }


    // TBD: Make the following a separate class???
    
    private static Map<String, String> createNewFlashMap()
    {
        Map<String, String> flashMap = new HashMap<String, String>();
        return flashMap;
    }

    @SuppressWarnings("unchecked")
    private static Map<String, String> getFlashMapAttribute(HttpServletRequest request)
    {
        Map<String, String> flashMap = null;
        if(request != null) {
            HttpSession session = request.getSession();
            flashMap = getFlashMapAttribute(session);
        }
        return flashMap;
    }
    @SuppressWarnings("unchecked")
    private static Map<String, String> getFlashMapAttribute(HttpSession session)
    {
        Map<String, String> flashMap = null;
        if(session != null) {
            flashMap = (Map<String, String>) session.getAttribute(SESSION_ATTR_FLASH_MAP);
        }
        return flashMap;
    }

    private static Map<String, String> setFlashMapAttribute(HttpServletRequest request, Map<String, String> flashMap)
    {
        if(request != null) {
            HttpSession session = request.getSession();
            flashMap = setFlashMapAttribute(session, flashMap);
        }
        return flashMap;
    }
    private static Map<String, String> setFlashMapAttribute(HttpSession session, Map<String, String> flashMap)
    {
        if(session != null) {
            session.setAttribute(SESSION_ATTR_FLASH_MAP, flashMap);
            return flashMap;
        }
        return null;
    }

    private static void removeFlashMapAttribute(HttpServletRequest request)
    {
        if(request != null) {
            HttpSession session = request.getSession();
            removeFlashMapAttribute(session);
        }
    }
    private static void removeFlashMapAttribute(HttpSession session)
    {
        if(session != null) {
            session.removeAttribute(SESSION_ATTR_FLASH_MAP);
        }
    }


}
