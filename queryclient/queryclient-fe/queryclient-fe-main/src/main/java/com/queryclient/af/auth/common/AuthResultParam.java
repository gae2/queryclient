package com.queryclient.af.auth.common;

import java.util.logging.Logger;
import java.util.logging.Level;


// Constants, to be used for CommonAuthUtil.PARAM_AUTHRESULT query param.
// And some utility/convenience methods.
public final class AuthResultParam
{
    private static final Logger log = Logger.getLogger(AuthResultParam.class.getName());

    private AuthResultParam() {}


    // TBD: PARAM_AUTHRESULT_MASK param ????
    // ....

    public static int parseAuthResult(String authResultStr)
    {
        int authResult = 0;
        try {
            authResult = Integer.parseInt(authResultStr);
        } catch(NumberFormatException e) {
            // Ignore
            if(log.isLoggable(Level.INFO)) log.info("Failed to parse authResult = " + authResultStr);
        }
        return authResult;
    }

}
