package com.queryclient.af.cert.proxy;

public abstract class AbstractProxyFactory
{
    public abstract PublicCertificateInfoServiceProxy getPublicCertificateInfoServiceProxy();
}
