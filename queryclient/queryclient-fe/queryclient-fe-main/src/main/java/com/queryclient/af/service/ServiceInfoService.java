package com.queryclient.af.service;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ServiceInfo;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ServiceInfoService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ServiceInfo getServiceInfo(String guid) throws BaseException;
    Object getServiceInfo(String guid, String field) throws BaseException;
    List<ServiceInfo> getServiceInfos(List<String> guids) throws BaseException;
    List<ServiceInfo> getAllServiceInfos() throws BaseException;
    /* @Deprecated */ List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count) throws BaseException;
    List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createServiceInfo(String title, String content, String type, String status, Long scheduledTime) throws BaseException;
    //String createServiceInfo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ServiceInfo?)
    String createServiceInfo(ServiceInfo serviceInfo) throws BaseException;
    ServiceInfo constructServiceInfo(ServiceInfo serviceInfo) throws BaseException;
    Boolean updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime) throws BaseException;
    //Boolean updateServiceInfo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateServiceInfo(ServiceInfo serviceInfo) throws BaseException;
    ServiceInfo refreshServiceInfo(ServiceInfo serviceInfo) throws BaseException;
    Boolean deleteServiceInfo(String guid) throws BaseException;
    Boolean deleteServiceInfo(ServiceInfo serviceInfo) throws BaseException;
    Long deleteServiceInfos(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createServiceInfos(List<ServiceInfo> serviceInfos) throws BaseException;
//    Boolean updateServiceInfos(List<ServiceInfo> serviceInfos) throws BaseException;

}
