package com.queryclient.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerKeySecretPairJsBean implements Serializable, Cloneable  //, ConsumerKeySecretPair
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ConsumerKeySecretPairJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String consumerKey;
    private String consumerSecret;

    // Ctors.
    public ConsumerKeySecretPairJsBean()
    {
        //this((String) null);
    }
    public ConsumerKeySecretPairJsBean(String consumerKey, String consumerSecret)
    {
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
    }
    public ConsumerKeySecretPairJsBean(ConsumerKeySecretPairJsBean bean)
    {
        if(bean != null) {
            setConsumerKey(bean.getConsumerKey());
            setConsumerSecret(bean.getConsumerSecret());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ConsumerKeySecretPairJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ConsumerKeySecretPairJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ConsumerKeySecretPairJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ConsumerKeySecretPairJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getConsumerKey()
    {
        return this.consumerKey;
    }
    public void setConsumerKey(String consumerKey)
    {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret()
    {
        return this.consumerSecret;
    }
    public void setConsumerSecret(String consumerSecret)
    {
        this.consumerSecret = consumerSecret;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getConsumerKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getConsumerSecret() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("consumerKey:null, ");
        sb.append("consumerSecret:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("consumerKey:");
        if(this.getConsumerKey() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getConsumerKey()).append("\", ");
        }
        sb.append("consumerSecret:");
        if(this.getConsumerSecret() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getConsumerSecret()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getConsumerKey() != null) {
            sb.append("\"consumerKey\":").append("\"").append(this.getConsumerKey()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"consumerKey\":").append("null, ");
        }
        if(this.getConsumerSecret() != null) {
            sb.append("\"consumerSecret\":").append("\"").append(this.getConsumerSecret()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"consumerSecret\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("consumerKey = " + this.consumerKey).append(";");
        sb.append("consumerSecret = " + this.consumerSecret).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ConsumerKeySecretPairJsBean cloned = new ConsumerKeySecretPairJsBean();
        cloned.setConsumerKey(this.getConsumerKey());   
        cloned.setConsumerSecret(this.getConsumerSecret());   
        return cloned;
    }

}
