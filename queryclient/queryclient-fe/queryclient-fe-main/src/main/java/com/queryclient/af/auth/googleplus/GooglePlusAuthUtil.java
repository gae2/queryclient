package com.queryclient.af.auth.googleplus;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.auth.common.OAuthConstants;


public class GooglePlusAuthUtil
{
    private static final Logger log = Logger.getLogger(GooglePlusAuthUtil.class.getName());

    // Constants
    private static final String CALLBACK_URL_OOB = "oob";    // Out of band.
    // ...

    public static final String CONFIG_KEY_APPLICATION_NAME = "queryclientapp.googleplus.application.name";
    public static final String CONFIG_KEY_CLIENT_ID = "queryclientapp.googleplus.clientid";
    public static final String CONFIG_KEY_CLIENT_SECRET = "queryclientapp.googleplus.clientsecret";
    // public static final String CONFIG_KEY_DEFAULT_ACCESSTOKEN = "queryclientapp.googleplus.default.accesstoken";
    // public static final String CONFIG_KEY_DEFAULT_ACCESSTOKENSECRET = "queryclientapp.googleplus.default.accesstokensecret";
    public static final String CONFIG_KEY_CALLBACK_TOKENHANDLER_URLPATH = "queryclientapp.googleplus.callback.tokenhandler.urlpath";
    public static final String CONFIG_KEY_CALLBACK_AUTHAJAX_URLPATH = "queryclientapp.googleplus.callback.authajax.urlpath";
    // public static final String CONFIG_KEY_OAUTH_REDIRECTURLPATH = "queryclientapp.googleplus.oauth.redirecturlpath";
    public static final String CONFIG_KEY_JAVASCRIPT_SIGNIN_CALLBACK = "queryclientapp.googleplus.javascript.signin.callback";     // Javascript callback function name
    // ...
    
    public static final String PARAM_CALLBACK = OAuthConstants.PARAM_OAUTH_CALLBACK;
    public static final String PARAM_CONSUMER_KEY = OAuthConstants.PARAM_OAUTH_CONSUMER_KEY;
    public static final String PARAM_NONCE = OAuthConstants.PARAM_OAUTH_NONCE;
    public static final String PARAM_SIGNATURE = OAuthConstants.PARAM_OAUTH_SIGNATURE;
    public static final String PARAM_SIGNATURE_METHOD = OAuthConstants.PARAM_OAUTH_SIGNATURE_METHOD;
    public static final String PARAM_TIMESTAMP = OAuthConstants.PARAM_OAUTH_TIMESTAMP;
    public static final String PARAM_VERSION = OAuthConstants.PARAM_OAUTH_VERSION;
    public static final String PARAM_REQUEST_TOKEN = OAuthConstants.PARAM_OAUTH_REQUEST_TOKEN;
    public static final String PARAM_TOKEN_VERIFIER = OAuthConstants.PARAM_OAUTH_TOKEN_VERIFIER;
    // These are specific to "google+ signin", which is a variation of OAuth2....
    // public static final String PARAM_CLIENT_ID = "client_id";
    // public static final String PARAM_APPLICATION_NAME = "application_name";
    public static final String PARAM_ACCESS_TOKEN = "access_token";
    public static final String PARAM_REFRESH_TOKEN = "refresh_token";
    public static final String PARAM_CODE = "code";              // Equivalent to OAuth request token ???
    // ...

    // TBD: Always using the same values for the google+ signin???
    public static final String DATA_SCOPE_PLUS_LOGIN = "https://www.googleapis.com/auth/plus.login";
    public static final String DATA_SCOPE_PLUS_ME = "https://www.googleapis.com/auth/plus.me";
    public static final String DATA_SCOPE_USERINFO_EMAIL = "https://www.googleapis.com/auth/userinfo.email"; 
    public static final String DATA_SCOPE_USERINFO_PROFILE = "https://www.googleapis.com/auth/userinfo.profile"; 
    public static final String DEFAULT_DATA_SCOPE = DATA_SCOPE_PLUS_LOGIN + " " + DATA_SCOPE_USERINFO_EMAIL;    // space separated...
    public static final String DEFAULT_DATA_REDIRECT_URL = "postmessage";                                 // ???
    public static final String DEFAULT_DATA_ACCESS_TYPE = "offline";                                      // ???
    public static final String DEFAULT_DATA_COOKIE_POLICY = "single_host_origin";                         // ???
    // ...

    // To be used for generating session attribute name for csrf state...
    public static final String DEFAULT_FORM_ID_GOOGLEPLUS_SIGNIN = "_googleplus_signin_form";         // ???
    // ...

    // "TokenResponse" object returned from Google+.
    public static final String SESSION_ATTR_TOKENRESPONSE = "com.queryclient.af.auth.googleplus.tokenresponse";
    // ...
    
    private GooglePlusAuthUtil() {}

    
    // temporary
    public static String getDefaultGooglePlusSignInFormId()
    {
        return DEFAULT_FORM_ID_GOOGLEPLUS_SIGNIN;
    }

    // temporary
    public static String getDefaultDataScope()
    {
        return DEFAULT_DATA_SCOPE;
    }

    // TBD:
    // ...
    
}
