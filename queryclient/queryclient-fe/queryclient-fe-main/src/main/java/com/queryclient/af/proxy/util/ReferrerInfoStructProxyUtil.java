package com.queryclient.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.ReferrerInfoStruct;
// import com.queryclient.ws.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;


public class ReferrerInfoStructProxyUtil
{
    private static final Logger log = Logger.getLogger(ReferrerInfoStructProxyUtil.class.getName());

    // Static methods only.
    private ReferrerInfoStructProxyUtil() {}

    public static ReferrerInfoStructBean convertServerReferrerInfoStructBeanToAppBean(ReferrerInfoStruct serverBean)
    {
        ReferrerInfoStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new ReferrerInfoStructBean();
            bean.setReferer(serverBean.getReferer());
            bean.setUserAgent(serverBean.getUserAgent());
            bean.setLanguage(serverBean.getLanguage());
            bean.setHostname(serverBean.getHostname());
            bean.setIpAddress(serverBean.getIpAddress());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.queryclient.ws.bean.ReferrerInfoStructBean convertAppReferrerInfoStructBeanToServerBean(ReferrerInfoStruct appBean)
    {
        com.queryclient.ws.bean.ReferrerInfoStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.ReferrerInfoStructBean();
            bean.setReferer(appBean.getReferer());
            bean.setUserAgent(appBean.getUserAgent());
            bean.setLanguage(appBean.getLanguage());
            bean.setHostname(appBean.getHostname());
            bean.setIpAddress(appBean.getIpAddress());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
