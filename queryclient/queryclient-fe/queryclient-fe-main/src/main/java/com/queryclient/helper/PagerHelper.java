package com.queryclient.helper;

import java.util.logging.Logger;

import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.app.util.ConfigUtil;
import com.queryclient.common.PagerMode;
import com.queryclient.fe.bean.PagerStateStructJsBean;
import com.queryclient.ws.PagerStateStruct;


public class PagerHelper
{
    private static final Logger log = Logger.getLogger(PagerHelper.class.getName());

    
    //
    private Integer mPageSize = null;
    
    
    private PagerHelper() 
    {
        init();
    }

    // Initialization-on-demand holder.
    private static final class PagerHelperHolder
    {
        private static final PagerHelper INSTANCE = new PagerHelper();
    }

    // Singleton method
    public static PagerHelper getInstance()
    {
        return PagerHelperHolder.INSTANCE;
    }

    private void init()
    {
        // TBD: Validation??? e.g., page size > 0.
        mPageSize = ConfigUtil.getPagerPageSize();
        if(mPageSize <= 0) {
            mPageSize = ConfigUtil.getDefaultPageSize();  // ???
        }
        // temporary
        log.info(">>>>>>> mPageSize = " + mPageSize);
    }

    
    public Integer getPageSize()
    {
        return mPageSize;
    }


    public PagerStateStructJsBean constructPagerStateFromOffset(Long offset, Integer count, Long totalCount, Long lowerBoundTotalCount)
    {
        PagerStateStructJsBean bean = new PagerStateStructJsBean();

        bean.setPagerMode(PagerMode.MODE_OFFSET);
        if(offset == null || offset < 0L) {  // ???
            offset = 0L;
        }
        bean.setCurrentOffset(offset);
        if(count == null || count <= 0) {
            count = getPageSize();
        }
        bean.setPageSize(count);
        bean.setTotalCount(totalCount);
        if(lowerBoundTotalCount == null) {
            lowerBoundTotalCount = totalCount;  // Could still be null...
        }
        bean.setLowerBoundTotalCount(lowerBoundTotalCount);
        bean.setPreviousPageOffset(getPreviousPageOffset(offset, count));
        bean.setNextPageOffset(getNextPageOffset(offset, count, totalCount, lowerBoundTotalCount));
        bean.setLastPageOffset(getLastPageOffset(offset, count, lowerBoundTotalCount));
        bean.setFirstActionEnabled(isFirstActionEnabled(offset, count));
        bean.setPreviousActionEnabled(isPreviousActionEnabled(offset, count));
        bean.setNextActionEnabled(isNextActionEnabled(offset, count, totalCount, lowerBoundTotalCount));
        bean.setLastActionEnabled(isLastActionEnabled(offset, count, totalCount));
        // etc...

        return bean;
    }

    public PagerStateStructJsBean constructPagerStateFromPage(long page, Integer pageSize, Long totalCount, Long lowerBoundTotalCount)
    {
        PagerStateStructJsBean bean = new PagerStateStructJsBean();

        bean.setPagerMode(PagerMode.MODE_PAGE);
        if(page < 0L) {  // ???
            page = 0L;
        }
        bean.setCurrentPage(page);
        if(pageSize == null || pageSize <= 0) {
            pageSize = getPageSize();
        }
        bean.setPageSize(pageSize);
        bean.setTotalCount(totalCount);
        if(lowerBoundTotalCount == null) {
            lowerBoundTotalCount = totalCount;  // Could still be null...
        }
        bean.setLowerBoundTotalCount(lowerBoundTotalCount);
        bean.setPreviousPageOffset(getPreviousPageOffset(page, pageSize));
        bean.setNextPageOffset(getNextPageOffset(page, pageSize, totalCount, lowerBoundTotalCount));
        bean.setLastPageOffset(getLastPageOffset(page, pageSize, lowerBoundTotalCount));
        bean.setFirstActionEnabled(isFirstActionEnabled(page, pageSize));
        bean.setPreviousActionEnabled(isPreviousActionEnabled(page, pageSize));
        bean.setNextActionEnabled(isNextActionEnabled(page, pageSize, totalCount, lowerBoundTotalCount));
        bean.setLastActionEnabled(isLastActionEnabled(page, pageSize, totalCount));
        // etc...

        return bean;
    }


    public long getOffsetFromPageIndex(long page)
    {
        return getOffsetFromPageIndex(page, null);
    } 

    public long getOffsetFromPageIndex(long page, Integer pageSize)
    {
        return getOffsetFromPageIndex(page, pageSize, null);
    }

    public long getOffsetFromPageIndex(long page, Integer pageSize, Long totalCount)
    {
        if(pageSize == null || pageSize <= 0) {
            pageSize = getPageSize();
        }
        long offset = 0L;
        if(pageSize != null) {
            if(page >= 0L) {
                offset = page * pageSize;
            } else {
                // ????
                if(totalCount == null || totalCount <= 0) {
                    // What to do???
                    log.warning("Could not compute the offset: page = " + page + "; totalCount = " + totalCount);
                    // --> offset = 0L;
                } else {
                    long totalPages = (long) Math.ceil( ((double) totalCount) / pageSize );
                    long realPageIdx = totalPages + page;
                    if(realPageIdx < 0L) {
                        realPageIdx = 0L;
                    }
                    offset = realPageIdx * pageSize;
                }
            }
        } else {
            // ??? Can this happen????
        }
        return offset;
    }


    public Long getPreviousPageOffset(Long offset, Integer count)
    {
        if(offset == null || offset <= 0L) {
            return null;   // Null return value means there is no "previous" page....
        }
        if(count == null || count <= 0) {
            count = getPageSize();
        }
        Long prevOffset = offset - count;
        if(prevOffset < 0L) {
            prevOffset = 0L;
        }
        return prevOffset;
    }

    public Long getNextPageOffset(Long offset, Integer count, Long totalCount, Long lowerBoundTotalCount)
    {
        if(lowerBoundTotalCount == null || lowerBoundTotalCount <= 0L || (totalCount != null && lowerBoundTotalCount < totalCount) ) {
            lowerBoundTotalCount = totalCount;  // Could still be null...
        }
        if(lowerBoundTotalCount == null || lowerBoundTotalCount <= 0) {
            // What to do???
            log.warning("Could not compute the next offset: offset = " + offset + "; lowerBoundTotalCount = " + lowerBoundTotalCount);
            return null;
        }
        if(offset == null) {
            offset = 0L;
        }
        if(count == null || count <= 0) {
            count = getPageSize();
        }
        Long nextOffset = offset + count;
        if(nextOffset < lowerBoundTotalCount) {
            // all is good...
        } else if((totalCount != null) && (nextOffset >= totalCount)) { 
            nextOffset = null;   // No "next" page...
        } else {
            // What to do ?????
            // ignore and go ahead.... ??? 
            log.warning("Computeted nextOffset might be incorrect: nextOffset = " + nextOffset);
        }
        return nextOffset;
    }

    // Last page is computed based on the current offset, not from 0.
    public Long getLastPageOffset(Long offset, Integer count, Long totalCount)
    {
        if(totalCount == null || totalCount <= 0) {
            // What to do???
            log.warning("Could not compute the last page offset: offset = " + offset + "; totalCount = " + totalCount);
            return null;
        }
        if(offset == null || offset >= totalCount) {  // always should be: offset < tatalCount...
            offset = 0L;
        }
        if(count == null || count <= 0) {
            count = getPageSize();
        }
        long remaining = totalCount - offset;
        long delta = (long) (remaining - 1) / count;
        long lastOffset = offset + delta * count;
        return lastOffset;
    }


    public Long getPreviousPageOffset(long page, Integer pageSize)
    {
        if(page <= 0L) {
            return null;
        } else if(page == 1L) {
            return 0L;
        }
        if(pageSize == null || pageSize <= 0) {
            pageSize = getPageSize();
        }
        Long prevOffset = (page - 1) * pageSize;
        if(prevOffset < 0L) {
            prevOffset = 0L;
        }
        return prevOffset;
    }

    public Long getNextPageOffset(long page, Integer pageSize, Long totalCount, Long lowerBoundTotalCount)
    {
        if(lowerBoundTotalCount == null || lowerBoundTotalCount <= 0L || (totalCount != null && lowerBoundTotalCount < totalCount) ) {
            lowerBoundTotalCount = totalCount;  // Could still be null...
        }
        if(lowerBoundTotalCount == null || lowerBoundTotalCount <= 0) {
            // What to do???
            log.warning("Could not compute the next offset: page = " + page + "; lowerBoundTotalCount = " + lowerBoundTotalCount);
            return null;
        }
        if(pageSize == null || pageSize <= 0) {
            pageSize = getPageSize();
        }
        Long nextOffset = (page + 1) * pageSize;
        if(nextOffset < lowerBoundTotalCount) {
            // all is good...
        } else if((totalCount != null) && (nextOffset >= totalCount)) { 
            nextOffset = null;   // No "next" page...
        } else {
            // What to do ?????
            // ignore and go ahead.... for now...
            log.warning("Computeted nextOffset might be incorrect: nextOffset = " + nextOffset);
        }
        return nextOffset;
    }

    // Last page is computed based on the current page, not from 0.
    public Long getLastPageOffset(long page, Integer pageSize, Long totalCount)
    {
        if(totalCount == null || totalCount <= 0) {
            // What to do???
            log.warning("Could not compute the last page offset: page = " + page + "; totalCount = " + totalCount);
            return null;
        }
        if(pageSize == null || pageSize <= 0) {
            pageSize = getPageSize();
        }
        long offset = page * pageSize;
        if(offset >= totalCount) {  // always should be: offset < tatalCount...
            offset = 0L;
        }
        long remaining = totalCount - offset;
        long delta = (long) (remaining - 1) / pageSize;
        long lastOffset = offset + delta * pageSize;
        return lastOffset;
    }

    // Last page is computed based on the current page, not from 0.
    public long getLastPageIndex(long page, Integer pageSize, Long totalCount)
    {
        Long lastOffset = getLastPageOffset(page, pageSize, totalCount);
        if(lastOffset == null) {
            return 0L;
        }
        if(pageSize == null || pageSize <= 0) {
            pageSize = getPageSize();
        }
        long lastPageIndex = lastOffset / pageSize; 
        return lastPageIndex;
    }

    
    
    // temporary
    
    public boolean isFirstActionEnabled(Long offset, Integer count)
    {
        // temporary
        return isPreviousActionEnabled(offset, count);
    }
    public boolean isPreviousActionEnabled(Long offset, Integer count)
    {
        if(offset != null && offset > 0L) {
            return true;
        } else {
            return false;
        }
    }
    public boolean isLastActionEnabled(Long offset, Integer count, Long totalCount)
    {
        // temporary
        if(totalCount == null || totalCount <= 1L) {
            return false;
        }
        return isNextActionEnabled(offset, count, totalCount, null);
    }
    public boolean isNextActionEnabled(Long offset, Integer count, Long totalCount, Long lowerBoundTotalCount)
    {
        // temporary
        Long nextOffset = getNextPageOffset(offset, count, totalCount, lowerBoundTotalCount);
        if(nextOffset == null) {
            return false;
        } else {
            //if(nextOffset >= offset + count) {
                return true;
            //} else {
            //    return false;
            //}
        }
    }
    
    
    public boolean isFirstActionEnabled(long page, Integer pageSize)
    {
        // temporary
        return isPreviousActionEnabled(page, pageSize);
    }
    public boolean isPreviousActionEnabled(long page, Integer pageSize)
    {
        if(page == 0L) {
            return false;
        } else {
            return true;
        }
    }
    public boolean isLastActionEnabled(long page, Integer pageSize, Long totalCount)
    {
        // temporary
        if(totalCount == null || totalCount <= 1L) {
            return false;
        }
        return isNextActionEnabled(page, pageSize, totalCount, null);
    }
    public boolean isNextActionEnabled(long page, Integer pageSize, Long totalCount, Long lowerBoundTotalCount)
    {
        // temporary
        Long nextOffset = getNextPageOffset(page, pageSize, totalCount, lowerBoundTotalCount);
        if(nextOffset == null) {
            return false;
        } else {
            //if(nextOffset >= (page + 1) * pageSize) {
                return true;
            //} else {
            //    return false;
            //}
        }
    }

    
}
