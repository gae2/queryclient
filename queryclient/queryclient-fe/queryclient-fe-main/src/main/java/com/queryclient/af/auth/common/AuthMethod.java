package com.queryclient.af.auth.common;

import java.util.logging.Logger;


// temporary
public final class AuthMethod
{
    private static final Logger log = Logger.getLogger(AuthMethod.class.getName());

    // ???
    public static final String NONE = "None";
    public static final String BASIC = "Basic";
    public static final String DIGEST = "Digest";
    public static final String OAUTH = "OAuth";   // OAuth1
    // public static final String OAUTH2 = "OAuth2";   // Use "OAuth2" or "Bearer"?
    public static final String BEARER = "Bearer";   // OAuth2 token type ???
    // ..
    public static final String TWOLEGGED = "TwoLegged";   // OAuth, client key based
    // etc...
    
    private AuthMethod() {}

    // TBD:
    // isValid() ???
    // ...

}
