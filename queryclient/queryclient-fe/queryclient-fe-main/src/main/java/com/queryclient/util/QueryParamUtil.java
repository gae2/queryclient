package com.queryclient.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

public class QueryParamUtil
{
    private static final Logger log = Logger.getLogger(QueryParamUtil.class.getName());

    private QueryParamUtil() {}

    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_COUNT = "count";
    public static final String PARAM_PAGESIZE = "pagesize";
    public static final String PARAM_ORDERING = "ordering";    // "field [asc/desc]"
    //public static final String PARAM_WEBPAGE = "queryrecord";
    //public static final String PARAM_WEBSITE = "website";
    public static final String PARAM_PAGEID = "pageid";      // Guid
    public static final String PARAM_SITEID = "siteid";      // Guid
    public static final String PARAM_THEME = "theme";
    public static final String PARAM_EDITMODE = "editmode";

    
    public static Long getParamOffset(HttpServletRequest request)
    {
        Long paramOffset = null;
        if(request != null) {
            String strOffset = request.getParameter(PARAM_OFFSET);
            if(strOffset != null) {
                try {
                    paramOffset = Long.parseLong(strOffset);
                } catch(NumberFormatException e) {
                    // ignore
                    log.log(Level.INFO, "Invalid param: offset = " + strOffset, e);
                }
            }
        }
        return paramOffset;
    }

    public static Integer getParamCount(HttpServletRequest request)
    {
        Integer paramCount = null;
        if(request != null) {
            String strCount = request.getParameter(PARAM_COUNT);
            if(strCount != null) {
                try {
                    paramCount = Integer.parseInt(strCount);
                } catch(NumberFormatException e) {
                    // ignore
                    log.log(Level.INFO, "Invalid param: count = " + strCount, e);
                }
            }
        }
        return paramCount;
    }

    public static Integer getParamPageSize(HttpServletRequest request)
    {
        Integer paramPageSize = null;
        if(request != null) {
            String strPageSize = request.getParameter(PARAM_PAGESIZE);
            if(strPageSize != null) {
                try {
                    paramPageSize = Integer.parseInt(strPageSize);
                } catch(NumberFormatException e) {
                    // ignore
                    log.log(Level.INFO, "Invalid param: count = " + strPageSize, e);
                }
            }
        }
        return paramPageSize;
    }

    public static String getParamOrdering(HttpServletRequest request)
    {
        String paramOrdering = null;
        if(request != null) {
            paramOrdering = request.getParameter(PARAM_ORDERING);
        }
        return paramOrdering;
    }

    public static String getParamPageId(HttpServletRequest request)
    {
        String paramPageId = null;
        if(request != null) {
            paramPageId = request.getParameter(PARAM_PAGEID);
        }
        return paramPageId;
    }

    public static String getParamSiteId(HttpServletRequest request)
    {
        String paramSiteId = null;
        if(request != null) {
            paramSiteId = request.getParameter(PARAM_SITEID);
        }
        return paramSiteId;
    }

    public static String getParamTheme(HttpServletRequest request)
    {
        String paramTheme = null;
        if(request != null) {
            paramTheme = request.getParameter(PARAM_THEME);
        }
        return paramTheme;
    }

//    public static String getParamEditMode(HttpServletRequest request)
//    {
//        String paramEditMode = null;
//        if(request != null) {
//            paramEditMode = request.getParameter(PARAM_EDITMODE);
//        }
//        return paramEditMode;
//    }

    public static int getParamEditMode(HttpServletRequest request)
    {
        int paramEditMode = -1;
        if(request != null) {
            String strEditMode = request.getParameter(PARAM_EDITMODE);
            if(strEditMode != null) {
                try {
                    paramEditMode = Integer.parseInt(strEditMode);
                } catch(NumberFormatException e) {
                    // ignore
                    log.log(Level.INFO, "Invalid param: strEditMode = " + strEditMode, e);
                }
            }
        }
        return paramEditMode;
    }

    
    // TBD:
    
//    public static Map<String, String> parseQueryString(HttpServletRequest request)
//    {
//        Map<String, String> paramMap = new HashMap<String, String>();
//        if(request != null) {
//           // TBD ...            
//        }
//        return paramMap;
//    }

}
