package com.queryclient.util.parser;

import java.util.HashSet;
import java.util.Set;

public class JdoqlKeyword
{
    private JdoqlKeyword() {}

    // "Keywords"
    public static final String SELECT = "SELECT";
    public static final String CREATE = "CREATE";
    public static final String UPDATE = "UPDATE";
    public static final String DELETE = "DELETE";
    public static final String GUID = "GUID";
    //public static final String AGGREGATE = "AGGREGATE";
    public static final String VALUE = "VALUE";   // Payload
    //public static final String PAYLOAD = "PAYLOAD";
    public static final String UNIQUE = "UNIQUE";
    public static final String FROM = "FROM";
    //public static final String INTO = "INTO";
    public static final String WHERE = "WHERE";
    public static final String GROUP = "GROUP";
    public static final String ORDER = "ORDER";
    public static final String BY = "BY";
    public static final String GROUP_BY = "GROUP BY";
    public static final String ORDER_BY = "ORDER BY";
    public static final String RANGE = "RANGE";
    public static final String OFFSET = "OFFSET";
    public static final String COUNT = "COUNT";
    // ...
    public static final String AGGREGATE_COUNT = "COUNT";
    // ...

    private static Set<String> TOKENS;
    static {
        TOKENS = new HashSet<String>();
        TOKENS.add(SELECT);
        TOKENS.add(CREATE);
        TOKENS.add(UPDATE);
        TOKENS.add(DELETE);
        TOKENS.add(GUID);
        TOKENS.add(VALUE);
        TOKENS.add(UNIQUE);
        TOKENS.add(FROM);
        TOKENS.add(WHERE);
        TOKENS.add(GROUP_BY);
        TOKENS.add(ORDER_BY);
        TOKENS.add(RANGE);
        TOKENS.add(OFFSET);
        TOKENS.add(COUNT);
        //TOKENS.add(AGGREGATE);
    }
    private static Set<String> KEYWORDS;
    static {
        KEYWORDS = new HashSet<String>();
        KEYWORDS.add(GROUP);
        KEYWORDS.add(ORDER);
        KEYWORDS.add(BY);
        // ...
    }
    private static Set<String> AGGREGATES;
    static {
        AGGREGATES = new HashSet<String>();
        AGGREGATES.add(AGGREGATE_COUNT);
        // ...
    }

    public static boolean isToken(String symbol)
    {
        if(symbol == null) {
            return false;
        }
        if(TOKENS.contains(symbol.toUpperCase())) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isKeyword(String symbol)
    {
        if(symbol == null) {
            return false;
        }
        if(KEYWORDS.contains(symbol.toUpperCase())) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isAggregate(String symbol)
    {
        if(symbol == null || symbol.trim().isEmpty()) {
            return false;
        }
        for(String a : AGGREGATES) {
            if(symbol.trim().toUpperCase().startsWith(a)) {
                return true;
            }
        }
        return false;
    }

}
