package com.queryclient.af.permission;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.permission.ApiConsumerBasePermission;
import com.queryclient.ws.permission.UserBasePermission;
import com.queryclient.ws.permission.UserPasswordBasePermission;
import com.queryclient.ws.permission.ExternalUserAuthBasePermission;
import com.queryclient.ws.permission.UserAuthStateBasePermission;
import com.queryclient.ws.permission.DataServiceBasePermission;
import com.queryclient.ws.permission.ServiceEndpointBasePermission;
import com.queryclient.ws.permission.QuerySessionBasePermission;
import com.queryclient.ws.permission.QueryRecordBasePermission;
import com.queryclient.ws.permission.DummyEntityBasePermission;
import com.queryclient.ws.permission.ServiceInfoBasePermission;
import com.queryclient.ws.permission.FiveTenBasePermission;


// TBD:
public class BasePermissionManager
{
    private static final Logger log = Logger.getLogger(BasePermissionManager.class.getName());

    private ApiConsumerBasePermission apiConsumerPermission = null;
    private UserBasePermission userPermission = null;
    private UserPasswordBasePermission userPasswordPermission = null;
    private ExternalUserAuthBasePermission externalUserAuthPermission = null;
    private UserAuthStateBasePermission userAuthStatePermission = null;
    private DataServiceBasePermission dataServicePermission = null;
    private ServiceEndpointBasePermission serviceEndpointPermission = null;
    private QuerySessionBasePermission querySessionPermission = null;
    private QueryRecordBasePermission queryRecordPermission = null;
    private DummyEntityBasePermission dummyEntityPermission = null;
    private ServiceInfoBasePermission serviceInfoPermission = null;
    private FiveTenBasePermission fiveTenPermission = null;

    // Ctor.
    public BasePermissionManager()
    {
        // TBD:
    }

	public ApiConsumerBasePermission getApiConsumerPermission() 
    {
        if(apiConsumerPermission == null) {
            apiConsumerPermission = new ApiConsumerBasePermission();
        }
        return apiConsumerPermission;
    }
	public void setApiConsumerPermission(ApiConsumerBasePermission apiConsumerPermission) 
    {
        this.apiConsumerPermission = apiConsumerPermission;
    }

	public UserBasePermission getUserPermission() 
    {
        if(userPermission == null) {
            userPermission = new UserBasePermission();
        }
        return userPermission;
    }
	public void setUserPermission(UserBasePermission userPermission) 
    {
        this.userPermission = userPermission;
    }

	public UserPasswordBasePermission getUserPasswordPermission() 
    {
        if(userPasswordPermission == null) {
            userPasswordPermission = new UserPasswordBasePermission();
        }
        return userPasswordPermission;
    }
	public void setUserPasswordPermission(UserPasswordBasePermission userPasswordPermission) 
    {
        this.userPasswordPermission = userPasswordPermission;
    }

	public ExternalUserAuthBasePermission getExternalUserAuthPermission() 
    {
        if(externalUserAuthPermission == null) {
            externalUserAuthPermission = new ExternalUserAuthBasePermission();
        }
        return externalUserAuthPermission;
    }
	public void setExternalUserAuthPermission(ExternalUserAuthBasePermission externalUserAuthPermission) 
    {
        this.externalUserAuthPermission = externalUserAuthPermission;
    }

	public UserAuthStateBasePermission getUserAuthStatePermission() 
    {
        if(userAuthStatePermission == null) {
            userAuthStatePermission = new UserAuthStateBasePermission();
        }
        return userAuthStatePermission;
    }
	public void setUserAuthStatePermission(UserAuthStateBasePermission userAuthStatePermission) 
    {
        this.userAuthStatePermission = userAuthStatePermission;
    }

	public DataServiceBasePermission getDataServicePermission() 
    {
        if(dataServicePermission == null) {
            dataServicePermission = new DataServiceBasePermission();
        }
        return dataServicePermission;
    }
	public void setDataServicePermission(DataServiceBasePermission dataServicePermission) 
    {
        this.dataServicePermission = dataServicePermission;
    }

	public ServiceEndpointBasePermission getServiceEndpointPermission() 
    {
        if(serviceEndpointPermission == null) {
            serviceEndpointPermission = new ServiceEndpointBasePermission();
        }
        return serviceEndpointPermission;
    }
	public void setServiceEndpointPermission(ServiceEndpointBasePermission serviceEndpointPermission) 
    {
        this.serviceEndpointPermission = serviceEndpointPermission;
    }

	public QuerySessionBasePermission getQuerySessionPermission() 
    {
        if(querySessionPermission == null) {
            querySessionPermission = new QuerySessionBasePermission();
        }
        return querySessionPermission;
    }
	public void setQuerySessionPermission(QuerySessionBasePermission querySessionPermission) 
    {
        this.querySessionPermission = querySessionPermission;
    }

	public QueryRecordBasePermission getQueryRecordPermission() 
    {
        if(queryRecordPermission == null) {
            queryRecordPermission = new QueryRecordBasePermission();
        }
        return queryRecordPermission;
    }
	public void setQueryRecordPermission(QueryRecordBasePermission queryRecordPermission) 
    {
        this.queryRecordPermission = queryRecordPermission;
    }

	public DummyEntityBasePermission getDummyEntityPermission() 
    {
        if(dummyEntityPermission == null) {
            dummyEntityPermission = new DummyEntityBasePermission();
        }
        return dummyEntityPermission;
    }
	public void setDummyEntityPermission(DummyEntityBasePermission dummyEntityPermission) 
    {
        this.dummyEntityPermission = dummyEntityPermission;
    }

	public ServiceInfoBasePermission getServiceInfoPermission() 
    {
        if(serviceInfoPermission == null) {
            serviceInfoPermission = new ServiceInfoBasePermission();
        }
        return serviceInfoPermission;
    }
	public void setServiceInfoPermission(ServiceInfoBasePermission serviceInfoPermission) 
    {
        this.serviceInfoPermission = serviceInfoPermission;
    }

	public FiveTenBasePermission getFiveTenPermission() 
    {
        if(fiveTenPermission == null) {
            fiveTenPermission = new FiveTenBasePermission();
        }
        return fiveTenPermission;
    }
	public void setFiveTenPermission(FiveTenBasePermission fiveTenPermission) 
    {
        this.fiveTenPermission = fiveTenPermission;
    }


	public boolean isPermissionRequired(String resource, String action) 
    {
        if(resource == null || resource.isEmpty()) {
            return false;   // ???
        } else if(resource.equals("ApiConsumer")) {
            return getApiConsumerPermission().isPermissionRequired(action);
        } else if(resource.equals("User")) {
            return getUserPermission().isPermissionRequired(action);
        } else if(resource.equals("UserPassword")) {
            return getUserPasswordPermission().isPermissionRequired(action);
        } else if(resource.equals("ExternalUserAuth")) {
            return getExternalUserAuthPermission().isPermissionRequired(action);
        } else if(resource.equals("UserAuthState")) {
            return getUserAuthStatePermission().isPermissionRequired(action);
        } else if(resource.equals("DataService")) {
            return getDataServicePermission().isPermissionRequired(action);
        } else if(resource.equals("ServiceEndpoint")) {
            return getServiceEndpointPermission().isPermissionRequired(action);
        } else if(resource.equals("QuerySession")) {
            return getQuerySessionPermission().isPermissionRequired(action);
        } else if(resource.equals("QueryRecord")) {
            return getQueryRecordPermission().isPermissionRequired(action);
        } else if(resource.equals("DummyEntity")) {
            return getDummyEntityPermission().isPermissionRequired(action);
        } else if(resource.equals("ServiceInfo")) {
            return getServiceInfoPermission().isPermissionRequired(action);
        } else if(resource.equals("FiveTen")) {
            return getFiveTenPermission().isPermissionRequired(action);
        } else {
            log.warning("Unrecognized resource = " + resource);
            return true;   // ???
        }
    }

    // TBD: "instance" field is currently ignored...
	public boolean isPermissionRequired(String permissionName) 
    {
        if(permissionName == null || permissionName.isEmpty()) {
            return false;   // ???
        } else if(permissionName.startsWith("ApiConsumer::")) {
            String resource = "ApiConsumer";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("User::")) {
            String resource = "User";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserPassword::")) {
            String resource = "UserPassword";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ExternalUserAuth::")) {
            String resource = "ExternalUserAuth";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserAuthState::")) {
            String resource = "UserAuthState";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("DataService::")) {
            String resource = "DataService";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ServiceEndpoint::")) {
            String resource = "ServiceEndpoint";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("QuerySession::")) {
            String resource = "QuerySession";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("QueryRecord::")) {
            String resource = "QueryRecord";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("DummyEntity::")) {
            String resource = "DummyEntity";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ServiceInfo::")) {
            String resource = "ServiceInfo";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("FiveTen::")) {
            String resource = "FiveTen";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else {
            log.warning("Unrecognized permissionName = " + permissionName);
            return true;   // ???
        }
    }

}
