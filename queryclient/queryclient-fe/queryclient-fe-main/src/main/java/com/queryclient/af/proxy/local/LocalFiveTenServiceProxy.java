package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.FiveTen;
// import com.queryclient.ws.bean.FiveTenBean;
import com.queryclient.ws.service.FiveTenService;
import com.queryclient.af.bean.FiveTenBean;
import com.queryclient.af.proxy.FiveTenServiceProxy;


public class LocalFiveTenServiceProxy extends BaseLocalServiceProxy implements FiveTenServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalFiveTenServiceProxy.class.getName());

    public LocalFiveTenServiceProxy()
    {
    }

    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        FiveTen serverBean = getFiveTenService().getFiveTen(guid);
        FiveTen appBean = convertServerFiveTenBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        return getFiveTenService().getFiveTen(guid, field);       
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        List<FiveTen> serverBeanList = getFiveTenService().getFiveTens(guids);
        List<FiveTen> appBeanList = convertServerFiveTenBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return getAllFiveTens(null, null, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFiveTenService().getAllFiveTens(ordering, offset, count);
        return getAllFiveTens(ordering, offset, count, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<FiveTen> serverBeanList = getFiveTenService().getAllFiveTens(ordering, offset, count, forwardCursor);
        List<FiveTen> appBeanList = convertServerFiveTenBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFiveTenService().getAllFiveTenKeys(ordering, offset, count);
        return getAllFiveTenKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFiveTenService().getAllFiveTenKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFiveTenService().findFiveTens(filter, ordering, params, values, grouping, unique, offset, count);
        return findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<FiveTen> serverBeanList = getFiveTenService().findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<FiveTen> appBeanList = convertServerFiveTenBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFiveTenService().findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFiveTenService().findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getFiveTenService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException
    {
        return getFiveTenService().createFiveTen(counter, requesterIpAddress);
    }

    @Override
    public String createFiveTen(FiveTen fiveTen) throws BaseException
    {
        com.queryclient.ws.bean.FiveTenBean serverBean =  convertAppFiveTenBeanToServerBean(fiveTen);
        return getFiveTenService().createFiveTen(serverBean);
    }

    @Override
    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException
    {
        return getFiveTenService().updateFiveTen(guid, counter, requesterIpAddress);
    }

    @Override
    public Boolean updateFiveTen(FiveTen fiveTen) throws BaseException
    {
        com.queryclient.ws.bean.FiveTenBean serverBean =  convertAppFiveTenBeanToServerBean(fiveTen);
        return getFiveTenService().updateFiveTen(serverBean);
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        return getFiveTenService().deleteFiveTen(guid);
    }

    @Override
    public Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException
    {
        com.queryclient.ws.bean.FiveTenBean serverBean =  convertAppFiveTenBeanToServerBean(fiveTen);
        return getFiveTenService().deleteFiveTen(serverBean);
    }

    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        return getFiveTenService().deleteFiveTens(filter, params, values);
    }




    public static FiveTenBean convertServerFiveTenBeanToAppBean(FiveTen serverBean)
    {
        FiveTenBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new FiveTenBean();
            bean.setGuid(serverBean.getGuid());
            bean.setCounter(serverBean.getCounter());
            bean.setRequesterIpAddress(serverBean.getRequesterIpAddress());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<FiveTen> convertServerFiveTenBeanListToAppBeanList(List<FiveTen> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<FiveTen> beanList = new ArrayList<FiveTen>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(FiveTen sb : serverBeanList) {
                FiveTenBean bean = convertServerFiveTenBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.FiveTenBean convertAppFiveTenBeanToServerBean(FiveTen appBean)
    {
        com.queryclient.ws.bean.FiveTenBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.FiveTenBean();
            bean.setGuid(appBean.getGuid());
            bean.setCounter(appBean.getCounter());
            bean.setRequesterIpAddress(appBean.getRequesterIpAddress());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<FiveTen> convertAppFiveTenBeanListToServerBeanList(List<FiveTen> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<FiveTen> beanList = new ArrayList<FiveTen>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(FiveTen sb : appBeanList) {
                com.queryclient.ws.bean.FiveTenBean bean = convertAppFiveTenBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
