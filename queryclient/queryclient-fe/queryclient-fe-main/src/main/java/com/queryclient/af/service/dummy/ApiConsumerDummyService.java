package com.queryclient.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ApiConsumer;
import com.queryclient.af.config.Config;

import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.ApiConsumerService;


// The primary purpose of ApiConsumerDummyService is to fake the service api, ApiConsumerService.
// It has no real implementation.
public class ApiConsumerDummyService implements ApiConsumerService
{
    private static final Logger log = Logger.getLogger(ApiConsumerDummyService.class.getName());

    public ApiConsumerDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // ApiConsumer related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ApiConsumer getApiConsumer(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getApiConsumer(): guid = " + guid);
        return null;
    }

    @Override
    public Object getApiConsumer(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getApiConsumer(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<ApiConsumer> getApiConsumers(List<String> guids) throws BaseException
    {
        log.fine("getApiConsumers()");
        return null;
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers() throws BaseException
    {
        return getAllApiConsumers(null, null, null);
    }


    @Override
    public List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllApiConsumers(ordering, offset, count, null);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllApiConsumers(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllApiConsumerKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllApiConsumerKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findApiConsumers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ApiConsumerDummyService.findApiConsumers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ApiConsumerDummyService.findApiConsumerKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ApiConsumerDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createApiConsumer(String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        log.finer("createApiConsumer()");
        return null;
    }

    @Override
    public String createApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("createApiConsumer()");
        return null;
    }

    @Override
    public ApiConsumer constructApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("constructApiConsumer()");
        return null;
    }

    @Override
    public Boolean updateApiConsumer(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        log.finer("updateApiConsumer()");
        return null;
    }
        
    @Override
    public Boolean updateApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("updateApiConsumer()");
        return null;
    }

    @Override
    public ApiConsumer refreshApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("refreshApiConsumer()");
        return null;
    }

    @Override
    public Boolean deleteApiConsumer(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteApiConsumer(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("deleteApiConsumer()");
        return null;
    }

    // TBD
    @Override
    public Long deleteApiConsumers(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteApiConsumer(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createApiConsumers(List<ApiConsumer> apiConsumers) throws BaseException
    {
        log.finer("createApiConsumers()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateApiConsumers(List<ApiConsumer> apiConsumers) throws BaseException
    //{
    //}

}
