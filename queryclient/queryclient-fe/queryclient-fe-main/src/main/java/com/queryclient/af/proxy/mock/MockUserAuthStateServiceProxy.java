package com.queryclient.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.UserAuthStateBean;
import com.queryclient.ws.service.UserAuthStateService;
import com.queryclient.af.proxy.UserAuthStateServiceProxy;


// MockUserAuthStateServiceProxy is a decorator.
// It can be used as a base class to mock UserAuthStateServiceProxy objects.
public abstract class MockUserAuthStateServiceProxy implements UserAuthStateServiceProxy
{
    private static final Logger log = Logger.getLogger(MockUserAuthStateServiceProxy.class.getName());

    // MockUserAuthStateServiceProxy uses the decorator design pattern.
    private UserAuthStateServiceProxy decoratedProxy;

    public MockUserAuthStateServiceProxy(UserAuthStateServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected UserAuthStateServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(UserAuthStateServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public UserAuthState getUserAuthState(String guid) throws BaseException
    {
        return decoratedProxy.getUserAuthState(guid);
    }

    @Override
    public Object getUserAuthState(String guid, String field) throws BaseException
    {
        return decoratedProxy.getUserAuthState(guid, field);       
    }

    @Override
    public List<UserAuthState> getUserAuthStates(List<String> guids) throws BaseException
    {
        return decoratedProxy.getUserAuthStates(guids);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates() throws BaseException
    {
        return getAllUserAuthStates(null, null, null);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllUserAuthStates(ordering, offset, count);
        return getAllUserAuthStates(ordering, offset, count, null);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUserAuthStates(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllUserAuthStateKeys(ordering, offset, count);
        return getAllUserAuthStateKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUserAuthStateKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserAuthStates(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserAuthState(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        return decoratedProxy.createUserAuthState(managerApp, appAcl, gaeApp, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime);
    }

    @Override
    public String createUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        return decoratedProxy.createUserAuthState(userAuthState);
    }

    @Override
    public Boolean updateUserAuthState(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        return decoratedProxy.updateUserAuthState(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime);
    }

    @Override
    public Boolean updateUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        return decoratedProxy.updateUserAuthState(userAuthState);
    }

    @Override
    public Boolean deleteUserAuthState(String guid) throws BaseException
    {
        return decoratedProxy.deleteUserAuthState(guid);
    }

    @Override
    public Boolean deleteUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        String guid = userAuthState.getGuid();
        return deleteUserAuthState(guid);
    }

    @Override
    public Long deleteUserAuthStates(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteUserAuthStates(filter, params, values);
    }

}
