package com.queryclient.af.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.utils.SystemProperty;
import com.google.appengine.api.utils.SystemProperty.Environment.Value;


// TBD: Make it a singleton???
public final class DevelUtil
{
    private static final Logger log = Logger.getLogger(DevelUtil.class.getName());

    private DevelUtil() {}

    // Lazy initialization...
    // TBD: DEVEL_ENABLED and BETA_ENABLED should be realy combined into one "integer flag".
    // See comment below...
    private static Boolean IS_DEVEL = null;


    // ???
    private static void initialize()
    {
        Value val = SystemProperty.environment.value();
        if(val == SystemProperty.Environment.Value.Production) {
            IS_DEVEL = false;
        } else if(val == SystemProperty.Environment.Value.Development) {
            IS_DEVEL = true;
        } else {
            if(log.isLoggable(Level.WARNING)) log.warning("Unrecognized environment = " + val);
            IS_DEVEL = false;    // ??? false is safer, in general (because we'll be likely more conservative in prod env...)
        }
    }

    
    // Returns true if the server is running locally.
    // Note that this value cannot change once initialized
    public static boolean isRunningOnDevel()
    {
        if(IS_DEVEL == null) {
            initialize();
        }
        return IS_DEVEL;
    }


}
