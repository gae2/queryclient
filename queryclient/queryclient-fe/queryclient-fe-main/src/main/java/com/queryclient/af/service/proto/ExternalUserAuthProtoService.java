package com.queryclient.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalUserAuthBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.ExternalUserAuthService;
import com.queryclient.af.service.impl.ExternalUserAuthServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ExternalUserAuthProtoService extends ExternalUserAuthServiceImpl implements ExternalUserAuthService
{
    private static final Logger log = Logger.getLogger(ExternalUserAuthProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ExternalUserAuthProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ExternalUserAuth related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ExternalUserAuth getExternalUserAuth(String guid) throws BaseException
    {
        return super.getExternalUserAuth(guid);
    }

    @Override
    public Object getExternalUserAuth(String guid, String field) throws BaseException
    {
        return super.getExternalUserAuth(guid, field);
    }

    @Override
    public List<ExternalUserAuth> getExternalUserAuths(List<String> guids) throws BaseException
    {
        return super.getExternalUserAuths(guids);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths() throws BaseException
    {
        return super.getAllExternalUserAuths();
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllExternalUserAuths(ordering, offset, count, null);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllExternalUserAuths(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllExternalUserAuthKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllExternalUserAuthKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        return super.createExternalUserAuth(externalUserAuth);
    }

    @Override
    public ExternalUserAuth constructExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        return super.constructExternalUserAuth(externalUserAuth);
    }


    @Override
    public Boolean updateExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        return super.updateExternalUserAuth(externalUserAuth);
    }
        
    @Override
    public ExternalUserAuth refreshExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        return super.refreshExternalUserAuth(externalUserAuth);
    }

    @Override
    public Boolean deleteExternalUserAuth(String guid) throws BaseException
    {
        return super.deleteExternalUserAuth(guid);
    }

    @Override
    public Boolean deleteExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        return super.deleteExternalUserAuth(externalUserAuth);
    }

    @Override
    public Integer createExternalUserAuths(List<ExternalUserAuth> externalUserAuths) throws BaseException
    {
        return super.createExternalUserAuths(externalUserAuths);
    }

    // TBD
    //@Override
    //public Boolean updateExternalUserAuths(List<ExternalUserAuth> externalUserAuths) throws BaseException
    //{
    //}

}
