package com.queryclient.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.app.service.UserAppService;
import com.queryclient.app.service.QueryRecordAppService;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.QueryRecord;


public class QueryRecordHelper
{
    private static final Logger log = Logger.getLogger(QueryRecordHelper.class.getName());

    private QueryRecordHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private QueryRecordAppService queryRecordService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private QueryRecordAppService getQueryRecordService()
    {
        if(queryRecordService == null) {
            queryRecordService = new QueryRecordAppService();
        }
        return queryRecordService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class QueryRecordHelperHolder
    {
        private static final QueryRecordHelper INSTANCE = new QueryRecordHelper();
    }

    // Singleton method
    public static QueryRecordHelper getInstance()
    {
        return QueryRecordHelperHolder.INSTANCE;
    }

    
    public QueryRecord getQueryRecord(String guid) 
    {
        QueryRecord message = null;
        try {
            message = getQueryRecordService().getQueryRecord(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
 
    // TBD:
    
    public List<QueryRecord> findQueryRecordsForUser(String userGuid)
    {
        return findQueryRecordsForUser(userGuid, null, null);
    }

    // TBD: sorting/ordering????
    public List<QueryRecord> findQueryRecordsForUser(String userGuid, Long offset, Integer count)
    {
        List<QueryRecord> beans = null;
        
        try {
            String filter = "user=='" + userGuid + "'";   // Status?
            String ordering = "createdTime desc";
            beans = getQueryRecordService().findQueryRecords(filter, ordering, null, null, null, null, offset, count);    
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find queryRecords for given user = " + userGuid, e);
        }

        return beans;
    }

    
    

}
