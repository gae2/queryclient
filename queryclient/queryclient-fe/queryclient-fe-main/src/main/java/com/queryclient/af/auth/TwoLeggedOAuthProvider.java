package com.queryclient.af.auth;

import java.net.URI;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.sun.jersey.api.core.HttpRequestContext;
import com.sun.jersey.oauth.server.OAuthServerRequest;
import com.sun.jersey.oauth.signature.OAuthParameters;
import com.sun.jersey.oauth.signature.OAuthSecrets;
import com.sun.jersey.oauth.signature.OAuthSignature;
import com.sun.jersey.oauth.signature.OAuthSignatureException;

import org.miniauth.MiniAuthException;
import org.miniauth.credential.AccessCredential;
import org.miniauth.oauth.common.OAuthIncomingRequest;
import org.miniauth.oauth.common.OAuthIncomingRequestBuilder;
import org.miniauth.oauth.common.OAuthParamMap;
import org.miniauth.oauth.credential.OAuthAccessCredential;
import org.miniauth.oauth.service.OAuthRequestVerifier;
import org.miniauth.util.ParamMapUtil;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.UnauthorizedException;
import com.queryclient.af.resource.registry.OAuthConsumerRegistry;


public class TwoLeggedOAuthProvider
{
    private static final Logger log = Logger.getLogger(TwoLeggedOAuthProvider.class.getName());

//    // Consumer key-secret map.
//    private Map<String, String> consumerSecretMap;
    
    private TwoLeggedOAuthProvider()
    {
//        // TBD: Populate this from data store ? 
//        consumerSecretMap = new HashMap<String, String>();
//        consumerSecretMap.put("6d7f9ecc-3bfb-4b0d-9145-c240ce75e9b9", "2d35d20e-a784-4c9c-8b21-b43bc7753ed6");
//        // ...
    }

    private static class TwoLeggedOAuthProviderHolder {
        private static TwoLeggedOAuthProvider INSTANCE = new TwoLeggedOAuthProvider();
    };

    public static TwoLeggedOAuthProvider getInstance()
    {
        return TwoLeggedOAuthProviderHolder.INSTANCE;
    }
    

    public boolean verify(HttpRequestContext containerRequest) throws BaseException
    {
        OAuthServerRequest request = new OAuthServerRequest(containerRequest);

        OAuthParameters params = new OAuthParameters();
        params.readRequest(request);
       
        // Check that the timestamp has not expired
        String timestampStr = params.getTimestamp();
        // TBD: timestamp checking code ...
        String consumerKey = params.getConsumerKey();
        // TBD: get consumerSecret for the given consumerKey...
//        String consumerSecret = consumerSecretMap.get(consumerKey);
        String consumerSecret = OAuthConsumerRegistry.getInstance().getConsumerSecret(consumerKey);

        // Set the secret(s), against which we will verify the request
        OAuthSecrets secrets = new OAuthSecrets().consumerSecret(consumerSecret);

        // Verify the signature
        try {
            if(!OAuthSignature.verify(request, params, secrets)) {
                log.warning("Two-legged OAuth verification failed.");
                // throw new UnauthorizedException("Two-legged OAuth verification failed.");
                return false;
            }
        } catch (OAuthSignatureException e) {
            log.log(Level.WARNING, "OAuth signature error.", e);
            throw new BadRequestException("OAuth signature error.", e);  // ???
        } catch (Exception e) {
            log.log(Level.WARNING, "Two-legged OAuth verification failed due to unknown error.", e);
            throw new BaseException("Two-legged OAuth verification failed due to unknown error.", e);  // ???
        }

        return true;
    }

    // TBD
    public boolean verify2(HttpRequestContext containerRequest) throws BaseException
    {
        boolean verified = false;
        try {
            String httpMethod = containerRequest.getMethod();
            URI baseUri = containerRequest.getBaseUri();
            String authHeaderStr = containerRequest.getHeaderValue("Authorization");
            Map<String, List<String>> queryMap = containerRequest.getQueryParameters();
            Map<String,String[]> queryParams = ParamMapUtil.convertStringListMapToStringArrayMap(queryMap);

            OAuthIncomingRequestBuilder oauthIncomingRequestBuilder = new OAuthIncomingRequestBuilder();
            OAuthIncomingRequest incomingRequest = oauthIncomingRequestBuilder.init().setHttpMethod(httpMethod).setBaseURI(baseUri).setAuthHeader(authHeaderStr).setQueryParams(queryParams).build();

            OAuthParamMap oauthParamMap = incomingRequest.getOAuthParamMap();
            // Check that the timestamp has not expired
            int timestamp = oauthParamMap.getTimestamp();
            // TBD: timestamp checking code ...
            String consumerKey = oauthParamMap.getConsumerKey();
            // TBD: get consumerSecret for the given consumerKey...
            String consumerSecret = OAuthConsumerRegistry.getInstance().getConsumerSecret(consumerKey);
            AccessCredential accessCredential = new OAuthAccessCredential(consumerSecret, null);

            verified = OAuthRequestVerifier.getInstance().verify(accessCredential, incomingRequest);
        } catch (MiniAuthException e) {
            log.log(Level.WARNING, "OAuth signature error.", e);
            throw new BadRequestException("OAuth signature error.", e);  // ???
        } catch (Exception e) {
            log.log(Level.WARNING, "Two-legged OAuth verification failed due to unknown error.", e);
            throw new BaseException("Two-legged OAuth verification failed due to unknown error.", e);  // ???
        }
        return verified;
    }

}
