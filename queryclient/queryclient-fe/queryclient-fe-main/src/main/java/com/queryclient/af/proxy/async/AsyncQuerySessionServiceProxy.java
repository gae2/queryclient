package com.queryclient.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.ws.stub.ReferrerInfoStructListStub;
import com.queryclient.ws.stub.QuerySessionStub;
import com.queryclient.ws.stub.QuerySessionListStub;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.ws.service.QuerySessionService;
import com.queryclient.af.proxy.QuerySessionServiceProxy;
import com.queryclient.af.proxy.remote.RemoteQuerySessionServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncQuerySessionServiceProxy extends BaseAsyncServiceProxy implements QuerySessionServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncQuerySessionServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteQuerySessionServiceProxy remoteProxy;

    public AsyncQuerySessionServiceProxy()
    {
        remoteProxy = new RemoteQuerySessionServiceProxy();
    }

    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        return remoteProxy.getQuerySession(guid);
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        return remoteProxy.getQuerySession(guid, field);       
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        return remoteProxy.getQuerySessions(guids);
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return getAllQuerySessions(null, null, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllQuerySessions(ordering, offset, count);
        return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllQuerySessions(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllQuerySessionKeys(ordering, offset, count);
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count);
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        QuerySessionBean bean = new QuerySessionBean(null, user, dataService, serviceUrl, inputFormat, outputFormat, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), status, note);
        return createQuerySession(bean);        
    }

    @Override
    public String createQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");

        String guid = querySession.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((QuerySessionBean) querySession).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateQuerySession-" + guid;
        String taskName = "RsCreateQuerySession-" + guid + "-" + (new Date()).getTime();
        QuerySessionStub stub = MarshalHelper.convertQuerySessionToStub(querySession);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(QuerySessionStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = querySession.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    QuerySessionStub dummyStub = new QuerySessionStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createQuerySession(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "querySessions/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createQuerySession(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "querySessions/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "QuerySession guid is invalid.");
        	throw new BaseException("QuerySession guid is invalid.");
        }
        QuerySessionBean bean = new QuerySessionBean(guid, user, dataService, serviceUrl, inputFormat, outputFormat, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), status, note);
        return updateQuerySession(bean);        
    }

    @Override
    public Boolean updateQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");

        String guid = querySession.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "QuerySession object is invalid.");
        	throw new BaseException("QuerySession object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateQuerySession-" + guid;
        String taskName = "RsUpdateQuerySession-" + guid + "-" + (new Date()).getTime();
        QuerySessionStub stub = MarshalHelper.convertQuerySessionToStub(querySession);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(QuerySessionStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = querySession.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    QuerySessionStub dummyStub = new QuerySessionStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateQuerySession(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "querySessions/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateQuerySession(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "querySessions/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteQuerySession-" + guid;
        String taskName = "RsDeleteQuerySession-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "querySessions/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteQuerySession(QuerySession querySession) throws BaseException
    {
        String guid = querySession.getGuid();
        return deleteQuerySession(guid);
    }

    @Override
    public Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteQuerySessions(filter, params, values);
    }

}
