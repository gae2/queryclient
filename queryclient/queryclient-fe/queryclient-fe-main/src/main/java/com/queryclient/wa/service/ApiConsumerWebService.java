package com.queryclient.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ApiConsumer;
import com.queryclient.af.bean.ApiConsumerBean;
import com.queryclient.af.service.ApiConsumerService;
import com.queryclient.af.service.manager.ServiceManager;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ApiConsumerJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ApiConsumerWebService // implements ApiConsumerService
{
    private static final Logger log = Logger.getLogger(ApiConsumerWebService.class.getName());
     
    // Af service interface.
    private ApiConsumerService mService = null;

    public ApiConsumerWebService()
    {
        this(ServiceManager.getApiConsumerService());
    }
    public ApiConsumerWebService(ApiConsumerService service)
    {
        mService = service;
    }
    
    private ApiConsumerService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getApiConsumerService();
        }
        return mService;
    }
    
    
    public ApiConsumerJsBean getApiConsumer(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            ApiConsumer apiConsumer = getService().getApiConsumer(guid);
            ApiConsumerJsBean bean = convertApiConsumerToJsBean(apiConsumer);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getApiConsumer(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getApiConsumer(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ApiConsumerJsBean> getApiConsumers(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ApiConsumerJsBean> jsBeans = new ArrayList<ApiConsumerJsBean>();
            List<ApiConsumer> apiConsumers = getService().getApiConsumers(guids);
            if(apiConsumers != null) {
                for(ApiConsumer apiConsumer : apiConsumers) {
                    jsBeans.add(convertApiConsumerToJsBean(apiConsumer));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ApiConsumerJsBean> getAllApiConsumers() throws WebException
    {
        return getAllApiConsumers(null, null, null);
    }

    // @Deprecated
    public List<ApiConsumerJsBean> getAllApiConsumers(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllApiConsumers(ordering, offset, count, null);
    }

    public List<ApiConsumerJsBean> getAllApiConsumers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ApiConsumerJsBean> jsBeans = new ArrayList<ApiConsumerJsBean>();
            List<ApiConsumer> apiConsumers = getService().getAllApiConsumers(ordering, offset, count, forwardCursor);
            if(apiConsumers != null) {
                for(ApiConsumer apiConsumer : apiConsumers) {
                    jsBeans.add(convertApiConsumerToJsBean(apiConsumer));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllApiConsumerKeys(ordering, offset, count, null);
    }

    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllApiConsumerKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<ApiConsumerJsBean> findApiConsumers(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findApiConsumers(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<ApiConsumerJsBean> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<ApiConsumerJsBean> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ApiConsumerJsBean> jsBeans = new ArrayList<ApiConsumerJsBean>();
            List<ApiConsumer> apiConsumers = getService().findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(apiConsumers != null) {
                for(ApiConsumer apiConsumer : apiConsumers) {
                    jsBeans.add(convertApiConsumerToJsBean(apiConsumer));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createApiConsumer(String aeryId, String name, String description, String appKey, String appSecret, String status) throws WebException
    {
        try {
            return getService().createApiConsumer(aeryId, name, description, appKey, appSecret, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createApiConsumer(String jsonStr) throws WebException
    {
        return createApiConsumer(ApiConsumerJsBean.fromJsonString(jsonStr));
    }

    public String createApiConsumer(ApiConsumerJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ApiConsumer apiConsumer = convertApiConsumerJsBeanToBean(jsBean);
            return getService().createApiConsumer(apiConsumer);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ApiConsumerJsBean constructApiConsumer(String jsonStr) throws WebException
    {
        return constructApiConsumer(ApiConsumerJsBean.fromJsonString(jsonStr));
    }

    public ApiConsumerJsBean constructApiConsumer(ApiConsumerJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ApiConsumer apiConsumer = convertApiConsumerJsBeanToBean(jsBean);
            apiConsumer = getService().constructApiConsumer(apiConsumer);
            jsBean = convertApiConsumerToJsBean(apiConsumer);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateApiConsumer(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status) throws WebException
    {
        try {
            return getService().updateApiConsumer(guid, aeryId, name, description, appKey, appSecret, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateApiConsumer(String jsonStr) throws WebException
    {
        return updateApiConsumer(ApiConsumerJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateApiConsumer(ApiConsumerJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ApiConsumer apiConsumer = convertApiConsumerJsBeanToBean(jsBean);
            return getService().updateApiConsumer(apiConsumer);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ApiConsumerJsBean refreshApiConsumer(String jsonStr) throws WebException
    {
        return refreshApiConsumer(ApiConsumerJsBean.fromJsonString(jsonStr));
    }

    public ApiConsumerJsBean refreshApiConsumer(ApiConsumerJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ApiConsumer apiConsumer = convertApiConsumerJsBeanToBean(jsBean);
            apiConsumer = getService().refreshApiConsumer(apiConsumer);
            jsBean = convertApiConsumerToJsBean(apiConsumer);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteApiConsumer(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteApiConsumer(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteApiConsumer(ApiConsumerJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ApiConsumer apiConsumer = convertApiConsumerJsBeanToBean(jsBean);
            return getService().deleteApiConsumer(apiConsumer);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteApiConsumers(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteApiConsumers(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static ApiConsumerJsBean convertApiConsumerToJsBean(ApiConsumer apiConsumer)
    {
        ApiConsumerJsBean jsBean = null;
        if(apiConsumer != null) {
            jsBean = new ApiConsumerJsBean();
            jsBean.setGuid(apiConsumer.getGuid());
            jsBean.setAeryId(apiConsumer.getAeryId());
            jsBean.setName(apiConsumer.getName());
            jsBean.setDescription(apiConsumer.getDescription());
            jsBean.setAppKey(apiConsumer.getAppKey());
            jsBean.setAppSecret(apiConsumer.getAppSecret());
            jsBean.setStatus(apiConsumer.getStatus());
            jsBean.setCreatedTime(apiConsumer.getCreatedTime());
            jsBean.setModifiedTime(apiConsumer.getModifiedTime());
        }
        return jsBean;
    }

    public static ApiConsumer convertApiConsumerJsBeanToBean(ApiConsumerJsBean jsBean)
    {
        ApiConsumerBean apiConsumer = null;
        if(jsBean != null) {
            apiConsumer = new ApiConsumerBean();
            apiConsumer.setGuid(jsBean.getGuid());
            apiConsumer.setAeryId(jsBean.getAeryId());
            apiConsumer.setName(jsBean.getName());
            apiConsumer.setDescription(jsBean.getDescription());
            apiConsumer.setAppKey(jsBean.getAppKey());
            apiConsumer.setAppSecret(jsBean.getAppSecret());
            apiConsumer.setStatus(jsBean.getStatus());
            apiConsumer.setCreatedTime(jsBean.getCreatedTime());
            apiConsumer.setModifiedTime(jsBean.getModifiedTime());
        }
        return apiConsumer;
    }

}
