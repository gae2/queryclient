package com.queryclient.app.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import com.queryclient.ws.BaseException;
import com.queryclient.helper.UrlHelper;


public class QueryUtil
{
    private static final Logger log = Logger.getLogger(QueryUtil.class.getName());

    public QueryUtil() {}


    public static QueryResultStruct processWebServiceQuery(String queryGuid, URL requestUrl, String httpMethod,
            Map<String, String> httpHeaders, String payload) throws BaseException
    {
        log.info("processWebServiceQuery(): queryGuid = " + queryGuid);
        
        int responseCode = 0;   // Failure, by default.
        String result = "Failed-";
        String output = null;
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) requestUrl.openConnection();
            
            // ???
            // Does this owrk on GAE?????
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(45000);
            // ???
            
            
            // temporary
            // TBD: Do this only if the service is oauth enabled????
//            String CONSUMER_KEY = "3ddb8e6c-ec74-46e5-9a8c-e4bc5afd794c";
//            String CONSUMER_SECRET = "5a2133b5-3456-4400-be22-7297597f6598";
//            OAuthConsumer consumer = new DefaultOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);

            // TBD:
            String requestUrlStr = requestUrl.toString();
            String service = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrlStr);
            log.info(">>>>>>>> service = " + service);
            // Hack
            if(! service.endsWith("/v1")) {
                if(service.endsWith("/")) {
                    service += "v1";
                } else {
                    service += "/v1";
                }
            }
            // Hack
            log.info(">>>>>>>> service = " + service);


            OAuthConsumer consumer = SignpostClientManager.getInstance().getConsumer(service);
            // temporary
            
            
            connection.setRequestMethod(httpMethod);
            //connection.setDoOutput(true);     // ???
            connection.setDoInput(true);
            
            if(httpHeaders != null) {
                if(httpHeaders.containsKey("Accept")) {
                    connection.setRequestProperty("Accept", httpHeaders.get("Accept"));                    
                }
                // else ???
                if(httpHeaders.containsKey("Content-Type")) {
                    connection.setRequestProperty("Content-Type", httpHeaders.get("Content-Type"));                    
                }
                // else ???
            }
            // else ???

            if(httpMethod.equals("GET")) {
                // ...
            } else if(httpMethod.equals("POST")) {
                connection.setDoOutput(true);
            } else if(httpMethod.equals("PUT")) {
                connection.setDoOutput(true);
            } else if(httpMethod.equals("DELETE")) {
                // ...
            } else {
                // This cannot happen.
                log.warning("Unknown httpMethod = " + httpMethod);
                throw new BaseException("Unknown httpMethod = " + httpMethod);
            }

            
            // temporary
            // TBD: Do this only if the service is oauth enabled????
            // Note: request should be signed after all headers have been written... ???
            // TBD: What about the payload???
            consumer.sign(connection);
            // temporary

            
            if(httpMethod.equals("POST") || httpMethod.equals("PUT")) {
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                if(payload != null) {
                    writer.write(payload);
                }
                writer.flush();
                writer.close();
            }
            
            responseCode = connection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK) {
                result = "Succeeded-";
                // Parse output ...
                BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuffer sb = new StringBuffer();
                String line = null;
                while ((line = rd.readLine()) != null)
                {
                    sb.append(line + '\n');
                }
                output = sb.toString();
                log.info("Output = " + output);
                
                // TBD:
                // "return" output...
                // ...
                
            } else if(responseCode == HttpURLConnection.HTTP_ACCEPTED) {
                result = "Succeeded-";
                // Output ???
            } else {
                // Server returned HTTP error code.
                // TBD: retry ??
                log.log(Level.WARNING, "Error processing the task. responseCode = " + responseCode);
            }
        } catch (IOException e) {
            log.log(Level.WARNING, "Error processing the task", e);
            throw new BaseException("Error processing the task", e);
        } catch (OAuthMessageSignerException e) {
            log.log(Level.WARNING, "Oauth exception while processing the task", e);
            throw new BaseException("Oauth exception while processing the task", e);
        } catch (OAuthExpectationFailedException e) {
            log.log(Level.WARNING, "Oauth exception while processing the task", e);
            throw new BaseException("Oauth exception while processing the task", e);
        } catch (OAuthCommunicationException e) {
            log.log(Level.WARNING, "Oauth exception while processing the task", e);
            throw new BaseException("Oauth exception while processing the task", e);
        } finally {
            connection = null;
        }
        log.info("processWebServiceQuery(): responseCode = " + responseCode);
        
        QueryResultStruct res = new QueryResultStruct();
        res.setResponseCode(responseCode);
        res.setResult(result);
        res.setOutput(output);

        return res;
    }

}
