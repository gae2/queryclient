package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.DummyEntity;
// import com.queryclient.ws.bean.DummyEntityBean;
import com.queryclient.ws.service.DummyEntityService;
import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.proxy.DummyEntityServiceProxy;


public class LocalDummyEntityServiceProxy extends BaseLocalServiceProxy implements DummyEntityServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalDummyEntityServiceProxy.class.getName());

    public LocalDummyEntityServiceProxy()
    {
    }

    @Override
    public DummyEntity getDummyEntity(String guid) throws BaseException
    {
        DummyEntity serverBean = getDummyEntityService().getDummyEntity(guid);
        DummyEntity appBean = convertServerDummyEntityBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getDummyEntity(String guid, String field) throws BaseException
    {
        return getDummyEntityService().getDummyEntity(guid, field);       
    }

    @Override
    public List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException
    {
        List<DummyEntity> serverBeanList = getDummyEntityService().getDummyEntities(guids);
        List<DummyEntity> appBeanList = convertServerDummyEntityBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<DummyEntity> getAllDummyEntities() throws BaseException
    {
        return getAllDummyEntities(null, null, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getDummyEntityService().getAllDummyEntities(ordering, offset, count);
        return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<DummyEntity> serverBeanList = getDummyEntityService().getAllDummyEntities(ordering, offset, count, forwardCursor);
        List<DummyEntity> appBeanList = convertServerDummyEntityBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getDummyEntityService().getAllDummyEntityKeys(ordering, offset, count);
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDummyEntityService().getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getDummyEntityService().findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count);
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<DummyEntity> serverBeanList = getDummyEntityService().findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<DummyEntity> appBeanList = convertServerDummyEntityBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getDummyEntityService().findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDummyEntityService().findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getDummyEntityService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        return getDummyEntityService().createDummyEntity(user, name, content, maxLength, expired, status);
    }

    @Override
    public String createDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        com.queryclient.ws.bean.DummyEntityBean serverBean =  convertAppDummyEntityBeanToServerBean(dummyEntity);
        return getDummyEntityService().createDummyEntity(serverBean);
    }

    @Override
    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        return getDummyEntityService().updateDummyEntity(guid, user, name, content, maxLength, expired, status);
    }

    @Override
    public Boolean updateDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        com.queryclient.ws.bean.DummyEntityBean serverBean =  convertAppDummyEntityBeanToServerBean(dummyEntity);
        return getDummyEntityService().updateDummyEntity(serverBean);
    }

    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        return getDummyEntityService().deleteDummyEntity(guid);
    }

    @Override
    public Boolean deleteDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        com.queryclient.ws.bean.DummyEntityBean serverBean =  convertAppDummyEntityBeanToServerBean(dummyEntity);
        return getDummyEntityService().deleteDummyEntity(serverBean);
    }

    @Override
    public Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException
    {
        return getDummyEntityService().deleteDummyEntities(filter, params, values);
    }




    public static DummyEntityBean convertServerDummyEntityBeanToAppBean(DummyEntity serverBean)
    {
        DummyEntityBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new DummyEntityBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setName(serverBean.getName());
            bean.setContent(serverBean.getContent());
            bean.setMaxLength(serverBean.getMaxLength());
            bean.setExpired(serverBean.isExpired());
            bean.setStatus(serverBean.getStatus());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<DummyEntity> convertServerDummyEntityBeanListToAppBeanList(List<DummyEntity> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<DummyEntity> beanList = new ArrayList<DummyEntity>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(DummyEntity sb : serverBeanList) {
                DummyEntityBean bean = convertServerDummyEntityBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.DummyEntityBean convertAppDummyEntityBeanToServerBean(DummyEntity appBean)
    {
        com.queryclient.ws.bean.DummyEntityBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.DummyEntityBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setName(appBean.getName());
            bean.setContent(appBean.getContent());
            bean.setMaxLength(appBean.getMaxLength());
            bean.setExpired(appBean.isExpired());
            bean.setStatus(appBean.getStatus());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<DummyEntity> convertAppDummyEntityBeanListToServerBeanList(List<DummyEntity> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<DummyEntity> beanList = new ArrayList<DummyEntity>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(DummyEntity sb : appBeanList) {
                com.queryclient.ws.bean.DummyEntityBean bean = convertAppDummyEntityBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
