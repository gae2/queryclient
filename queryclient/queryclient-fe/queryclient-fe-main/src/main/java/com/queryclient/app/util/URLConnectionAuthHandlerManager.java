package com.queryclient.app.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniauth.credential.AccessCredential;
import org.miniauth.exception.CredentialStoreException;
import org.miniauth.oauth.credential.mapper.OAuthLocalConsumerCredentialMapper;
import org.miniauth.oauth.credential.mapper.OAuthSingleConsumerCredentialMapper;
import org.miniauth.web.oauth.OAuthConsumerURLConnectionAuthHandler;
import org.miniauth.web.oauth.OAuthSingleConsumerURLConnectionAuthHandler;

import com.queryclient.app.endpoint.TargetServiceManager;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.exception.InternalServerErrorException;


/**
 * URLConnectionAuthHandler "registry"/cache.
 */
public final class URLConnectionAuthHandlerManager
{
    private static final Logger log = Logger.getLogger(URLConnectionAuthHandlerManager.class.getName());
    
    // TBD:
    // We can use either a single OAuthConsumerURLConnectionAuthHandler (with OAuthLocalConsumerCredentialMapper)
    // or we can use a map of <String, OAuthSinglCosnumerURLConnectionAuthHandler> (with each URLConnectionAuthHandler associated with a OAuthSingleConsumerCredentialMapper).
    // ....

    // temporary
    private Map<String, OAuthSingleConsumerURLConnectionAuthHandler> endorserMapCache = null;

    private OAuthConsumerURLConnectionAuthHandler urlConnectionAuthHandler = null;
    private OAuthLocalConsumerCredentialMapper credentialMapper = null;
    
    private URLConnectionAuthHandlerManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static final class URLConnectionAuthHandlerHolder
    {
        private static final URLConnectionAuthHandlerManager INSTANCE = new URLConnectionAuthHandlerManager();
    }
    // Singleton method
    public static URLConnectionAuthHandlerManager getInstance()
    {
        return URLConnectionAuthHandlerHolder.INSTANCE;
    }

    
    private void init()
    {
        // temporary
        endorserMapCache = new HashMap<>();

        credentialMapper = OAuthLocalConsumerCredentialMapper.getInstance();
        urlConnectionAuthHandler = new OAuthConsumerURLConnectionAuthHandler(credentialMapper);
        // ...
    }

    
    // TBD: ...
    // Which implementation is better???
    public OAuthSingleConsumerURLConnectionAuthHandler getSingleConsumerURLConnectionAuthHandler(String service) throws BaseException
    {
        OAuthSingleConsumerURLConnectionAuthHandler endorser = null;
        if(endorserMapCache.containsKey(service)) {
            endorser = endorserMapCache.get(service);
        } else {
            // temporary
            ConsumerKeySecretPair pair = TargetServiceManager.getInstance().getKeySecretPair(service);
            log.warning("######### pair = " + pair);

            if(pair != null) {
                String consumerKey = pair.getConsumerKey();
                String consumerSecret = pair.getConsumerSecret();
                OAuthSingleConsumerCredentialMapper mapper = new OAuthSingleConsumerCredentialMapper(consumerKey, consumerSecret);
                endorser = new OAuthSingleConsumerURLConnectionAuthHandler(mapper);
                endorserMapCache.put(service, endorser);
            } else {
                log.log(Level.WARNING, "Failed to create an URLConnectionAuthHandler for service = " + service);
                throw new BaseException("Failed to create an URLConnectionAuthHandler for service = " + service);
            }
        }
        return endorser;
    }

    public OAuthConsumerURLConnectionAuthHandler getConsumerURLConnectionAuthHandler(String service) throws BaseException
    {
        // temporary
        ConsumerKeySecretPair pair = TargetServiceManager.getInstance().getKeySecretPair(service);
        log.warning("######### pair = " + pair);

        if(pair != null) {
            String consumerKey = pair.getConsumerKey();
            String consumerSecret = pair.getConsumerSecret();
            try {
                AccessCredential accessCredential = credentialMapper.getAccesssCredential(consumerKey);
                log.warning("######### accessCredential = " + accessCredential);

                // Updated!!!!
                if(accessCredential == null ||
                    (accessCredential.getConsumerSecret() == null || accessCredential.getConsumerSecret().isEmpty())
                    ) {
                    credentialMapper.putConsumerSecret(consumerKey, consumerSecret);
                } else {
                    // URLConnectionAuthHandler already includes the consumer key/secret pair for the given service.
                }
            } catch (CredentialStoreException e) {
                throw new InternalServerErrorException("Error while adding consumer key/secret for service = " + service, e);
            }
        } else {
            log.log(Level.WARNING, "Failed to get an URLConnectionAuthHandler for service = " + service);
            throw new BaseException("Failed to get an URLConnectionAuthHandler for service = " + service);
        }
        return urlConnectionAuthHandler;
    }

}
