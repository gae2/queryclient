package com.queryclient.af.auth.googleplus;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


@JsonIgnoreProperties(ignoreUnknown = true)
public class GooglePlusSignInStruct implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GooglePlusSignInStruct.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String dataScope;
    private String dataClientId;
    private String dataRedirectUri;
    private String dataAccessType;
    private String dataCookiePolicy;
    private String dataCallback;
    private String dataApprovalPrompt;
    private Map<String, String> extraAttrMap;


    public GooglePlusSignInStruct()
    {
        this(null, null, null, null, null, null, null);
    }
    public GooglePlusSignInStruct(String dataScope, String dataClientId,
            String dataRedirectUri, String dataAccessType,
            String dataCookiePolicy, String dataCallback,
            String dataApprovalPrompt)
    {
        this(dataScope, dataClientId, dataRedirectUri, dataAccessType, dataCookiePolicy, dataCallback, dataApprovalPrompt, null);
    }
    public GooglePlusSignInStruct(String dataScope, String dataClientId,
            String dataRedirectUri, String dataAccessType,
            String dataCookiePolicy, String dataCallback,
            String dataApprovalPrompt, Map<String, String> extraAttrMap)
    {
        super();
        this.dataScope = dataScope;
        this.dataClientId = dataClientId;
        this.dataRedirectUri = dataRedirectUri;
        this.dataAccessType = dataAccessType;
        this.dataCookiePolicy = dataCookiePolicy;
        this.dataCallback = dataCallback;
        this.dataApprovalPrompt = dataApprovalPrompt;
        this.extraAttrMap = extraAttrMap;
        if(this.extraAttrMap == null) {
            this.extraAttrMap = new HashMap<>();
        }
    }


    public String getDataScope()
    {
        return dataScope;
    }
    public void setDataScope(String dataScope)
    {
        this.dataScope = dataScope;
    }

    public String getDataClientId()
    {
        return dataClientId;
    }
    public void setDataClientId(String dataClientId)
    {
        this.dataClientId = dataClientId;
    }

    public String getDataRedirectUri()
    {
        return dataRedirectUri;
    }
    public void setDataRedirectUri(String dataRedirectUri)
    {
        this.dataRedirectUri = dataRedirectUri;
    }

    public String getDataAccessType()
    {
        return dataAccessType;
    }
    public void setDataAccessType(String dataAccessType)
    {
        this.dataAccessType = dataAccessType;
    }

    public String getDataCookiePolicy()
    {
        return dataCookiePolicy;
    }
    public void setDataCookiePolicy(String dataCookiePolicy)
    {
        this.dataCookiePolicy = dataCookiePolicy;
    }

    public String getDataCallback()
    {
        return dataCallback;
    }
    public void setDataCallback(String dataCallback)
    {
        this.dataCallback = dataCallback;
    }

    public String getDataApprovalPrompt()
    {
        return dataApprovalPrompt;
    }
    public void setDataApprovalPrompt(String dataApprovalPrompt)
    {
        this.dataApprovalPrompt = dataApprovalPrompt;
    }


    public Map<String, String> getExtraAttrMap()
    {
        return extraAttrMap;
    }
    public void setExtraAttrMap(Map<String, String> extraAttrMap)
    {
        this.extraAttrMap = extraAttrMap;
        if(this.extraAttrMap == null) {
            this.extraAttrMap = new HashMap<>();
        }
    }
    public void clearExtraAttrMap()
    {
        if(extraAttrMap == null) {
            extraAttrMap = new HashMap<>();
        } else {
            extraAttrMap.clear();
        }
    }
    // Note that the key should not be one the attribute keys that are explicitly included here.
    // TBD: Need to check this, if key is one of them, use the corresponding fields...
    public void addExtraAttrPair(String key, String value)
    {
        if(extraAttrMap == null) {
            extraAttrMap = new HashMap<>();
        }
        extraAttrMap.put(key, value);
    }


    // Temporary
    public String buildSignInButtonHtml()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("<div id=\"signinButton\">");
        sb.append("<span class=\"g-signin\"");
        if(dataScope != null && !dataScope.isEmpty()) {
            sb.append(" data-scope=\"");
            sb.append(dataScope);         // Need to escape the string in some way???
            sb.append("\"");
        }
        if(dataClientId != null && !dataClientId.isEmpty()) {
            sb.append(" data-clientid=\"");
            sb.append(dataClientId);         // Need to escape the string in some way???
            sb.append("\"");
        }
        if(dataRedirectUri != null && !dataRedirectUri.isEmpty()) {
            sb.append(" data-redirecturi=\"");
            sb.append(dataRedirectUri);         // Need to escape the string in some way???
            sb.append("\"");
        }
        if(dataAccessType != null && !dataAccessType.isEmpty()) {
            sb.append(" data-accesstype=\"");
            sb.append(dataAccessType);         // Need to escape the string in some way???
            sb.append("\"");
        }
        if(dataCookiePolicy != null && !dataCookiePolicy.isEmpty()) {
            sb.append(" data-cookiepolicy=\"");
            sb.append(dataCookiePolicy);         // Need to escape the string in some way???
            sb.append("\"");
        }
        if(dataCallback != null && !dataCallback.isEmpty()) {
            sb.append(" data-callback=\"");
            sb.append(dataCallback);         // Need to escape the string in some way???
            sb.append("\"");
        }
        if(dataApprovalPrompt != null && !dataApprovalPrompt.isEmpty()) {
            sb.append(" data-approvalprompt=\"");
            sb.append(dataApprovalPrompt);         // Need to escape the string in some way???
            sb.append("\"");
        }
        if(extraAttrMap != null && !extraAttrMap.isEmpty()) {
            for(String key : extraAttrMap.keySet()) {
                String value = extraAttrMap.get(key);
                if(value != null) {   // Could be an empty string.
                    sb.append(" " + key + "=\"");
                    sb.append(value);
                    sb.append("\"");                    
                } else {
                    // ???
                    sb.append(" " + key);
                }
            }
        }
        sb.append("></span>");
        sb.append("</div>");
        // sb.append("<div id=\"result\"></div>");
        
        String html =  sb.toString();
        if(log.isLoggable(Level.FINE)) log.fine("HTML returned = " + html);
        return html;
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        if(log.isLoggable(Level.FINE)) log.fine("jsonStr = " + jsonStr);
        return jsonStr;
    }

    @Override
    public String toString()
    {
        return "GooglePlusSignInStruct [dataScope=" + dataScope
                + ", dataClientId=" + dataClientId + ", dataRedirectUri="
                + dataRedirectUri + ", dataAccessType=" + dataAccessType
                + ", dataCookiePolicy=" + dataCookiePolicy + ", dataCallback="
                + dataCallback + ", dataApprovalPrompt=" + dataApprovalPrompt
                + ", extraAttrMap=" + extraAttrMap + "]";
    }
    
}
