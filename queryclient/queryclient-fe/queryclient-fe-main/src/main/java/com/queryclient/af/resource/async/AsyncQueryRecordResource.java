package com.queryclient.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.stub.QueryRecordStub;
import com.queryclient.ws.stub.QueryRecordListStub;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.proxy.QueryRecordServiceProxy;
import com.queryclient.af.proxy.remote.RemoteProxyFactory;
import com.queryclient.af.proxy.remote.RemoteQueryRecordServiceProxy;
import com.queryclient.af.resource.QueryRecordResource;
import com.queryclient.af.resource.util.ReferrerInfoStructResourceUtil;


@Path("/_task/r/queryRecords/")
public class AsyncQueryRecordResource extends BaseAsyncResource implements QueryRecordResource
{
    private static final Logger log = Logger.getLogger(AsyncQueryRecordResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncQueryRecordResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getQueryRecordList(List<QueryRecord> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findQueryRecordsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getQueryRecordAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getQueryRecord(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getQueryRecordAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getQueryRecord(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructQueryRecord(QueryRecordStub queryRecord) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructQueryRecord(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            QueryRecordBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                QueryRecordStub realStub = (QueryRecordStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertQueryRecordStubToBean(realStub);
            } else {
                bean = convertQueryRecordStubToBean(queryRecord);
            }
            //bean = (QueryRecordBean) RemoteProxyFactory.getInstance().getQueryRecordServiceProxy().constructQueryRecord(bean);
            //queryRecord = QueryRecordStub.convertBeanToStub(bean);
            //String guid = queryRecord.getGuid();
            // TBD: createQueryRecord() or constructQueryRecord()???  (constructQueryRecord() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getQueryRecordServiceProxy().createQueryRecord(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructQueryRecord(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(queryRecord).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createQueryRecord(QueryRecordStub queryRecord) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createQueryRecord(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            QueryRecordBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                QueryRecordStub realStub = (QueryRecordStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertQueryRecordStubToBean(realStub);
            } else {
                bean = convertQueryRecordStubToBean(queryRecord);
            }
            String guid = RemoteProxyFactory.getInstance().getQueryRecordServiceProxy().createQueryRecord(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createQueryRecord(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createQueryRecord(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshQueryRecord(String guid, QueryRecordStub queryRecord) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshQueryRecord(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(queryRecord == null || !guid.equals(queryRecord.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from queryRecord guid = " + queryRecord.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the queryRecord with guid = " + guid);
            }
            QueryRecordBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                QueryRecordStub realStub = (QueryRecordStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertQueryRecordStubToBean(realStub);
            } else {
                bean = convertQueryRecordStubToBean(queryRecord);
            }
            //bean = (QueryRecordBean) RemoteProxyFactory.getInstance().getQueryRecordServiceProxy().refreshQueryRecord(bean);
            //if(bean == null) {
            //    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the queryRecord with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the queryRecord with guid = " + guid);
            //}
            //queryRecord = QueryRecordStub.convertBeanToStub(bean);
            // TBD: updateQueryRecord() or refreshQueryRecord()???  (refreshQueryRecord() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getQueryRecordServiceProxy().updateQueryRecord(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refrefsh the queryRecord with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the queryRecord with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshQueryRecord(): Successfully processed the request: guid = " + guid);
            return Response.ok(queryRecord).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateQueryRecord(String guid, QueryRecordStub queryRecord) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateQueryRecord(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(queryRecord == null || !guid.equals(queryRecord.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from queryRecord guid = " + queryRecord.getGuid());
                throw new RequestForbiddenRsException("Failed to update the queryRecord with guid = " + guid);
            }
            QueryRecordBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                QueryRecordStub realStub = (QueryRecordStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertQueryRecordStubToBean(realStub);
            } else {
                bean = convertQueryRecordStubToBean(queryRecord);
            }
            boolean suc = RemoteProxyFactory.getInstance().getQueryRecordServiceProxy().updateQueryRecord(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the queryRecord with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the queryRecord with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateQueryRecord(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, String referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
    public Response updateQueryRecord(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteQueryRecord(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteQueryRecord(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getQueryRecordServiceProxy().deleteQueryRecord(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the queryRecord with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the queryRecord with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteQueryRecord(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteQueryRecords(String filter, String params, List<String> values) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteQueryRecords(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getQueryRecordServiceProxy().deleteQueryRecords(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createQueryRecords(QueryRecordListStub queryRecords) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createQueryRecords(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            QueryRecordListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                QueryRecordListStub realStubs = (QueryRecordListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = queryRecords;
            }

            List<QueryRecordStub> stubList = queryRecords.getList();
            List<VisitorSetting> beans = new ArrayList<QueryRecord>();
            for(QueryRecordStub stub : stubList) {
                QueryRecordBean bean = convertQueryRecordStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getQueryRecordServiceProxy().createQueryRecords(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static QueryRecordBean convertQueryRecordStubToBean(QueryRecord stub)
    {
        QueryRecordBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new QueryRecordBean();
            bean.setGuid(stub.getGuid());
            bean.setQuerySession(stub.getQuerySession());
            bean.setQueryId(stub.getQueryId());
            bean.setDataService(stub.getDataService());
            bean.setServiceUrl(stub.getServiceUrl());
            bean.setDelayed(stub.isDelayed());
            bean.setQuery(stub.getQuery());
            bean.setInputFormat(stub.getInputFormat());
            bean.setInputFile(stub.getInputFile());
            bean.setInputContent(stub.getInputContent());
            bean.setTargetOutputFormat(stub.getTargetOutputFormat());
            bean.setOutputFormat(stub.getOutputFormat());
            bean.setOutputFile(stub.getOutputFile());
            bean.setOutputContent(stub.getOutputContent());
            bean.setResponseCode(stub.getResponseCode());
            bean.setResult(stub.getResult());
            bean.setReferrerInfo(ReferrerInfoStructResourceUtil.convertReferrerInfoStructStubToBean(stub.getReferrerInfo()));
            bean.setStatus(stub.getStatus());
            bean.setExtra(stub.getExtra());
            bean.setNote(stub.getNote());
            bean.setScheduledTime(stub.getScheduledTime());
            bean.setProcessedTime(stub.getProcessedTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
