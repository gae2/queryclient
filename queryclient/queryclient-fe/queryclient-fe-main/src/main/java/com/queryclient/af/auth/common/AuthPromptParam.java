package com.queryclient.af.auth.common;

import java.util.logging.Logger;
import java.util.logging.Level;


// Constants, to be used for CommonAuthUtil.PARAM_AUTHPROMPT query param.
// And some utility/convenience methods.
public final class AuthPromptParam
{
    private static final Logger log = Logger.getLogger(AuthPromptParam.class.getName());

    private AuthPromptParam() {}


    ///////////////////////////////////////////
    // Bit fields definitions
    // (Note that zero value in a particular field does not mean that particular action will not be prompted.
    //     Value of 1 however forces that action to be prompted.)

    // None.
    public static final int PROMPT_NONE = 0;

    // Show "login" button even when the user is already logged in.
    public static final int PROMPT_LOGIN = 1;

    // Show app/auth approval prompt even when the user has already approved. 
    public static final int PROMPT_APPROVAL = 2;
    
    // etc...


    public static boolean isForcePromptLogin(int authPrompt)
    {
        return (PROMPT_LOGIN & authPrompt) != 0;
    }

    public static boolean isForcePromptApproval(int authPrompt)
    {
        return (PROMPT_APPROVAL & authPrompt) != 0;
    }

    public static int requirePromptLogin(int authPrompt)
    {
        return (PROMPT_LOGIN | authPrompt);
    }

    public static int requirePromptApproval(int authPrompt)
    {
        return (PROMPT_APPROVAL | authPrompt);
    }


    public static int parseAuthPrompt(String authPromptStr)
    {
        int authPrompt = 0;
        try {
            authPrompt = Integer.parseInt(authPromptStr);
        } catch(NumberFormatException e) {
            // Ignore
            if(log.isLoggable(Level.INFO)) log.info("Failed to parse authPrompt = " + authPromptStr);
        }
        return authPrompt;
    }

}
