package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.stub.GaeAppStructStub;
import com.queryclient.ws.stub.ExternalUserIdStructStub;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.stub.ExternalUserAuthStub;


// Wrapper class + bean combo.
public class ExternalUserAuthBean implements ExternalUserAuth, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExternalUserAuthBean.class.getName());

    // [1] With an embedded object.
    private ExternalUserAuthStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private String providerId;
    private ExternalUserIdStructBean externalUserId;
    private String requestToken;
    private String accessToken;
    private String accessTokenSecret;
    private String email;
    private String firstName;
    private String lastName;
    private String fullName;
    private String displayName;
    private String description;
    private String gender;
    private String dateOfBirth;
    private String profileImageUrl;
    private String timeZone;
    private String postalCode;
    private String location;
    private String country;
    private String language;
    private String status;
    private Long authTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ExternalUserAuthBean()
    {
        //this((String) null);
    }
    public ExternalUserAuthBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ExternalUserAuthBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStructBean externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, providerId, externalUserId, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime, null, null);
    }
    public ExternalUserAuthBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStructBean externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.providerId = providerId;
        this.externalUserId = externalUserId;
        this.requestToken = requestToken;
        this.accessToken = accessToken;
        this.accessTokenSecret = accessTokenSecret;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
        this.displayName = displayName;
        this.description = description;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.profileImageUrl = profileImageUrl;
        this.timeZone = timeZone;
        this.postalCode = postalCode;
        this.location = location;
        this.country = country;
        this.language = language;
        this.status = status;
        this.authTime = authTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ExternalUserAuthBean(ExternalUserAuth stub)
    {
        if(stub instanceof ExternalUserAuthStub) {
            this.stub = (ExternalUserAuthStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setManagerApp(stub.getManagerApp());   
            setAppAcl(stub.getAppAcl());   
            GaeAppStruct gaeApp = stub.getGaeApp();
            if(gaeApp instanceof GaeAppStructBean) {
                setGaeApp((GaeAppStructBean) gaeApp);   
            } else {
                setGaeApp(new GaeAppStructBean(gaeApp));   
            }
            setOwnerUser(stub.getOwnerUser());   
            setUserAcl(stub.getUserAcl());   
            setUser(stub.getUser());   
            setProviderId(stub.getProviderId());   
            ExternalUserIdStruct externalUserId = stub.getExternalUserId();
            if(externalUserId instanceof ExternalUserIdStructBean) {
                setExternalUserId((ExternalUserIdStructBean) externalUserId);   
            } else {
                setExternalUserId(new ExternalUserIdStructBean(externalUserId));   
            }
            setRequestToken(stub.getRequestToken());   
            setAccessToken(stub.getAccessToken());   
            setAccessTokenSecret(stub.getAccessTokenSecret());   
            setEmail(stub.getEmail());   
            setFirstName(stub.getFirstName());   
            setLastName(stub.getLastName());   
            setFullName(stub.getFullName());   
            setDisplayName(stub.getDisplayName());   
            setDescription(stub.getDescription());   
            setGender(stub.getGender());   
            setDateOfBirth(stub.getDateOfBirth());   
            setProfileImageUrl(stub.getProfileImageUrl());   
            setTimeZone(stub.getTimeZone());   
            setPostalCode(stub.getPostalCode());   
            setLocation(stub.getLocation());   
            setCountry(stub.getCountry());   
            setLanguage(stub.getLanguage());   
            setStatus(stub.getStatus());   
            setAuthTime(stub.getAuthTime());   
            setExpirationTime(stub.getExpirationTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getManagerApp()
    {
        if(getStub() != null) {
            return getStub().getManagerApp();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.managerApp;
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getStub() != null) {
            getStub().setManagerApp(managerApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.managerApp = managerApp;
        }
    }

    public Long getAppAcl()
    {
        if(getStub() != null) {
            return getStub().getAppAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appAcl;
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getStub() != null) {
            getStub().setAppAcl(appAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appAcl = appAcl;
        }
    }

    public GaeAppStruct getGaeApp()
    {  
        if(getStub() != null) {
            // Note the object type.
            GaeAppStruct _stub_field = getStub().getGaeApp();
            if(_stub_field == null) {
                return null;
            } else {
                return new GaeAppStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.gaeApp;
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGaeApp(gaeApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(gaeApp == null) {
                this.gaeApp = null;
            } else {
                if(gaeApp instanceof GaeAppStructBean) {
                    this.gaeApp = (GaeAppStructBean) gaeApp;
                } else {
                    this.gaeApp = new GaeAppStructBean(gaeApp);
                }
            }
        }
    }

    public String getOwnerUser()
    {
        if(getStub() != null) {
            return getStub().getOwnerUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ownerUser;
        }
    }
    public void setOwnerUser(String ownerUser)
    {
        if(getStub() != null) {
            getStub().setOwnerUser(ownerUser);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ownerUser = ownerUser;
        }
    }

    public Long getUserAcl()
    {
        if(getStub() != null) {
            return getStub().getUserAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userAcl;
        }
    }
    public void setUserAcl(Long userAcl)
    {
        if(getStub() != null) {
            getStub().setUserAcl(userAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userAcl = userAcl;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getProviderId()
    {
        if(getStub() != null) {
            return getStub().getProviderId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.providerId;
        }
    }
    public void setProviderId(String providerId)
    {
        if(getStub() != null) {
            getStub().setProviderId(providerId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.providerId = providerId;
        }
    }

    public ExternalUserIdStruct getExternalUserId()
    {  
        if(getStub() != null) {
            // Note the object type.
            ExternalUserIdStruct _stub_field = getStub().getExternalUserId();
            if(_stub_field == null) {
                return null;
            } else {
                return new ExternalUserIdStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.externalUserId;
        }
    }
    public void setExternalUserId(ExternalUserIdStruct externalUserId)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setExternalUserId(externalUserId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(externalUserId == null) {
                this.externalUserId = null;
            } else {
                if(externalUserId instanceof ExternalUserIdStructBean) {
                    this.externalUserId = (ExternalUserIdStructBean) externalUserId;
                } else {
                    this.externalUserId = new ExternalUserIdStructBean(externalUserId);
                }
            }
        }
    }

    public String getRequestToken()
    {
        if(getStub() != null) {
            return getStub().getRequestToken();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.requestToken;
        }
    }
    public void setRequestToken(String requestToken)
    {
        if(getStub() != null) {
            getStub().setRequestToken(requestToken);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.requestToken = requestToken;
        }
    }

    public String getAccessToken()
    {
        if(getStub() != null) {
            return getStub().getAccessToken();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.accessToken;
        }
    }
    public void setAccessToken(String accessToken)
    {
        if(getStub() != null) {
            getStub().setAccessToken(accessToken);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.accessToken = accessToken;
        }
    }

    public String getAccessTokenSecret()
    {
        if(getStub() != null) {
            return getStub().getAccessTokenSecret();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.accessTokenSecret;
        }
    }
    public void setAccessTokenSecret(String accessTokenSecret)
    {
        if(getStub() != null) {
            getStub().setAccessTokenSecret(accessTokenSecret);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.accessTokenSecret = accessTokenSecret;
        }
    }

    public String getEmail()
    {
        if(getStub() != null) {
            return getStub().getEmail();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.email;
        }
    }
    public void setEmail(String email)
    {
        if(getStub() != null) {
            getStub().setEmail(email);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.email = email;
        }
    }

    public String getFirstName()
    {
        if(getStub() != null) {
            return getStub().getFirstName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.firstName;
        }
    }
    public void setFirstName(String firstName)
    {
        if(getStub() != null) {
            getStub().setFirstName(firstName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.firstName = firstName;
        }
    }

    public String getLastName()
    {
        if(getStub() != null) {
            return getStub().getLastName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastName;
        }
    }
    public void setLastName(String lastName)
    {
        if(getStub() != null) {
            getStub().setLastName(lastName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastName = lastName;
        }
    }

    public String getFullName()
    {
        if(getStub() != null) {
            return getStub().getFullName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fullName;
        }
    }
    public void setFullName(String fullName)
    {
        if(getStub() != null) {
            getStub().setFullName(fullName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fullName = fullName;
        }
    }

    public String getDisplayName()
    {
        if(getStub() != null) {
            return getStub().getDisplayName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.displayName;
        }
    }
    public void setDisplayName(String displayName)
    {
        if(getStub() != null) {
            getStub().setDisplayName(displayName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.displayName = displayName;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public String getGender()
    {
        if(getStub() != null) {
            return getStub().getGender();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.gender;
        }
    }
    public void setGender(String gender)
    {
        if(getStub() != null) {
            getStub().setGender(gender);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.gender = gender;
        }
    }

    public String getDateOfBirth()
    {
        if(getStub() != null) {
            return getStub().getDateOfBirth();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.dateOfBirth;
        }
    }
    public void setDateOfBirth(String dateOfBirth)
    {
        if(getStub() != null) {
            getStub().setDateOfBirth(dateOfBirth);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.dateOfBirth = dateOfBirth;
        }
    }

    public String getProfileImageUrl()
    {
        if(getStub() != null) {
            return getStub().getProfileImageUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.profileImageUrl;
        }
    }
    public void setProfileImageUrl(String profileImageUrl)
    {
        if(getStub() != null) {
            getStub().setProfileImageUrl(profileImageUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.profileImageUrl = profileImageUrl;
        }
    }

    public String getTimeZone()
    {
        if(getStub() != null) {
            return getStub().getTimeZone();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.timeZone;
        }
    }
    public void setTimeZone(String timeZone)
    {
        if(getStub() != null) {
            getStub().setTimeZone(timeZone);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.timeZone = timeZone;
        }
    }

    public String getPostalCode()
    {
        if(getStub() != null) {
            return getStub().getPostalCode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.postalCode;
        }
    }
    public void setPostalCode(String postalCode)
    {
        if(getStub() != null) {
            getStub().setPostalCode(postalCode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.postalCode = postalCode;
        }
    }

    public String getLocation()
    {
        if(getStub() != null) {
            return getStub().getLocation();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.location;
        }
    }
    public void setLocation(String location)
    {
        if(getStub() != null) {
            getStub().setLocation(location);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.location = location;
        }
    }

    public String getCountry()
    {
        if(getStub() != null) {
            return getStub().getCountry();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.country;
        }
    }
    public void setCountry(String country)
    {
        if(getStub() != null) {
            getStub().setCountry(country);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.country = country;
        }
    }

    public String getLanguage()
    {
        if(getStub() != null) {
            return getStub().getLanguage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.language;
        }
    }
    public void setLanguage(String language)
    {
        if(getStub() != null) {
            getStub().setLanguage(language);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.language = language;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getAuthTime()
    {
        if(getStub() != null) {
            return getStub().getAuthTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.authTime;
        }
    }
    public void setAuthTime(Long authTime)
    {
        if(getStub() != null) {
            getStub().setAuthTime(authTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.authTime = authTime;
        }
    }

    public Long getExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getExpirationTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationTime;
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getStub() != null) {
            getStub().setExpirationTime(expirationTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationTime = expirationTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public ExternalUserAuthStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ExternalUserAuthStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("managerApp = " + this.managerApp).append(";");
            sb.append("appAcl = " + this.appAcl).append(";");
            sb.append("gaeApp = " + this.gaeApp).append(";");
            sb.append("ownerUser = " + this.ownerUser).append(";");
            sb.append("userAcl = " + this.userAcl).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("providerId = " + this.providerId).append(";");
            sb.append("externalUserId = " + this.externalUserId).append(";");
            sb.append("requestToken = " + this.requestToken).append(";");
            sb.append("accessToken = " + this.accessToken).append(";");
            sb.append("accessTokenSecret = " + this.accessTokenSecret).append(";");
            sb.append("email = " + this.email).append(";");
            sb.append("firstName = " + this.firstName).append(";");
            sb.append("lastName = " + this.lastName).append(";");
            sb.append("fullName = " + this.fullName).append(";");
            sb.append("displayName = " + this.displayName).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("gender = " + this.gender).append(";");
            sb.append("dateOfBirth = " + this.dateOfBirth).append(";");
            sb.append("profileImageUrl = " + this.profileImageUrl).append(";");
            sb.append("timeZone = " + this.timeZone).append(";");
            sb.append("postalCode = " + this.postalCode).append(";");
            sb.append("location = " + this.location).append(";");
            sb.append("country = " + this.country).append(";");
            sb.append("language = " + this.language).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("authTime = " + this.authTime).append(";");
            sb.append("expirationTime = " + this.expirationTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = managerApp == null ? 0 : managerApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = appAcl == null ? 0 : appAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = gaeApp == null ? 0 : gaeApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = ownerUser == null ? 0 : ownerUser.hashCode();
            _hash = 31 * _hash + delta;
            delta = userAcl == null ? 0 : userAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = providerId == null ? 0 : providerId.hashCode();
            _hash = 31 * _hash + delta;
            delta = externalUserId == null ? 0 : externalUserId.hashCode();
            _hash = 31 * _hash + delta;
            delta = requestToken == null ? 0 : requestToken.hashCode();
            _hash = 31 * _hash + delta;
            delta = accessToken == null ? 0 : accessToken.hashCode();
            _hash = 31 * _hash + delta;
            delta = accessTokenSecret == null ? 0 : accessTokenSecret.hashCode();
            _hash = 31 * _hash + delta;
            delta = email == null ? 0 : email.hashCode();
            _hash = 31 * _hash + delta;
            delta = firstName == null ? 0 : firstName.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastName == null ? 0 : lastName.hashCode();
            _hash = 31 * _hash + delta;
            delta = fullName == null ? 0 : fullName.hashCode();
            _hash = 31 * _hash + delta;
            delta = displayName == null ? 0 : displayName.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = gender == null ? 0 : gender.hashCode();
            _hash = 31 * _hash + delta;
            delta = dateOfBirth == null ? 0 : dateOfBirth.hashCode();
            _hash = 31 * _hash + delta;
            delta = profileImageUrl == null ? 0 : profileImageUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = timeZone == null ? 0 : timeZone.hashCode();
            _hash = 31 * _hash + delta;
            delta = postalCode == null ? 0 : postalCode.hashCode();
            _hash = 31 * _hash + delta;
            delta = location == null ? 0 : location.hashCode();
            _hash = 31 * _hash + delta;
            delta = country == null ? 0 : country.hashCode();
            _hash = 31 * _hash + delta;
            delta = language == null ? 0 : language.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = authTime == null ? 0 : authTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationTime == null ? 0 : expirationTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
