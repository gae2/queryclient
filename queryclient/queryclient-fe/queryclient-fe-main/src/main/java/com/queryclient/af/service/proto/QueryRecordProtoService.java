package com.queryclient.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.service.impl.QueryRecordServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class QueryRecordProtoService extends QueryRecordServiceImpl implements QueryRecordService
{
    private static final Logger log = Logger.getLogger(QueryRecordProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public QueryRecordProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // QueryRecord related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public QueryRecord getQueryRecord(String guid) throws BaseException
    {
        return super.getQueryRecord(guid);
    }

    @Override
    public Object getQueryRecord(String guid, String field) throws BaseException
    {
        return super.getQueryRecord(guid, field);
    }

    @Override
    public List<QueryRecord> getQueryRecords(List<String> guids) throws BaseException
    {
        return super.getQueryRecords(guids);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords() throws BaseException
    {
        return super.getAllQueryRecords();
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecords(ordering, offset, count, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllQueryRecords(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        return super.createQueryRecord(queryRecord);
    }

    @Override
    public QueryRecord constructQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        return super.constructQueryRecord(queryRecord);
    }


    @Override
    public Boolean updateQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        return super.updateQueryRecord(queryRecord);
    }
        
    @Override
    public QueryRecord refreshQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        return super.refreshQueryRecord(queryRecord);
    }

    @Override
    public Boolean deleteQueryRecord(String guid) throws BaseException
    {
        return super.deleteQueryRecord(guid);
    }

    @Override
    public Boolean deleteQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        return super.deleteQueryRecord(queryRecord);
    }

    @Override
    public Integer createQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    {
        return super.createQueryRecords(queryRecords);
    }

    // TBD
    //@Override
    //public Boolean updateQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    //{
    //}

}
