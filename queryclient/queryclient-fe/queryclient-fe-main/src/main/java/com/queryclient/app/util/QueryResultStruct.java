package com.queryclient.app.util;

public class QueryResultStruct
{
    private int responseCode;
    private String result;
    private String output;


    public QueryResultStruct()
    {
        this(0, null, null);
    }

    public QueryResultStruct(int responseCode, String result, String output)
    {
        this.responseCode = responseCode;
        this.result = result;
        this.output = output;
    }

    
    public int getResponseCode()
    {
        return responseCode;
    }
    public void setResponseCode(int responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getResult()
    {
        return result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public String getOutput()
    {
        return output;
    }
    public void setOutput(String output)
    {
        this.output = output;
    }

    
    
    
}
