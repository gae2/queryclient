package com.queryclient.app.util;

import java.util.logging.Logger;
//import oauth.signpost.OAuthConsumer;
//import oauth.signpost.basic.DefaultOAuthConsumer;


// TBD:
// Use MiniClient or at least MiniAuth
// ...


@Deprecated
public class SignpostClientManagerTBD
{
    private static final Logger log = Logger.getLogger(SignpostClientManagerTBD.class.getName());
    
//    // temporary
//    private Map<String, OAuthConsumer> consumerMapCache = null;

    
    private SignpostClientManagerTBD()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static final class SignpostClientManagerHolder
    {
        private static final SignpostClientManagerTBD INSTANCE = new SignpostClientManagerTBD();
    }

    // Singleton method
    public static SignpostClientManagerTBD getInstance()
    {
        return SignpostClientManagerHolder.INSTANCE;
    }

    
    private void init()
    {
        
//        // temporary
//        consumerMapCache = new HashMap<String, OAuthConsumer>();

        // ...
        
    }

    
//    // TBD: ...
//    public OAuthConsumer getConsumer(String service) throws BaseException
//    {
//        OAuthConsumer consumer = null;
//        if(consumerMapCache.containsKey(service)) {
//            consumer = consumerMapCache.get(service);
//        } else {
//            // temporary
//            ConsumerKeySecretPair pair = TargetServiceManager.getInstance().getKeySecretPair(service);
//            if(pair != null) {
//                consumer = new DefaultOAuthConsumer(pair.getConsumerKey(), pair.getConsumerSecret());
//                consumerMapCache.put(service, consumer);
//            } else {
//                log.log(Level.WARNING, "Failed to create an OAuthConsumer for service = " + service);
//                throw new BaseException("Failed to create an OAuthConsumer for service = " + service);
//            }
//        }
//        
//        return consumer;
//    }

    
}
