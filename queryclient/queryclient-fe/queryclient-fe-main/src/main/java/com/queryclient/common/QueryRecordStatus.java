package com.queryclient.common;

import java.util.logging.Logger;


public class QueryRecordStatus
{
    private static final Logger log = Logger.getLogger(QueryRecordStatus.class.getName());

    private QueryRecordStatus() {}

    // TBD:
    public static final String STATUS_UNKNOWN = "unknown";  // ???

    public static final String STATUS_ACTIVE = "active";  // ???
    public static final String STATUS_PAUSED = "paused";  // ???  Redirected??
    public static final String STATUS_HIDDEN = "hidden";  // ???
    public static final String STATUS_DELETED = "deleted";  // ???
    // ...

    
}
