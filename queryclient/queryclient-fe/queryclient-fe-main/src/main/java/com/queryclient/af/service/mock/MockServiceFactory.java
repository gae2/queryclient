package com.queryclient.af.service.mock;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.service.AbstractServiceFactory;
import com.queryclient.af.service.ApiConsumerService;
import com.queryclient.af.service.UserService;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.af.service.ExternalUserAuthService;
import com.queryclient.af.service.UserAuthStateService;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.service.ServiceInfoService;
import com.queryclient.af.service.FiveTenService;


// Create your own mock object factory using MockServiceFactory as a template.
public class MockServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(MockServiceFactory.class.getName());

    // Using the Decorator pattern.
    private AbstractServiceFactory decoratedServiceFactory;
    private MockServiceFactory()
    {
        this(null);   // ????
    }
    private MockServiceFactory(AbstractServiceFactory decoratedServiceFactory)
    {
        this.decoratedServiceFactory = decoratedServiceFactory;
    }

    // Initialization-on-demand holder.
    private static class MockServiceFactoryHolder
    {
        private static final MockServiceFactory INSTANCE = new MockServiceFactory();
    }

    // Singleton method
    public static MockServiceFactory getInstance()
    {
        return MockServiceFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public AbstractServiceFactory getDecoratedServiceFactory()
    {
        return decoratedServiceFactory;
    }
    public void setDecoratedServiceFactory(AbstractServiceFactory decoratedServiceFactory)
    {
        this.decoratedServiceFactory = decoratedServiceFactory;
    }


    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerMockService(decoratedServiceFactory.getApiConsumerService()) {};
    }

    @Override
    public UserService getUserService()
    {
        return new UserMockService(decoratedServiceFactory.getUserService()) {};
    }

    @Override
    public UserPasswordService getUserPasswordService()
    {
        return new UserPasswordMockService(decoratedServiceFactory.getUserPasswordService()) {};
    }

    @Override
    public ExternalUserAuthService getExternalUserAuthService()
    {
        return new ExternalUserAuthMockService(decoratedServiceFactory.getExternalUserAuthService()) {};
    }

    @Override
    public UserAuthStateService getUserAuthStateService()
    {
        return new UserAuthStateMockService(decoratedServiceFactory.getUserAuthStateService()) {};
    }

    @Override
    public DataServiceService getDataServiceService()
    {
        return new DataServiceMockService(decoratedServiceFactory.getDataServiceService()) {};
    }

    @Override
    public ServiceEndpointService getServiceEndpointService()
    {
        return new ServiceEndpointMockService(decoratedServiceFactory.getServiceEndpointService()) {};
    }

    @Override
    public QuerySessionService getQuerySessionService()
    {
        return new QuerySessionMockService(decoratedServiceFactory.getQuerySessionService()) {};
    }

    @Override
    public QueryRecordService getQueryRecordService()
    {
        return new QueryRecordMockService(decoratedServiceFactory.getQueryRecordService()) {};
    }

    @Override
    public DummyEntityService getDummyEntityService()
    {
        return new DummyEntityMockService(decoratedServiceFactory.getDummyEntityService()) {};
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoMockService(decoratedServiceFactory.getServiceInfoService()) {};
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenMockService(decoratedServiceFactory.getFiveTenService()) {};
    }


}
