package com.queryclient.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.stub.ConsumerKeySecretPairStub;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;


public class ConsumerKeySecretPairResourceUtil
{
    private static final Logger log = Logger.getLogger(ConsumerKeySecretPairResourceUtil.class.getName());

    // Static methods only.
    private ConsumerKeySecretPairResourceUtil() {}

    public static ConsumerKeySecretPairBean convertConsumerKeySecretPairStubToBean(ConsumerKeySecretPair stub)
    {
        ConsumerKeySecretPairBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new ConsumerKeySecretPairBean();
            bean.setConsumerKey(stub.getConsumerKey());
            bean.setConsumerSecret(stub.getConsumerSecret());
        }
        return bean;
    }

}
