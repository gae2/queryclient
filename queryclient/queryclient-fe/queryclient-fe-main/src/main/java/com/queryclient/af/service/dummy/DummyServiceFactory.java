package com.queryclient.af.service.dummy;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.service.AbstractServiceFactory;
import com.queryclient.af.service.ApiConsumerService;
import com.queryclient.af.service.UserService;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.af.service.ExternalUserAuthService;
import com.queryclient.af.service.UserAuthStateService;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.service.ServiceInfoService;
import com.queryclient.af.service.FiveTenService;


// The primary purpose of a dummy service is to fake the service api.
// The caller/client can use the same API as other abstract factory-based services,
// but the dummy services do not do anything.
public class DummyServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(DummyServiceFactory.class.getName());

    private DummyServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class DummyServiceFactoryHolder
    {
        private static final DummyServiceFactory INSTANCE = new DummyServiceFactory();
    }

    // Singleton method
    public static DummyServiceFactory getInstance()
    {
        return DummyServiceFactoryHolder.INSTANCE;
    }


    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerDummyService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserDummyService();
    }

    @Override
    public UserPasswordService getUserPasswordService()
    {
        return new UserPasswordDummyService();
    }

    @Override
    public ExternalUserAuthService getExternalUserAuthService()
    {
        return new ExternalUserAuthDummyService();
    }

    @Override
    public UserAuthStateService getUserAuthStateService()
    {
        return new UserAuthStateDummyService();
    }

    @Override
    public DataServiceService getDataServiceService()
    {
        return new DataServiceDummyService();
    }

    @Override
    public ServiceEndpointService getServiceEndpointService()
    {
        return new ServiceEndpointDummyService();
    }

    @Override
    public QuerySessionService getQuerySessionService()
    {
        return new QuerySessionDummyService();
    }

    @Override
    public QueryRecordService getQueryRecordService()
    {
        return new QueryRecordDummyService();
    }

    @Override
    public DummyEntityService getDummyEntityService()
    {
        return new DummyEntityDummyService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoDummyService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenDummyService();
    }


}
