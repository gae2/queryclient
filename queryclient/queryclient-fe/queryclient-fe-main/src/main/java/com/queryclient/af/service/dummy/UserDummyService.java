package com.queryclient.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.User;
import com.queryclient.af.config.Config;

import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.UserService;


// The primary purpose of UserDummyService is to fake the service api, UserService.
// It has no real implementation.
public class UserDummyService implements UserService
{
    private static final Logger log = Logger.getLogger(UserDummyService.class.getName());

    public UserDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // User related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public User getUser(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUser(): guid = " + guid);
        return null;
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUser(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        log.fine("getUsers()");
        return null;
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }


    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUsers(ordering, offset, count, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUsers(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUsers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserDummyService.findUsers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserDummyService.findUserKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String address, String location, String ipAddress, String referer, Boolean obsolete, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        log.finer("createUser()");
        return null;
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        log.finer("createUser()");
        return null;
    }

    @Override
    public User constructUser(User user) throws BaseException
    {
        log.finer("constructUser()");
        return null;
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String address, String location, String ipAddress, String referer, Boolean obsolete, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        log.finer("updateUser()");
        return null;
    }
        
    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        log.finer("updateUser()");
        return null;
    }

    @Override
    public User refreshUser(User user) throws BaseException
    {
        log.finer("refreshUser()");
        return null;
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteUser(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        log.finer("deleteUser()");
        return null;
    }

    // TBD
    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteUser(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUsers(List<User> users) throws BaseException
    {
        log.finer("createUsers()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateUsers(List<User> users) throws BaseException
    //{
    //}

}
