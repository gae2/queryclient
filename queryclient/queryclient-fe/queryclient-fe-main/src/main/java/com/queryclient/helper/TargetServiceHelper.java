package com.queryclient.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.queryclient.app.endpoint.TargetServiceManager;


// For now, this whole thing is very fragile...
// All settings in web.xml and TargetServiceManager, and here should all be consistent...
// ....
public class TargetServiceHelper
{
    private static final Logger log = Logger.getLogger(TargetServiceHelper.class.getName());

    // temporary
    private static final String sServiceScrapeService = "ScrapeDBApp";
    private static final String sServletPathScrapeService = "scrapeservice";
    private static final String sServicePageSynopsis = "PageSynopsisApp";
    private static final String sServletPathPageSynopsis = "pagesynopsis";
    // etc...

    // temporary
    private Map<String,String> mServletPathMap;

    
    private TargetServiceHelper()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static final class TargetServiceHelperHolder
    {
        private static final TargetServiceHelper INSTANCE = new TargetServiceHelper();
    }

    // Singleton method
    public static TargetServiceHelper getInstance()
    {
        return TargetServiceHelperHolder.INSTANCE;
    }

    private void init()
    {
        mServletPathMap = new HashMap<String,String>();
        
        // temporary
        mServletPathMap.put(sServletPathScrapeService, sServiceScrapeService);
        mServletPathMap.put(sServletPathPageSynopsis, sServicePageSynopsis);
        // etc...
        
    }

    
    // temporary
    public String getServiceUrlFromServletPath(String servletPath)
    {
        if(servletPath == null || servletPath.isEmpty()) {
            return null;
        }
        if(servletPath.startsWith("/")) {
            servletPath = servletPath.substring(1);
        }
        if(servletPath.isEmpty()) {
            return null;
        }        
        if(servletPath.endsWith("/")) {
            servletPath = servletPath.substring(0, servletPath.length()-1);
        }
        if(servletPath.isEmpty()) {
            return null;
        }
        
        String serviceName = mServletPathMap.get(servletPath);
        if(serviceName == null) {
            return null;
        }
        
        String serviceUrl = TargetServiceManager.getInstance().getPrimaryServiceUrl(serviceName);
        return serviceUrl;
    }

}
