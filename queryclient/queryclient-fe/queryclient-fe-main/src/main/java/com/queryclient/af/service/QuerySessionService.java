package com.queryclient.af.service;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface QuerySessionService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    QuerySession getQuerySession(String guid) throws BaseException;
    Object getQuerySession(String guid, String field) throws BaseException;
    List<QuerySession> getQuerySessions(List<String> guids) throws BaseException;
    List<QuerySession> getAllQuerySessions() throws BaseException;
    /* @Deprecated */ List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException;
    List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException;
    //String createQuerySession(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return QuerySession?)
    String createQuerySession(QuerySession querySession) throws BaseException;
    QuerySession constructQuerySession(QuerySession querySession) throws BaseException;
    Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException;
    //Boolean updateQuerySession(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateQuerySession(QuerySession querySession) throws BaseException;
    QuerySession refreshQuerySession(QuerySession querySession) throws BaseException;
    Boolean deleteQuerySession(String guid) throws BaseException;
    Boolean deleteQuerySession(QuerySession querySession) throws BaseException;
    Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createQuerySessions(List<QuerySession> querySessions) throws BaseException;
//    Boolean updateQuerySessions(List<QuerySession> querySessions) throws BaseException;

}
