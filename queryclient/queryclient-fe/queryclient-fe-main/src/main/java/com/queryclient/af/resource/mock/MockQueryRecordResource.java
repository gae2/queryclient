package com.queryclient.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.stub.QueryRecordStub;
import com.queryclient.ws.stub.QueryRecordListStub;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.resource.QueryRecordResource;
import com.queryclient.af.resource.util.ReferrerInfoStructResourceUtil;


// MockQueryRecordResource is a decorator.
// It can be used as a base class to mock QueryRecordResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/queryRecords/")
public abstract class MockQueryRecordResource implements QueryRecordResource
{
    private static final Logger log = Logger.getLogger(MockQueryRecordResource.class.getName());

    // MockQueryRecordResource uses the decorator design pattern.
    private QueryRecordResource decoratedResource;

    public MockQueryRecordResource(QueryRecordResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected QueryRecordResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(QueryRecordResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllQueryRecords(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findQueryRecordsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findQueryRecordsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getQueryRecordAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getQueryRecordAsHtml(guid);
//    }

    @Override
    public Response getQueryRecord(String guid) throws BaseResourceException
    {
        return decoratedResource.getQueryRecord(guid);
    }

    @Override
    public Response getQueryRecordAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getQueryRecordAsJsonp(guid, callback);
    }

    @Override
    public Response getQueryRecord(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getQueryRecord(guid, field);
    }

    // TBD
    @Override
    public Response constructQueryRecord(QueryRecordStub queryRecord) throws BaseResourceException
    {
        return decoratedResource.constructQueryRecord(queryRecord);
    }

    @Override
    public Response createQueryRecord(QueryRecordStub queryRecord) throws BaseResourceException
    {
        return decoratedResource.createQueryRecord(queryRecord);
    }

//    @Override
//    public Response createQueryRecord(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createQueryRecord(formParams);
//    }

    // TBD
    @Override
    public Response refreshQueryRecord(String guid, QueryRecordStub queryRecord) throws BaseResourceException
    {
        return decoratedResource.refreshQueryRecord(guid, queryRecord);
    }

    @Override
    public Response updateQueryRecord(String guid, QueryRecordStub queryRecord) throws BaseResourceException
    {
        return decoratedResource.updateQueryRecord(guid, queryRecord);
    }

    @Override
    public Response updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, String referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime)
    {
        return decoratedResource.updateQueryRecord(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime);
    }

//    @Override
//    public Response updateQueryRecord(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateQueryRecord(guid, formParams);
//    }

    @Override
    public Response deleteQueryRecord(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteQueryRecord(guid);
    }

    @Override
    public Response deleteQueryRecords(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteQueryRecords(filter, params, values);
    }


// TBD ....
    @Override
    public Response createQueryRecords(QueryRecordListStub queryRecords) throws BaseResourceException
    {
        return decoratedResource.createQueryRecords(queryRecords);
    }


}
