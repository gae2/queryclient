package com.queryclient.app.auth;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.aeryid.af.bean.AeryUserBean;
import com.aeryid.af.service.AeryUserService;
import com.aeryid.rf.proxy.AeryUserServiceProxy;
import com.aeryid.ws.AeryUser;
import com.queryclient.af.auth.user.AuthUser;
import com.queryclient.af.bean.UserBean;
import com.queryclient.af.config.Config;
import com.queryclient.app.service.UserAppService;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.User;
import com.queryclient.ws.core.GUID;


public class AuthConfigManager
{
    private static final Logger log = Logger.getLogger(AuthConfigManager.class.getName());

    // TBD
    private static final String CONFIG_KEY_AUTH_USE_AERYID = "queryclientapp.auth.useaeryid";
    // ...

    // TBD
    private UserAppService userAppService = null;
    private AeryUserService mAeryUserService = null;
    // ...

    // Default value is false...
    private boolean useAeryId = false;
    
    private AuthConfigManager() 
    {
        useAeryId = Config.getInstance().getBoolean(CONFIG_KEY_AUTH_USE_AERYID, useAeryId);
    }

    // Initialization-on-demand holder.
    private static final class AuthConfigManagerHolder
    {
        private static final AuthConfigManager INSTANCE = new AuthConfigManager();
    }

    // Singleton method
    public static AuthConfigManager getInstance()
    {
        return AuthConfigManagerHolder.INSTANCE;
    }
    
    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private AeryUserService getAeryUserService()
    {
        if(mAeryUserService == null) {
            mAeryUserService = new AeryUserServiceProxy();
        }
        return mAeryUserService;
    }


    public boolean useAeryId()
    {
        return useAeryId;
    }
    

    public String createUser(String sessionGuid, String userGuid)
    {
        if(useAeryId()) {
            return AeryUserHelper.getInstance().createAeryUser(sessionGuid, userGuid);
        } else {
            return AuthUserHelper.getInstance().createUser(sessionGuid, userGuid);
        }
    }


    
    public Boolean createOrUpdateUser(String currentUserGuid, String currentSessionId, AuthUser authUser)
    {
        Boolean created = null;
        if(useAeryId()) {
            created = AeryUserHelper.getInstance().createOrUpdateAeryUser(currentUserGuid, currentSessionId, authUser);
        } else {
            created = AuthUserHelper.getInstance().createOrUpdateUserBean(currentUserGuid, currentSessionId, authUser);
        }
        return created;
    }

    public Boolean updateUserIfNecessary(String currentUserGuid, String currentSessionId)
    {
        Boolean updated = null;
        if(useAeryId()) {
            updated = AeryUserHelper.getInstance().updateAeryUserIfNecessary(currentUserGuid, currentSessionId);
        } else {
            updated = AuthUserHelper.getInstance().updateUserBeanIfNecessary(currentUserGuid, currentSessionId);
        }
        return updated;
    }

    public String getUserGuid(String authUserGuid)
    {
        String userGuid = null;
        if(useAeryId()) {
            AeryUser aeryUser = AeryUserHelper.getInstance().getAeryUser(authUserGuid);
            if(aeryUser != null) {
                userGuid = aeryUser.getGuid();
            }
        } else {
            User appUser = AuthUserHelper.getInstance().getUser(authUserGuid);
            if(appUser != null) {
                userGuid = appUser.getGuid();
            }
        }
        return userGuid;
    }

    public String findUserGuidByGaeNicknameAndAuthDomain(String gaeOpenId, String authDomain)
    {
        String userGuid = null;
        if(useAeryId()) {
            AeryUser aeryUser = AeryUserHelper.getInstance().findAeryUserByGaeNicknameAndAuthDomain(gaeOpenId, authDomain);
            if(aeryUser != null) {
                userGuid = aeryUser.getGuid();
            }
        } else {
            User appUser = AuthUserHelper.getInstance().findUserByGaeNicknameAndAuthDomain(gaeOpenId, authDomain);
            if(appUser != null) {
                userGuid = appUser.getGuid();
            }
        }        
        return userGuid;
    }
    
    
}
