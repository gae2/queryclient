package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.FiveTen;
import com.queryclient.ws.stub.FiveTenStub;


// Wrapper class + bean combo.
public class FiveTenBean implements FiveTen, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FiveTenBean.class.getName());

    // [1] With an embedded object.
    private FiveTenStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private Integer counter;
    private String requesterIpAddress;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FiveTenBean()
    {
        //this((String) null);
    }
    public FiveTenBean(String guid)
    {
        this(guid, null, null, null, null);
    }
    public FiveTenBean(String guid, Integer counter, String requesterIpAddress)
    {
        this(guid, counter, requesterIpAddress, null, null);
    }
    public FiveTenBean(String guid, Integer counter, String requesterIpAddress, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.counter = counter;
        this.requesterIpAddress = requesterIpAddress;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FiveTenBean(FiveTen stub)
    {
        if(stub instanceof FiveTenStub) {
            this.stub = (FiveTenStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setCounter(stub.getCounter());   
            setRequesterIpAddress(stub.getRequesterIpAddress());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public Integer getCounter()
    {
        if(getStub() != null) {
            return getStub().getCounter();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.counter;
        }
    }
    public void setCounter(Integer counter)
    {
        if(getStub() != null) {
            getStub().setCounter(counter);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.counter = counter;
        }
    }

    public String getRequesterIpAddress()
    {
        if(getStub() != null) {
            return getStub().getRequesterIpAddress();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.requesterIpAddress;
        }
    }
    public void setRequesterIpAddress(String requesterIpAddress)
    {
        if(getStub() != null) {
            getStub().setRequesterIpAddress(requesterIpAddress);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.requesterIpAddress = requesterIpAddress;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public FiveTenStub getStub()
    {
        return this.stub;
    }
    protected void setStub(FiveTenStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("counter = " + this.counter).append(";");
            sb.append("requesterIpAddress = " + this.requesterIpAddress).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = counter == null ? 0 : counter.hashCode();
            _hash = 31 * _hash + delta;
            delta = requesterIpAddress == null ? 0 : requesterIpAddress.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
