package com.queryclient.af.mail;


public class MailManager
{
    // Singleton.
    private MailManager() {}

    // Initialization-on-demand holder.
    private static class MailManagerHolder
    {
        private static final MailManager INSTANCE = new MailManager();
    }

    // Singleton method
    public static MailManager getInstance()
    {
        return MailManagerHolder.INSTANCE;
    }

    public void createMail(String recipient, String subject, String message)
    {
        // TBD
        // ...
        // Return "mail object"
    }
    
}
