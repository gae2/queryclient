package com.queryclient.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.QueryRecordService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class QueryRecordServiceImpl implements QueryRecordService
{
    private static final Logger log = Logger.getLogger(QueryRecordServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "QueryRecord-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("QueryRecord:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public QueryRecordServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // QueryRecord related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public QueryRecord getQueryRecord(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getQueryRecord(): guid = " + guid);

        QueryRecordBean bean = null;
        if(getCache() != null) {
            bean = (QueryRecordBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (QueryRecordBean) getProxyFactory().getQueryRecordServiceProxy().getQueryRecord(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "QueryRecordBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve QueryRecordBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getQueryRecord(String guid, String field) throws BaseException
    {
        QueryRecordBean bean = null;
        if(getCache() != null) {
            bean = (QueryRecordBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (QueryRecordBean) getProxyFactory().getQueryRecordServiceProxy().getQueryRecord(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "QueryRecordBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve QueryRecordBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("querySession")) {
            return bean.getQuerySession();
        } else if(field.equals("queryId")) {
            return bean.getQueryId();
        } else if(field.equals("dataService")) {
            return bean.getDataService();
        } else if(field.equals("serviceUrl")) {
            return bean.getServiceUrl();
        } else if(field.equals("delayed")) {
            return bean.isDelayed();
        } else if(field.equals("query")) {
            return bean.getQuery();
        } else if(field.equals("inputFormat")) {
            return bean.getInputFormat();
        } else if(field.equals("inputFile")) {
            return bean.getInputFile();
        } else if(field.equals("inputContent")) {
            return bean.getInputContent();
        } else if(field.equals("targetOutputFormat")) {
            return bean.getTargetOutputFormat();
        } else if(field.equals("outputFormat")) {
            return bean.getOutputFormat();
        } else if(field.equals("outputFile")) {
            return bean.getOutputFile();
        } else if(field.equals("outputContent")) {
            return bean.getOutputContent();
        } else if(field.equals("responseCode")) {
            return bean.getResponseCode();
        } else if(field.equals("result")) {
            return bean.getResult();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("extra")) {
            return bean.getExtra();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("scheduledTime")) {
            return bean.getScheduledTime();
        } else if(field.equals("processedTime")) {
            return bean.getProcessedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<QueryRecord> getQueryRecords(List<String> guids) throws BaseException
    {
        log.fine("getQueryRecords()");

        // TBD: Is there a better way????
        List<QueryRecord> queryRecords = getProxyFactory().getQueryRecordServiceProxy().getQueryRecords(guids);
        if(queryRecords == null) {
            log.log(Level.WARNING, "Failed to retrieve QueryRecordBean list.");
        }

        log.finer("END");
        return queryRecords;
    }

    @Override
    public List<QueryRecord> getAllQueryRecords() throws BaseException
    {
        return getAllQueryRecords(null, null, null);
    }


    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecords(ordering, offset, count, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQueryRecords(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<QueryRecord> queryRecords = getProxyFactory().getQueryRecordServiceProxy().getAllQueryRecords(ordering, offset, count, forwardCursor);
        if(queryRecords == null) {
            log.log(Level.WARNING, "Failed to retrieve QueryRecordBean list.");
        }

        log.finer("END");
        return queryRecords;
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQueryRecordKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getQueryRecordServiceProxy().getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve QueryRecordBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty QueryRecordBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QueryRecordServiceImpl.findQueryRecords(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<QueryRecord> queryRecords = getProxyFactory().getQueryRecordServiceProxy().findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(queryRecords == null) {
            log.log(Level.WARNING, "Failed to find queryRecords for the given criterion.");
        }

        log.finer("END");
        return queryRecords;
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QueryRecordServiceImpl.findQueryRecordKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getQueryRecordServiceProxy().findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find QueryRecord keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty QueryRecord key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QueryRecordServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getQueryRecordServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createQueryRecord(String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        QueryRecordBean bean = new QueryRecordBean(null, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfoBean, status, extra, note, scheduledTime, processedTime);
        return createQueryRecord(bean);
    }

    @Override
    public String createQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //QueryRecord bean = constructQueryRecord(queryRecord);
        //return bean.getGuid();

        // Param queryRecord cannot be null.....
        if(queryRecord == null) {
            log.log(Level.INFO, "Param queryRecord is null!");
            throw new BadRequestException("Param queryRecord object is null!");
        }
        QueryRecordBean bean = null;
        if(queryRecord instanceof QueryRecordBean) {
            bean = (QueryRecordBean) queryRecord;
        } else if(queryRecord instanceof QueryRecord) {
            // bean = new QueryRecordBean(null, queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), (ReferrerInfoStructBean) queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new QueryRecordBean(queryRecord.getGuid(), queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), (ReferrerInfoStructBean) queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
        } else {
            log.log(Level.WARNING, "createQueryRecord(): Arg queryRecord is of an unknown type.");
            //bean = new QueryRecordBean();
            bean = new QueryRecordBean(queryRecord.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getQueryRecordServiceProxy().createQueryRecord(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public QueryRecord constructQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");

        // Param queryRecord cannot be null.....
        if(queryRecord == null) {
            log.log(Level.INFO, "Param queryRecord is null!");
            throw new BadRequestException("Param queryRecord object is null!");
        }
        QueryRecordBean bean = null;
        if(queryRecord instanceof QueryRecordBean) {
            bean = (QueryRecordBean) queryRecord;
        } else if(queryRecord instanceof QueryRecord) {
            // bean = new QueryRecordBean(null, queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), (ReferrerInfoStructBean) queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new QueryRecordBean(queryRecord.getGuid(), queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), (ReferrerInfoStructBean) queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
        } else {
            log.log(Level.WARNING, "createQueryRecord(): Arg queryRecord is of an unknown type.");
            //bean = new QueryRecordBean();
            bean = new QueryRecordBean(queryRecord.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getQueryRecordServiceProxy().createQueryRecord(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        QueryRecordBean bean = new QueryRecordBean(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfoBean, status, extra, note, scheduledTime, processedTime);
        return updateQueryRecord(bean);
    }
        
    @Override
    public Boolean updateQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //QueryRecord bean = refreshQueryRecord(queryRecord);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param queryRecord cannot be null.....
        if(queryRecord == null || queryRecord.getGuid() == null) {
            log.log(Level.INFO, "Param queryRecord or its guid is null!");
            throw new BadRequestException("Param queryRecord object or its guid is null!");
        }
        QueryRecordBean bean = null;
        if(queryRecord instanceof QueryRecordBean) {
            bean = (QueryRecordBean) queryRecord;
        } else {  // if(queryRecord instanceof QueryRecord)
            bean = new QueryRecordBean(queryRecord.getGuid(), queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), (ReferrerInfoStructBean) queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
        }
        Boolean suc = getProxyFactory().getQueryRecordServiceProxy().updateQueryRecord(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public QueryRecord refreshQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");

        // Param queryRecord cannot be null.....
        if(queryRecord == null || queryRecord.getGuid() == null) {
            log.log(Level.INFO, "Param queryRecord or its guid is null!");
            throw new BadRequestException("Param queryRecord object or its guid is null!");
        }
        QueryRecordBean bean = null;
        if(queryRecord instanceof QueryRecordBean) {
            bean = (QueryRecordBean) queryRecord;
        } else {  // if(queryRecord instanceof QueryRecord)
            bean = new QueryRecordBean(queryRecord.getGuid(), queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), (ReferrerInfoStructBean) queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
        }
        Boolean suc = getProxyFactory().getQueryRecordServiceProxy().updateQueryRecord(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteQueryRecord(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getQueryRecordServiceProxy().deleteQueryRecord(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            QueryRecord queryRecord = null;
            try {
                queryRecord = getProxyFactory().getQueryRecordServiceProxy().getQueryRecord(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch queryRecord with a key, " + guid);
                return false;
            }
            if(queryRecord != null) {
                String beanGuid = queryRecord.getGuid();
                Boolean suc1 = getProxyFactory().getQueryRecordServiceProxy().deleteQueryRecord(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("queryRecord with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");

        // Param queryRecord cannot be null.....
        if(queryRecord == null || queryRecord.getGuid() == null) {
            log.log(Level.INFO, "Param queryRecord or its guid is null!");
            throw new BadRequestException("Param queryRecord object or its guid is null!");
        }
        QueryRecordBean bean = null;
        if(queryRecord instanceof QueryRecordBean) {
            bean = (QueryRecordBean) queryRecord;
        } else {  // if(queryRecord instanceof QueryRecord)
            // ????
            log.warning("queryRecord is not an instance of QueryRecordBean.");
            bean = new QueryRecordBean(queryRecord.getGuid(), queryRecord.getQuerySession(), queryRecord.getQueryId(), queryRecord.getDataService(), queryRecord.getServiceUrl(), queryRecord.isDelayed(), queryRecord.getQuery(), queryRecord.getInputFormat(), queryRecord.getInputFile(), queryRecord.getInputContent(), queryRecord.getTargetOutputFormat(), queryRecord.getOutputFormat(), queryRecord.getOutputFile(), queryRecord.getOutputContent(), queryRecord.getResponseCode(), queryRecord.getResult(), (ReferrerInfoStructBean) queryRecord.getReferrerInfo(), queryRecord.getStatus(), queryRecord.getExtra(), queryRecord.getNote(), queryRecord.getScheduledTime(), queryRecord.getProcessedTime());
        }
        Boolean suc = getProxyFactory().getQueryRecordServiceProxy().deleteQueryRecord(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteQueryRecords(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getQueryRecordServiceProxy().deleteQueryRecords(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    {
        log.finer("BEGIN");

        if(queryRecords == null) {
            log.log(Level.WARNING, "createQueryRecords() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = queryRecords.size();
        if(size == 0) {
            log.log(Level.WARNING, "createQueryRecords() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(QueryRecord queryRecord : queryRecords) {
            String guid = createQueryRecord(queryRecord);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createQueryRecords() failed for at least one queryRecord. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    //{
    //}

}
