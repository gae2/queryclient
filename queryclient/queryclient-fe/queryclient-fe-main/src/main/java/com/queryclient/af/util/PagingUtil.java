package com.queryclient.af.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;


// [ Notes on paging design ]
//
// We have a very complicated pagination design/implementation
//   (partly due to GAE limitations, etc.)
// We support, at high level, two alternative paging schemes.
// [1] Page index based
// [2] offset/cursor based
// (page-based scheme should be internally converted to offset/cursor scheme)
// ....
// [A] If no param (page or offset/cursor) is specified,
//     it is the first page...
// [B] If page is specified,
//     if pcm is present, then it is converted to cursor internally (to fetch the data)
//     if no pcm is specified, it is converted to offset internally.
// [C] If cursor or token/pcm is specified, it takes precedence over offset
//     If not, offset is used.
// ...
// We can potentially support up to four actions, first page, prev page, next page, and last page.
// [A] No first, no prev.
//     we can support next page, if 
//         a) we know whether (total count > count/pagesize) or not, or
//         b) we know whether (the next page cursor == null) or not.
//     if total count is known, we can support last page
// [B] We can always support first.
//     We can support prev/next, if pcm contains info on those pages (mapping of page -> cursor)
//     If we the current cursor is known, we can always support next page.
//     we can additionally support next page, if 
//         a) we know whether (total count > count/pagesize) or not, or
//         b) we know whether (the next page cursor == null) or not.
//     we can support last page, if
//         a) the total count is known,
//         b) we know the last page index, or
//         c) we know the last page cursor.
// [C] We can always support first.
// [C-1] Cursor or token/pcm is used,
//     we cannot support prev page (tbd: use cursor.reverse() ???)
//     we can always support next page.
//     we can support last page if pcm contains info on the last page (mapping of last page -> cursor)
// [C-2] Offset is used,
//     we can always support prev page.
//     we can support next page, if we know whether (total count > count/pagesize) or not.
//     we can support last page, if
//         a) the total count is known, of
//         b) we know the last page index.
// 
// Notes:
// (1) Page number starts from 0, not from 1...
// (2) Negative numbers mean pages from the last page, starting from -1. (do we support this yet?)
// (3) special constants supported: "first" (page==0) and "last" (page==-1). 
// (4) In case of [C], param "count" (or, default value, if none is specified) is used.
//     In case of [B], param "pagesize" is checked first, and if none is found, use "count", or the default value.
// (5) Note that multiple params may be specified which "conflict" with each other...
//     In such cases, we follow the "algorithm", [A] -> [B] -> [C].
//     That is, page takes precedence over offset/cursor.
//     Also, cursor takes precedence over token/pcm, which takes precedence over offset. 
// ..

public class PagingUtil
{
    private static final Logger log = Logger.getLogger(PagingUtil.class.getName());

    // Query params

    // TBD: PageCursorMap memcache key/token.
    public static final String PARAM_PCM = "pcm";
    // ....

    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_COUNT = "count";
    public static final String PARAM_PAGESIZE = "pagesize";    // vs count???
    public static final String PARAM_ORDERING = "ordering";    // "field [asc/desc]"

    public static final String PARAM_CURRENT_PAGE_CURSOR = "ccursor";
    // The following three not being used
    public static final String PARAM_PREVIOUS_PAGE_CURSOR = "pcursor";
    public static final String PARAM_NEXT_PAGE_CURSOR = "ncursor";
    public static final String PARAM_LAST_PAGE_CURSOR = "lcursor";
    // ....
    public static final String PARAM_CURRENT_PAGE_TOKEN = "ctoken";
    // The following three not being used
    public static final String PARAM_PREVIOUS_PAGE_TOKEN = "ptoken";
    public static final String PARAM_NEXT_PAGE_TOKEN = "ntoken";
    public static final String PARAM_LAST_PAGE_TOKEN = "ltoken";
    // ....

    // Page is used only as a path segment not as a query param...
    public static final String PATH_PARAM_PAGE = "page";
    private static final String PATH_PARAM_PAGE_SEGMENT = "/" + PATH_PARAM_PAGE + "/";
    // ....

    // Special constants.
    public static final String PAGER_PAGE_FIRST = "first";
    public static final String PAGER_PAGE_LAST = "last";
    // ....


    private PagingUtil() {}


    public static String getParamPCM(HttpServletRequest request)
    {
        String paramPCM = null;
        if(request != null) {
            paramPCM = request.getParameter(PARAM_PCM);
        }
        return paramPCM;
    }


    public static Long getParamOffset(HttpServletRequest request)
    {
        Long paramOffset = null;
        if(request != null) {
            String strOffset = request.getParameter(PARAM_OFFSET);
            if(strOffset != null) {
                try {
                    paramOffset = Long.parseLong(strOffset);
                } catch(NumberFormatException e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: offset = " + strOffset, e);
                }
            }
        }
        return paramOffset;
    }

    public static Integer getParamCount(HttpServletRequest request)
    {
        Integer paramCount = null;
        if(request != null) {
            String strCount = request.getParameter(PARAM_COUNT);
            if(strCount != null) {
                try {
                    paramCount = Integer.parseInt(strCount);
                } catch(NumberFormatException e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: count = " + strCount, e);
                }
            }
        }
        return paramCount;
    }

    public static Integer getParamPageSize(HttpServletRequest request)
    {
        Integer paramPageSize = null;
        if(request != null) {
            String strPageSize = request.getParameter(PARAM_PAGESIZE);
            if(strPageSize != null && !strPageSize.isEmpty()) {
                try {
                    paramPageSize = Integer.parseInt(strPageSize);
                } catch(NumberFormatException e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: count = " + strPageSize, e);
                }
            }
            if(paramPageSize == null) {
                // Use count param as a fallback....
                paramPageSize = getParamCount(request);
            }
        }
        return paramPageSize;
    }

    public static String getParamOrdering(HttpServletRequest request)
    {
        String paramOrdering = null;
        if(request != null) {
            paramOrdering = request.getParameter(PARAM_ORDERING);
        }
        return paramOrdering;
    }


    public static String getParamCurrentPageCursor(HttpServletRequest request)
    {
        String paramCursor = null;
        if(request != null) {
            paramCursor = request.getParameter(PARAM_CURRENT_PAGE_CURSOR);
        }
        return paramCursor;
    }

    // The following three methods not being used
    public static String getParamPreviousPageCursor(HttpServletRequest request)
    {
        String paramCursor = null;
        if(request != null) {
            paramCursor = request.getParameter(PARAM_PREVIOUS_PAGE_CURSOR);
        }
        return paramCursor;
    }
    public static String getParamNextPageCursor(HttpServletRequest request)
    {
        String paramCursor = null;
        if(request != null) {
            paramCursor = request.getParameter(PARAM_NEXT_PAGE_CURSOR);
        }
        return paramCursor;
    }
    public static String getParamLastPageCursor(HttpServletRequest request)
    {
        String paramCursor = null;
        if(request != null) {
            paramCursor = request.getParameter(PARAM_LAST_PAGE_CURSOR);
        }
        return paramCursor;
    }


    public static String getParamCurrentPageToken(HttpServletRequest request)
    {
        String paramToken = null;
        if(request != null) {
            paramToken = request.getParameter(PARAM_CURRENT_PAGE_TOKEN);
        }
        return paramToken;
    }

    // The following three methods not being used
    public static String getParamPreviousPageToken(HttpServletRequest request)
    {
        String paramToken = null;
        if(request != null) {
            paramToken = request.getParameter(PARAM_PREVIOUS_PAGE_TOKEN);
        }
        return paramToken;
    }
    public static String getParamNextPageToken(HttpServletRequest request)
    {
        String paramToken = null;
        if(request != null) {
            paramToken = request.getParameter(PARAM_NEXT_PAGE_TOKEN);
        }
        return paramToken;
    }
    public static String getParamLastPageToken(HttpServletRequest request)
    {
        String paramToken = null;
        if(request != null) {
            paramToken = request.getParameter(PARAM_LAST_PAGE_TOKEN);
        }
        return paramToken;
    }

    
    // Note:
    // The following pair of methods are tyring to be too general,
    // and in some cases that can be problem.
    // For example, if a url has a form like ".../itemcount/37" or ".../zipcode/94040", etc.,
    //   then the trailing 37 or 94040 may be parsed as a page number according to the implementation used below,
    //   which may not be what we want. 
    // In such cases, as a workaround, 
    //   use explicit page number at the end. E.g., ".../itemcount/37/0" or ".../zipcode/94040/0".
    // TBD: Or 
    //   either (1) we can implement a new pair of methods
    //   which only accept a URL form ".../page/x/..." but not ".../x".
    //   or (2) Use an extra flag to turn on/off the trailing page number support.
    // We use the second alternative, using the flag disallowTrailingPageIndex.
    // ....

    
    // Parses the pathInfo to get the page index.
    //   e.g., from URL ".../delegate/page/3"...
    // the page index should be the last part of the pathInfo, if "page" path segment is not present.
    //   e.g., ".../abc/5" is valid as well as ".../page/5/abc". but "5" in ".../abc/5/xyz" is not a page index.
    // we support two additional special constants, "first" and "last", in place of the page number.
    // if the path pattern, "page/number", is included in the pathInfo, 
    //   then it takes precedence over the end-of-the-path page index (even if it's present).
    // TBD: Use Integer instead of Long ???
    public static Long getPageIndexFromPathInfo(String pathInfo)
    {
        return getPageIndexFromPathInfo(pathInfo, false);
    }
    public static Long getPageIndexFromPathInfo(String pathInfo, boolean disallowTrailingPageIndex)
    {
        if(pathInfo == null || pathInfo.isEmpty() || pathInfo.equals("/")) {
            return null;
        }
        
        Long page = null;

        // [1] ".../page/index/..." pattern
        int len = pathInfo.length();
        int segLen = PATH_PARAM_PAGE_SEGMENT.length();
        int idx1 = pathInfo.lastIndexOf(PATH_PARAM_PAGE_SEGMENT);
        if(idx1 > -1 && idx1 + segLen < len) {
            int idx2 = pathInfo.indexOf("/", idx1 + segLen);
            String pageStr = null;
            if(idx2 == -1) {
                pageStr = pathInfo.substring(idx1 + segLen);
            } else {
                pageStr = pathInfo.substring(idx1 + segLen, idx2);
            }
            if(pageStr.equals(PAGER_PAGE_FIRST)) {
                page = 0L;
            } else if(pageStr.equals(PAGER_PAGE_LAST)) {
                // ????
                page = -1L;  // == last page ????
            } else {
                try {
                    page = Long.valueOf(pageStr);
                } catch(NumberFormatException e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: page = " + pageStr, e);
                }
            }
        } else {
            // ignore
        }

        // [2] ".../index" pattern... (Note: ".../page/index" is handled by [1])
        if(page == null) {
            if(disallowTrailingPageIndex == false) {
                if(!pathInfo.endsWith("/")) {
                    int idx3 = pathInfo.lastIndexOf("/");
                    String pageStr = null;
                    if(idx3 > -1) {
                        pageStr = pathInfo.substring(idx3 + 1);
                    } else {
                        // this cannot happen
                        pageStr = pathInfo;   // ????
                    }
                    if(pageStr.equals(PAGER_PAGE_FIRST)) {
                        page = 0L;
                    } else if(pageStr.equals(PAGER_PAGE_LAST)) {
                        // ????
                        page = -1L;  // == last page ????
                    } else {
                        try {
                            page = Long.valueOf(pageStr);
                        } catch(NumberFormatException e) {
                            // ignore
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: page = " + pageStr, e);
                        }
                    }
                } else {
                    // No page index part found...
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid URL and/or no page param specified: pathInfo = " + pathInfo);
                }
            }
        }

        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "page info found, or not found, from pathInfo = " + pathInfo + "; page = " + page);
        return page;
    }


    // TBD:
    // Note: requestURI excludes the query param string.
    public static String buildRequestURIWithPageIndex(String requestURI, Long pageIndex)
    {
        return buildRequestURIWithPageIndex(requestURI, pageIndex, false);
    }
    public static String buildRequestURIWithPageIndex(String requestURI, Long pageIndex, boolean disallowTrailingPageIndex)
    {
        if(requestURI == null) {
            requestURI = "";   // ??? "/" ???
        }
        if(pageIndex == null) {
            pageIndex = 0L;
        }

        String firstPart = "";
        String lastPart = "";

        Long currentPageIndex = null;

        // [1] ".../page/index/..." pattern
        int len = requestURI.length();
        int segLen = PATH_PARAM_PAGE_SEGMENT.length();
        int idx1 = requestURI.lastIndexOf(PATH_PARAM_PAGE_SEGMENT);
        if(idx1 > -1 && idx1 + segLen < len) {
            int idx2 = requestURI.indexOf("/", idx1 + segLen);
            String pageStr = null;
            if(idx2 == -1) {
                pageStr = requestURI.substring(idx1 + segLen);
            } else {
                pageStr = requestURI.substring(idx1 + segLen, idx2);
            }
            if(pageStr.equals(PAGER_PAGE_FIRST)) {
                currentPageIndex = 0L;
            } else if(pageStr.equals(PAGER_PAGE_LAST)) {
                // ????
                currentPageIndex = -1L;  // == last page ????
            } else {
                try {
                    currentPageIndex = Long.valueOf(pageStr);
                } catch(NumberFormatException e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: page = " + pageStr, e);
                }
            }
            if(currentPageIndex != null) {
                firstPart = requestURI.substring(0, idx1 + segLen);
                if(idx2 == -1) {
                    lastPart = "";
                } else {
                    lastPart = requestURI.substring(idx2);
                }
            }
        } else {
            // ignore
        }

        // [2] ".../index" pattern... (Note: ".../page/index" is handled by [1])
        if(currentPageIndex == null) {
            if(disallowTrailingPageIndex == false) {
                if(!requestURI.endsWith("/")) {
                    int idx3 = requestURI.lastIndexOf("/");
                    String pageStr = null;
                    if(idx3 > -1) {
                        pageStr = requestURI.substring(idx3 + 1);
                    } else {
                        // this cannot happen
                        pageStr = requestURI;   // ????
                    }
                    if(pageStr.equals(PAGER_PAGE_FIRST)) {
                        currentPageIndex = 0L;
                    } else if(pageStr.equals(PAGER_PAGE_LAST)) {
                        // ????
                        currentPageIndex = -1L;  // == last page ????
                    } else {
                        try {
                            currentPageIndex = Long.valueOf(pageStr);
                        } catch(NumberFormatException e) {
                            // ignore
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: page = " + pageStr, e);
                        }
                    }
                    if(currentPageIndex != null) {
                        if(idx3 > -1) {
                            firstPart = requestURI.substring(0, idx3 + 1);
                        } else {
                            firstPart = requestURI + "/";
                        }
                        lastPart = "";
                    }
                } else {
                    // Ingore...
                }
            }
        }

        if(currentPageIndex == null) {
            // No page index part found...
            // Just append the pageIndex at the end... ???
            if(requestURI.endsWith("/")) {
                firstPart = requestURI;
            } else {
                firstPart = requestURI + "/";
            }
            if(disallowTrailingPageIndex == true) {
                firstPart += PATH_PARAM_PAGE + "/";
            }
            lastPart = "";
        }

        String newRequestURI = firstPart + pageIndex + lastPart;

        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "newRequestURI = " + newRequestURI);
        return newRequestURI;
    }


}
