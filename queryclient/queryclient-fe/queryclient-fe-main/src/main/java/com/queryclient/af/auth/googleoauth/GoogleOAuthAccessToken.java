package com.queryclient.af.auth.googleoauth;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import java.util.logging.Level;


// Encapsulates the Google auth information.
public final class GoogleOAuthAccessToken implements Serializable
{
    private static final Logger log = Logger.getLogger(GoogleOAuthAccessToken.class.getName());
    private static final long serialVersionUID = 1L;

    // "Read-only" variables. 
    private final String accessToken;
    private final String refreshToken;
    private final long expirationTime;
    private final String tokenType;
    private final String idToken;       // OpenID Connect, id token.
    // This is also read-only, but we allow to change its value for convenience.
    // Note that the accessToken/refreshToken is valid only for the given dataScopes.
    private final Set<String> dataScopes;

    // Copy ctor.
    public GoogleOAuthAccessToken(GoogleOAuthAccessToken oauthToken)
    {
        this( 
            (oauthToken != null) ? oauthToken.getAccessToken() : null,
            (oauthToken != null) ? oauthToken.getRefreshToken() : null,
            (oauthToken != null) ? oauthToken.getExpirationTime() : 0L,
            (oauthToken != null) ? oauthToken.getTokenType() : null,
            (oauthToken != null) ? oauthToken.getIdToken() : null
        );
    }
    public GoogleOAuthAccessToken(String accessToken, String refreshToken)
    {
        this(accessToken, refreshToken, 0L);
    }
    public GoogleOAuthAccessToken(String accessToken, String refreshToken, long expirationTime)
    {
        this(accessToken, refreshToken, expirationTime, null);
    }
    public GoogleOAuthAccessToken(String accessToken, String refreshToken, long expirationTime, String tokenType)
    {
        this(accessToken, refreshToken, expirationTime, tokenType, null);
    }
    public GoogleOAuthAccessToken(String accessToken, String refreshToken, long expirationTime, String tokenType, String idToken)
    {
        super();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        if(expirationTime > 0L) {
            this.expirationTime = expirationTime;
        } else {
            this.expirationTime = System.currentTimeMillis();    // ????
        }
        if(tokenType != null && !tokenType.isEmpty()) {
            this.tokenType = tokenType;
        } else {
            this.tokenType = "Bearer";      // Always this value according to Google OAuth2 doc.      
        }
        this.idToken = idToken;
        dataScopes = new HashSet<>();
    }

    // The object should contain at least accessToken, otherwise it is "invalid".
    // Non-null/non-empty accessToken does not mean, the token is valid. (E.g., it might have expired.)
    // This method simply returns true if the accessToken is set.
    public boolean isValid()
    {
        return (accessToken != null && !accessToken.isEmpty());
    }

    // Returns true if refreshToken is set.
    // Again, having refreshToken does not mean it's refreshable since the token might be invalid/revoked, etc.
    // It just returns true if at least non-null/non-empty refreshToken is set.
    public boolean isRefreshable()
    {
        return (refreshToken != null && !refreshToken.isEmpty());
    }

    // Returns true if the current access token has expired according to the expiration time.
    public boolean isExpired()
    {
        long now = System.currentTimeMillis();
        if(expirationTime <= now) {
            return true;
        } else {
            return false;
        }
    }

    
    // Getters only
    
    public String getAccessToken()
    {
        return accessToken;
    }

    public String getRefreshToken()
    {
        return refreshToken;
    }

    public long getExpirationTime()
    {
        return expirationTime;
    }

    public String getTokenType()
    {
        return tokenType;
    }

    public String getIdToken()
    {
        return idToken;
    }


    // Note:
    // DataScopes is really read-only. 
    // Fixed value which is used during the original one-time authorization code request.
    // But, for convenience, we allow setting the list element after the object is created.
    
    public Set<String> getDataScopes()
    {
        return dataScopes;
    }
    public boolean addDataScope(String scope)
    {
        return dataScopes.add(scope);
    }
    public boolean addDataScopes(Collection<String> scopes)
    {
        return dataScopes.addAll(scopes);
    }
    public boolean setDataScopes(Collection<String> scopes)
    {
        dataScopes.clear();
        return dataScopes.addAll(scopes);
    }
    public boolean removeDataScope(String scope)
    {
        return dataScopes.remove(scope);
    }
    public boolean removeDataScopes(Collection<String> scopes)
    {
        return dataScopes.removeAll(scopes);
    }
    public void clearDataScopes()
    {
        dataScopes.clear();
    }


    // For debugging purposes only...
    // Since this object contains sensitive data,
    // this should NEVER be logged.
    @Override
    public String toString()
    {
        log.warning("GoogleOAuthAccessToken should never be logged.");
        return "GoogleOAuthAccessToken [accessToken=" + accessToken
                + ", refreshToken=" + refreshToken + ", expirationTime="
                + expirationTime + ", tokenType=" + tokenType + ", idToken="
                + idToken + ", dataScopes=" + dataScopes + "]";
    }

}
