package com.queryclient.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.service.manager.ServiceManager;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.fe.bean.QuerySessionJsBean;
import com.queryclient.wa.util.ReferrerInfoStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class QuerySessionWebService // implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(QuerySessionWebService.class.getName());
     
    // Af service interface.
    private QuerySessionService mService = null;

    public QuerySessionWebService()
    {
        this(ServiceManager.getQuerySessionService());
    }
    public QuerySessionWebService(QuerySessionService service)
    {
        mService = service;
    }
    
    private QuerySessionService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getQuerySessionService();
        }
        return mService;
    }
    
    
    public QuerySessionJsBean getQuerySession(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            QuerySession querySession = getService().getQuerySession(guid);
            QuerySessionJsBean bean = convertQuerySessionToJsBean(querySession);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getQuerySession(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getQuerySession(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QuerySessionJsBean> getQuerySessions(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QuerySessionJsBean> jsBeans = new ArrayList<QuerySessionJsBean>();
            List<QuerySession> querySessions = getService().getQuerySessions(guids);
            if(querySessions != null) {
                for(QuerySession querySession : querySessions) {
                    jsBeans.add(convertQuerySessionToJsBean(querySession));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QuerySessionJsBean> getAllQuerySessions() throws WebException
    {
        return getAllQuerySessions(null, null, null);
    }

    // @Deprecated
    public List<QuerySessionJsBean> getAllQuerySessions(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllQuerySessions(ordering, offset, count, null);
    }

    public List<QuerySessionJsBean> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QuerySessionJsBean> jsBeans = new ArrayList<QuerySessionJsBean>();
            List<QuerySession> querySessions = getService().getAllQuerySessions(ordering, offset, count, forwardCursor);
            if(querySessions != null) {
                for(QuerySession querySession : querySessions) {
                    jsBeans.add(convertQuerySessionToJsBean(querySession));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<QuerySessionJsBean> findQuerySessions(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<QuerySessionJsBean> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<QuerySessionJsBean> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QuerySessionJsBean> jsBeans = new ArrayList<QuerySessionJsBean>();
            List<QuerySession> querySessions = getService().findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(querySessions != null) {
                for(QuerySession querySession : querySessions) {
                    jsBeans.add(convertQuerySessionToJsBean(querySession));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStructJsBean referrerInfo, String status, String note) throws WebException
    {
        try {
            return getService().createQuerySession(user, dataService, serviceUrl, inputFormat, outputFormat, ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createQuerySession(String jsonStr) throws WebException
    {
        return createQuerySession(QuerySessionJsBean.fromJsonString(jsonStr));
    }

    public String createQuerySession(QuerySessionJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QuerySession querySession = convertQuerySessionJsBeanToBean(jsBean);
            return getService().createQuerySession(querySession);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public QuerySessionJsBean constructQuerySession(String jsonStr) throws WebException
    {
        return constructQuerySession(QuerySessionJsBean.fromJsonString(jsonStr));
    }

    public QuerySessionJsBean constructQuerySession(QuerySessionJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QuerySession querySession = convertQuerySessionJsBeanToBean(jsBean);
            querySession = getService().constructQuerySession(querySession);
            jsBean = convertQuerySessionToJsBean(querySession);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStructJsBean referrerInfo, String status, String note) throws WebException
    {
        try {
            return getService().updateQuerySession(guid, user, dataService, serviceUrl, inputFormat, outputFormat, ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateQuerySession(String jsonStr) throws WebException
    {
        return updateQuerySession(QuerySessionJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateQuerySession(QuerySessionJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QuerySession querySession = convertQuerySessionJsBeanToBean(jsBean);
            return getService().updateQuerySession(querySession);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public QuerySessionJsBean refreshQuerySession(String jsonStr) throws WebException
    {
        return refreshQuerySession(QuerySessionJsBean.fromJsonString(jsonStr));
    }

    public QuerySessionJsBean refreshQuerySession(QuerySessionJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QuerySession querySession = convertQuerySessionJsBeanToBean(jsBean);
            querySession = getService().refreshQuerySession(querySession);
            jsBean = convertQuerySessionToJsBean(querySession);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteQuerySession(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteQuerySession(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteQuerySession(QuerySessionJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QuerySession querySession = convertQuerySessionJsBeanToBean(jsBean);
            return getService().deleteQuerySession(querySession);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteQuerySessions(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteQuerySessions(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static QuerySessionJsBean convertQuerySessionToJsBean(QuerySession querySession)
    {
        QuerySessionJsBean jsBean = null;
        if(querySession != null) {
            jsBean = new QuerySessionJsBean();
            jsBean.setGuid(querySession.getGuid());
            jsBean.setUser(querySession.getUser());
            jsBean.setDataService(querySession.getDataService());
            jsBean.setServiceUrl(querySession.getServiceUrl());
            jsBean.setInputFormat(querySession.getInputFormat());
            jsBean.setOutputFormat(querySession.getOutputFormat());
            jsBean.setReferrerInfo(ReferrerInfoStructWebUtil.convertReferrerInfoStructToJsBean(querySession.getReferrerInfo()));
            jsBean.setStatus(querySession.getStatus());
            jsBean.setNote(querySession.getNote());
            jsBean.setCreatedTime(querySession.getCreatedTime());
            jsBean.setModifiedTime(querySession.getModifiedTime());
        }
        return jsBean;
    }

    public static QuerySession convertQuerySessionJsBeanToBean(QuerySessionJsBean jsBean)
    {
        QuerySessionBean querySession = null;
        if(jsBean != null) {
            querySession = new QuerySessionBean();
            querySession.setGuid(jsBean.getGuid());
            querySession.setUser(jsBean.getUser());
            querySession.setDataService(jsBean.getDataService());
            querySession.setServiceUrl(jsBean.getServiceUrl());
            querySession.setInputFormat(jsBean.getInputFormat());
            querySession.setOutputFormat(jsBean.getOutputFormat());
            querySession.setReferrerInfo(ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            querySession.setStatus(jsBean.getStatus());
            querySession.setNote(jsBean.getNote());
            querySession.setCreatedTime(jsBean.getCreatedTime());
            querySession.setModifiedTime(jsBean.getModifiedTime());
        }
        return querySession;
    }

}
