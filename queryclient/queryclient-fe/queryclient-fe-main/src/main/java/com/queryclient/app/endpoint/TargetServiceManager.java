package com.queryclient.app.endpoint;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.app.helper.DataServiceHelper;
import com.queryclient.app.helper.ServiceEndpointHelper;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.DataService;
import com.queryclient.ws.ServiceEndpoint;
// import com.queryclient.app.core.ConsumerKeySecretPair;


public class TargetServiceManager
{
    private static final Logger log = Logger.getLogger(TargetServiceManager.class.getName());

    // key: serviceName??? service Top level url????
    //     Or, duplicate entries using both serviceName an top-level url????
    // Service url examples:
    //     http://abc.appspot.com/
    //     http://abc.appspot.com/v1  (no "/" in path, without the trail end)
    //     etc...
    private Map<String, DataServiceBean> serviceMap = null;
    
    // temporary
    private Map<String, ConsumerKeySecretPair> keySecretMap = null;
    
    // temporary
    // { "app name" -> [ "web service endpoint" ] }
    private Map<String, Set<String>> serviceUrlSet = null;

    
    private TargetServiceManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static final class TargetServiceManagerHolder
    {
        private static final TargetServiceManager INSTANCE = new TargetServiceManager();
    }

    // Singleton method
    public static TargetServiceManager getInstance()
    {
        return TargetServiceManagerHolder.INSTANCE;
    }

    
    private void init()
    {
        // ...
        serviceMap = new HashMap<String, DataServiceBean>();
        // TBD:
        // Popluate this from data store???
        // ???
        // TBD:
        // When a new dataService is added
        // this should be refreshed...
        // ...
        
        // Note that each service's allowed key/secret pairs are currently defined 
        // in com.<app>.af.resource.registry.OAuthConsumerRegistry
        // and in com.<app>.ws.cert.registry.ConsumerRegistry....
        // ...
        
        // temporary
        keySecretMap = new HashMap<String, ConsumerKeySecretPair>();
        
        // temporary... First element in the set is the "default" url....
        serviceUrlSet = new HashMap<String, Set<String>>();

        
        // Two (duplicate, or multiple) entries per service ????
        
        serviceUrlSet.put("QueryClient", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://queryclient3.appspot.com/v1"})));
        keySecretMap.put("QueryClient", new ConsumerKeySecretPairBean("39576d83-6b08-48ff-9590-ffd5a2bb151b","02c216cc-b8ae-4cc8-8351-6478aac5f9e8"));
        keySecretMap.put("http://queryclient3.appspot.com/v1", new ConsumerKeySecretPairBean("39576d83-6b08-48ff-9590-ffd5a2bb151b","02c216cc-b8ae-4cc8-8351-6478aac5f9e8"));

        serviceUrlSet.put("QueryClientApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.queryclient.com/v1"})));
        keySecretMap.put("QueryClientApp", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4","ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://queryclient4.appspot.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4","ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://www.queryclient.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4","ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        // testing from local host.
        //keySecretMap.put("http://localhost:9989/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4","ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));

        // Localhost for testing. "Universal" key-secrets
        //keySecretMap.put("http://localhost:8888/v1", new ConsumerKeySecretPairBean("c3be89ad-f5fc-4595-9d44-e8990fa1b168","543d122f-43ed-482e-8d84-3c22bfce52ce"));
        //keySecretMap.put("http://localhost:8899/v1", new ConsumerKeySecretPairBean("de14a111-993b-4060-95ab-3de6d028f3bf","284e6652-f00c-4957-9e82-55dd292f9561"));

        serviceUrlSet.put("GAEStat", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://gaestat1.appspot.com/v1"})));
        keySecretMap.put("GAEStat", new ConsumerKeySecretPairBean("225377b4-4907-4b64-a579-47db09f58bcd","53bf9092-7342-42ac-828f-9c48177e8a01"));
        keySecretMap.put("http://gaestat00.appspot.com/v1", new ConsumerKeySecretPairBean("225377b4-4907-4b64-a579-47db09f58bcd","53bf9092-7342-42ac-828f-9c48177e8a01"));
        keySecretMap.put("http://gaestat01.appspot.com/v1", new ConsumerKeySecretPairBean("225377b4-4907-4b64-a579-47db09f58bcd","53bf9092-7342-42ac-828f-9c48177e8a01"));
        keySecretMap.put("http://gaestat02.appspot.com/v1", new ConsumerKeySecretPairBean("225377b4-4907-4b64-a579-47db09f58bcd","53bf9092-7342-42ac-828f-9c48177e8a01"));
        keySecretMap.put("http://gaestat03.appspot.com/v1", new ConsumerKeySecretPairBean("225377b4-4907-4b64-a579-47db09f58bcd","53bf9092-7342-42ac-828f-9c48177e8a01"));
        keySecretMap.put("http://gaestat04.appspot.com/v1", new ConsumerKeySecretPairBean("225377b4-4907-4b64-a579-47db09f58bcd","53bf9092-7342-42ac-828f-9c48177e8a01"));
        keySecretMap.put("http://gaestat05.appspot.com/v1", new ConsumerKeySecretPairBean("225377b4-4907-4b64-a579-47db09f58bcd","53bf9092-7342-42ac-828f-9c48177e8a01"));
        keySecretMap.put("http://gaestat06.appspot.com/v1", new ConsumerKeySecretPairBean("225377b4-4907-4b64-a579-47db09f58bcd","53bf9092-7342-42ac-828f-9c48177e8a01"));
        keySecretMap.put("http://gaestat07.appspot.com/v1", new ConsumerKeySecretPairBean("225377b4-4907-4b64-a579-47db09f58bcd","53bf9092-7342-42ac-828f-9c48177e8a01"));
        keySecretMap.put("http://gaestat08.appspot.com/v1", new ConsumerKeySecretPairBean("225377b4-4907-4b64-a579-47db09f58bcd","53bf9092-7342-42ac-828f-9c48177e8a01"));
        
        serviceUrlSet.put("GAEStatApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://gaestat10.appspot.com/v1"})));
        keySecretMap.put("GAEStatApp", new ConsumerKeySecretPairBean("b612e8a9-de4b-48ac-92b0-51d1a3d47dfe","960ae080-85a4-4869-b8ba-5c0fecc0a8dc"));
        keySecretMap.put("http://gaestat10.appspot.com/v1", new ConsumerKeySecretPairBean("b612e8a9-de4b-48ac-92b0-51d1a3d47dfe","960ae080-85a4-4869-b8ba-5c0fecc0a8dc"));
        keySecretMap.put("http://gaestat11.appspot.com/v1", new ConsumerKeySecretPairBean("b612e8a9-de4b-48ac-92b0-51d1a3d47dfe","960ae080-85a4-4869-b8ba-5c0fecc0a8dc"));
        keySecretMap.put("http://gaestat12.appspot.com/v1", new ConsumerKeySecretPairBean("b612e8a9-de4b-48ac-92b0-51d1a3d47dfe","960ae080-85a4-4869-b8ba-5c0fecc0a8dc"));
        keySecretMap.put("http://gaestat13.appspot.com/v1", new ConsumerKeySecretPairBean("b612e8a9-de4b-48ac-92b0-51d1a3d47dfe","960ae080-85a4-4869-b8ba-5c0fecc0a8dc"));
        keySecretMap.put("http://gaestat14.appspot.com/v1", new ConsumerKeySecretPairBean("b612e8a9-de4b-48ac-92b0-51d1a3d47dfe","960ae080-85a4-4869-b8ba-5c0fecc0a8dc"));
        keySecretMap.put("http://gaestat15.appspot.com/v1", new ConsumerKeySecretPairBean("b612e8a9-de4b-48ac-92b0-51d1a3d47dfe","960ae080-85a4-4869-b8ba-5c0fecc0a8dc"));
        keySecretMap.put("http://gaestat16.appspot.com/v1", new ConsumerKeySecretPairBean("b612e8a9-de4b-48ac-92b0-51d1a3d47dfe","960ae080-85a4-4869-b8ba-5c0fecc0a8dc"));
        keySecretMap.put("http://gaestat17.appspot.com/v1", new ConsumerKeySecretPairBean("b612e8a9-de4b-48ac-92b0-51d1a3d47dfe","960ae080-85a4-4869-b8ba-5c0fecc0a8dc"));
        keySecretMap.put("http://gaestat18.appspot.com/v1", new ConsumerKeySecretPairBean("b612e8a9-de4b-48ac-92b0-51d1a3d47dfe","960ae080-85a4-4869-b8ba-5c0fecc0a8dc"));

        serviceUrlSet.put("AeryId", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://aeryid3.appspot.com/v1"})));
        keySecretMap.put("AeryId", new ConsumerKeySecretPairBean("1ae6ef07-004a-438e-b943-872713d40edc","76d255e9-b1e8-45bd-a831-7ad4553eed84"));
        keySecretMap.put("http://aeryid3.appspot.com/v1", new ConsumerKeySecretPairBean("1ae6ef07-004a-438e-b943-872713d40edc","76d255e9-b1e8-45bd-a831-7ad4553eed84"));

        serviceUrlSet.put("AeryIdApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.aeryid.com/v1"})));
        keySecretMap.put("AeryIdApp", new ConsumerKeySecretPairBean("11c2d699-7b6b-4732-94de-5e11eee62237","cc31b5f2-dcbd-46b6-87a3-6ba8a49496f1"));
        keySecretMap.put("http://aeryid4.appspot.com/v1", new ConsumerKeySecretPairBean("11c2d699-7b6b-4732-94de-5e11eee62237","cc31b5f2-dcbd-46b6-87a3-6ba8a49496f1"));
        keySecretMap.put("http://www.aeryid.com/v1", new ConsumerKeySecretPairBean("11c2d699-7b6b-4732-94de-5e11eee62237","cc31b5f2-dcbd-46b6-87a3-6ba8a49496f1"));
        
        serviceUrlSet.put("MemoDB", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://memodb01.appspot.com/v1"})));
        keySecretMap.put("MemoDB", new ConsumerKeySecretPairBean("5b7a4b21-366e-4e13-b38e-9b299cfe16ae","bf66d4fa-a828-4ef3-b367-6223f4b873cc"));
        keySecretMap.put("http://memodb01.appspot.com/v1", new ConsumerKeySecretPairBean("5b7a4b21-366e-4e13-b38e-9b299cfe16ae","bf66d4fa-a828-4ef3-b367-6223f4b873cc"));

        serviceUrlSet.put("ScribbleMemoApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.scribblememo.com/v1"})));
        keySecretMap.put("ScribbleMemoApp", new ConsumerKeySecretPairBean("c4d017d5-a4ee-42b9-848a-a7770f8828fe","d2928419-cd47-4f87-a617-e7bba48dc47c"));
        keySecretMap.put("http://beta.scribblememo.com/v1", new ConsumerKeySecretPairBean("c4d017d5-a4ee-42b9-848a-a7770f8828fe","d2928419-cd47-4f87-a617-e7bba48dc47c"));
        keySecretMap.put("http://www.scribblememo.com/v1", new ConsumerKeySecretPairBean("c4d017d5-a4ee-42b9-848a-a7770f8828fe","d2928419-cd47-4f87-a617-e7bba48dc47c"));

        serviceUrlSet.put("MailMorTest", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://mailmortest1.appspot.com/v1"})));
        keySecretMap.put("MailMorTest", new ConsumerKeySecretPairBean("94c063ab-9560-4070-a28a-068ea8d3e0ba","5be7e9ac-4adc-4342-83f3-fe533c4788c6"));
        keySecretMap.put("http://mailmortest1.appspot.com/v1", new ConsumerKeySecretPairBean("94c063ab-9560-4070-a28a-068ea8d3e0ba","5be7e9ac-4adc-4342-83f3-fe533c4788c6"));

        serviceUrlSet.put("MailMorTestApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.mailmor.com/v1"})));
        keySecretMap.put("MailMorTestApp", new ConsumerKeySecretPairBean("5154cef1-4761-41a7-8689-4dfcc1533480","fa5d3efa-fbc8-47a5-82b1-cdcb15a8b540"));
        keySecretMap.put("http://mailmortest.appspot.com/v1", new ConsumerKeySecretPairBean("5154cef1-4761-41a7-8689-4dfcc1533480","fa5d3efa-fbc8-47a5-82b1-cdcb15a8b540"));
        keySecretMap.put("http://beta.mailmor.com/v1", new ConsumerKeySecretPairBean("5154cef1-4761-41a7-8689-4dfcc1533480","fa5d3efa-fbc8-47a5-82b1-cdcb15a8b540"));
        keySecretMap.put("http://www.mailmor.com/v1", new ConsumerKeySecretPairBean("5154cef1-4761-41a7-8689-4dfcc1533480","fa5d3efa-fbc8-47a5-82b1-cdcb15a8b540"));

        serviceUrlSet.put("TweetMor", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://tweetmor3.appspot.com/v1"})));
        keySecretMap.put("TweetMor", new ConsumerKeySecretPairBean("5e99ab47-0539-4b59-990a-5e31990db3f5", "d49d550f-2cc4-4e0a-94fb-a335bdd6eb32"));
        keySecretMap.put("http://tweetmor3.appspot.com/v1", new ConsumerKeySecretPairBean("5e99ab47-0539-4b59-990a-5e31990db3f5", "d49d550f-2cc4-4e0a-94fb-a335bdd6eb32"));

        serviceUrlSet.put("TweetMorApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.tweetmor.com/v1"})));
        keySecretMap.put("TweetMorApp", new ConsumerKeySecretPairBean("338d0614-da7e-466f-861c-f8b59ad14e77", "95686033-3ea1-47a4-b5aa-5b1ce7d81305"));
        keySecretMap.put("http://tweetmor4.appspot.com/v1", new ConsumerKeySecretPairBean("338d0614-da7e-466f-861c-f8b59ad14e77", "95686033-3ea1-47a4-b5aa-5b1ce7d81305"));
        keySecretMap.put("http://beta.tweetmor.com/v1", new ConsumerKeySecretPairBean("338d0614-da7e-466f-861c-f8b59ad14e77", "95686033-3ea1-47a4-b5aa-5b1ce7d81305"));
        keySecretMap.put("http://www.tweetmor.com/v1", new ConsumerKeySecretPairBean("338d0614-da7e-466f-861c-f8b59ad14e77", "95686033-3ea1-47a4-b5aa-5b1ce7d81305"));
        keySecretMap.put("http://beta.tweetstoa.com/v1", new ConsumerKeySecretPairBean("338d0614-da7e-466f-861c-f8b59ad14e77", "95686033-3ea1-47a4-b5aa-5b1ce7d81305"));
        keySecretMap.put("http://www.tweetstoa.com/v1", new ConsumerKeySecretPairBean("338d0614-da7e-466f-861c-f8b59ad14e77", "95686033-3ea1-47a4-b5aa-5b1ce7d81305"));
        keySecretMap.put("http://beta.tweetpie.com/v1", new ConsumerKeySecretPairBean("338d0614-da7e-466f-861c-f8b59ad14e77", "95686033-3ea1-47a4-b5aa-5b1ce7d81305"));
        keySecretMap.put("http://www.tweetpie.com/v1", new ConsumerKeySecretPairBean("338d0614-da7e-466f-861c-f8b59ad14e77", "95686033-3ea1-47a4-b5aa-5b1ce7d81305"));
        keySecretMap.put("http://beta.tweetassistant.com/v1", new ConsumerKeySecretPairBean("338d0614-da7e-466f-861c-f8b59ad14e77", "95686033-3ea1-47a4-b5aa-5b1ce7d81305"));
        keySecretMap.put("http://www.tweetassistant.com/v1", new ConsumerKeySecretPairBean("338d0614-da7e-466f-861c-f8b59ad14e77", "95686033-3ea1-47a4-b5aa-5b1ce7d81305"));

        serviceUrlSet.put("TimeSpool", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://timespool3.appspot.com/v1"})));
        keySecretMap.put("TimeSpool", new ConsumerKeySecretPairBean("b1fad8fc-3675-4115-b1b8-e54fa6a05381", "bf159861-9ee3-4d83-8900-5fff75142374"));
        keySecretMap.put("http://timespool3.appspot.com/v1", new ConsumerKeySecretPairBean("b1fad8fc-3675-4115-b1b8-e54fa6a05381", "bf159861-9ee3-4d83-8900-5fff75142374"));

        serviceUrlSet.put("TimeSpoolApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.timespool.com/v1"})));
        keySecretMap.put("TimeSpoolApp", new ConsumerKeySecretPairBean("ff6acc92-9647-43f3-a7ac-f9a43367bcb2", "3eadf25a-c3ef-4fa3-8eea-5fe353e36d5b"));
        keySecretMap.put("http://timespool4.appspot.com/v1", new ConsumerKeySecretPairBean("ff6acc92-9647-43f3-a7ac-f9a43367bcb2", "3eadf25a-c3ef-4fa3-8eea-5fe353e36d5b"));
        keySecretMap.put("http://beta.timespool.com/v1", new ConsumerKeySecretPairBean("ff6acc92-9647-43f3-a7ac-f9a43367bcb2", "3eadf25a-c3ef-4fa3-8eea-5fe353e36d5b"));
        keySecretMap.put("http://www.timespool.com/v1", new ConsumerKeySecretPairBean("ff6acc92-9647-43f3-a7ac-f9a43367bcb2", "3eadf25a-c3ef-4fa3-8eea-5fe353e36d5b"));

        serviceUrlSet.put("InviteDB", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://invitedb01.appspot.com/v1"})));
        keySecretMap.put("InviteDB", new ConsumerKeySecretPairBean("fb5b0d97-8f18-47a1-a6ce-5dad3296e2ad", "b82389bd-f615-409f-b7a9-0a8b13d57e13"));
        keySecretMap.put("http://invitedb01.appspot.com/v1", new ConsumerKeySecretPairBean("fb5b0d97-8f18-47a1-a6ce-5dad3296e2ad", "b82389bd-f615-409f-b7a9-0a8b13d57e13"));

        serviceUrlSet.put("SignupPadApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.signuppad.com/v1"})));
        keySecretMap.put("SignupPadApp", new ConsumerKeySecretPairBean("83f10f07-91ec-48cb-9f9b-0ebf4949e08f", "d543d6ba-fccd-4f19-9bfb-5b798c2d3d2e"));
        keySecretMap.put("http://signuppad2.appspot.com/v1", new ConsumerKeySecretPairBean("83f10f07-91ec-48cb-9f9b-0ebf4949e08f", "d543d6ba-fccd-4f19-9bfb-5b798c2d3d2e"));
        keySecretMap.put("http://beta.signuppad.com/v1", new ConsumerKeySecretPairBean("83f10f07-91ec-48cb-9f9b-0ebf4949e08f", "d543d6ba-fccd-4f19-9bfb-5b798c2d3d2e"));
        keySecretMap.put("http://www.signuppad.com/v1", new ConsumerKeySecretPairBean("83f10f07-91ec-48cb-9f9b-0ebf4949e08f", "d543d6ba-fccd-4f19-9bfb-5b798c2d3d2e"));

        serviceUrlSet.put("TagSilo", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://tagsilo1.appspot.com/v1"})));
        keySecretMap.put("TagSilo", new ConsumerKeySecretPairBean("7b6b2f0a-2029-4ac9-9bd5-97f127db2ed2", "d82f5da8-0c8d-4b70-8365-74f9e9e23743"));
        keySecretMap.put("http://tagsilo1.appspot.com/v1", new ConsumerKeySecretPairBean("7b6b2f0a-2029-4ac9-9bd5-97f127db2ed2", "d82f5da8-0c8d-4b70-8365-74f9e9e23743"));

        serviceUrlSet.put("TagChaseApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.tagchase.com/v1"})));
        keySecretMap.put("TagChaseApp", new ConsumerKeySecretPairBean("a7af79d8-b8f0-403b-bce8-b6116c7b8e48", "51591b3a-8f51-4ee5-bbf6-87e247eb5ef6"));
        keySecretMap.put("http://tagchase.appspot.com/v1", new ConsumerKeySecretPairBean("a7af79d8-b8f0-403b-bce8-b6116c7b8e48", "51591b3a-8f51-4ee5-bbf6-87e247eb5ef6"));
        keySecretMap.put("http://beta.tagchase.com/v1", new ConsumerKeySecretPairBean("a7af79d8-b8f0-403b-bce8-b6116c7b8e48", "51591b3a-8f51-4ee5-bbf6-87e247eb5ef6"));
        keySecretMap.put("http://www.tagchase.com/v1", new ConsumerKeySecretPairBean("a7af79d8-b8f0-403b-bce8-b6116c7b8e48", "51591b3a-8f51-4ee5-bbf6-87e247eb5ef6"));

        serviceUrlSet.put("OpenLetter", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://openLettercc.appspot.com/v1"})));
        keySecretMap.put("OpenLetter", new ConsumerKeySecretPairBean("70c7f5b4-b5c0-46ef-a2ea-1b511b96f238", "1413491e-7e13-4ac6-ab7d-511c49c85894"));
        keySecretMap.put("http://openLettercc.appspot.com/v1", new ConsumerKeySecretPairBean("70c7f5b4-b5c0-46ef-a2ea-1b511b96f238", "1413491e-7e13-4ac6-ab7d-511c49c85894"));

        serviceUrlSet.put("OpenLetterApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.openletter.cc/v1"})));
        keySecretMap.put("OpenLetterApp", new ConsumerKeySecretPairBean("98495a91-d58d-4029-a4e5-8829f1a6d601", "bfb66733-0b2c-4d8c-b874-b194529d2b7f"));
        keySecretMap.put("http://openletter01.appspot.com/v1", new ConsumerKeySecretPairBean("98495a91-d58d-4029-a4e5-8829f1a6d601", "bfb66733-0b2c-4d8c-b874-b194529d2b7f"));
        keySecretMap.put("http://beta.openletter.cc/v1", new ConsumerKeySecretPairBean("98495a91-d58d-4029-a4e5-8829f1a6d601", "bfb66733-0b2c-4d8c-b874-b194529d2b7f"));
        keySecretMap.put("http://www.openletter.cc/v1", new ConsumerKeySecretPairBean("98495a91-d58d-4029-a4e5-8829f1a6d601", "bfb66733-0b2c-4d8c-b874-b194529d2b7f"));

        serviceUrlSet.put("ScrapeDB", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://scrapdb.appspot.com/v1"})));
        keySecretMap.put("ScrapeDB", new ConsumerKeySecretPairBean("807c5740-31e6-4aa3-a6bc-21afa55830e9", "79dce205-002d-4633-a476-5a24819630dc"));
        keySecretMap.put("http://scrapdb.appspot.com/v1", new ConsumerKeySecretPairBean("807c5740-31e6-4aa3-a6bc-21afa55830e9", "79dce205-002d-4633-a476-5a24819630dc"));

        serviceUrlSet.put("ScrapeDBApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.scrapeservice.com/v1"})));
        keySecretMap.put("ScrapeDBApp", new ConsumerKeySecretPairBean("82111b17-6014-4420-ab7d-58ffbed33580", "126f8a5f-47a2-459b-be09-364c978c642f"));
        keySecretMap.put("http://scrapeservice.appspot.com/v1", new ConsumerKeySecretPairBean("82111b17-6014-4420-ab7d-58ffbed33580", "126f8a5f-47a2-459b-be09-364c978c642f"));
        keySecretMap.put("http://beta.scrapeservice.com/v1", new ConsumerKeySecretPairBean("82111b17-6014-4420-ab7d-58ffbed33580", "126f8a5f-47a2-459b-be09-364c978c642f"));
        keySecretMap.put("http://www.scrapeservice.com/v1", new ConsumerKeySecretPairBean("82111b17-6014-4420-ab7d-58ffbed33580", "126f8a5f-47a2-459b-be09-364c978c642f"));

        serviceUrlSet.put("ShoppingMemo", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://shoppingmemo3.appspot.com/v1"})));
        keySecretMap.put("ShoppingMemo", new ConsumerKeySecretPairBean("5f38b474-5290-447c-a20c-578317f85c2e", "254b17eb-e048-4257-b9e1-f0bb5be1d40f"));
        keySecretMap.put("http://shoppingmemo3.appspot.com/v1", new ConsumerKeySecretPairBean("5f38b474-5290-447c-a20c-578317f85c2e", "254b17eb-e048-4257-b9e1-f0bb5be1d40f"));

        serviceUrlSet.put("ShoppingMemoApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.shoppingmemo.com/v1"})));
        keySecretMap.put("ShoppingMemoApp", new ConsumerKeySecretPairBean("802cc411-5f25-4d34-af01-167c67645836", "f23dcadb-b1d0-4687-9df1-555f51f905e3"));
        keySecretMap.put("http://shoppingmemo4.appspot.com/v1", new ConsumerKeySecretPairBean("802cc411-5f25-4d34-af01-167c67645836", "f23dcadb-b1d0-4687-9df1-555f51f905e3"));
        keySecretMap.put("http://beta.shoppingmemo.com/v1", new ConsumerKeySecretPairBean("802cc411-5f25-4d34-af01-167c67645836", "f23dcadb-b1d0-4687-9df1-555f51f905e3"));
        keySecretMap.put("http://www.shoppingmemo.com/v1", new ConsumerKeySecretPairBean("802cc411-5f25-4d34-af01-167c67645836", "f23dcadb-b1d0-4687-9df1-555f51f905e3"));
        
        serviceUrlSet.put("MimeDB", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://mimedb01.appspot.com/v1"})));
        keySecretMap.put("MimeDB", new ConsumerKeySecretPairBean("022f28b0-2cfc-44ed-8354-ad6c9c039f1f", "e6fdd345-0811-4eeb-b9be-e29ea3086dc8"));
        keySecretMap.put("http://mimedb01.appspot.com/v1", new ConsumerKeySecretPairBean("022f28b0-2cfc-44ed-8354-ad6c9c039f1f", "e6fdd345-0811-4eeb-b9be-e29ea3086dc8"));

        serviceUrlSet.put("MimeDBApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.mimedb.com/v1"})));
        keySecretMap.put("MimeDBApp", new ConsumerKeySecretPairBean("562c0a75-a521-46c4-a805-1ee85203c8f6", "17ec0cb8-657d-4ecd-806a-529f9150627f"));
        keySecretMap.put("http://mimedb02.appspot.com/v1", new ConsumerKeySecretPairBean("562c0a75-a521-46c4-a805-1ee85203c8f6", "17ec0cb8-657d-4ecd-806a-529f9150627f"));
        keySecretMap.put("http://beta.mimedb.com/v1", new ConsumerKeySecretPairBean("562c0a75-a521-46c4-a805-1ee85203c8f6", "17ec0cb8-657d-4ecd-806a-529f9150627f"));
        keySecretMap.put("http://www.mimedb.com/v1", new ConsumerKeySecretPairBean("562c0a75-a521-46c4-a805-1ee85203c8f6", "17ec0cb8-657d-4ecd-806a-529f9150627f"));
        keySecretMap.put("http://www.mediastoa.com/v1", new ConsumerKeySecretPairBean("562c0a75-a521-46c4-a805-1ee85203c8f6", "17ec0cb8-657d-4ecd-806a-529f9150627f"));
        keySecretMap.put("http://www.mediastoa.com/v1", new ConsumerKeySecretPairBean("562c0a75-a521-46c4-a805-1ee85203c8f6", "17ec0cb8-657d-4ecd-806a-529f9150627f"));

        serviceUrlSet.put("PageSynopsis", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://pagesynopsis1.appspot.com/v1"})));
        keySecretMap.put("PageSynopsis", new ConsumerKeySecretPairBean("a8d36bfb-c747-40c1-8249-b12f0a8ba49c", "dc1414a4-7acd-479d-b741-51c56d5d6bca"));
        keySecretMap.put("http://pagesynopsis1.appspot.com/v1", new ConsumerKeySecretPairBean("a8d36bfb-c747-40c1-8249-b12f0a8ba49c", "dc1414a4-7acd-479d-b741-51c56d5d6bca"));

        serviceUrlSet.put("PageSynopsisApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.pagesynopsis.com/v1"})));
        keySecretMap.put("PageSynopsisApp", new ConsumerKeySecretPairBean("f4d148d4-f6ca-4c3c-8443-1cf365672a60", "f8835ab5-4a45-433a-a595-e5a5c6498086"));
        keySecretMap.put("http://pagesynopsis2.appspot.com/v1", new ConsumerKeySecretPairBean("f4d148d4-f6ca-4c3c-8443-1cf365672a60", "f8835ab5-4a45-433a-a595-e5a5c6498086"));
        keySecretMap.put("http://beta.pagesynopsis.com/v1", new ConsumerKeySecretPairBean("f4d148d4-f6ca-4c3c-8443-1cf365672a60", "f8835ab5-4a45-433a-a595-e5a5c6498086"));
        keySecretMap.put("http://www.pagesynopsis.com/v1", new ConsumerKeySecretPairBean("f4d148d4-f6ca-4c3c-8443-1cf365672a60", "f8835ab5-4a45-433a-a595-e5a5c6498086"));
        
        serviceUrlSet.put("BuddyWork", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://buddywork01.appspot.com/v1"})));
        keySecretMap.put("BuddyWork", new ConsumerKeySecretPairBean("80cfe787-a839-47e8-b960-e679f0b86212", "bd861999-d4d5-46d9-897c-b025bbe1aef8"));
        keySecretMap.put("http://buddywork01.appspot.com/v1", new ConsumerKeySecretPairBean("80cfe787-a839-47e8-b960-e679f0b86212", "bd861999-d4d5-46d9-897c-b025bbe1aef8"));

        serviceUrlSet.put("BuddyWorkApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.buddywork.com/v1"})));
        keySecretMap.put("BuddyWorkApp", new ConsumerKeySecretPairBean("c0f1acb9-d84c-4c27-aa11-a1d3dd618a88", "86c4b3ea-1140-4d3d-89e6-046ed789b7a4"));
        keySecretMap.put("http://buddywork02.appspot.com/v1", new ConsumerKeySecretPairBean("c0f1acb9-d84c-4c27-aa11-a1d3dd618a88", "86c4b3ea-1140-4d3d-89e6-046ed789b7a4"));
        keySecretMap.put("http://beta.buddywork.com/v1", new ConsumerKeySecretPairBean("c0f1acb9-d84c-4c27-aa11-a1d3dd618a88", "86c4b3ea-1140-4d3d-89e6-046ed789b7a4"));
        keySecretMap.put("http://www.buddywork.com/v1", new ConsumerKeySecretPairBean("c0f1acb9-d84c-4c27-aa11-a1d3dd618a88", "86c4b3ea-1140-4d3d-89e6-046ed789b7a4"));
        
        serviceUrlSet.put("CarpoolBay", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://carpoolbay3.appspot.com/v1"})));
        keySecretMap.put("CarpoolBay", new ConsumerKeySecretPairBean("1916f2a0-6e69-4ec3-89d8-0afa66f6637a", "a01717e7-e7b0-4225-9a8c-6f6d88ba2c0f"));
        keySecretMap.put("http://carpoolbay3.appspot.com/v1", new ConsumerKeySecretPairBean("1916f2a0-6e69-4ec3-89d8-0afa66f6637a", "a01717e7-e7b0-4225-9a8c-6f6d88ba2c0f"));

        serviceUrlSet.put("CarpoolBayApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.carpoolbay.com/v1"})));
        keySecretMap.put("CarpoolBayApp", new ConsumerKeySecretPairBean("953a3a11-5c83-4128-bb47-c1bf43220b74", "96f772c1-ec02-44fd-bc94-fa8e4445e973"));
        keySecretMap.put("http://carpoolbay4.appspot.com/v1", new ConsumerKeySecretPairBean("953a3a11-5c83-4128-bb47-c1bf43220b74", "96f772c1-ec02-44fd-bc94-fa8e4445e973"));
        keySecretMap.put("http://beta.carpoolbay.com/v1", new ConsumerKeySecretPairBean("953a3a11-5c83-4128-bb47-c1bf43220b74", "96f772c1-ec02-44fd-bc94-fa8e4445e973"));
        keySecretMap.put("http://www.carpoolbay.com/v1", new ConsumerKeySecretPairBean("953a3a11-5c83-4128-bb47-c1bf43220b74", "96f772c1-ec02-44fd-bc94-fa8e4445e973"));
        keySecretMap.put("http://beta.ridemoment.com/v1", new ConsumerKeySecretPairBean("953a3a11-5c83-4128-bb47-c1bf43220b74", "96f772c1-ec02-44fd-bc94-fa8e4445e973"));
        keySecretMap.put("http://www.ridemoment.com/v1", new ConsumerKeySecretPairBean("953a3a11-5c83-4128-bb47-c1bf43220b74", "96f772c1-ec02-44fd-bc94-fa8e4445e973"));

        serviceUrlSet.put("MyUrlDB", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://myurldb2.appspot.com/v1"})));
        keySecretMap.put("MyUrlDB", new ConsumerKeySecretPairBean("2c158880-4f81-43db-9d8d-f9b3eee9bac9", "43d08e3d-853d-416d-b992-e2b3ca085116"));
        keySecretMap.put("http://myurldb.appspot.com/v1", new ConsumerKeySecretPairBean("2c158880-4f81-43db-9d8d-f9b3eee9bac9", "43d08e3d-853d-416d-b992-e2b3ca085116"));
        keySecretMap.put("http://myurldb1.appspot.com/v1", new ConsumerKeySecretPairBean("2c158880-4f81-43db-9d8d-f9b3eee9bac9", "43d08e3d-853d-416d-b992-e2b3ca085116"));
        keySecretMap.put("http://myurldb2.appspot.com/v1", new ConsumerKeySecretPairBean("2c158880-4f81-43db-9d8d-f9b3eee9bac9", "43d08e3d-853d-416d-b992-e2b3ca085116"));

        serviceUrlSet.put("CannyUrlApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.su2.us/v1"})));
        keySecretMap.put("CannyUrlApp", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://cannyurl3.appspot.com/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://www.su2.us/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://a.su2.us/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));

        serviceUrlSet.put("FlashUrlApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.mq.ms/v1"})));
        keySecretMap.put("FlashUrlApp", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://flashurl3.appspot.com/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://www.mq.ms/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://a.mq.ms/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));

        serviceUrlSet.put("FemtoUrlApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.fm.gs/v1"})));
        keySecretMap.put("FemtoUrlApp", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://femtourl1.appspot.com/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://www.fa0.us/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://a.fa0.us/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://femtourl3.appspot.com/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://www.fm.gs/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://a.fm.gs/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));

        serviceUrlSet.put("SassyUrlApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.oc.gs/v1"})));
        keySecretMap.put("SassyUrlApp", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://sassyurl3.appspot.com/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://www.oc.gs/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));
        keySecretMap.put("http://a.oc.gs/v1", new ConsumerKeySecretPairBean("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa"));

        serviceUrlSet.put("InviteUrl", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://inviteurl.appspot.com/v1"})));
        keySecretMap.put("InviteUrl", new ConsumerKeySecretPairBean("f1849459-5419-4564-9963-46a9e53ba294", "6185e944-df30-4385-8f05-314c024088aa"));
        keySecretMap.put("http://inviteurl.appspot.com/v1", new ConsumerKeySecretPairBean("f1849459-5419-4564-9963-46a9e53ba294", "6185e944-df30-4385-8f05-314c024088aa"));

        serviceUrlSet.put("InviteUrlApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://inviteurl1.appspot.com/v1"})));
        keySecretMap.put("InviteUrlApp", new ConsumerKeySecretPairBean("80816ee7-e930-4279-90d2-7727df93def3", "9717250d-0453-4cad-90b1-e2379caa4a95"));
        keySecretMap.put("http://inviteurl1.appspot.com/v1", new ConsumerKeySecretPairBean("80816ee7-e930-4279-90d2-7727df93def3", "9717250d-0453-4cad-90b1-e2379caa4a95"));

        serviceUrlSet.put("IngressDB", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://ingressdb01.appspot.com/v1"})));
        keySecretMap.put("IngressDB", new ConsumerKeySecretPairBean("2a4b8bb5-6d0f-4382-bbd6-1856109352ee", "e1b2f647-f104-4065-875f-af6570c36bee"));
        keySecretMap.put("http://ingressdb01.appspot.com/v1", new ConsumerKeySecretPairBean("2a4b8bb5-6d0f-4382-bbd6-1856109352ee", "e1b2f647-f104-4065-875f-af6570c36bee"));
        keySecretMap.put("http://ingressdb11.appspot.com/v1", new ConsumerKeySecretPairBean("2a4b8bb5-6d0f-4382-bbd6-1856109352ee", "e1b2f647-f104-4065-875f-af6570c36bee"));
        keySecretMap.put("http://ingressdb21.appspot.com/v1", new ConsumerKeySecretPairBean("2a4b8bb5-6d0f-4382-bbd6-1856109352ee", "e1b2f647-f104-4065-875f-af6570c36bee"));
        keySecretMap.put("http://ingressdb31.appspot.com/v1", new ConsumerKeySecretPairBean("2a4b8bb5-6d0f-4382-bbd6-1856109352ee", "e1b2f647-f104-4065-875f-af6570c36bee"));

        serviceUrlSet.put("IngressDBApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://ingressdb02.appspot.com/v1"})));
        keySecretMap.put("IngressDBApp", new ConsumerKeySecretPairBean("ddb9bef3-2d6d-4e62-831e-7c6ca2feb60d", "485ca5d5-bfca-4cb7-a5d0-0b13bcbac83c"));
        keySecretMap.put("http://ingressdb02.appspot.com/v1", new ConsumerKeySecretPairBean("ddb9bef3-2d6d-4e62-831e-7c6ca2feb60d", "485ca5d5-bfca-4cb7-a5d0-0b13bcbac83c"));
        keySecretMap.put("http://ingressdb12.appspot.com/v1", new ConsumerKeySecretPairBean("ddb9bef3-2d6d-4e62-831e-7c6ca2feb60d", "485ca5d5-bfca-4cb7-a5d0-0b13bcbac83c"));
        keySecretMap.put("http://ingressdb22.appspot.com/v1", new ConsumerKeySecretPairBean("ddb9bef3-2d6d-4e62-831e-7c6ca2feb60d", "485ca5d5-bfca-4cb7-a5d0-0b13bcbac83c"));
        keySecretMap.put("http://ingressdb32.appspot.com/v1", new ConsumerKeySecretPairBean("ddb9bef3-2d6d-4e62-831e-7c6ca2feb60d", "485ca5d5-bfca-4cb7-a5d0-0b13bcbac83c"));

        serviceUrlSet.put("UrlTally", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://urltally2.appspot.com/v1"})));
        keySecretMap.put("UrlTally", new ConsumerKeySecretPairBean("c502e7c2-1870-420a-b6bc-d80e3954aebf", "c4718f88-6464-4191-9a44-eb90dfbdb2df"));
        keySecretMap.put("http://urltally.appspot.com/v1", new ConsumerKeySecretPairBean("c502e7c2-1870-420a-b6bc-d80e3954aebf", "c4718f88-6464-4191-9a44-eb90dfbdb2df"));
        keySecretMap.put("http://urltally2.appspot.com/v1", new ConsumerKeySecretPairBean("c502e7c2-1870-420a-b6bc-d80e3954aebf", "c4718f88-6464-4191-9a44-eb90dfbdb2df"));

        serviceUrlSet.put("UrlTallyApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://urltally1.appspot.com/v1"})));
        keySecretMap.put("UrlTallyApp", new ConsumerKeySecretPairBean("1d69aa70-9199-4e2e-9bd6-3a01b40299e8", "993a3c2f-f382-4aec-8dbb-0c254d3e0bfe"));
        keySecretMap.put("http://urltally1.appspot.com/v1", new ConsumerKeySecretPairBean("1d69aa70-9199-4e2e-9bd6-3a01b40299e8", "993a3c2f-f382-4aec-8dbb-0c254d3e0bfe"));

        serviceUrlSet.put("FeedTrail", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://feedtrail3.appspot.com/v1"})));
        keySecretMap.put("FeedTrail", new ConsumerKeySecretPairBean("b1180d2c-28f5-4307-a383-121f4f9f734e", "a8085d84-1c4c-4a1a-82c3-134975840da8"));
        keySecretMap.put("http://feedtrail3.appspot.com/v1", new ConsumerKeySecretPairBean("b1180d2c-28f5-4307-a383-121f4f9f734e", "a8085d84-1c4c-4a1a-82c3-134975840da8"));

        serviceUrlSet.put("FeedTrailApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.feedtrail.com/v1"})));
        keySecretMap.put("FeedTrailApp", new ConsumerKeySecretPairBean("7f7b487b-5c30-45c9-ba05-3c344fc3f889", "f2d74e53-7d22-455b-afaa-27ef1ea42ef3"));
        keySecretMap.put("http://feedtrail4.appspot.com/v1", new ConsumerKeySecretPairBean("7f7b487b-5c30-45c9-ba05-3c344fc3f889", "f2d74e53-7d22-455b-afaa-27ef1ea42ef3"));
        keySecretMap.put("http://beta.feedtrail.com/v1", new ConsumerKeySecretPairBean("7f7b487b-5c30-45c9-ba05-3c344fc3f889", "f2d74e53-7d22-455b-afaa-27ef1ea42ef3"));
        keySecretMap.put("http://www.feedtrail.com/v1", new ConsumerKeySecretPairBean("7f7b487b-5c30-45c9-ba05-3c344fc3f889", "f2d74e53-7d22-455b-afaa-27ef1ea42ef3"));

        serviceUrlSet.put("WebStoa", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://webstoa1.appspot.com/v1"})));
        keySecretMap.put("WebStoa", new ConsumerKeySecretPairBean("2c83f1fb-f2c8-4632-8444-150c4133edbc", "8d9adf17-83b1-4a5a-beaa-fb622ad988aa"));
        keySecretMap.put("http://webstoa1.appspot.com/v1", new ConsumerKeySecretPairBean("2c83f1fb-f2c8-4632-8444-150c4133edbc", "8d9adf17-83b1-4a5a-beaa-fb622ad988aa"));

        serviceUrlSet.put("WebStoaApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.webstoa.com/v1"})));
        keySecretMap.put("WebStoaApp", new ConsumerKeySecretPairBean("b624b644-c89e-4b21-9238-3d61ec48a796", "265661d0-f452-482e-855f-aff0e4c4881a"));
        keySecretMap.put("http://webstoa.appspot.com/v1", new ConsumerKeySecretPairBean("b624b644-c89e-4b21-9238-3d61ec48a796", "265661d0-f452-482e-855f-aff0e4c4881a"));
        keySecretMap.put("http://www.webstoa.com/v1", new ConsumerKeySecretPairBean("b624b644-c89e-4b21-9238-3d61ec48a796", "265661d0-f452-482e-855f-aff0e4c4881a"));

        serviceUrlSet.put("DataStoa", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://datastoa1.appspot.com/v1"})));
        keySecretMap.put("DataStoa", new ConsumerKeySecretPairBean("c7f2094b-4f0c-4e81-83ef-bd03ac6eadae", "7c63702a-2440-44bb-a699-e802ce1e6bab"));
        keySecretMap.put("http://datastoa1.appspot.com/v1", new ConsumerKeySecretPairBean("c7f2094b-4f0c-4e81-83ef-bd03ac6eadae", "7c63702a-2440-44bb-a699-e802ce1e6bab"));

        serviceUrlSet.put("DataStoaApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.datastoa.com/v1"})));
        keySecretMap.put("DataStoaApp", new ConsumerKeySecretPairBean("2fd8e243-c348-4550-883b-a3fadc193ea1", "e741e795-3677-4d44-b13a-b62b7b24b0cd"));
        keySecretMap.put("http://datastoa.appspot.com/v1", new ConsumerKeySecretPairBean("2fd8e243-c348-4550-883b-a3fadc193ea1", "e741e795-3677-4d44-b13a-b62b7b24b0cd"));
        keySecretMap.put("http://www.datastoa.com/v1", new ConsumerKeySecretPairBean("2fd8e243-c348-4550-883b-a3fadc193ea1", "e741e795-3677-4d44-b13a-b62b7b24b0cd"));


        serviceUrlSet.put("feedstoa", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://feedstoa.appspot.com/v1"})));
        keySecretMap.put("feedstoa", new ConsumerKeySecretPairBean("c85ba22c-f49c-422c-83bf-9d73717b0f1c", "f585c179-d420-458e-96f8-4d3ab1ef9789"));
        keySecretMap.put("http://feedstoa.appspot.com/v1", new ConsumerKeySecretPairBean("c85ba22c-f49c-422c-83bf-9d73717b0f1c", "f585c179-d420-458e-96f8-4d3ab1ef9789"));
        keySecretMap.put("http://feedstoa0.appspot.com/v1", new ConsumerKeySecretPairBean("c85ba22c-f49c-422c-83bf-9d73717b0f1c", "f585c179-d420-458e-96f8-4d3ab1ef9789"));
        keySecretMap.put("http://feedstoa1.appspot.com/v1", new ConsumerKeySecretPairBean("c85ba22c-f49c-422c-83bf-9d73717b0f1c", "f585c179-d420-458e-96f8-4d3ab1ef9789"));
        keySecretMap.put("http://feedstoa3.appspot.com/v1", new ConsumerKeySecretPairBean("c85ba22c-f49c-422c-83bf-9d73717b0f1c", "f585c179-d420-458e-96f8-4d3ab1ef9789"));
        keySecretMap.put("http://feedstoa5.appspot.com/v1", new ConsumerKeySecretPairBean("c85ba22c-f49c-422c-83bf-9d73717b0f1c", "f585c179-d420-458e-96f8-4d3ab1ef9789"));
        keySecretMap.put("http://feedstoa15.appspot.com/v1", new ConsumerKeySecretPairBean("c85ba22c-f49c-422c-83bf-9d73717b0f1c", "f585c179-d420-458e-96f8-4d3ab1ef9789"));
        keySecretMap.put("http://feedstoa25.appspot.com/v1", new ConsumerKeySecretPairBean("c85ba22c-f49c-422c-83bf-9d73717b0f1c", "f585c179-d420-458e-96f8-4d3ab1ef9789"));

        serviceUrlSet.put("FeedStoaApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.feedstoa.com/v1"})));
        keySecretMap.put("FeedStoaApp", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://www.feedstoa.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa2.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://beta.feedstoa.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa4.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://fetch.feedstoa.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa6.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa7.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa8.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa16.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa17.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa18.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa26.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa27.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));
        keySecretMap.put("http://feedstoa28.appspot.com/v1", new ConsumerKeySecretPairBean("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0"));


        serviceUrlSet.put("cubbychat", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://cubbychat1.appspot.com/v1"})));
        keySecretMap.put("cubbychat", new ConsumerKeySecretPairBean("0ecb83b5-cfb9-4cee-82cd-a4ce964e94e8", "c6c428ab-dfd5-4a2d-8272-ffbc27154106"));
        keySecretMap.put("http://cubbychat1.appspot.com/v1", new ConsumerKeySecretPairBean("0ecb83b5-cfb9-4cee-82cd-a4ce964e94e8", "c6c428ab-dfd5-4a2d-8272-ffbc27154106"));
        keySecretMap.put("http://cubbychat3.appspot.com/v1", new ConsumerKeySecretPairBean("0ecb83b5-cfb9-4cee-82cd-a4ce964e94e8", "c6c428ab-dfd5-4a2d-8272-ffbc27154106"));
        keySecretMap.put("http://statusapi1.appspot.com/v1", new ConsumerKeySecretPairBean("0ecb83b5-cfb9-4cee-82cd-a4ce964e94e8", "c6c428ab-dfd5-4a2d-8272-ffbc27154106"));
        keySecretMap.put("http://statusapi3.appspot.com/v1", new ConsumerKeySecretPairBean("0ecb83b5-cfb9-4cee-82cd-a4ce964e94e8", "c6c428ab-dfd5-4a2d-8272-ffbc27154106"));
        keySecretMap.put("http://statusapi5.appspot.com/v1", new ConsumerKeySecretPairBean("0ecb83b5-cfb9-4cee-82cd-a4ce964e94e8", "c6c428ab-dfd5-4a2d-8272-ffbc27154106"));

        serviceUrlSet.put("CubbyChatApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.cubbychat.com/v1"})));
        keySecretMap.put("CubbyChatApp", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://www.cubbychat.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://beta.cubbychat.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://www.statusapi.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://beta.statusapi.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://sandbox.statusapi.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://cubbychat2.appspot.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://cubbychat4.appspot.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://statusapi2.appspot.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://statusapi4.appspot.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));
        keySecretMap.put("http://statusapi6.appspot.com/v1", new ConsumerKeySecretPairBean("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a"));

        serviceUrlSet.put("urlmover", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://urlmover1.appspot.com/v1"})));
        keySecretMap.put("urlmover", new ConsumerKeySecretPairBean("11f47ee3-4f40-4e5f-9844-60b0004e3c24", "f0ddf029-d67f-4635-a15f-43d04c54e732"));
        keySecretMap.put("http://urlmover1.appspot.com/v1", new ConsumerKeySecretPairBean("11f47ee3-4f40-4e5f-9844-60b0004e3c24", "f0ddf029-d67f-4635-a15f-43d04c54e732"));

        serviceUrlSet.put("UrlMoverApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.urlmover.com/v1"})));
        keySecretMap.put("UrlMoverApp", new ConsumerKeySecretPairBean("08d91e04-e16e-458f-a283-d1eba7adf7c9", "12eb98f3-73cd-4ac7-aade-3fc7d17e2ef4"));
        keySecretMap.put("http://www.urlmover.com/v1", new ConsumerKeySecretPairBean("08d91e04-e16e-458f-a283-d1eba7adf7c9", "12eb98f3-73cd-4ac7-aade-3fc7d17e2ef4"));
        keySecretMap.put("http://urlmover2.appspot.com/v1", new ConsumerKeySecretPairBean("08d91e04-e16e-458f-a283-d1eba7adf7c9", "12eb98f3-73cd-4ac7-aade-3fc7d17e2ef4"));


        serviceUrlSet.put("stylestoa", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://stylestoa1.appspot.com/v1"})));
        keySecretMap.put("stylestoa", new ConsumerKeySecretPairBean("94a50747-dcc0-42c7-bfdc-16b806dbc82d", "53faadf1-f2e6-4c66-8dd5-113a3d65814d"));
        keySecretMap.put("http://stylestoa1.appspot.com/v1", new ConsumerKeySecretPairBean("94a50747-dcc0-42c7-bfdc-16b806dbc82d", "53faadf1-f2e6-4c66-8dd5-113a3d65814d"));

        serviceUrlSet.put("StyleStoaApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.stylestoa.com/v1"})));
        keySecretMap.put("StyleStoaApp", new ConsumerKeySecretPairBean("44bb38f1-5cf2-4bc9-8886-a808c49ccf9c", "578558b2-740f-4934-b8c7-1aadb97b844a"));
        keySecretMap.put("http://www.stylestoa.com/v1", new ConsumerKeySecretPairBean("44bb38f1-5cf2-4bc9-8886-a808c49ccf9c", "578558b2-740f-4934-b8c7-1aadb97b844a"));
        keySecretMap.put("http://stylestoa2.appspot.com/v1", new ConsumerKeySecretPairBean("44bb38f1-5cf2-4bc9-8886-a808c49ccf9c", "578558b2-740f-4934-b8c7-1aadb97b844a"));


        serviceUrlSet.put("imagestoa", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://imagestoa1.appspot.com/v1"})));
        keySecretMap.put("imagestoa", new ConsumerKeySecretPairBean("ca22d006-db60-442d-bb3d-0e16e77396df", "a53d5b9a-fd21-410f-ad06-2795974eea2a"));
        keySecretMap.put("http://imagestoa1.appspot.com/v1", new ConsumerKeySecretPairBean("ca22d006-db60-442d-bb3d-0e16e77396df", "a53d5b9a-fd21-410f-ad06-2795974eea2a"));

        serviceUrlSet.put("ImageStoaApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.imagestoa.com/v1"})));
        keySecretMap.put("ImageStoaApp", new ConsumerKeySecretPairBean("178810b7-2f40-4a2f-9503-b7ebc981c6ed", "63f26d1e-5cda-4fc4-8702-5623e04c0c02"));
        keySecretMap.put("http://www.imagestoa.com/v1", new ConsumerKeySecretPairBean("178810b7-2f40-4a2f-9503-b7ebc981c6ed", "63f26d1e-5cda-4fc4-8702-5623e04c0c02"));
        keySecretMap.put("http://imagestoa2.appspot.com/v1", new ConsumerKeySecretPairBean("178810b7-2f40-4a2f-9503-b7ebc981c6ed", "63f26d1e-5cda-4fc4-8702-5623e04c0c02"));

        
        serviceUrlSet.put("blogstoa", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://blogstoa1.appspot.com/v1"})));
        keySecretMap.put("blogstoa", new ConsumerKeySecretPairBean("a2982bd5-e06d-41db-ac1a-c66ef71569da", "92a9746c-49d0-4cc2-882b-3d1089f526ac"));
        keySecretMap.put("http://blogstoa1.appspot.com/v1", new ConsumerKeySecretPairBean("a2982bd5-e06d-41db-ac1a-c66ef71569da", "92a9746c-49d0-4cc2-882b-3d1089f526ac"));
        keySecretMap.put("http://snipblog1.appspot.com/v1", new ConsumerKeySecretPairBean("a2982bd5-e06d-41db-ac1a-c66ef71569da", "92a9746c-49d0-4cc2-882b-3d1089f526ac"));
        keySecretMap.put("http://blogbeta1.appspot.com/v1", new ConsumerKeySecretPairBean("a2982bd5-e06d-41db-ac1a-c66ef71569da", "92a9746c-49d0-4cc2-882b-3d1089f526ac"));

        serviceUrlSet.put("BlogStoaApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.blogstoa.com/v1"})));
        keySecretMap.put("BlogStoaApp", new ConsumerKeySecretPairBean("b8b9408d-2d00-4724-be6d-f8f52b9faf58", "6612d061-fe23-4fd0-8b7d-7f425b6eee30"));
        keySecretMap.put("http://www.blogstoa.com/v1", new ConsumerKeySecretPairBean("b8b9408d-2d00-4724-be6d-f8f52b9faf58", "6612d061-fe23-4fd0-8b7d-7f425b6eee30"));
        keySecretMap.put("http://blogstoa.appspot.com/v1", new ConsumerKeySecretPairBean("b8b9408d-2d00-4724-be6d-f8f52b9faf58", "6612d061-fe23-4fd0-8b7d-7f425b6eee30"));
        keySecretMap.put("http://blogstoa2.appspot.com/v1", new ConsumerKeySecretPairBean("b8b9408d-2d00-4724-be6d-f8f52b9faf58", "6612d061-fe23-4fd0-8b7d-7f425b6eee30"));
        keySecretMap.put("http://www.snipblog.com/v1", new ConsumerKeySecretPairBean("b8b9408d-2d00-4724-be6d-f8f52b9faf58", "6612d061-fe23-4fd0-8b7d-7f425b6eee30"));
        keySecretMap.put("http://snipblog2.appspot.com/v1", new ConsumerKeySecretPairBean("b8b9408d-2d00-4724-be6d-f8f52b9faf58", "6612d061-fe23-4fd0-8b7d-7f425b6eee30"));
        keySecretMap.put("http://www.cubbyblog.com/v1", new ConsumerKeySecretPairBean("b8b9408d-2d00-4724-be6d-f8f52b9faf58", "6612d061-fe23-4fd0-8b7d-7f425b6eee30"));
        keySecretMap.put("http://cubbyblog2.appspot.com/v1", new ConsumerKeySecretPairBean("b8b9408d-2d00-4724-be6d-f8f52b9faf58", "6612d061-fe23-4fd0-8b7d-7f425b6eee30"));
        keySecretMap.put("http://beta.cubbyblog.com/v1", new ConsumerKeySecretPairBean("b8b9408d-2d00-4724-be6d-f8f52b9faf58", "6612d061-fe23-4fd0-8b7d-7f425b6eee30"));
        keySecretMap.put("http://blogbeta2.appspot.com/v1", new ConsumerKeySecretPairBean("b8b9408d-2d00-4724-be6d-f8f52b9faf58", "6612d061-fe23-4fd0-8b7d-7f425b6eee30"));
        
        
        serviceUrlSet.put("filestoa", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://filestoa1.appspot.com/v1"})));
        keySecretMap.put("filestoa", new ConsumerKeySecretPairBean("3b89ddd0-27ff-4344-a403-5f11f26f46f5", "db25fe5e-ab90-4ba9-a19e-85f3e51e5cf8"));
        keySecretMap.put("http://filestoa1.appspot.com/v1", new ConsumerKeySecretPairBean("3b89ddd0-27ff-4344-a403-5f11f26f46f5", "db25fe5e-ab90-4ba9-a19e-85f3e51e5cf8"));

        serviceUrlSet.put("FileStoaApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.filestoa.com/v1"})));
        keySecretMap.put("FileStoaApp", new ConsumerKeySecretPairBean("4041c55f-6736-4da0-af0e-91c402fd5be4", "ec1f931e-2f6c-4a6e-8f79-4580bd31b749"));
        keySecretMap.put("http://www.filestoa.com/v1", new ConsumerKeySecretPairBean("4041c55f-6736-4da0-af0e-91c402fd5be4", "ec1f931e-2f6c-4a6e-8f79-4580bd31b749"));
        keySecretMap.put("http://filestoa.appspot.com/v1", new ConsumerKeySecretPairBean("4041c55f-6736-4da0-af0e-91c402fd5be4", "ec1f931e-2f6c-4a6e-8f79-4580bd31b749"));

        
        serviceUrlSet.put("cronbay", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://cronbay3.appspot.com/v1"})));
        keySecretMap.put("cronbay", new ConsumerKeySecretPairBean("3f5296cb-51b6-4464-90e8-53f4a27dfea6", "1a373da6-2ae3-4053-9fdf-f5076625658f"));
        keySecretMap.put("http://cronbay3.appspot.com/v1", new ConsumerKeySecretPairBean("3f5296cb-51b6-4464-90e8-53f4a27dfea6", "1a373da6-2ae3-4053-9fdf-f5076625658f"));

        serviceUrlSet.put("CronBayApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.cronbay.com/v1"})));
        keySecretMap.put("CronBayApp", new ConsumerKeySecretPairBean("be08ff70-62c3-4597-8f55-b59ea6a7eb3d", "085134ca-e9d1-4a36-a2da-9cff6cf0c6f8"));
        keySecretMap.put("http://www.cronbay.com/v1", new ConsumerKeySecretPairBean("be08ff70-62c3-4597-8f55-b59ea6a7eb3d", "085134ca-e9d1-4a36-a2da-9cff6cf0c6f8"));
        keySecretMap.put("http://cronbay4.appspot.com/v1", new ConsumerKeySecretPairBean("be08ff70-62c3-4597-8f55-b59ea6a7eb3d", "085134ca-e9d1-4a36-a2da-9cff6cf0c6f8"));

        
        serviceUrlSet.put("scrapedb", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://scrapedb.appspot.com/v1"})));
        keySecretMap.put("scrapedb", new ConsumerKeySecretPairBean("807c5740-31e6-4aa3-a6bc-21afa55830e9", "79dce205-002d-4633-a476-5a24819630dc"));
        keySecretMap.put("http://scrapedb.appspot.com/v1", new ConsumerKeySecretPairBean("807c5740-31e6-4aa3-a6bc-21afa55830e9", "79dce205-002d-4633-a476-5a24819630dc"));

        serviceUrlSet.put("ScrapeDBApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.scrapeservice.com/v1"})));
        keySecretMap.put("ScrapeDBApp", new ConsumerKeySecretPairBean("82111b17-6014-4420-ab7d-58ffbed33580", "126f8a5f-47a2-459b-be09-364c978c642f"));
        keySecretMap.put("http://www.scrapeservice.com/v1", new ConsumerKeySecretPairBean("82111b17-6014-4420-ab7d-58ffbed33580", "126f8a5f-47a2-459b-be09-364c978c642f"));
        keySecretMap.put("http://scrapeservice.appspot.com/v1", new ConsumerKeySecretPairBean("82111b17-6014-4420-ab7d-58ffbed33580", "126f8a5f-47a2-459b-be09-364c978c642f"));

        
        serviceUrlSet.put("crawldb", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://crawldb.appspot.com/v1"})));
        keySecretMap.put("crawldb", new ConsumerKeySecretPairBean("080bd893-033b-458b-a98c-5e6c3b9c836d", "194023ed-1e2d-437d-b25a-5f84edda6496"));
        keySecretMap.put("http://crawldb.appspot.com/v1", new ConsumerKeySecretPairBean("080bd893-033b-458b-a98c-5e6c3b9c836d", "194023ed-1e2d-437d-b25a-5f84edda6496"));

        serviceUrlSet.put("CrawlDBApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.crawlservice.com/v1"})));
        keySecretMap.put("CrawlDBApp", new ConsumerKeySecretPairBean("81c57338-dbd0-4f14-9ed4-18eb787170da", "cdda0a8a-f1c5-4e6f-a77f-64c8ecab8a34"));
        keySecretMap.put("http://www.crawlservice.com/v1", new ConsumerKeySecretPairBean("81c57338-dbd0-4f14-9ed4-18eb787170da", "cdda0a8a-f1c5-4e6f-a77f-64c8ecab8a34"));
        keySecretMap.put("http://crawlservice.appspot.com/v1", new ConsumerKeySecretPairBean("81c57338-dbd0-4f14-9ed4-18eb787170da", "cdda0a8a-f1c5-4e6f-a77f-64c8ecab8a34"));


        serviceUrlSet.put("betastoa", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://betastoa1.appspot.com/v1"})));
        keySecretMap.put("betastoa", new ConsumerKeySecretPairBean("3e3a10b2-9a60-4484-b5be-a4e2ccda52c8", "9609d925-7088-432a-9e3c-f303695c2f3b"));
        keySecretMap.put("http://betastoa1.appspot.com/v1", new ConsumerKeySecretPairBean("3e3a10b2-9a60-4484-b5be-a4e2ccda52c8", "9609d925-7088-432a-9e3c-f303695c2f3b"));

        serviceUrlSet.put("BetaParadeApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.betaparade.com/v1"})));
        keySecretMap.put("BetaParadeApp", new ConsumerKeySecretPairBean("f3cc4a84-a46c-4ac9-81e1-aa1f825d4605", "f9d7a40d-e2e3-4835-91d6-f92b10b4f043"));
        keySecretMap.put("http://www.betaparade.com/v1", new ConsumerKeySecretPairBean("f3cc4a84-a46c-4ac9-81e1-aa1f825d4605", "f9d7a40d-e2e3-4835-91d6-f92b10b4f043"));
        keySecretMap.put("http://betaparade2.appspot.com/v1", new ConsumerKeySecretPairBean("f3cc4a84-a46c-4ac9-81e1-aa1f825d4605", "f9d7a40d-e2e3-4835-91d6-f92b10b4f043"));

        
        serviceUrlSet.put("ideachase", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://ideachase1.appspot.com/v1"})));
        keySecretMap.put("ideachase", new ConsumerKeySecretPairBean("d86dbb33-9822-4cc0-8fb5-1ee0dde650da", "8f991ce4-2d2a-44dd-9869-e89c73e243e2"));
        keySecretMap.put("http://ideachase1.appspot.com/v1", new ConsumerKeySecretPairBean("d86dbb33-9822-4cc0-8fb5-1ee0dde650da", "8f991ce4-2d2a-44dd-9869-e89c73e243e2"));

        serviceUrlSet.put("IdeaChaseApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.ideachase.com/v1"})));
        keySecretMap.put("IdeaChaseApp", new ConsumerKeySecretPairBean("a46ce110-0121-4236-a753-bda200a8605f", "3f3ef562-55c7-47ab-8bd4-6b85a4752038"));
        keySecretMap.put("http://www.ideachase.com/v1", new ConsumerKeySecretPairBean("a46ce110-0121-4236-a753-bda200a8605f", "3f3ef562-55c7-47ab-8bd4-6b85a4752038"));
        keySecretMap.put("http://ideachase1.appspot.com/v1", new ConsumerKeySecretPairBean("a46ce110-0121-4236-a753-bda200a8605f", "3f3ef562-55c7-47ab-8bd4-6b85a4752038"));

        
        serviceUrlSet.put("memotally", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://memotally3.appspot.com/v1"})));
        keySecretMap.put("memotally", new ConsumerKeySecretPairBean("66dc7425-aee3-4f72-a074-c5fe70a17c5b", "b89d5827-73d1-42fb-84e3-c53e8919e537"));
        keySecretMap.put("http://memotally3.appspot.com/v1", new ConsumerKeySecretPairBean("66dc7425-aee3-4f72-a074-c5fe70a17c5b", "b89d5827-73d1-42fb-84e3-c53e8919e537"));

        serviceUrlSet.put("MemoTallyApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.memotally.com/v1"})));
        keySecretMap.put("MemoTallyApp", new ConsumerKeySecretPairBean("c98abba7-6963-4b20-8eb4-c55859f595ef", "06f25e96-00ec-4889-af23-6db3bb82ede2"));
        keySecretMap.put("http://www.memotally.com/v1", new ConsumerKeySecretPairBean("c98abba7-6963-4b20-8eb4-c55859f595ef", "06f25e96-00ec-4889-af23-6db3bb82ede2"));
        keySecretMap.put("http://memotally4.appspot.com/v1", new ConsumerKeySecretPairBean("c98abba7-6963-4b20-8eb4-c55859f595ef", "06f25e96-00ec-4889-af23-6db3bb82ede2"));
        
        
        serviceUrlSet.put("glossdb", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://glossdb1.appspot.com/v1"})));
        keySecretMap.put("glossdb", new ConsumerKeySecretPairBean("cd842635-8650-4d56-addc-b09601993d93", "0b53cf72-daca-4d5e-b300-c15909858f1d"));
        keySecretMap.put("http://glossdb1.appspot.com/v1", new ConsumerKeySecretPairBean("cd842635-8650-4d56-addc-b09601993d93", "0b53cf72-daca-4d5e-b300-c15909858f1d"));

        serviceUrlSet.put("GlossDBApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.glossbin.com/v1"})));
        keySecretMap.put("GlossDBApp", new ConsumerKeySecretPairBean("05026222-8af3-41a9-b17c-e5ebe33ed5e8", "26bbbbc7-4050-41d4-b8a6-4f8931581731"));
        keySecretMap.put("http://www.glossbin.com/v1", new ConsumerKeySecretPairBean("05026222-8af3-41a9-b17c-e5ebe33ed5e8", "26bbbbc7-4050-41d4-b8a6-4f8931581731"));
        keySecretMap.put("http://glossdb2.appspot.com/v1", new ConsumerKeySecretPairBean("05026222-8af3-41a9-b17c-e5ebe33ed5e8", "26bbbbc7-4050-41d4-b8a6-4f8931581731"));


        serviceUrlSet.put("glassmemo", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://glassmemo1.appspot.com/v1"})));
        keySecretMap.put("glassmemo", new ConsumerKeySecretPairBean("75f07ff0-2284-499e-91e8-41f601391bc1", "33a0c93b-30d5-4711-88b1-2a225f9119b4"));
        keySecretMap.put("http://glassmemo1.appspot.com/v1", new ConsumerKeySecretPairBean("75f07ff0-2284-499e-91e8-41f601391bc1", "33a0c93b-30d5-4711-88b1-2a225f9119b4"));
        keySecretMap.put("http://glassterm1.appspot.com/v1", new ConsumerKeySecretPairBean("75f07ff0-2284-499e-91e8-41f601391bc1", "33a0c93b-30d5-4711-88b1-2a225f9119b4"));
        keySecretMap.put("http://glassdiary1.appspot.com/v1", new ConsumerKeySecretPairBean("75f07ff0-2284-499e-91e8-41f601391bc1", "33a0c93b-30d5-4711-88b1-2a225f9119b4"));
        keySecretMap.put("http://glasstodo1.appspot.com/v1", new ConsumerKeySecretPairBean("75f07ff0-2284-499e-91e8-41f601391bc1", "33a0c93b-30d5-4711-88b1-2a225f9119b4"));
        // etc...

        serviceUrlSet.put("GlassMemoApp", new LinkedHashSet<String>(Arrays.asList(new String[]{"http://www.glassmemo.com/v1"})));
        keySecretMap.put("GlassMemoApp", new ConsumerKeySecretPairBean("eaa38f0e-a5eb-4e03-96b6-4277ca0253a1", "158bf755-d0d0-417f-900a-28694a253383"));
        keySecretMap.put("http://www.glassmemo.com/v1", new ConsumerKeySecretPairBean("eaa38f0e-a5eb-4e03-96b6-4277ca0253a1", "158bf755-d0d0-417f-900a-28694a253383"));
        keySecretMap.put("http://glassmemo2.appspot.com/v1", new ConsumerKeySecretPairBean("eaa38f0e-a5eb-4e03-96b6-4277ca0253a1", "158bf755-d0d0-417f-900a-28694a253383"));
        keySecretMap.put("http://www.glassterm.com/v1", new ConsumerKeySecretPairBean("eaa38f0e-a5eb-4e03-96b6-4277ca0253a1", "158bf755-d0d0-417f-900a-28694a253383"));
        keySecretMap.put("http://glassterm2.appspot.com/v1", new ConsumerKeySecretPairBean("eaa38f0e-a5eb-4e03-96b6-4277ca0253a1", "158bf755-d0d0-417f-900a-28694a253383"));
        keySecretMap.put("http://www.glassdiary.com/v1", new ConsumerKeySecretPairBean("eaa38f0e-a5eb-4e03-96b6-4277ca0253a1", "158bf755-d0d0-417f-900a-28694a253383"));
        keySecretMap.put("http://glassdiary2.appspot.com/v1", new ConsumerKeySecretPairBean("eaa38f0e-a5eb-4e03-96b6-4277ca0253a1", "158bf755-d0d0-417f-900a-28694a253383"));
        keySecretMap.put("http://www.glasstodo.com/v1", new ConsumerKeySecretPairBean("eaa38f0e-a5eb-4e03-96b6-4277ca0253a1", "158bf755-d0d0-417f-900a-28694a253383"));
        keySecretMap.put("http://glasstodo2.appspot.com/v1", new ConsumerKeySecretPairBean("eaa38f0e-a5eb-4e03-96b6-4277ca0253a1", "158bf755-d0d0-417f-900a-28694a253383"));
        // etc...


        // Etc....
        // TBD:
        // ...
        
    }
    

    
    // ????
    public String getPrimaryServiceUrl(String serviceName)
    {
        return getPrimaryServiceUrl(serviceName, true);
    }
    public String getPrimaryServiceUrl(String serviceName, boolean checkDB)
    {
        String serviceUrl = null;
        //LinkedHashSet<String> urls = (LinkedHashSet<String>) serviceUrlSet.get(serviceName);
        Set<String> urls = serviceUrlSet.get(serviceName);
        if(urls != null && urls.size() > 0) {
            // Does this work ????
            serviceUrl = urls.toArray(new String[]{})[0];
        } else {
            if(checkDB) {
                DataService dataService = DataServiceHelper.getInstance().getDataServiceByName(serviceName);
                if(dataService != null) {
                    serviceUrl = dataService.getServiceUrl();
                }
                if(serviceUrl == null || serviceUrl.isEmpty()) {
                    ServiceEndpoint serviceEndpoint = ServiceEndpointHelper.getInstance().getServiceEndpointByServiceName(serviceName);
                    if(serviceEndpoint != null) {
                        serviceUrl = serviceEndpoint.getServiceUrl();
                    }
                }
            }
        }
        return serviceUrl;
    }    
    
    
    // ????
    
    // TBD: ...
    // service: serviceName, serviceTopLevelUrl, or resourceUrl ????
    public ConsumerKeySecretPair getKeySecretPair(String service)
    {
        return getKeySecretPair(service, true);
    }
    public ConsumerKeySecretPair getKeySecretPair(String service, boolean checkDB)
    {
        ConsumerKeySecretPair pair = keySecretMap.get(service);
        
        if(pair == null) {
            if(checkDB) {
                ServiceEndpoint serviceEndpoint = ServiceEndpointHelper.getInstance().getServiceEndpointByServiceUrl(service);
                if(serviceEndpoint != null) {
                    pair = serviceEndpoint.getAuthCredential();
                }
                if(pair == null) {
                    DataService dataService = DataServiceHelper.getInstance().getDataServiceByName(service);
                    if(dataService == null) {
                        dataService = DataServiceHelper.getInstance().getDataServiceByServiceUrl(service);
                    }
                    if(dataService != null) {
                        pair = dataService.getAuthCredential();
                    }
                }
            }
        }

        if(pair == null) {
            // TBD:
            String serviceUrl = getServiceURLFromRequestURL(service);
            if(serviceUrl != null) {
                pair = keySecretMap.get(serviceUrl);
                if(pair == null) {
                    if(checkDB) {
                        ServiceEndpoint serviceEndpoint = ServiceEndpointHelper.getInstance().getServiceEndpointByServiceUrl(serviceUrl);
                        if(serviceEndpoint != null) {
                            pair = serviceEndpoint.getAuthCredential();
                        }
                        if(pair == null) {
                            DataService dataService = DataServiceHelper.getInstance().getDataServiceByServiceUrl(serviceUrl);
                            if(dataService != null) {
                                pair = dataService.getAuthCredential();
                            }
                        }
                    }
                }
            }
        }

        // pair can still be null at this point.
        return pair;
    }
    
    
    // temporary implementation.
    private String getServiceURLFromRequestURL(String requestURL)
    {
        if(requestURL == null || requestURL.length() < 10) {
            log.warning("Invalid requestURL.");
            return null;  // ???
        }
        
        String topLevelURL = null;
        int idx1 = requestURL.indexOf("/", 9);
        if(idx1 < 0) {
            log.warning("Invalid requestURL = " + requestURL);
            topLevelURL = requestURL;  // ???          
        } else {
            int idx2 = requestURL.indexOf("/", idx1+1);
            if(idx2 < 0) {
                topLevelURL = requestURL;                
            } else {
                topLevelURL = requestURL.substring(0, idx2);  // Without the trailing "/".
            }
        }
        log.info("getServiceURLFromRequestURL(): topLevelURL = " + topLevelURL);

        return topLevelURL;
    }

}
