package com.queryclient.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ServiceEndpointJsBean;
import com.queryclient.wa.service.ServiceEndpointWebService;
import com.queryclient.wa.service.DataServiceWebService;


public class ServiceEndpointHelper
{
    private static final Logger log = Logger.getLogger(ServiceEndpointHelper.class.getName());

    private DataServiceWebService dataServiceWebService = null;
    private ServiceEndpointWebService mServiceEndpointWebService = null;

    private ServiceEndpointHelper() {}

    // Initialization-on-demand holder.
    private static final class MessageHelperHolder
    {
        private static final ServiceEndpointHelper INSTANCE = new ServiceEndpointHelper();
    }

    // Singleton method
    public static ServiceEndpointHelper getInstance()
    {
        return MessageHelperHolder.INSTANCE;
    }


    private DataServiceWebService getDataServiceWebService()
    {
        if(dataServiceWebService == null) {
            dataServiceWebService = new DataServiceWebService();
        }
        return dataServiceWebService;
    }
    private ServiceEndpointWebService getServiceEndpointWebService()
    {
        if(mServiceEndpointWebService == null) {
            mServiceEndpointWebService = new ServiceEndpointWebService();
        }
        return mServiceEndpointWebService;
    }
    
    
    public ServiceEndpointJsBean getServiceEndpoint(String pageGuid)
    {
        ServiceEndpointJsBean bean = null;
        try {
            bean = getServiceEndpointWebService().getServiceEndpoint(pageGuid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to get the ServiceEndpoint for the gven guid = " + pageGuid, e);
        }
        return bean;
    }
    
    
//    // Returns the template jsp page relative URL....
//    public String getFilePath(ServiceEndpointJsBean pageBean)
//    {
//        if(pageBean == null) {
//            log.info("Failed to construct template file path because the pageBean is null.");
//            return null;  // ???
//        }
//
//        String filePath = null;
//        String templateFile = pageBean.getPageTemplateFilePath();   // Can still include subdir...
//        if(templateFile == null) {
//            log.info("pageBean does not have the templatefile attribute.");
//            String templateCode = pageBean.getPageTemplateCode();
//            if(templateCode == null) {
//                log.warning("Failed to construct template file path because neither templateFile nor templateCode is set for the given pageBean.");
//                return null;
//            } else {
//                filePath = TemplateHelper.getInstance().constructFilePathFromPageTemplateCode(templateCode);
//            }
//        } else {
//            filePath = TemplateUtil.constructFilePathFromPageTemplateFile(templateFile);
//        }
//
//        return filePath;
//    }
   

    // temporary. Is there a better way?????
    public int getTotalCountForDataService(String dataServiceGuid)
    {
        return getTotalCountForDataService(dataServiceGuid, null, null);
    }
    public int getTotalCountForDataService(String dataServiceGuid, Long offset, Integer maxCount)
    {
        int totalCount = 0;
        try {
            String filter = "dataService=='" + dataServiceGuid + "'";   // Status?
            String ordering = "createdTime desc";
            List<String> keys = getServiceEndpointWebService().findServiceEndpointKeys(filter, ordering, null, null, null, null, offset, maxCount);
            if(keys != null) {
                totalCount = keys.size();
            }
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to find beans for given dataService = " + dataServiceGuid, e);
        }
        return totalCount;
    }
    
    
    public List<ServiceEndpointJsBean> findServiceEndpointsForDataService(String dataServiceGuid)
    {
        return findServiceEndpointsForDataService(dataServiceGuid, null, null);
    }

    // TBD: sorting/ordering????
    public List<ServiceEndpointJsBean> findServiceEndpointsForDataService(String dataServiceGuid, Long offset, Integer count)
    {
        List<ServiceEndpointJsBean> beans = null;
        
        try {
            String filter = "dataService=='" + dataServiceGuid + "'";   // Status?
            String ordering = "createdTime desc";
            beans = getServiceEndpointWebService().findServiceEndpoints(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to find beans for given dataService = " + dataServiceGuid, e);
        }

        return beans;
    }

    public List<ServiceEndpointJsBean> findRecentServiceEndpoints(Long offset, Integer count)
    {
        List<ServiceEndpointJsBean> beans = null;
        
        try {
            String filter = null;   // ????
            String ordering = "createdTime desc";
            beans = getServiceEndpointWebService().findServiceEndpoints(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to find recent serviceEndpoints", e);
        }

        return beans;
    }

}
