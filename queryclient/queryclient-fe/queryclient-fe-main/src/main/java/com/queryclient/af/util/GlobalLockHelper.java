package com.queryclient.af.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;

import com.queryclient.ws.ApiConsumer;
import com.queryclient.ws.User;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.UserAuthState;
import com.queryclient.ws.DataService;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.ServiceInfo;
import com.queryclient.ws.FiveTen;


// Temporary
public final class GlobalLockHelper
{
    private static final Logger log = Logger.getLogger(GlobalLockHelper.class.getName());


    // Singleton
    private GlobalLockHelper() {}

    private static final class GlobalLockHelperHolder
    {
        private static final GlobalLockHelper INSTANCE = new GlobalLockHelper();
    }
    public static GlobalLockHelper getInstance()
    {
        return GlobalLockHelperHolder.INSTANCE;
    }


    // temporary
    // Poorman's "global locking" using a distributed cache...
    private static Cache getCache()
    {
        // 30 seconds
        return CacheHelper.getFlashInstance().getCache();
    }


    private static String getApiConsumerLockKey(ApiConsumer apiConsumer)
    {
        // Note: We assume that the arg apiConsumer is not null.
        return "ApiConsumer-Processing-Lock-" + apiConsumer.getGuid();
    }
    public String lockApiConsumer(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            // The stored value is not important.
            String val = apiConsumer.getGuid();
            getCache().put(getApiConsumerLockKey(apiConsumer), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockApiConsumer(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            String val = apiConsumer.getGuid();
            getCache().remove(getApiConsumerLockKey(apiConsumer));
            return val;
        } else {
            return null;
        }
    }
    public boolean isApiConsumerLocked(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            return getCache().containsKey(getApiConsumerLockKey(apiConsumer)); 
        } else {
            return false;
        }
    }

    private static String getUserLockKey(User user)
    {
        // Note: We assume that the arg user is not null.
        return "User-Processing-Lock-" + user.getGuid();
    }
    public String lockUser(User user)
    {
        if(getCache() != null && user != null) {
            // The stored value is not important.
            String val = user.getGuid();
            getCache().put(getUserLockKey(user), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUser(User user)
    {
        if(getCache() != null && user != null) {
            String val = user.getGuid();
            getCache().remove(getUserLockKey(user));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserLocked(User user)
    {
        if(getCache() != null && user != null) {
            return getCache().containsKey(getUserLockKey(user)); 
        } else {
            return false;
        }
    }

    private static String getUserPasswordLockKey(UserPassword userPassword)
    {
        // Note: We assume that the arg userPassword is not null.
        return "UserPassword-Processing-Lock-" + userPassword.getGuid();
    }
    public String lockUserPassword(UserPassword userPassword)
    {
        if(getCache() != null && userPassword != null) {
            // The stored value is not important.
            String val = userPassword.getGuid();
            getCache().put(getUserPasswordLockKey(userPassword), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserPassword(UserPassword userPassword)
    {
        if(getCache() != null && userPassword != null) {
            String val = userPassword.getGuid();
            getCache().remove(getUserPasswordLockKey(userPassword));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserPasswordLocked(UserPassword userPassword)
    {
        if(getCache() != null && userPassword != null) {
            return getCache().containsKey(getUserPasswordLockKey(userPassword)); 
        } else {
            return false;
        }
    }

    private static String getExternalUserAuthLockKey(ExternalUserAuth externalUserAuth)
    {
        // Note: We assume that the arg externalUserAuth is not null.
        return "ExternalUserAuth-Processing-Lock-" + externalUserAuth.getGuid();
    }
    public String lockExternalUserAuth(ExternalUserAuth externalUserAuth)
    {
        if(getCache() != null && externalUserAuth != null) {
            // The stored value is not important.
            String val = externalUserAuth.getGuid();
            getCache().put(getExternalUserAuthLockKey(externalUserAuth), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockExternalUserAuth(ExternalUserAuth externalUserAuth)
    {
        if(getCache() != null && externalUserAuth != null) {
            String val = externalUserAuth.getGuid();
            getCache().remove(getExternalUserAuthLockKey(externalUserAuth));
            return val;
        } else {
            return null;
        }
    }
    public boolean isExternalUserAuthLocked(ExternalUserAuth externalUserAuth)
    {
        if(getCache() != null && externalUserAuth != null) {
            return getCache().containsKey(getExternalUserAuthLockKey(externalUserAuth)); 
        } else {
            return false;
        }
    }

    private static String getUserAuthStateLockKey(UserAuthState userAuthState)
    {
        // Note: We assume that the arg userAuthState is not null.
        return "UserAuthState-Processing-Lock-" + userAuthState.getGuid();
    }
    public String lockUserAuthState(UserAuthState userAuthState)
    {
        if(getCache() != null && userAuthState != null) {
            // The stored value is not important.
            String val = userAuthState.getGuid();
            getCache().put(getUserAuthStateLockKey(userAuthState), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserAuthState(UserAuthState userAuthState)
    {
        if(getCache() != null && userAuthState != null) {
            String val = userAuthState.getGuid();
            getCache().remove(getUserAuthStateLockKey(userAuthState));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserAuthStateLocked(UserAuthState userAuthState)
    {
        if(getCache() != null && userAuthState != null) {
            return getCache().containsKey(getUserAuthStateLockKey(userAuthState)); 
        } else {
            return false;
        }
    }

    private static String getDataServiceLockKey(DataService dataService)
    {
        // Note: We assume that the arg dataService is not null.
        return "DataService-Processing-Lock-" + dataService.getGuid();
    }
    public String lockDataService(DataService dataService)
    {
        if(getCache() != null && dataService != null) {
            // The stored value is not important.
            String val = dataService.getGuid();
            getCache().put(getDataServiceLockKey(dataService), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockDataService(DataService dataService)
    {
        if(getCache() != null && dataService != null) {
            String val = dataService.getGuid();
            getCache().remove(getDataServiceLockKey(dataService));
            return val;
        } else {
            return null;
        }
    }
    public boolean isDataServiceLocked(DataService dataService)
    {
        if(getCache() != null && dataService != null) {
            return getCache().containsKey(getDataServiceLockKey(dataService)); 
        } else {
            return false;
        }
    }

    private static String getServiceEndpointLockKey(ServiceEndpoint serviceEndpoint)
    {
        // Note: We assume that the arg serviceEndpoint is not null.
        return "ServiceEndpoint-Processing-Lock-" + serviceEndpoint.getGuid();
    }
    public String lockServiceEndpoint(ServiceEndpoint serviceEndpoint)
    {
        if(getCache() != null && serviceEndpoint != null) {
            // The stored value is not important.
            String val = serviceEndpoint.getGuid();
            getCache().put(getServiceEndpointLockKey(serviceEndpoint), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockServiceEndpoint(ServiceEndpoint serviceEndpoint)
    {
        if(getCache() != null && serviceEndpoint != null) {
            String val = serviceEndpoint.getGuid();
            getCache().remove(getServiceEndpointLockKey(serviceEndpoint));
            return val;
        } else {
            return null;
        }
    }
    public boolean isServiceEndpointLocked(ServiceEndpoint serviceEndpoint)
    {
        if(getCache() != null && serviceEndpoint != null) {
            return getCache().containsKey(getServiceEndpointLockKey(serviceEndpoint)); 
        } else {
            return false;
        }
    }

    private static String getQuerySessionLockKey(QuerySession querySession)
    {
        // Note: We assume that the arg querySession is not null.
        return "QuerySession-Processing-Lock-" + querySession.getGuid();
    }
    public String lockQuerySession(QuerySession querySession)
    {
        if(getCache() != null && querySession != null) {
            // The stored value is not important.
            String val = querySession.getGuid();
            getCache().put(getQuerySessionLockKey(querySession), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockQuerySession(QuerySession querySession)
    {
        if(getCache() != null && querySession != null) {
            String val = querySession.getGuid();
            getCache().remove(getQuerySessionLockKey(querySession));
            return val;
        } else {
            return null;
        }
    }
    public boolean isQuerySessionLocked(QuerySession querySession)
    {
        if(getCache() != null && querySession != null) {
            return getCache().containsKey(getQuerySessionLockKey(querySession)); 
        } else {
            return false;
        }
    }

    private static String getQueryRecordLockKey(QueryRecord queryRecord)
    {
        // Note: We assume that the arg queryRecord is not null.
        return "QueryRecord-Processing-Lock-" + queryRecord.getGuid();
    }
    public String lockQueryRecord(QueryRecord queryRecord)
    {
        if(getCache() != null && queryRecord != null) {
            // The stored value is not important.
            String val = queryRecord.getGuid();
            getCache().put(getQueryRecordLockKey(queryRecord), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockQueryRecord(QueryRecord queryRecord)
    {
        if(getCache() != null && queryRecord != null) {
            String val = queryRecord.getGuid();
            getCache().remove(getQueryRecordLockKey(queryRecord));
            return val;
        } else {
            return null;
        }
    }
    public boolean isQueryRecordLocked(QueryRecord queryRecord)
    {
        if(getCache() != null && queryRecord != null) {
            return getCache().containsKey(getQueryRecordLockKey(queryRecord)); 
        } else {
            return false;
        }
    }

    private static String getDummyEntityLockKey(DummyEntity dummyEntity)
    {
        // Note: We assume that the arg dummyEntity is not null.
        return "DummyEntity-Processing-Lock-" + dummyEntity.getGuid();
    }
    public String lockDummyEntity(DummyEntity dummyEntity)
    {
        if(getCache() != null && dummyEntity != null) {
            // The stored value is not important.
            String val = dummyEntity.getGuid();
            getCache().put(getDummyEntityLockKey(dummyEntity), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockDummyEntity(DummyEntity dummyEntity)
    {
        if(getCache() != null && dummyEntity != null) {
            String val = dummyEntity.getGuid();
            getCache().remove(getDummyEntityLockKey(dummyEntity));
            return val;
        } else {
            return null;
        }
    }
    public boolean isDummyEntityLocked(DummyEntity dummyEntity)
    {
        if(getCache() != null && dummyEntity != null) {
            return getCache().containsKey(getDummyEntityLockKey(dummyEntity)); 
        } else {
            return false;
        }
    }

    private static String getServiceInfoLockKey(ServiceInfo serviceInfo)
    {
        // Note: We assume that the arg serviceInfo is not null.
        return "ServiceInfo-Processing-Lock-" + serviceInfo.getGuid();
    }
    public String lockServiceInfo(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            // The stored value is not important.
            String val = serviceInfo.getGuid();
            getCache().put(getServiceInfoLockKey(serviceInfo), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockServiceInfo(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            String val = serviceInfo.getGuid();
            getCache().remove(getServiceInfoLockKey(serviceInfo));
            return val;
        } else {
            return null;
        }
    }
    public boolean isServiceInfoLocked(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            return getCache().containsKey(getServiceInfoLockKey(serviceInfo)); 
        } else {
            return false;
        }
    }

    private static String getFiveTenLockKey(FiveTen fiveTen)
    {
        // Note: We assume that the arg fiveTen is not null.
        return "FiveTen-Processing-Lock-" + fiveTen.getGuid();
    }
    public String lockFiveTen(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            // The stored value is not important.
            String val = fiveTen.getGuid();
            getCache().put(getFiveTenLockKey(fiveTen), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockFiveTen(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            String val = fiveTen.getGuid();
            getCache().remove(getFiveTenLockKey(fiveTen));
            return val;
        } else {
            return null;
        }
    }
    public boolean isFiveTenLocked(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            return getCache().containsKey(getFiveTenLockKey(fiveTen)); 
        } else {
            return false;
        }
    }

 
}
