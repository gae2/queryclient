package com.queryclient.util.parser;

import com.queryclient.ws.BaseException;


public class ParseException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public ParseException() 
    {
        super();
    }
    public ParseException(String message) 
    {
        super(message);
    }
    public ParseException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public ParseException(Throwable cause) 
    {
        super(cause);
    }

}
