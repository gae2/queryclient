package com.queryclient.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.UserAuthStateBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.UserAuthStateService;


// UserAuthStateMockService is a decorator.
// It can be used as a base class to mock UserAuthStateService objects.
public abstract class UserAuthStateMockService implements UserAuthStateService
{
    private static final Logger log = Logger.getLogger(UserAuthStateMockService.class.getName());

    // UserAuthStateMockService uses the decorator design pattern.
    private UserAuthStateService decoratedService;

    public UserAuthStateMockService(UserAuthStateService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected UserAuthStateService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(UserAuthStateService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // UserAuthState related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserAuthState getUserAuthState(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserAuthState(): guid = " + guid);
        UserAuthState bean = decoratedService.getUserAuthState(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserAuthState(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getUserAuthState(guid, field);
        return obj;
    }

    @Override
    public List<UserAuthState> getUserAuthStates(List<String> guids) throws BaseException
    {
        log.fine("getUserAuthStates()");
        List<UserAuthState> userAuthStates = decoratedService.getUserAuthStates(guids);
        log.finer("END");
        return userAuthStates;
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates() throws BaseException
    {
        return getAllUserAuthStates(null, null, null);
    }


    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserAuthStates(ordering, offset, count, null);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserAuthStates(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<UserAuthState> userAuthStates = decoratedService.getAllUserAuthStates(ordering, offset, count, forwardCursor);
        log.finer("END");
        return userAuthStates;
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserAuthStateKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserAuthStateKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllUserAuthStateKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserAuthStates(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserAuthStateMockService.findUserAuthStates(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<UserAuthState> userAuthStates = decoratedService.findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return userAuthStates;
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserAuthStateMockService.findUserAuthStateKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserAuthStateMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUserAuthState(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        ExternalUserIdStructBean externalIdBean = null;
        if(externalId instanceof ExternalUserIdStructBean) {
            externalIdBean = (ExternalUserIdStructBean) externalId;
        } else if(externalId instanceof ExternalUserIdStruct) {
            externalIdBean = new ExternalUserIdStructBean(externalId.getUuid(), externalId.getId(), externalId.getName(), externalId.getEmail(), externalId.getUsername(), externalId.getOpenId(), externalId.getNote());
        } else {
            externalIdBean = null;   // ????
        }
        UserAuthStateBean bean = new UserAuthStateBean(null, managerApp, appAcl, gaeAppBean, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalIdBean, status, firstAuthTime, lastAuthTime, expirationTime);
        return createUserAuthState(bean);
    }

    @Override
    public String createUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createUserAuthState(userAuthState);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public UserAuthState constructUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");
        UserAuthState bean = decoratedService.constructUserAuthState(userAuthState);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateUserAuthState(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        ExternalUserIdStructBean externalIdBean = null;
        if(externalId instanceof ExternalUserIdStructBean) {
            externalIdBean = (ExternalUserIdStructBean) externalId;
        } else if(externalId instanceof ExternalUserIdStruct) {
            externalIdBean = new ExternalUserIdStructBean(externalId.getUuid(), externalId.getId(), externalId.getName(), externalId.getEmail(), externalId.getUsername(), externalId.getOpenId(), externalId.getNote());
        } else {
            externalIdBean = null;   // ????
        }
        UserAuthStateBean bean = new UserAuthStateBean(guid, managerApp, appAcl, gaeAppBean, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalIdBean, status, firstAuthTime, lastAuthTime, expirationTime);
        return updateUserAuthState(bean);
    }
        
    @Override
    public Boolean updateUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateUserAuthState(userAuthState);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public UserAuthState refreshUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");
        UserAuthState bean = decoratedService.refreshUserAuthState(userAuthState);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteUserAuthState(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteUserAuthState(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteUserAuthState(userAuthState);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUserAuthStates(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteUserAuthStates(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUserAuthStates(List<UserAuthState> userAuthStates) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createUserAuthStates(userAuthStates);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateUserAuthStates(List<UserAuthState> userAuthStates) throws BaseException
    //{
    //}

}
