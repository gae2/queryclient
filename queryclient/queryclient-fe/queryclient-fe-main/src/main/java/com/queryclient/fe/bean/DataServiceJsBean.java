package com.queryclient.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class DataServiceJsBean implements Serializable, Cloneable  //, DataService
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DataServiceJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String user;
    private String name;
    private String description;
    private String type;
    private String mode;
    private String serviceUrl;
    private Boolean authRequired;
    private ConsumerKeySecretPairJsBean authCredential;
    private ReferrerInfoStructJsBean referrerInfo;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public DataServiceJsBean()
    {
        //this((String) null);
    }
    public DataServiceJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public DataServiceJsBean(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairJsBean authCredential, ReferrerInfoStructJsBean referrerInfo, String status)
    {
        this(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status, null, null);
    }
    public DataServiceJsBean(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairJsBean authCredential, ReferrerInfoStructJsBean referrerInfo, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.name = name;
        this.description = description;
        this.type = type;
        this.mode = mode;
        this.serviceUrl = serviceUrl;
        this.authRequired = authRequired;
        this.authCredential = authCredential;
        this.referrerInfo = referrerInfo;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public DataServiceJsBean(DataServiceJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setName(bean.getName());
            setDescription(bean.getDescription());
            setType(bean.getType());
            setMode(bean.getMode());
            setServiceUrl(bean.getServiceUrl());
            setAuthRequired(bean.isAuthRequired());
            setAuthCredential(bean.getAuthCredential());
            setReferrerInfo(bean.getReferrerInfo());
            setStatus(bean.getStatus());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static DataServiceJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        DataServiceJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(DataServiceJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, DataServiceJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getMode()
    {
        return this.mode;
    }
    public void setMode(String mode)
    {
        this.mode = mode;
    }

    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    public Boolean isAuthRequired()
    {
        return this.authRequired;
    }
    public void setAuthRequired(Boolean authRequired)
    {
        this.authRequired = authRequired;
    }

    public ConsumerKeySecretPairJsBean getAuthCredential()
    {  
        return this.authCredential;
    }
    public void setAuthCredential(ConsumerKeySecretPairJsBean authCredential)
    {
        this.authCredential = authCredential;
    }

    public ReferrerInfoStructJsBean getReferrerInfo()
    {  
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStructJsBean referrerInfo)
    {
        this.referrerInfo = referrerInfo;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("name:null, ");
        sb.append("description:null, ");
        sb.append("type:null, ");
        sb.append("mode:null, ");
        sb.append("serviceUrl:null, ");
        sb.append("authRequired:false, ");
        sb.append("authCredential:{}, ");
        sb.append("referrerInfo:{}, ");
        sb.append("status:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("name:");
        if(this.getName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getName()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("type:");
        if(this.getType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getType()).append("\", ");
        }
        sb.append("mode:");
        if(this.getMode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getMode()).append("\", ");
        }
        sb.append("serviceUrl:");
        if(this.getServiceUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getServiceUrl()).append("\", ");
        }
        sb.append("authRequired:" + this.isAuthRequired()).append(", ");
        sb.append("authCredential:");
        if(this.getAuthCredential() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAuthCredential()).append("\", ");
        }
        sb.append("referrerInfo:");
        if(this.getReferrerInfo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferrerInfo()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getName() != null) {
            sb.append("\"name\":").append("\"").append(this.getName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"name\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getType() != null) {
            sb.append("\"type\":").append("\"").append(this.getType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"type\":").append("null, ");
        }
        if(this.getMode() != null) {
            sb.append("\"mode\":").append("\"").append(this.getMode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"mode\":").append("null, ");
        }
        if(this.getServiceUrl() != null) {
            sb.append("\"serviceUrl\":").append("\"").append(this.getServiceUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"serviceUrl\":").append("null, ");
        }
        if(this.isAuthRequired() != null) {
            sb.append("\"authRequired\":").append("").append(this.isAuthRequired()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"authRequired\":").append("null, ");
        }
        sb.append("\"authCredential\":").append(this.authCredential.toJsonString()).append(", ");
        sb.append("\"referrerInfo\":").append(this.referrerInfo.toJsonString()).append(", ");
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("name = " + this.name).append(";");
        sb.append("description = " + this.description).append(";");
        sb.append("type = " + this.type).append(";");
        sb.append("mode = " + this.mode).append(";");
        sb.append("serviceUrl = " + this.serviceUrl).append(";");
        sb.append("authRequired = " + this.authRequired).append(";");
        sb.append("authCredential = " + this.authCredential).append(";");
        sb.append("referrerInfo = " + this.referrerInfo).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        DataServiceJsBean cloned = new DataServiceJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setName(this.getName());   
        cloned.setDescription(this.getDescription());   
        cloned.setType(this.getType());   
        cloned.setMode(this.getMode());   
        cloned.setServiceUrl(this.getServiceUrl());   
        cloned.setAuthRequired(this.isAuthRequired());   
        cloned.setAuthCredential( (ConsumerKeySecretPairJsBean) this.getAuthCredential().clone() );
        cloned.setReferrerInfo( (ReferrerInfoStructJsBean) this.getReferrerInfo().clone() );
        cloned.setStatus(this.getStatus());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
