package com.queryclient.af.auth;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.servlet.http.Cookie;


// Persistent/permanent session cookie.
public class SessionCookie extends Cookie implements Serializable
{
    private static final Logger log = Logger.getLogger(SessionCookie.class.getName());
    private static final long serialVersionUID = 1L;

    public static final String SESSION_COOKIE_NAME = "SESSIONBEAN";
    // TBD: Make the age configurable?
    //      In some apps, a large value can be potentially a problem....  ???? 
    // private static final int AGE_FOREVER = Integer.MAX_VALUE;   // This does not work. Probably too big. :)
    private static final int AGE_FOREVER = 3600 * 24 * 365;        // One year... This should be big enough.
    private static final String DEFAULT_COMMENT = "Persistent auth session cookie.";


    // Object representation of cookie.value.
    private SessionBean mSessionBean = null;

    public SessionCookie()
    {
        this(null);
    }
    public SessionCookie(SessionBean sessionBean)
    {
        // Use encrypted value...?
        super(SESSION_COOKIE_NAME, (sessionBean != null) ? sessionBean.toEncryptedJsonString() : "");
        
        // Set default values.
        // Can be overwritten....
        setPath("/");
        setMaxAge(AGE_FOREVER);
        setComment(DEFAULT_COMMENT);
        // ...
    }
    
    public SessionBean getSessionBean()
    {
        if(mSessionBean == null) {
            // Use encrypted value...?
            mSessionBean = SessionBean.fromEncryptedJsonString(getValue());
        }
        return mSessionBean;
    }
    public void setSessionBean(SessionBean sessionBean)
    {
        mSessionBean = sessionBean;
        // Use encrypted value...?
        super.setValue((mSessionBean != null) ? mSessionBean.toEncryptedJsonString() : "");
    }

    @Override
    public void setValue(String newValue)
    {
        //mSessionBean = SessionBean.fromEncryptedJsonString(newValue);
        mSessionBean = null;  // ???
        super.setValue(newValue);
    }

    public String getSessionGuid()
    {
        return getSessionBean().getGuid();
    }

    public String getSessionUser()
    {
        return getSessionBean().getUser();
    }

    public String getSessionToken()
    {
        return getSessionBean().getToken();
    }

    public void markForRemoval()
    {
        setMaxAge(0);
    }


    @Override
    public String toString()
    {
        return "SessionCookie [mSessionBean=" + ((getSessionBean() != null) ? getSessionBean().toJsonString() : "")
                + ", getComment()=" + getComment() + ", getDomain()="
                + getDomain() + ", getMaxAge()=" + getMaxAge() + ", getPath()="
                + getPath() + ", getSecure()=" + getSecure() + ", getName()="
                + getName() + ", getValue()=" + getValue() + ", getVersion()="
                + getVersion() + "]";
    }
    
}
