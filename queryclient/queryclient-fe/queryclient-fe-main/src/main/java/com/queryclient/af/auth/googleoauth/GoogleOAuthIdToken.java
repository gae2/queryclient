package com.queryclient.af.auth.googleoauth;

import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

import com.queryclient.ws.util.JsonUtil;


// Simple wrapper class around the "id_token" object.
// Not being used.
// As it turns out, the it_token string is not just a based64-encoded JSON string, but a JWT object (cryptographically signed JSON object).
// https://developers.google.com/accounts/docs/OAuth2Login
// http://openid.net/specs/draft-jones-json-web-token-07.html
public final class GoogleOAuthIdToken implements Serializable
{
    private static final Logger log = Logger.getLogger(GoogleOAuthIdToken.class.getName());
    private static final long serialVersionUID = 1L;

    // For debugging purposes.
    // Set it to true during testing/debugging only.
    private static final boolean LOGGING_ENABLED = false;
    // ....
    
    // "Read-only" variables. 
    private final String iss;
    private final String sub;
    private final String email;
    private final boolean emailVerified;
    private final String atHash;
    private final String azp;
    private final String aud;
    private final int iat;
    private final int exp;

    public GoogleOAuthIdToken(String iss, String sub, String email,
            boolean emailVerified, String atHash, String azp, String aud,
            int iat, int exp)
    {
        super();
        this.iss = iss;
        this.sub = sub;
        this.email = email;
        this.emailVerified = emailVerified;
        this.atHash = atHash;
        this.azp = azp;
        this.aud = aud;
        this.iat = iat;
        this.exp = exp;
    }


    // Getters only.

    public String getIss()
    {
        return iss;
    }
    public String getSub()
    {
        return sub;
    }
    public String getEmail()
    {
        return email;
    }
    public boolean isEmailVerified()
    {
        return emailVerified;
    }
    public String getAtHash()
    {
        return atHash;
    }
    public String getAzp()
    {
        return azp;
    }
    public String getAud()
    {
        return aud;
    }
    public int getIat()
    {
        return iat;
    }
    public int getExp()
    {
        return exp;
    }


    // TBD:
    // This is wrong.
    // Just use GoogleOAuthIdTokenUtil....
    // ...
    // Parses the base64-encoded id_token string and build a GoogleOAuthIdToken object.
    private static GoogleOAuthIdToken parseIdTokenString(String base64EncodedToken)
    {
        if(base64EncodedToken == null) {
            return null;
        }
        byte[] decodedBytes = null;
        try {
            decodedBytes = DatatypeConverter.parseBase64Binary(base64EncodedToken);
        } catch(Exception e) {
            // ignore
            if(LOGGING_ENABLED) {
                log.log(Level.WARNING, "Failed to base64-decode base64EncodedToken = " + base64EncodedToken, e);
            } else {
                log.log(Level.WARNING, "Failed to base64-decode base64EncodedToken.", e);
            }
        }
        if(decodedBytes == null) {
            return null;
        }
        
        String jsonStr = null;
        try {
            jsonStr = new String(decodedBytes, "UTF8");
        } catch (Exception e) {
            // ???
            log.log(Level.WARNING, "Failed to convert base64 decoded byte array to JSON string.", e);
        }
        if(LOGGING_ENABLED && log.isLoggable(Level.FINE)) log.fine("jsonStr = " + jsonStr);
        if(jsonStr == null) {
            return null;
        }

        Map<String,String> map = JsonUtil.parseJsonObjectString(jsonStr);
        
        String iss = map.get("iss");
        String sub = map.get("sub");
        String email = map.get("email");
        boolean emailVerified = false;
        String emailVerifiedStr = map.get("email_verified");
        if(emailVerifiedStr != null) {
            emailVerified = Boolean.parseBoolean(emailVerifiedStr);
        }
        String atHash = map.get("at_hash");
        String azp = map.get("azp");
        String aud = map.get("aud");
        int iat = 0;
        String iatStr = map.get("iat");
        if(iatStr != null) {
            try {
                iat = Integer.parseInt(iatStr);
            } catch(Exception e) {
                // ignore
                if(LOGGING_ENABLED && log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse iatStr = " + iatStr, e);
            }
        }
        int exp = 0;
        String expStr = map.get("exp");
        if(expStr != null) {
            try {
                exp = Integer.parseInt(expStr);
            } catch(Exception e) {
                // ignore
                if(LOGGING_ENABLED && log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse expStr = " + expStr, e);
            }
        }

        GoogleOAuthIdToken oauthIdToken = new GoogleOAuthIdToken(iss, sub, email, emailVerified, atHash, azp, aud, iat, exp);
        if(LOGGING_ENABLED && log.isLoggable(Level.FINE)) log.fine("oauthIdToken = " + oauthIdToken);
         
        return oauthIdToken;
    }


    // For debugging purposes
    @Override
    public String toString()
    {
        return "GoogleOAuthIdToken [iss=" + iss + ", sub=" + sub + ", email="
                + email + ", emailVerified=" + emailVerified + ", atHash="
                + atHash + ", azp=" + azp + ", aud=" + aud + ", iat=" + iat
                + ", exp=" + exp + "]";
    }

    
}
