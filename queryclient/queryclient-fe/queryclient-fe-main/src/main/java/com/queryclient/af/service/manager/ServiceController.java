package com.queryclient.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.service.ApiConsumerService;
import com.queryclient.af.service.UserService;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.af.service.ExternalUserAuthService;
import com.queryclient.af.service.UserAuthStateService;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.service.ServiceInfoService;
import com.queryclient.af.service.FiveTenService;

// TBD: DI
// (For now, this class is used to make ServiceManager "DI-capable".)
public class ServiceController
{
    private static final Logger log = Logger.getLogger(ServiceController.class.getName());

    // "Injectable" services.
    private ApiConsumerService apiConsumerService = null;
    private UserService userService = null;
    private UserPasswordService userPasswordService = null;
    private ExternalUserAuthService externalUserAuthService = null;
    private UserAuthStateService userAuthStateService = null;
    private DataServiceService dataServiceService = null;
    private ServiceEndpointService serviceEndpointService = null;
    private QuerySessionService querySessionService = null;
    private QueryRecordService queryRecordService = null;
    private DummyEntityService dummyEntityService = null;
    private ServiceInfoService serviceInfoService = null;
    private FiveTenService fiveTenService = null;

    // Ctor injection.
    public ServiceController(ApiConsumerService apiConsumerService, UserService userService, UserPasswordService userPasswordService, ExternalUserAuthService externalUserAuthService, UserAuthStateService userAuthStateService, DataServiceService dataServiceService, ServiceEndpointService serviceEndpointService, QuerySessionService querySessionService, QueryRecordService queryRecordService, DummyEntityService dummyEntityService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.userService = userService;
        this.userPasswordService = userPasswordService;
        this.externalUserAuthService = externalUserAuthService;
        this.userAuthStateService = userAuthStateService;
        this.dataServiceService = dataServiceService;
        this.serviceEndpointService = serviceEndpointService;
        this.querySessionService = querySessionService;
        this.queryRecordService = queryRecordService;
        this.dummyEntityService = dummyEntityService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }

    // Returns a ApiConsumerService instance.
	public ApiConsumerService getApiConsumerService() 
    {
        return this.apiConsumerService;
    }
    // Setter injection for ApiConsumerService.
	public void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        this.apiConsumerService = apiConsumerService;
    }

    // Returns a UserService instance.
	public UserService getUserService() 
    {
        return this.userService;
    }
    // Setter injection for UserService.
	public void setUserService(UserService userService) 
    {
        this.userService = userService;
    }

    // Returns a UserPasswordService instance.
	public UserPasswordService getUserPasswordService() 
    {
        return this.userPasswordService;
    }
    // Setter injection for UserPasswordService.
	public void setUserPasswordService(UserPasswordService userPasswordService) 
    {
        this.userPasswordService = userPasswordService;
    }

    // Returns a ExternalUserAuthService instance.
	public ExternalUserAuthService getExternalUserAuthService() 
    {
        return this.externalUserAuthService;
    }
    // Setter injection for ExternalUserAuthService.
	public void setExternalUserAuthService(ExternalUserAuthService externalUserAuthService) 
    {
        this.externalUserAuthService = externalUserAuthService;
    }

    // Returns a UserAuthStateService instance.
	public UserAuthStateService getUserAuthStateService() 
    {
        return this.userAuthStateService;
    }
    // Setter injection for UserAuthStateService.
	public void setUserAuthStateService(UserAuthStateService userAuthStateService) 
    {
        this.userAuthStateService = userAuthStateService;
    }

    // Returns a DataServiceService instance.
	public DataServiceService getDataServiceService() 
    {
        return this.dataServiceService;
    }
    // Setter injection for DataServiceService.
	public void setDataServiceService(DataServiceService dataServiceService) 
    {
        this.dataServiceService = dataServiceService;
    }

    // Returns a ServiceEndpointService instance.
	public ServiceEndpointService getServiceEndpointService() 
    {
        return this.serviceEndpointService;
    }
    // Setter injection for ServiceEndpointService.
	public void setServiceEndpointService(ServiceEndpointService serviceEndpointService) 
    {
        this.serviceEndpointService = serviceEndpointService;
    }

    // Returns a QuerySessionService instance.
	public QuerySessionService getQuerySessionService() 
    {
        return this.querySessionService;
    }
    // Setter injection for QuerySessionService.
	public void setQuerySessionService(QuerySessionService querySessionService) 
    {
        this.querySessionService = querySessionService;
    }

    // Returns a QueryRecordService instance.
	public QueryRecordService getQueryRecordService() 
    {
        return this.queryRecordService;
    }
    // Setter injection for QueryRecordService.
	public void setQueryRecordService(QueryRecordService queryRecordService) 
    {
        this.queryRecordService = queryRecordService;
    }

    // Returns a DummyEntityService instance.
	public DummyEntityService getDummyEntityService() 
    {
        return this.dummyEntityService;
    }
    // Setter injection for DummyEntityService.
	public void setDummyEntityService(DummyEntityService dummyEntityService) 
    {
        this.dummyEntityService = dummyEntityService;
    }

    // Returns a ServiceInfoService instance.
	public ServiceInfoService getServiceInfoService() 
    {
        return this.serviceInfoService;
    }
    // Setter injection for ServiceInfoService.
	public void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        this.serviceInfoService = serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public FiveTenService getFiveTenService() 
    {
        return this.fiveTenService;
    }
    // Setter injection for FiveTenService.
	public void setFiveTenService(FiveTenService fiveTenService) 
    {
        this.fiveTenService = fiveTenService;
    }

}
