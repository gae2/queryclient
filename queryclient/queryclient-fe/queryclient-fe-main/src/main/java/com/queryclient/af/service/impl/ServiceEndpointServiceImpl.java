package com.queryclient.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.ServiceEndpointService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ServiceEndpointServiceImpl implements ServiceEndpointService
{
    private static final Logger log = Logger.getLogger(ServiceEndpointServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "ServiceEndpoint-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("ServiceEndpoint:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public ServiceEndpointServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // ServiceEndpoint related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ServiceEndpoint getServiceEndpoint(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getServiceEndpoint(): guid = " + guid);

        ServiceEndpointBean bean = null;
        if(getCache() != null) {
            bean = (ServiceEndpointBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ServiceEndpointBean) getProxyFactory().getServiceEndpointServiceProxy().getServiceEndpoint(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ServiceEndpointBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ServiceEndpointBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getServiceEndpoint(String guid, String field) throws BaseException
    {
        ServiceEndpointBean bean = null;
        if(getCache() != null) {
            bean = (ServiceEndpointBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ServiceEndpointBean) getProxyFactory().getServiceEndpointServiceProxy().getServiceEndpoint(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ServiceEndpointBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ServiceEndpointBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("dataService")) {
            return bean.getDataService();
        } else if(field.equals("serviceName")) {
            return bean.getServiceName();
        } else if(field.equals("serviceUrl")) {
            return bean.getServiceUrl();
        } else if(field.equals("authRequired")) {
            return bean.isAuthRequired();
        } else if(field.equals("authCredential")) {
            return bean.getAuthCredential();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException
    {
        log.fine("getServiceEndpoints()");

        // TBD: Is there a better way????
        List<ServiceEndpoint> serviceEndpoints = getProxyFactory().getServiceEndpointServiceProxy().getServiceEndpoints(guids);
        if(serviceEndpoints == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceEndpointBean list.");
        }

        log.finer("END");
        return serviceEndpoints;
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException
    {
        return getAllServiceEndpoints(null, null, null);
    }


    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpoints(ordering, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllServiceEndpoints(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ServiceEndpoint> serviceEndpoints = getProxyFactory().getServiceEndpointServiceProxy().getAllServiceEndpoints(ordering, offset, count, forwardCursor);
        if(serviceEndpoints == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceEndpointBean list.");
        }

        log.finer("END");
        return serviceEndpoints;
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllServiceEndpointKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getServiceEndpointServiceProxy().getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceEndpointBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ServiceEndpointBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ServiceEndpointServiceImpl.findServiceEndpoints(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ServiceEndpoint> serviceEndpoints = getProxyFactory().getServiceEndpointServiceProxy().findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(serviceEndpoints == null) {
            log.log(Level.WARNING, "Failed to find serviceEndpoints for the given criterion.");
        }

        log.finer("END");
        return serviceEndpoints;
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ServiceEndpointServiceImpl.findServiceEndpointKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getServiceEndpointServiceProxy().findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ServiceEndpoint keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ServiceEndpoint key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ServiceEndpointServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getServiceEndpointServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createServiceEndpoint(String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ConsumerKeySecretPairBean authCredentialBean = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialBean = (ConsumerKeySecretPairBean) authCredential;
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialBean = new ConsumerKeySecretPairBean(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialBean = null;   // ????
        }
        ServiceEndpointBean bean = new ServiceEndpointBean(null, user, dataService, serviceName, serviceUrl, authRequired, authCredentialBean, status);
        return createServiceEndpoint(bean);
    }

    @Override
    public String createServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ServiceEndpoint bean = constructServiceEndpoint(serviceEndpoint);
        //return bean.getGuid();

        // Param serviceEndpoint cannot be null.....
        if(serviceEndpoint == null) {
            log.log(Level.INFO, "Param serviceEndpoint is null!");
            throw new BadRequestException("Param serviceEndpoint object is null!");
        }
        ServiceEndpointBean bean = null;
        if(serviceEndpoint instanceof ServiceEndpointBean) {
            bean = (ServiceEndpointBean) serviceEndpoint;
        } else if(serviceEndpoint instanceof ServiceEndpoint) {
            // bean = new ServiceEndpointBean(null, serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), (ConsumerKeySecretPairBean) serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ServiceEndpointBean(serviceEndpoint.getGuid(), serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), (ConsumerKeySecretPairBean) serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
        } else {
            log.log(Level.WARNING, "createServiceEndpoint(): Arg serviceEndpoint is of an unknown type.");
            //bean = new ServiceEndpointBean();
            bean = new ServiceEndpointBean(serviceEndpoint.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getServiceEndpointServiceProxy().createServiceEndpoint(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ServiceEndpoint constructServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceEndpoint cannot be null.....
        if(serviceEndpoint == null) {
            log.log(Level.INFO, "Param serviceEndpoint is null!");
            throw new BadRequestException("Param serviceEndpoint object is null!");
        }
        ServiceEndpointBean bean = null;
        if(serviceEndpoint instanceof ServiceEndpointBean) {
            bean = (ServiceEndpointBean) serviceEndpoint;
        } else if(serviceEndpoint instanceof ServiceEndpoint) {
            // bean = new ServiceEndpointBean(null, serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), (ConsumerKeySecretPairBean) serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ServiceEndpointBean(serviceEndpoint.getGuid(), serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), (ConsumerKeySecretPairBean) serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
        } else {
            log.log(Level.WARNING, "createServiceEndpoint(): Arg serviceEndpoint is of an unknown type.");
            //bean = new ServiceEndpointBean();
            bean = new ServiceEndpointBean(serviceEndpoint.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getServiceEndpointServiceProxy().createServiceEndpoint(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ConsumerKeySecretPairBean authCredentialBean = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialBean = (ConsumerKeySecretPairBean) authCredential;
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialBean = new ConsumerKeySecretPairBean(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialBean = null;   // ????
        }
        ServiceEndpointBean bean = new ServiceEndpointBean(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredentialBean, status);
        return updateServiceEndpoint(bean);
    }
        
    @Override
    public Boolean updateServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ServiceEndpoint bean = refreshServiceEndpoint(serviceEndpoint);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param serviceEndpoint cannot be null.....
        if(serviceEndpoint == null || serviceEndpoint.getGuid() == null) {
            log.log(Level.INFO, "Param serviceEndpoint or its guid is null!");
            throw new BadRequestException("Param serviceEndpoint object or its guid is null!");
        }
        ServiceEndpointBean bean = null;
        if(serviceEndpoint instanceof ServiceEndpointBean) {
            bean = (ServiceEndpointBean) serviceEndpoint;
        } else {  // if(serviceEndpoint instanceof ServiceEndpoint)
            bean = new ServiceEndpointBean(serviceEndpoint.getGuid(), serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), (ConsumerKeySecretPairBean) serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
        }
        Boolean suc = getProxyFactory().getServiceEndpointServiceProxy().updateServiceEndpoint(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public ServiceEndpoint refreshServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceEndpoint cannot be null.....
        if(serviceEndpoint == null || serviceEndpoint.getGuid() == null) {
            log.log(Level.INFO, "Param serviceEndpoint or its guid is null!");
            throw new BadRequestException("Param serviceEndpoint object or its guid is null!");
        }
        ServiceEndpointBean bean = null;
        if(serviceEndpoint instanceof ServiceEndpointBean) {
            bean = (ServiceEndpointBean) serviceEndpoint;
        } else {  // if(serviceEndpoint instanceof ServiceEndpoint)
            bean = new ServiceEndpointBean(serviceEndpoint.getGuid(), serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), (ConsumerKeySecretPairBean) serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
        }
        Boolean suc = getProxyFactory().getServiceEndpointServiceProxy().updateServiceEndpoint(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getServiceEndpointServiceProxy().deleteServiceEndpoint(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            ServiceEndpoint serviceEndpoint = null;
            try {
                serviceEndpoint = getProxyFactory().getServiceEndpointServiceProxy().getServiceEndpoint(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch serviceEndpoint with a key, " + guid);
                return false;
            }
            if(serviceEndpoint != null) {
                String beanGuid = serviceEndpoint.getGuid();
                Boolean suc1 = getProxyFactory().getServiceEndpointServiceProxy().deleteServiceEndpoint(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("serviceEndpoint with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceEndpoint cannot be null.....
        if(serviceEndpoint == null || serviceEndpoint.getGuid() == null) {
            log.log(Level.INFO, "Param serviceEndpoint or its guid is null!");
            throw new BadRequestException("Param serviceEndpoint object or its guid is null!");
        }
        ServiceEndpointBean bean = null;
        if(serviceEndpoint instanceof ServiceEndpointBean) {
            bean = (ServiceEndpointBean) serviceEndpoint;
        } else {  // if(serviceEndpoint instanceof ServiceEndpoint)
            // ????
            log.warning("serviceEndpoint is not an instance of ServiceEndpointBean.");
            bean = new ServiceEndpointBean(serviceEndpoint.getGuid(), serviceEndpoint.getUser(), serviceEndpoint.getDataService(), serviceEndpoint.getServiceName(), serviceEndpoint.getServiceUrl(), serviceEndpoint.isAuthRequired(), (ConsumerKeySecretPairBean) serviceEndpoint.getAuthCredential(), serviceEndpoint.getStatus());
        }
        Boolean suc = getProxyFactory().getServiceEndpointServiceProxy().deleteServiceEndpoint(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getServiceEndpointServiceProxy().deleteServiceEndpoints(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException
    {
        log.finer("BEGIN");

        if(serviceEndpoints == null) {
            log.log(Level.WARNING, "createServiceEndpoints() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = serviceEndpoints.size();
        if(size == 0) {
            log.log(Level.WARNING, "createServiceEndpoints() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ServiceEndpoint serviceEndpoint : serviceEndpoints) {
            String guid = createServiceEndpoint(serviceEndpoint);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createServiceEndpoints() failed for at least one serviceEndpoint. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException
    //{
    //}

}
