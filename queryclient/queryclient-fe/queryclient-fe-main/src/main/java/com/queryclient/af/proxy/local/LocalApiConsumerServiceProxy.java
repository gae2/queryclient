package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ApiConsumer;
// import com.queryclient.ws.bean.ApiConsumerBean;
import com.queryclient.ws.service.ApiConsumerService;
import com.queryclient.af.bean.ApiConsumerBean;
import com.queryclient.af.proxy.ApiConsumerServiceProxy;


public class LocalApiConsumerServiceProxy extends BaseLocalServiceProxy implements ApiConsumerServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalApiConsumerServiceProxy.class.getName());

    public LocalApiConsumerServiceProxy()
    {
    }

    @Override
    public ApiConsumer getApiConsumer(String guid) throws BaseException
    {
        ApiConsumer serverBean = getApiConsumerService().getApiConsumer(guid);
        ApiConsumer appBean = convertServerApiConsumerBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getApiConsumer(String guid, String field) throws BaseException
    {
        return getApiConsumerService().getApiConsumer(guid, field);       
    }

    @Override
    public List<ApiConsumer> getApiConsumers(List<String> guids) throws BaseException
    {
        List<ApiConsumer> serverBeanList = getApiConsumerService().getApiConsumers(guids);
        List<ApiConsumer> appBeanList = convertServerApiConsumerBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers() throws BaseException
    {
        return getAllApiConsumers(null, null, null);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getApiConsumerService().getAllApiConsumers(ordering, offset, count);
        return getAllApiConsumers(ordering, offset, count, null);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ApiConsumer> serverBeanList = getApiConsumerService().getAllApiConsumers(ordering, offset, count, forwardCursor);
        List<ApiConsumer> appBeanList = convertServerApiConsumerBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getApiConsumerService().getAllApiConsumerKeys(ordering, offset, count);
        return getAllApiConsumerKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getApiConsumerService().getAllApiConsumerKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findApiConsumers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getApiConsumerService().findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count);
        return findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ApiConsumer> serverBeanList = getApiConsumerService().findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<ApiConsumer> appBeanList = convertServerApiConsumerBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getApiConsumerService().findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getApiConsumerService().findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getApiConsumerService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createApiConsumer(String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        return getApiConsumerService().createApiConsumer(aeryId, name, description, appKey, appSecret, status);
    }

    @Override
    public String createApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        com.queryclient.ws.bean.ApiConsumerBean serverBean =  convertAppApiConsumerBeanToServerBean(apiConsumer);
        return getApiConsumerService().createApiConsumer(serverBean);
    }

    @Override
    public Boolean updateApiConsumer(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        return getApiConsumerService().updateApiConsumer(guid, aeryId, name, description, appKey, appSecret, status);
    }

    @Override
    public Boolean updateApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        com.queryclient.ws.bean.ApiConsumerBean serverBean =  convertAppApiConsumerBeanToServerBean(apiConsumer);
        return getApiConsumerService().updateApiConsumer(serverBean);
    }

    @Override
    public Boolean deleteApiConsumer(String guid) throws BaseException
    {
        return getApiConsumerService().deleteApiConsumer(guid);
    }

    @Override
    public Boolean deleteApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        com.queryclient.ws.bean.ApiConsumerBean serverBean =  convertAppApiConsumerBeanToServerBean(apiConsumer);
        return getApiConsumerService().deleteApiConsumer(serverBean);
    }

    @Override
    public Long deleteApiConsumers(String filter, String params, List<String> values) throws BaseException
    {
        return getApiConsumerService().deleteApiConsumers(filter, params, values);
    }




    public static ApiConsumerBean convertServerApiConsumerBeanToAppBean(ApiConsumer serverBean)
    {
        ApiConsumerBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new ApiConsumerBean();
            bean.setGuid(serverBean.getGuid());
            bean.setAeryId(serverBean.getAeryId());
            bean.setName(serverBean.getName());
            bean.setDescription(serverBean.getDescription());
            bean.setAppKey(serverBean.getAppKey());
            bean.setAppSecret(serverBean.getAppSecret());
            bean.setStatus(serverBean.getStatus());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ApiConsumer> convertServerApiConsumerBeanListToAppBeanList(List<ApiConsumer> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<ApiConsumer> beanList = new ArrayList<ApiConsumer>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(ApiConsumer sb : serverBeanList) {
                ApiConsumerBean bean = convertServerApiConsumerBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.ApiConsumerBean convertAppApiConsumerBeanToServerBean(ApiConsumer appBean)
    {
        com.queryclient.ws.bean.ApiConsumerBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.ApiConsumerBean();
            bean.setGuid(appBean.getGuid());
            bean.setAeryId(appBean.getAeryId());
            bean.setName(appBean.getName());
            bean.setDescription(appBean.getDescription());
            bean.setAppKey(appBean.getAppKey());
            bean.setAppSecret(appBean.getAppSecret());
            bean.setStatus(appBean.getStatus());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ApiConsumer> convertAppApiConsumerBeanListToServerBeanList(List<ApiConsumer> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<ApiConsumer> beanList = new ArrayList<ApiConsumer>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(ApiConsumer sb : appBeanList) {
                com.queryclient.ws.bean.ApiConsumerBean bean = convertAppApiConsumerBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
