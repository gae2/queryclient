package com.queryclient.af.daemon;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.apphosting.api.DeadlineExceededException;

// Place holder for now.
// Task definition including the task handler (web service endpoint) information.
public class LoopingTask
{
    private static final Logger log = Logger.getLogger(LoopingTask.class.getName());

    // temporary.
    // The task will be terminated after MAX_LOOPING iterations.
    private static final int MAX_LOOPING = 100;  // ???

    // The task will be terminated after MAX_DURATION milliseconds.
    private static final long MAX_DURATION = 540 * 1000L;  // ???  9 mins 

    
    // Track name
    String mTrack = null;  // Null means "default" track...

    // TBD: ...
    Map<String, String> params; 
    // Body/Post data?
    // Task handler??? (Servlet or Jersey resource ...)
    // ...
    
    
    // Scheduled start time???
    // ....
    
    
    // Unix epoch. The time when the task was first performed.
    private long mStartTime;  // Non-zero mStartTime indicates that the looping has started...
    
    // in MilliSeconds.
    private long mMaxDuration;
    
    // The task will be terminated after mMaxLooping iterations.
    private int mMaxLooping;

    
    
    // ???
    private Loopable mLooplet;

    // TBD
    public LoopingTask(Loopable looplet)
    {
        this(looplet, null);
    }
    public LoopingTask(Loopable looplet, String track)
    {
        mLooplet = looplet;
        mTrack = track;
        mStartTime = 0L;
        mMaxDuration = MAX_DURATION;  // ???
        mMaxLooping = MAX_LOOPING;    // ???
    }


    public String getTrack()
    {
        return mTrack;
    }
    public void setTrack(String track)
    {
        this.mTrack = track;
    }

    
    // TBD
    // ...

    public Map<String, String> getParams()
    {
        return params;
    }
    public void setParams(Map<String, String> params)
    {
        this.params = params;
    }

    // TBD
    // ...
    

    public long getStartTime()
    {
        return mStartTime;
    }
//    public void setStartTime(long startTime)
//    {
//        this.mStartTime = startTime;
//    }

    public long getMaxDuration()
    {
        return mMaxDuration;
    }
    public void setMaxDuration(long maxDuration)
    {
        if(mStartTime == 0L) {
            this.mMaxDuration = (maxDuration > MAX_DURATION * 1000L) ? MAX_DURATION * 1000L : maxDuration;
        } else {
            log.info("max duration cannot be changed since the looping has already started...");
        }
    }

    public int getMaxLooping()
    {
        return mMaxLooping;
    }
    public void setMaxLooping(int maxLooping)
    {
        if(mStartTime == 0L) {
            this.mMaxDuration = (maxLooping > MAX_LOOPING) ? MAX_LOOPING : maxLooping;
        } else {
            log.info("max looping cannot be changed since the looping has already started...");
        }
    }



    // The "main" method....
    // Returns the number of "successful" iterations.
    // (Doe this make sense? Who's gonna use this return value?) 
    public int doLoop()
    {
        if(mLooplet == null) {
            // Cannot proceed...
        }
        
        int counter = 0;
        int sucCnt = 0;
        mStartTime = System.currentTimeMillis();
        long endTime = mStartTime + mMaxDuration;
        long now = System.currentTimeMillis();
        
        // "setup"
        mLooplet.preLoops();
        
        // "Main loop"
        while(counter < mMaxLooping && now < endTime) {
            
            // Move the try/catch block to the outside of the loop????
            try {

                mLooplet.doOneLoop();

            } catch(DeadlineExceededException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "DeadlineExceededException at " + now, e);
                break;
            }

            counter++;
            now = System.currentTimeMillis();
        }
        
        
        // TBD:
        // Resubmit the task...  -> done by LoopingDaemon.performTask() ????
        //LoopingDaemon.getInstance().tickle(mTrack);
        // ....
        

        return sucCnt;
        //return counter;
    }

    
}
