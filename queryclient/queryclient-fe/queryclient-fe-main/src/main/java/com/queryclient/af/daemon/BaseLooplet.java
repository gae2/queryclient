package com.queryclient.af.daemon;

// Place holder for now.
// Task definition including the task handler (web service endpoint) information.
abstract public class BaseLooplet implements Loopable
{    
    // Returns true if the processing was successful (app dependent)
    @Override
    public boolean doOneLoop()
    {
        // TBD
        return true;
    }
    
}
