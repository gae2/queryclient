package com.queryclient.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.User;
import com.queryclient.af.bean.UserBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.UserService;
import com.queryclient.af.service.impl.UserServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserAppService extends UserServiceImpl implements UserService
{
    private static final Logger log = Logger.getLogger(UserAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // User related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public User getUser(String guid) throws BaseException
    {
        return super.getUser(guid);
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        return super.getUser(guid, field);
    }
    
    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return super.getAllUsers();
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUsers(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUser(User user) throws BaseException
    {
        return super.createUser(user);
    }

    @Override
    public User constructUser(User user) throws BaseException
    {
        return super.constructUser(user);
    }


    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        return super.updateUser(user);
    }
        
    @Override
    public User refreshUser(User user) throws BaseException
    {
        return super.refreshUser(user);
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        return super.deleteUser(guid);
    }

    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        return super.deleteUser(user);
    }

    @Override
    public Integer createUsers(List<User> users) throws BaseException
    {
        return super.createUsers(users);
    }

    // TBD
    //@Override
    //public Boolean updateUsers(List<User> users) throws BaseException
    //{
    //}

}
