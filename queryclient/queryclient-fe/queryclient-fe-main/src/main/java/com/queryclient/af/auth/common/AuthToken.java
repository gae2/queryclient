package com.queryclient.af.auth.common;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;


// This is used to represent the current "auth state" across multiple identity providers.
// Note that the username (val) is optional....
// Note: "username" can be a username or userId depending on the providerId.
//      (We should be consistent with the use for each provider....)
//      (For Twitter, we use @-username/handle (although long userId is more permanent)
//                    because username is more "displayable" to the user.
//                    this can potentially cause problems if the user changes his/her username during the active session...) 
// TBD: Support multiple values? (e.g., username(string), and userid(long), etc...) ???? 
// Note: 
//       Use AuthState instead.
//       This class is kept here, for now, for backward compatibility only.
// @Deprecated
public class AuthToken implements Serializable
{
    private static final Logger log = Logger.getLogger(AuthToken.class.getName());
    private static final long serialVersionUID = 1L;

    // We only support one user/auth per provider..., for now...
    private transient Map<String,String> authMap = new HashMap<String,String>();
    
    public AuthToken()
    {
        this(null);
    }
    public AuthToken(String authState)
    {
        authMap = parseAuthStateString(authState);
    }
    
    
    // authState is a comma-separated list of providerId:username pairs.
    // providerId and (optional) username are separated by a colon (:).
    // e.g., "twitter:ladygaga,facebook:12345", etc.
    private static Map<String,String> parseAuthStateString(String authState)
    {
        Map<String,String> authMap = new HashMap<String,String>();
        if(authState != null && !authState.isEmpty()) {
            String[] pairs = authState.split("\\s*,\\s*");
            if(pairs != null && pairs.length > 0) {
                for(String pair : pairs) {
                    String[] keyValue = pair.split("\\s*:\\s*", 2);
                    if(keyValue != null && keyValue.length > 0) {
                        String key = keyValue[0];
                        String val = null;
                        if(keyValue.length > 1) {
                            val = keyValue[1];
                        }
                        if(val == null) {
                            val = "";       // See the comment below. We use username="" instead of username=null.
                        }
                        authMap.put(key,  val);
                    }
                }
            }
        }
        return authMap;
    }

    public static AuthToken valueOf(String authState)
    {
        AuthToken authToken = new AuthToken(authState);
        return authToken;
    }
    
    
    public boolean isAuthenticated()
    {
        if(! authMap.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
    public boolean isAuthenticated(String providerId)
    {
        if(providerId == null) {
            return isAuthenticated();
        }
        if(authMap.containsKey(providerId)) {
            return true;
        } else {
            return false;
        }
    }

    public String getUsername(String providerId)
    {
        if(providerId == null) {
            return null;
        }
        return authMap.get(providerId);
    }


    // username is optional.
    public void addAuthState(String providerId, String username)
    {
        // To distinguish two different cases of the return value of null for removeAuthState,
        // We use "" when username is null.
        if(username == null) {
            username = "";
        }
        authMap.put(providerId, username);
    }
    // The return value of null means that the providerId did not exist in the authState.
    // whereas the return value of an empty string means the providerId was in the authState (without a specific username).
    public String removeAuthState(String providerId)
    {
        String username = authMap.remove(providerId);
        return username;
    }
    public void clearAllAuthState()
    {
        authMap.clear();
    }
    
    
    @Override
    public String toString()
    {
        if(authMap == null || authMap.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for(String key : authMap.keySet()) {
            sb.append(key).append(":");
            String val = authMap.get(key);
            if(val == null) {
                val = "";
            }
            sb.append(val).append(",");
        }
        sb.deleteCharAt(sb.length()-1);
        return sb.toString();
    }

    
    // temporary

    private void writeObject(java.io.ObjectOutputStream out)
            throws IOException
    {
        out.writeUTF(toString());
    }
    
    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException
    {
        String authState = in.readUTF();
        authMap = parseAuthStateString(authState);
    }        

        
}
