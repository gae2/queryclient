package com.queryclient.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.ServiceEndpointService;


// ServiceEndpointMockService is a decorator.
// It can be used as a base class to mock ServiceEndpointService objects.
public abstract class ServiceEndpointMockService implements ServiceEndpointService
{
    private static final Logger log = Logger.getLogger(ServiceEndpointMockService.class.getName());

    // ServiceEndpointMockService uses the decorator design pattern.
    private ServiceEndpointService decoratedService;

    public ServiceEndpointMockService(ServiceEndpointService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected ServiceEndpointService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(ServiceEndpointService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // ServiceEndpoint related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ServiceEndpoint getServiceEndpoint(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getServiceEndpoint(): guid = " + guid);
        ServiceEndpoint bean = decoratedService.getServiceEndpoint(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getServiceEndpoint(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getServiceEndpoint(guid, field);
        return obj;
    }

    @Override
    public List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException
    {
        log.fine("getServiceEndpoints()");
        List<ServiceEndpoint> serviceEndpoints = decoratedService.getServiceEndpoints(guids);
        log.finer("END");
        return serviceEndpoints;
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException
    {
        return getAllServiceEndpoints(null, null, null);
    }


    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpoints(ordering, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllServiceEndpoints(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<ServiceEndpoint> serviceEndpoints = decoratedService.getAllServiceEndpoints(ordering, offset, count, forwardCursor);
        log.finer("END");
        return serviceEndpoints;
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllServiceEndpointKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ServiceEndpointMockService.findServiceEndpoints(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<ServiceEndpoint> serviceEndpoints = decoratedService.findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return serviceEndpoints;
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ServiceEndpointMockService.findServiceEndpointKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ServiceEndpointMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createServiceEndpoint(String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        ConsumerKeySecretPairBean authCredentialBean = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialBean = (ConsumerKeySecretPairBean) authCredential;
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialBean = new ConsumerKeySecretPairBean(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialBean = null;   // ????
        }
        ServiceEndpointBean bean = new ServiceEndpointBean(null, user, dataService, serviceName, serviceUrl, authRequired, authCredentialBean, status);
        return createServiceEndpoint(bean);
    }

    @Override
    public String createServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createServiceEndpoint(serviceEndpoint);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ServiceEndpoint constructServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");
        ServiceEndpoint bean = decoratedService.constructServiceEndpoint(serviceEndpoint);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ConsumerKeySecretPairBean authCredentialBean = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialBean = (ConsumerKeySecretPairBean) authCredential;
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialBean = new ConsumerKeySecretPairBean(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialBean = null;   // ????
        }
        ServiceEndpointBean bean = new ServiceEndpointBean(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredentialBean, status);
        return updateServiceEndpoint(bean);
    }
        
    @Override
    public Boolean updateServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateServiceEndpoint(serviceEndpoint);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public ServiceEndpoint refreshServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");
        ServiceEndpoint bean = decoratedService.refreshServiceEndpoint(serviceEndpoint);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteServiceEndpoint(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteServiceEndpoint(serviceEndpoint);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteServiceEndpoints(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createServiceEndpoints(serviceEndpoints);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException
    //{
    //}

}
