package com.queryclient.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.DummyEntity;
import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.ws.service.DummyEntityService;
import com.queryclient.af.proxy.DummyEntityServiceProxy;


// MockDummyEntityServiceProxy is a decorator.
// It can be used as a base class to mock DummyEntityServiceProxy objects.
public abstract class MockDummyEntityServiceProxy implements DummyEntityServiceProxy
{
    private static final Logger log = Logger.getLogger(MockDummyEntityServiceProxy.class.getName());

    // MockDummyEntityServiceProxy uses the decorator design pattern.
    private DummyEntityServiceProxy decoratedProxy;

    public MockDummyEntityServiceProxy(DummyEntityServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected DummyEntityServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(DummyEntityServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public DummyEntity getDummyEntity(String guid) throws BaseException
    {
        return decoratedProxy.getDummyEntity(guid);
    }

    @Override
    public Object getDummyEntity(String guid, String field) throws BaseException
    {
        return decoratedProxy.getDummyEntity(guid, field);       
    }

    @Override
    public List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException
    {
        return decoratedProxy.getDummyEntities(guids);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities() throws BaseException
    {
        return getAllDummyEntities(null, null, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllDummyEntities(ordering, offset, count);
        return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDummyEntities(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllDummyEntityKeys(ordering, offset, count);
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count);
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        return decoratedProxy.createDummyEntity(user, name, content, maxLength, expired, status);
    }

    @Override
    public String createDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        return decoratedProxy.createDummyEntity(dummyEntity);
    }

    @Override
    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        return decoratedProxy.updateDummyEntity(guid, user, name, content, maxLength, expired, status);
    }

    @Override
    public Boolean updateDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        return decoratedProxy.updateDummyEntity(dummyEntity);
    }

    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        return decoratedProxy.deleteDummyEntity(guid);
    }

    @Override
    public Boolean deleteDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        String guid = dummyEntity.getGuid();
        return deleteDummyEntity(guid);
    }

    @Override
    public Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteDummyEntities(filter, params, values);
    }

}
