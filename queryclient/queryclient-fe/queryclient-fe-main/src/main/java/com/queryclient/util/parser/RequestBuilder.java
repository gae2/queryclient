package com.queryclient.util.parser;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import com.queryclient.ws.BaseException;

public class RequestBuilder
{
    private static final Logger log = Logger.getLogger(RequestBuilder.class.getName());

    // raw "input"
    private String rootUrl = null;
    private QueryObject queryObj = null;

    // processed "output"
    private URL requestUrl = null;
    private String httpMethod = null;
    private Map<String, String> httpHeaders = null;
    private String payload = null;
    
    // "un-dirty" flag
    private boolean built = false;

    public RequestBuilder(String rootUrl, QueryObject queryObj)
    {
        this.rootUrl = rootUrl;
        this.queryObj = queryObj;
        reset();
    }

    public String getRootUrl()
    {
        return rootUrl;
    }
    public void setRootUrl(String rootUrl)
    {
        this.rootUrl = rootUrl;
        reset();
    }

    public QueryObject getQueryObj()
    {
        return queryObj;
    }
    public void setQueryObj(QueryObject queryObj)
    {
        this.queryObj = queryObj;
        reset();
    }

    private void reset()
    {
        built = false;
        requestUrl = null;
        httpMethod = null;
        httpHeaders = null;
        payload = null;
    }
    
    public URL getRequestUrl() throws BaseException
    {
        if(!built) {
            build();
        }
        return requestUrl;
    }
    public String getHttpMethod() throws BaseException
    {
        if(!built) {
            build();
        }
        return httpMethod;
    }
    public Map<String, String> getHttpHeaders() throws BaseException
    {
        if(!built) {
            build();
        }
        return httpHeaders;
    }
    public String getPayload() throws BaseException
    {
        if(!built) {
            build();
        }
        return payload;
    }


    private void build() throws BaseException
    {
        if(rootUrl == null || queryObj == null) {
            throw new BaseException("Invalid web services url or invalid query object.");
        }
        
        String entities = null;
        if(queryObj.getEntity() != null) {
            entities = Character.toLowerCase(queryObj.getEntity().charAt(0)) + queryObj.getEntity().substring(1);
        } else {
            // This cannot happen.
            throw new BaseException("Query target entity is not set.");
        }
        String path = null;
        // TBD:
        // get media types from the input bean/queryObj..
        String acceptMediaType = MediaType.APPLICATION_JSON;   // temporary
        String contentMediaType = MediaType.APPLICATION_JSON;  // temporary
        httpHeaders = new HashMap<String, String>();
        if(queryObj.getMethod() == QueryMethod.SELECT) {
            httpMethod = "GET";
            if(queryObj.getGuid() == null) {
                path = entities;
                httpHeaders.put("Accept", acceptMediaType);
            } else {
                if(queryObj.getFields() == null || queryObj.getFields().equals("*")) {  // "*" == all.
                    path = entities + "/" + queryObj.getGuid();
                    httpHeaders.put("Accept", acceptMediaType);
                } else {
                    path = entities + "/" + queryObj.getGuid() + "/" + queryObj.getFields();
                    httpHeaders.put("Accept", MediaType.TEXT_PLAIN);
                }
            }
        } else if(queryObj.getMethod() == QueryMethod.CREATE) {
            if(queryObj.getGuid() == null) {
                httpMethod = "POST";
                path = entities;
            } else {
                httpMethod = "PUT";
                path = entities + "/" + queryObj.getGuid();
            }
            payload = queryObj.getPayload();
            httpHeaders.put("Content-Type", contentMediaType);
        } else if(queryObj.getMethod() == QueryMethod.UPDATE) {
            httpMethod = "PUT";
            path = entities + "/" + queryObj.getGuid();
            payload = queryObj.getPayload();
            httpHeaders.put("Content-Type", contentMediaType);
        } else if(queryObj.getMethod() == QueryMethod.DELETE) {
            httpMethod = "DELETE";
            if(queryObj.getGuid() == null) {
                path = entities;
            } else {
                path = entities + "/" + queryObj.getGuid();
            }
            //httpHeaders.put("Accept", MediaType.TEXT_PLAIN);
        } else {
            // This cannot happen.
            //log.warning("Unrecgonized method: " + queryObj.getMethod());
            throw new BaseException("Unrecognized method: " + queryObj.getMethod());
        }
 
        StringBuffer sbUri = new StringBuffer(rootUrl);
        if(!rootUrl.endsWith("/")) {
            sbUri.append("/");
        }
        sbUri.append(path);
        
        String encodedParam = null;
        StringBuffer sbQuery = new StringBuffer();
        if(Boolean.TRUE.equals(queryObj.getUnique())) {
            if(sbQuery.length() == 0) {
                sbQuery.append("?");
            } else {
                sbQuery.append("&");
            }
            sbQuery.append("unique=true");
        }
        if(queryObj.getAggregate() != null) {
            try {
                encodedParam = URLEncoder.encode(queryObj.getAggregate(), "UTF-8");
                if(sbQuery.length() == 0) {
                    sbQuery.append("?");
                } else {
                    sbQuery.append("&");
                }
                sbQuery.append("aggregate=");
                sbQuery.append(encodedParam); 
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to add aggregate param.", e);
            }
        }
        if(queryObj.getFilter() != null) {
            try {
                encodedParam = URLEncoder.encode(queryObj.getFilter(), "UTF-8");
                if(sbQuery.length() == 0) {
                    sbQuery.append("?");
                } else {
                    sbQuery.append("&");
                }
                sbQuery.append("filter=");
                sbQuery.append(encodedParam); 
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to add filter param.", e);
            }
        }
        if(queryObj.getOrdering() != null) {
            try {
                encodedParam = URLEncoder.encode(queryObj.getOrdering(), "UTF-8");
                if(sbQuery.length() == 0) {
                    sbQuery.append("?");
                } else {
                    sbQuery.append("&");
                }
                sbQuery.append("ordering=");
                sbQuery.append(encodedParam); 
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to add order-by param.", e);
            }
        }
        if(queryObj.getGrouping() != null) {
            try {
                encodedParam = URLEncoder.encode(queryObj.getGrouping(), "UTF-8");
                if(sbQuery.length() == 0) {
                    sbQuery.append("?");
                } else {
                    sbQuery.append("&");
                }
                sbQuery.append("grouping=");
                sbQuery.append(encodedParam); 
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to add group-by param.", e);
            }
        }
        if(queryObj.getOffset() != null) {
            if(sbQuery.length() == 0) {
                sbQuery.append("?");
            } else {
                sbQuery.append("&");
            }
            sbQuery.append("offset=");            
            sbQuery.append(queryObj.getOffset()); 
        }
        if(queryObj.getCount() != null) {
            if(sbQuery.length() == 0) {
                sbQuery.append("?");
            } else {
                sbQuery.append("&");
            }
            sbQuery.append("count=");            
            sbQuery.append(queryObj.getCount()); 
        }

        sbUri.append(sbQuery);
        try {
            String strUri = sbUri.toString();
            log.info("Request URL = " + strUri);
            requestUrl = new URL(strUri);
        } catch (MalformedURLException e) {
            throw new BaseException("Failed to create a request uri endpoint", e);
        }
       
        // TBD: Headers???
        // xml/json ???
        
        
        
        // Finally set the flag
        built = true;
    }

    
}
