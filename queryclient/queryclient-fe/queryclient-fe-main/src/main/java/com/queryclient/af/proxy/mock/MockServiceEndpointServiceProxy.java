package com.queryclient.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.ws.service.ServiceEndpointService;
import com.queryclient.af.proxy.ServiceEndpointServiceProxy;


// MockServiceEndpointServiceProxy is a decorator.
// It can be used as a base class to mock ServiceEndpointServiceProxy objects.
public abstract class MockServiceEndpointServiceProxy implements ServiceEndpointServiceProxy
{
    private static final Logger log = Logger.getLogger(MockServiceEndpointServiceProxy.class.getName());

    // MockServiceEndpointServiceProxy uses the decorator design pattern.
    private ServiceEndpointServiceProxy decoratedProxy;

    public MockServiceEndpointServiceProxy(ServiceEndpointServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected ServiceEndpointServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(ServiceEndpointServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public ServiceEndpoint getServiceEndpoint(String guid) throws BaseException
    {
        return decoratedProxy.getServiceEndpoint(guid);
    }

    @Override
    public Object getServiceEndpoint(String guid, String field) throws BaseException
    {
        return decoratedProxy.getServiceEndpoint(guid, field);       
    }

    @Override
    public List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException
    {
        return decoratedProxy.getServiceEndpoints(guids);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException
    {
        return getAllServiceEndpoints(null, null, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllServiceEndpoints(ordering, offset, count);
        return getAllServiceEndpoints(ordering, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllServiceEndpoints(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllServiceEndpointKeys(ordering, offset, count);
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count);
        return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createServiceEndpoint(String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        return decoratedProxy.createServiceEndpoint(user, dataService, serviceName, serviceUrl, authRequired, authCredential, status);
    }

    @Override
    public String createServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        return decoratedProxy.createServiceEndpoint(serviceEndpoint);
    }

    @Override
    public Boolean updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        return decoratedProxy.updateServiceEndpoint(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredential, status);
    }

    @Override
    public Boolean updateServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        return decoratedProxy.updateServiceEndpoint(serviceEndpoint);
    }

    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        return decoratedProxy.deleteServiceEndpoint(guid);
    }

    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        String guid = serviceEndpoint.getGuid();
        return deleteServiceEndpoint(guid);
    }

    @Override
    public Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteServiceEndpoints(filter, params, values);
    }

}
