package com.queryclient.af.auth.common;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;


public final class AuthStateUtil
{
    private static final Logger log = Logger.getLogger(AuthStateUtil.class.getName());

    private AuthStateUtil() {}

    
    // Note that we store the string version of AuthState, not the object itself, in the session.
    // This is done through AuthState.writeObject() .readObject().
    // We do not need to string-serialize here...
    public static AuthState getAuthStateInSession(HttpSession session)
    {
        if(session == null) {
            return null;
        }
//        AuthState authState = null;
//        String authStateStr = (String) session.getAttribute(CommonAuthUtil.SESSION_ATTR_AUTHSTATE_STRING);
//        if(authStateStr != null) {
//            authState = AuthState.valueOf(authStateStr);
//        }   
        AuthState authState = (AuthState) session.getAttribute(CommonAuthUtil.SESSION_ATTR_AUTHSTATE);
        return authState;
    }
    public static void setAuthStateInSession(HttpSession session, AuthState authState)
    {
        if(session == null) {
            return;
        }
//        if(authState != null) {
//            String strAuthState = authState.toString();
//            session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHSTATE_STRING, strAuthState);
//        } else {
//            // do nothing.
//        }
        // What happens if authState is null?
        session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHSTATE, authState);
    }
    public static void removeAuthStateInSession(HttpSession session)
    {
        if(session == null) {
            return;
        }
//        session.removeAttribute(CommonAuthUtil.SESSION_ATTR_AUTHSTATE_STRING);
        session.removeAttribute(CommonAuthUtil.SESSION_ATTR_AUTHSTATE);
    }
    public static void removeAuthStateForProviderAndUserId(HttpSession session, String providerId, String userId)
    {
        if(session == null) {
            return;
        }
        AuthState authState = getAuthStateInSession(session);
        if(authState != null) {
            authState.remove(providerId, userId);
//            String strAuthState = authState.toString();
//            session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHSTATE_STRING, strAuthState);
            session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHSTATE, authState);
        } else {
            // do nothing.
        }
    }

    
    
}
