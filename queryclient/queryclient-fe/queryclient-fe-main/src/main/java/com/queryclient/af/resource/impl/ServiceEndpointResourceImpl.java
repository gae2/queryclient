package com.queryclient.af.resource.impl;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.DatatypeConverter;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.json.JSONWithPadding;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.af.config.Config;
import com.queryclient.af.auth.TwoLeggedOAuthProvider;
import com.queryclient.af.auth.common.AuthMethod;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;
import com.queryclient.ws.resource.exception.UnauthorizedRsException;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.ServiceEndpointStub;
import com.queryclient.ws.stub.ServiceEndpointListStub;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.service.manager.ServiceManager;
import com.queryclient.af.resource.ServiceEndpointResource;
import com.queryclient.af.resource.util.ConsumerKeySecretPairResourceUtil;


@Path("/serviceEndpoints/")
public class ServiceEndpointResourceImpl extends BaseResourceImpl implements ServiceEndpointResource
{
    private static final Logger log = Logger.getLogger(ServiceEndpointResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public ServiceEndpointResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;

        // TBD:
        // The auth-related checking should be done here rather than putting the same code in every method... ???
        // ...
    }

    // Throws exception if auth check fails
    private void doAuthCheck() throws BaseResourceException
    {
        // if(isRunningOnDevel && isIgnoreAuth()) {
        if(isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, then it could be a serious error.");
        } else {
            String authMethod = getAuthMethod();
            if(authMethod.equals(AuthMethod.NONE)) {
                // Auth ignored.
            } else if(authMethod.equals(AuthMethod.BASIC)) {
                String authType = null;
                String encodedAuthToken = null;
                if(httpHeaders != null) {
                    List<String> authList = httpHeaders.getRequestHeader("Authorization");
                    if(authList != null && !authList.isEmpty()) {
                        String authLine = authList.get(0);    // Use the first value.
                        String[] parts = authLine.split("\\s+", 2);
                        authType = parts[0];
                        encodedAuthToken = parts[1];
                    }
                }
                if(authType == null || ! authType.equalsIgnoreCase("Basic")) {
                    log.warning("HTTP Basic authentication failed. Request authType = " + authType);
                    throw new UnauthorizedRsException("HTTP Basic authentication failed. Request authType = " + authType, resourceUri);
                }
                String authToken = null;
                if(encodedAuthToken != null) {
                    byte[] decodedBytes = DatatypeConverter.parseBase64Binary(encodedAuthToken);
                    authToken = new String(decodedBytes);
                }
                String serverUsernamePasswordPair = getHttpBasicAuthUsernameAndPassword(null);
                if(authToken == null || ! authToken.equals(serverUsernamePasswordPair)) {
                    log.warning("HTTP Basic authentication failed: Invalid username/password.");
                    throw new UnauthorizedRsException("HTTP Basic authentication failed: Invalid username/password.", resourceUri);                        
                }
            } else if(authMethod.equals(AuthMethod.BEARER)) {
                String tokenType = null;
                String bearerToken = null;
                if(httpHeaders != null) {
                    List<String> authList = httpHeaders.getRequestHeader("Authorization");
                    if(authList != null && !authList.isEmpty()) {
                        String authLine = authList.get(0);    // Use the first value.
                        String[] parts = authLine.split("\\s+", 2);
                        tokenType = parts[0];
                        bearerToken = parts[1];
                    }
                }
                if(tokenType == null || ! tokenType.equalsIgnoreCase("Bearer")) {
                    log.warning("OAuth2 authorization failed. Request tokenType = " + tokenType);
                    throw new UnauthorizedRsException("OAuth2 authorization failed. Request tokenType = " + tokenType, resourceUri);
                }
                String serverUserAccessToken = getUserOAuth2AccessToken(null);
                if(bearerToken == null || ! bearerToken.equals(serverUserAccessToken)) {
                    log.warning("OAuth2 authorization failed: Invalid bearer token");
                    throw new UnauthorizedRsException("OAuth2 authorization failed: Invalid bearer token", resourceUri);                        
                }
            } else if(authMethod.equals(AuthMethod.TWOLEGGED)) {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            } else {
                // Unsupported auth method.
                log.warning("AuthMethod currently unsupported: " + authMethod);
                throw new UnauthorizedRsException("AuthMethod currently unsupported: " + authMethod, resourceUri);
            }
        }
    }


    private Response getServiceEndpointList(List<ServiceEndpoint> beans, StringCursor forwardCursor) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }

        ServiceEndpointListStub listStub = null;
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            listStub = new ServiceEndpointListStub();
        } else {
            List<ServiceEndpointStub> stubs = new ArrayList<ServiceEndpointStub>();
            Iterator<ServiceEndpoint> it = beans.iterator();
            while(it.hasNext()) {
                ServiceEndpoint bean = (ServiceEndpoint) it.next();
                stubs.add(ServiceEndpointStub.convertBeanToStub(bean));
            }
            listStub = new ServiceEndpointListStub(stubs);
        }
        String cursorString = null;
        if(forwardCursor != null) {
            cursorString = forwardCursor.getWebSafeString();
        }
        listStub.setForwardCursor(cursorString);
        return Response.ok(listStub).build();        
    }

    // TBD
    private Response getServiceEndpointListAsJsonp(List<ServiceEndpoint> beans, StringCursor forwardCursor, String callback) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }

        ServiceEndpointListStub listStub = null;
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            listStub = new ServiceEndpointListStub();
        } else {
            List<ServiceEndpointStub> stubs = new ArrayList<ServiceEndpointStub>();
            Iterator<ServiceEndpoint> it = beans.iterator();
            while(it.hasNext()) {
                ServiceEndpoint bean = (ServiceEndpoint) it.next();
                stubs.add(ServiceEndpointStub.convertBeanToStub(bean));
            }
            listStub = new ServiceEndpointListStub(stubs);
        }
        String cursorString = null;
        if(forwardCursor != null) {
            cursorString = forwardCursor.getWebSafeString();
        }
        listStub.setForwardCursor(cursorString);
        return Response.ok(new JSONWithPadding(listStub, callback)).build();        
    }

    @Override
    public Response getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<ServiceEndpoint> beans = ServiceManager.getServiceEndpointService().getAllServiceEndpoints(ordering, offset, count, forwardCursor);
            return getServiceEndpointList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<String> keys = ServiceManager.getServiceEndpointService().getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<String> keys = ServiceManager.getServiceEndpointService().findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<ServiceEndpoint> beans = ServiceManager.getServiceEndpointService().findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return getServiceEndpointList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findServiceEndpointsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<ServiceEndpoint> beans = ServiceManager.getServiceEndpointService().findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return getServiceEndpointListAsJsonp(beans, forwardCursor, callback);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            Long count = ServiceManager.getServiceEndpointService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
//    public Response getServiceEndpointAsHtml(String guid) throws BaseResourceException
//    {
//        // TBD
//        throw new NotImplementedRsException("Html format currently not supported.", resourceUri);
//    }

    @Override
    public Response getServiceEndpoint(String guid) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            ServiceEndpoint bean = ServiceManager.getServiceEndpointService().getServiceEndpoint(guid);
            ServiceEndpointStub stub = ServiceEndpointStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("ServiceEndpoint stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full ServiceEndpoint stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getServiceEndpointAsJsonp(String guid, String callback) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            ServiceEndpoint bean = ServiceManager.getServiceEndpointService().getServiceEndpoint(guid);
            ServiceEndpointStub stub = ServiceEndpointStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("ServiceEndpoint stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full ServiceEndpoint stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getServiceEndpoint(String guid, String field) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            if(guid != null) {
                guid = guid.toLowerCase();  // TBD: Validate/normalize guid....
            }
            if(field == null || field.trim().length() == 0) {
                return getServiceEndpoint(guid);
            }
            ServiceEndpoint bean = ServiceManager.getServiceEndpointService().getServiceEndpoint(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("user")) {
                    String fval = bean.getUser();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("dataService")) {
                    String fval = bean.getDataService();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("serviceName")) {
                    String fval = bean.getServiceName();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("serviceUrl")) {
                    String fval = bean.getServiceUrl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("authRequired")) {
                    Boolean fval = bean.isAuthRequired();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("authCredential")) {
                    ConsumerKeySecretPair fval = bean.getAuthCredential();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("status")) {
                    String fval = bean.getStatus();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    // TBD
    @Override
    public Response constructServiceEndpoint(ServiceEndpointStub serviceEndpoint) throws BaseResourceException
    {
        doAuthCheck();

        try {
            ServiceEndpointBean bean = convertServiceEndpointStubToBean(serviceEndpoint);
            bean = (ServiceEndpointBean) ServiceManager.getServiceEndpointService().constructServiceEndpoint(bean);
            serviceEndpoint = ServiceEndpointStub.convertBeanToStub(bean);
            String guid = serviceEndpoint.getGuid();
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(serviceEndpoint).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createServiceEndpoint(ServiceEndpointStub serviceEndpoint) throws BaseResourceException
    {
        doAuthCheck();

        try {
            ServiceEndpointBean bean = convertServiceEndpointStubToBean(serviceEndpoint);
            String guid = ServiceManager.getServiceEndpointService().createServiceEndpoint(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createServiceEndpoint(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        doAuthCheck();

        try {
            ServiceEndpointBean bean = convertFormParamsToBean(formParams);
            String guid = ServiceManager.getServiceEndpointService().createServiceEndpoint(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    // TBD
    @Override
    public Response refreshServiceEndpoint(String guid, ServiceEndpointStub serviceEndpoint) throws BaseResourceException
    {
        doAuthCheck();

        try {
            if(serviceEndpoint == null || !guid.equals(serviceEndpoint.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from serviceEndpoint guid = " + serviceEndpoint.getGuid());
                throw new RequestForbiddenException("Failed to refresh the serviceEndpoint with guid = " + guid);
            }
            ServiceEndpointBean bean = convertServiceEndpointStubToBean(serviceEndpoint);
            bean = (ServiceEndpointBean) ServiceManager.getServiceEndpointService().refreshServiceEndpoint(bean);
            if(bean == null) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the serviceEndpoint with guid = " + guid);
                throw new InternalServerErrorException("Failed to refresh the serviceEndpoint with guid = " + guid);
            }
            serviceEndpoint = ServiceEndpointStub.convertBeanToStub(bean);
            return Response.ok(serviceEndpoint).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateServiceEndpoint(String guid, ServiceEndpointStub serviceEndpoint) throws BaseResourceException
    {
        doAuthCheck();

        try {
            if(serviceEndpoint == null || !guid.equals(serviceEndpoint.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from serviceEndpoint guid = " + serviceEndpoint.getGuid());
                throw new RequestForbiddenException("Failed to update the serviceEndpoint with guid = " + guid);
            }
            ServiceEndpointBean bean = convertServiceEndpointStubToBean(serviceEndpoint);
            boolean suc = ServiceManager.getServiceEndpointService().updateServiceEndpoint(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the serviceEndpoint with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the serviceEndpoint with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, String authCredential, String status)
    {
        doAuthCheck();

        try {
            /*
            boolean suc = ServiceManager.getServiceEndpointService().updateServiceEndpoint(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredential, status);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the serviceEndpoint with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the serviceEndpoint with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
        //} catch(BadRequestException ex) {
        //    throw new BadRequestRsException(ex, resourceUri);
        //} catch(ResourceNotFoundException ex) {
        //    throw new ResourceNotFoundRsException(ex, resourceUri);
        //} catch(ResourceGoneException ex) {
        //    throw new ResourceGoneRsException(ex, resourceUri);
        //} catch(RequestForbiddenException ex) {
        //    throw new RequestForbiddenRsException(ex, resourceUri);
        //} catch(RequestConflictException ex) {
        //    throw new RequestConflictRsException(ex, resourceUri);
        //} catch(ServiceUnavailableException ex) {
        //    throw new ServiceUnavailableRsException(ex, resourceUri);
        //} catch(InternalServerErrorException ex) {
        //    throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response updateServiceEndpoint(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        doAuthCheck();

        try {
            ServiceEndpointBean bean = convertFormParamsToBean(formParams);
            boolean suc = ServiceManager.getServiceEndpointService().updateServiceEndpoint(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the serviceEndpoint with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the serviceEndpoint with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteServiceEndpoint(String guid) throws BaseResourceException
    {
        doAuthCheck();

        try {
            boolean suc = ServiceManager.getServiceEndpointService().deleteServiceEndpoint(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the serviceEndpoint with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the serviceEndpoint with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseResourceException
    {
        doAuthCheck();

        try {
            Long count = ServiceManager.getServiceEndpointService().deleteServiceEndpoints(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    @Override
    public Response createServiceEndpoints(ServiceEndpointListStub serviceEndpoints) throws BaseResourceException
    {
        doAuthCheck();

        try {
            List<ServiceEndpointStub> stubs = serviceEndpoints.getList();
            List<ServiceEndpoint> beans = new ArrayList<ServiceEndpoint>();
            for(ServiceEndpointStub stub : stubs) {
                ServiceEndpointBean bean = convertServiceEndpointStubToBean(stub);
                beans.add(bean);
            }
            Integer count = ServiceManager.getServiceEndpointService().createServiceEndpoints(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    public static ServiceEndpointBean convertServiceEndpointStubToBean(ServiceEndpoint stub)
    {
        ServiceEndpointBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new ServiceEndpointBean();
            bean.setGuid(stub.getGuid());
            bean.setUser(stub.getUser());
            bean.setDataService(stub.getDataService());
            bean.setServiceName(stub.getServiceName());
            bean.setServiceUrl(stub.getServiceUrl());
            bean.setAuthRequired(stub.isAuthRequired());
            bean.setAuthCredential(ConsumerKeySecretPairResourceUtil.convertConsumerKeySecretPairStubToBean(stub.getAuthCredential()));
            bean.setStatus(stub.getStatus());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<ServiceEndpointBean> convertServiceEndpointListStubToBeanList(ServiceEndpointListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<ServiceEndpointStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<ServiceEndpointBean> beanList = new ArrayList<ServiceEndpointBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(ServiceEndpointStub stub : stubList) {
                    ServiceEndpointBean bean = convertServiceEndpointStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }


    // TBD
    // This needs to be implemented before url-encoded form create/update can be supported 
    public static ServiceEndpointBean convertFormParamsToBean(MultivaluedMap<String, String> formParams)
    {
        ServiceEndpointBean bean = new ServiceEndpointBean();
        if(formParams == null) {
            log.log(Level.INFO, "FormParams is null. Empty bean is returned.");
        } else {
            Iterator<MultivaluedMap.Entry<String,List<String>>> it = formParams.entrySet().iterator();
            while(it.hasNext()) {
                MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
                String key = (String) m.getKey();
                String val = null;
                List<String> list = m.getValue();
                if(list != null && list.size() > 0) {
                    val = list.get(0);
                    if(key.equals("guid")) {
                        bean.setGuid(val);                        
                    } else if(key.equals("user")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setUser(v);
                    } else if(key.equals("dataService")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setDataService(v);
                    } else if(key.equals("serviceName")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setServiceName(v);
                    } else if(key.equals("serviceUrl")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setServiceUrl(v);
                    } else if(key.equals("authRequired")) {
                        // TBD: Needs to use "parse()" methods.
                        //Boolean v = (Boolean) val;
                        //bean.setAuthRequired(v);
                    } else if(key.equals("authCredential")) {
                        // TBD: Add "valueOf()" static method to the bean object.
                        //bean.setAuthCredential(AuthCredentialBean.valueOf(val));
                    } else if(key.equals("status")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setStatus(v);
                    } else if(key.equals("createdTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setCreatedTime(v);
                    } else if(key.equals("modifiedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setModifiedTime(v);
                    }
                }
            }
        }
        return bean;
    }

}
