package com.queryclient.af.service;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UserAuthStateService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UserAuthState getUserAuthState(String guid) throws BaseException;
    Object getUserAuthState(String guid, String field) throws BaseException;
    List<UserAuthState> getUserAuthStates(List<String> guids) throws BaseException;
    List<UserAuthState> getAllUserAuthStates() throws BaseException;
    /* @Deprecated */ List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count) throws BaseException;
    List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUserAuthState(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException;
    //String createUserAuthState(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserAuthState?)
    String createUserAuthState(UserAuthState userAuthState) throws BaseException;
    UserAuthState constructUserAuthState(UserAuthState userAuthState) throws BaseException;
    Boolean updateUserAuthState(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException;
    //Boolean updateUserAuthState(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserAuthState(UserAuthState userAuthState) throws BaseException;
    UserAuthState refreshUserAuthState(UserAuthState userAuthState) throws BaseException;
    Boolean deleteUserAuthState(String guid) throws BaseException;
    Boolean deleteUserAuthState(UserAuthState userAuthState) throws BaseException;
    Long deleteUserAuthStates(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createUserAuthStates(List<UserAuthState> userAuthStates) throws BaseException;
//    Boolean updateUserAuthStates(List<UserAuthState> userAuthStates) throws BaseException;

}
