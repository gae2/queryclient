package com.queryclient.af.auth;

import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.af.util.SimpleEncryptUtil;


// Session bean is used to store "persistent session" information in a cookie.
// Note:
// To make this more light-weight, we removed Jackson dependency...
// Json parsing/serialization is hand-coded now...
// ....
// Note:
// We do not really have to use json format for serialization.
// ....
// @JsonIgnoreProperties(ignoreUnknown = true)
public class SessionBean implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SessionBean.class.getName());

//    private static ObjectMapper sObjectMapper = null;
//    private static ObjectMapper getObjectMapper()
//    {
//        if(sObjectMapper == null) {
//            sObjectMapper = new ObjectMapper(); // can reuse, share globally
//            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
//            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
//        }
//        return sObjectMapper;
//    }

    // Note:
    // If we change the fields (e.g., by changing their data types, or by adding/removing fields),
    //    we have to change the JSON parsing/serialization code below.....
    private String guid;
    private String user;         // User.guid.
//    private String userId;     // external user id???  <-- this does not make sense without providerId.
    private String token;
    private String ipAddress;
    private String referer;
    private String status;
    private Long authenticatedTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;


    // Ctors.
    public SessionBean()
    {
        this((String) null);
    }
    public SessionBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public SessionBean(String guid, String user, String token, String ipAddress, String referer, String status, Long authenticatedTime, Long expirationTime)
    {
        this(guid, user, token, ipAddress, referer, status, authenticatedTime, expirationTime, null, null);
    }
    public SessionBean(String guid, String user, String token, String ipAddress, String referer, String status, Long authenticatedTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.token = token;
        this.ipAddress = ipAddress;
        this.referer = referer;
        this.status = status;
        this.authenticatedTime = authenticatedTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }


    public static SessionBean fromJsonString(String jsonStr)
    {
//        SessionBean bean = null;
//        try {
//            bean = getObjectMapper().readValue(jsonStr, SessionBean.class);
//        } catch (JsonParseException e) {
//            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
//        } catch (JsonProcessingException e) {
//            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
//        } catch (IOException e) {
//            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
//        }
//        return bean;

        // TBD:
        // Is this hand-coded parsing more efficient than using a generic JSOn parser ????
        // ...

        if(jsonStr == null || jsonStr.isEmpty()) {
            return null;    // ???
        }

        // Since we know the exact format of the serialized session bean,
        //     we do not have to check a lot of things (including spaces, etc.)...
        // --> Although we use "json", it's not exactly a json.
        // This parsing will only work with the seriazlied string using SessionBean.toJsonString()...
        // ....

//        int idx1 = jsonStr.indexOf("{");
//        int idx2 = jsonStr.lastIndexOf("}");
//        if(idx1 < 0 || idx2 < 0) {
//            return null;  // ???
//        }
        
//        // jsonStr = jsonStr.substring(idx1+1, idx2).trim();
//        jsonStr = jsonStr.substring(idx1+1, idx2);

        // jsonStr = jsonStr.substring(1, jsonStr.length()-1).trim();
        jsonStr = jsonStr.substring(1, jsonStr.length()-1);

        // TBD: Does it really save any to not check spaces ????
        // StringTokenizer st = new StringTokenizer(jsonStr, ",\\s*");
        StringTokenizer st = new StringTokenizer(jsonStr, ",");

        SessionBean bean = new SessionBean();
        while(st.hasMoreTokens()) {
            String tok = st.nextToken();
            // String[] row = tok.split("\\s*:\\s*", 2);
            String[] row = tok.split(":", 2);
//            String key = row[0].substring(1, row[0].length()-1);
//            switch(key) {
            switch(row[0]) {
            // case "guid":
            case "\"guid\"":
                if(! row[1].equals("null")) {
                    String guid = row[1].substring(1, row[1].length()-1);
                    bean.setGuid(guid);
                }
                break;
            // case "user":
            case "\"user\"":
                if(! row[1].equals("null")) {
                    String user = row[1].substring(1, row[1].length()-1);
                    bean.setUser(user);
                }
                break;
            // case "token":
            case "\"token\"":
                if(! row[1].equals("null")) {
                    String token = row[1].substring(1, row[1].length()-1);
                    bean.setToken(token);
                }
                break;
            // case "ipAddress":
            case "\"ipAddress\"":
                if(! row[1].equals("null")) {
                    String ipAddress = row[1].substring(1, row[1].length()-1);
                    bean.setIpAddress(ipAddress);
                }
                break;
            // case "referer":
            case "\"referer\"":
                if(! row[1].equals("null")) {
                    String referer = row[1].substring(1, row[1].length()-1);
                    bean.setReferer(referer);
                }
                break;
            // case "status":
            case "\"status\"":
                if(! row[1].equals("null")) {
                    String status = row[1].substring(1, row[1].length()-1);
                    bean.setStatus(status);
                }
                break;
            // case "authenticatedTime":
            case "\"authenticatedTime\"":
                if(! row[1].equals("null")) {
                    String timeStr = row[1];
                    long authenticatedTime = 0L;
                    try {
                        authenticatedTime = Long.parseLong(timeStr);
                    } catch(Exception e) {
                        // ignore. This should not normally happen.
                        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse authenticatedTime = " + timeStr, e);
                    }
                    bean.setAuthenticatedTime(authenticatedTime);
                }
                break;
            // case "expirationTime":
            case "\"expirationTime\"":
                if(! row[1].equals("null")) {
                    String timeStr = row[1];
                    long expirationTime = 0L;
                    try {
                        expirationTime = Long.parseLong(timeStr);
                    } catch(Exception e) {
                        // ignore. This should not normally happen.
                        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse expirationTime = " + timeStr, e);
                    }
                    bean.setExpirationTime(expirationTime);
                }
                break;
            // case "createdTime":
            case "\"createdTime\"":
                if(! row[1].equals("null")) {
                    String timeStr = row[1];
                    long createdTime = 0L;
                    try {
                        createdTime = Long.parseLong(timeStr);
                    } catch(Exception e) {
                        // ignore. This should not normally happen.
                        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse createdTime = " + timeStr, e);
                    }
                    bean.setCreatedTime(createdTime);
                }
                break;
            // case "modifiedTime":
            case "\"modifiedTime\"":
                if(! row[1].equals("null")) {
                    String timeStr = row[1];
                    long modifiedTime = 0L;
                    try {
                        modifiedTime = Long.parseLong(timeStr);
                    } catch(Exception e) {
                        // ignore. This should not normally happen.
                        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse modifiedTime = " + timeStr, e);
                    }
                    bean.setModifiedTime(modifiedTime);
                }
                break;
            default:
                // ???
                // if(log.isLoggable(Level.INFO)) log.info("Unrecognized field: " + key);
                if(log.isLoggable(Level.INFO)) log.info("Unrecognized field: " + row[0]);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("SessionBean created from JSON string: " + bean);
        return bean;
    }

    public static SessionBean fromEncryptedJsonString(String encJsonStr)
    {
        String jsonStr = SimpleEncryptUtil.decrypt(encJsonStr);
        return fromJsonString(jsonStr);
    }
    
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @Deprecated
    public String getUserId()
    {
        return getUser();
    }
    @Deprecated
    public void setUserId(String userId)
    {
        setUser(userId);
    }

//    public String getUserId()
//    {
//        return userId;
//    }
//    public void setUserId(String userId)
//    {
//        this.userId = userId;
//    }

    public String getToken()
    {
        return this.token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }

    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public String getReferer()
    {
        return this.referer;
    }
    public void setReferer(String referer)
    {
        this.referer = referer;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getAuthenticatedTime()
    {
        return this.authenticatedTime;
    }
    public void setAuthenticatedTime(Long authenticatedTime)
    {
        this.authenticatedTime = authenticatedTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    public String toJsonString()
    {
//        String jsonStr = null;
//        try {
//            StringWriter writer = new StringWriter();
//            getObjectMapper().writeValue(writer, this);
//            jsonStr = writer.toString();
//        } catch (IOException e) {
//            log.log(Level.WARNING, "Exception while writing jsonString.", e);
//        }
//        return jsonStr;

        // TBD:
        // Is this hand-coded serialization more efficient than using a generic JSOn builder ????
        // ...

        // Note that we do not have to escape any of the strings
        //     since we know they are all valid json strings.
        StringBuilder sb = new StringBuilder();
        
        sb.append("{");
        if(guid != null) {
            sb.append("\"guid\":\"").append(guid).append("\",");
        } else {
            sb.append("\"guid\":null,");
        }
        if(user != null) {
            sb.append("\"user\":\"").append(user).append("\",");
        } else {
            sb.append("\"user\":null,");
        }
        if(token != null) {
            sb.append("\"token\":\"").append(token).append("\",");
        } else {
            sb.append("\"token\":null,");
        }
        if(ipAddress != null) {
            sb.append("\"ipAddress\":\"").append(ipAddress).append("\",");
        } else {
            sb.append("\"ipAddress\":null,");
        }
        if(referer != null) {
            sb.append("\"referer\":\"").append(referer).append("\",");
        } else {
            sb.append("\"referer\":null,");
        }
        if(status != null) {
            sb.append("\"status\":\"").append(status).append("\",");
        } else {
            sb.append("\"status\":null,");
        }
        if(authenticatedTime != null) {
            sb.append("\"authenticatedTime\":").append(authenticatedTime).append(",");
        } else {
            sb.append("\"authenticatedTime\":null,");   // null or 0 ??
        }
        if(expirationTime != null) {
            sb.append("\"expirationTime\":").append(expirationTime).append(",");
        } else {
            sb.append("\"expirationTime\":null,");   // null or 0 ??
        }
        if(createdTime != null) {
            sb.append("\"createdTime\":").append(createdTime).append(",");
        } else {
            sb.append("\"createdTime\":null,");   // null or 0 ??
        }
        if(modifiedTime != null) {
            sb.append("\"modifiedTime\":").append(modifiedTime);
        } else {
            sb.append("\"modifiedTime\":null");   // null or 0 ??
        }
        sb.append("}");

        return sb.toString();
    }

    public String toEncryptedJsonString()
    {
        String jsonStr = toJsonString();
        return SimpleEncryptUtil.encrypt(jsonStr);
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("guid = " + this.guid).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("token = " + this.token).append(";");
        sb.append("ipAddress = " + this.ipAddress).append(";");
        sb.append("referer = " + this.referer).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("authenticatedTime = " + this.authenticatedTime).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        hash = 31 * hash + delta;
        delta = user == null ? 0 : user.hashCode();
        hash = 31 * hash + delta;
        delta = token == null ? 0 : token.hashCode();
        hash = 31 * hash + delta;
        delta = ipAddress == null ? 0 : ipAddress.hashCode();
        hash = 31 * hash + delta;
        delta = referer == null ? 0 : referer.hashCode();
        hash = 31 * hash + delta;
        delta = status == null ? 0 : status.hashCode();
        hash = 31 * hash + delta;
        delta = authenticatedTime == null ? 0 : authenticatedTime.hashCode();
        hash = 31 * hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        hash = 31 * hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        hash = 31 * hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        hash = 31 * hash + delta;
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        } 
        if(obj == null) {
            return false;
        }
        if(getClass() != obj.getClass()) {
            return false;
        } else {
            SessionBean other = (SessionBean) obj;
            // TBD:
            return (this.hashCode() == other.hashCode());
        }
    }

}
