package com.queryclient.af.service.impl;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.service.AbstractServiceFactory;
import com.queryclient.af.service.ApiConsumerService;
import com.queryclient.af.service.UserService;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.af.service.ExternalUserAuthService;
import com.queryclient.af.service.UserAuthStateService;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.service.ServiceInfoService;
import com.queryclient.af.service.FiveTenService;

public class ImplServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ImplServiceFactory.class.getName());

    private ImplServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ImplServiceFactoryHolder
    {
        private static final ImplServiceFactory INSTANCE = new ImplServiceFactory();
    }

    // Singleton method
    public static ImplServiceFactory getInstance()
    {
        return ImplServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerServiceImpl();
    }

    @Override
    public UserService getUserService()
    {
        return new UserServiceImpl();
    }

    @Override
    public UserPasswordService getUserPasswordService()
    {
        return new UserPasswordServiceImpl();
    }

    @Override
    public ExternalUserAuthService getExternalUserAuthService()
    {
        return new ExternalUserAuthServiceImpl();
    }

    @Override
    public UserAuthStateService getUserAuthStateService()
    {
        return new UserAuthStateServiceImpl();
    }

    @Override
    public DataServiceService getDataServiceService()
    {
        return new DataServiceServiceImpl();
    }

    @Override
    public ServiceEndpointService getServiceEndpointService()
    {
        return new ServiceEndpointServiceImpl();
    }

    @Override
    public QuerySessionService getQuerySessionService()
    {
        return new QuerySessionServiceImpl();
    }

    @Override
    public QueryRecordService getQueryRecordService()
    {
        return new QueryRecordServiceImpl();
    }

    @Override
    public DummyEntityService getDummyEntityService()
    {
        return new DummyEntityServiceImpl();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoServiceImpl();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenServiceImpl();
    }


}
