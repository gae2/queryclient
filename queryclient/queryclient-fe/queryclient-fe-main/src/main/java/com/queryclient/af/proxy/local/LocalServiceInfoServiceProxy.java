package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ServiceInfo;
// import com.queryclient.ws.bean.ServiceInfoBean;
import com.queryclient.ws.service.ServiceInfoService;
import com.queryclient.af.bean.ServiceInfoBean;
import com.queryclient.af.proxy.ServiceInfoServiceProxy;


public class LocalServiceInfoServiceProxy extends BaseLocalServiceProxy implements ServiceInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalServiceInfoServiceProxy.class.getName());

    public LocalServiceInfoServiceProxy()
    {
    }

    @Override
    public ServiceInfo getServiceInfo(String guid) throws BaseException
    {
        ServiceInfo serverBean = getServiceInfoService().getServiceInfo(guid);
        ServiceInfo appBean = convertServerServiceInfoBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getServiceInfo(String guid, String field) throws BaseException
    {
        return getServiceInfoService().getServiceInfo(guid, field);       
    }

    @Override
    public List<ServiceInfo> getServiceInfos(List<String> guids) throws BaseException
    {
        List<ServiceInfo> serverBeanList = getServiceInfoService().getServiceInfos(guids);
        List<ServiceInfo> appBeanList = convertServerServiceInfoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos() throws BaseException
    {
        return getAllServiceInfos(null, null, null);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getServiceInfoService().getAllServiceInfos(ordering, offset, count);
        return getAllServiceInfos(ordering, offset, count, null);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ServiceInfo> serverBeanList = getServiceInfoService().getAllServiceInfos(ordering, offset, count, forwardCursor);
        List<ServiceInfo> appBeanList = convertServerServiceInfoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getServiceInfoService().getAllServiceInfoKeys(ordering, offset, count);
        return getAllServiceInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getServiceInfoService().getAllServiceInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findServiceInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getServiceInfoService().findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count);
        return findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ServiceInfo> serverBeanList = getServiceInfoService().findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<ServiceInfo> appBeanList = convertServerServiceInfoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getServiceInfoService().findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getServiceInfoService().findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getServiceInfoService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createServiceInfo(String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        return getServiceInfoService().createServiceInfo(title, content, type, status, scheduledTime);
    }

    @Override
    public String createServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        com.queryclient.ws.bean.ServiceInfoBean serverBean =  convertAppServiceInfoBeanToServerBean(serviceInfo);
        return getServiceInfoService().createServiceInfo(serverBean);
    }

    @Override
    public Boolean updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        return getServiceInfoService().updateServiceInfo(guid, title, content, type, status, scheduledTime);
    }

    @Override
    public Boolean updateServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        com.queryclient.ws.bean.ServiceInfoBean serverBean =  convertAppServiceInfoBeanToServerBean(serviceInfo);
        return getServiceInfoService().updateServiceInfo(serverBean);
    }

    @Override
    public Boolean deleteServiceInfo(String guid) throws BaseException
    {
        return getServiceInfoService().deleteServiceInfo(guid);
    }

    @Override
    public Boolean deleteServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        com.queryclient.ws.bean.ServiceInfoBean serverBean =  convertAppServiceInfoBeanToServerBean(serviceInfo);
        return getServiceInfoService().deleteServiceInfo(serverBean);
    }

    @Override
    public Long deleteServiceInfos(String filter, String params, List<String> values) throws BaseException
    {
        return getServiceInfoService().deleteServiceInfos(filter, params, values);
    }




    public static ServiceInfoBean convertServerServiceInfoBeanToAppBean(ServiceInfo serverBean)
    {
        ServiceInfoBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new ServiceInfoBean();
            bean.setGuid(serverBean.getGuid());
            bean.setTitle(serverBean.getTitle());
            bean.setContent(serverBean.getContent());
            bean.setType(serverBean.getType());
            bean.setStatus(serverBean.getStatus());
            bean.setScheduledTime(serverBean.getScheduledTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ServiceInfo> convertServerServiceInfoBeanListToAppBeanList(List<ServiceInfo> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<ServiceInfo> beanList = new ArrayList<ServiceInfo>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(ServiceInfo sb : serverBeanList) {
                ServiceInfoBean bean = convertServerServiceInfoBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.ServiceInfoBean convertAppServiceInfoBeanToServerBean(ServiceInfo appBean)
    {
        com.queryclient.ws.bean.ServiceInfoBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.ServiceInfoBean();
            bean.setGuid(appBean.getGuid());
            bean.setTitle(appBean.getTitle());
            bean.setContent(appBean.getContent());
            bean.setType(appBean.getType());
            bean.setStatus(appBean.getStatus());
            bean.setScheduledTime(appBean.getScheduledTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ServiceInfo> convertAppServiceInfoBeanListToServerBeanList(List<ServiceInfo> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<ServiceInfo> beanList = new ArrayList<ServiceInfo>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(ServiceInfo sb : appBeanList) {
                com.queryclient.ws.bean.ServiceInfoBean bean = convertAppServiceInfoBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
