package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.stub.QuerySessionStub;


// Wrapper class + bean combo.
public class QuerySessionBean implements QuerySession, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QuerySessionBean.class.getName());

    // [1] With an embedded object.
    private QuerySessionStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String dataService;
    private String serviceUrl;
    private String inputFormat;
    private String outputFormat;
    private ReferrerInfoStructBean referrerInfo;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public QuerySessionBean()
    {
        //this((String) null);
    }
    public QuerySessionBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public QuerySessionBean(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStructBean referrerInfo, String status, String note)
    {
        this(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note, null, null);
    }
    public QuerySessionBean(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStructBean referrerInfo, String status, String note, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.dataService = dataService;
        this.serviceUrl = serviceUrl;
        this.inputFormat = inputFormat;
        this.outputFormat = outputFormat;
        this.referrerInfo = referrerInfo;
        this.status = status;
        this.note = note;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public QuerySessionBean(QuerySession stub)
    {
        if(stub instanceof QuerySessionStub) {
            this.stub = (QuerySessionStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setDataService(stub.getDataService());   
            setServiceUrl(stub.getServiceUrl());   
            setInputFormat(stub.getInputFormat());   
            setOutputFormat(stub.getOutputFormat());   
            ReferrerInfoStruct referrerInfo = stub.getReferrerInfo();
            if(referrerInfo instanceof ReferrerInfoStructBean) {
                setReferrerInfo((ReferrerInfoStructBean) referrerInfo);   
            } else {
                setReferrerInfo(new ReferrerInfoStructBean(referrerInfo));   
            }
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getDataService()
    {
        if(getStub() != null) {
            return getStub().getDataService();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.dataService;
        }
    }
    public void setDataService(String dataService)
    {
        if(getStub() != null) {
            getStub().setDataService(dataService);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.dataService = dataService;
        }
    }

    public String getServiceUrl()
    {
        if(getStub() != null) {
            return getStub().getServiceUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceUrl;
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getStub() != null) {
            getStub().setServiceUrl(serviceUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceUrl = serviceUrl;
        }
    }

    public String getInputFormat()
    {
        if(getStub() != null) {
            return getStub().getInputFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.inputFormat;
        }
    }
    public void setInputFormat(String inputFormat)
    {
        if(getStub() != null) {
            getStub().setInputFormat(inputFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.inputFormat = inputFormat;
        }
    }

    public String getOutputFormat()
    {
        if(getStub() != null) {
            return getStub().getOutputFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputFormat;
        }
    }
    public void setOutputFormat(String outputFormat)
    {
        if(getStub() != null) {
            getStub().setOutputFormat(outputFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputFormat = outputFormat;
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {  
        if(getStub() != null) {
            // Note the object type.
            ReferrerInfoStruct _stub_field = getStub().getReferrerInfo();
            if(_stub_field == null) {
                return null;
            } else {
                return new ReferrerInfoStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referrerInfo;
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setReferrerInfo(referrerInfo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(referrerInfo == null) {
                this.referrerInfo = null;
            } else {
                if(referrerInfo instanceof ReferrerInfoStructBean) {
                    this.referrerInfo = (ReferrerInfoStructBean) referrerInfo;
                } else {
                    this.referrerInfo = new ReferrerInfoStructBean(referrerInfo);
                }
            }
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public QuerySessionStub getStub()
    {
        return this.stub;
    }
    protected void setStub(QuerySessionStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("dataService = " + this.dataService).append(";");
            sb.append("serviceUrl = " + this.serviceUrl).append(";");
            sb.append("inputFormat = " + this.inputFormat).append(";");
            sb.append("outputFormat = " + this.outputFormat).append(";");
            sb.append("referrerInfo = " + this.referrerInfo).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = dataService == null ? 0 : dataService.hashCode();
            _hash = 31 * _hash + delta;
            delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = inputFormat == null ? 0 : inputFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputFormat == null ? 0 : outputFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
