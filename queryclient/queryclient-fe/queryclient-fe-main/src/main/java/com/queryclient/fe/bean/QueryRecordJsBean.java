package com.queryclient.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryRecordJsBean implements Serializable, Cloneable  //, QueryRecord
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QueryRecordJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String querySession;
    private Integer queryId;
    private String dataService;
    private String serviceUrl;
    private Boolean delayed;
    private String query;
    private String inputFormat;
    private String inputFile;
    private String inputContent;
    private String targetOutputFormat;
    private String outputFormat;
    private String outputFile;
    private String outputContent;
    private Integer responseCode;
    private String result;
    private ReferrerInfoStructJsBean referrerInfo;
    private String status;
    private String extra;
    private String note;
    private Long scheduledTime;
    private Long processedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public QueryRecordJsBean()
    {
        //this((String) null);
    }
    public QueryRecordJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public QueryRecordJsBean(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStructJsBean referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime)
    {
        this(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime, null, null);
    }
    public QueryRecordJsBean(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStructJsBean referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.querySession = querySession;
        this.queryId = queryId;
        this.dataService = dataService;
        this.serviceUrl = serviceUrl;
        this.delayed = delayed;
        this.query = query;
        this.inputFormat = inputFormat;
        this.inputFile = inputFile;
        this.inputContent = inputContent;
        this.targetOutputFormat = targetOutputFormat;
        this.outputFormat = outputFormat;
        this.outputFile = outputFile;
        this.outputContent = outputContent;
        this.responseCode = responseCode;
        this.result = result;
        this.referrerInfo = referrerInfo;
        this.status = status;
        this.extra = extra;
        this.note = note;
        this.scheduledTime = scheduledTime;
        this.processedTime = processedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public QueryRecordJsBean(QueryRecordJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setQuerySession(bean.getQuerySession());
            setQueryId(bean.getQueryId());
            setDataService(bean.getDataService());
            setServiceUrl(bean.getServiceUrl());
            setDelayed(bean.isDelayed());
            setQuery(bean.getQuery());
            setInputFormat(bean.getInputFormat());
            setInputFile(bean.getInputFile());
            setInputContent(bean.getInputContent());
            setTargetOutputFormat(bean.getTargetOutputFormat());
            setOutputFormat(bean.getOutputFormat());
            setOutputFile(bean.getOutputFile());
            setOutputContent(bean.getOutputContent());
            setResponseCode(bean.getResponseCode());
            setResult(bean.getResult());
            setReferrerInfo(bean.getReferrerInfo());
            setStatus(bean.getStatus());
            setExtra(bean.getExtra());
            setNote(bean.getNote());
            setScheduledTime(bean.getScheduledTime());
            setProcessedTime(bean.getProcessedTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static QueryRecordJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        QueryRecordJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(QueryRecordJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, QueryRecordJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getQuerySession()
    {
        return this.querySession;
    }
    public void setQuerySession(String querySession)
    {
        this.querySession = querySession;
    }

    public Integer getQueryId()
    {
        return this.queryId;
    }
    public void setQueryId(Integer queryId)
    {
        this.queryId = queryId;
    }

    public String getDataService()
    {
        return this.dataService;
    }
    public void setDataService(String dataService)
    {
        this.dataService = dataService;
    }

    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    public Boolean isDelayed()
    {
        return this.delayed;
    }
    public void setDelayed(Boolean delayed)
    {
        this.delayed = delayed;
    }

    public String getQuery()
    {
        return this.query;
    }
    public void setQuery(String query)
    {
        this.query = query;
    }

    public String getInputFormat()
    {
        return this.inputFormat;
    }
    public void setInputFormat(String inputFormat)
    {
        this.inputFormat = inputFormat;
    }

    public String getInputFile()
    {
        return this.inputFile;
    }
    public void setInputFile(String inputFile)
    {
        this.inputFile = inputFile;
    }

    public String getInputContent()
    {
        return this.inputContent;
    }
    public void setInputContent(String inputContent)
    {
        this.inputContent = inputContent;
    }

    public String getTargetOutputFormat()
    {
        return this.targetOutputFormat;
    }
    public void setTargetOutputFormat(String targetOutputFormat)
    {
        this.targetOutputFormat = targetOutputFormat;
    }

    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    public String getOutputFile()
    {
        return this.outputFile;
    }
    public void setOutputFile(String outputFile)
    {
        this.outputFile = outputFile;
    }

    public String getOutputContent()
    {
        return this.outputContent;
    }
    public void setOutputContent(String outputContent)
    {
        this.outputContent = outputContent;
    }

    public Integer getResponseCode()
    {
        return this.responseCode;
    }
    public void setResponseCode(Integer responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public ReferrerInfoStructJsBean getReferrerInfo()
    {  
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStructJsBean referrerInfo)
    {
        this.referrerInfo = referrerInfo;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getExtra()
    {
        return this.extra;
    }
    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getScheduledTime()
    {
        return this.scheduledTime;
    }
    public void setScheduledTime(Long scheduledTime)
    {
        this.scheduledTime = scheduledTime;
    }

    public Long getProcessedTime()
    {
        return this.processedTime;
    }
    public void setProcessedTime(Long processedTime)
    {
        this.processedTime = processedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("querySession:null, ");
        sb.append("queryId:0, ");
        sb.append("dataService:null, ");
        sb.append("serviceUrl:null, ");
        sb.append("delayed:false, ");
        sb.append("query:null, ");
        sb.append("inputFormat:null, ");
        sb.append("inputFile:null, ");
        sb.append("inputContent:null, ");
        sb.append("targetOutputFormat:null, ");
        sb.append("outputFormat:null, ");
        sb.append("outputFile:null, ");
        sb.append("outputContent:null, ");
        sb.append("responseCode:0, ");
        sb.append("result:null, ");
        sb.append("referrerInfo:{}, ");
        sb.append("status:null, ");
        sb.append("extra:null, ");
        sb.append("note:null, ");
        sb.append("scheduledTime:0, ");
        sb.append("processedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("querySession:");
        if(this.getQuerySession() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQuerySession()).append("\", ");
        }
        sb.append("queryId:" + this.getQueryId()).append(", ");
        sb.append("dataService:");
        if(this.getDataService() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDataService()).append("\", ");
        }
        sb.append("serviceUrl:");
        if(this.getServiceUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getServiceUrl()).append("\", ");
        }
        sb.append("delayed:" + this.isDelayed()).append(", ");
        sb.append("query:");
        if(this.getQuery() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQuery()).append("\", ");
        }
        sb.append("inputFormat:");
        if(this.getInputFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getInputFormat()).append("\", ");
        }
        sb.append("inputFile:");
        if(this.getInputFile() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getInputFile()).append("\", ");
        }
        sb.append("inputContent:");
        if(this.getInputContent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getInputContent()).append("\", ");
        }
        sb.append("targetOutputFormat:");
        if(this.getTargetOutputFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTargetOutputFormat()).append("\", ");
        }
        sb.append("outputFormat:");
        if(this.getOutputFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputFormat()).append("\", ");
        }
        sb.append("outputFile:");
        if(this.getOutputFile() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputFile()).append("\", ");
        }
        sb.append("outputContent:");
        if(this.getOutputContent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputContent()).append("\", ");
        }
        sb.append("responseCode:" + this.getResponseCode()).append(", ");
        sb.append("result:");
        if(this.getResult() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getResult()).append("\", ");
        }
        sb.append("referrerInfo:");
        if(this.getReferrerInfo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferrerInfo()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("extra:");
        if(this.getExtra() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getExtra()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("scheduledTime:" + this.getScheduledTime()).append(", ");
        sb.append("processedTime:" + this.getProcessedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getQuerySession() != null) {
            sb.append("\"querySession\":").append("\"").append(this.getQuerySession()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"querySession\":").append("null, ");
        }
        if(this.getQueryId() != null) {
            sb.append("\"queryId\":").append("").append(this.getQueryId()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryId\":").append("null, ");
        }
        if(this.getDataService() != null) {
            sb.append("\"dataService\":").append("\"").append(this.getDataService()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"dataService\":").append("null, ");
        }
        if(this.getServiceUrl() != null) {
            sb.append("\"serviceUrl\":").append("\"").append(this.getServiceUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"serviceUrl\":").append("null, ");
        }
        if(this.isDelayed() != null) {
            sb.append("\"delayed\":").append("").append(this.isDelayed()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"delayed\":").append("null, ");
        }
        if(this.getQuery() != null) {
            sb.append("\"query\":").append("\"").append(this.getQuery()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"query\":").append("null, ");
        }
        if(this.getInputFormat() != null) {
            sb.append("\"inputFormat\":").append("\"").append(this.getInputFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"inputFormat\":").append("null, ");
        }
        if(this.getInputFile() != null) {
            sb.append("\"inputFile\":").append("\"").append(this.getInputFile()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"inputFile\":").append("null, ");
        }
        if(this.getInputContent() != null) {
            sb.append("\"inputContent\":").append("\"").append(this.getInputContent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"inputContent\":").append("null, ");
        }
        if(this.getTargetOutputFormat() != null) {
            sb.append("\"targetOutputFormat\":").append("\"").append(this.getTargetOutputFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"targetOutputFormat\":").append("null, ");
        }
        if(this.getOutputFormat() != null) {
            sb.append("\"outputFormat\":").append("\"").append(this.getOutputFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputFormat\":").append("null, ");
        }
        if(this.getOutputFile() != null) {
            sb.append("\"outputFile\":").append("\"").append(this.getOutputFile()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputFile\":").append("null, ");
        }
        if(this.getOutputContent() != null) {
            sb.append("\"outputContent\":").append("\"").append(this.getOutputContent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputContent\":").append("null, ");
        }
        if(this.getResponseCode() != null) {
            sb.append("\"responseCode\":").append("").append(this.getResponseCode()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"responseCode\":").append("null, ");
        }
        if(this.getResult() != null) {
            sb.append("\"result\":").append("\"").append(this.getResult()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"result\":").append("null, ");
        }
        sb.append("\"referrerInfo\":").append(this.referrerInfo.toJsonString()).append(", ");
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getExtra() != null) {
            sb.append("\"extra\":").append("\"").append(this.getExtra()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"extra\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getScheduledTime() != null) {
            sb.append("\"scheduledTime\":").append("").append(this.getScheduledTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"scheduledTime\":").append("null, ");
        }
        if(this.getProcessedTime() != null) {
            sb.append("\"processedTime\":").append("").append(this.getProcessedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"processedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("querySession = " + this.querySession).append(";");
        sb.append("queryId = " + this.queryId).append(";");
        sb.append("dataService = " + this.dataService).append(";");
        sb.append("serviceUrl = " + this.serviceUrl).append(";");
        sb.append("delayed = " + this.delayed).append(";");
        sb.append("query = " + this.query).append(";");
        sb.append("inputFormat = " + this.inputFormat).append(";");
        sb.append("inputFile = " + this.inputFile).append(";");
        sb.append("inputContent = " + this.inputContent).append(";");
        sb.append("targetOutputFormat = " + this.targetOutputFormat).append(";");
        sb.append("outputFormat = " + this.outputFormat).append(";");
        sb.append("outputFile = " + this.outputFile).append(";");
        sb.append("outputContent = " + this.outputContent).append(";");
        sb.append("responseCode = " + this.responseCode).append(";");
        sb.append("result = " + this.result).append(";");
        sb.append("referrerInfo = " + this.referrerInfo).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("extra = " + this.extra).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("scheduledTime = " + this.scheduledTime).append(";");
        sb.append("processedTime = " + this.processedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        QueryRecordJsBean cloned = new QueryRecordJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setQuerySession(this.getQuerySession());   
        cloned.setQueryId(this.getQueryId());   
        cloned.setDataService(this.getDataService());   
        cloned.setServiceUrl(this.getServiceUrl());   
        cloned.setDelayed(this.isDelayed());   
        cloned.setQuery(this.getQuery());   
        cloned.setInputFormat(this.getInputFormat());   
        cloned.setInputFile(this.getInputFile());   
        cloned.setInputContent(this.getInputContent());   
        cloned.setTargetOutputFormat(this.getTargetOutputFormat());   
        cloned.setOutputFormat(this.getOutputFormat());   
        cloned.setOutputFile(this.getOutputFile());   
        cloned.setOutputContent(this.getOutputContent());   
        cloned.setResponseCode(this.getResponseCode());   
        cloned.setResult(this.getResult());   
        cloned.setReferrerInfo( (ReferrerInfoStructJsBean) this.getReferrerInfo().clone() );
        cloned.setStatus(this.getStatus());   
        cloned.setExtra(this.getExtra());   
        cloned.setNote(this.getNote());   
        cloned.setScheduledTime(this.getScheduledTime());   
        cloned.setProcessedTime(this.getProcessedTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
