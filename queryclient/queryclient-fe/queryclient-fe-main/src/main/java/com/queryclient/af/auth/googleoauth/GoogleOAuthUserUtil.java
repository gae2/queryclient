package com.queryclient.af.auth.googleoauth;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.auth.common.OAuthConstants;


public final class GoogleOAuthUserUtil
{
    private static final Logger log = Logger.getLogger(GoogleOAuthUserUtil.class.getName());

    // Constants

    // For request param...
    public static final String PARAM_ACCESS_TOKEN = "access_token";
    public static final String PARAM_ALT = "alt";    // format ???

    // ...
    // For response fields...
    public static final String PARAM_ID = "sub";     // "id"   ?????
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_EMAIL_VERIFIED = "email_verified";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_GIVEN_NAME = "given_name";
    public static final String PARAM_FAMILY_NAME = "family_name";
    public static final String PARAM_PROFILE = "profile";
    public static final String PARAM_PICTURE = "picture";
    public static final String PARAM_GENDER = "gender";
    public static final String PARAM_BIRTHDATE = "birthdate";    // "yyyy-mm-dd"
    public static final String PARAM_LOCALE = "locale";
    public static final String PARAM_ERROR = "error";
    // ...

    // UserInfo webservice endpoint...
    public static final String GOOGLEOAUTH_USERINFO_ENDPOINT_URL = "https://www.googleapis.com/oauth2/v3/userinfo";

    // Request attr to store the GoogleOAuthUserInfo object.
    public static final String REQUEST_ATTR_USERINFO = "com.queryclient.af.auth.googleoauth.userinfo";

    
    private GoogleOAuthUserUtil() {}


    // TBD:
    // ...
    
}
