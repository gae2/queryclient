package com.queryclient.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.QueryRecordJsBean;
import com.queryclient.wa.service.QueryRecordWebService;
import com.queryclient.wa.service.UserWebService;


public class QueryRecordHelper
{
    private static final Logger log = Logger.getLogger(QueryRecordHelper.class.getName());

    private UserWebService userWebService = null;
    private QueryRecordWebService mQueryRecordWebService = null;

    private QueryRecordHelper() {}

    // Initialization-on-demand holder.
    private static final class MessageHelperHolder
    {
        private static final QueryRecordHelper INSTANCE = new QueryRecordHelper();
    }

    // Singleton method
    public static QueryRecordHelper getInstance()
    {
        return MessageHelperHolder.INSTANCE;
    }


    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private QueryRecordWebService getQueryRecordWebService()
    {
        if(mQueryRecordWebService == null) {
            mQueryRecordWebService = new QueryRecordWebService();
        }
        return mQueryRecordWebService;
    }
    
    
    public QueryRecordJsBean getQueryRecord(String pageGuid)
    {
        QueryRecordJsBean bean = null;
        try {
            bean = getQueryRecordWebService().getQueryRecord(pageGuid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to get the QueryRecord for the gven guid = " + pageGuid, e);
        }
        return bean;
    }
    
    
//    // Returns the template jsp page relative URL....
//    public String getFilePath(QueryRecordJsBean pageBean)
//    {
//        if(pageBean == null) {
//            log.info("Failed to construct template file path because the pageBean is null.");
//            return null;  // ???
//        }
//
//        String filePath = null;
//        String templateFile = pageBean.getPageTemplateFilePath();   // Can still include subdir...
//        if(templateFile == null) {
//            log.info("pageBean does not have the templatefile attribute.");
//            String templateCode = pageBean.getPageTemplateCode();
//            if(templateCode == null) {
//                log.warning("Failed to construct template file path because neither templateFile nor templateCode is set for the given pageBean.");
//                return null;
//            } else {
//                filePath = TemplateHelper.getInstance().constructFilePathFromPageTemplateCode(templateCode);
//            }
//        } else {
//            filePath = TemplateUtil.constructFilePathFromPageTemplateFile(templateFile);
//        }
//
//        return filePath;
//    }
   

    // temporary. Is there a better way?????
    public int getTotalCountForUser(String userGuid)
    {
        return getTotalCountForUser(userGuid, null, null);
    }
    public int getTotalCountForUser(String userGuid, Long offset, Integer maxCount)
    {
        int totalCount = 0;
        try {
            String filter = "user=='" + userGuid + "'";   // Status?
            String ordering = "createdTime desc";
            List<String> keys = getQueryRecordWebService().findQueryRecordKeys(filter, ordering, null, null, null, null, offset, maxCount);
            if(keys != null) {
                totalCount = keys.size();
            }
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to find beans for given user = " + userGuid, e);
        }
        return totalCount;
    }
    
    
    public List<QueryRecordJsBean> findQueryRecordsForUser(String userGuid)
    {
        return findQueryRecordsForUser(userGuid, null, null);
    }

    // TBD: sorting/ordering????
    public List<QueryRecordJsBean> findQueryRecordsForUser(String userGuid, Long offset, Integer count)
    {
        List<QueryRecordJsBean> beans = null;
        
        try {
            String filter = "user=='" + userGuid + "'";   // Status?
            String ordering = "createdTime desc";
            beans = getQueryRecordWebService().findQueryRecords(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to find beans for given user = " + userGuid, e);
        }

        return beans;
    }

}
