package com.queryclient.af.proxy.local;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.service.ApiConsumerService;
import com.queryclient.ws.service.UserService;
import com.queryclient.ws.service.UserPasswordService;
import com.queryclient.ws.service.ExternalUserAuthService;
import com.queryclient.ws.service.UserAuthStateService;
import com.queryclient.ws.service.DataServiceService;
import com.queryclient.ws.service.ServiceEndpointService;
import com.queryclient.ws.service.QuerySessionService;
import com.queryclient.ws.service.QueryRecordService;
import com.queryclient.ws.service.DummyEntityService;
import com.queryclient.ws.service.ServiceInfoService;
import com.queryclient.ws.service.FiveTenService;


// TBD: How to best inject the service instances?
// (The current implementation does not seem to make sense...)
public abstract class BaseLocalServiceProxy
{
    private static final Logger log = Logger.getLogger(BaseLocalServiceProxy.class.getName());

    private ApiConsumerService apiConsumerService;
    private UserService userService;
    private UserPasswordService userPasswordService;
    private ExternalUserAuthService externalUserAuthService;
    private UserAuthStateService userAuthStateService;
    private DataServiceService dataServiceService;
    private ServiceEndpointService serviceEndpointService;
    private QuerySessionService querySessionService;
    private QueryRecordService queryRecordService;
    private DummyEntityService dummyEntityService;
    private ServiceInfoService serviceInfoService;
    private FiveTenService fiveTenService;

    public BaseLocalServiceProxy()
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService)
    {
        this(apiConsumerService, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserService userService)
    {
        this(null, userService, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserPasswordService userPasswordService)
    {
        this(null, null, userPasswordService, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ExternalUserAuthService externalUserAuthService)
    {
        this(null, null, null, externalUserAuthService, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserAuthStateService userAuthStateService)
    {
        this(null, null, null, null, userAuthStateService, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(DataServiceService dataServiceService)
    {
        this(null, null, null, null, null, dataServiceService, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ServiceEndpointService serviceEndpointService)
    {
        this(null, null, null, null, null, null, serviceEndpointService, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(QuerySessionService querySessionService)
    {
        this(null, null, null, null, null, null, null, querySessionService, null, null, null, null);
    }
    public BaseLocalServiceProxy(QueryRecordService queryRecordService)
    {
        this(null, null, null, null, null, null, null, null, queryRecordService, null, null, null);
    }
    public BaseLocalServiceProxy(DummyEntityService dummyEntityService)
    {
        this(null, null, null, null, null, null, null, null, null, dummyEntityService, null, null);
    }
    public BaseLocalServiceProxy(ServiceInfoService serviceInfoService)
    {
        this(null, null, null, null, null, null, null, null, null, null, serviceInfoService, null);
    }
    public BaseLocalServiceProxy(FiveTenService fiveTenService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, fiveTenService);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService, UserService userService, UserPasswordService userPasswordService, ExternalUserAuthService externalUserAuthService, UserAuthStateService userAuthStateService, DataServiceService dataServiceService, ServiceEndpointService serviceEndpointService, QuerySessionService querySessionService, QueryRecordService queryRecordService, DummyEntityService dummyEntityService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.userService = userService;
        this.userPasswordService = userPasswordService;
        this.externalUserAuthService = externalUserAuthService;
        this.userAuthStateService = userAuthStateService;
        this.dataServiceService = dataServiceService;
        this.serviceEndpointService = serviceEndpointService;
        this.querySessionService = querySessionService;
        this.queryRecordService = queryRecordService;
        this.dummyEntityService = dummyEntityService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }
    
    // Inject dependencies.
    public void setApiConsumerService(ApiConsumerService apiConsumerService)
    {
        this.apiConsumerService = apiConsumerService;
    }
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }
    public void setUserPasswordService(UserPasswordService userPasswordService)
    {
        this.userPasswordService = userPasswordService;
    }
    public void setExternalUserAuthService(ExternalUserAuthService externalUserAuthService)
    {
        this.externalUserAuthService = externalUserAuthService;
    }
    public void setUserAuthStateService(UserAuthStateService userAuthStateService)
    {
        this.userAuthStateService = userAuthStateService;
    }
    public void setDataServiceService(DataServiceService dataServiceService)
    {
        this.dataServiceService = dataServiceService;
    }
    public void setServiceEndpointService(ServiceEndpointService serviceEndpointService)
    {
        this.serviceEndpointService = serviceEndpointService;
    }
    public void setQuerySessionService(QuerySessionService querySessionService)
    {
        this.querySessionService = querySessionService;
    }
    public void setQueryRecordService(QueryRecordService queryRecordService)
    {
        this.queryRecordService = queryRecordService;
    }
    public void setDummyEntityService(DummyEntityService dummyEntityService)
    {
        this.dummyEntityService = dummyEntityService;
    }
    public void setServiceInfoService(ServiceInfoService serviceInfoService)
    {
        this.serviceInfoService = serviceInfoService;
    }
    public void setFiveTenService(FiveTenService fiveTenService)
    {
        this.fiveTenService = fiveTenService;
    }
   
    // Returns a ApiConsumerService instance.
    public ApiConsumerService getApiConsumerService() 
    {
        return apiConsumerService;
    }

    // Returns a UserService instance.
    public UserService getUserService() 
    {
        return userService;
    }

    // Returns a UserPasswordService instance.
    public UserPasswordService getUserPasswordService() 
    {
        return userPasswordService;
    }

    // Returns a ExternalUserAuthService instance.
    public ExternalUserAuthService getExternalUserAuthService() 
    {
        return externalUserAuthService;
    }

    // Returns a UserAuthStateService instance.
    public UserAuthStateService getUserAuthStateService() 
    {
        return userAuthStateService;
    }

    // Returns a DataServiceService instance.
    public DataServiceService getDataServiceService() 
    {
        return dataServiceService;
    }

    // Returns a ServiceEndpointService instance.
    public ServiceEndpointService getServiceEndpointService() 
    {
        return serviceEndpointService;
    }

    // Returns a QuerySessionService instance.
    public QuerySessionService getQuerySessionService() 
    {
        return querySessionService;
    }

    // Returns a QueryRecordService instance.
    public QueryRecordService getQueryRecordService() 
    {
        return queryRecordService;
    }

    // Returns a DummyEntityService instance.
    public DummyEntityService getDummyEntityService() 
    {
        return dummyEntityService;
    }

    // Returns a ServiceInfoService instance.
    public ServiceInfoService getServiceInfoService() 
    {
        return serviceInfoService;
    }

    // Returns a FiveTenService instance.
    public FiveTenService getFiveTenService() 
    {
        return fiveTenService;
    }

}
