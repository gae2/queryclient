package com.queryclient.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.queryclient.af.core.JerseyClient;
import com.queryclient.helper.UrlHelper;
import com.queryclient.wa.service.QueryRecordWebService;
import com.queryclient.wa.service.UserWebService;


// TBD: This is not being used....
// Just use "wa" servlets....

// Mainly, for ajax calls....
public class QueryServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QueryServlet.class.getName());

    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private QueryRecordWebService memoMetaWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private QueryRecordWebService getQueryRecordService()
    {
        if(memoMetaWebService == null) {
            memoMetaWebService = new QueryRecordWebService();
        }
        return memoMetaWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singletong instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    
    // TBD:
    // depending on memo.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPost() called.");


    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPut() called.");

        // TBD: Check Accept header. Etc...

        String pathInfo = req.getPathInfo();
        String guid = UrlHelper.getInstance().getGuidFromPathInfo(pathInfo);        


    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    
}
