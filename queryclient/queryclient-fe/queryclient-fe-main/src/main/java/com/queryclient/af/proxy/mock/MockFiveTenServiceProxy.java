package com.queryclient.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.FiveTen;
import com.queryclient.af.bean.FiveTenBean;
import com.queryclient.ws.service.FiveTenService;
import com.queryclient.af.proxy.FiveTenServiceProxy;


// MockFiveTenServiceProxy is a decorator.
// It can be used as a base class to mock FiveTenServiceProxy objects.
public abstract class MockFiveTenServiceProxy implements FiveTenServiceProxy
{
    private static final Logger log = Logger.getLogger(MockFiveTenServiceProxy.class.getName());

    // MockFiveTenServiceProxy uses the decorator design pattern.
    private FiveTenServiceProxy decoratedProxy;

    public MockFiveTenServiceProxy(FiveTenServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected FiveTenServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(FiveTenServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        return decoratedProxy.getFiveTen(guid);
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        return decoratedProxy.getFiveTen(guid, field);       
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        return decoratedProxy.getFiveTens(guids);
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return getAllFiveTens(null, null, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllFiveTens(ordering, offset, count);
        return getAllFiveTens(ordering, offset, count, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllFiveTens(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllFiveTenKeys(ordering, offset, count);
        return getAllFiveTenKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllFiveTenKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findFiveTens(filter, ordering, params, values, grouping, unique, offset, count);
        return findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException
    {
        return decoratedProxy.createFiveTen(counter, requesterIpAddress);
    }

    @Override
    public String createFiveTen(FiveTen fiveTen) throws BaseException
    {
        return decoratedProxy.createFiveTen(fiveTen);
    }

    @Override
    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException
    {
        return decoratedProxy.updateFiveTen(guid, counter, requesterIpAddress);
    }

    @Override
    public Boolean updateFiveTen(FiveTen fiveTen) throws BaseException
    {
        return decoratedProxy.updateFiveTen(fiveTen);
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        return decoratedProxy.deleteFiveTen(guid);
    }

    @Override
    public Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException
    {
        String guid = fiveTen.getGuid();
        return deleteFiveTen(guid);
    }

    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteFiveTens(filter, params, values);
    }

}
