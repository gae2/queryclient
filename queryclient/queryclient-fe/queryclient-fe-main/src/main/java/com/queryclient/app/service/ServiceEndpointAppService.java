package com.queryclient.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.service.impl.DataServiceServiceImpl;
import com.queryclient.af.service.impl.ServiceEndpointServiceImpl;
import com.queryclient.app.endpoint.TargetServiceManager;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.DataService;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.exception.BadRequestException;


// Updated.
public class ServiceEndpointAppService extends ServiceEndpointServiceImpl implements ServiceEndpointService
{
    private static final Logger log = Logger.getLogger(ServiceEndpointAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ServiceEndpointAppService()
    {
         super();
    }


    private DataServiceService dataServiceService = null;
    private DataServiceService getDataServiceService()
    {
        if(dataServiceService == null) {
            dataServiceService = new DataServiceServiceImpl();
        }
        return dataServiceService;
    }
    
    
    //////////////////////////////////////////////////////////////////////////
    // ServiceEndpoint related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ServiceEndpoint getServiceEndpoint(String guid) throws BaseException
    {
        return super.getServiceEndpoint(guid);
    }

    @Override
    public Object getServiceEndpoint(String guid, String field) throws BaseException
    {
        return super.getServiceEndpoint(guid, field);
    }

    @Override
    public List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException
    {
        return super.getServiceEndpoints(guids);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException
    {
        return super.getAllServiceEndpoints();
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllServiceEndpointKeys(ordering, offset, count);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        // TBD:
        serviceEndpoint = validateServiceEndpoint(serviceEndpoint);
        serviceEndpoint = processServiceEndpoint(serviceEndpoint);
        // ...

        String guid = super.createServiceEndpoint(serviceEndpoint);
        return guid;
    }

    @Override
    public ServiceEndpoint constructServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        // TBD:
        serviceEndpoint = validateServiceEndpoint(serviceEndpoint);
        serviceEndpoint = processServiceEndpoint(serviceEndpoint);
        // ...

        serviceEndpoint = super.constructServiceEndpoint(serviceEndpoint);
        return serviceEndpoint;
    }


    @Override
    public Boolean updateServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        // ?????
        // serviceEndpoint = validateServiceEndpoint(serviceEndpoint);
        // serviceEndpoint = processServiceEndpoint(serviceEndpoint);
        // ...

        return super.updateServiceEndpoint(serviceEndpoint);
    }
        
    @Override
    public ServiceEndpoint refreshServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        // ?????
        // serviceEndpoint = validateServiceEndpoint(serviceEndpoint);
        // serviceEndpoint = processServiceEndpoint(serviceEndpoint);
        // ...

        return super.refreshServiceEndpoint(serviceEndpoint);
    }

    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        return super.deleteServiceEndpoint(guid);
    }

    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        return super.deleteServiceEndpoint(serviceEndpoint);
    }

    @Override
    public Integer createServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException
    {
        return super.createServiceEndpoints(serviceEndpoints);
    }

    // TBD
    //@Override
    //public Boolean updateServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException
    //{
    //}

    private ServiceEndpointBean validateServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.fine("ENTERING: validateServiceEndpoint()");

        ServiceEndpointBean bean = null;
        if(serviceEndpoint instanceof ServiceEndpointBean) {
            bean = (ServiceEndpointBean) serviceEndpoint;
        } else {
            // ????? Can this happen????
            bean = new ServiceEndpointBean(serviceEndpoint);
        }
        
        // TBD: serviceUrl (by itself) should really be a required field.... ????
        String serviceUrl = bean.getServiceUrl();
        String serviceName = bean.getServiceName();
        if((serviceUrl == null || serviceUrl.isEmpty()) && (serviceName == null || serviceName.isEmpty())) {
            // ???
            throw new BadRequestException("Both serviceName and serviceUrl cannot be null/empty.");
        }

        // etc...
        
        return bean;
    }

 
    private ServiceEndpointBean processServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.fine("ENTERING: processServiceEndpoint()");

        ServiceEndpointBean bean = null;
        if(serviceEndpoint instanceof ServiceEndpointBean) {
            bean = (ServiceEndpointBean) serviceEndpoint;
        } else {
            // ????? Can this happen????
            bean = new ServiceEndpointBean(serviceEndpoint);
        }
        
        // TBD: "inheritable" attributes...
        String serviceUrl = bean.getServiceUrl();
        ConsumerKeySecretPair authCredential = bean.getAuthCredential();  // What else???
        // if((serviceUrl==null || serviceUrl.isEmpty()) || authCredential == null) {
            String serviceGuid = bean.getDataService();
            String serviceName = bean.getServiceName();
            if((serviceGuid != null && !serviceGuid.isEmpty()) || (serviceName != null && !serviceName.isEmpty())) {
                // TBD:
                // Save/fetch Data Service
                DataService dataService = null;
                try {
                    if(serviceGuid != null && !serviceGuid.isEmpty()) {
                        dataService = getDataServiceService().getDataService(serviceGuid);
                    } else {
                        // TBD: Check TargetServiceManager first ???
                        // .. (See below)                        
                        String filter = "name=='" + serviceName + "'";
                        String ordering = "createdTime desc";
                        List<String> list = getDataServiceService().findDataServiceKeys(filter, ordering, null, null, null, null, 0L, 2);
                        if(list != null && !list.isEmpty()) {
                            if(list.size() > 1) {
                                // This should not happen...
                                if(log.isLoggable(Level.WARNING)) log.warning("More than one DataService found for serviceName = " + serviceName);
                            }
                            // Full fetch...
                            String key = list.get(0);
                            dataService = getDataServiceService().getDataService(key);
                        }
                    } 
                    // TBD:
                    if(dataService == null) {
                        // TBD: Create a new dataService????
                        // Do this only if authCredential is set.... ???
                        if(authCredential != null && (authCredential.getConsumerKey() != null && !authCredential.getConsumerKey().isEmpty())) {
                            DataServiceBean newDataService = new DataServiceBean();
                            if(serviceGuid == null || serviceGuid.isEmpty()) {
                                serviceGuid = GUID.generate();
                            }
                            newDataService.setGuid(serviceGuid);
                            if(serviceName != null && !serviceName.isEmpty()) {   // TBD: Create a random/unique name when serviceName is not set ???
                                newDataService.setName(serviceName);
                            }
                            // copy serviceEndpoint.serviceUrl (and serviceEndpoint.authCredential if set), etc... ????
                            if(authCredential != null) {
                                newDataService.setAuthCredential(authCredential);  // ???
                            }
                            if(serviceUrl != null && !serviceUrl.isEmpty()) {
                                newDataService.setServiceUrl(serviceUrl);
                            }
                            // etc...
                            dataService = getDataServiceService().constructDataService(newDataService);
                        }
                    }
                } catch(Exception e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Error while fetching/saving: serviceGuid = " + serviceGuid + "; serviceName = " + serviceName, e);
                }
                if(dataService != null) {
                    serviceGuid = dataService.getGuid();
                    bean.setDataService(serviceGuid);
                    serviceName = dataService.getName();
                    bean.setServiceName(serviceName);
                    if(authCredential == null || (authCredential.getConsumerKey() == null || authCredential.getConsumerKey().isEmpty()) ) {   // ????
                        authCredential = dataService.getAuthCredential();
                        if(authCredential != null) {
                            bean.setAuthCredential(authCredential);
                        }
                    }
                    if(serviceUrl == null || serviceUrl.isEmpty()) {   // URL validation???
                        serviceUrl = dataService.getServiceUrl();
                        if(serviceUrl != null && !serviceUrl.isEmpty()) {
                            bean.setServiceUrl(serviceUrl);
                        }
                    }
                    // etc...
                } else {
                    // This is primarily used to "copy" the authCredential from the TargetServiceManager list....
                    if(serviceName != null && !serviceName.isEmpty()) {
                        if(authCredential == null || (authCredential.getConsumerKey() == null || authCredential.getConsumerKey().isEmpty())) {
                            authCredential = TargetServiceManager.getInstance().getKeySecretPair(serviceName, false);
                            if(authCredential != null) {
                                bean.setAuthCredential(authCredential);
                            }                            
                        }
                        if(serviceUrl == null || serviceUrl.isEmpty()) {   // ???
                            serviceUrl = TargetServiceManager.getInstance().getPrimaryServiceUrl(serviceName, false);
                            if(serviceUrl != null) {
                                bean.setServiceUrl(serviceUrl);
                            }
                        }
                    }
                }
            }
        // }
        
        // etc..

        return bean;
    }

}
