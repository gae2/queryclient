package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.stub.GaeUserStructStub;


// Wrapper class + bean combo.
public class GaeUserStructBean implements GaeUserStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GaeUserStructBean.class.getName());

    // [1] With an embedded object.
    private GaeUserStructStub stub = null;

    // [2] Or, without an embedded object.
    private String authDomain;
    private String federatedIdentity;
    private String nickname;
    private String userId;
    private String email;
    private String note;

    // Ctors.
    public GaeUserStructBean()
    {
        //this((String) null);
    }
    public GaeUserStructBean(String authDomain, String federatedIdentity, String nickname, String userId, String email, String note)
    {
        this.authDomain = authDomain;
        this.federatedIdentity = federatedIdentity;
        this.nickname = nickname;
        this.userId = userId;
        this.email = email;
        this.note = note;
    }
    public GaeUserStructBean(GaeUserStruct stub)
    {
        if(stub instanceof GaeUserStructStub) {
            this.stub = (GaeUserStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setAuthDomain(stub.getAuthDomain());   
            setFederatedIdentity(stub.getFederatedIdentity());   
            setNickname(stub.getNickname());   
            setUserId(stub.getUserId());   
            setEmail(stub.getEmail());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getAuthDomain()
    {
        if(getStub() != null) {
            return getStub().getAuthDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.authDomain;
        }
    }
    public void setAuthDomain(String authDomain)
    {
        if(getStub() != null) {
            getStub().setAuthDomain(authDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.authDomain = authDomain;
        }
    }

    public String getFederatedIdentity()
    {
        if(getStub() != null) {
            return getStub().getFederatedIdentity();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.federatedIdentity;
        }
    }
    public void setFederatedIdentity(String federatedIdentity)
    {
        if(getStub() != null) {
            getStub().setFederatedIdentity(federatedIdentity);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.federatedIdentity = federatedIdentity;
        }
    }

    public String getNickname()
    {
        if(getStub() != null) {
            return getStub().getNickname();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.nickname;
        }
    }
    public void setNickname(String nickname)
    {
        if(getStub() != null) {
            getStub().setNickname(nickname);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.nickname = nickname;
        }
    }

    public String getUserId()
    {
        if(getStub() != null) {
            return getStub().getUserId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userId;
        }
    }
    public void setUserId(String userId)
    {
        if(getStub() != null) {
            getStub().setUserId(userId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userId = userId;
        }
    }

    public String getEmail()
    {
        if(getStub() != null) {
            return getStub().getEmail();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.email;
        }
    }
    public void setEmail(String email)
    {
        if(getStub() != null) {
            getStub().setEmail(email);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.email = email;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getAuthDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFederatedIdentity() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNickname() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUserId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmail() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public GaeUserStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(GaeUserStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("authDomain = " + this.authDomain).append(";");
            sb.append("federatedIdentity = " + this.federatedIdentity).append(";");
            sb.append("nickname = " + this.nickname).append(";");
            sb.append("userId = " + this.userId).append(";");
            sb.append("email = " + this.email).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = authDomain == null ? 0 : authDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = federatedIdentity == null ? 0 : federatedIdentity.hashCode();
            _hash = 31 * _hash + delta;
            delta = nickname == null ? 0 : nickname.hashCode();
            _hash = 31 * _hash + delta;
            delta = userId == null ? 0 : userId.hashCode();
            _hash = 31 * _hash + delta;
            delta = email == null ? 0 : email.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
