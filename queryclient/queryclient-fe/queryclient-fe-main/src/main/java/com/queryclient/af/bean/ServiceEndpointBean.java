package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.stub.ConsumerKeySecretPairStub;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.stub.ServiceEndpointStub;


// Wrapper class + bean combo.
public class ServiceEndpointBean implements ServiceEndpoint, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ServiceEndpointBean.class.getName());

    // [1] With an embedded object.
    private ServiceEndpointStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String dataService;
    private String serviceName;
    private String serviceUrl;
    private Boolean authRequired;
    private ConsumerKeySecretPairBean authCredential;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ServiceEndpointBean()
    {
        //this((String) null);
    }
    public ServiceEndpointBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public ServiceEndpointBean(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairBean authCredential, String status)
    {
        this(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredential, status, null, null);
    }
    public ServiceEndpointBean(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairBean authCredential, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.dataService = dataService;
        this.serviceName = serviceName;
        this.serviceUrl = serviceUrl;
        this.authRequired = authRequired;
        this.authCredential = authCredential;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ServiceEndpointBean(ServiceEndpoint stub)
    {
        if(stub instanceof ServiceEndpointStub) {
            this.stub = (ServiceEndpointStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setDataService(stub.getDataService());   
            setServiceName(stub.getServiceName());   
            setServiceUrl(stub.getServiceUrl());   
            setAuthRequired(stub.isAuthRequired());   
            ConsumerKeySecretPair authCredential = stub.getAuthCredential();
            if(authCredential instanceof ConsumerKeySecretPairBean) {
                setAuthCredential((ConsumerKeySecretPairBean) authCredential);   
            } else {
                setAuthCredential(new ConsumerKeySecretPairBean(authCredential));   
            }
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getDataService()
    {
        if(getStub() != null) {
            return getStub().getDataService();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.dataService;
        }
    }
    public void setDataService(String dataService)
    {
        if(getStub() != null) {
            getStub().setDataService(dataService);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.dataService = dataService;
        }
    }

    public String getServiceName()
    {
        if(getStub() != null) {
            return getStub().getServiceName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceName;
        }
    }
    public void setServiceName(String serviceName)
    {
        if(getStub() != null) {
            getStub().setServiceName(serviceName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceName = serviceName;
        }
    }

    public String getServiceUrl()
    {
        if(getStub() != null) {
            return getStub().getServiceUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceUrl;
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getStub() != null) {
            getStub().setServiceUrl(serviceUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceUrl = serviceUrl;
        }
    }

    public Boolean isAuthRequired()
    {
        if(getStub() != null) {
            return getStub().isAuthRequired();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.authRequired;
        }
    }
    public void setAuthRequired(Boolean authRequired)
    {
        if(getStub() != null) {
            getStub().setAuthRequired(authRequired);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.authRequired = authRequired;
        }
    }

    public ConsumerKeySecretPair getAuthCredential()
    {  
        if(getStub() != null) {
            // Note the object type.
            ConsumerKeySecretPair _stub_field = getStub().getAuthCredential();
            if(_stub_field == null) {
                return null;
            } else {
                return new ConsumerKeySecretPairBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.authCredential;
        }
    }
    public void setAuthCredential(ConsumerKeySecretPair authCredential)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setAuthCredential(authCredential);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(authCredential == null) {
                this.authCredential = null;
            } else {
                if(authCredential instanceof ConsumerKeySecretPairBean) {
                    this.authCredential = (ConsumerKeySecretPairBean) authCredential;
                } else {
                    this.authCredential = new ConsumerKeySecretPairBean(authCredential);
                }
            }
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public ServiceEndpointStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ServiceEndpointStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("dataService = " + this.dataService).append(";");
            sb.append("serviceName = " + this.serviceName).append(";");
            sb.append("serviceUrl = " + this.serviceUrl).append(";");
            sb.append("authRequired = " + this.authRequired).append(";");
            sb.append("authCredential = " + this.authCredential).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = dataService == null ? 0 : dataService.hashCode();
            _hash = 31 * _hash + delta;
            delta = serviceName == null ? 0 : serviceName.hashCode();
            _hash = 31 * _hash + delta;
            delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = authRequired == null ? 0 : authRequired.hashCode();
            _hash = 31 * _hash + delta;
            delta = authCredential == null ? 0 : authCredential.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
