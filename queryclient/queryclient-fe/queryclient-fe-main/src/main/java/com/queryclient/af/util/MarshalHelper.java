package com.queryclient.af.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.KeyValuePairStruct;
import com.queryclient.ws.KeyValueRelationStruct;
import com.queryclient.ws.ExternalServiceApiKeyStruct;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.PagerStateStruct;
import com.queryclient.ws.ApiConsumer;
import com.queryclient.ws.User;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.UserAuthState;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.DataService;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.ServiceInfo;
import com.queryclient.ws.FiveTen;
import com.queryclient.ws.stub.KeyValuePairStructStub;
import com.queryclient.ws.stub.KeyValuePairStructListStub;
import com.queryclient.ws.stub.KeyValueRelationStructStub;
import com.queryclient.ws.stub.KeyValueRelationStructListStub;
import com.queryclient.ws.stub.ExternalServiceApiKeyStructStub;
import com.queryclient.ws.stub.ExternalServiceApiKeyStructListStub;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.ws.stub.ReferrerInfoStructListStub;
import com.queryclient.ws.stub.GaeAppStructStub;
import com.queryclient.ws.stub.GaeAppStructListStub;
import com.queryclient.ws.stub.GaeUserStructStub;
import com.queryclient.ws.stub.GaeUserStructListStub;
import com.queryclient.ws.stub.PagerStateStructStub;
import com.queryclient.ws.stub.PagerStateStructListStub;
import com.queryclient.ws.stub.ApiConsumerStub;
import com.queryclient.ws.stub.ApiConsumerListStub;
import com.queryclient.ws.stub.UserStub;
import com.queryclient.ws.stub.UserListStub;
import com.queryclient.ws.stub.UserPasswordStub;
import com.queryclient.ws.stub.UserPasswordListStub;
import com.queryclient.ws.stub.ExternalUserIdStructStub;
import com.queryclient.ws.stub.ExternalUserIdStructListStub;
import com.queryclient.ws.stub.ExternalUserAuthStub;
import com.queryclient.ws.stub.ExternalUserAuthListStub;
import com.queryclient.ws.stub.UserAuthStateStub;
import com.queryclient.ws.stub.UserAuthStateListStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairListStub;
import com.queryclient.ws.stub.DataServiceStub;
import com.queryclient.ws.stub.DataServiceListStub;
import com.queryclient.ws.stub.ServiceEndpointStub;
import com.queryclient.ws.stub.ServiceEndpointListStub;
import com.queryclient.ws.stub.QuerySessionStub;
import com.queryclient.ws.stub.QuerySessionListStub;
import com.queryclient.ws.stub.QueryRecordStub;
import com.queryclient.ws.stub.QueryRecordListStub;
import com.queryclient.ws.stub.DummyEntityStub;
import com.queryclient.ws.stub.DummyEntityListStub;
import com.queryclient.ws.stub.ServiceInfoStub;
import com.queryclient.ws.stub.ServiceInfoListStub;
import com.queryclient.ws.stub.FiveTenStub;
import com.queryclient.ws.stub.FiveTenListStub;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.GaeUserStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.ApiConsumerBean;
import com.queryclient.af.bean.UserBean;
import com.queryclient.af.bean.UserPasswordBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalUserAuthBean;
import com.queryclient.af.bean.UserAuthStateBean;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.bean.ServiceInfoBean;
import com.queryclient.af.bean.FiveTenBean;


public final class MarshalHelper
{
    private static final Logger log = Logger.getLogger(MarshalHelper.class.getName());

    // Static methods only.
    private MarshalHelper() {}

    // temporary
    public static KeyValuePairStructBean convertKeyValuePairStructToBean(KeyValuePairStruct stub)
    {
        KeyValuePairStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeyValuePairStructBean();
        } else if(stub instanceof KeyValuePairStructBean) {
        	bean = (KeyValuePairStructBean) stub;
        } else {
        	bean = new KeyValuePairStructBean(stub);
        }
        return bean;
    }
    public static List<KeyValuePairStructBean> convertKeyValuePairStructListToBeanList(List<KeyValuePairStruct> stubList)
    {
        List<KeyValuePairStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeyValuePairStruct>();
        } else {
            beanList = new ArrayList<KeyValuePairStructBean>();
            for(KeyValuePairStruct stub : stubList) {
                beanList.add(convertKeyValuePairStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeyValuePairStruct> convertKeyValuePairStructListStubToBeanList(KeyValuePairStructListStub listStub)
    {
        return convertKeyValuePairStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<KeyValuePairStruct> convertKeyValuePairStructListStubToBeanList(KeyValuePairStructListStub listStub, StringCursor forwardCursor)
    {
        List<KeyValuePairStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeyValuePairStruct>();
        } else {
            beanList = new ArrayList<KeyValuePairStruct>();
            for(KeyValuePairStructStub stub : listStub.getList()) {
            	beanList.add(convertKeyValuePairStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static KeyValuePairStructStub convertKeyValuePairStructToStub(KeyValuePairStruct bean)
    {
        KeyValuePairStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeyValuePairStructStub();
        } else if(bean instanceof KeyValuePairStructStub) {
            stub = (KeyValuePairStructStub) bean;
        } else if(bean instanceof KeyValuePairStructBean && ((KeyValuePairStructBean) bean).isWrapper()) {
            stub = ((KeyValuePairStructBean) bean).getStub();
        } else {
            stub = KeyValuePairStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static KeyValueRelationStructBean convertKeyValueRelationStructToBean(KeyValueRelationStruct stub)
    {
        KeyValueRelationStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeyValueRelationStructBean();
        } else if(stub instanceof KeyValueRelationStructBean) {
        	bean = (KeyValueRelationStructBean) stub;
        } else {
        	bean = new KeyValueRelationStructBean(stub);
        }
        return bean;
    }
    public static List<KeyValueRelationStructBean> convertKeyValueRelationStructListToBeanList(List<KeyValueRelationStruct> stubList)
    {
        List<KeyValueRelationStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeyValueRelationStruct>();
        } else {
            beanList = new ArrayList<KeyValueRelationStructBean>();
            for(KeyValueRelationStruct stub : stubList) {
                beanList.add(convertKeyValueRelationStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeyValueRelationStruct> convertKeyValueRelationStructListStubToBeanList(KeyValueRelationStructListStub listStub)
    {
        return convertKeyValueRelationStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<KeyValueRelationStruct> convertKeyValueRelationStructListStubToBeanList(KeyValueRelationStructListStub listStub, StringCursor forwardCursor)
    {
        List<KeyValueRelationStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeyValueRelationStruct>();
        } else {
            beanList = new ArrayList<KeyValueRelationStruct>();
            for(KeyValueRelationStructStub stub : listStub.getList()) {
            	beanList.add(convertKeyValueRelationStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static KeyValueRelationStructStub convertKeyValueRelationStructToStub(KeyValueRelationStruct bean)
    {
        KeyValueRelationStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeyValueRelationStructStub();
        } else if(bean instanceof KeyValueRelationStructStub) {
            stub = (KeyValueRelationStructStub) bean;
        } else if(bean instanceof KeyValueRelationStructBean && ((KeyValueRelationStructBean) bean).isWrapper()) {
            stub = ((KeyValueRelationStructBean) bean).getStub();
        } else {
            stub = KeyValueRelationStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ExternalServiceApiKeyStructBean convertExternalServiceApiKeyStructToBean(ExternalServiceApiKeyStruct stub)
    {
        ExternalServiceApiKeyStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ExternalServiceApiKeyStructBean();
        } else if(stub instanceof ExternalServiceApiKeyStructBean) {
        	bean = (ExternalServiceApiKeyStructBean) stub;
        } else {
        	bean = new ExternalServiceApiKeyStructBean(stub);
        }
        return bean;
    }
    public static List<ExternalServiceApiKeyStructBean> convertExternalServiceApiKeyStructListToBeanList(List<ExternalServiceApiKeyStruct> stubList)
    {
        List<ExternalServiceApiKeyStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ExternalServiceApiKeyStruct>();
        } else {
            beanList = new ArrayList<ExternalServiceApiKeyStructBean>();
            for(ExternalServiceApiKeyStruct stub : stubList) {
                beanList.add(convertExternalServiceApiKeyStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ExternalServiceApiKeyStruct> convertExternalServiceApiKeyStructListStubToBeanList(ExternalServiceApiKeyStructListStub listStub)
    {
        return convertExternalServiceApiKeyStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ExternalServiceApiKeyStruct> convertExternalServiceApiKeyStructListStubToBeanList(ExternalServiceApiKeyStructListStub listStub, StringCursor forwardCursor)
    {
        List<ExternalServiceApiKeyStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ExternalServiceApiKeyStruct>();
        } else {
            beanList = new ArrayList<ExternalServiceApiKeyStruct>();
            for(ExternalServiceApiKeyStructStub stub : listStub.getList()) {
            	beanList.add(convertExternalServiceApiKeyStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ExternalServiceApiKeyStructStub convertExternalServiceApiKeyStructToStub(ExternalServiceApiKeyStruct bean)
    {
        ExternalServiceApiKeyStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ExternalServiceApiKeyStructStub();
        } else if(bean instanceof ExternalServiceApiKeyStructStub) {
            stub = (ExternalServiceApiKeyStructStub) bean;
        } else if(bean instanceof ExternalServiceApiKeyStructBean && ((ExternalServiceApiKeyStructBean) bean).isWrapper()) {
            stub = ((ExternalServiceApiKeyStructBean) bean).getStub();
        } else {
            stub = ExternalServiceApiKeyStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ReferrerInfoStructBean convertReferrerInfoStructToBean(ReferrerInfoStruct stub)
    {
        ReferrerInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ReferrerInfoStructBean();
        } else if(stub instanceof ReferrerInfoStructBean) {
        	bean = (ReferrerInfoStructBean) stub;
        } else {
        	bean = new ReferrerInfoStructBean(stub);
        }
        return bean;
    }
    public static List<ReferrerInfoStructBean> convertReferrerInfoStructListToBeanList(List<ReferrerInfoStruct> stubList)
    {
        List<ReferrerInfoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStructBean>();
            for(ReferrerInfoStruct stub : stubList) {
                beanList.add(convertReferrerInfoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ReferrerInfoStruct> convertReferrerInfoStructListStubToBeanList(ReferrerInfoStructListStub listStub)
    {
        return convertReferrerInfoStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ReferrerInfoStruct> convertReferrerInfoStructListStubToBeanList(ReferrerInfoStructListStub listStub, StringCursor forwardCursor)
    {
        List<ReferrerInfoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStruct>();
            for(ReferrerInfoStructStub stub : listStub.getList()) {
            	beanList.add(convertReferrerInfoStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ReferrerInfoStructStub convertReferrerInfoStructToStub(ReferrerInfoStruct bean)
    {
        ReferrerInfoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ReferrerInfoStructStub();
        } else if(bean instanceof ReferrerInfoStructStub) {
            stub = (ReferrerInfoStructStub) bean;
        } else if(bean instanceof ReferrerInfoStructBean && ((ReferrerInfoStructBean) bean).isWrapper()) {
            stub = ((ReferrerInfoStructBean) bean).getStub();
        } else {
            stub = ReferrerInfoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeAppStructBean convertGaeAppStructToBean(GaeAppStruct stub)
    {
        GaeAppStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeAppStructBean();
        } else if(stub instanceof GaeAppStructBean) {
        	bean = (GaeAppStructBean) stub;
        } else {
        	bean = new GaeAppStructBean(stub);
        }
        return bean;
    }
    public static List<GaeAppStructBean> convertGaeAppStructListToBeanList(List<GaeAppStruct> stubList)
    {
        List<GaeAppStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStructBean>();
            for(GaeAppStruct stub : stubList) {
                beanList.add(convertGaeAppStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeAppStruct> convertGaeAppStructListStubToBeanList(GaeAppStructListStub listStub)
    {
        return convertGaeAppStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GaeAppStruct> convertGaeAppStructListStubToBeanList(GaeAppStructListStub listStub, StringCursor forwardCursor)
    {
        List<GaeAppStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStruct>();
            for(GaeAppStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeAppStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GaeAppStructStub convertGaeAppStructToStub(GaeAppStruct bean)
    {
        GaeAppStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeAppStructStub();
        } else if(bean instanceof GaeAppStructStub) {
            stub = (GaeAppStructStub) bean;
        } else if(bean instanceof GaeAppStructBean && ((GaeAppStructBean) bean).isWrapper()) {
            stub = ((GaeAppStructBean) bean).getStub();
        } else {
            stub = GaeAppStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeUserStructBean convertGaeUserStructToBean(GaeUserStruct stub)
    {
        GaeUserStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeUserStructBean();
        } else if(stub instanceof GaeUserStructBean) {
        	bean = (GaeUserStructBean) stub;
        } else {
        	bean = new GaeUserStructBean(stub);
        }
        return bean;
    }
    public static List<GaeUserStructBean> convertGaeUserStructListToBeanList(List<GaeUserStruct> stubList)
    {
        List<GaeUserStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStructBean>();
            for(GaeUserStruct stub : stubList) {
                beanList.add(convertGaeUserStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeUserStruct> convertGaeUserStructListStubToBeanList(GaeUserStructListStub listStub)
    {
        return convertGaeUserStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GaeUserStruct> convertGaeUserStructListStubToBeanList(GaeUserStructListStub listStub, StringCursor forwardCursor)
    {
        List<GaeUserStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStruct>();
            for(GaeUserStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeUserStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GaeUserStructStub convertGaeUserStructToStub(GaeUserStruct bean)
    {
        GaeUserStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeUserStructStub();
        } else if(bean instanceof GaeUserStructStub) {
            stub = (GaeUserStructStub) bean;
        } else if(bean instanceof GaeUserStructBean && ((GaeUserStructBean) bean).isWrapper()) {
            stub = ((GaeUserStructBean) bean).getStub();
        } else {
            stub = GaeUserStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static PagerStateStructBean convertPagerStateStructToBean(PagerStateStruct stub)
    {
        PagerStateStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new PagerStateStructBean();
        } else if(stub instanceof PagerStateStructBean) {
        	bean = (PagerStateStructBean) stub;
        } else {
        	bean = new PagerStateStructBean(stub);
        }
        return bean;
    }
    public static List<PagerStateStructBean> convertPagerStateStructListToBeanList(List<PagerStateStruct> stubList)
    {
        List<PagerStateStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<PagerStateStruct>();
        } else {
            beanList = new ArrayList<PagerStateStructBean>();
            for(PagerStateStruct stub : stubList) {
                beanList.add(convertPagerStateStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<PagerStateStruct> convertPagerStateStructListStubToBeanList(PagerStateStructListStub listStub)
    {
        return convertPagerStateStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<PagerStateStruct> convertPagerStateStructListStubToBeanList(PagerStateStructListStub listStub, StringCursor forwardCursor)
    {
        List<PagerStateStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<PagerStateStruct>();
        } else {
            beanList = new ArrayList<PagerStateStruct>();
            for(PagerStateStructStub stub : listStub.getList()) {
            	beanList.add(convertPagerStateStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static PagerStateStructStub convertPagerStateStructToStub(PagerStateStruct bean)
    {
        PagerStateStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new PagerStateStructStub();
        } else if(bean instanceof PagerStateStructStub) {
            stub = (PagerStateStructStub) bean;
        } else if(bean instanceof PagerStateStructBean && ((PagerStateStructBean) bean).isWrapper()) {
            stub = ((PagerStateStructBean) bean).getStub();
        } else {
            stub = PagerStateStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ApiConsumerBean convertApiConsumerToBean(ApiConsumer stub)
    {
        ApiConsumerBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ApiConsumerBean();
        } else if(stub instanceof ApiConsumerBean) {
        	bean = (ApiConsumerBean) stub;
        } else {
        	bean = new ApiConsumerBean(stub);
        }
        return bean;
    }
    public static List<ApiConsumerBean> convertApiConsumerListToBeanList(List<ApiConsumer> stubList)
    {
        List<ApiConsumerBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumerBean>();
            for(ApiConsumer stub : stubList) {
                beanList.add(convertApiConsumerToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ApiConsumer> convertApiConsumerListStubToBeanList(ApiConsumerListStub listStub)
    {
        return convertApiConsumerListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ApiConsumer> convertApiConsumerListStubToBeanList(ApiConsumerListStub listStub, StringCursor forwardCursor)
    {
        List<ApiConsumer> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumer>();
            for(ApiConsumerStub stub : listStub.getList()) {
            	beanList.add(convertApiConsumerToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ApiConsumerStub convertApiConsumerToStub(ApiConsumer bean)
    {
        ApiConsumerStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ApiConsumerStub();
        } else if(bean instanceof ApiConsumerStub) {
            stub = (ApiConsumerStub) bean;
        } else if(bean instanceof ApiConsumerBean && ((ApiConsumerBean) bean).isWrapper()) {
            stub = ((ApiConsumerBean) bean).getStub();
        } else {
            stub = ApiConsumerStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserBean convertUserToBean(User stub)
    {
        UserBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserBean();
        } else if(stub instanceof UserBean) {
        	bean = (UserBean) stub;
        } else {
        	bean = new UserBean(stub);
        }
        return bean;
    }
    public static List<UserBean> convertUserListToBeanList(List<User> stubList)
    {
        List<UserBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<UserBean>();
            for(User stub : stubList) {
                beanList.add(convertUserToBean(stub));
            }
        }
        return beanList;
    }
    public static List<User> convertUserListStubToBeanList(UserListStub listStub)
    {
        return convertUserListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<User> convertUserListStubToBeanList(UserListStub listStub, StringCursor forwardCursor)
    {
        List<User> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<User>();
            for(UserStub stub : listStub.getList()) {
            	beanList.add(convertUserToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UserStub convertUserToStub(User bean)
    {
        UserStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserStub();
        } else if(bean instanceof UserStub) {
            stub = (UserStub) bean;
        } else if(bean instanceof UserBean && ((UserBean) bean).isWrapper()) {
            stub = ((UserBean) bean).getStub();
        } else {
            stub = UserStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserPasswordBean convertUserPasswordToBean(UserPassword stub)
    {
        UserPasswordBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserPasswordBean();
        } else if(stub instanceof UserPasswordBean) {
        	bean = (UserPasswordBean) stub;
        } else {
        	bean = new UserPasswordBean(stub);
        }
        return bean;
    }
    public static List<UserPasswordBean> convertUserPasswordListToBeanList(List<UserPassword> stubList)
    {
        List<UserPasswordBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserPassword>();
        } else {
            beanList = new ArrayList<UserPasswordBean>();
            for(UserPassword stub : stubList) {
                beanList.add(convertUserPasswordToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserPassword> convertUserPasswordListStubToBeanList(UserPasswordListStub listStub)
    {
        return convertUserPasswordListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<UserPassword> convertUserPasswordListStubToBeanList(UserPasswordListStub listStub, StringCursor forwardCursor)
    {
        List<UserPassword> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserPassword>();
        } else {
            beanList = new ArrayList<UserPassword>();
            for(UserPasswordStub stub : listStub.getList()) {
            	beanList.add(convertUserPasswordToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UserPasswordStub convertUserPasswordToStub(UserPassword bean)
    {
        UserPasswordStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserPasswordStub();
        } else if(bean instanceof UserPasswordStub) {
            stub = (UserPasswordStub) bean;
        } else if(bean instanceof UserPasswordBean && ((UserPasswordBean) bean).isWrapper()) {
            stub = ((UserPasswordBean) bean).getStub();
        } else {
            stub = UserPasswordStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ExternalUserIdStructBean convertExternalUserIdStructToBean(ExternalUserIdStruct stub)
    {
        ExternalUserIdStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ExternalUserIdStructBean();
        } else if(stub instanceof ExternalUserIdStructBean) {
        	bean = (ExternalUserIdStructBean) stub;
        } else {
        	bean = new ExternalUserIdStructBean(stub);
        }
        return bean;
    }
    public static List<ExternalUserIdStructBean> convertExternalUserIdStructListToBeanList(List<ExternalUserIdStruct> stubList)
    {
        List<ExternalUserIdStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ExternalUserIdStruct>();
        } else {
            beanList = new ArrayList<ExternalUserIdStructBean>();
            for(ExternalUserIdStruct stub : stubList) {
                beanList.add(convertExternalUserIdStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ExternalUserIdStruct> convertExternalUserIdStructListStubToBeanList(ExternalUserIdStructListStub listStub)
    {
        return convertExternalUserIdStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ExternalUserIdStruct> convertExternalUserIdStructListStubToBeanList(ExternalUserIdStructListStub listStub, StringCursor forwardCursor)
    {
        List<ExternalUserIdStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ExternalUserIdStruct>();
        } else {
            beanList = new ArrayList<ExternalUserIdStruct>();
            for(ExternalUserIdStructStub stub : listStub.getList()) {
            	beanList.add(convertExternalUserIdStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ExternalUserIdStructStub convertExternalUserIdStructToStub(ExternalUserIdStruct bean)
    {
        ExternalUserIdStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ExternalUserIdStructStub();
        } else if(bean instanceof ExternalUserIdStructStub) {
            stub = (ExternalUserIdStructStub) bean;
        } else if(bean instanceof ExternalUserIdStructBean && ((ExternalUserIdStructBean) bean).isWrapper()) {
            stub = ((ExternalUserIdStructBean) bean).getStub();
        } else {
            stub = ExternalUserIdStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ExternalUserAuthBean convertExternalUserAuthToBean(ExternalUserAuth stub)
    {
        ExternalUserAuthBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ExternalUserAuthBean();
        } else if(stub instanceof ExternalUserAuthBean) {
        	bean = (ExternalUserAuthBean) stub;
        } else {
        	bean = new ExternalUserAuthBean(stub);
        }
        return bean;
    }
    public static List<ExternalUserAuthBean> convertExternalUserAuthListToBeanList(List<ExternalUserAuth> stubList)
    {
        List<ExternalUserAuthBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ExternalUserAuth>();
        } else {
            beanList = new ArrayList<ExternalUserAuthBean>();
            for(ExternalUserAuth stub : stubList) {
                beanList.add(convertExternalUserAuthToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ExternalUserAuth> convertExternalUserAuthListStubToBeanList(ExternalUserAuthListStub listStub)
    {
        return convertExternalUserAuthListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ExternalUserAuth> convertExternalUserAuthListStubToBeanList(ExternalUserAuthListStub listStub, StringCursor forwardCursor)
    {
        List<ExternalUserAuth> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ExternalUserAuth>();
        } else {
            beanList = new ArrayList<ExternalUserAuth>();
            for(ExternalUserAuthStub stub : listStub.getList()) {
            	beanList.add(convertExternalUserAuthToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ExternalUserAuthStub convertExternalUserAuthToStub(ExternalUserAuth bean)
    {
        ExternalUserAuthStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ExternalUserAuthStub();
        } else if(bean instanceof ExternalUserAuthStub) {
            stub = (ExternalUserAuthStub) bean;
        } else if(bean instanceof ExternalUserAuthBean && ((ExternalUserAuthBean) bean).isWrapper()) {
            stub = ((ExternalUserAuthBean) bean).getStub();
        } else {
            stub = ExternalUserAuthStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserAuthStateBean convertUserAuthStateToBean(UserAuthState stub)
    {
        UserAuthStateBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserAuthStateBean();
        } else if(stub instanceof UserAuthStateBean) {
        	bean = (UserAuthStateBean) stub;
        } else {
        	bean = new UserAuthStateBean(stub);
        }
        return bean;
    }
    public static List<UserAuthStateBean> convertUserAuthStateListToBeanList(List<UserAuthState> stubList)
    {
        List<UserAuthStateBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserAuthState>();
        } else {
            beanList = new ArrayList<UserAuthStateBean>();
            for(UserAuthState stub : stubList) {
                beanList.add(convertUserAuthStateToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserAuthState> convertUserAuthStateListStubToBeanList(UserAuthStateListStub listStub)
    {
        return convertUserAuthStateListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<UserAuthState> convertUserAuthStateListStubToBeanList(UserAuthStateListStub listStub, StringCursor forwardCursor)
    {
        List<UserAuthState> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserAuthState>();
        } else {
            beanList = new ArrayList<UserAuthState>();
            for(UserAuthStateStub stub : listStub.getList()) {
            	beanList.add(convertUserAuthStateToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UserAuthStateStub convertUserAuthStateToStub(UserAuthState bean)
    {
        UserAuthStateStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserAuthStateStub();
        } else if(bean instanceof UserAuthStateStub) {
            stub = (UserAuthStateStub) bean;
        } else if(bean instanceof UserAuthStateBean && ((UserAuthStateBean) bean).isWrapper()) {
            stub = ((UserAuthStateBean) bean).getStub();
        } else {
            stub = UserAuthStateStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ConsumerKeySecretPairBean convertConsumerKeySecretPairToBean(ConsumerKeySecretPair stub)
    {
        ConsumerKeySecretPairBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ConsumerKeySecretPairBean();
        } else if(stub instanceof ConsumerKeySecretPairBean) {
        	bean = (ConsumerKeySecretPairBean) stub;
        } else {
        	bean = new ConsumerKeySecretPairBean(stub);
        }
        return bean;
    }
    public static List<ConsumerKeySecretPairBean> convertConsumerKeySecretPairListToBeanList(List<ConsumerKeySecretPair> stubList)
    {
        List<ConsumerKeySecretPairBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ConsumerKeySecretPair>();
        } else {
            beanList = new ArrayList<ConsumerKeySecretPairBean>();
            for(ConsumerKeySecretPair stub : stubList) {
                beanList.add(convertConsumerKeySecretPairToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ConsumerKeySecretPair> convertConsumerKeySecretPairListStubToBeanList(ConsumerKeySecretPairListStub listStub)
    {
        return convertConsumerKeySecretPairListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ConsumerKeySecretPair> convertConsumerKeySecretPairListStubToBeanList(ConsumerKeySecretPairListStub listStub, StringCursor forwardCursor)
    {
        List<ConsumerKeySecretPair> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ConsumerKeySecretPair>();
        } else {
            beanList = new ArrayList<ConsumerKeySecretPair>();
            for(ConsumerKeySecretPairStub stub : listStub.getList()) {
            	beanList.add(convertConsumerKeySecretPairToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ConsumerKeySecretPairStub convertConsumerKeySecretPairToStub(ConsumerKeySecretPair bean)
    {
        ConsumerKeySecretPairStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ConsumerKeySecretPairStub();
        } else if(bean instanceof ConsumerKeySecretPairStub) {
            stub = (ConsumerKeySecretPairStub) bean;
        } else if(bean instanceof ConsumerKeySecretPairBean && ((ConsumerKeySecretPairBean) bean).isWrapper()) {
            stub = ((ConsumerKeySecretPairBean) bean).getStub();
        } else {
            stub = ConsumerKeySecretPairStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static DataServiceBean convertDataServiceToBean(DataService stub)
    {
        DataServiceBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new DataServiceBean();
        } else if(stub instanceof DataServiceBean) {
        	bean = (DataServiceBean) stub;
        } else {
        	bean = new DataServiceBean(stub);
        }
        return bean;
    }
    public static List<DataServiceBean> convertDataServiceListToBeanList(List<DataService> stubList)
    {
        List<DataServiceBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<DataService>();
        } else {
            beanList = new ArrayList<DataServiceBean>();
            for(DataService stub : stubList) {
                beanList.add(convertDataServiceToBean(stub));
            }
        }
        return beanList;
    }
    public static List<DataService> convertDataServiceListStubToBeanList(DataServiceListStub listStub)
    {
        return convertDataServiceListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<DataService> convertDataServiceListStubToBeanList(DataServiceListStub listStub, StringCursor forwardCursor)
    {
        List<DataService> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<DataService>();
        } else {
            beanList = new ArrayList<DataService>();
            for(DataServiceStub stub : listStub.getList()) {
            	beanList.add(convertDataServiceToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static DataServiceStub convertDataServiceToStub(DataService bean)
    {
        DataServiceStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new DataServiceStub();
        } else if(bean instanceof DataServiceStub) {
            stub = (DataServiceStub) bean;
        } else if(bean instanceof DataServiceBean && ((DataServiceBean) bean).isWrapper()) {
            stub = ((DataServiceBean) bean).getStub();
        } else {
            stub = DataServiceStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ServiceEndpointBean convertServiceEndpointToBean(ServiceEndpoint stub)
    {
        ServiceEndpointBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ServiceEndpointBean();
        } else if(stub instanceof ServiceEndpointBean) {
        	bean = (ServiceEndpointBean) stub;
        } else {
        	bean = new ServiceEndpointBean(stub);
        }
        return bean;
    }
    public static List<ServiceEndpointBean> convertServiceEndpointListToBeanList(List<ServiceEndpoint> stubList)
    {
        List<ServiceEndpointBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ServiceEndpoint>();
        } else {
            beanList = new ArrayList<ServiceEndpointBean>();
            for(ServiceEndpoint stub : stubList) {
                beanList.add(convertServiceEndpointToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ServiceEndpoint> convertServiceEndpointListStubToBeanList(ServiceEndpointListStub listStub)
    {
        return convertServiceEndpointListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ServiceEndpoint> convertServiceEndpointListStubToBeanList(ServiceEndpointListStub listStub, StringCursor forwardCursor)
    {
        List<ServiceEndpoint> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ServiceEndpoint>();
        } else {
            beanList = new ArrayList<ServiceEndpoint>();
            for(ServiceEndpointStub stub : listStub.getList()) {
            	beanList.add(convertServiceEndpointToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ServiceEndpointStub convertServiceEndpointToStub(ServiceEndpoint bean)
    {
        ServiceEndpointStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ServiceEndpointStub();
        } else if(bean instanceof ServiceEndpointStub) {
            stub = (ServiceEndpointStub) bean;
        } else if(bean instanceof ServiceEndpointBean && ((ServiceEndpointBean) bean).isWrapper()) {
            stub = ((ServiceEndpointBean) bean).getStub();
        } else {
            stub = ServiceEndpointStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static QuerySessionBean convertQuerySessionToBean(QuerySession stub)
    {
        QuerySessionBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new QuerySessionBean();
        } else if(stub instanceof QuerySessionBean) {
        	bean = (QuerySessionBean) stub;
        } else {
        	bean = new QuerySessionBean(stub);
        }
        return bean;
    }
    public static List<QuerySessionBean> convertQuerySessionListToBeanList(List<QuerySession> stubList)
    {
        List<QuerySessionBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<QuerySession>();
        } else {
            beanList = new ArrayList<QuerySessionBean>();
            for(QuerySession stub : stubList) {
                beanList.add(convertQuerySessionToBean(stub));
            }
        }
        return beanList;
    }
    public static List<QuerySession> convertQuerySessionListStubToBeanList(QuerySessionListStub listStub)
    {
        return convertQuerySessionListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<QuerySession> convertQuerySessionListStubToBeanList(QuerySessionListStub listStub, StringCursor forwardCursor)
    {
        List<QuerySession> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<QuerySession>();
        } else {
            beanList = new ArrayList<QuerySession>();
            for(QuerySessionStub stub : listStub.getList()) {
            	beanList.add(convertQuerySessionToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static QuerySessionStub convertQuerySessionToStub(QuerySession bean)
    {
        QuerySessionStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new QuerySessionStub();
        } else if(bean instanceof QuerySessionStub) {
            stub = (QuerySessionStub) bean;
        } else if(bean instanceof QuerySessionBean && ((QuerySessionBean) bean).isWrapper()) {
            stub = ((QuerySessionBean) bean).getStub();
        } else {
            stub = QuerySessionStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static QueryRecordBean convertQueryRecordToBean(QueryRecord stub)
    {
        QueryRecordBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new QueryRecordBean();
        } else if(stub instanceof QueryRecordBean) {
        	bean = (QueryRecordBean) stub;
        } else {
        	bean = new QueryRecordBean(stub);
        }
        return bean;
    }
    public static List<QueryRecordBean> convertQueryRecordListToBeanList(List<QueryRecord> stubList)
    {
        List<QueryRecordBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<QueryRecord>();
        } else {
            beanList = new ArrayList<QueryRecordBean>();
            for(QueryRecord stub : stubList) {
                beanList.add(convertQueryRecordToBean(stub));
            }
        }
        return beanList;
    }
    public static List<QueryRecord> convertQueryRecordListStubToBeanList(QueryRecordListStub listStub)
    {
        return convertQueryRecordListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<QueryRecord> convertQueryRecordListStubToBeanList(QueryRecordListStub listStub, StringCursor forwardCursor)
    {
        List<QueryRecord> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<QueryRecord>();
        } else {
            beanList = new ArrayList<QueryRecord>();
            for(QueryRecordStub stub : listStub.getList()) {
            	beanList.add(convertQueryRecordToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static QueryRecordStub convertQueryRecordToStub(QueryRecord bean)
    {
        QueryRecordStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new QueryRecordStub();
        } else if(bean instanceof QueryRecordStub) {
            stub = (QueryRecordStub) bean;
        } else if(bean instanceof QueryRecordBean && ((QueryRecordBean) bean).isWrapper()) {
            stub = ((QueryRecordBean) bean).getStub();
        } else {
            stub = QueryRecordStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static DummyEntityBean convertDummyEntityToBean(DummyEntity stub)
    {
        DummyEntityBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new DummyEntityBean();
        } else if(stub instanceof DummyEntityBean) {
        	bean = (DummyEntityBean) stub;
        } else {
        	bean = new DummyEntityBean(stub);
        }
        return bean;
    }
    public static List<DummyEntityBean> convertDummyEntityListToBeanList(List<DummyEntity> stubList)
    {
        List<DummyEntityBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<DummyEntity>();
        } else {
            beanList = new ArrayList<DummyEntityBean>();
            for(DummyEntity stub : stubList) {
                beanList.add(convertDummyEntityToBean(stub));
            }
        }
        return beanList;
    }
    public static List<DummyEntity> convertDummyEntityListStubToBeanList(DummyEntityListStub listStub)
    {
        return convertDummyEntityListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<DummyEntity> convertDummyEntityListStubToBeanList(DummyEntityListStub listStub, StringCursor forwardCursor)
    {
        List<DummyEntity> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<DummyEntity>();
        } else {
            beanList = new ArrayList<DummyEntity>();
            for(DummyEntityStub stub : listStub.getList()) {
            	beanList.add(convertDummyEntityToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static DummyEntityStub convertDummyEntityToStub(DummyEntity bean)
    {
        DummyEntityStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new DummyEntityStub();
        } else if(bean instanceof DummyEntityStub) {
            stub = (DummyEntityStub) bean;
        } else if(bean instanceof DummyEntityBean && ((DummyEntityBean) bean).isWrapper()) {
            stub = ((DummyEntityBean) bean).getStub();
        } else {
            stub = DummyEntityStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ServiceInfoBean convertServiceInfoToBean(ServiceInfo stub)
    {
        ServiceInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ServiceInfoBean();
        } else if(stub instanceof ServiceInfoBean) {
        	bean = (ServiceInfoBean) stub;
        } else {
        	bean = new ServiceInfoBean(stub);
        }
        return bean;
    }
    public static List<ServiceInfoBean> convertServiceInfoListToBeanList(List<ServiceInfo> stubList)
    {
        List<ServiceInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfoBean>();
            for(ServiceInfo stub : stubList) {
                beanList.add(convertServiceInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ServiceInfo> convertServiceInfoListStubToBeanList(ServiceInfoListStub listStub)
    {
        return convertServiceInfoListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ServiceInfo> convertServiceInfoListStubToBeanList(ServiceInfoListStub listStub, StringCursor forwardCursor)
    {
        List<ServiceInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfo>();
            for(ServiceInfoStub stub : listStub.getList()) {
            	beanList.add(convertServiceInfoToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ServiceInfoStub convertServiceInfoToStub(ServiceInfo bean)
    {
        ServiceInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ServiceInfoStub();
        } else if(bean instanceof ServiceInfoStub) {
            stub = (ServiceInfoStub) bean;
        } else if(bean instanceof ServiceInfoBean && ((ServiceInfoBean) bean).isWrapper()) {
            stub = ((ServiceInfoBean) bean).getStub();
        } else {
            stub = ServiceInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FiveTenBean convertFiveTenToBean(FiveTen stub)
    {
        FiveTenBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FiveTenBean();
        } else if(stub instanceof FiveTenBean) {
        	bean = (FiveTenBean) stub;
        } else {
        	bean = new FiveTenBean(stub);
        }
        return bean;
    }
    public static List<FiveTenBean> convertFiveTenListToBeanList(List<FiveTen> stubList)
    {
        List<FiveTenBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTenBean>();
            for(FiveTen stub : stubList) {
                beanList.add(convertFiveTenToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FiveTen> convertFiveTenListStubToBeanList(FiveTenListStub listStub)
    {
        return convertFiveTenListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FiveTen> convertFiveTenListStubToBeanList(FiveTenListStub listStub, StringCursor forwardCursor)
    {
        List<FiveTen> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTen>();
            for(FiveTenStub stub : listStub.getList()) {
            	beanList.add(convertFiveTenToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FiveTenStub convertFiveTenToStub(FiveTen bean)
    {
        FiveTenStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FiveTenStub();
        } else if(bean instanceof FiveTenStub) {
            stub = (FiveTenStub) bean;
        } else if(bean instanceof FiveTenBean && ((FiveTenBean) bean).isWrapper()) {
            stub = ((FiveTenBean) bean).getStub();
        } else {
            stub = FiveTenStub.convertBeanToStub(bean);
        }
        return stub;
    }

    
}
