package com.queryclient.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.app.service.UserAppService;
import com.queryclient.app.service.DataServiceAppService;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.DataService;


public class DataServiceHelper
{
    private static final Logger log = Logger.getLogger(DataServiceHelper.class.getName());

    private DataServiceHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private DataServiceAppService dataServiceService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private DataServiceAppService getDataServiceService()
    {
        if(dataServiceService == null) {
            dataServiceService = new DataServiceAppService();
        }
        return dataServiceService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class DataServiceHelperHolder
    {
        private static final DataServiceHelper INSTANCE = new DataServiceHelper();
    }

    // Singleton method
    public static DataServiceHelper getInstance()
    {
        return DataServiceHelperHolder.INSTANCE;
    }

    
    public DataService getDataService(String guid) 
    {
        DataService dataService = null;
        try {
            dataService = getDataServiceService().getDataService(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the dataService for guid = " + guid, e);
        }
        return dataService;
    }
    
 
    // TBD:
    
    public DataService getDataServiceByName(String name)
    {
        DataService bean = null;
        
        try {
            String filter = "name=='" + name + "'";   // Status?
            String ordering = "createdTime desc";
            // String ordering = null;
            Long offset = 0L;
            Integer count = 2;
            List<DataService> list = getDataServiceService().findDataServices(filter, ordering, null, null, null, null, offset, count);
            if(list != null && !list.isEmpty()) {
                bean = list.get(0);
                if(list.size() > 1) {
                    if(log.isLoggable(Level.INFO)) log.info("More than on dataService found for name = " + name);
                }
                // full fetch
                bean = getDataService(bean.getGuid());                
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find dataService for given name = " + name, e);
        }

        return bean;
    }

//    public List<DataService> findDataServicesByName(String name)
//    {
//        return findDataServicesByName(name, null, null);
//    }
//    public List<DataService> findDataServicesByName(String name, Long offset, Integer count)
//    {
//        List<DataService> beans = null;
//        
//        try {
//            String filter = "name=='" + name + "'";   // Status?
//            // String ordering = "createdTime desc";
//            String ordering = null;
//            beans = getDataServiceService().findDataServices(filter, ordering, null, null, null, null, offset, count);    
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to find dataServices for given name = " + name, e);
//        }
//
//        return beans;
//    }

    
    public DataService getDataServiceByServiceUrl(String serviceUrl)
    {
        DataService bean = null;
        
        try {
            String filter = "serviceUrl=='" + serviceUrl + "'";   // Status?
            String ordering = "createdTime desc";
            // String ordering = null;
            Long offset = 0L;
            Integer count = 2;
            List<DataService> list = getDataServiceService().findDataServices(filter, ordering, null, null, null, null, offset, count);
            if(list != null && !list.isEmpty()) {
                bean = list.get(0);
                if(list.size() > 1) {
                    if(log.isLoggable(Level.INFO)) log.info("More than on dataService found for serviceUrl = " + serviceUrl);
                }
                // full fetch
                bean = getDataService(bean.getGuid());                
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find dataService for given serviceUrl = " + serviceUrl, e);
        }

        return bean;
    }


}
