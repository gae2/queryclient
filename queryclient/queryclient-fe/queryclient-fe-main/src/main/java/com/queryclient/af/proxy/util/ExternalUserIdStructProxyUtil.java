package com.queryclient.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.ExternalUserIdStruct;
// import com.queryclient.ws.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;


public class ExternalUserIdStructProxyUtil
{
    private static final Logger log = Logger.getLogger(ExternalUserIdStructProxyUtil.class.getName());

    // Static methods only.
    private ExternalUserIdStructProxyUtil() {}

    public static ExternalUserIdStructBean convertServerExternalUserIdStructBeanToAppBean(ExternalUserIdStruct serverBean)
    {
        ExternalUserIdStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new ExternalUserIdStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setId(serverBean.getId());
            bean.setName(serverBean.getName());
            bean.setEmail(serverBean.getEmail());
            bean.setUsername(serverBean.getUsername());
            bean.setOpenId(serverBean.getOpenId());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.queryclient.ws.bean.ExternalUserIdStructBean convertAppExternalUserIdStructBeanToServerBean(ExternalUserIdStruct appBean)
    {
        com.queryclient.ws.bean.ExternalUserIdStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.ExternalUserIdStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setId(appBean.getId());
            bean.setName(appBean.getName());
            bean.setEmail(appBean.getEmail());
            bean.setUsername(appBean.getUsername());
            bean.setOpenId(appBean.getOpenId());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
