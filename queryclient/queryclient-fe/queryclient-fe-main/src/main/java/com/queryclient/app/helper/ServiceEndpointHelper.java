package com.queryclient.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.app.service.UserAppService;
import com.queryclient.app.service.ServiceEndpointAppService;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.ServiceEndpoint;


public class ServiceEndpointHelper
{
    private static final Logger log = Logger.getLogger(ServiceEndpointHelper.class.getName());

    private ServiceEndpointHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private ServiceEndpointAppService serviceEndpointService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private ServiceEndpointAppService getServiceEndpointService()
    {
        if(serviceEndpointService == null) {
            serviceEndpointService = new ServiceEndpointAppService();
        }
        return serviceEndpointService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class ServiceEndpointHelperHolder
    {
        private static final ServiceEndpointHelper INSTANCE = new ServiceEndpointHelper();
    }

    // Singleton method
    public static ServiceEndpointHelper getInstance()
    {
        return ServiceEndpointHelperHolder.INSTANCE;
    }

    
    public ServiceEndpoint getServiceEndpoint(String guid) 
    {
        ServiceEndpoint serviceEndpoint = null;
        try {
            serviceEndpoint = getServiceEndpointService().getServiceEndpoint(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the serviceEndpoint for guid = " + guid, e);
        }
        return serviceEndpoint;
    }
    
 
    // TBD:
    
    // There should be only one ServiceEndpoint for the given serviceUrl..
    public ServiceEndpoint getServiceEndpointByServiceUrl(String serviceUrl)
    {
        ServiceEndpoint bean = null;
        
        try {
            String filter = "serviceUrl=='" + serviceUrl + "'";   // Status?
            String ordering = "createdTime desc";
            // String ordering = null;
            Long offset = 0L;
            Integer count = 2;
            List<ServiceEndpoint> list = getServiceEndpointService().findServiceEndpoints(filter, ordering, null, null, null, null, offset, count);
            if(list != null && !list.isEmpty()) {
                bean = list.get(0);
                if(list.size() > 1) {
                    if(log.isLoggable(Level.INFO)) log.info("More than on serviceEndpoint found for serviceUrl = " + serviceUrl);
                }
                // full fetch
                bean = getServiceEndpoint(bean.getGuid());                
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find serviceEndpoint for given serviceUrl = " + serviceUrl, e);
        }

        return bean;
    }

    // Returns the most recent one...
    public ServiceEndpoint getServiceEndpointByServiceName(String serviceName)
    {
        ServiceEndpoint bean = null;
        
        try {
            String filter = "serviceName=='" + serviceName + "'";   // Status?
            String ordering = "createdTime desc";
            // String ordering = null;
            Long offset = 0L;
            Integer count = 2;
            List<ServiceEndpoint> list = getServiceEndpointService().findServiceEndpoints(filter, ordering, null, null, null, null, offset, count);
            if(list != null && !list.isEmpty()) {
                bean = list.get(0);
                // full fetch
                bean = getServiceEndpoint(bean.getGuid());                
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find serviceEndpoint for given serviceName = " + serviceName, e);
        }

        return bean;
    }

    public List<ServiceEndpoint> findServiceEndpointsByDataService(String dataService)
    {
        return findServiceEndpointsByDataService(dataService, null, null);
    }
    public List<ServiceEndpoint> findServiceEndpointsByDataService(String dataService, Long offset, Integer count)
    {
        List<ServiceEndpoint> beans = null;
        
        try {
            String filter = "dataService=='" + dataService + "'";   // Status?
            // String ordering = "createdTime desc";
            String ordering = null;
            beans = getServiceEndpointService().findServiceEndpoints(filter, ordering, null, null, null, null, offset, count);    
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find serviceEndpoints for given dataService = " + dataService, e);
        }

        return beans;
    }
    
    

}
