package com.queryclient.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalServiceApiKeyStructJsBean implements Serializable, Cloneable  //, ExternalServiceApiKeyStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExternalServiceApiKeyStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String service;
    private String key;
    private String secret;
    private String note;

    // Ctors.
    public ExternalServiceApiKeyStructJsBean()
    {
        //this((String) null);
    }
    public ExternalServiceApiKeyStructJsBean(String uuid, String service, String key, String secret, String note)
    {
        this.uuid = uuid;
        this.service = service;
        this.key = key;
        this.secret = secret;
        this.note = note;
    }
    public ExternalServiceApiKeyStructJsBean(ExternalServiceApiKeyStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setService(bean.getService());
            setKey(bean.getKey());
            setSecret(bean.getSecret());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ExternalServiceApiKeyStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ExternalServiceApiKeyStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ExternalServiceApiKeyStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ExternalServiceApiKeyStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getService()
    {
        return this.service;
    }
    public void setService(String service)
    {
        this.service = service;
    }

    public String getKey()
    {
        return this.key;
    }
    public void setKey(String key)
    {
        this.key = key;
    }

    public String getSecret()
    {
        return this.secret;
    }
    public void setSecret(String secret)
    {
        this.secret = secret;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getService() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecret() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("service:null, ");
        sb.append("key:null, ");
        sb.append("secret:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("service:");
        if(this.getService() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getService()).append("\", ");
        }
        sb.append("key:");
        if(this.getKey() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getKey()).append("\", ");
        }
        sb.append("secret:");
        if(this.getSecret() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSecret()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getService() != null) {
            sb.append("\"service\":").append("\"").append(this.getService()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"service\":").append("null, ");
        }
        if(this.getKey() != null) {
            sb.append("\"key\":").append("\"").append(this.getKey()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"key\":").append("null, ");
        }
        if(this.getSecret() != null) {
            sb.append("\"secret\":").append("\"").append(this.getSecret()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"secret\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("service = " + this.service).append(";");
        sb.append("key = " + this.key).append(";");
        sb.append("secret = " + this.secret).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ExternalServiceApiKeyStructJsBean cloned = new ExternalServiceApiKeyStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setService(this.getService());   
        cloned.setKey(this.getKey());   
        cloned.setSecret(this.getSecret());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
