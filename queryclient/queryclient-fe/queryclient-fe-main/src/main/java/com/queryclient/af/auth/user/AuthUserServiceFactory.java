package com.queryclient.af.auth.user;

import java.util.logging.Logger;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class AuthUserServiceFactory
{
    private static final Logger log = Logger.getLogger(AuthUserServiceFactory.class.getName());

//    private UserServiceFactory mGaeUserServiceFactory = null;
//    private AuthUserServiceFactory(UserServiceFactory gaeUserServiceFactory)
//    {
//        mGaeUserServiceFactory = gaeUserServiceFactory;
//    }


    // Is it safe to cache this????
    private static AuthUserService mAuthUserService = null;

    public static AuthUserService getAuthUserService()
    {
        if(mAuthUserService == null) {
            UserService gaeUserService = UserServiceFactory.getUserService();
            if(gaeUserService != null) {
                mAuthUserService = new AuthUserServiceImpl(gaeUserService);
            }
            // else ?????
        }
        return mAuthUserService;
    }

    
}
