package com.queryclient.af.cron;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * To support multiple cron expressions. 
 * E.g., use cases:
 * "every 10AM and 5PM weekdays, and 2PM Saturdays and Sundays"
 * etc.
 */
public final class CronExpressionListParser 
{
    private static final Logger log = Logger.getLogger(CronExpressionListParser.class.getName());

    // Input cron expression list
    //private final List<String> expressions;

    // List of CronExpressionParsers.
    private List<CronExpressionParser> parsers = null;


    // Ctors.
    public CronExpressionListParser(List<String> expressions) 
    {
        this(expressions, null);
    }
    public CronExpressionListParser(List<String> expressions, String timezoneID) 
    {
        //this.expressions = expressions;
        parsers = new ArrayList<CronExpressionParser>();
        if(expressions != null && !expressions.isEmpty()) {
            for(String e : expressions) {
                CronExpressionParser parser = new CronExpressionParser(e, timezoneID);
                parsers.add(parser);
            }
        }
    }

//    public List<String> getExpressions()
//    {
//        return expressions;
//    }
//    public List<CronExpressionParser> getParsers()
//    {
//        return parsers;
//    }


    public Long getNextCronTime()
    {
        return getNextCronTime(System.currentTimeMillis());
    }
    public Long getNextCronTime(long now)
    {
        if(log.isLoggable(Level.INFO)) log.info("getNextCronTime() BEGIN: now = " + now);

        // TBD: Just use Math.min(a, b) ?
        Long nextCronTime = null;
        for(CronExpressionParser parser : parsers) {
            Long t = parser.getNextCronTime();
            if(t != null && t > 0L) {
                if(nextCronTime == null || nextCronTime == 0L) {
                    nextCronTime = t;
                } else {
                    if(t < nextCronTime) {
                        nextCronTime = t;
                    }
                }
            }
        }

        if(log.isLoggable(Level.INFO)) log.info("getNextCronTime() END: nextCronTime = " + nextCronTime);
        return nextCronTime;
    }
    
    public Long getPreviousCronTime()
    {
        return getPreviousCronTime(System.currentTimeMillis());
    }
    public Long getPreviousCronTime(long now)
    {
        if(log.isLoggable(Level.INFO)) log.info("getPreviousCronTime() BEGIN: now = " + now);

        // TBD: Just use Math.max(a, b) ?
        Long previousCronTime = null;
        for(CronExpressionParser parser : parsers) {
            Long t = parser.getPreviousCronTime();
            if(t != null && t > 0L) {
                if(previousCronTime == null || previousCronTime == 0L) {
                    previousCronTime = t;
                } else {
                    if(t > previousCronTime) {
                        previousCronTime = t;
                    }
                }
            }
        }

        if(log.isLoggable(Level.INFO)) log.info("getPreviousCronTime() END: previousCronTime = " + previousCronTime);
        return previousCronTime;
    }
    

    @Override
    public String toString() 
    {
        StringBuilder sb = new StringBuilder();
        for(CronExpressionParser parser : parsers) {
            sb.append(parser.toString()).append(" ");
        }
        return sb.toString();
    }
    
    
}