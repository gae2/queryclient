package com.queryclient.af.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


// Using Dependency Injection.
// Serves sort of as a factory method.
public final class PermissionManagerFactory
{
    private static final Logger log = Logger.getLogger(PermissionManagerFactory.class.getName());

    private BasePermissionManager permissionManager = null;

    // Prevents instantiation.
    private PermissionManagerFactory() {}


    // Initialization-on-demand holder.
    private static final class PermissionManagerFactoryHolder
    {
        private static final PermissionManagerFactory INSTANCE = new PermissionManagerFactory();
    }

    // Singleton method
    public static PermissionManagerFactory getInstance()
    {
        return PermissionManagerFactoryHolder.INSTANCE;
    }


    // Returns a permission manager.
    public BasePermissionManager getPermissionManager() 
    {
        if(permissionManager == null) {
            permissionManager = new BasePermissionManager();
        }
        return permissionManager;
    }

    public void setPermissionManager(BasePermissionManager permissionManager) 
    {
        this.permissionManager = permissionManager;
    }

}
