package com.queryclient.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class KeyValueRelationStructJsBean extends KeyValuePairStructJsBean implements Serializable, Cloneable  //, KeyValueRelationStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeyValueRelationStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String relation;

    // Ctors.
    public KeyValueRelationStructJsBean()
    {
        //this((String) null);
    }
    public KeyValueRelationStructJsBean(String uuid, String key, String value, String note, String relation)
    {
        super(uuid, key, value, note);

        this.relation = relation;
    }
    public KeyValueRelationStructJsBean(KeyValueRelationStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setKey(bean.getKey());
            setValue(bean.getValue());
            setNote(bean.getNote());
            setRelation(bean.getRelation());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static KeyValueRelationStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        KeyValueRelationStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(KeyValueRelationStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, KeyValueRelationStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return super.getUuid();
    }
    public void setUuid(String uuid)
    {
        super.setUuid(uuid);
    }

    public String getKey()
    {
        return super.getKey();
    }
    public void setKey(String key)
    {
        super.setKey(key);
    }

    public String getValue()
    {
        return super.getValue();
    }
    public void setValue(String value)
    {
        super.setValue(value);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public String getRelation()
    {
        return this.relation;
    }
    public void setRelation(String relation)
    {
        this.relation = relation;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getValue() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRelation() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("key:null, ");
        sb.append("value:null, ");
        sb.append("note:null, ");
        sb.append("relation:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("key:");
        if(this.getKey() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getKey()).append("\", ");
        }
        sb.append("value:");
        if(this.getValue() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getValue()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("relation:");
        if(this.getRelation() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRelation()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getKey() != null) {
            sb.append("\"key\":").append("\"").append(this.getKey()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"key\":").append("null, ");
        }
        if(this.getValue() != null) {
            sb.append("\"value\":").append("\"").append(this.getValue()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"value\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getRelation() != null) {
            sb.append("\"relation\":").append("\"").append(this.getRelation()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"relation\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("relation = " + this.relation).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        KeyValueRelationStructJsBean cloned = new KeyValueRelationStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setKey(this.getKey());   
        cloned.setValue(this.getValue());   
        cloned.setNote(this.getNote());   
        cloned.setRelation(this.getRelation());   
        return cloned;
    }

}
