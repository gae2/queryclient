package com.queryclient.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.fe.bean.ConsumerKeySecretPairJsBean;


public class ConsumerKeySecretPairWebUtil
{
    private static final Logger log = Logger.getLogger(ConsumerKeySecretPairWebUtil.class.getName());

    // Static methods only.
    private ConsumerKeySecretPairWebUtil() {}
    

    public static ConsumerKeySecretPairJsBean convertConsumerKeySecretPairToJsBean(ConsumerKeySecretPair consumerKeySecretPair)
    {
        ConsumerKeySecretPairJsBean jsBean = null;
        if(consumerKeySecretPair != null) {
            jsBean = new ConsumerKeySecretPairJsBean();
            jsBean.setConsumerKey(consumerKeySecretPair.getConsumerKey());
            jsBean.setConsumerSecret(consumerKeySecretPair.getConsumerSecret());
        }
        return jsBean;
    }

    public static ConsumerKeySecretPair convertConsumerKeySecretPairJsBeanToBean(ConsumerKeySecretPairJsBean jsBean)
    {
        ConsumerKeySecretPairBean consumerKeySecretPair = null;
        if(jsBean != null) {
            consumerKeySecretPair = new ConsumerKeySecretPairBean();
            consumerKeySecretPair.setConsumerKey(jsBean.getConsumerKey());
            consumerKeySecretPair.setConsumerSecret(jsBean.getConsumerSecret());
        }
        return consumerKeySecretPair;
    }

}
