package com.queryclient.af.auth.googleplus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.queryclient.af.config.Config;
import com.queryclient.af.util.URLUtil;
import com.queryclient.af.util.CsrfHelper;
import com.queryclient.af.auth.common.CommonAuthUtil;


// Note:
public class GooglePlusAuthHelper
{
    private static final Logger log = Logger.getLogger(GooglePlusAuthHelper.class.getName());

    // "Lazy initialization"
    private String mApplicationName = null;
    private String mClientId = null;
    private String mClientSecret = null;
    // private String mDefaultAccessToken = null;
    // private String mDefaultAccessTokenSecret = null;
    private String mCallbackTokenHandlerUrlPath = null;
    private String mCallbackAuthAjaxUrlPath = null;
    // private String mOAuthRedirectUrlPath = null;
    private String mJavascriptSignInCallback = null;
    // private String mDefaultSignInFormId = null;


    private GooglePlusAuthHelper() {}

    // Initialization-on-demand holder.
    private static final class GooglePlusAuthHelperHolder
    {
        private static final GooglePlusAuthHelper INSTANCE = new GooglePlusAuthHelper();
    }

    // Singleton method
    public static GooglePlusAuthHelper getInstance()
    {
        return GooglePlusAuthHelperHolder.INSTANCE;
    }
    
    
    public String getApplicationName()
    {
        if(mApplicationName == null) {
            mApplicationName = Config.getInstance().getString(GooglePlusAuthUtil.CONFIG_KEY_APPLICATION_NAME);
        }
        return mApplicationName;
    }
    public String getClientId()
    {
        if(mClientId == null) {
            mClientId = Config.getInstance().getString(GooglePlusAuthUtil.CONFIG_KEY_CLIENT_ID);
        }
        return mClientId;
    }
    public String getClientSecret()
    {
        if(mClientSecret == null) {
            mClientSecret = Config.getInstance().getString(GooglePlusAuthUtil.CONFIG_KEY_CLIENT_SECRET);
        }
        return mClientSecret;
    }
//    public String getDefaultAccessToken()
//    {
//        if(mDefaultAccessToken == null) {
//            mDefaultAccessToken = Config.getInstance().getString(GooglePlusAuthUtil.CONFIG_KEY_DEFAULT_ACCESSTOKEN);
//        }
//        return mDefaultAccessToken;
//    }
//    public String getDefaultAccessTokenSecret()
//    {
//        if(mDefaultAccessTokenSecret == null) {
//            mDefaultAccessTokenSecret = Config.getInstance().getString(GooglePlusAuthUtil.CONFIG_KEY_DEFAULT_ACCESSTOKENSECRET);
//        }
//        return mDefaultAccessTokenSecret;
//    }

    private String getCallbackTokenHandlerUrlPath()
    {
        if(mCallbackTokenHandlerUrlPath == null) {
            mCallbackTokenHandlerUrlPath = Config.getInstance().getString(GooglePlusAuthUtil.CONFIG_KEY_CALLBACK_TOKENHANDLER_URLPATH);
        }
        return mCallbackTokenHandlerUrlPath;
    }
    public String getCallbackTokenHandlerUri(String topLevelUrl)
    {
        return CommonAuthUtil.constructUrl(topLevelUrl, getCallbackTokenHandlerUrlPath());
    }
    private String getCallbackAuthAjaxUrlPath()
    {
        if(mCallbackAuthAjaxUrlPath == null) {
            mCallbackAuthAjaxUrlPath = Config.getInstance().getString(GooglePlusAuthUtil.CONFIG_KEY_CALLBACK_AUTHAJAX_URLPATH);
        }
        return mCallbackAuthAjaxUrlPath;
    }
    public String getCallbackAuthAjaxUri(String topLevelUrl)
    {
        return CommonAuthUtil.constructUrl(topLevelUrl, getCallbackAuthAjaxUrlPath());
    }

//    private String getOAuthRedirectUrlPath()
//    {
//        if(mOAuthRedirectUrlPath == null) {
//            mOAuthRedirectUrlPath = Config.getInstance().getString(GooglePlusAuthUtil.CONFIG_KEY_OAUTH_REDIRECTURLPATH);
//        }
//        return mOAuthRedirectUrlPath;
//    }
//    public String getDefaultOAuthRedirectUri(String topLevelUrl)
//    {
//        return CommonAuthUtil.constructUrl(topLevelUrl, getOAuthRedirectUrlPath());
//    }

    public String getJavascriptSignInCallback()
    {
        if(mJavascriptSignInCallback == null) {
            mJavascriptSignInCallback = Config.getInstance().getString(GooglePlusAuthUtil.CONFIG_KEY_JAVASCRIPT_SIGNIN_CALLBACK);
        }
        return mJavascriptSignInCallback;
    }

    public String getDefaultSignInFormId()
    {
        return GooglePlusAuthUtil.DEFAULT_FORM_ID_GOOGLEPLUS_SIGNIN;
    }


    // Returns the csrf in the current session or the generated csrf state string.
    public String setCsrfStateSessionAttribute(HttpSession session)
    {
        return setCsrfStateSessionAttribute(session, null);
    }
    public String setCsrfStateSessionAttribute(HttpSession session, String formId)
    {
        if(session == null) {
            // ????
            return null;
        }
        if(formId == null || formId.isEmpty()) {
            formId = GooglePlusAuthUtil.getDefaultGooglePlusSignInFormId();
        }
        String csrfState = CsrfHelper.getInstance().setCsrfStateSessionAttribute(session, null, formId);
//        String csrfState = CsrfHelper.getInstance().getCsrfStateFromSession(session, formId);
//        if(csrfState != null && !csrfState.isEmpty()) {
//            if(log.isLoggable(Level.FINE)) log.fine("csrfState found in the current session: " + csrfState);
//            return csrfState;
//        }
//        csrfState = CsrfHelper.getInstance().generateRandomCsrfState();
//        csrfState = CsrfHelper.getInstance().setCsrfStateSessionAttribute(session, csrfState, formId);
        return csrfState;
    }


    public boolean verifyCsrfStateFromRequest(HttpServletRequest request)
    {
        if(request == null) {
            // ????
            return false;
        }
        String formId = GooglePlusAuthUtil.getDefaultGooglePlusSignInFormId();
        boolean isVerified = CsrfHelper.getInstance().verifyCsrfState(request, null, formId);
        return isVerified;
    }


}
