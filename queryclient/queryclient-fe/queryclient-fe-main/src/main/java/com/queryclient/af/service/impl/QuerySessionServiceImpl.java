package com.queryclient.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.QuerySessionService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class QuerySessionServiceImpl implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(QuerySessionServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "QuerySession-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("QuerySession:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public QuerySessionServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // QuerySession related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getQuerySession(): guid = " + guid);

        QuerySessionBean bean = null;
        if(getCache() != null) {
            bean = (QuerySessionBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (QuerySessionBean) getProxyFactory().getQuerySessionServiceProxy().getQuerySession(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "QuerySessionBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve QuerySessionBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        QuerySessionBean bean = null;
        if(getCache() != null) {
            bean = (QuerySessionBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (QuerySessionBean) getProxyFactory().getQuerySessionServiceProxy().getQuerySession(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "QuerySessionBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve QuerySessionBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("dataService")) {
            return bean.getDataService();
        } else if(field.equals("serviceUrl")) {
            return bean.getServiceUrl();
        } else if(field.equals("inputFormat")) {
            return bean.getInputFormat();
        } else if(field.equals("outputFormat")) {
            return bean.getOutputFormat();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        log.fine("getQuerySessions()");

        // TBD: Is there a better way????
        List<QuerySession> querySessions = getProxyFactory().getQuerySessionServiceProxy().getQuerySessions(guids);
        if(querySessions == null) {
            log.log(Level.WARNING, "Failed to retrieve QuerySessionBean list.");
        }

        log.finer("END");
        return querySessions;
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return getAllQuerySessions(null, null, null);
    }


    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQuerySessions(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<QuerySession> querySessions = getProxyFactory().getQuerySessionServiceProxy().getAllQuerySessions(ordering, offset, count, forwardCursor);
        if(querySessions == null) {
            log.log(Level.WARNING, "Failed to retrieve QuerySessionBean list.");
        }

        log.finer("END");
        return querySessions;
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQuerySessionKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getQuerySessionServiceProxy().getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve QuerySessionBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty QuerySessionBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QuerySessionServiceImpl.findQuerySessions(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<QuerySession> querySessions = getProxyFactory().getQuerySessionServiceProxy().findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(querySessions == null) {
            log.log(Level.WARNING, "Failed to find querySessions for the given criterion.");
        }

        log.finer("END");
        return querySessions;
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QuerySessionServiceImpl.findQuerySessionKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getQuerySessionServiceProxy().findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find QuerySession keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty QuerySession key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QuerySessionServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getQuerySessionServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        QuerySessionBean bean = new QuerySessionBean(null, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfoBean, status, note);
        return createQuerySession(bean);
    }

    @Override
    public String createQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //QuerySession bean = constructQuerySession(querySession);
        //return bean.getGuid();

        // Param querySession cannot be null.....
        if(querySession == null) {
            log.log(Level.INFO, "Param querySession is null!");
            throw new BadRequestException("Param querySession object is null!");
        }
        QuerySessionBean bean = null;
        if(querySession instanceof QuerySessionBean) {
            bean = (QuerySessionBean) querySession;
        } else if(querySession instanceof QuerySession) {
            // bean = new QuerySessionBean(null, querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), (ReferrerInfoStructBean) querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new QuerySessionBean(querySession.getGuid(), querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), (ReferrerInfoStructBean) querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
        } else {
            log.log(Level.WARNING, "createQuerySession(): Arg querySession is of an unknown type.");
            //bean = new QuerySessionBean();
            bean = new QuerySessionBean(querySession.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getQuerySessionServiceProxy().createQuerySession(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public QuerySession constructQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");

        // Param querySession cannot be null.....
        if(querySession == null) {
            log.log(Level.INFO, "Param querySession is null!");
            throw new BadRequestException("Param querySession object is null!");
        }
        QuerySessionBean bean = null;
        if(querySession instanceof QuerySessionBean) {
            bean = (QuerySessionBean) querySession;
        } else if(querySession instanceof QuerySession) {
            // bean = new QuerySessionBean(null, querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), (ReferrerInfoStructBean) querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new QuerySessionBean(querySession.getGuid(), querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), (ReferrerInfoStructBean) querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
        } else {
            log.log(Level.WARNING, "createQuerySession(): Arg querySession is of an unknown type.");
            //bean = new QuerySessionBean();
            bean = new QuerySessionBean(querySession.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getQuerySessionServiceProxy().createQuerySession(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        QuerySessionBean bean = new QuerySessionBean(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfoBean, status, note);
        return updateQuerySession(bean);
    }
        
    @Override
    public Boolean updateQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //QuerySession bean = refreshQuerySession(querySession);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param querySession cannot be null.....
        if(querySession == null || querySession.getGuid() == null) {
            log.log(Level.INFO, "Param querySession or its guid is null!");
            throw new BadRequestException("Param querySession object or its guid is null!");
        }
        QuerySessionBean bean = null;
        if(querySession instanceof QuerySessionBean) {
            bean = (QuerySessionBean) querySession;
        } else {  // if(querySession instanceof QuerySession)
            bean = new QuerySessionBean(querySession.getGuid(), querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), (ReferrerInfoStructBean) querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
        }
        Boolean suc = getProxyFactory().getQuerySessionServiceProxy().updateQuerySession(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public QuerySession refreshQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");

        // Param querySession cannot be null.....
        if(querySession == null || querySession.getGuid() == null) {
            log.log(Level.INFO, "Param querySession or its guid is null!");
            throw new BadRequestException("Param querySession object or its guid is null!");
        }
        QuerySessionBean bean = null;
        if(querySession instanceof QuerySessionBean) {
            bean = (QuerySessionBean) querySession;
        } else {  // if(querySession instanceof QuerySession)
            bean = new QuerySessionBean(querySession.getGuid(), querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), (ReferrerInfoStructBean) querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
        }
        Boolean suc = getProxyFactory().getQuerySessionServiceProxy().updateQuerySession(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getQuerySessionServiceProxy().deleteQuerySession(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            QuerySession querySession = null;
            try {
                querySession = getProxyFactory().getQuerySessionServiceProxy().getQuerySession(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch querySession with a key, " + guid);
                return false;
            }
            if(querySession != null) {
                String beanGuid = querySession.getGuid();
                Boolean suc1 = getProxyFactory().getQuerySessionServiceProxy().deleteQuerySession(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("querySession with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("BEGIN");

        // Param querySession cannot be null.....
        if(querySession == null || querySession.getGuid() == null) {
            log.log(Level.INFO, "Param querySession or its guid is null!");
            throw new BadRequestException("Param querySession object or its guid is null!");
        }
        QuerySessionBean bean = null;
        if(querySession instanceof QuerySessionBean) {
            bean = (QuerySessionBean) querySession;
        } else {  // if(querySession instanceof QuerySession)
            // ????
            log.warning("querySession is not an instance of QuerySessionBean.");
            bean = new QuerySessionBean(querySession.getGuid(), querySession.getUser(), querySession.getDataService(), querySession.getServiceUrl(), querySession.getInputFormat(), querySession.getOutputFormat(), (ReferrerInfoStructBean) querySession.getReferrerInfo(), querySession.getStatus(), querySession.getNote());
        }
        Boolean suc = getProxyFactory().getQuerySessionServiceProxy().deleteQuerySession(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getQuerySessionServiceProxy().deleteQuerySessions(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createQuerySessions(List<QuerySession> querySessions) throws BaseException
    {
        log.finer("BEGIN");

        if(querySessions == null) {
            log.log(Level.WARNING, "createQuerySessions() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = querySessions.size();
        if(size == 0) {
            log.log(Level.WARNING, "createQuerySessions() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(QuerySession querySession : querySessions) {
            String guid = createQuerySession(querySession);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createQuerySessions() failed for at least one querySession. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateQuerySessions(List<QuerySession> querySessions) throws BaseException
    //{
    //}

}
