package com.queryclient.af.auth.common;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * This is used to represent the current "auth state" across multiple identity providers.
 * This will be primarily used for "caching" (e.g., as a session variable, etc.).
 * Note that the existence of the provider ID in this object (even without the userId, etc.) means 
 *      that the user is currently authenticated on that provider.
 * Supports multiple userIds for a given provider id (for the same user) 
 * Each userId may be associated with a rolemask:long, and other attributes.
 *      (The fact that certain field values are missing in this object does not mean the values do not exist (e.g. in the DB, etc.).
 *       It may be due to security reasons, or it may have been inconvenient to populate those values.)
 * Note: The only reason why it extends from AuthToken is for backward compatibility.
 *       AuthToken has been deprecated in favor of this new class.
 *       AuthToken will be deleted eventually.
 */
public final class AuthState extends AuthToken implements Serializable
{
    private static final Logger log = Logger.getLogger(AuthState.class.getName());
    private static final long serialVersionUID = 1L;

    // Attribute field definitions:
    //     Fields are ordered (in the string representation).
    // Note that this is extensible, but all newly added fields should be appended at the end
    //     (so that the serialization is backward compatible).
    // TBD: how much information do we want/need to store here????
    private static final String FIELD_USERID = "i";        // String. (Note: We always store this as String even though it's long.)
    private static final String FIELD_USERNAME = "u";      // String. Do we need this extra field??? e.g. Twitter handle, etc.
    private static final String FIELD_USERROLE = "r";      // long. Bit mask.
    private static final String FIELD_AUTHTOKEN = "t";     // String. Access token.
    private static final String FIELD_AUTHSECRET = "s";    // String. Token secret, if any.
    private static final String FIELD_EXPIRATION = "x";    // long. Expiration time of the access token.
    private static final String FIELD_REFRESHED = "f";     // long. Last refreshed time.
    // private static final String FIELD_DISPLAYNAME = "n";   // String.
    // private static final String FIELD_EMAIL = "e";         // String.
    // etc...
    
    // "active" providerId/userId marker
    private static final String ACTIVE_ID_MARKER = "*";
    // ...
    
    // For convenience during parsing...
    private static final String[] FIELDS = new String[]{FIELD_USERID, FIELD_USERNAME, FIELD_USERROLE, FIELD_AUTHTOKEN, FIELD_AUTHSECRET, FIELD_EXPIRATION, FIELD_REFRESHED};
    // ...

    // Map of { provider Id -> Map of { userId -> Map of { field -> value } } }.
    // It used to be a simple map. Now it's so complicated because we want to support multiple userIds (for a given provider) with attributes....
    // The provider Id keys are ordered, for debugging purposes only.
    // UserId are ordered. The first (or, first inserted) userId is the "primary" userId.
    // Field attrs are ordered to preserve the string representation.
    // Note 1: userId can be null/empty, but if so, we can only support one entry for the given provider Id. And, the field-value list cannot be included.
    // Note 2: we use Map<String, Map<String, Map<String,String>>> not Map<String, Map<String, Map<String,Object>>>.
    private Map<String, Map<String, Map<String,String>>> authMap = new LinkedHashMap<>();
    // "Cached" string representation.
    // This needs to be invalidated every time the data changes.
    // Note that this may not really work since the client can modify the returned partial map without going through the API.
    private transient String authStateStr = null;
    
    // Currently "active" providerId and currently active userId for each providerId.
    // Note that these values are to be dynamically set/reset as well as based on user input.
    private String activeProviderId = null;
    private Map<String,String> activeUserIds = new HashMap<>();
    
    
    // Some general comments:
    // Methods that take only providerId which otherwise would have taken (providerId, userId)
    // have slightly different semantics depending on the methods.
    // (TBD: It's a bit confusing, and we need a better way to do this.)
    // In some methods, the currently active userId is used.
    // In some methods, it means for all userIds for the given providerId.
    // In some methods, it means for any userId for the given providerId.
    // ....

    
    // Ctor's.
    public AuthState()
    {
        this(null);
    }
    public AuthState(String authStateStr)
    {
        this.authStateStr = authStateStr;
        activeProviderId = parseAuthStateString(authStateStr, authMap, activeUserIds);
    }
    
    
    // authStateStr is a comma-separated list of a pair, providerId:values,
    //      where values is a concatenated string of field values: u;r;t;s;x;f;...
    // providerId and (optional) values are separated by a colon (:).
    // Each provider can be associated with multiple values, separated by "|".
    //      (Each value must start with non-null/empty userId.)
    // Each field is separated by a semi-colon (;).
    // e.g., "twitter:ladygaga;010,facebook:12345", etc.
    // Note that all field values are stored as String.
    // TBD: Just use JSON format ???
    // authMap and activeUserIdMap are in-out params.
    // It returns activeProviderId.
    private static String parseAuthStateString(String authStateStr,
            Map<String, Map<String, Map<String,String>>> authMap, Map<String,String> activeUserIdMap)
    {
        // Map<String, Map<String, Map<String,String>>> authMap = new LinkedHashMap<>();
        // authMap and activeUserIdMap cannot be null.
        authMap.clear();
        activeUserIdMap.clear();
        String activeProviderId = null;
        if(authStateStr != null && !authStateStr.isEmpty()) {
            String[] pairs = authStateStr.split("\\s*,\\s*");
            if(pairs != null && pairs.length > 0) {
                for(String pair : pairs) {
                    String[] keyValue = pair.split("\\s*:\\s*", 2);
                    if(keyValue != null && keyValue.length > 0) {
                        String key = keyValue[0];   // key==providerId cannot be null/empty.
                        if(key.endsWith(ACTIVE_ID_MARKER)) {
                            key = key.substring(0, key.length() - 1);
                            if(activeProviderId == null) {
                                // Only for the first one.
                                // Later ones are ignored.
                                activeProviderId = key;
                            }
                        }
                        String fields = null;
                        if(keyValue.length > 1) {
                            fields = keyValue[1];
                        }
                        if(fields == null || fields.isEmpty()) {
                            authMap.put(key, new LinkedHashMap<String, Map<String,String>>());  // we use an empty map instead of null, to distinguish return values of remove()...
                        } else {
                            Map<String, Map<String,String>> userIdMap = new LinkedHashMap<>();
                            String[] fieldArr = fields.split("\\*|\\*");   // arbitrary length
                            for(String attrs : fieldArr) {
                                String[] multiplet = attrs.split("\\*;\\*", FIELDS.length);   // up to FIELDS.length.
                                if(multiplet != null && multiplet.length > 0) {
                                    String u = multiplet[0];     // u==userId cannot be normally null/empty.
                                    if(u == null || u.isEmpty()) {
                                        break;
                                    }
                                    if(u.endsWith(ACTIVE_ID_MARKER)) {
                                        u = u.substring(0, u.length()-1);
                                        if(activeUserIdMap.get(key) == null) {
                                            // Only for the first one for the given key==providerId.
                                            // Later ones are ignored.
                                            activeUserIdMap.put(key, u);
                                        }
                                    }
                                    Map<String,String> attrMap = new LinkedHashMap<>();
                                    for(int i=1; i<multiplet.length; i++) {
                                        String v = multiplet[i];
                                        // if(v == null) {   // Can this happen?
                                        //     v = "";
                                        // }
                                        attrMap.put(FIELDS[i], v);
                                    }
                                    userIdMap.put(u, attrMap);
                                }
                            }
                            authMap.put(key,  userIdMap);
                        }
                    }
                }
            }
        }
        return activeProviderId;
    }

    // Note that this does not initialize the currently active providerId/userIds info.
    public static AuthState valueOf(String authStateStr)
    {
        AuthState authState = new AuthState(authStateStr);
        return authState;
    }
    
    
    public String getActiveProviderId()
    {
        if(activeProviderId != null) {
            if(! authMap.keySet().contains(activeProviderId)) {
                if(log.isLoggable(Level.INFO)) log.info("activeProviderId, " + activeProviderId + ", is no longer valid.");
                activeProviderId = null;
            }
        }
        if(activeProviderId == null) {
            if(! getProviderIds().isEmpty()) {
                activeProviderId = getProviderIds().get(0);   // Just use the first element.
                if(log.isLoggable(Level.FINE)) log.fine("activeProviderId set to " + activeProviderId);
                // Invalidate the "cache" string.
                this.authStateStr = null;
            }
        }
        return activeProviderId;
    }
    public String setActiveProviderId(String activeProviderId)
    {
        if(activeProviderId == null || ! authMap.keySet().contains(activeProviderId)) {
            if(log.isLoggable(Level.INFO)) log.info("Invalid input activeProviderId, " + activeProviderId + ", is ignored.");
        } else {
            this.activeProviderId = activeProviderId;
            // Invalidate the "cache" string.
            this.authStateStr = null;
        }
        return this.activeProviderId;
    }

    public String getActiveUserId(String providerId)
    {
        if(providerId == null || providerId.isEmpty()) {
            return null;
        }
        String activeUserId = activeUserIds.get(providerId);
        if(activeUserId != null) {
            Map<String,Map<String,String>> userIdMap = authMap.get(providerId);
            if(userIdMap == null || ! userIdMap.keySet().contains(activeUserId)) {
                if(log.isLoggable(Level.INFO)) log.info("activeUserId, " + activeUserId + ", is no longer valid for providerId = " + providerId);
                activeUserId = null;
            }
        }
        if(activeUserId == null) {
            if(! getUserIds(providerId).isEmpty()) {
                activeUserId = getUserIds(providerId).get(0);  // Just use the first element.
                activeUserIds.put(providerId, activeUserId);
                if(log.isLoggable(Level.FINE)) log.fine("activeUserId set to " + activeUserId + " for providerId = " + providerId);
                // Invalidate the "cache" string.
                this.authStateStr = null;
            }
        }
        return activeUserId;
    }
    public String setActiveUserId(String providerId, String activeUserId)
    {
        if(providerId == null || providerId.isEmpty()) {
            return null;
        }
        if(activeUserId == null || activeUserId.isEmpty()) {
            if(log.isLoggable(Level.INFO)) log.info("Invalid input activeUserId, " + activeUserId + ", is ignored.");
            return activeUserIds.get(providerId);
        }
        Map<String,Map<String,String>> userIdMap = authMap.get(providerId);
        if(userIdMap == null || ! userIdMap.keySet().contains(activeUserId)) {
            if(log.isLoggable(Level.INFO)) log.info("Invalid input activeUserId, " + activeUserId + ", is ignored.");
        } else {
            activeUserIds.put(providerId, activeUserId);
            // Invalidate the "cache" string.
            this.authStateStr = null;
        }
        return activeUserIds.get(providerId);
    }

    public String[] getActiveProviderIdAndUserId()
    {
        String pid = getActiveProviderId();
        if(pid == null) {
            return null;
        }
        String uname = getActiveUserId(pid);
        return new String[]{pid, uname};        
    }
    public String[] setActiveProviderIdAndUserId(String activeProviderId, String activeUserId)
    {
        activeProviderId = setActiveProviderId(activeProviderId);
        activeUserId = setActiveUserId(activeProviderId, activeUserId);
        return new String[]{activeProviderId, activeUserId};        
    }


    /**
     * Returns all providerIds on which the user is currently authenticated/logged-in.
     */
    public List<String> getProviderIds()
    {
        // TBD: Does this preserve the ordering??? (because we used LinkedHashMap?)
        Set<String> keys = authMap.keySet();
        return new ArrayList<>(keys);
    }    


    // Note the interesting "chaining" here...
    @Override
    public boolean isAuthenticated()
    {
        if(! authMap.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    public boolean isAuthenticated(String providerId)
    {
        if(providerId == null) {
            return isAuthenticated();
        }
        if(authMap.containsKey(providerId)) {
            return true;
        } else {
            return false;
        }
    }
    public boolean isAuthenticated(String providerId, String userId)
    {
        if(providerId == null || userId == null) {
            return isAuthenticated(providerId);
        }
        Map<String,Map<String,String>> userIdMap = authMap.get(providerId);
        if(userIdMap == null) {
            return false;
        } else {
            if(userIdMap.containsKey(userId)) {
                return true;
            } else {
                return false;
            }
        }
    }


    /**
     * Returns the first/primary userId authenticated on providerId.
     */
    public String getUserId(String providerId)
    {
//        List<String> userIds = getUserIds(providerId);
//        if(userIds != null && !userIds.isEmpty()) {
//            return userIds.get(0);   // The first userId is the "primary" one.
//        }
//        return null;
        return getActiveUserId(providerId);
    }
    /**
     * Returns all userIds authenticated on providerId.
     */
    public List<String> getUserIds(String providerId)
    {
        if(providerId == null) {
            return null;
        }
        Map<String,Map<String,String>> userIdMap = authMap.get(providerId);
        if(userIdMap != null) {
            // TBD: Does this preserve the ordering??? (because we used LinkedHashMap?)
            Set<String> keys = userIdMap.keySet();
            return new ArrayList<>(keys);
        }
        return null;
    }    


    // private: Prevents the client from modifying the internal data.
    private Map<String,String> getAttributeMap(String providerId, String userId)
    {
        if((providerId == null || providerId.isEmpty()) || (userId == null || userId.isEmpty())) {
            return null;
        }
        Map<String, Map<String,String>> userIdMap = authMap.get(providerId);
        if(userIdMap == null || userIdMap.isEmpty()) {
            return null;
        }
        return userIdMap.get(userId);
    }
    // This is public.
    public String[] getAttributes(String providerId, String userId)
    {
        Map<String,String> attrMap = getAttributeMap(providerId, userId);
        String[] oldAttrList = convertAttrMapToArray(attrMap);
        return oldAttrList;
    }


    /**
     * Returns the username for the first/primary/active userId authenticated on providerId.
     */
    @Override
    public String getUsername(String providerId)
    {
        String userId = getUserId(providerId);
        return getUsername(providerId, userId);
    }
    /**
     * Returns the auth/access token for the authenticated userId on providerId.
     */
    public String getUsername(String providerId, String userId)
    {
        Map<String,String> attrMap = getAttributeMap(providerId, userId);
        if(attrMap == null) {
            return null;
        }
        return attrMap.get(FIELD_USERNAME);
    }

    /**
     * Returns the user role for the first/primary userId authenticated on providerId.
     */
    public long getUserRole(String providerId)
    {
        String userId = getUserId(providerId);
        return getUserRole(providerId, userId);
    }
    /**
     * Returns the user role for the authenticated userId on providerId.
     */
    public long getUserRole(String providerId, String userId)
    {
        Map<String,String> attrMap = getAttributeMap(providerId, userId);
        if(attrMap == null) {
            return 0L;
        }
        String r = attrMap.get(FIELD_USERROLE);
        if(r == null) {
            return 0L;
        } else {
            try {
                Long role = Long.parseLong(r);
                return role;
            } catch(Exception e) {
                // ignore
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse the role: " + r, e);
            }
        }
        return 0L;
    }
    
    /**
     * Returns the auth/access token for the first/primary userId authenticated on providerId.
     */
    public String getAuthToken(String providerId)
    {
        String userId = getUserId(providerId);
        return getAuthToken(providerId, userId);
    }
    /**
     * Returns the auth/access token for the authenticated userId on providerId.
     */
    public String getAuthToken(String providerId, String userId)
    {
        Map<String,String> attrMap = getAttributeMap(providerId, userId);
        if(attrMap == null) {
            return null;
        }
        return attrMap.get(FIELD_AUTHTOKEN);
    }

    /**
     * Returns the auth secret for the first/primary userId authenticated on providerId.
     */
    public String getAuthSecret(String providerId)
    {
        String userId = getUserId(providerId);
        return getAuthSecret(providerId, userId);
    }
    /**
     * Returns the auth secret for the authenticated userId on providerId.
     */
    public String getAuthSecret(String providerId, String userId)
    {
        Map<String,String> attrMap = getAttributeMap(providerId, userId);
        if(attrMap == null) {
            return null;
        }
        return attrMap.get(FIELD_AUTHSECRET);
    }
    
    /**
     * Returns the auth token expiration time for the first/primary userId authenticated on providerId.
     */
    public long getExpirationTime(String providerId)
    {
        String userId = getUserId(providerId);
        return getExpirationTime(providerId, userId);
    }
    /**
     * Returns the auth token expiration time for the authenticated userId on providerId.
     */
    public long getExpirationTime(String providerId, String userId)
    {
        Map<String,String> attrMap = getAttributeMap(providerId, userId);
        if(attrMap == null) {
            return 0L;
        }
        String x = attrMap.get(FIELD_EXPIRATION);
        if(x == null) {
            return 0L;
        } else {
            try {
                Long expirationTime = Long.parseLong(x);
                return expirationTime;
            } catch(Exception e) {
                // ignore
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse the expirationTime: " + x, e);
            }
        }
        return 0L;
    }

    /**
     * Returns the auth token refreshed time for the first/primary userId authenticated on providerId.
     */
    public long getRefreshedTime(String providerId)
    {
        String userId = getUserId(providerId);
        return getRefreshedTime(providerId, userId);
    }
    /**
     * Returns the auth token refreshed time for the authenticated userId on providerId.
     */
    public long getRefreshedTime(String providerId, String userId)
    {
        Map<String,String> attrMap = getAttributeMap(providerId, userId);
        if(attrMap == null) {
            return 0L;
        }
        String f = attrMap.get(FIELD_REFRESHED);
        if(f == null) {
            return 0L;
        } else {
            try {
                Long refreshedTime = Long.parseLong(f);
                return refreshedTime;
            } catch(Exception e) {
                // ignore
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse the refreshedTime: " + f, e);
            }
        }
        return 0L;
    }


    /**
     * Add or update the auth state for the given providerId and userId.
     * attrs is an array: [ user role, auth token, auth secret, expiration time, last refreshed time ].
     * Returns the last attrs associated with providerId/userId, if any.
     */
    public String[] add(String providerId, String userId, String... attrs)
    {
        Map<String,String> attrMap = convertAttrArrayToMap(attrs);
        Map<String,String> oldAttrMap = addAuthState(providerId, userId, attrMap);
        String[] oldAttrList = convertAttrMapToArray(oldAttrMap);
        return oldAttrList;
    }
    private Map<String,String> addAuthState(String providerId, String userId, Map<String,String> attrs)
    {
        Map<String,String> oldAttrs = null;        
        if(userId == null || userId.isEmpty()) {
            if(authMap.containsKey(providerId)) {
                log.warning("Null/empty userId cannot be added.");
            } else {
                // We allow empty userId only for the first/only entry for the given provider.
                // And, attrs are ignored.
                log.info("For null/empty userId, attrs are ignored.");
                authMap.put(providerId, new LinkedHashMap<String, Map<String,String>>());  // we use an empty map instead of null, to distinguish return values of remove()...
            }
        } else {
            Map<String, Map<String,String>> userIdMap = null;
            if(authMap.containsKey(providerId)) {
                userIdMap = authMap.get(providerId);   // Could be null.
            }
            if(userIdMap == null) {
                userIdMap = new LinkedHashMap<>();
            }
            oldAttrs = userIdMap.put(userId, attrs);
            authMap.put(providerId, userIdMap);
        }
        // Invalidate the "cache" string.
        this.authStateStr = null;
        return oldAttrs;
    }
    // userId is optional.
    @Override
    @Deprecated
    public void addAuthState(String providerId, String userId)
    {
        add(providerId, userId);
    }
    public String[] add(String providerId, String userId)
    {
        return add(providerId, userId, (String[]) null);
    }

    /**
     * Removes the auth state for the given providerId, and userId.
     * It returns the old auth attributes for the given providerId/userId, if any.
     */
    public String[] remove(String providerId, String userId)
    {
        Map<String,String> oldAttrMap = removeAuthState(providerId, userId);
        String[] oldAttrList = convertAttrMapToArray(oldAttrMap);
        return oldAttrList;
    }
    private Map<String,String> removeAuthState(String providerId, String userId)
    {
        if(providerId == null) {
            return null;
        }
        Map<String,String> oldAttrs = null;        
        if(userId == null || userId.isEmpty()) {
            remove(providerId);       //  ?????
        } else if(! authMap.containsKey(providerId)) {
            // Nothing to do.
        } else {
            Map<String, Map<String,String>> userIdMap = authMap.get(providerId);
            if(userIdMap == null) {
                authMap.remove(providerId);
                if(providerId.equals(this.activeProviderId)) {
                    this.activeProviderId = null;
                    this.activeUserIds.remove(this.activeProviderId);
                }
            } else {
                oldAttrs = userIdMap.remove(userId);
                if(userId.equals(this.activeUserIds.get(providerId))) {
                    this.activeUserIds.remove(providerId);
                }
            }
            // Invalidate the "cache" string.
            this.authStateStr = null;
        }
        return oldAttrs;
    }

    // The return value of null means that the providerId did not exist in the authStateStr.
    // whereas the return value of an empty string means the providerId was in the authStateStr (without a specific userId).
    @Override
    @Deprecated
    public String removeAuthState(String providerId)
    {
        List<String> userIds = remove(providerId);
        if(userIds == null) {
            return null;
        } else {
            return userIds.get(0);   // ???
        }
    }
    // Note: the return value of an empty list and null are different.
    public List<String> remove(String providerId)
    {
        if(providerId == null) {
            return null;
        }
        Map<String, Map<String,String>> userIdMap = authMap.remove(providerId);
        if(providerId.equals(this.activeProviderId)) {
            this.activeProviderId = null;
            this.activeUserIds.remove(this.activeProviderId);
        }
        if(userIdMap == null) {
            return null;
        }
        List<String> userIds = new ArrayList<>();
        if(! userIdMap.isEmpty()) {
            // TBD: Does this preserve the ordering??? (because we used LinkedHashMap?)
            Set<String> keys = userIdMap.keySet();
            userIds = new ArrayList<>(keys);
        }
        // Invalidate the "cache" string.
        this.authStateStr = null;
        return userIds;
    }

    public void clear(String providerId)
    {
        remove(providerId);
    }
    public void clear()
    {
        authMap.clear();
        // Invalidate the "cache" string.
        this.authStateStr = null;
    }
    @Override
    @Deprecated
    public void clearAllAuthState()
    {
        clear();
    }    
    
    

    // External API uses String[].
    // Internal API uses attr map.
    private String[] convertAttrMapToArray(Map<String,String> attrMap)
    {
        String[] attArr = new String[FIELDS.length];
        if(attrMap != null) {
            String u = attrMap.get(FIELD_USERNAME);
            attArr[0] = ((u == null) ? "" : u);
            String r = attrMap.get(FIELD_USERROLE);
            attArr[0] = ((r == null) ? "" : r);
            String t = attrMap.get(FIELD_AUTHTOKEN);
            attArr[1] = ((t == null) ? "" : t);
            String s = attrMap.get(FIELD_AUTHSECRET);
            attArr[2] = ((s == null) ? "" : s);
            String x = attrMap.get(FIELD_EXPIRATION);
            attArr[3] = ((x == null) ? "" : x);
            String f = attrMap.get(FIELD_REFRESHED);
            attArr[4] = ((f == null) ? "" : f);
            // ...
        }
        return attArr;
    }
    private Map<String,String> convertAttrArrayToMap(String[] attrArr)
    {
        Map<String,String> attrMap = null;
        if(attrArr != null) {
            attrMap = new LinkedHashMap<>();
            // attrs does not include the first userId field.
            int sz = (FIELDS.length - 1 < attrArr.length) ? FIELDS.length - 1 : attrArr.length;
            for(int i=0; i<sz; i++) {
                attrMap.put(FIELDS[i+1], attrArr[i]);   // Note the "i+1".
            }
        }
        return attrMap;
    }


    // Note: The string form serialization loses some information, e.g., regarding the currently active providerId/userIds.
    //       Need to fix this...

    // This is used to serialize the object to a string.
    // AuthState.valueOf(obj.toString()) == obj.
    @Override
    public String toString()
    {
        if(this.authStateStr == null) {
            if(authMap == null || authMap.isEmpty()) {
                return "";
            }
            StringBuilder sb = new StringBuilder();
            for(String pid : authMap.keySet()) {
                if(pid == null) {
                    pid = "";  // ???
                }
                if(pid.equals(activeProviderId)) {
                    sb.append(pid).append(ACTIVE_ID_MARKER).append(":");
                } else {
                    sb.append(pid).append(":");
                }
                Map<String, Map<String,String>> userIdMap = authMap.get(pid);
                if(userIdMap != null && !userIdMap.isEmpty()) {
                    for(String uid : userIdMap.keySet()) {
                        if(uid == null) {
                            uid = "";  // ???
                        }
                        if(uid.equals(activeUserIds.get(pid))) {
                            sb.append(uid).append(ACTIVE_ID_MARKER).append(";");
                        } else {
                            sb.append(uid).append(";");
                        }
                        Map<String,String> attrs = userIdMap.get(uid);
                        if(attrs != null && !attrs.isEmpty()) {
                            // We do this in order to use the particular ordering.
                            //   instead of doing the loop, relying on the LinkedHashMap ordering.
                            String u = attrs.get(FIELD_USERNAME);
                            sb.append((u == null) ? "" : u).append(";");
                            String r = attrs.get(FIELD_USERROLE);
                            sb.append((r == null) ? "" : r).append(";");
                            String t = attrs.get(FIELD_AUTHTOKEN);
                            sb.append((t == null) ? "" : t).append(";");
                            String s = attrs.get(FIELD_AUTHSECRET);
                            sb.append((s == null) ? "" : s).append(";");
                            String x = attrs.get(FIELD_EXPIRATION);
                            sb.append((x == null) ? "" : x).append(";");
                            String f = attrs.get(FIELD_REFRESHED);
                            sb.append((f == null) ? "" : f);   // No trailing ";".
                        }
                        sb.append("|");
                    }
                    // if(sb.charAt(sb.length()-1) == '|') {
                        sb.deleteCharAt(sb.length()-1);  // Remove the last "|";
                    // }
                }
                sb.append(",");
            }
            // if(sb.charAt(sb.length()-1) == ',') {
                sb.deleteCharAt(sb.length()-1);  // Remove the last ",";
            // }
            this.authStateStr = sb.toString();
        }
        return this.authStateStr;
    }


    // temporary

    private void writeObject(java.io.ObjectOutputStream out)
            throws IOException
    {
        out.writeUTF(toString());
    }
    
    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException
    {
        String authStateStr = in.readUTF();
        if(authMap == null) {
            authMap = new LinkedHashMap<>();
        }
        if(activeUserIds == null) {
            activeUserIds = new HashMap<>();
        }
        activeProviderId = parseAuthStateString(authStateStr, authMap, activeUserIds);
    }        

        
}
