package com.queryclient.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.af.bean.ReferrerInfoStructBean;


public class ReferrerInfoStructResourceUtil
{
    private static final Logger log = Logger.getLogger(ReferrerInfoStructResourceUtil.class.getName());

    // Static methods only.
    private ReferrerInfoStructResourceUtil() {}

    public static ReferrerInfoStructBean convertReferrerInfoStructStubToBean(ReferrerInfoStruct stub)
    {
        ReferrerInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new ReferrerInfoStructBean();
            bean.setReferer(stub.getReferer());
            bean.setUserAgent(stub.getUserAgent());
            bean.setLanguage(stub.getLanguage());
            bean.setHostname(stub.getHostname());
            bean.setIpAddress(stub.getIpAddress());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
