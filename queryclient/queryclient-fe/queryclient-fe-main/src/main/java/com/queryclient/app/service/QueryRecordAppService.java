package com.queryclient.app.service;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.DeferredTask;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.service.impl.QueryRecordServiceImpl;
import com.queryclient.app.util.QueryResultStruct;
import com.queryclient.app.util.QueryTask;
import com.queryclient.app.util.QueryUtil;
import com.queryclient.util.parser.QueryObject;
import com.queryclient.util.parser.QueryParser;
import com.queryclient.util.parser.RequestBuilder;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.exception.BadRequestException;


// Updated.
public class QueryRecordAppService extends QueryRecordServiceImpl implements QueryRecordService
{
    private static final Logger log = Logger.getLogger(QueryRecordAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();
    
    // Max entity size on GAE data store/memcache is 1M.
    private static final int MAX_OUTPUT_LENGTH = 450000;  // ???
    

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public QueryRecordAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // QueryRecord related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public QueryRecord getQueryRecord(String guid) throws BaseException
    {
        return super.getQueryRecord(guid);
    }

    @Override
    public Object getQueryRecord(String guid, String field) throws BaseException
    {
        return super.getQueryRecord(guid, field);
    }
    
    @Override
    public List<QueryRecord> getAllQueryRecords() throws BaseException
    {
        return super.getAllQueryRecords();
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        // TBD:
        queryRecord = processQuery(queryRecord);
        queryRecord = sanitizeQueryRecord(queryRecord);
        // ...

        String guid = super.createQueryRecord(queryRecord);
        return guid;
    }

    @Override
    public QueryRecord constructQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        // TBD:
        queryRecord = processQuery(queryRecord);
        queryRecord = sanitizeQueryRecord(queryRecord);
        // ...

        queryRecord = super.constructQueryRecord(queryRecord);
        return queryRecord;
    }


    @Override
    public Boolean updateQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        // TBD:
        queryRecord = sanitizeQueryRecord(queryRecord);
        // ...
        return super.updateQueryRecord(queryRecord);
    }
        
    @Override
    public QueryRecord refreshQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        // TBD:
        queryRecord = sanitizeQueryRecord(queryRecord);
        // ...
        return super.refreshQueryRecord(queryRecord);
    }

    @Override
    public Boolean deleteQueryRecord(String guid) throws BaseException
    {
        return super.deleteQueryRecord(guid);
    }

    @Override
    public Boolean deleteQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        return super.deleteQueryRecord(queryRecord);
    }

    @Override
    public Integer createQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    {
        return super.createQueryRecords(queryRecords);
    }

    // TBD
    //@Override
    //public Boolean updateQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    //{
    //}

    
    private QueryRecordBean processQuery(QueryRecord queryRecord) throws BaseException
    {
        log.fine("ENTERING: processQuery()");

        QueryRecordBean bean = null;
        if(queryRecord instanceof QueryRecordBean) {
            bean = (QueryRecordBean) queryRecord;
        } else {
            // ????? Can this happen????
            bean = new QueryRecordBean(queryRecord);
        }
        
        
        // TBD:
        // validation???
        
        
        // TBD:
        // Check "delayed" field first...
        boolean delayed = false;
        if(Boolean.TRUE.equals(bean.isDelayed())) {
            delayed = true;
        }
        // ...
        
        
        // Process query
        
        String queryGuid = bean.getGuid();
        
        String serviceUrl = bean.getServiceUrl();
        if(serviceUrl == null || serviceUrl.isEmpty()) {
            // TBD: Get it from dataService or from querySession???
            // ..
            throw new BadRequestException("serviceUrl cannot be empty.");
        }
        serviceUrl = serviceUrl.trim();
        
        String queryStmt = bean.getQuery();
        if(queryStmt == null || queryStmt.isEmpty()) {
            throw new BadRequestException("queryStmt cannot be empty.");
        }
        queryStmt = queryStmt.trim();
        log.fine("Parsed: queryStmt = " + queryStmt);
        
        // parse queryStmt
        QueryObject queryObj = null;
        try {
            QueryParser parser = new QueryParser(queryStmt);
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            log.log(Level.WARNING, "Parse failed for queryStmt = " + queryStmt, e);
            throw new BaseException("Parse failed for queryStmt = " + queryStmt, e);
        }
        log.fine("Parsed: queryObj = " + queryObj);


        // TBD:
        // Currently, getting errors when fetching large number of records...
        // Maybe, divide them into multiple batches...
        // and fetch them in chunks???
        // ...

        
        // convert it to web services url
        RequestBuilder reqBuilder = null;
        URL requestUrl = null;
        String httpMethod = null;
        Map<String, String> httpHeaders = null;
        String payload = null;

        try {
            reqBuilder = new RequestBuilder(serviceUrl, queryObj);
            requestUrl = reqBuilder.getRequestUrl();
            httpMethod = reqBuilder.getHttpMethod();
            httpHeaders = reqBuilder.getHttpHeaders();
            payload = reqBuilder.getPayload();
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to build request for queryObj = " + queryObj, e);
            throw new BaseException("Failed to build request for queryObj = " + queryObj, e);
        }
        log.fine("RequestBuilder: requestUrl = " + requestUrl);
        log.fine("RequestBuilder: httpMethod = " + httpMethod);
        log.fine("RequestBuilder: httpHeaders = " + (httpHeaders == null ? 0 : httpHeaders.size()));
        log.fine("RequestBuilder: payload = " + payload);

        
        // ????
        //bean.setInputContent(payload);
        // input formate ???
        
        
        String status = "created";
        int responseCode = 0;
        String result = null; 
        String output = null;
        long processedTime = 0L;
        if(delayed == false) {
            // If delayed==false, Process the request now...
            
            QueryResultStruct res = QueryUtil.processWebServiceQuery(queryGuid, requestUrl, httpMethod, httpHeaders, payload);
            responseCode = res.getResponseCode();
            result = res.getResult();
            output = res.getOutput();
            status = "processed";
            processedTime = System.currentTimeMillis();
            // ...
            
        } else {
            // If delayed==true, create a deferred task..

            DeferredTask task = new QueryTask(queryGuid, requestUrl, httpMethod, httpHeaders, payload);
            log.log(Level.INFO, "Query task created: " + task);
            QueueFactory.getDefaultQueue().add(TaskOptions.Builder.withPayload(task));
            status = "queued";
        }
        
        // Update bean
        bean.setStatus(status);
        bean.setResponseCode(responseCode);
        bean.setResult(result);
        bean.setProcessedTime(processedTime);
        // ...
        if(output != null && !output.isEmpty()) {
            int len = output.length();
            if(len > MAX_OUTPUT_LENGTH) {
                log.info("Output object is too big. Truncating...");
                String truncated = output.substring(0, MAX_OUTPUT_LENGTH) + "...";  // truncate...  Note: the marker "..." will be used in UI to test if the output has been truncated... 
                bean.setOutputContent(truncated);
            } else {
                bean.setOutputContent(output);
            }
        } else {
            // ???
            // ignore...
            log.info("Output is empty.");
        }
        // ...

        log.fine("EXITING: processQuery()");
        return bean;
    }

    private QueryRecordBean sanitizeQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.fine("ENTERING: sanitizeQueryRecord()");

        QueryRecordBean bean = null;
        if(queryRecord instanceof QueryRecordBean) {
            bean = (QueryRecordBean) queryRecord;
        } else {
            // ????? Can this happen????
            bean = new QueryRecordBean(queryRecord);
        }
        // ...
        
/*
        // Truncate query.
        String query = bean.getQuery();
        if(query != null) {
            int queryLen = query.length();
            if(queryLen > 450) {
                query = query.substring(0, 447) + "...";
                bean.setQuery(query);
            }
        }
*/

        log.fine("EXITING: sanitizeQueryRecord()");
        return bean;
    }

}
