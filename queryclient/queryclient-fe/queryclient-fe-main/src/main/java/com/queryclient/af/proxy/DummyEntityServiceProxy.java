package com.queryclient.af.proxy;

import com.queryclient.ws.service.DummyEntityService;

public interface DummyEntityServiceProxy extends DummyEntityService
{

}
