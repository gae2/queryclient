package com.queryclient.af.auth.common;

import java.util.logging.Logger;
import java.util.logging.Level;


// Constants, to be used for CommonAuthUtil.PARAM_USERROLE query param.
// And some utility/convenience methods.
public final class UserRoleParam
{
    private static final Logger log = Logger.getLogger(UserRoleParam.class.getName());

    private UserRoleParam() {}


    ///////////////////////////////////////////
    // Bit field definitions
    // (Note that zero value in a particular field does not mean that particular role is missing.
    //     Role should be always read in view of a filter/mask. )
    // Note: the lowest 8 bits are "reserved" for common use.
    //     App specific bits should start from the 8-th bit (counting from 0).

    // None.
    public static final long ROLE_NONE = 0L;

    // The bit fields are app-specific,
    // but the zero-th field is always reserved for the universal "admin" role.
    public static final long ROLE_ADMIN = 1L;
    
    // etc...


    // Returns true if the role bit includes admin role
    public static boolean isAdmin(long userRole)
    {
        return (ROLE_ADMIN & userRole) != 0L;
    }

    // Turns "on" the admin bit field in the bit mask.
    public static long requireAdmin(long userRoleMask)
    {
        return (ROLE_ADMIN | userRoleMask);
    }


    public static long parseUserRole(String userRoleStr)
    {
        long userRole = 0L;
        try {
            userRole = Long.parseLong(userRoleStr);
        } catch(NumberFormatException e) {
            // Ignore
            if(log.isLoggable(Level.INFO)) log.info("Failed to parse userRole = " + userRoleStr);
        }
        return userRole;
    }

}
