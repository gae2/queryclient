package com.queryclient.cert.af;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseOAuthConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseOAuthConsumerRegistry.class.getName());

    // Consumer key-secret map.
    private Map<String, String> consumerSecretMap;

    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD:
        consumerSecretMap.put("4ee99392-1160-4484-a632-9993d279d934", "ed0437fb-c321-4516-bfc1-322c3cd9ada6");  // QueryClientApp
        consumerSecretMap.put("b8de1e9c-f8fe-4a0f-b006-2f257d3370d4", "ad99eed2-76dd-45e0-a12a-3c8946a4f79a");  // QueryClientApp + QueryClient
        consumerSecretMap.put("de14a111-993b-4060-95ab-3de6d028f3bf", "284e6652-f00c-4957-9e82-55dd292f9561");  // "localhost:8899" + QueryClient (for local debugging)
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
