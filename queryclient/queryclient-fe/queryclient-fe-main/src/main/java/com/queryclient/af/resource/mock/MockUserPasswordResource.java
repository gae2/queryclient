package com.queryclient.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.stub.UserPasswordStub;
import com.queryclient.ws.stub.UserPasswordListStub;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.UserPasswordBean;
import com.queryclient.af.resource.UserPasswordResource;
import com.queryclient.af.resource.util.GaeAppStructResourceUtil;


// MockUserPasswordResource is a decorator.
// It can be used as a base class to mock UserPasswordResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/userPasswords/")
public abstract class MockUserPasswordResource implements UserPasswordResource
{
    private static final Logger log = Logger.getLogger(MockUserPasswordResource.class.getName());

    // MockUserPasswordResource uses the decorator design pattern.
    private UserPasswordResource decoratedResource;

    public MockUserPasswordResource(UserPasswordResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected UserPasswordResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(UserPasswordResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUserPasswords(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findUserPasswordsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findUserPasswordsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getUserPasswordAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getUserPasswordAsHtml(guid);
//    }

    @Override
    public Response getUserPassword(String guid) throws BaseResourceException
    {
        return decoratedResource.getUserPassword(guid);
    }

    @Override
    public Response getUserPasswordAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getUserPasswordAsJsonp(guid, callback);
    }

    @Override
    public Response getUserPassword(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getUserPassword(guid, field);
    }

    // TBD
    @Override
    public Response constructUserPassword(UserPasswordStub userPassword) throws BaseResourceException
    {
        return decoratedResource.constructUserPassword(userPassword);
    }

    @Override
    public Response createUserPassword(UserPasswordStub userPassword) throws BaseResourceException
    {
        return decoratedResource.createUserPassword(userPassword);
    }

//    @Override
//    public Response createUserPassword(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createUserPassword(formParams);
//    }

    // TBD
    @Override
    public Response refreshUserPassword(String guid, UserPasswordStub userPassword) throws BaseResourceException
    {
        return decoratedResource.refreshUserPassword(guid, userPassword);
    }

    @Override
    public Response updateUserPassword(String guid, UserPasswordStub userPassword) throws BaseResourceException
    {
        return decoratedResource.updateUserPassword(guid, userPassword);
    }

    @Override
    public Response updateUserPassword(String guid, String managerApp, Long appAcl, String gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime)
    {
        return decoratedResource.updateUserPassword(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

//    @Override
//    public Response updateUserPassword(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateUserPassword(guid, formParams);
//    }

    @Override
    public Response deleteUserPassword(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteUserPassword(guid);
    }

    @Override
    public Response deleteUserPasswords(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteUserPasswords(filter, params, values);
    }


// TBD ....
    @Override
    public Response createUserPasswords(UserPasswordListStub userPasswords) throws BaseResourceException
    {
        return decoratedResource.createUserPasswords(userPasswords);
    }


}
