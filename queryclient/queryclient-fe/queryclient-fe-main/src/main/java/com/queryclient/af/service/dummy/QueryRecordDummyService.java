package com.queryclient.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.af.config.Config;

import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.QueryRecordService;


// The primary purpose of QueryRecordDummyService is to fake the service api, QueryRecordService.
// It has no real implementation.
public class QueryRecordDummyService implements QueryRecordService
{
    private static final Logger log = Logger.getLogger(QueryRecordDummyService.class.getName());

    public QueryRecordDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // QueryRecord related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public QueryRecord getQueryRecord(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getQueryRecord(): guid = " + guid);
        return null;
    }

    @Override
    public Object getQueryRecord(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getQueryRecord(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<QueryRecord> getQueryRecords(List<String> guids) throws BaseException
    {
        log.fine("getQueryRecords()");
        return null;
    }

    @Override
    public List<QueryRecord> getAllQueryRecords() throws BaseException
    {
        return getAllQueryRecords(null, null, null);
    }


    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecords(ordering, offset, count, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQueryRecords(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQueryRecordKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QueryRecordDummyService.findQueryRecords(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QueryRecordDummyService.findQueryRecordKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QueryRecordDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createQueryRecord(String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        log.finer("createQueryRecord()");
        return null;
    }

    @Override
    public String createQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("createQueryRecord()");
        return null;
    }

    @Override
    public QueryRecord constructQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("constructQueryRecord()");
        return null;
    }

    @Override
    public Boolean updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        log.finer("updateQueryRecord()");
        return null;
    }
        
    @Override
    public Boolean updateQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("updateQueryRecord()");
        return null;
    }

    @Override
    public QueryRecord refreshQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("refreshQueryRecord()");
        return null;
    }

    @Override
    public Boolean deleteQueryRecord(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteQueryRecord(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("deleteQueryRecord()");
        return null;
    }

    // TBD
    @Override
    public Long deleteQueryRecords(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteQueryRecord(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    {
        log.finer("createQueryRecords()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    //{
    //}

}
