package com.queryclient.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.service.impl.QuerySessionServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class QuerySessionProtoService extends QuerySessionServiceImpl implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(QuerySessionProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public QuerySessionProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // QuerySession related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        return super.getQuerySession(guid);
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        return super.getQuerySession(guid, field);
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        return super.getQuerySessions(guids);
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return super.getAllQuerySessions();
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllQuerySessions(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createQuerySession(QuerySession querySession) throws BaseException
    {
        return super.createQuerySession(querySession);
    }

    @Override
    public QuerySession constructQuerySession(QuerySession querySession) throws BaseException
    {
        return super.constructQuerySession(querySession);
    }


    @Override
    public Boolean updateQuerySession(QuerySession querySession) throws BaseException
    {
        return super.updateQuerySession(querySession);
    }
        
    @Override
    public QuerySession refreshQuerySession(QuerySession querySession) throws BaseException
    {
        return super.refreshQuerySession(querySession);
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        return super.deleteQuerySession(guid);
    }

    @Override
    public Boolean deleteQuerySession(QuerySession querySession) throws BaseException
    {
        return super.deleteQuerySession(querySession);
    }

    @Override
    public Integer createQuerySessions(List<QuerySession> querySessions) throws BaseException
    {
        return super.createQuerySessions(querySessions);
    }

    // TBD
    //@Override
    //public Boolean updateQuerySessions(List<QuerySession> querySessions) throws BaseException
    //{
    //}

}
