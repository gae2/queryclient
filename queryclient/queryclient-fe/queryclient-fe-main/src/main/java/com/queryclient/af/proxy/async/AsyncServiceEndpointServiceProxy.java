package com.queryclient.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairListStub;
import com.queryclient.ws.stub.ServiceEndpointStub;
import com.queryclient.ws.stub.ServiceEndpointListStub;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.ws.service.ServiceEndpointService;
import com.queryclient.af.proxy.ServiceEndpointServiceProxy;
import com.queryclient.af.proxy.remote.RemoteServiceEndpointServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncServiceEndpointServiceProxy extends BaseAsyncServiceProxy implements ServiceEndpointServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncServiceEndpointServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteServiceEndpointServiceProxy remoteProxy;

    public AsyncServiceEndpointServiceProxy()
    {
        remoteProxy = new RemoteServiceEndpointServiceProxy();
    }

    @Override
    public ServiceEndpoint getServiceEndpoint(String guid) throws BaseException
    {
        return remoteProxy.getServiceEndpoint(guid);
    }

    @Override
    public Object getServiceEndpoint(String guid, String field) throws BaseException
    {
        return remoteProxy.getServiceEndpoint(guid, field);       
    }

    @Override
    public List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException
    {
        return remoteProxy.getServiceEndpoints(guids);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException
    {
        return getAllServiceEndpoints(null, null, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllServiceEndpoints(ordering, offset, count);
        return getAllServiceEndpoints(ordering, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllServiceEndpoints(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllServiceEndpointKeys(ordering, offset, count);
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count);
        return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createServiceEndpoint(String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        ServiceEndpointBean bean = new ServiceEndpointBean(null, user, dataService, serviceName, serviceUrl, authRequired, MarshalHelper.convertConsumerKeySecretPairToBean(authCredential), status);
        return createServiceEndpoint(bean);        
    }

    @Override
    public String createServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");

        String guid = serviceEndpoint.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((ServiceEndpointBean) serviceEndpoint).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateServiceEndpoint-" + guid;
        String taskName = "RsCreateServiceEndpoint-" + guid + "-" + (new Date()).getTime();
        ServiceEndpointStub stub = MarshalHelper.convertServiceEndpointToStub(serviceEndpoint);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ServiceEndpointStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = serviceEndpoint.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ServiceEndpointStub dummyStub = new ServiceEndpointStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createServiceEndpoint(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "serviceEndpoints/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createServiceEndpoint(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "serviceEndpoints/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ServiceEndpoint guid is invalid.");
        	throw new BaseException("ServiceEndpoint guid is invalid.");
        }
        ServiceEndpointBean bean = new ServiceEndpointBean(guid, user, dataService, serviceName, serviceUrl, authRequired, MarshalHelper.convertConsumerKeySecretPairToBean(authCredential), status);
        return updateServiceEndpoint(bean);        
    }

    @Override
    public Boolean updateServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        log.finer("BEGIN");

        String guid = serviceEndpoint.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ServiceEndpoint object is invalid.");
        	throw new BaseException("ServiceEndpoint object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateServiceEndpoint-" + guid;
        String taskName = "RsUpdateServiceEndpoint-" + guid + "-" + (new Date()).getTime();
        ServiceEndpointStub stub = MarshalHelper.convertServiceEndpointToStub(serviceEndpoint);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ServiceEndpointStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = serviceEndpoint.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ServiceEndpointStub dummyStub = new ServiceEndpointStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateServiceEndpoint(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "serviceEndpoints/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateServiceEndpoint(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "serviceEndpoints/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteServiceEndpoint-" + guid;
        String taskName = "RsDeleteServiceEndpoint-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "serviceEndpoints/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        String guid = serviceEndpoint.getGuid();
        return deleteServiceEndpoint(guid);
    }

    @Override
    public Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteServiceEndpoints(filter, params, values);
    }

}
