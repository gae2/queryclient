package com.queryclient.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.af.config.Config;

import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.QuerySessionService;


// The primary purpose of QuerySessionDummyService is to fake the service api, QuerySessionService.
// It has no real implementation.
public class QuerySessionDummyService implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(QuerySessionDummyService.class.getName());

    public QuerySessionDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // QuerySession related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getQuerySession(): guid = " + guid);
        return null;
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getQuerySession(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        log.fine("getQuerySessions()");
        return null;
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return getAllQuerySessions(null, null, null);
    }


    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQuerySessions(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQuerySessionKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QuerySessionDummyService.findQuerySessions(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QuerySessionDummyService.findQuerySessionKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QuerySessionDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        log.finer("createQuerySession()");
        return null;
    }

    @Override
    public String createQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("createQuerySession()");
        return null;
    }

    @Override
    public QuerySession constructQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("constructQuerySession()");
        return null;
    }

    @Override
    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        log.finer("updateQuerySession()");
        return null;
    }
        
    @Override
    public Boolean updateQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("updateQuerySession()");
        return null;
    }

    @Override
    public QuerySession refreshQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("refreshQuerySession()");
        return null;
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteQuerySession(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteQuerySession(QuerySession querySession) throws BaseException
    {
        log.finer("deleteQuerySession()");
        return null;
    }

    // TBD
    @Override
    public Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteQuerySession(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createQuerySessions(List<QuerySession> querySessions) throws BaseException
    {
        log.finer("createQuerySessions()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateQuerySessions(List<QuerySession> querySessions) throws BaseException
    //{
    //}

}
