package com.queryclient.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.GaeUserStruct;
// import com.queryclient.ws.bean.GaeUserStructBean;
import com.queryclient.af.bean.GaeUserStructBean;


public class GaeUserStructProxyUtil
{
    private static final Logger log = Logger.getLogger(GaeUserStructProxyUtil.class.getName());

    // Static methods only.
    private GaeUserStructProxyUtil() {}

    public static GaeUserStructBean convertServerGaeUserStructBeanToAppBean(GaeUserStruct serverBean)
    {
        GaeUserStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new GaeUserStructBean();
            bean.setAuthDomain(serverBean.getAuthDomain());
            bean.setFederatedIdentity(serverBean.getFederatedIdentity());
            bean.setNickname(serverBean.getNickname());
            bean.setUserId(serverBean.getUserId());
            bean.setEmail(serverBean.getEmail());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.queryclient.ws.bean.GaeUserStructBean convertAppGaeUserStructBeanToServerBean(GaeUserStruct appBean)
    {
        com.queryclient.ws.bean.GaeUserStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.GaeUserStructBean();
            bean.setAuthDomain(appBean.getAuthDomain());
            bean.setFederatedIdentity(appBean.getFederatedIdentity());
            bean.setNickname(appBean.getNickname());
            bean.setUserId(appBean.getUserId());
            bean.setEmail(appBean.getEmail());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
