package com.queryclient.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.GaeAppStructStub;
import com.queryclient.ws.stub.GaeAppStructListStub;
import com.queryclient.ws.stub.UserPasswordStub;
import com.queryclient.ws.stub.UserPasswordListStub;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.UserPasswordBean;
import com.queryclient.ws.service.UserPasswordService;
import com.queryclient.af.proxy.UserPasswordServiceProxy;
import com.queryclient.af.proxy.remote.RemoteUserPasswordServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncUserPasswordServiceProxy extends BaseAsyncServiceProxy implements UserPasswordServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncUserPasswordServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteUserPasswordServiceProxy remoteProxy;

    public AsyncUserPasswordServiceProxy()
    {
        remoteProxy = new RemoteUserPasswordServiceProxy();
    }

    @Override
    public UserPassword getUserPassword(String guid) throws BaseException
    {
        return remoteProxy.getUserPassword(guid);
    }

    @Override
    public Object getUserPassword(String guid, String field) throws BaseException
    {
        return remoteProxy.getUserPassword(guid, field);       
    }

    @Override
    public List<UserPassword> getUserPasswords(List<String> guids) throws BaseException
    {
        return remoteProxy.getUserPasswords(guids);
    }

    @Override
    public List<UserPassword> getAllUserPasswords() throws BaseException
    {
        return getAllUserPasswords(null, null, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllUserPasswords(ordering, offset, count);
        return getAllUserPasswords(ordering, offset, count, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllUserPasswords(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllUserPasswordKeys(ordering, offset, count);
        return getAllUserPasswordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserPassword(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        UserPasswordBean bean = new UserPasswordBean(null, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
        return createUserPassword(bean);        
    }

    @Override
    public String createUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userPassword.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((UserPasswordBean) userPassword).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateUserPassword-" + guid;
        String taskName = "RsCreateUserPassword-" + guid + "-" + (new Date()).getTime();
        UserPasswordStub stub = MarshalHelper.convertUserPasswordToStub(userPassword);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserPasswordStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userPassword.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserPasswordStub dummyStub = new UserPasswordStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createUserPassword(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userPasswords/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createUserPassword(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userPasswords/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateUserPassword(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserPassword guid is invalid.");
        	throw new BaseException("UserPassword guid is invalid.");
        }
        UserPasswordBean bean = new UserPasswordBean(guid, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
        return updateUserPassword(bean);        
    }

    @Override
    public Boolean updateUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userPassword.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserPassword object is invalid.");
        	throw new BaseException("UserPassword object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateUserPassword-" + guid;
        String taskName = "RsUpdateUserPassword-" + guid + "-" + (new Date()).getTime();
        UserPasswordStub stub = MarshalHelper.convertUserPasswordToStub(userPassword);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserPasswordStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userPassword.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserPasswordStub dummyStub = new UserPasswordStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateUserPassword(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userPasswords/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateUserPassword(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userPasswords/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteUserPassword-" + guid;
        String taskName = "RsDeleteUserPassword-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "userPasswords/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserPassword(UserPassword userPassword) throws BaseException
    {
        String guid = userPassword.getGuid();
        return deleteUserPassword(guid);
    }

    @Override
    public Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteUserPasswords(filter, params, values);
    }

}
