package com.queryclient.af.resource.impl;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.DatatypeConverter;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.json.JSONWithPadding;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.af.config.Config;
import com.queryclient.af.auth.TwoLeggedOAuthProvider;
import com.queryclient.af.auth.common.AuthMethod;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;
import com.queryclient.ws.resource.exception.UnauthorizedRsException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.User;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.UserStub;
import com.queryclient.ws.stub.UserListStub;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.GaeUserStructBean;
import com.queryclient.af.bean.UserBean;
import com.queryclient.af.service.manager.ServiceManager;
import com.queryclient.af.resource.UserResource;
import com.queryclient.af.resource.util.GaeAppStructResourceUtil;
import com.queryclient.af.resource.util.GaeUserStructResourceUtil;


@Path("/users/")
public class UserResourceImpl extends BaseResourceImpl implements UserResource
{
    private static final Logger log = Logger.getLogger(UserResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public UserResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;

        // TBD:
        // The auth-related checking should be done here rather than putting the same code in every method... ???
        // ...
    }

    // Throws exception if auth check fails
    private void doAuthCheck() throws BaseResourceException
    {
        // if(isRunningOnDevel && isIgnoreAuth()) {
        if(isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, then it could be a serious error.");
        } else {
            String authMethod = getAuthMethod();
            if(authMethod.equals(AuthMethod.NONE)) {
                // Auth ignored.
            } else if(authMethod.equals(AuthMethod.BASIC)) {
                String authType = null;
                String encodedAuthToken = null;
                if(httpHeaders != null) {
                    List<String> authList = httpHeaders.getRequestHeader("Authorization");
                    if(authList != null && !authList.isEmpty()) {
                        String authLine = authList.get(0);    // Use the first value.
                        String[] parts = authLine.split("\\s+", 2);
                        authType = parts[0];
                        encodedAuthToken = parts[1];
                    }
                }
                if(authType == null || ! authType.equalsIgnoreCase("Basic")) {
                    log.warning("HTTP Basic authentication failed. Request authType = " + authType);
                    throw new UnauthorizedRsException("HTTP Basic authentication failed. Request authType = " + authType, resourceUri);
                }
                String authToken = null;
                if(encodedAuthToken != null) {
                    byte[] decodedBytes = DatatypeConverter.parseBase64Binary(encodedAuthToken);
                    authToken = new String(decodedBytes);
                }
                String serverUsernamePasswordPair = getHttpBasicAuthUsernameAndPassword(null);
                if(authToken == null || ! authToken.equals(serverUsernamePasswordPair)) {
                    log.warning("HTTP Basic authentication failed: Invalid username/password.");
                    throw new UnauthorizedRsException("HTTP Basic authentication failed: Invalid username/password.", resourceUri);                        
                }
            } else if(authMethod.equals(AuthMethod.BEARER)) {
                String tokenType = null;
                String bearerToken = null;
                if(httpHeaders != null) {
                    List<String> authList = httpHeaders.getRequestHeader("Authorization");
                    if(authList != null && !authList.isEmpty()) {
                        String authLine = authList.get(0);    // Use the first value.
                        String[] parts = authLine.split("\\s+", 2);
                        tokenType = parts[0];
                        bearerToken = parts[1];
                    }
                }
                if(tokenType == null || ! tokenType.equalsIgnoreCase("Bearer")) {
                    log.warning("OAuth2 authorization failed. Request tokenType = " + tokenType);
                    throw new UnauthorizedRsException("OAuth2 authorization failed. Request tokenType = " + tokenType, resourceUri);
                }
                String serverUserAccessToken = getUserOAuth2AccessToken(null);
                if(bearerToken == null || ! bearerToken.equals(serverUserAccessToken)) {
                    log.warning("OAuth2 authorization failed: Invalid bearer token");
                    throw new UnauthorizedRsException("OAuth2 authorization failed: Invalid bearer token", resourceUri);                        
                }
            } else if(authMethod.equals(AuthMethod.TWOLEGGED)) {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            } else {
                // Unsupported auth method.
                log.warning("AuthMethod currently unsupported: " + authMethod);
                throw new UnauthorizedRsException("AuthMethod currently unsupported: " + authMethod, resourceUri);
            }
        }
    }


    private Response getUserList(List<User> beans, StringCursor forwardCursor) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }

        UserListStub listStub = null;
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            listStub = new UserListStub();
        } else {
            List<UserStub> stubs = new ArrayList<UserStub>();
            Iterator<User> it = beans.iterator();
            while(it.hasNext()) {
                User bean = (User) it.next();
                stubs.add(UserStub.convertBeanToStub(bean));
            }
            listStub = new UserListStub(stubs);
        }
        String cursorString = null;
        if(forwardCursor != null) {
            cursorString = forwardCursor.getWebSafeString();
        }
        listStub.setForwardCursor(cursorString);
        return Response.ok(listStub).build();        
    }

    // TBD
    private Response getUserListAsJsonp(List<User> beans, StringCursor forwardCursor, String callback) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }

        UserListStub listStub = null;
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            listStub = new UserListStub();
        } else {
            List<UserStub> stubs = new ArrayList<UserStub>();
            Iterator<User> it = beans.iterator();
            while(it.hasNext()) {
                User bean = (User) it.next();
                stubs.add(UserStub.convertBeanToStub(bean));
            }
            listStub = new UserListStub(stubs);
        }
        String cursorString = null;
        if(forwardCursor != null) {
            cursorString = forwardCursor.getWebSafeString();
        }
        listStub.setForwardCursor(cursorString);
        return Response.ok(new JSONWithPadding(listStub, callback)).build();        
    }

    @Override
    public Response getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<User> beans = ServiceManager.getUserService().getAllUsers(ordering, offset, count, forwardCursor);
            return getUserList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<String> keys = ServiceManager.getUserService().getAllUserKeys(ordering, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<String> keys = ServiceManager.getUserService().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<User> beans = ServiceManager.getUserService().findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return getUserList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findUsersAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<User> beans = ServiceManager.getUserService().findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return getUserListAsJsonp(beans, forwardCursor, callback);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            Long count = ServiceManager.getUserService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
//    public Response getUserAsHtml(String guid) throws BaseResourceException
//    {
//        // TBD
//        throw new NotImplementedRsException("Html format currently not supported.", resourceUri);
//    }

    @Override
    public Response getUser(String guid) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            User bean = ServiceManager.getUserService().getUser(guid);
            UserStub stub = UserStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("User stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full User stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getUserAsJsonp(String guid, String callback) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            User bean = ServiceManager.getUserService().getUser(guid);
            UserStub stub = UserStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("User stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full User stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getUser(String guid, String field) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            if(guid != null) {
                guid = guid.toLowerCase();  // TBD: Validate/normalize guid....
            }
            if(field == null || field.trim().length() == 0) {
                return getUser(guid);
            }
            User bean = ServiceManager.getUserService().getUser(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("managerApp")) {
                    String fval = bean.getManagerApp();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("appAcl")) {
                    Long fval = bean.getAppAcl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("gaeApp")) {
                    GaeAppStruct fval = bean.getGaeApp();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("aeryId")) {
                    String fval = bean.getAeryId();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("sessionId")) {
                    String fval = bean.getSessionId();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("username")) {
                    String fval = bean.getUsername();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("nickname")) {
                    String fval = bean.getNickname();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("avatar")) {
                    String fval = bean.getAvatar();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("email")) {
                    String fval = bean.getEmail();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("openId")) {
                    String fval = bean.getOpenId();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("gaeUser")) {
                    GaeUserStruct fval = bean.getGaeUser();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("timeZone")) {
                    String fval = bean.getTimeZone();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("address")) {
                    String fval = bean.getAddress();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("location")) {
                    String fval = bean.getLocation();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("ipAddress")) {
                    String fval = bean.getIpAddress();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("referer")) {
                    String fval = bean.getReferer();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("obsolete")) {
                    Boolean fval = bean.isObsolete();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("status")) {
                    String fval = bean.getStatus();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("emailVerifiedTime")) {
                    Long fval = bean.getEmailVerifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("openIdVerifiedTime")) {
                    Long fval = bean.getOpenIdVerifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("authenticatedTime")) {
                    Long fval = bean.getAuthenticatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    // TBD
    @Override
    public Response constructUser(UserStub user) throws BaseResourceException
    {
        doAuthCheck();

        try {
            UserBean bean = convertUserStubToBean(user);
            bean = (UserBean) ServiceManager.getUserService().constructUser(bean);
            user = UserStub.convertBeanToStub(bean);
            String guid = user.getGuid();
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(user).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createUser(UserStub user) throws BaseResourceException
    {
        doAuthCheck();

        try {
            UserBean bean = convertUserStubToBean(user);
            String guid = ServiceManager.getUserService().createUser(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createUser(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        doAuthCheck();

        try {
            UserBean bean = convertFormParamsToBean(formParams);
            String guid = ServiceManager.getUserService().createUser(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    // TBD
    @Override
    public Response refreshUser(String guid, UserStub user) throws BaseResourceException
    {
        doAuthCheck();

        try {
            if(user == null || !guid.equals(user.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from user guid = " + user.getGuid());
                throw new RequestForbiddenException("Failed to refresh the user with guid = " + guid);
            }
            UserBean bean = convertUserStubToBean(user);
            bean = (UserBean) ServiceManager.getUserService().refreshUser(bean);
            if(bean == null) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the user with guid = " + guid);
                throw new InternalServerErrorException("Failed to refresh the user with guid = " + guid);
            }
            user = UserStub.convertBeanToStub(bean);
            return Response.ok(user).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateUser(String guid, UserStub user) throws BaseResourceException
    {
        doAuthCheck();

        try {
            if(user == null || !guid.equals(user.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from user guid = " + user.getGuid());
                throw new RequestForbiddenException("Failed to update the user with guid = " + guid);
            }
            UserBean bean = convertUserStubToBean(user);
            boolean suc = ServiceManager.getUserService().updateUser(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the user with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the user with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateUser(String guid, String managerApp, Long appAcl, String gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, String gaeUser, String timeZone, String address, String location, String ipAddress, String referer, Boolean obsolete, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime)
    {
        doAuthCheck();

        try {
            /*
            boolean suc = ServiceManager.getUserService().updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, address, location, ipAddress, referer, obsolete, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the user with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the user with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
        //} catch(BadRequestException ex) {
        //    throw new BadRequestRsException(ex, resourceUri);
        //} catch(ResourceNotFoundException ex) {
        //    throw new ResourceNotFoundRsException(ex, resourceUri);
        //} catch(ResourceGoneException ex) {
        //    throw new ResourceGoneRsException(ex, resourceUri);
        //} catch(RequestForbiddenException ex) {
        //    throw new RequestForbiddenRsException(ex, resourceUri);
        //} catch(RequestConflictException ex) {
        //    throw new RequestConflictRsException(ex, resourceUri);
        //} catch(ServiceUnavailableException ex) {
        //    throw new ServiceUnavailableRsException(ex, resourceUri);
        //} catch(InternalServerErrorException ex) {
        //    throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response updateUser(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        doAuthCheck();

        try {
            UserBean bean = convertFormParamsToBean(formParams);
            boolean suc = ServiceManager.getUserService().updateUser(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the user with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the user with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteUser(String guid) throws BaseResourceException
    {
        doAuthCheck();

        try {
            boolean suc = ServiceManager.getUserService().deleteUser(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the user with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the user with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteUsers(String filter, String params, List<String> values) throws BaseResourceException
    {
        doAuthCheck();

        try {
            Long count = ServiceManager.getUserService().deleteUsers(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    @Override
    public Response createUsers(UserListStub users) throws BaseResourceException
    {
        doAuthCheck();

        try {
            List<UserStub> stubs = users.getList();
            List<User> beans = new ArrayList<User>();
            for(UserStub stub : stubs) {
                UserBean bean = convertUserStubToBean(stub);
                beans.add(bean);
            }
            Integer count = ServiceManager.getUserService().createUsers(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    public static UserBean convertUserStubToBean(User stub)
    {
        UserBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new UserBean();
            bean.setGuid(stub.getGuid());
            bean.setManagerApp(stub.getManagerApp());
            bean.setAppAcl(stub.getAppAcl());
            bean.setGaeApp(GaeAppStructResourceUtil.convertGaeAppStructStubToBean(stub.getGaeApp()));
            bean.setAeryId(stub.getAeryId());
            bean.setSessionId(stub.getSessionId());
            bean.setUsername(stub.getUsername());
            bean.setNickname(stub.getNickname());
            bean.setAvatar(stub.getAvatar());
            bean.setEmail(stub.getEmail());
            bean.setOpenId(stub.getOpenId());
            bean.setGaeUser(GaeUserStructResourceUtil.convertGaeUserStructStubToBean(stub.getGaeUser()));
            bean.setTimeZone(stub.getTimeZone());
            bean.setAddress(stub.getAddress());
            bean.setLocation(stub.getLocation());
            bean.setIpAddress(stub.getIpAddress());
            bean.setReferer(stub.getReferer());
            bean.setObsolete(stub.isObsolete());
            bean.setStatus(stub.getStatus());
            bean.setEmailVerifiedTime(stub.getEmailVerifiedTime());
            bean.setOpenIdVerifiedTime(stub.getOpenIdVerifiedTime());
            bean.setAuthenticatedTime(stub.getAuthenticatedTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<UserBean> convertUserListStubToBeanList(UserListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<UserStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<UserBean> beanList = new ArrayList<UserBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(UserStub stub : stubList) {
                    UserBean bean = convertUserStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }


    // TBD
    // This needs to be implemented before url-encoded form create/update can be supported 
    public static UserBean convertFormParamsToBean(MultivaluedMap<String, String> formParams)
    {
        UserBean bean = new UserBean();
        if(formParams == null) {
            log.log(Level.INFO, "FormParams is null. Empty bean is returned.");
        } else {
            Iterator<MultivaluedMap.Entry<String,List<String>>> it = formParams.entrySet().iterator();
            while(it.hasNext()) {
                MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
                String key = (String) m.getKey();
                String val = null;
                List<String> list = m.getValue();
                if(list != null && list.size() > 0) {
                    val = list.get(0);
                    if(key.equals("guid")) {
                        bean.setGuid(val);                        
                    } else if(key.equals("managerApp")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setManagerApp(v);
                    } else if(key.equals("appAcl")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setAppAcl(v);
                    } else if(key.equals("gaeApp")) {
                        // TBD: Add "valueOf()" static method to the bean object.
                        //bean.setGaeApp(GaeAppBean.valueOf(val));
                    } else if(key.equals("aeryId")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setAeryId(v);
                    } else if(key.equals("sessionId")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setSessionId(v);
                    } else if(key.equals("username")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setUsername(v);
                    } else if(key.equals("nickname")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setNickname(v);
                    } else if(key.equals("avatar")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setAvatar(v);
                    } else if(key.equals("email")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setEmail(v);
                    } else if(key.equals("openId")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setOpenId(v);
                    } else if(key.equals("gaeUser")) {
                        // TBD: Add "valueOf()" static method to the bean object.
                        //bean.setGaeUser(GaeUserBean.valueOf(val));
                    } else if(key.equals("timeZone")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setTimeZone(v);
                    } else if(key.equals("address")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setAddress(v);
                    } else if(key.equals("location")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setLocation(v);
                    } else if(key.equals("ipAddress")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setIpAddress(v);
                    } else if(key.equals("referer")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setReferer(v);
                    } else if(key.equals("obsolete")) {
                        // TBD: Needs to use "parse()" methods.
                        //Boolean v = (Boolean) val;
                        //bean.setObsolete(v);
                    } else if(key.equals("status")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setStatus(v);
                    } else if(key.equals("emailVerifiedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setEmailVerifiedTime(v);
                    } else if(key.equals("openIdVerifiedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setOpenIdVerifiedTime(v);
                    } else if(key.equals("authenticatedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setAuthenticatedTime(v);
                    } else if(key.equals("createdTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setCreatedTime(v);
                    } else if(key.equals("modifiedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setModifiedTime(v);
                    }
                }
            }
        }
        return bean;
    }

}
