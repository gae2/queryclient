package com.queryclient.fe.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.fe.bean.KeyValuePairStructJsBean;
import com.queryclient.fe.bean.KeyValueRelationStructJsBean;
import com.queryclient.fe.bean.ExternalServiceApiKeyStructJsBean;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.fe.bean.GaeAppStructJsBean;
import com.queryclient.fe.bean.GaeUserStructJsBean;
import com.queryclient.fe.bean.PagerStateStructJsBean;
import com.queryclient.fe.bean.ApiConsumerJsBean;
import com.queryclient.fe.bean.UserJsBean;
import com.queryclient.fe.bean.UserPasswordJsBean;
import com.queryclient.fe.bean.ExternalUserIdStructJsBean;
import com.queryclient.fe.bean.ExternalUserAuthJsBean;
import com.queryclient.fe.bean.UserAuthStateJsBean;
import com.queryclient.fe.bean.ConsumerKeySecretPairJsBean;
import com.queryclient.fe.bean.DataServiceJsBean;
import com.queryclient.fe.bean.ServiceEndpointJsBean;
import com.queryclient.fe.bean.QuerySessionJsBean;
import com.queryclient.fe.bean.QueryRecordJsBean;
import com.queryclient.fe.bean.DummyEntityJsBean;
import com.queryclient.fe.bean.ServiceInfoJsBean;
import com.queryclient.fe.bean.FiveTenJsBean;


// Utility functions for combining/decomposing into json string segments.
public class JsonJsBeanUtil
{
    private static final Logger log = Logger.getLogger(JsonJsBeanUtil.class.getName());

    private JsonJsBeanUtil() {}

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> objJsonString
    public static Map<String, String> parseJsonObjectMapString(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, String> jsonObjectMap = new HashMap<String, String>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            factory.setCodec(getObjectMapper());
            JsonParser parser = factory.createJsonParser(jsonStr);

            JsonNode topNode = parser.readValueAsTree();
            Iterator<String> fieldNames = topNode.getFieldNames();
            
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                JsonNode leafNode = topNode.get(name);
                if(! leafNode.isNull()) {
                    // ???
                    // String value = leafNode.asText();
                    String value = leafNode.toString();
                    // ...
                    jsonObjectMap.put(name, value);
                    if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: name = " + name + "; value = " + value);
                } else {
                    if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: Empty node skipped. name = " + name);
                }
            }
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> objJsonString
    public static String generateJsonObjectMapString(Map<String, String> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for(String key : jsonObjectMap.keySet()) {
            String json = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(json == null) {
                sb.append("null");  // ????
            } else {
                sb.append(json);
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectMapString(): jsonStr = " + jsonStr);

        return jsonStr;
    }


    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> jsBean
    public static Map<String, Object> parseJsonObjectMap(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, Object> jsonObjectMap = new HashMap<String, Object>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            factory.setCodec(getObjectMapper());
            JsonParser parser = factory.createJsonParser(jsonStr);
            JsonNode topNode = parser.readValueAsTree();

/*
            // TBD: Remove the loop.
            Iterator<String> fieldNames = topNode.getFieldNames();
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                String value = topNode.get(name).asText();
                if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: name = " + name + "; value = " + value);

                if(name.equals("keyValuePairStruct")) {
                    KeyValuePairStructJsBean bean = KeyValuePairStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("keyValueRelationStruct")) {
                    KeyValueRelationStructJsBean bean = KeyValueRelationStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("externalServiceApiKeyStruct")) {
                    ExternalServiceApiKeyStructJsBean bean = ExternalServiceApiKeyStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("referrerInfoStruct")) {
                    ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeAppStruct")) {
                    GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeUserStruct")) {
                    GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("pagerStateStruct")) {
                    PagerStateStructJsBean bean = PagerStateStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("apiConsumer")) {
                    ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("user")) {
                    UserJsBean bean = UserJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userPassword")) {
                    UserPasswordJsBean bean = UserPasswordJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("externalUserIdStruct")) {
                    ExternalUserIdStructJsBean bean = ExternalUserIdStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("externalUserAuth")) {
                    ExternalUserAuthJsBean bean = ExternalUserAuthJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userAuthState")) {
                    UserAuthStateJsBean bean = UserAuthStateJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("consumerKeySecretPair")) {
                    ConsumerKeySecretPairJsBean bean = ConsumerKeySecretPairJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("dataService")) {
                    DataServiceJsBean bean = DataServiceJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("serviceEndpoint")) {
                    ServiceEndpointJsBean bean = ServiceEndpointJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("querySession")) {
                    QuerySessionJsBean bean = QuerySessionJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("queryRecord")) {
                    QueryRecordJsBean bean = QueryRecordJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("dummyEntity")) {
                    DummyEntityJsBean bean = DummyEntityJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("serviceInfo")) {
                    ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fiveTen")) {
                    FiveTenJsBean bean = FiveTenJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
            }
*/

            JsonNode objNode = null;
            String objValueStr = null;
            objNode = topNode.findValue("keyValuePairStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeyValuePairStructJsBean bean = KeyValuePairStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keyValuePairStruct", bean);               
            }
            objNode = topNode.findValue("keyValueRelationStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeyValueRelationStructJsBean bean = KeyValueRelationStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keyValueRelationStruct", bean);               
            }
            objNode = topNode.findValue("externalServiceApiKeyStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ExternalServiceApiKeyStructJsBean bean = ExternalServiceApiKeyStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("externalServiceApiKeyStruct", bean);               
            }
            objNode = topNode.findValue("referrerInfoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("referrerInfoStruct", bean);               
            }
            objNode = topNode.findValue("gaeAppStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeAppStruct", bean);               
            }
            objNode = topNode.findValue("gaeUserStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeUserStruct", bean);               
            }
            objNode = topNode.findValue("pagerStateStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                PagerStateStructJsBean bean = PagerStateStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("pagerStateStruct", bean);               
            }
            objNode = topNode.findValue("apiConsumer");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("apiConsumer", bean);               
            }
            objNode = topNode.findValue("user");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserJsBean bean = UserJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("user", bean);               
            }
            objNode = topNode.findValue("userPassword");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserPasswordJsBean bean = UserPasswordJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userPassword", bean);               
            }
            objNode = topNode.findValue("externalUserIdStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ExternalUserIdStructJsBean bean = ExternalUserIdStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("externalUserIdStruct", bean);               
            }
            objNode = topNode.findValue("externalUserAuth");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ExternalUserAuthJsBean bean = ExternalUserAuthJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("externalUserAuth", bean);               
            }
            objNode = topNode.findValue("userAuthState");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserAuthStateJsBean bean = UserAuthStateJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userAuthState", bean);               
            }
            objNode = topNode.findValue("consumerKeySecretPair");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ConsumerKeySecretPairJsBean bean = ConsumerKeySecretPairJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("consumerKeySecretPair", bean);               
            }
            objNode = topNode.findValue("dataService");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                DataServiceJsBean bean = DataServiceJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("dataService", bean);               
            }
            objNode = topNode.findValue("serviceEndpoint");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ServiceEndpointJsBean bean = ServiceEndpointJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("serviceEndpoint", bean);               
            }
            objNode = topNode.findValue("querySession");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                QuerySessionJsBean bean = QuerySessionJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("querySession", bean);               
            }
            objNode = topNode.findValue("queryRecord");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                QueryRecordJsBean bean = QueryRecordJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("queryRecord", bean);               
            }
            objNode = topNode.findValue("dummyEntity");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                DummyEntityJsBean bean = DummyEntityJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("dummyEntity", bean);               
            }
            objNode = topNode.findValue("serviceInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("serviceInfo", bean);               
            }
            objNode = topNode.findValue("fiveTen");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FiveTenJsBean bean = FiveTenJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fiveTen", bean);               
            }
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> jsBean
    public static String generateJsonObjectMap(Map<String, Object> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        // TBD: Remove the loop.
        for(String key : jsonObjectMap.keySet()) {
            Object jsonObj = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(jsonObj == null) {
                sb.append("null");  // ????
            } else {
                if(jsonObj instanceof KeyValuePairStructJsBean) {
                    sb.append(((KeyValuePairStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof KeyValueRelationStructJsBean) {
                    sb.append(((KeyValueRelationStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ExternalServiceApiKeyStructJsBean) {
                    sb.append(((ExternalServiceApiKeyStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ReferrerInfoStructJsBean) {
                    sb.append(((ReferrerInfoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeAppStructJsBean) {
                    sb.append(((GaeAppStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeUserStructJsBean) {
                    sb.append(((GaeUserStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof PagerStateStructJsBean) {
                    sb.append(((PagerStateStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ApiConsumerJsBean) {
                    sb.append(((ApiConsumerJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserJsBean) {
                    sb.append(((UserJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserPasswordJsBean) {
                    sb.append(((UserPasswordJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ExternalUserIdStructJsBean) {
                    sb.append(((ExternalUserIdStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ExternalUserAuthJsBean) {
                    sb.append(((ExternalUserAuthJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserAuthStateJsBean) {
                    sb.append(((UserAuthStateJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ConsumerKeySecretPairJsBean) {
                    sb.append(((ConsumerKeySecretPairJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof DataServiceJsBean) {
                    sb.append(((DataServiceJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ServiceEndpointJsBean) {
                    sb.append(((ServiceEndpointJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof QuerySessionJsBean) {
                    sb.append(((QuerySessionJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof QueryRecordJsBean) {
                    sb.append(((QueryRecordJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof DummyEntityJsBean) {
                    sb.append(((DummyEntityJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ServiceInfoJsBean) {
                    sb.append(((ServiceInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FiveTenJsBean) {
                    sb.append(((FiveTenJsBean) jsonObj).toJsonString());
                }
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectMap(): jsonStr = " + jsonStr);

        return jsonStr;
    }
    
    
}
