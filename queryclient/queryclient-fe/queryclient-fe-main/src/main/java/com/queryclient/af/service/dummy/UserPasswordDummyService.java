package com.queryclient.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.af.config.Config;

import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.UserPasswordService;


// The primary purpose of UserPasswordDummyService is to fake the service api, UserPasswordService.
// It has no real implementation.
public class UserPasswordDummyService implements UserPasswordService
{
    private static final Logger log = Logger.getLogger(UserPasswordDummyService.class.getName());

    public UserPasswordDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // UserPassword related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserPassword getUserPassword(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserPassword(): guid = " + guid);
        return null;
    }

    @Override
    public Object getUserPassword(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserPassword(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<UserPassword> getUserPasswords(List<String> guids) throws BaseException
    {
        log.fine("getUserPasswords()");
        return null;
    }

    @Override
    public List<UserPassword> getAllUserPasswords() throws BaseException
    {
        return getAllUserPasswords(null, null, null);
    }


    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswords(ordering, offset, count, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserPasswords(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserPasswordKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserPasswordDummyService.findUserPasswords(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserPasswordDummyService.findUserPasswordKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserPasswordDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createUserPassword(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        log.finer("createUserPassword()");
        return null;
    }

    @Override
    public String createUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("createUserPassword()");
        return null;
    }

    @Override
    public UserPassword constructUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("constructUserPassword()");
        return null;
    }

    @Override
    public Boolean updateUserPassword(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        log.finer("updateUserPassword()");
        return null;
    }
        
    @Override
    public Boolean updateUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("updateUserPassword()");
        return null;
    }

    @Override
    public UserPassword refreshUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("refreshUserPassword()");
        return null;
    }

    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteUserPassword(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("deleteUserPassword()");
        return null;
    }

    // TBD
    @Override
    public Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteUserPassword(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUserPasswords(List<UserPassword> userPasswords) throws BaseException
    {
        log.finer("createUserPasswords()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateUserPasswords(List<UserPassword> userPasswords) throws BaseException
    //{
    //}

}
