package com.queryclient.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalUserAuthJsBean implements Serializable, Cloneable  //, ExternalUserAuth
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExternalUserAuthJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructJsBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private String providerId;
    private ExternalUserIdStructJsBean externalUserId;
    private String requestToken;
    private String accessToken;
    private String accessTokenSecret;
    private String email;
    private String firstName;
    private String lastName;
    private String fullName;
    private String displayName;
    private String description;
    private String gender;
    private String dateOfBirth;
    private String profileImageUrl;
    private String timeZone;
    private String postalCode;
    private String location;
    private String country;
    private String language;
    private String status;
    private Long authTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ExternalUserAuthJsBean()
    {
        //this((String) null);
    }
    public ExternalUserAuthJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ExternalUserAuthJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStructJsBean externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, providerId, externalUserId, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime, null, null);
    }
    public ExternalUserAuthJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStructJsBean externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.providerId = providerId;
        this.externalUserId = externalUserId;
        this.requestToken = requestToken;
        this.accessToken = accessToken;
        this.accessTokenSecret = accessTokenSecret;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
        this.displayName = displayName;
        this.description = description;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.profileImageUrl = profileImageUrl;
        this.timeZone = timeZone;
        this.postalCode = postalCode;
        this.location = location;
        this.country = country;
        this.language = language;
        this.status = status;
        this.authTime = authTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ExternalUserAuthJsBean(ExternalUserAuthJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setManagerApp(bean.getManagerApp());
            setAppAcl(bean.getAppAcl());
            setGaeApp(bean.getGaeApp());
            setOwnerUser(bean.getOwnerUser());
            setUserAcl(bean.getUserAcl());
            setUser(bean.getUser());
            setProviderId(bean.getProviderId());
            setExternalUserId(bean.getExternalUserId());
            setRequestToken(bean.getRequestToken());
            setAccessToken(bean.getAccessToken());
            setAccessTokenSecret(bean.getAccessTokenSecret());
            setEmail(bean.getEmail());
            setFirstName(bean.getFirstName());
            setLastName(bean.getLastName());
            setFullName(bean.getFullName());
            setDisplayName(bean.getDisplayName());
            setDescription(bean.getDescription());
            setGender(bean.getGender());
            setDateOfBirth(bean.getDateOfBirth());
            setProfileImageUrl(bean.getProfileImageUrl());
            setTimeZone(bean.getTimeZone());
            setPostalCode(bean.getPostalCode());
            setLocation(bean.getLocation());
            setCountry(bean.getCountry());
            setLanguage(bean.getLanguage());
            setStatus(bean.getStatus());
            setAuthTime(bean.getAuthTime());
            setExpirationTime(bean.getExpirationTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ExternalUserAuthJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ExternalUserAuthJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ExternalUserAuthJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ExternalUserAuthJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStructJsBean getGaeApp()
    {  
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStructJsBean gaeApp)
    {
        this.gaeApp = gaeApp;
    }

    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getProviderId()
    {
        return this.providerId;
    }
    public void setProviderId(String providerId)
    {
        this.providerId = providerId;
    }

    public ExternalUserIdStructJsBean getExternalUserId()
    {  
        return this.externalUserId;
    }
    public void setExternalUserId(ExternalUserIdStructJsBean externalUserId)
    {
        this.externalUserId = externalUserId;
    }

    public String getRequestToken()
    {
        return this.requestToken;
    }
    public void setRequestToken(String requestToken)
    {
        this.requestToken = requestToken;
    }

    public String getAccessToken()
    {
        return this.accessToken;
    }
    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getAccessTokenSecret()
    {
        return this.accessTokenSecret;
    }
    public void setAccessTokenSecret(String accessTokenSecret)
    {
        this.accessTokenSecret = accessTokenSecret;
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFirstName()
    {
        return this.firstName;
    }
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return this.lastName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getFullName()
    {
        return this.fullName;
    }
    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getDisplayName()
    {
        return this.displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getGender()
    {
        return this.gender;
    }
    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public String getDateOfBirth()
    {
        return this.dateOfBirth;
    }
    public void setDateOfBirth(String dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }

    public String getProfileImageUrl()
    {
        return this.profileImageUrl;
    }
    public void setProfileImageUrl(String profileImageUrl)
    {
        this.profileImageUrl = profileImageUrl;
    }

    public String getTimeZone()
    {
        return this.timeZone;
    }
    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }

    public String getPostalCode()
    {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getCountry()
    {
        return this.country;
    }
    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getAuthTime()
    {
        return this.authTime;
    }
    public void setAuthTime(Long authTime)
    {
        this.authTime = authTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("managerApp:null, ");
        sb.append("appAcl:0, ");
        sb.append("gaeApp:{}, ");
        sb.append("ownerUser:null, ");
        sb.append("userAcl:0, ");
        sb.append("user:null, ");
        sb.append("providerId:null, ");
        sb.append("externalUserId:{}, ");
        sb.append("requestToken:null, ");
        sb.append("accessToken:null, ");
        sb.append("accessTokenSecret:null, ");
        sb.append("email:null, ");
        sb.append("firstName:null, ");
        sb.append("lastName:null, ");
        sb.append("fullName:null, ");
        sb.append("displayName:null, ");
        sb.append("description:null, ");
        sb.append("gender:null, ");
        sb.append("dateOfBirth:null, ");
        sb.append("profileImageUrl:null, ");
        sb.append("timeZone:null, ");
        sb.append("postalCode:null, ");
        sb.append("location:null, ");
        sb.append("country:null, ");
        sb.append("language:null, ");
        sb.append("status:null, ");
        sb.append("authTime:0, ");
        sb.append("expirationTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("managerApp:");
        if(this.getManagerApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getManagerApp()).append("\", ");
        }
        sb.append("appAcl:" + this.getAppAcl()).append(", ");
        sb.append("gaeApp:");
        if(this.getGaeApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGaeApp()).append("\", ");
        }
        sb.append("ownerUser:");
        if(this.getOwnerUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOwnerUser()).append("\", ");
        }
        sb.append("userAcl:" + this.getUserAcl()).append(", ");
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("providerId:");
        if(this.getProviderId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getProviderId()).append("\", ");
        }
        sb.append("externalUserId:");
        if(this.getExternalUserId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getExternalUserId()).append("\", ");
        }
        sb.append("requestToken:");
        if(this.getRequestToken() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRequestToken()).append("\", ");
        }
        sb.append("accessToken:");
        if(this.getAccessToken() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAccessToken()).append("\", ");
        }
        sb.append("accessTokenSecret:");
        if(this.getAccessTokenSecret() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAccessTokenSecret()).append("\", ");
        }
        sb.append("email:");
        if(this.getEmail() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getEmail()).append("\", ");
        }
        sb.append("firstName:");
        if(this.getFirstName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFirstName()).append("\", ");
        }
        sb.append("lastName:");
        if(this.getLastName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLastName()).append("\", ");
        }
        sb.append("fullName:");
        if(this.getFullName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFullName()).append("\", ");
        }
        sb.append("displayName:");
        if(this.getDisplayName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDisplayName()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("gender:");
        if(this.getGender() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGender()).append("\", ");
        }
        sb.append("dateOfBirth:");
        if(this.getDateOfBirth() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDateOfBirth()).append("\", ");
        }
        sb.append("profileImageUrl:");
        if(this.getProfileImageUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getProfileImageUrl()).append("\", ");
        }
        sb.append("timeZone:");
        if(this.getTimeZone() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTimeZone()).append("\", ");
        }
        sb.append("postalCode:");
        if(this.getPostalCode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPostalCode()).append("\", ");
        }
        sb.append("location:");
        if(this.getLocation() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLocation()).append("\", ");
        }
        sb.append("country:");
        if(this.getCountry() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCountry()).append("\", ");
        }
        sb.append("language:");
        if(this.getLanguage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLanguage()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("authTime:" + this.getAuthTime()).append(", ");
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getManagerApp() != null) {
            sb.append("\"managerApp\":").append("\"").append(this.getManagerApp()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"managerApp\":").append("null, ");
        }
        if(this.getAppAcl() != null) {
            sb.append("\"appAcl\":").append("").append(this.getAppAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appAcl\":").append("null, ");
        }
        sb.append("\"gaeApp\":").append(this.gaeApp.toJsonString()).append(", ");
        if(this.getOwnerUser() != null) {
            sb.append("\"ownerUser\":").append("\"").append(this.getOwnerUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ownerUser\":").append("null, ");
        }
        if(this.getUserAcl() != null) {
            sb.append("\"userAcl\":").append("").append(this.getUserAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userAcl\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getProviderId() != null) {
            sb.append("\"providerId\":").append("\"").append(this.getProviderId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"providerId\":").append("null, ");
        }
        sb.append("\"externalUserId\":").append(this.externalUserId.toJsonString()).append(", ");
        if(this.getRequestToken() != null) {
            sb.append("\"requestToken\":").append("\"").append(this.getRequestToken()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"requestToken\":").append("null, ");
        }
        if(this.getAccessToken() != null) {
            sb.append("\"accessToken\":").append("\"").append(this.getAccessToken()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"accessToken\":").append("null, ");
        }
        if(this.getAccessTokenSecret() != null) {
            sb.append("\"accessTokenSecret\":").append("\"").append(this.getAccessTokenSecret()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"accessTokenSecret\":").append("null, ");
        }
        if(this.getEmail() != null) {
            sb.append("\"email\":").append("\"").append(this.getEmail()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"email\":").append("null, ");
        }
        if(this.getFirstName() != null) {
            sb.append("\"firstName\":").append("\"").append(this.getFirstName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"firstName\":").append("null, ");
        }
        if(this.getLastName() != null) {
            sb.append("\"lastName\":").append("\"").append(this.getLastName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastName\":").append("null, ");
        }
        if(this.getFullName() != null) {
            sb.append("\"fullName\":").append("\"").append(this.getFullName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fullName\":").append("null, ");
        }
        if(this.getDisplayName() != null) {
            sb.append("\"displayName\":").append("\"").append(this.getDisplayName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"displayName\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getGender() != null) {
            sb.append("\"gender\":").append("\"").append(this.getGender()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"gender\":").append("null, ");
        }
        if(this.getDateOfBirth() != null) {
            sb.append("\"dateOfBirth\":").append("\"").append(this.getDateOfBirth()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"dateOfBirth\":").append("null, ");
        }
        if(this.getProfileImageUrl() != null) {
            sb.append("\"profileImageUrl\":").append("\"").append(this.getProfileImageUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"profileImageUrl\":").append("null, ");
        }
        if(this.getTimeZone() != null) {
            sb.append("\"timeZone\":").append("\"").append(this.getTimeZone()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"timeZone\":").append("null, ");
        }
        if(this.getPostalCode() != null) {
            sb.append("\"postalCode\":").append("\"").append(this.getPostalCode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"postalCode\":").append("null, ");
        }
        if(this.getLocation() != null) {
            sb.append("\"location\":").append("\"").append(this.getLocation()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"location\":").append("null, ");
        }
        if(this.getCountry() != null) {
            sb.append("\"country\":").append("\"").append(this.getCountry()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"country\":").append("null, ");
        }
        if(this.getLanguage() != null) {
            sb.append("\"language\":").append("\"").append(this.getLanguage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"language\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getAuthTime() != null) {
            sb.append("\"authTime\":").append("").append(this.getAuthTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"authTime\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("managerApp = " + this.managerApp).append(";");
        sb.append("appAcl = " + this.appAcl).append(";");
        sb.append("gaeApp = " + this.gaeApp).append(";");
        sb.append("ownerUser = " + this.ownerUser).append(";");
        sb.append("userAcl = " + this.userAcl).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("providerId = " + this.providerId).append(";");
        sb.append("externalUserId = " + this.externalUserId).append(";");
        sb.append("requestToken = " + this.requestToken).append(";");
        sb.append("accessToken = " + this.accessToken).append(";");
        sb.append("accessTokenSecret = " + this.accessTokenSecret).append(";");
        sb.append("email = " + this.email).append(";");
        sb.append("firstName = " + this.firstName).append(";");
        sb.append("lastName = " + this.lastName).append(";");
        sb.append("fullName = " + this.fullName).append(";");
        sb.append("displayName = " + this.displayName).append(";");
        sb.append("description = " + this.description).append(";");
        sb.append("gender = " + this.gender).append(";");
        sb.append("dateOfBirth = " + this.dateOfBirth).append(";");
        sb.append("profileImageUrl = " + this.profileImageUrl).append(";");
        sb.append("timeZone = " + this.timeZone).append(";");
        sb.append("postalCode = " + this.postalCode).append(";");
        sb.append("location = " + this.location).append(";");
        sb.append("country = " + this.country).append(";");
        sb.append("language = " + this.language).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("authTime = " + this.authTime).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ExternalUserAuthJsBean cloned = new ExternalUserAuthJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setManagerApp(this.getManagerApp());   
        cloned.setAppAcl(this.getAppAcl());   
        cloned.setGaeApp( (GaeAppStructJsBean) this.getGaeApp().clone() );
        cloned.setOwnerUser(this.getOwnerUser());   
        cloned.setUserAcl(this.getUserAcl());   
        cloned.setUser(this.getUser());   
        cloned.setProviderId(this.getProviderId());   
        cloned.setExternalUserId( (ExternalUserIdStructJsBean) this.getExternalUserId().clone() );
        cloned.setRequestToken(this.getRequestToken());   
        cloned.setAccessToken(this.getAccessToken());   
        cloned.setAccessTokenSecret(this.getAccessTokenSecret());   
        cloned.setEmail(this.getEmail());   
        cloned.setFirstName(this.getFirstName());   
        cloned.setLastName(this.getLastName());   
        cloned.setFullName(this.getFullName());   
        cloned.setDisplayName(this.getDisplayName());   
        cloned.setDescription(this.getDescription());   
        cloned.setGender(this.getGender());   
        cloned.setDateOfBirth(this.getDateOfBirth());   
        cloned.setProfileImageUrl(this.getProfileImageUrl());   
        cloned.setTimeZone(this.getTimeZone());   
        cloned.setPostalCode(this.getPostalCode());   
        cloned.setLocation(this.getLocation());   
        cloned.setCountry(this.getCountry());   
        cloned.setLanguage(this.getLanguage());   
        cloned.setStatus(this.getStatus());   
        cloned.setAuthTime(this.getAuthTime());   
        cloned.setExpirationTime(this.getExpirationTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
