package com.queryclient.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.ws.service.QuerySessionService;
import com.queryclient.af.proxy.QuerySessionServiceProxy;


// MockQuerySessionServiceProxy is a decorator.
// It can be used as a base class to mock QuerySessionServiceProxy objects.
public abstract class MockQuerySessionServiceProxy implements QuerySessionServiceProxy
{
    private static final Logger log = Logger.getLogger(MockQuerySessionServiceProxy.class.getName());

    // MockQuerySessionServiceProxy uses the decorator design pattern.
    private QuerySessionServiceProxy decoratedProxy;

    public MockQuerySessionServiceProxy(QuerySessionServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected QuerySessionServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(QuerySessionServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        return decoratedProxy.getQuerySession(guid);
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        return decoratedProxy.getQuerySession(guid, field);       
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        return decoratedProxy.getQuerySessions(guids);
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return getAllQuerySessions(null, null, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllQuerySessions(ordering, offset, count);
        return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllQuerySessions(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllQuerySessionKeys(ordering, offset, count);
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count);
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        return decoratedProxy.createQuerySession(user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note);
    }

    @Override
    public String createQuerySession(QuerySession querySession) throws BaseException
    {
        return decoratedProxy.createQuerySession(querySession);
    }

    @Override
    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        return decoratedProxy.updateQuerySession(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note);
    }

    @Override
    public Boolean updateQuerySession(QuerySession querySession) throws BaseException
    {
        return decoratedProxy.updateQuerySession(querySession);
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        return decoratedProxy.deleteQuerySession(guid);
    }

    @Override
    public Boolean deleteQuerySession(QuerySession querySession) throws BaseException
    {
        String guid = querySession.getGuid();
        return deleteQuerySession(guid);
    }

    @Override
    public Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteQuerySessions(filter, params, values);
    }

}
