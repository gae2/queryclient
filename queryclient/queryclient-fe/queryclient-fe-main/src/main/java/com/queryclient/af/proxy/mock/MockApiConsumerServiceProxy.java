package com.queryclient.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ApiConsumer;
import com.queryclient.af.bean.ApiConsumerBean;
import com.queryclient.ws.service.ApiConsumerService;
import com.queryclient.af.proxy.ApiConsumerServiceProxy;


// MockApiConsumerServiceProxy is a decorator.
// It can be used as a base class to mock ApiConsumerServiceProxy objects.
public abstract class MockApiConsumerServiceProxy implements ApiConsumerServiceProxy
{
    private static final Logger log = Logger.getLogger(MockApiConsumerServiceProxy.class.getName());

    // MockApiConsumerServiceProxy uses the decorator design pattern.
    private ApiConsumerServiceProxy decoratedProxy;

    public MockApiConsumerServiceProxy(ApiConsumerServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected ApiConsumerServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(ApiConsumerServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public ApiConsumer getApiConsumer(String guid) throws BaseException
    {
        return decoratedProxy.getApiConsumer(guid);
    }

    @Override
    public Object getApiConsumer(String guid, String field) throws BaseException
    {
        return decoratedProxy.getApiConsumer(guid, field);       
    }

    @Override
    public List<ApiConsumer> getApiConsumers(List<String> guids) throws BaseException
    {
        return decoratedProxy.getApiConsumers(guids);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers() throws BaseException
    {
        return getAllApiConsumers(null, null, null);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllApiConsumers(ordering, offset, count);
        return getAllApiConsumers(ordering, offset, count, null);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllApiConsumers(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllApiConsumerKeys(ordering, offset, count);
        return getAllApiConsumerKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllApiConsumerKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findApiConsumers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count);
        return findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createApiConsumer(String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        return decoratedProxy.createApiConsumer(aeryId, name, description, appKey, appSecret, status);
    }

    @Override
    public String createApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return decoratedProxy.createApiConsumer(apiConsumer);
    }

    @Override
    public Boolean updateApiConsumer(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        return decoratedProxy.updateApiConsumer(guid, aeryId, name, description, appKey, appSecret, status);
    }

    @Override
    public Boolean updateApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return decoratedProxy.updateApiConsumer(apiConsumer);
    }

    @Override
    public Boolean deleteApiConsumer(String guid) throws BaseException
    {
        return decoratedProxy.deleteApiConsumer(guid);
    }

    @Override
    public Boolean deleteApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        String guid = apiConsumer.getGuid();
        return deleteApiConsumer(guid);
    }

    @Override
    public Long deleteApiConsumers(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteApiConsumers(filter, params, values);
    }

}
