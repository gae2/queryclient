package com.queryclient.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.service.impl.ServiceEndpointServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ServiceEndpointProtoService extends ServiceEndpointServiceImpl implements ServiceEndpointService
{
    private static final Logger log = Logger.getLogger(ServiceEndpointProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ServiceEndpointProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ServiceEndpoint related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ServiceEndpoint getServiceEndpoint(String guid) throws BaseException
    {
        return super.getServiceEndpoint(guid);
    }

    @Override
    public Object getServiceEndpoint(String guid, String field) throws BaseException
    {
        return super.getServiceEndpoint(guid, field);
    }

    @Override
    public List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException
    {
        return super.getServiceEndpoints(guids);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException
    {
        return super.getAllServiceEndpoints();
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpoints(ordering, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllServiceEndpoints(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        return super.createServiceEndpoint(serviceEndpoint);
    }

    @Override
    public ServiceEndpoint constructServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        return super.constructServiceEndpoint(serviceEndpoint);
    }


    @Override
    public Boolean updateServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        return super.updateServiceEndpoint(serviceEndpoint);
    }
        
    @Override
    public ServiceEndpoint refreshServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        return super.refreshServiceEndpoint(serviceEndpoint);
    }

    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        return super.deleteServiceEndpoint(guid);
    }

    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        return super.deleteServiceEndpoint(serviceEndpoint);
    }

    @Override
    public Integer createServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException
    {
        return super.createServiceEndpoints(serviceEndpoints);
    }

    // TBD
    //@Override
    //public Boolean updateServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException
    //{
    //}

}
