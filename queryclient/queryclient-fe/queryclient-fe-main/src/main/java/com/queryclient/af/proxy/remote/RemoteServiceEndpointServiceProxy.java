package com.queryclient.af.proxy.remote;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairListStub;
import com.queryclient.ws.stub.ServiceEndpointStub;
import com.queryclient.ws.stub.ServiceEndpointListStub;
import com.queryclient.af.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.proxy.ServiceEndpointServiceProxy;

import com.queryclient.af.config.Config;


// Note on retries, upon arbitrary app engine urlfetch timeout (5 secs):
// In our current implementation, create (POST) and update (PUT) have essentially the same semantics.
// If an object with the client-supplied guid does not exist in the data store, it will be created.
// If an object with the given guid already exists, it will be replaced with the supplied object.
// The only difference is, the update operation (PUT) will modify the modifiedTime field.
// (Note: Ccreate does not require guid (pk) although we will generally try to supply guid from the client side.)
// Therefore, retries are relatively safe for POST and PUT.
// GET is by definition idempotent, and hence retry is safe.
// DELETE, on the other hand, will likely cause an error if the same request is repeated.
// The consequence of this is that, a user may be told there was an error deleting an object
// when in fact the object was already deleted during the retry sequence.
public class RemoteServiceEndpointServiceProxy extends BaseRemoteServiceProxy implements ServiceEndpointServiceProxy
{
    private static final Logger log = Logger.getLogger(RemoteServiceEndpointServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_SERVICEENDPOINT = "serviceEndpoints";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public RemoteServiceEndpointServiceProxy()
    {
        initCache();
    }


    protected WebResource getServiceEndpointWebResource()
    {
        return getWebResource(RESOURCE_SERVICEENDPOINT);
    }
    protected WebResource getServiceEndpointWebResource(String path)
    {
        return getWebResource(RESOURCE_SERVICEENDPOINT, path);
    }
    protected WebResource getServiceEndpointWebResourceByGuid(String guid)
    {
        return getServiceEndpointWebResource(guid);
    }


    @Override
    public ServiceEndpoint getServiceEndpoint(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ServiceEndpoint bean = null;

        String key = getResourcePath(RESOURCE_SERVICEENDPOINT, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getServiceEndpointWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
 	            clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                ServiceEndpointStub stub = clientResponse.getEntity(ServiceEndpointStub.class);
                bean = MarshalHelper.convertServiceEndpointToBean(stub);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "ServiceEndpoint bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SERVICEENDPOINT);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getServiceEndpoint(String guid, String field) throws BaseException
    {
        ServiceEndpoint bean = getServiceEndpoint(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("dataService")) {
            return bean.getDataService();
        } else if(field.equals("serviceName")) {
            return bean.getServiceName();
        } else if(field.equals("serviceUrl")) {
            return bean.getServiceUrl();
        } else if(field.equals("authRequired")) {
            return bean.isAuthRequired();
        } else if(field.equals("authCredential")) {
            return bean.getAuthCredential();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException
    {
        // Temporary implementation
        if(guids == null || guids.isEmpty()) {
            return null;
        }
        List<ServiceEndpoint> beans = new ArrayList<ServiceEndpoint>();
        for(String guid : guids) {
            ServiceEndpoint bean = getServiceEndpoint(guid);
            beans.add(bean);
        }
        return beans;
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException
    {
        return getAllServiceEndpoints(null, null, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpoints(ordering, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        log.finer("BEGIN");

    	List<ServiceEndpoint> list = null;

     	WebResource webResource = getServiceEndpointWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                ServiceEndpointListStub stub = clientResponse.getEntity(ServiceEndpointListStub.class);
                list = MarshalHelper.convertServiceEndpointListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "ServiceEndpoint list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SERVICEENDPOINT);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        log.finer("BEGIN");

    	List<String> list = null;

     	WebResource webResource = getServiceEndpointWebResource(RESOURCE_PATH_ALLKEYS);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
    	
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "ServiceEndpoint key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SERVICEENDPOINT);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        log.finer("BEGIN");

    	List<ServiceEndpoint> list = null;
    	
//        ClientResponse clientResponse = getServiceEndpointWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getServiceEndpointWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                ServiceEndpointListStub stub = clientResponse.getEntity(ServiceEndpointListStub.class);
                list = MarshalHelper.convertServiceEndpointListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "ServiceEndpoint list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SERVICEENDPOINT);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        log.finer("BEGIN");

    	List<String> list = null;

    	WebResource webResource = getServiceEndpointWebResource(RESOURCE_PATH_KEYS);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "ServiceEndpoint key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SERVICEENDPOINT);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.finer("BEGIN");

        Long count = 0L;
     	WebResource webResource = getServiceEndpointWebResource(RESOURCE_PATH_COUNT);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                // Order of the mime type ???
                //clientResponse = webResource.accept(MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML).get(ClientResponse.class);
                clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "ServiceEndpoint count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);  // ???
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SERVICEENDPOINT);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createServiceEndpoint(String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        ServiceEndpointBean bean = new ServiceEndpointBean(null, user, dataService, serviceName, serviceUrl, authRequired, MarshalHelper.convertConsumerKeySecretPairToBean(authCredential), status);
        return createServiceEndpoint(bean);        
    }

    @Override
    public String createServiceEndpoint(ServiceEndpoint bean) throws BaseException
    {
        log.finer("BEGIN");

        String guid = null;
        ServiceEndpointStub stub = MarshalHelper.convertServiceEndpointToStub(bean);
        WebResource webResource = getServiceEndpointWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.type(getInputMediaType()).post(ClientResponse.class, stub);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "New ServiceEndpoint guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New ServiceEndpoint resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SERVICEENDPOINT);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ServiceEndpoint guid is invalid.");
        	throw new BaseException("ServiceEndpoint guid is invalid.");
        }
        ServiceEndpointBean bean = new ServiceEndpointBean(guid, user, dataService, serviceName, serviceUrl, authRequired, MarshalHelper.convertConsumerKeySecretPairToBean(authCredential), status);
        return updateServiceEndpoint(bean);        
    }

    @Override
    public Boolean updateServiceEndpoint(ServiceEndpoint bean) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ServiceEndpoint object is invalid.");
        	throw new BaseException("ServiceEndpoint object is invalid.");
        }
        ServiceEndpointStub stub = MarshalHelper.convertServiceEndpointToStub(bean);

        WebResource webResource = getServiceEndpointWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);
        
        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Successfully updated the object with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SERVICEENDPOINT);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END: false");
        return false;
    }

    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        log.finer("BEGIN");

        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ServiceEndpoint guid is invalid.");
        	throw new BaseException("ServiceEndpoint guid is invalid.");
        }

        WebResource webResource = getServiceEndpointWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.delete(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: guid = " + guid);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: guid = " + guid);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Successfully deleted the object with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SERVICEENDPOINT);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END: false");
        return false;
    }

    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpoint bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deleteServiceEndpoint(guid);
    }

    @Override
    public Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = 0L;
     	WebResource webResource = getServiceEndpointWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exceptionif the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                // Order of the mime types ???
                clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: delete-filter = " + filter);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: delete-filter = " + filter);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Deleted ServiceEndpoints: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class); // ???
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SERVICEENDPOINT);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
