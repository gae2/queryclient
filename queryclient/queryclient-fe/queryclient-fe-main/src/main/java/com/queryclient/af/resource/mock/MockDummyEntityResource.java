package com.queryclient.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.stub.DummyEntityStub;
import com.queryclient.ws.stub.DummyEntityListStub;
import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.resource.DummyEntityResource;


// MockDummyEntityResource is a decorator.
// It can be used as a base class to mock DummyEntityResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/dummyEntities/")
public abstract class MockDummyEntityResource implements DummyEntityResource
{
    private static final Logger log = Logger.getLogger(MockDummyEntityResource.class.getName());

    // MockDummyEntityResource uses the decorator design pattern.
    private DummyEntityResource decoratedResource;

    public MockDummyEntityResource(DummyEntityResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected DummyEntityResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(DummyEntityResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDummyEntities(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findDummyEntitiesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findDummyEntitiesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getDummyEntityAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getDummyEntityAsHtml(guid);
//    }

    @Override
    public Response getDummyEntity(String guid) throws BaseResourceException
    {
        return decoratedResource.getDummyEntity(guid);
    }

    @Override
    public Response getDummyEntityAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getDummyEntityAsJsonp(guid, callback);
    }

    @Override
    public Response getDummyEntity(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getDummyEntity(guid, field);
    }

    // TBD
    @Override
    public Response constructDummyEntity(DummyEntityStub dummyEntity) throws BaseResourceException
    {
        return decoratedResource.constructDummyEntity(dummyEntity);
    }

    @Override
    public Response createDummyEntity(DummyEntityStub dummyEntity) throws BaseResourceException
    {
        return decoratedResource.createDummyEntity(dummyEntity);
    }

//    @Override
//    public Response createDummyEntity(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createDummyEntity(formParams);
//    }

    // TBD
    @Override
    public Response refreshDummyEntity(String guid, DummyEntityStub dummyEntity) throws BaseResourceException
    {
        return decoratedResource.refreshDummyEntity(guid, dummyEntity);
    }

    @Override
    public Response updateDummyEntity(String guid, DummyEntityStub dummyEntity) throws BaseResourceException
    {
        return decoratedResource.updateDummyEntity(guid, dummyEntity);
    }

    @Override
    public Response updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status)
    {
        return decoratedResource.updateDummyEntity(guid, user, name, content, maxLength, expired, status);
    }

//    @Override
//    public Response updateDummyEntity(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateDummyEntity(guid, formParams);
//    }

    @Override
    public Response deleteDummyEntity(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteDummyEntity(guid);
    }

    @Override
    public Response deleteDummyEntities(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteDummyEntities(filter, params, values);
    }


// TBD ....
    @Override
    public Response createDummyEntities(DummyEntityListStub dummyEntities) throws BaseResourceException
    {
        return decoratedResource.createDummyEntities(dummyEntities);
    }


}
