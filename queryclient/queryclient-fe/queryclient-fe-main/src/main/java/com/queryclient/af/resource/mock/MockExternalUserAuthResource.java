package com.queryclient.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.stub.ExternalUserAuthStub;
import com.queryclient.ws.stub.ExternalUserAuthListStub;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalUserAuthBean;
import com.queryclient.af.resource.ExternalUserAuthResource;
import com.queryclient.af.resource.util.GaeAppStructResourceUtil;
import com.queryclient.af.resource.util.ExternalUserIdStructResourceUtil;


// MockExternalUserAuthResource is a decorator.
// It can be used as a base class to mock ExternalUserAuthResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/externalUserAuths/")
public abstract class MockExternalUserAuthResource implements ExternalUserAuthResource
{
    private static final Logger log = Logger.getLogger(MockExternalUserAuthResource.class.getName());

    // MockExternalUserAuthResource uses the decorator design pattern.
    private ExternalUserAuthResource decoratedResource;

    public MockExternalUserAuthResource(ExternalUserAuthResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected ExternalUserAuthResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(ExternalUserAuthResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllExternalUserAuths(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllExternalUserAuths(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllExternalUserAuthKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllExternalUserAuthKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findExternalUserAuthsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findExternalUserAuthsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getExternalUserAuthAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getExternalUserAuthAsHtml(guid);
//    }

    @Override
    public Response getExternalUserAuth(String guid) throws BaseResourceException
    {
        return decoratedResource.getExternalUserAuth(guid);
    }

    @Override
    public Response getExternalUserAuthAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getExternalUserAuthAsJsonp(guid, callback);
    }

    @Override
    public Response getExternalUserAuth(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getExternalUserAuth(guid, field);
    }

    // TBD
    @Override
    public Response constructExternalUserAuth(ExternalUserAuthStub externalUserAuth) throws BaseResourceException
    {
        return decoratedResource.constructExternalUserAuth(externalUserAuth);
    }

    @Override
    public Response createExternalUserAuth(ExternalUserAuthStub externalUserAuth) throws BaseResourceException
    {
        return decoratedResource.createExternalUserAuth(externalUserAuth);
    }

//    @Override
//    public Response createExternalUserAuth(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createExternalUserAuth(formParams);
//    }

    // TBD
    @Override
    public Response refreshExternalUserAuth(String guid, ExternalUserAuthStub externalUserAuth) throws BaseResourceException
    {
        return decoratedResource.refreshExternalUserAuth(guid, externalUserAuth);
    }

    @Override
    public Response updateExternalUserAuth(String guid, ExternalUserAuthStub externalUserAuth) throws BaseResourceException
    {
        return decoratedResource.updateExternalUserAuth(guid, externalUserAuth);
    }

    @Override
    public Response updateExternalUserAuth(String guid, String managerApp, Long appAcl, String gaeApp, String ownerUser, Long userAcl, String user, String providerId, String externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime)
    {
        return decoratedResource.updateExternalUserAuth(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, providerId, externalUserId, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
    }

//    @Override
//    public Response updateExternalUserAuth(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateExternalUserAuth(guid, formParams);
//    }

    @Override
    public Response deleteExternalUserAuth(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteExternalUserAuth(guid);
    }

    @Override
    public Response deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteExternalUserAuths(filter, params, values);
    }


// TBD ....
    @Override
    public Response createExternalUserAuths(ExternalUserAuthListStub externalUserAuths) throws BaseResourceException
    {
        return decoratedResource.createExternalUserAuths(externalUserAuths);
    }


}
