package com.queryclient.fe.bean.mock;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.fe.Validateable;
import com.queryclient.fe.core.StringEscapeUtil;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.fe.bean.QueryRecordJsBean;


// Place holder...
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryRecordMockBean extends QueryRecordJsBean implements Serializable, Cloneable, Validateable  //, QueryRecord
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QueryRecordMockBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public QueryRecordMockBean()
    {
        super();
    }
    public QueryRecordMockBean(String guid)
    {
       super(guid);
    }
    public QueryRecordMockBean(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStructJsBean referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime)
    {
        super(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime);
    }
    public QueryRecordMockBean(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStructJsBean referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime, Long createdTime, Long modifiedTime)
    {
        super(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime, createdTime, modifiedTime);
    }
    public QueryRecordMockBean(QueryRecordJsBean bean)
    {
        super(bean);
    }

    public static QueryRecordMockBean fromJsonString(String jsonStr)
    {
        QueryRecordMockBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, QueryRecordMockBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getQuerySession() == null) {
//            addError("querySession", "querySession is null");
//            allOK = false;
//        }
//        // TBD
//        if(getQueryId() == null) {
//            addError("queryId", "queryId is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDataService() == null) {
//            addError("dataService", "dataService is null");
//            allOK = false;
//        }
//        // TBD
//        if(getServiceUrl() == null) {
//            addError("serviceUrl", "serviceUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(isDelayed() == null) {
//            addError("delayed", "delayed is null");
//            allOK = false;
//        }
//        // TBD
//        if(getQuery() == null) {
//            addError("query", "query is null");
//            allOK = false;
//        }
//        // TBD
//        if(getInputFormat() == null) {
//            addError("inputFormat", "inputFormat is null");
//            allOK = false;
//        }
//        // TBD
//        if(getInputFile() == null) {
//            addError("inputFile", "inputFile is null");
//            allOK = false;
//        }
//        // TBD
//        if(getInputContent() == null) {
//            addError("inputContent", "inputContent is null");
//            allOK = false;
//        }
//        // TBD
//        if(getTargetOutputFormat() == null) {
//            addError("targetOutputFormat", "targetOutputFormat is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOutputFormat() == null) {
//            addError("outputFormat", "outputFormat is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOutputFile() == null) {
//            addError("outputFile", "outputFile is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOutputContent() == null) {
//            addError("outputContent", "outputContent is null");
//            allOK = false;
//        }
//        // TBD
//        if(getResponseCode() == null) {
//            addError("responseCode", "responseCode is null");
//            allOK = false;
//        }
//        // TBD
//        if(getResult() == null) {
//            addError("result", "result is null");
//            allOK = false;
//        }
//        // TBD
//        if(getReferrerInfo() == null) {
//            addError("referrerInfo", "referrerInfo is null");
//            allOK = false;
//        } else {
//            ReferrerInfoStructJsBean referrerInfo = getReferrerInfo();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExtra() == null) {
//            addError("extra", "extra is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNote() == null) {
//            addError("note", "note is null");
//            allOK = false;
//        }
//        // TBD
//        if(getScheduledTime() == null) {
//            addError("scheduledTime", "scheduledTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getProcessedTime() == null) {
//            addError("processedTime", "processedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        QueryRecordMockBean cloned = new QueryRecordMockBean((QueryRecordJsBean) super.clone());
        return cloned;
    }

}
