package com.queryclient.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.stub.GaeUserStructStub;
import com.queryclient.af.bean.GaeUserStructBean;


public class GaeUserStructResourceUtil
{
    private static final Logger log = Logger.getLogger(GaeUserStructResourceUtil.class.getName());

    // Static methods only.
    private GaeUserStructResourceUtil() {}

    public static GaeUserStructBean convertGaeUserStructStubToBean(GaeUserStruct stub)
    {
        GaeUserStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new GaeUserStructBean();
            bean.setAuthDomain(stub.getAuthDomain());
            bean.setFederatedIdentity(stub.getFederatedIdentity());
            bean.setNickname(stub.getNickname());
            bean.setUserId(stub.getUserId());
            bean.setEmail(stub.getEmail());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
