package com.queryclient.util;

import java.util.logging.Logger;


// Temporary
public final class QueryRecordUtil
{
    private static final Logger log = Logger.getLogger(QueryRecordUtil.class.getName());

    private QueryRecordUtil() {}
    
    
//    // TBD: This should be read from config, etc....
//    public static String getDefaultMemoType()
//    {
//        // temporary
//        return ContentDataType.ITEM_TYPE_TEXT;
//    }

    // TBD:
    public static String createDefaultMemoTitle()
    {
        // temporary
        String title = "Query Client " + System.currentTimeMillis();
        return title;
    }

    // TBD:
    public static String createHtmlPageTitle(String memoTitle)
    {
        // temporary
        if(memoTitle == null) {
            memoTitle = "";
        }
        String title = "Query Client - " + memoTitle;
        return title;
    }

    
}
