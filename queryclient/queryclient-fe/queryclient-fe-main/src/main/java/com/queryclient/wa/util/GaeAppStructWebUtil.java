package com.queryclient.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.fe.bean.GaeAppStructJsBean;


public class GaeAppStructWebUtil
{
    private static final Logger log = Logger.getLogger(GaeAppStructWebUtil.class.getName());

    // Static methods only.
    private GaeAppStructWebUtil() {}
    

    public static GaeAppStructJsBean convertGaeAppStructToJsBean(GaeAppStruct gaeAppStruct)
    {
        GaeAppStructJsBean jsBean = null;
        if(gaeAppStruct != null) {
            jsBean = new GaeAppStructJsBean();
            jsBean.setGroupId(gaeAppStruct.getGroupId());
            jsBean.setAppId(gaeAppStruct.getAppId());
            jsBean.setAppDomain(gaeAppStruct.getAppDomain());
            jsBean.setNamespace(gaeAppStruct.getNamespace());
            jsBean.setAcl(gaeAppStruct.getAcl());
            jsBean.setNote(gaeAppStruct.getNote());
        }
        return jsBean;
    }

    public static GaeAppStruct convertGaeAppStructJsBeanToBean(GaeAppStructJsBean jsBean)
    {
        GaeAppStructBean gaeAppStruct = null;
        if(jsBean != null) {
            gaeAppStruct = new GaeAppStructBean();
            gaeAppStruct.setGroupId(jsBean.getGroupId());
            gaeAppStruct.setAppId(jsBean.getAppId());
            gaeAppStruct.setAppDomain(jsBean.getAppDomain());
            gaeAppStruct.setNamespace(jsBean.getNamespace());
            gaeAppStruct.setAcl(jsBean.getAcl());
            gaeAppStruct.setNote(jsBean.getNote());
        }
        return gaeAppStruct;
    }

}
