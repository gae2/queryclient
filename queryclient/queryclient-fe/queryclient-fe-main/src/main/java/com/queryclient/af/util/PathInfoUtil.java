package com.queryclient.af.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;


public class PathInfoUtil
{
    private static final Logger log = Logger.getLogger(PathInfoUtil.class.getName());

    // Special const. to indicate the map.key is a command...
    public static final String TOKEN_COMMAND = "_command_";

    
    private PathInfoUtil() {}


    // Parses pathInfo string, 
    //   e.g., "/key1/val1/key2/val2", or "/cmd1/cmd2/cmd3/key1/val1/key2/val2" 
    // and returns an (ordered) map of { key -> val }
    public static Map<String, String> parsePathInfo(String pathInfo)
    {
        return parsePathInfo(pathInfo, 0);
    }
    public static Map<String, String> parsePathInfo(String pathInfo, int commandCount)
    {
        if(pathInfo == null || pathInfo.isEmpty()) {
            return null;
        }
        // pathInfo = pathInfo.trim();  // pathInfo is already trimmed.

        if(commandCount < 0) {
            commandCount = 0;
        }
        Map<String, String> map = new LinkedHashMap<String, String>();
        if(pathInfo.equals("/")) {
            return map;   // Returns an empty map.
        }
        if(pathInfo.startsWith("/")) {
            pathInfo = pathInfo.substring(1);
        }
        
        String[] parts = pathInfo.split("/");
        int len = parts.length;
        if(commandCount > len) {
            commandCount = len;
        }
        for(int i=0; i<commandCount; i++) {
            String key = parts[i];
            // String value = null;
            String value = TOKEN_COMMAND;
            map.put(key, value);
        }
        for(int i=commandCount; i<len; i+=2) {
            String key = parts[i];
            String value = null;
            int j = i+1;
            if(j < len) {
                value = parts[j];
            }
            map.put(key, value);
        }

        return map;
    }
    
}
