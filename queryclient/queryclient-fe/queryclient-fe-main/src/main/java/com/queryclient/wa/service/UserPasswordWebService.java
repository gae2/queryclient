package com.queryclient.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.af.bean.UserPasswordBean;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.af.service.manager.ServiceManager;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.GaeAppStructJsBean;
import com.queryclient.fe.bean.UserPasswordJsBean;
import com.queryclient.wa.util.GaeAppStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserPasswordWebService // implements UserPasswordService
{
    private static final Logger log = Logger.getLogger(UserPasswordWebService.class.getName());
     
    // Af service interface.
    private UserPasswordService mService = null;

    public UserPasswordWebService()
    {
        this(ServiceManager.getUserPasswordService());
    }
    public UserPasswordWebService(UserPasswordService service)
    {
        mService = service;
    }
    
    private UserPasswordService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getUserPasswordService();
        }
        return mService;
    }
    
    
    public UserPasswordJsBean getUserPassword(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = getService().getUserPassword(guid);
            UserPasswordJsBean bean = convertUserPasswordToJsBean(userPassword);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUserPassword(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getUserPassword(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserPasswordJsBean> getUserPasswords(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserPasswordJsBean> jsBeans = new ArrayList<UserPasswordJsBean>();
            List<UserPassword> userPasswords = getService().getUserPasswords(guids);
            if(userPasswords != null) {
                for(UserPassword userPassword : userPasswords) {
                    jsBeans.add(convertUserPasswordToJsBean(userPassword));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserPasswordJsBean> getAllUserPasswords() throws WebException
    {
        return getAllUserPasswords(null, null, null);
    }

    // @Deprecated
    public List<UserPasswordJsBean> getAllUserPasswords(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllUserPasswords(ordering, offset, count, null);
    }

    public List<UserPasswordJsBean> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserPasswordJsBean> jsBeans = new ArrayList<UserPasswordJsBean>();
            List<UserPassword> userPasswords = getService().getAllUserPasswords(ordering, offset, count, forwardCursor);
            if(userPasswords != null) {
                for(UserPassword userPassword : userPasswords) {
                    jsBeans.add(convertUserPasswordToJsBean(userPassword));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllUserPasswordKeys(ordering, offset, count, null);
    }

    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<UserPasswordJsBean> findUserPasswords(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<UserPasswordJsBean> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<UserPasswordJsBean> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserPasswordJsBean> jsBeans = new ArrayList<UserPasswordJsBean>();
            List<UserPassword> userPasswords = getService().findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(userPasswords != null) {
                for(UserPassword userPassword : userPasswords) {
                    jsBeans.add(convertUserPasswordToJsBean(userPassword));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserPassword(String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws WebException
    {
        try {
            return getService().createUserPassword(managerApp, appAcl, GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserPassword(String jsonStr) throws WebException
    {
        return createUserPassword(UserPasswordJsBean.fromJsonString(jsonStr));
    }

    public String createUserPassword(UserPasswordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = convertUserPasswordJsBeanToBean(jsBean);
            return getService().createUserPassword(userPassword);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserPasswordJsBean constructUserPassword(String jsonStr) throws WebException
    {
        return constructUserPassword(UserPasswordJsBean.fromJsonString(jsonStr));
    }

    public UserPasswordJsBean constructUserPassword(UserPasswordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = convertUserPasswordJsBeanToBean(jsBean);
            userPassword = getService().constructUserPassword(userPassword);
            jsBean = convertUserPasswordToJsBean(userPassword);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUserPassword(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws WebException
    {
        try {
            return getService().updateUserPassword(guid, managerApp, appAcl, GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUserPassword(String jsonStr) throws WebException
    {
        return updateUserPassword(UserPasswordJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateUserPassword(UserPasswordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = convertUserPasswordJsBeanToBean(jsBean);
            return getService().updateUserPassword(userPassword);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserPasswordJsBean refreshUserPassword(String jsonStr) throws WebException
    {
        return refreshUserPassword(UserPasswordJsBean.fromJsonString(jsonStr));
    }

    public UserPasswordJsBean refreshUserPassword(UserPasswordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = convertUserPasswordJsBeanToBean(jsBean);
            userPassword = getService().refreshUserPassword(userPassword);
            jsBean = convertUserPasswordToJsBean(userPassword);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserPassword(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteUserPassword(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserPassword(UserPasswordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = convertUserPasswordJsBeanToBean(jsBean);
            return getService().deleteUserPassword(userPassword);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUserPasswords(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteUserPasswords(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static UserPasswordJsBean convertUserPasswordToJsBean(UserPassword userPassword)
    {
        UserPasswordJsBean jsBean = null;
        if(userPassword != null) {
            jsBean = new UserPasswordJsBean();
            jsBean.setGuid(userPassword.getGuid());
            jsBean.setManagerApp(userPassword.getManagerApp());
            jsBean.setAppAcl(userPassword.getAppAcl());
            jsBean.setGaeApp(GaeAppStructWebUtil.convertGaeAppStructToJsBean(userPassword.getGaeApp()));
            jsBean.setOwnerUser(userPassword.getOwnerUser());
            jsBean.setUserAcl(userPassword.getUserAcl());
            jsBean.setAdmin(userPassword.getAdmin());
            jsBean.setUser(userPassword.getUser());
            jsBean.setUsername(userPassword.getUsername());
            jsBean.setEmail(userPassword.getEmail());
            jsBean.setOpenId(userPassword.getOpenId());
            jsBean.setPlainPassword(userPassword.getPlainPassword());
            jsBean.setHashedPassword(userPassword.getHashedPassword());
            jsBean.setSalt(userPassword.getSalt());
            jsBean.setHashMethod(userPassword.getHashMethod());
            jsBean.setResetRequired(userPassword.isResetRequired());
            jsBean.setChallengeQuestion(userPassword.getChallengeQuestion());
            jsBean.setChallengeAnswer(userPassword.getChallengeAnswer());
            jsBean.setStatus(userPassword.getStatus());
            jsBean.setLastResetTime(userPassword.getLastResetTime());
            jsBean.setExpirationTime(userPassword.getExpirationTime());
            jsBean.setCreatedTime(userPassword.getCreatedTime());
            jsBean.setModifiedTime(userPassword.getModifiedTime());
        }
        return jsBean;
    }

    public static UserPassword convertUserPasswordJsBeanToBean(UserPasswordJsBean jsBean)
    {
        UserPasswordBean userPassword = null;
        if(jsBean != null) {
            userPassword = new UserPasswordBean();
            userPassword.setGuid(jsBean.getGuid());
            userPassword.setManagerApp(jsBean.getManagerApp());
            userPassword.setAppAcl(jsBean.getAppAcl());
            userPassword.setGaeApp(GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(jsBean.getGaeApp()));
            userPassword.setOwnerUser(jsBean.getOwnerUser());
            userPassword.setUserAcl(jsBean.getUserAcl());
            userPassword.setAdmin(jsBean.getAdmin());
            userPassword.setUser(jsBean.getUser());
            userPassword.setUsername(jsBean.getUsername());
            userPassword.setEmail(jsBean.getEmail());
            userPassword.setOpenId(jsBean.getOpenId());
            userPassword.setPlainPassword(jsBean.getPlainPassword());
            userPassword.setHashedPassword(jsBean.getHashedPassword());
            userPassword.setSalt(jsBean.getSalt());
            userPassword.setHashMethod(jsBean.getHashMethod());
            userPassword.setResetRequired(jsBean.isResetRequired());
            userPassword.setChallengeQuestion(jsBean.getChallengeQuestion());
            userPassword.setChallengeAnswer(jsBean.getChallengeAnswer());
            userPassword.setStatus(jsBean.getStatus());
            userPassword.setLastResetTime(jsBean.getLastResetTime());
            userPassword.setExpirationTime(jsBean.getExpirationTime());
            userPassword.setCreatedTime(jsBean.getCreatedTime());
            userPassword.setModifiedTime(jsBean.getModifiedTime());
        }
        return userPassword;
    }

}
