package com.queryclient.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.ws.stub.DataServiceStub;
import com.queryclient.ws.stub.DataServiceListStub;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.af.resource.DataServiceResource;
import com.queryclient.af.resource.util.ConsumerKeySecretPairResourceUtil;
import com.queryclient.af.resource.util.ReferrerInfoStructResourceUtil;


// MockDataServiceResource is a decorator.
// It can be used as a base class to mock DataServiceResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/dataServices/")
public abstract class MockDataServiceResource implements DataServiceResource
{
    private static final Logger log = Logger.getLogger(MockDataServiceResource.class.getName());

    // MockDataServiceResource uses the decorator design pattern.
    private DataServiceResource decoratedResource;

    public MockDataServiceResource(DataServiceResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected DataServiceResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(DataServiceResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDataServices(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDataServiceKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findDataServicesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findDataServicesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getDataServiceAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getDataServiceAsHtml(guid);
//    }

    @Override
    public Response getDataService(String guid) throws BaseResourceException
    {
        return decoratedResource.getDataService(guid);
    }

    @Override
    public Response getDataServiceAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getDataServiceAsJsonp(guid, callback);
    }

    @Override
    public Response getDataService(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getDataService(guid, field);
    }

    // TBD
    @Override
    public Response constructDataService(DataServiceStub dataService) throws BaseResourceException
    {
        return decoratedResource.constructDataService(dataService);
    }

    @Override
    public Response createDataService(DataServiceStub dataService) throws BaseResourceException
    {
        return decoratedResource.createDataService(dataService);
    }

//    @Override
//    public Response createDataService(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createDataService(formParams);
//    }

    // TBD
    @Override
    public Response refreshDataService(String guid, DataServiceStub dataService) throws BaseResourceException
    {
        return decoratedResource.refreshDataService(guid, dataService);
    }

    @Override
    public Response updateDataService(String guid, DataServiceStub dataService) throws BaseResourceException
    {
        return decoratedResource.updateDataService(guid, dataService);
    }

    @Override
    public Response updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, String authCredential, String referrerInfo, String status)
    {
        return decoratedResource.updateDataService(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status);
    }

//    @Override
//    public Response updateDataService(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateDataService(guid, formParams);
//    }

    @Override
    public Response deleteDataService(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteDataService(guid);
    }

    @Override
    public Response deleteDataServices(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteDataServices(filter, params, values);
    }


// TBD ....
    @Override
    public Response createDataServices(DataServiceListStub dataServices) throws BaseResourceException
    {
        return decoratedResource.createDataServices(dataServices);
    }


}
