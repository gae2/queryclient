package com.queryclient.fe.bean.mock;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.fe.Validateable;
import com.queryclient.fe.core.StringEscapeUtil;
import com.queryclient.fe.bean.GaeAppStructJsBean;
import com.queryclient.fe.bean.ExternalUserIdStructJsBean;
import com.queryclient.fe.bean.UserAuthStateJsBean;


// Place holder...
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAuthStateMockBean extends UserAuthStateJsBean implements Serializable, Cloneable, Validateable  //, UserAuthState
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserAuthStateMockBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public UserAuthStateMockBean()
    {
        super();
    }
    public UserAuthStateMockBean(String guid)
    {
       super(guid);
    }
    public UserAuthStateMockBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStructJsBean externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime)
    {
        super(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime);
    }
    public UserAuthStateMockBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStructJsBean externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime, createdTime, modifiedTime);
    }
    public UserAuthStateMockBean(UserAuthStateJsBean bean)
    {
        super(bean);
    }

    public static UserAuthStateMockBean fromJsonString(String jsonStr)
    {
        UserAuthStateMockBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, UserAuthStateMockBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getManagerApp() == null) {
//            addError("managerApp", "managerApp is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAppAcl() == null) {
//            addError("appAcl", "appAcl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getGaeApp() == null) {
//            addError("gaeApp", "gaeApp is null");
//            allOK = false;
//        } else {
//            GaeAppStructJsBean gaeApp = getGaeApp();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getOwnerUser() == null) {
//            addError("ownerUser", "ownerUser is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUserAcl() == null) {
//            addError("userAcl", "userAcl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getProviderId() == null) {
//            addError("providerId", "providerId is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUser() == null) {
//            addError("user", "user is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUsername() == null) {
//            addError("username", "username is null");
//            allOK = false;
//        }
//        // TBD
//        if(getEmail() == null) {
//            addError("email", "email is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOpenId() == null) {
//            addError("openId", "openId is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDeviceId() == null) {
//            addError("deviceId", "deviceId is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSessionId() == null) {
//            addError("sessionId", "sessionId is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAuthToken() == null) {
//            addError("authToken", "authToken is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAuthStatus() == null) {
//            addError("authStatus", "authStatus is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExternalAuth() == null) {
//            addError("externalAuth", "externalAuth is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExternalId() == null) {
//            addError("externalId", "externalId is null");
//            allOK = false;
//        } else {
//            ExternalUserIdStructJsBean externalId = getExternalId();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFirstAuthTime() == null) {
//            addError("firstAuthTime", "firstAuthTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLastAuthTime() == null) {
//            addError("lastAuthTime", "lastAuthTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExpirationTime() == null) {
//            addError("expirationTime", "expirationTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UserAuthStateMockBean cloned = new UserAuthStateMockBean((UserAuthStateJsBean) super.clone());
        return cloned;
    }

}
