package com.queryclient.af.search;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CommonSearchManager
{
    private static final Logger log = Logger.getLogger(CommonSearchManager.class.getName());

    private CommonSearchManager() {}

    // Initialization-on-demand holder.
    private static final class CommonSearchManagerHolder
    {
        private static final CommonSearchManager INSTANCE = new CommonSearchManager();
    }

    // Singleton method
    public static CommonSearchManager getInstance()
    {
        return CommonSearchManagerHolder.INSTANCE;
    }


}
