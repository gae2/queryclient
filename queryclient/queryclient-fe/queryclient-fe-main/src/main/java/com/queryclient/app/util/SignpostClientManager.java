package com.queryclient.app.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;

import com.queryclient.app.endpoint.TargetServiceManager;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.ConsumerKeySecretPair;


public class SignpostClientManager
{
    private static final Logger log = Logger.getLogger(SignpostClientManager.class.getName());
    
    // temporary
    private Map<String, OAuthConsumer> consumerMapCache = null;

    
    private SignpostClientManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static final class SignpostClientManagerHolder
    {
        private static final SignpostClientManager INSTANCE = new SignpostClientManager();
    }

    // Singleton method
    public static SignpostClientManager getInstance()
    {
        return SignpostClientManagerHolder.INSTANCE;
    }

    
    private void init()
    {
        
        // temporary
        consumerMapCache = new HashMap<String, OAuthConsumer>();

        // ...
        
    }

    
    // TBD: ...
    public OAuthConsumer getConsumer(String service) throws BaseException
    {
        OAuthConsumer consumer = null;
        if(consumerMapCache.containsKey(service)) {
            consumer = consumerMapCache.get(service);
        } else {
            // temporary
            ConsumerKeySecretPair pair = TargetServiceManager.getInstance().getKeySecretPair(service);
            if(pair != null) {
                consumer = new DefaultOAuthConsumer(pair.getConsumerKey(), pair.getConsumerSecret());
                consumerMapCache.put(service, consumer);
            } else {
                log.log(Level.WARNING, "Failed to create an OAuthConsumer for service = " + service);
                throw new BaseException("Failed to create an OAuthConsumer for service = " + service);
            }
        }
        
        return consumer;
    }

    
}
