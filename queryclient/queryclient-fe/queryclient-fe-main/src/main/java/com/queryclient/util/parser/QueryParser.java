package com.queryclient.util.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.BaseException;

// Variation/Extension of JDOQL grammar.
// http://db.apache.org/jdo/jdoql.html
// http://www.datanucleus.org/products/accessplatform/jdo/jdoql.html
// SELECT [UNIQUE] [<fields> | <aggregate>] FROM <entity> 
//        [GUID <guid> | WHERE <filter>]  ???
//        [GROUP BY <grouping>]
//        [ORDER BY <order>]
//        [RANGE <start,end> | [[OFFSET <offset>] [COUNT <count>]]]  ???
// CREATE <entity>
//        [GUID <guid>]
//        [VALUE payload]
// UPDATE <entity>
//        GUID <guid>
//        VALUE payload
// DELETE <entity>
//        GUID <guid> | WHERE <filter>  ???
// Note: For <fields>, only a single field is currently supported. For <aggregate>, only 'count(*)' is supported.
// Note: space is important even around keywords, or quoted strings.
// Note: Double quotes (") can be used to make any token "text" type.
public class QueryParser
{
    private static final Logger log = Logger.getLogger(QueryParser.class.getName());

    private String queryStmt;
    private QueryObject queryObj;

    public QueryParser()
    {
        this(null);
    }
    public QueryParser(String queryStmt)
    {
        setQueryStatement(queryStmt);
    }

    public void setQueryStatement(String queryStmt)
    {
        this.queryStmt = queryStmt;
        this.queryObj = null;
    }
    public String getQueryStatement()
    {
        return this.queryStmt;
    }
    
    public QueryObject getQueryObject() throws BaseException
    {
        if(queryObj == null) {
            parse();
        }
        
        return queryObj;
    }

    private void parse() throws BaseException
    {
        if(queryStmt == null || queryStmt.trim().isEmpty()) {
            throw new BaseException("Empty query statement.");
        }
     
        queryStmt = queryStmt.trim();
        int idx = queryStmt.indexOf(" ");
        if(idx < 0) {
            throw new BaseException("Invalid query statement.");            
        }
        
        String command = queryStmt.substring(0, idx).toUpperCase();
        QueryMethod method = null;
        try {
            method = QueryMethod.valueOf(command);
        } catch(Exception e) {
            throw new BaseException("Invalid query method. command = " + command); 
        }

        List<Token> tokens = tokenize(queryStmt);
        if(QueryMethod.SELECT == method) {
            queryObj = parseSelect(tokens);
        } else if(QueryMethod.CREATE == method) {
            queryObj = parseCreate(tokens);
        } else if(QueryMethod.UPDATE == method) {
            queryObj = parseUpdate(tokens);
        } else if(QueryMethod.DELETE == method) {
            queryObj = parseDelete(tokens);
        } else {
            // This cannot happen
            log.warning("Invalid query method. This should not happen.");
        }

    }

    
    // Lexical analyzer... + "sanitizer"
    private static List<Token> tokenize(String stmt)
    {
        List<Token> tokens = new ArrayList<Token>();
        String[] parts = stmt.split("[\\s]+");
        StringBuffer sb = new StringBuffer();
        int length = parts.length;
        boolean withinQuoted = false;
        for(int i=0; i<length; i++) {
            boolean quoteJustStarted = false;
            if(!withinQuoted && parts[i].startsWith("\"")) {
                withinQuoted = true;
                quoteJustStarted = true;
            }
            if(!withinQuoted && JdoqlKeyword.isToken(parts[i])) {
                if(sb.length() > 0) {
                    tokens.add(Token.textToken(sb.toString().trim(), true));
                    sb = new StringBuffer();
                }
                tokens.add(Token.keywordToken(parts[i].toUpperCase()));
            } else {
                if(!withinQuoted && JdoqlKeyword.isKeyword(parts[i])) {
                    if(i < length - 1 && JdoqlKeyword.isToken(parts[i] + " " + parts[i+1])) {
                        if(sb.length() > 0) {
                            tokens.add(Token.textToken(sb.toString().trim(), true));
                            sb = new StringBuffer();
                        }
                        tokens.add(Token.keywordToken(parts[i].toUpperCase() + " " + parts[++i].toUpperCase()));
                    } else {
                        // This is probably a syntax error.
                        // TBD: throw exception ???
                        sb.append(" " + parts[i]);  // ????
                    }
                } else {
                    sb.append(" " + parts[i]);
                }
            }
            if(withinQuoted && parts[i].endsWith("\"") && (!quoteJustStarted || parts[i].length() > 1)) {
                withinQuoted = false;
            }
        }
        if(sb.length() > 0) {
            tokens.add(Token.textToken(sb.toString().trim(), true));
        }

        // For debugging
        if(log.isLoggable(Level.INFO)) {
            StringBuffer strBuff = new StringBuffer();
            strBuff.append("{");
            for(Token t : tokens) {
                strBuff.append(t);
                strBuff.append("; ");
            }
            strBuff.append("}");
            log.info("Token = " + strBuff.toString());
        }

        return tokens;
    }
    
    private static QueryObject parseSelect(List<Token> tokens) throws BaseException
    {
        QueryObject queryObject = new QueryObject();
        queryObject.setMethod(QueryMethod.SELECT);

        int length = tokens.size();
        for(int i=0; i<length; i++) {
            Token t = tokens.get(i);
            if(t.isKeyword()) {
                String keyword = t.getContent();
                if(keyword.equals(JdoqlKeyword.SELECT)) {
                    // [ <fields> | <aggregate> ]
                    if(i < length-1) {
                        Token peeked = tokens.get(i + 1);
                        if(!peeked.isKeyword()) {
                            String peekedContent = peeked.getContent(); 
                            if(JdoqlKeyword.isAggregate(peekedContent)) {
                                queryObject.setAggregate(peekedContent);
                            } else {
                                queryObject.setFields(peekedContent);
                            }
                            ++i;
                        }
                    }
                } else if(keyword.equals(JdoqlKeyword.UNIQUE)) {
                    queryObject.setUnique(true);
                    // [ <fields> | <aggregate> ]
                    if(i < length-1) {
                        Token peeked = tokens.get(i + 1);
                        if(!peeked.isKeyword()) {
                            String peekedContent = peeked.getContent(); 
                            if(JdoqlKeyword.isAggregate(peekedContent)) {
                                queryObject.setAggregate(peekedContent);
                            } else {
                                queryObject.setFields(peekedContent);
                            }
                            ++i;
                        }
                    }
                } else if(keyword.equals(JdoqlKeyword.FROM)) {
                    if(queryObject.getEntity() != null) {
                        throw new ParseException("Parse error: More than one Entity arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Entity value missing");
                    }
                    Token next = tokens.get(++i);
                    if(next.isKeyword()) {
                        // Note: the arg value might be one of the "reserved keywords", and there appears no easy way to tell if this is an error.
                        //throw new ParseException("Parse error: Entity value missing");
                        log.warning("Potential syntax error: Entity value might be missing");
                    }
                    queryObject.setEntity(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.GUID)) {
                    if(queryObject.getFilter() != null) {
                        throw new ParseException("Parse error: Both guid and filter cannot be specified at the same time."); 
                    }
                    if(queryObject.getGuid() != null) {
                        throw new ParseException("Parse error: More than one Guid arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Guid value missing");
                    }
                    Token next = tokens.get(++i);
                    // Guid value cannot be a keyword, even by accident.
                    if(next.isKeyword()) {
                        throw new ParseException("Parse error: Guid value missing");                        
                    }
                    queryObject.setGuid(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.WHERE)) {
                    if(queryObject.getGuid() != null) {
                        throw new ParseException("Parse error: Both guid and filter cannot be specified at the same time."); 
                    }
                    if(queryObject.getFilter() != null) {
                        throw new ParseException("Parse error: More than one Filter arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Filter value missing");
                    }
                    Token next = tokens.get(++i);
                    if(next.isKeyword()) {
                        // Note: the arg value might be one of the "reserved keywords", and there appears no easy way to tell if this is an error.
                        //throw new ParseException("Parse error: Filter arg value missing");
                        log.warning("Potential syntax error: Filter arg value might be missing");
                    }
                    queryObject.setFilter(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.GROUP_BY)) {
                    if(queryObject.getGrouping() != null) {
                        throw new ParseException("Parse error: More than one Group-by arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Group-by arg value missing");
                    }
                    Token next = tokens.get(++i);
                    if(next.isKeyword()) {
                        // Note: the arg value might be one of the "reserved keywords", and there appears no easy way to tell if this is an error.
                        //throw new ParseException("Parse error: Group-by value missing");
                        log.warning("Potential syntax error: Group-by value might be missing");
                    }
                    queryObject.setGrouping(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.ORDER_BY)) {
                    if(queryObject.getOrdering() != null) {
                        throw new ParseException("Parse error: More than one Order-by arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Order-by arg value missing");
                    }
                    Token next = tokens.get(++i);
                    if(next.isKeyword()) {
                        // Note: the arg value might be one of the "reserved keywords", and there appears no easy way to tell if this is an error.
                        //throw new ParseException("Parse error: Order-by value missing");
                        log.warning("Potential syntax error: Order-by value might be missing");
                    }
                    queryObject.setOrdering(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.RANGE)) {
                    if(queryObject.getOffset() != null || queryObject.getCount() != null) {
                        throw new ParseException("Parse error: Both range and offset/count cannot be specified at the same time.");                        
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Range arg value missing");
                    }
                    Token next = tokens.get(++i);
                    // Range value cannot be a keyword, even by accident.
                    if(next.isKeyword()) {
                        throw new ParseException("Parse error: Range arg value missing");
                    }
                    String range = next.getContent();
                    String[] rangeParts = range.split(",\\s");
                    if(rangeParts.length < 2) {
                        throw new ParseException("Parse error: Invalid range arg");                        
                    }
                    Long start = null;
                    Long end = null;
                    try {
                        start = Long.valueOf(rangeParts[0]);
                        end = Long.valueOf(rangeParts[1]);
                        queryObject.setOffset(start);
                        queryObject.setCount((int) (end - start));
                    } catch(Exception e) {
                        throw new ParseException("Parse error: Invalid range arg", e);                         
                    }
                } else if(keyword.equals(JdoqlKeyword.OFFSET)) {
                    if(queryObject.getOffset() != null) {
                        throw new ParseException("Parse error: Offset has been already specified (eg through Range arg)"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Offset arg value missing");
                    }
                    Token next = tokens.get(++i);
                    // Offset value cannot be a keyword, even by accident.
                    if(next.isKeyword()) {
                        throw new ParseException("Parse error: Offset arg value missing");
                    }
                    try {
                        Long offset = Long.valueOf(next.getContent());
                        queryObject.setOffset(offset);
                    } catch(Exception e) {
                        throw new ParseException("Parse error: Invalid offset arg: " + next.getContent(), e);
                    }
                } else if(keyword.equals(JdoqlKeyword.COUNT)) {
                    if(queryObject.getCount() != null) {
                        throw new ParseException("Parse error: Offset has been already specified (eg through Range arg)"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Count arg value missing");
                    }
                    Token next = tokens.get(++i);
                    // Count value cannot be a keyword, even by accident.
                    if(next.isKeyword()) {
                        throw new ParseException("Parse error: Count arg value missing");
                    }
                    try {
                        Integer count = Integer.valueOf(next.getContent());
                        queryObject.setCount(count);
                    } catch(Exception e) {
                        throw new ParseException("Parse error: Invalid count arg: " + next.getContent(), e);
                    }
                } else {
                    // Unrecognized token???
                    log.warning("Unrecognized keyword token: " + t.getContent());
                }
            } else {
                // ignore, and move on to the next token.
                log.warning("Unrecognized text token: " + t.getContent());
            }
        }

        // Check mandatory field(s)
        String entity = queryObject.getEntity();
        if(entity == null || entity.isEmpty()) {
            throw new ParseException("Parse error: Mandatory field (entity) is missing.");            
        }

        return queryObject;
    }
    
    private static QueryObject parseCreate(List<Token> tokens) throws BaseException
    {
        QueryObject queryObject = new QueryObject();
        queryObject.setMethod(QueryMethod.CREATE);

        int length = tokens.size();
        for(int i=0; i<length; i++) {
            Token t = tokens.get(i);
            if(t.isKeyword()) {
                String keyword = t.getContent();
                if(keyword.equals(JdoqlKeyword.CREATE)) {
                    if(queryObject.getEntity() != null) {
                        throw new ParseException("Parse error: More than one Entity arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Entity value missing");
                    }
                    Token next = tokens.get(++i);
                    if(next.isKeyword()) {
                        // Note: the arg value might be one of the "reserved keywords", and there appears no easy way to tell if this is an error.
                        //throw new ParseException("Parse error: Entity value missing");
                        log.warning("Potential syntax error: Entity value might be missing");
                    }
                    queryObject.setEntity(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.GUID)) {
                    if(queryObject.getGuid() != null) {
                        throw new ParseException("Parse error: More than one Guid arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Guid value missing");
                    }
                    Token next = tokens.get(++i);
                    // Guid value cannot be a keyword, even by accident.
                    if(next.isKeyword()) {
                        throw new ParseException("Parse error: Guid value missing");                        
                    }
                    queryObject.setGuid(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.VALUE)) {
                    if(queryObject.getPayload() != null) {
                        throw new ParseException("Parse error: More than one Value arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Payload value missing");
                    }
                    Token next = tokens.get(++i);
                    if(next.isKeyword()) {
                        // Note: the arg value might be one of the "reserved keywords", and there appears no easy way to tell if this is an error.
                        //throw new ParseException("Parse error: Payload value missing");
                        log.warning("Potential syntax error: Payload value might be missing");
                    }
                    queryObject.setPayload(next.getContent());
                } else {
                    // Unrecognized token???
                    log.warning("Unrecognized keyword token: " + t.getContent());
                }
            } else {
                // ignore, and move on to the next token.
                log.warning("Unrecognized text token: " + t.getContent());
            }
        }

        // Check mandatory field(s)
        String entity = queryObject.getEntity();
        if(entity == null || entity.isEmpty()) {
            throw new ParseException("Parse error: Mandatory field (entity) is missing.");            
        }

        return queryObject;
    }
    
    private static QueryObject parseUpdate(List<Token> tokens) throws BaseException
    {
        QueryObject queryObject = new QueryObject();
        queryObject.setMethod(QueryMethod.UPDATE);

        int length = tokens.size();
        for(int i=0; i<length; i++) {
            Token t = tokens.get(i);
            if(t.isKeyword()) {
                String keyword = t.getContent();
                if(keyword.equals(JdoqlKeyword.UPDATE)) {
                    if(queryObject.getEntity() != null) {
                        throw new ParseException("Parse error: More than one Entity arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Entity value missing");
                    }
                    Token next = tokens.get(++i);
                    if(next.isKeyword()) {
                        // Note: the arg value might be one of the "reserved keywords", and there appears no easy way to tell if this is an error.
                        //throw new ParseException("Parse error: Entity value missing");
                        log.warning("Potential syntax error: Entity value might be missing");
                    }
                    queryObject.setEntity(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.GUID)) {
                    if(queryObject.getGuid() != null) {
                        throw new ParseException("Parse error: More than one Guid arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Guid value missing");
                    }
                    Token next = tokens.get(++i);
                    // Guid value cannot be a keyword, even by accident.
                    if(next.isKeyword()) {
                        throw new ParseException("Parse error: Guid value missing");                        
                    }
                    queryObject.setGuid(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.VALUE)) {
                    if(queryObject.getPayload() != null) {
                        throw new ParseException("Parse error: More than one Value arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Payload value missing");
                    }
                    Token next = tokens.get(++i);
                    if(next.isKeyword()) {
                        // Note: the arg value might be one of the "reserved keywords", and there appears no easy way to tell if this is an error.
                        //throw new ParseException("Parse error: Payload value missing");
                        log.warning("Potential syntax error: Payload value might be missing");
                    }
                    queryObject.setPayload(next.getContent());
                } else {
                    // Unrecognized token???
                    log.warning("Unrecognized keyword token: " + t.getContent());
                }
            } else {
                // ignore, and move on to the next token.
                log.warning("Unrecognized text token: " + t.getContent());
            }
        }

        // Check mandatory field(s)
        String entity = queryObject.getEntity();
        if(entity == null || entity.isEmpty()) {
            throw new ParseException("Parse error: Mandatory field (entity) is missing.");            
        }
        String guid = queryObject.getGuid();
        if(guid == null || guid.isEmpty()) {
            throw new ParseException("Parse error: Mandatory field (guid) is missing.");            
        }
        String payload = queryObject.getPayload();
        if(payload == null || payload.isEmpty()) {
            throw new ParseException("Parse error: Mandatory field (payload) is missing.");            
        }

        return queryObject;
    }
    
    private static QueryObject parseDelete(List<Token> tokens) throws BaseException
    {
        QueryObject queryObject = new QueryObject();
        queryObject.setMethod(QueryMethod.DELETE);

        int length = tokens.size();
        for(int i=0; i<length; i++) {
            Token t = tokens.get(i);
            if(t.isKeyword()) {
                String keyword = t.getContent();
                if(keyword.equals(JdoqlKeyword.DELETE)) {
                    if(queryObject.getEntity() != null) {
                        throw new ParseException("Parse error: More than one Entity arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Entity value missing");
                    }
                    Token next = tokens.get(++i);
                    if(next.isKeyword()) {
                        // Note: the arg value might be one of the "reserved keywords", and there appears no easy way to tell if this is an error.
                        //throw new ParseException("Parse error: Entity value missing");
                        log.warning("Potential syntax error: Entity value might be missing");
                    }
                    queryObject.setEntity(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.GUID)) {
                    if(queryObject.getFilter() != null) {
                        throw new ParseException("Parse error: Both guid and filter cannot be specified at the same time."); 
                    }
                    if(queryObject.getGuid() != null) {
                        throw new ParseException("Parse error: More than one Guid arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Guid value missing");
                    }
                    Token next = tokens.get(++i);
                    // Guid value cannot be a keyword, even by accident.
                    if(next.isKeyword()) {
                        throw new ParseException("Parse error: Guid value missing");                        
                    }
                    queryObject.setGuid(next.getContent());
                } else if(keyword.equals(JdoqlKeyword.WHERE)) {
                    if(queryObject.getGuid() != null) {
                        throw new ParseException("Parse error: Both guid and filter cannot be specified at the same time."); 
                    }
                    if(queryObject.getFilter() != null) {
                        throw new ParseException("Parse error: More than one Filter arg found"); 
                    }
                    if(i >= length-1) {
                        throw new ParseException("Parse error: Filter value missing");
                    }
                    Token next = tokens.get(++i);
                    if(next.isKeyword()) {
                        // Note: the arg value might be one of the "reserved keywords", and there appears no easy way to tell if this is an error.
                        //throw new ParseException("Parse error: Filter arg value missing");
                        log.warning("Potential syntax error: Filter arg value might be missing");
                    }
                    queryObject.setFilter(next.getContent());
                } else {
                    // Unrecognized token???
                    log.warning("Unrecognized keyword token: " + t.getContent());
                }
            } else {
                // ignore, and move on to the next token.
                log.warning("Unrecognized text token: " + t.getContent());
            }
        }

        // Check mandatory field(s)
        String entity = queryObject.getEntity();
        if(entity == null || entity.isEmpty()) {
            throw new ParseException("Parse error: Mandatory field (entity) is missing.");            
        }
        String guid = queryObject.getGuid();
        String filter = queryObject.getFilter();
        if((guid == null || guid.isEmpty()) && (filter == null || filter.isEmpty())) {
            throw new ParseException("Parse error: Mandatory field (guid or filter) is missing.");            
        }

        return queryObject;
    }


}
