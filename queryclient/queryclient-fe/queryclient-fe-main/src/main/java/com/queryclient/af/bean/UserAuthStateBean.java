package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.stub.GaeAppStructStub;
import com.queryclient.ws.stub.ExternalUserIdStructStub;
import com.queryclient.ws.UserAuthState;
import com.queryclient.ws.stub.UserAuthStateStub;


// Wrapper class + bean combo.
public class UserAuthStateBean implements UserAuthState, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserAuthStateBean.class.getName());

    // [1] With an embedded object.
    private UserAuthStateStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String providerId;
    private String user;
    private String username;
    private String email;
    private String openId;
    private String deviceId;
    private String sessionId;
    private String authToken;
    private String authStatus;
    private String externalAuth;
    private ExternalUserIdStructBean externalId;
    private String status;
    private Long firstAuthTime;
    private Long lastAuthTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserAuthStateBean()
    {
        //this((String) null);
    }
    public UserAuthStateBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserAuthStateBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStructBean externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime, null, null);
    }
    public UserAuthStateBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStructBean externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.providerId = providerId;
        this.user = user;
        this.username = username;
        this.email = email;
        this.openId = openId;
        this.deviceId = deviceId;
        this.sessionId = sessionId;
        this.authToken = authToken;
        this.authStatus = authStatus;
        this.externalAuth = externalAuth;
        this.externalId = externalId;
        this.status = status;
        this.firstAuthTime = firstAuthTime;
        this.lastAuthTime = lastAuthTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserAuthStateBean(UserAuthState stub)
    {
        if(stub instanceof UserAuthStateStub) {
            this.stub = (UserAuthStateStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setManagerApp(stub.getManagerApp());   
            setAppAcl(stub.getAppAcl());   
            GaeAppStruct gaeApp = stub.getGaeApp();
            if(gaeApp instanceof GaeAppStructBean) {
                setGaeApp((GaeAppStructBean) gaeApp);   
            } else {
                setGaeApp(new GaeAppStructBean(gaeApp));   
            }
            setOwnerUser(stub.getOwnerUser());   
            setUserAcl(stub.getUserAcl());   
            setProviderId(stub.getProviderId());   
            setUser(stub.getUser());   
            setUsername(stub.getUsername());   
            setEmail(stub.getEmail());   
            setOpenId(stub.getOpenId());   
            setDeviceId(stub.getDeviceId());   
            setSessionId(stub.getSessionId());   
            setAuthToken(stub.getAuthToken());   
            setAuthStatus(stub.getAuthStatus());   
            setExternalAuth(stub.getExternalAuth());   
            ExternalUserIdStruct externalId = stub.getExternalId();
            if(externalId instanceof ExternalUserIdStructBean) {
                setExternalId((ExternalUserIdStructBean) externalId);   
            } else {
                setExternalId(new ExternalUserIdStructBean(externalId));   
            }
            setStatus(stub.getStatus());   
            setFirstAuthTime(stub.getFirstAuthTime());   
            setLastAuthTime(stub.getLastAuthTime());   
            setExpirationTime(stub.getExpirationTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getManagerApp()
    {
        if(getStub() != null) {
            return getStub().getManagerApp();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.managerApp;
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getStub() != null) {
            getStub().setManagerApp(managerApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.managerApp = managerApp;
        }
    }

    public Long getAppAcl()
    {
        if(getStub() != null) {
            return getStub().getAppAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appAcl;
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getStub() != null) {
            getStub().setAppAcl(appAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appAcl = appAcl;
        }
    }

    public GaeAppStruct getGaeApp()
    {  
        if(getStub() != null) {
            // Note the object type.
            GaeAppStruct _stub_field = getStub().getGaeApp();
            if(_stub_field == null) {
                return null;
            } else {
                return new GaeAppStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.gaeApp;
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGaeApp(gaeApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(gaeApp == null) {
                this.gaeApp = null;
            } else {
                if(gaeApp instanceof GaeAppStructBean) {
                    this.gaeApp = (GaeAppStructBean) gaeApp;
                } else {
                    this.gaeApp = new GaeAppStructBean(gaeApp);
                }
            }
        }
    }

    public String getOwnerUser()
    {
        if(getStub() != null) {
            return getStub().getOwnerUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ownerUser;
        }
    }
    public void setOwnerUser(String ownerUser)
    {
        if(getStub() != null) {
            getStub().setOwnerUser(ownerUser);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ownerUser = ownerUser;
        }
    }

    public Long getUserAcl()
    {
        if(getStub() != null) {
            return getStub().getUserAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userAcl;
        }
    }
    public void setUserAcl(Long userAcl)
    {
        if(getStub() != null) {
            getStub().setUserAcl(userAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userAcl = userAcl;
        }
    }

    public String getProviderId()
    {
        if(getStub() != null) {
            return getStub().getProviderId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.providerId;
        }
    }
    public void setProviderId(String providerId)
    {
        if(getStub() != null) {
            getStub().setProviderId(providerId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.providerId = providerId;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getUsername()
    {
        if(getStub() != null) {
            return getStub().getUsername();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.username;
        }
    }
    public void setUsername(String username)
    {
        if(getStub() != null) {
            getStub().setUsername(username);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.username = username;
        }
    }

    public String getEmail()
    {
        if(getStub() != null) {
            return getStub().getEmail();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.email;
        }
    }
    public void setEmail(String email)
    {
        if(getStub() != null) {
            getStub().setEmail(email);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.email = email;
        }
    }

    public String getOpenId()
    {
        if(getStub() != null) {
            return getStub().getOpenId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.openId;
        }
    }
    public void setOpenId(String openId)
    {
        if(getStub() != null) {
            getStub().setOpenId(openId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.openId = openId;
        }
    }

    public String getDeviceId()
    {
        if(getStub() != null) {
            return getStub().getDeviceId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.deviceId;
        }
    }
    public void setDeviceId(String deviceId)
    {
        if(getStub() != null) {
            getStub().setDeviceId(deviceId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.deviceId = deviceId;
        }
    }

    public String getSessionId()
    {
        if(getStub() != null) {
            return getStub().getSessionId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.sessionId;
        }
    }
    public void setSessionId(String sessionId)
    {
        if(getStub() != null) {
            getStub().setSessionId(sessionId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.sessionId = sessionId;
        }
    }

    public String getAuthToken()
    {
        if(getStub() != null) {
            return getStub().getAuthToken();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.authToken;
        }
    }
    public void setAuthToken(String authToken)
    {
        if(getStub() != null) {
            getStub().setAuthToken(authToken);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.authToken = authToken;
        }
    }

    public String getAuthStatus()
    {
        if(getStub() != null) {
            return getStub().getAuthStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.authStatus;
        }
    }
    public void setAuthStatus(String authStatus)
    {
        if(getStub() != null) {
            getStub().setAuthStatus(authStatus);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.authStatus = authStatus;
        }
    }

    public String getExternalAuth()
    {
        if(getStub() != null) {
            return getStub().getExternalAuth();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.externalAuth;
        }
    }
    public void setExternalAuth(String externalAuth)
    {
        if(getStub() != null) {
            getStub().setExternalAuth(externalAuth);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.externalAuth = externalAuth;
        }
    }

    public ExternalUserIdStruct getExternalId()
    {  
        if(getStub() != null) {
            // Note the object type.
            ExternalUserIdStruct _stub_field = getStub().getExternalId();
            if(_stub_field == null) {
                return null;
            } else {
                return new ExternalUserIdStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.externalId;
        }
    }
    public void setExternalId(ExternalUserIdStruct externalId)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setExternalId(externalId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(externalId == null) {
                this.externalId = null;
            } else {
                if(externalId instanceof ExternalUserIdStructBean) {
                    this.externalId = (ExternalUserIdStructBean) externalId;
                } else {
                    this.externalId = new ExternalUserIdStructBean(externalId);
                }
            }
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getFirstAuthTime()
    {
        if(getStub() != null) {
            return getStub().getFirstAuthTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.firstAuthTime;
        }
    }
    public void setFirstAuthTime(Long firstAuthTime)
    {
        if(getStub() != null) {
            getStub().setFirstAuthTime(firstAuthTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.firstAuthTime = firstAuthTime;
        }
    }

    public Long getLastAuthTime()
    {
        if(getStub() != null) {
            return getStub().getLastAuthTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastAuthTime;
        }
    }
    public void setLastAuthTime(Long lastAuthTime)
    {
        if(getStub() != null) {
            getStub().setLastAuthTime(lastAuthTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastAuthTime = lastAuthTime;
        }
    }

    public Long getExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getExpirationTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationTime;
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getStub() != null) {
            getStub().setExpirationTime(expirationTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationTime = expirationTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public UserAuthStateStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UserAuthStateStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("managerApp = " + this.managerApp).append(";");
            sb.append("appAcl = " + this.appAcl).append(";");
            sb.append("gaeApp = " + this.gaeApp).append(";");
            sb.append("ownerUser = " + this.ownerUser).append(";");
            sb.append("userAcl = " + this.userAcl).append(";");
            sb.append("providerId = " + this.providerId).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("username = " + this.username).append(";");
            sb.append("email = " + this.email).append(";");
            sb.append("openId = " + this.openId).append(";");
            sb.append("deviceId = " + this.deviceId).append(";");
            sb.append("sessionId = " + this.sessionId).append(";");
            sb.append("authToken = " + this.authToken).append(";");
            sb.append("authStatus = " + this.authStatus).append(";");
            sb.append("externalAuth = " + this.externalAuth).append(";");
            sb.append("externalId = " + this.externalId).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("firstAuthTime = " + this.firstAuthTime).append(";");
            sb.append("lastAuthTime = " + this.lastAuthTime).append(";");
            sb.append("expirationTime = " + this.expirationTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = managerApp == null ? 0 : managerApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = appAcl == null ? 0 : appAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = gaeApp == null ? 0 : gaeApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = ownerUser == null ? 0 : ownerUser.hashCode();
            _hash = 31 * _hash + delta;
            delta = userAcl == null ? 0 : userAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = providerId == null ? 0 : providerId.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = username == null ? 0 : username.hashCode();
            _hash = 31 * _hash + delta;
            delta = email == null ? 0 : email.hashCode();
            _hash = 31 * _hash + delta;
            delta = openId == null ? 0 : openId.hashCode();
            _hash = 31 * _hash + delta;
            delta = deviceId == null ? 0 : deviceId.hashCode();
            _hash = 31 * _hash + delta;
            delta = sessionId == null ? 0 : sessionId.hashCode();
            _hash = 31 * _hash + delta;
            delta = authToken == null ? 0 : authToken.hashCode();
            _hash = 31 * _hash + delta;
            delta = authStatus == null ? 0 : authStatus.hashCode();
            _hash = 31 * _hash + delta;
            delta = externalAuth == null ? 0 : externalAuth.hashCode();
            _hash = 31 * _hash + delta;
            delta = externalId == null ? 0 : externalId.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = firstAuthTime == null ? 0 : firstAuthTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastAuthTime == null ? 0 : lastAuthTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationTime == null ? 0 : expirationTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
