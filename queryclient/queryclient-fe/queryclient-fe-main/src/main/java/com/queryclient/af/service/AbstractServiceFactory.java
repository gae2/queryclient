package com.queryclient.af.service;

public abstract class AbstractServiceFactory
{
    public abstract ApiConsumerService getApiConsumerService();
    public abstract UserService getUserService();
    public abstract UserPasswordService getUserPasswordService();
    public abstract ExternalUserAuthService getExternalUserAuthService();
    public abstract UserAuthStateService getUserAuthStateService();
    public abstract DataServiceService getDataServiceService();
    public abstract ServiceEndpointService getServiceEndpointService();
    public abstract QuerySessionService getQuerySessionService();
    public abstract QueryRecordService getQueryRecordService();
    public abstract DummyEntityService getDummyEntityService();
    public abstract ServiceInfoService getServiceInfoService();
    public abstract FiveTenService getFiveTenService();

}
