package com.queryclient.af.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ExceptionUtil
{
    private static final Logger log = Logger.getLogger(ExceptionUtil.class.getName());

    // Static methods only.
    private ExceptionUtil() {}

    
    // Utility method...
    public static String getStackTrace(Throwable e)
    {
    	if(e == null) {
    		return "";   // Return null?
    	}
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

}
