package com.queryclient.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.QueryRecordService;


// QueryRecordMockService is a decorator.
// It can be used as a base class to mock QueryRecordService objects.
public abstract class QueryRecordMockService implements QueryRecordService
{
    private static final Logger log = Logger.getLogger(QueryRecordMockService.class.getName());

    // QueryRecordMockService uses the decorator design pattern.
    private QueryRecordService decoratedService;

    public QueryRecordMockService(QueryRecordService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected QueryRecordService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(QueryRecordService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // QueryRecord related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public QueryRecord getQueryRecord(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getQueryRecord(): guid = " + guid);
        QueryRecord bean = decoratedService.getQueryRecord(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getQueryRecord(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getQueryRecord(guid, field);
        return obj;
    }

    @Override
    public List<QueryRecord> getQueryRecords(List<String> guids) throws BaseException
    {
        log.fine("getQueryRecords()");
        List<QueryRecord> queryRecords = decoratedService.getQueryRecords(guids);
        log.finer("END");
        return queryRecords;
    }

    @Override
    public List<QueryRecord> getAllQueryRecords() throws BaseException
    {
        return getAllQueryRecords(null, null, null);
    }


    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecords(ordering, offset, count, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQueryRecords(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<QueryRecord> queryRecords = decoratedService.getAllQueryRecords(ordering, offset, count, forwardCursor);
        log.finer("END");
        return queryRecords;
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllQueryRecordKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QueryRecordMockService.findQueryRecords(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<QueryRecord> queryRecords = decoratedService.findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return queryRecords;
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QueryRecordMockService.findQueryRecordKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("QueryRecordMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createQueryRecord(String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        QueryRecordBean bean = new QueryRecordBean(null, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfoBean, status, extra, note, scheduledTime, processedTime);
        return createQueryRecord(bean);
    }

    @Override
    public String createQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createQueryRecord(queryRecord);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public QueryRecord constructQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");
        QueryRecord bean = decoratedService.constructQueryRecord(queryRecord);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        QueryRecordBean bean = new QueryRecordBean(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfoBean, status, extra, note, scheduledTime, processedTime);
        return updateQueryRecord(bean);
    }
        
    @Override
    public Boolean updateQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateQueryRecord(queryRecord);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public QueryRecord refreshQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");
        QueryRecord bean = decoratedService.refreshQueryRecord(queryRecord);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteQueryRecord(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteQueryRecord(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteQueryRecord(queryRecord);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteQueryRecords(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteQueryRecords(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createQueryRecords(queryRecords);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    //{
    //}

}
