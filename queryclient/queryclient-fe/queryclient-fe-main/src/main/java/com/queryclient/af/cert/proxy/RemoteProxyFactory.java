package com.queryclient.af.cert.proxy;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.cert.proxy.PublicCertificateInfoServiceProxy;


public class RemoteProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(RemoteProxyFactory.class.getName());

    private RemoteProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class RemoteProxyFactoryHolder
    {
        private static final RemoteProxyFactory INSTANCE = new RemoteProxyFactory();
    }

    // Singleton method
    public static RemoteProxyFactory getInstance()
    {
        return RemoteProxyFactoryHolder.INSTANCE;
    }

    @Override
    public PublicCertificateInfoServiceProxy getPublicCertificateInfoServiceProxy()
    {
        return new RemotePublicCertificateInfoServiceProxy();
    }

}
