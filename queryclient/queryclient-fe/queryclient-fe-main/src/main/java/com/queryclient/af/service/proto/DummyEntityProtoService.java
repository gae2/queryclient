package com.queryclient.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.DummyEntity;
import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.service.impl.DummyEntityServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class DummyEntityProtoService extends DummyEntityServiceImpl implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(DummyEntityProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public DummyEntityProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // DummyEntity related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public DummyEntity getDummyEntity(String guid) throws BaseException
    {
        return super.getDummyEntity(guid);
    }

    @Override
    public Object getDummyEntity(String guid, String field) throws BaseException
    {
        return super.getDummyEntity(guid, field);
    }

    @Override
    public List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException
    {
        return super.getDummyEntities(guids);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities() throws BaseException
    {
        return super.getAllDummyEntities();
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllDummyEntities(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        return super.createDummyEntity(dummyEntity);
    }

    @Override
    public DummyEntity constructDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        return super.constructDummyEntity(dummyEntity);
    }


    @Override
    public Boolean updateDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        return super.updateDummyEntity(dummyEntity);
    }
        
    @Override
    public DummyEntity refreshDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        return super.refreshDummyEntity(dummyEntity);
    }

    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        return super.deleteDummyEntity(guid);
    }

    @Override
    public Boolean deleteDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        return super.deleteDummyEntity(dummyEntity);
    }

    @Override
    public Integer createDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    {
        return super.createDummyEntities(dummyEntities);
    }

    // TBD
    //@Override
    //public Boolean updateDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    //{
    //}

}
