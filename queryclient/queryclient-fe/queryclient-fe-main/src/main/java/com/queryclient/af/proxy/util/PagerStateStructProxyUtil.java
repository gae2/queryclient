package com.queryclient.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.PagerStateStruct;
// import com.queryclient.ws.bean.PagerStateStructBean;
import com.queryclient.af.bean.PagerStateStructBean;


public class PagerStateStructProxyUtil
{
    private static final Logger log = Logger.getLogger(PagerStateStructProxyUtil.class.getName());

    // Static methods only.
    private PagerStateStructProxyUtil() {}

    public static PagerStateStructBean convertServerPagerStateStructBeanToAppBean(PagerStateStruct serverBean)
    {
        PagerStateStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new PagerStateStructBean();
            bean.setPagerMode(serverBean.getPagerMode());
            bean.setPrimaryOrdering(serverBean.getPrimaryOrdering());
            bean.setSecondaryOrdering(serverBean.getSecondaryOrdering());
            bean.setCurrentOffset(serverBean.getCurrentOffset());
            bean.setCurrentPage(serverBean.getCurrentPage());
            bean.setPageSize(serverBean.getPageSize());
            bean.setTotalCount(serverBean.getTotalCount());
            bean.setLowerBoundTotalCount(serverBean.getLowerBoundTotalCount());
            bean.setPreviousPageOffset(serverBean.getPreviousPageOffset());
            bean.setNextPageOffset(serverBean.getNextPageOffset());
            bean.setLastPageOffset(serverBean.getLastPageOffset());
            bean.setLastPageIndex(serverBean.getLastPageIndex());
            bean.setFirstActionEnabled(serverBean.isFirstActionEnabled());
            bean.setPreviousActionEnabled(serverBean.isPreviousActionEnabled());
            bean.setNextActionEnabled(serverBean.isNextActionEnabled());
            bean.setLastActionEnabled(serverBean.isLastActionEnabled());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.queryclient.ws.bean.PagerStateStructBean convertAppPagerStateStructBeanToServerBean(PagerStateStruct appBean)
    {
        com.queryclient.ws.bean.PagerStateStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.PagerStateStructBean();
            bean.setPagerMode(appBean.getPagerMode());
            bean.setPrimaryOrdering(appBean.getPrimaryOrdering());
            bean.setSecondaryOrdering(appBean.getSecondaryOrdering());
            bean.setCurrentOffset(appBean.getCurrentOffset());
            bean.setCurrentPage(appBean.getCurrentPage());
            bean.setPageSize(appBean.getPageSize());
            bean.setTotalCount(appBean.getTotalCount());
            bean.setLowerBoundTotalCount(appBean.getLowerBoundTotalCount());
            bean.setPreviousPageOffset(appBean.getPreviousPageOffset());
            bean.setNextPageOffset(appBean.getNextPageOffset());
            bean.setLastPageOffset(appBean.getLastPageOffset());
            bean.setLastPageIndex(appBean.getLastPageIndex());
            bean.setFirstActionEnabled(appBean.isFirstActionEnabled());
            bean.setPreviousActionEnabled(appBean.isPreviousActionEnabled());
            bean.setNextActionEnabled(appBean.isNextActionEnabled());
            bean.setLastActionEnabled(appBean.isLastActionEnabled());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
