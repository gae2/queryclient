package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.stub.QueryRecordStub;


// Wrapper class + bean combo.
public class QueryRecordBean implements QueryRecord, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QueryRecordBean.class.getName());

    // [1] With an embedded object.
    private QueryRecordStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String querySession;
    private Integer queryId;
    private String dataService;
    private String serviceUrl;
    private Boolean delayed;
    private String query;
    private String inputFormat;
    private String inputFile;
    private String inputContent;
    private String targetOutputFormat;
    private String outputFormat;
    private String outputFile;
    private String outputContent;
    private Integer responseCode;
    private String result;
    private ReferrerInfoStructBean referrerInfo;
    private String status;
    private String extra;
    private String note;
    private Long scheduledTime;
    private Long processedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public QueryRecordBean()
    {
        //this((String) null);
    }
    public QueryRecordBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public QueryRecordBean(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStructBean referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime)
    {
        this(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime, null, null);
    }
    public QueryRecordBean(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStructBean referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.querySession = querySession;
        this.queryId = queryId;
        this.dataService = dataService;
        this.serviceUrl = serviceUrl;
        this.delayed = delayed;
        this.query = query;
        this.inputFormat = inputFormat;
        this.inputFile = inputFile;
        this.inputContent = inputContent;
        this.targetOutputFormat = targetOutputFormat;
        this.outputFormat = outputFormat;
        this.outputFile = outputFile;
        this.outputContent = outputContent;
        this.responseCode = responseCode;
        this.result = result;
        this.referrerInfo = referrerInfo;
        this.status = status;
        this.extra = extra;
        this.note = note;
        this.scheduledTime = scheduledTime;
        this.processedTime = processedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public QueryRecordBean(QueryRecord stub)
    {
        if(stub instanceof QueryRecordStub) {
            this.stub = (QueryRecordStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setQuerySession(stub.getQuerySession());   
            setQueryId(stub.getQueryId());   
            setDataService(stub.getDataService());   
            setServiceUrl(stub.getServiceUrl());   
            setDelayed(stub.isDelayed());   
            setQuery(stub.getQuery());   
            setInputFormat(stub.getInputFormat());   
            setInputFile(stub.getInputFile());   
            setInputContent(stub.getInputContent());   
            setTargetOutputFormat(stub.getTargetOutputFormat());   
            setOutputFormat(stub.getOutputFormat());   
            setOutputFile(stub.getOutputFile());   
            setOutputContent(stub.getOutputContent());   
            setResponseCode(stub.getResponseCode());   
            setResult(stub.getResult());   
            ReferrerInfoStruct referrerInfo = stub.getReferrerInfo();
            if(referrerInfo instanceof ReferrerInfoStructBean) {
                setReferrerInfo((ReferrerInfoStructBean) referrerInfo);   
            } else {
                setReferrerInfo(new ReferrerInfoStructBean(referrerInfo));   
            }
            setStatus(stub.getStatus());   
            setExtra(stub.getExtra());   
            setNote(stub.getNote());   
            setScheduledTime(stub.getScheduledTime());   
            setProcessedTime(stub.getProcessedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getQuerySession()
    {
        if(getStub() != null) {
            return getStub().getQuerySession();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.querySession;
        }
    }
    public void setQuerySession(String querySession)
    {
        if(getStub() != null) {
            getStub().setQuerySession(querySession);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.querySession = querySession;
        }
    }

    public Integer getQueryId()
    {
        if(getStub() != null) {
            return getStub().getQueryId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.queryId;
        }
    }
    public void setQueryId(Integer queryId)
    {
        if(getStub() != null) {
            getStub().setQueryId(queryId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.queryId = queryId;
        }
    }

    public String getDataService()
    {
        if(getStub() != null) {
            return getStub().getDataService();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.dataService;
        }
    }
    public void setDataService(String dataService)
    {
        if(getStub() != null) {
            getStub().setDataService(dataService);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.dataService = dataService;
        }
    }

    public String getServiceUrl()
    {
        if(getStub() != null) {
            return getStub().getServiceUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceUrl;
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getStub() != null) {
            getStub().setServiceUrl(serviceUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceUrl = serviceUrl;
        }
    }

    public Boolean isDelayed()
    {
        if(getStub() != null) {
            return getStub().isDelayed();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.delayed;
        }
    }
    public void setDelayed(Boolean delayed)
    {
        if(getStub() != null) {
            getStub().setDelayed(delayed);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.delayed = delayed;
        }
    }

    public String getQuery()
    {
        if(getStub() != null) {
            return getStub().getQuery();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.query;
        }
    }
    public void setQuery(String query)
    {
        if(getStub() != null) {
            getStub().setQuery(query);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.query = query;
        }
    }

    public String getInputFormat()
    {
        if(getStub() != null) {
            return getStub().getInputFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.inputFormat;
        }
    }
    public void setInputFormat(String inputFormat)
    {
        if(getStub() != null) {
            getStub().setInputFormat(inputFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.inputFormat = inputFormat;
        }
    }

    public String getInputFile()
    {
        if(getStub() != null) {
            return getStub().getInputFile();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.inputFile;
        }
    }
    public void setInputFile(String inputFile)
    {
        if(getStub() != null) {
            getStub().setInputFile(inputFile);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.inputFile = inputFile;
        }
    }

    public String getInputContent()
    {
        if(getStub() != null) {
            return getStub().getInputContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.inputContent;
        }
    }
    public void setInputContent(String inputContent)
    {
        if(getStub() != null) {
            getStub().setInputContent(inputContent);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.inputContent = inputContent;
        }
    }

    public String getTargetOutputFormat()
    {
        if(getStub() != null) {
            return getStub().getTargetOutputFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.targetOutputFormat;
        }
    }
    public void setTargetOutputFormat(String targetOutputFormat)
    {
        if(getStub() != null) {
            getStub().setTargetOutputFormat(targetOutputFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.targetOutputFormat = targetOutputFormat;
        }
    }

    public String getOutputFormat()
    {
        if(getStub() != null) {
            return getStub().getOutputFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputFormat;
        }
    }
    public void setOutputFormat(String outputFormat)
    {
        if(getStub() != null) {
            getStub().setOutputFormat(outputFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputFormat = outputFormat;
        }
    }

    public String getOutputFile()
    {
        if(getStub() != null) {
            return getStub().getOutputFile();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputFile;
        }
    }
    public void setOutputFile(String outputFile)
    {
        if(getStub() != null) {
            getStub().setOutputFile(outputFile);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputFile = outputFile;
        }
    }

    public String getOutputContent()
    {
        if(getStub() != null) {
            return getStub().getOutputContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputContent;
        }
    }
    public void setOutputContent(String outputContent)
    {
        if(getStub() != null) {
            getStub().setOutputContent(outputContent);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputContent = outputContent;
        }
    }

    public Integer getResponseCode()
    {
        if(getStub() != null) {
            return getStub().getResponseCode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.responseCode;
        }
    }
    public void setResponseCode(Integer responseCode)
    {
        if(getStub() != null) {
            getStub().setResponseCode(responseCode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.responseCode = responseCode;
        }
    }

    public String getResult()
    {
        if(getStub() != null) {
            return getStub().getResult();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.result;
        }
    }
    public void setResult(String result)
    {
        if(getStub() != null) {
            getStub().setResult(result);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.result = result;
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {  
        if(getStub() != null) {
            // Note the object type.
            ReferrerInfoStruct _stub_field = getStub().getReferrerInfo();
            if(_stub_field == null) {
                return null;
            } else {
                return new ReferrerInfoStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referrerInfo;
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setReferrerInfo(referrerInfo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(referrerInfo == null) {
                this.referrerInfo = null;
            } else {
                if(referrerInfo instanceof ReferrerInfoStructBean) {
                    this.referrerInfo = (ReferrerInfoStructBean) referrerInfo;
                } else {
                    this.referrerInfo = new ReferrerInfoStructBean(referrerInfo);
                }
            }
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getExtra()
    {
        if(getStub() != null) {
            return getStub().getExtra();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.extra;
        }
    }
    public void setExtra(String extra)
    {
        if(getStub() != null) {
            getStub().setExtra(extra);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.extra = extra;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getScheduledTime()
    {
        if(getStub() != null) {
            return getStub().getScheduledTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.scheduledTime;
        }
    }
    public void setScheduledTime(Long scheduledTime)
    {
        if(getStub() != null) {
            getStub().setScheduledTime(scheduledTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.scheduledTime = scheduledTime;
        }
    }

    public Long getProcessedTime()
    {
        if(getStub() != null) {
            return getStub().getProcessedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.processedTime;
        }
    }
    public void setProcessedTime(Long processedTime)
    {
        if(getStub() != null) {
            getStub().setProcessedTime(processedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.processedTime = processedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public QueryRecordStub getStub()
    {
        return this.stub;
    }
    protected void setStub(QueryRecordStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("querySession = " + this.querySession).append(";");
            sb.append("queryId = " + this.queryId).append(";");
            sb.append("dataService = " + this.dataService).append(";");
            sb.append("serviceUrl = " + this.serviceUrl).append(";");
            sb.append("delayed = " + this.delayed).append(";");
            sb.append("query = " + this.query).append(";");
            sb.append("inputFormat = " + this.inputFormat).append(";");
            sb.append("inputFile = " + this.inputFile).append(";");
            sb.append("inputContent = " + this.inputContent).append(";");
            sb.append("targetOutputFormat = " + this.targetOutputFormat).append(";");
            sb.append("outputFormat = " + this.outputFormat).append(";");
            sb.append("outputFile = " + this.outputFile).append(";");
            sb.append("outputContent = " + this.outputContent).append(";");
            sb.append("responseCode = " + this.responseCode).append(";");
            sb.append("result = " + this.result).append(";");
            sb.append("referrerInfo = " + this.referrerInfo).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("extra = " + this.extra).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("scheduledTime = " + this.scheduledTime).append(";");
            sb.append("processedTime = " + this.processedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = querySession == null ? 0 : querySession.hashCode();
            _hash = 31 * _hash + delta;
            delta = queryId == null ? 0 : queryId.hashCode();
            _hash = 31 * _hash + delta;
            delta = dataService == null ? 0 : dataService.hashCode();
            _hash = 31 * _hash + delta;
            delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = delayed == null ? 0 : delayed.hashCode();
            _hash = 31 * _hash + delta;
            delta = query == null ? 0 : query.hashCode();
            _hash = 31 * _hash + delta;
            delta = inputFormat == null ? 0 : inputFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = inputFile == null ? 0 : inputFile.hashCode();
            _hash = 31 * _hash + delta;
            delta = inputContent == null ? 0 : inputContent.hashCode();
            _hash = 31 * _hash + delta;
            delta = targetOutputFormat == null ? 0 : targetOutputFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputFormat == null ? 0 : outputFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputFile == null ? 0 : outputFile.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputContent == null ? 0 : outputContent.hashCode();
            _hash = 31 * _hash + delta;
            delta = responseCode == null ? 0 : responseCode.hashCode();
            _hash = 31 * _hash + delta;
            delta = result == null ? 0 : result.hashCode();
            _hash = 31 * _hash + delta;
            delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = extra == null ? 0 : extra.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = scheduledTime == null ? 0 : scheduledTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = processedTime == null ? 0 : processedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
