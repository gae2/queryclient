package com.queryclient.af.cert.proxy;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.config.Config;
import com.queryclient.af.cert.proxy.AbstractProxyFactory;
import com.queryclient.af.cert.proxy.LocalProxyFactory;
import com.queryclient.af.cert.proxy.RemoteProxyFactory;


// We use Abstract Factory pattern.
// This "manager" class provides a way to choose a concrete factory.
public final class ProxyFactoryManager
{
    private static final Logger log = Logger.getLogger(ProxyFactoryManager.class.getName());

    // temporary
    private static final String CONFIG_KEY_PROXY_TYPE = "memodbapp.dataservice.proxytype";
    private static final String DEFAULT_PROXY_TYPE = "remote";  // Use "local"???

    // Prevents instantiation.
    private ProxyFactoryManager() {}

    // Returns a proxy factory.
    public static AbstractProxyFactory getProxyFactory() 
    {
        // TBD: This should really be hard-coded during deployment??
        String proxyType = Config.getInstance().getString(CONFIG_KEY_PROXY_TYPE, DEFAULT_PROXY_TYPE);
        if("local".equals(proxyType)) {
            return LocalProxyFactory.getInstance();
        } else {
            return RemoteProxyFactory.getInstance();
        }
    }

}
