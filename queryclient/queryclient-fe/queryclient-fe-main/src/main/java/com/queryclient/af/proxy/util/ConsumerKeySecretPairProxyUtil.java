package com.queryclient.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.ConsumerKeySecretPair;
// import com.queryclient.ws.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;


public class ConsumerKeySecretPairProxyUtil
{
    private static final Logger log = Logger.getLogger(ConsumerKeySecretPairProxyUtil.class.getName());

    // Static methods only.
    private ConsumerKeySecretPairProxyUtil() {}

    public static ConsumerKeySecretPairBean convertServerConsumerKeySecretPairBeanToAppBean(ConsumerKeySecretPair serverBean)
    {
        ConsumerKeySecretPairBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new ConsumerKeySecretPairBean();
            bean.setConsumerKey(serverBean.getConsumerKey());
            bean.setConsumerSecret(serverBean.getConsumerSecret());
        }
        return bean;
    }

    public static com.queryclient.ws.bean.ConsumerKeySecretPairBean convertAppConsumerKeySecretPairBeanToServerBean(ConsumerKeySecretPair appBean)
    {
        com.queryclient.ws.bean.ConsumerKeySecretPairBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.ConsumerKeySecretPairBean();
            bean.setConsumerKey(appBean.getConsumerKey());
            bean.setConsumerSecret(appBean.getConsumerSecret());
        }
        return bean;
    }

}
