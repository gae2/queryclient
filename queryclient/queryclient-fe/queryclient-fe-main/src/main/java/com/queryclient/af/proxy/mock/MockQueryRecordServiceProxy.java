package com.queryclient.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.ws.service.QueryRecordService;
import com.queryclient.af.proxy.QueryRecordServiceProxy;


// MockQueryRecordServiceProxy is a decorator.
// It can be used as a base class to mock QueryRecordServiceProxy objects.
public abstract class MockQueryRecordServiceProxy implements QueryRecordServiceProxy
{
    private static final Logger log = Logger.getLogger(MockQueryRecordServiceProxy.class.getName());

    // MockQueryRecordServiceProxy uses the decorator design pattern.
    private QueryRecordServiceProxy decoratedProxy;

    public MockQueryRecordServiceProxy(QueryRecordServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected QueryRecordServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(QueryRecordServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public QueryRecord getQueryRecord(String guid) throws BaseException
    {
        return decoratedProxy.getQueryRecord(guid);
    }

    @Override
    public Object getQueryRecord(String guid, String field) throws BaseException
    {
        return decoratedProxy.getQueryRecord(guid, field);       
    }

    @Override
    public List<QueryRecord> getQueryRecords(List<String> guids) throws BaseException
    {
        return decoratedProxy.getQueryRecords(guids);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords() throws BaseException
    {
        return getAllQueryRecords(null, null, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllQueryRecords(ordering, offset, count);
        return getAllQueryRecords(ordering, offset, count, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllQueryRecords(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllQueryRecordKeys(ordering, offset, count);
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count);
        return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createQueryRecord(String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        return decoratedProxy.createQueryRecord(querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime);
    }

    @Override
    public String createQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        return decoratedProxy.createQueryRecord(queryRecord);
    }

    @Override
    public Boolean updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        return decoratedProxy.updateQueryRecord(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime);
    }

    @Override
    public Boolean updateQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        return decoratedProxy.updateQueryRecord(queryRecord);
    }

    @Override
    public Boolean deleteQueryRecord(String guid) throws BaseException
    {
        return decoratedProxy.deleteQueryRecord(guid);
    }

    @Override
    public Boolean deleteQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        String guid = queryRecord.getGuid();
        return deleteQueryRecord(guid);
    }

    @Override
    public Long deleteQueryRecords(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteQueryRecords(filter, params, values);
    }

}
