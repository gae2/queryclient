package com.queryclient.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.DataServiceService;


// DataServiceMockService is a decorator.
// It can be used as a base class to mock DataServiceService objects.
public abstract class DataServiceMockService implements DataServiceService
{
    private static final Logger log = Logger.getLogger(DataServiceMockService.class.getName());

    // DataServiceMockService uses the decorator design pattern.
    private DataServiceService decoratedService;

    public DataServiceMockService(DataServiceService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected DataServiceService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(DataServiceService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // DataService related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DataService getDataService(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDataService(): guid = " + guid);
        DataService bean = decoratedService.getDataService(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getDataService(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getDataService(guid, field);
        return obj;
    }

    @Override
    public List<DataService> getDataServices(List<String> guids) throws BaseException
    {
        log.fine("getDataServices()");
        List<DataService> dataServices = decoratedService.getDataServices(guids);
        log.finer("END");
        return dataServices;
    }

    @Override
    public List<DataService> getAllDataServices() throws BaseException
    {
        return getAllDataServices(null, null, null);
    }


    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServices(ordering, offset, count, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDataServices(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<DataService> dataServices = decoratedService.getAllDataServices(ordering, offset, count, forwardCursor);
        log.finer("END");
        return dataServices;
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDataServiceKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllDataServiceKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DataServiceMockService.findDataServices(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<DataService> dataServices = decoratedService.findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return dataServices;
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DataServiceMockService.findDataServiceKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DataServiceMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDataService(String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        ConsumerKeySecretPairBean authCredentialBean = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialBean = (ConsumerKeySecretPairBean) authCredential;
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialBean = new ConsumerKeySecretPairBean(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialBean = null;   // ????
        }
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        DataServiceBean bean = new DataServiceBean(null, user, name, description, type, mode, serviceUrl, authRequired, authCredentialBean, referrerInfoBean, status);
        return createDataService(bean);
    }

    @Override
    public String createDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createDataService(dataService);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public DataService constructDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");
        DataService bean = decoratedService.constructDataService(dataService);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ConsumerKeySecretPairBean authCredentialBean = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialBean = (ConsumerKeySecretPairBean) authCredential;
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialBean = new ConsumerKeySecretPairBean(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialBean = null;   // ????
        }
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        DataServiceBean bean = new DataServiceBean(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredentialBean, referrerInfoBean, status);
        return updateDataService(bean);
    }
        
    @Override
    public Boolean updateDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateDataService(dataService);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public DataService refreshDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");
        DataService bean = decoratedService.refreshDataService(dataService);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDataService(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDataService(dataService);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDataServices(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteDataServices(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDataServices(List<DataService> dataServices) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createDataServices(dataServices);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateDataServices(List<DataService> dataServices) throws BaseException
    //{
    //}

}
