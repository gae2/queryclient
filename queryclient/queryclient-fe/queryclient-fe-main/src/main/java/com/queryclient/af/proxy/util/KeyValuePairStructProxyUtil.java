package com.queryclient.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.KeyValuePairStruct;
// import com.queryclient.ws.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;


public class KeyValuePairStructProxyUtil
{
    private static final Logger log = Logger.getLogger(KeyValuePairStructProxyUtil.class.getName());

    // Static methods only.
    private KeyValuePairStructProxyUtil() {}

    public static KeyValuePairStructBean convertServerKeyValuePairStructBeanToAppBean(KeyValuePairStruct serverBean)
    {
        KeyValuePairStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new KeyValuePairStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setKey(serverBean.getKey());
            bean.setValue(serverBean.getValue());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.queryclient.ws.bean.KeyValuePairStructBean convertAppKeyValuePairStructBeanToServerBean(KeyValuePairStruct appBean)
    {
        com.queryclient.ws.bean.KeyValuePairStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.KeyValuePairStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setKey(appBean.getKey());
            bean.setValue(appBean.getValue());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
