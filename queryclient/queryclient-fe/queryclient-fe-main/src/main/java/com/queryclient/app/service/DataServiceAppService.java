package com.queryclient.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.DataService;
import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.af.service.impl.DataServiceServiceImpl;


// Updated.
public class DataServiceAppService extends DataServiceServiceImpl implements DataServiceService
{
    private static final Logger log = Logger.getLogger(DataServiceAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public DataServiceAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // DataService related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public DataService getDataService(String guid) throws BaseException
    {
        return super.getDataService(guid);
    }

    @Override
    public Object getDataService(String guid, String field) throws BaseException
    {
        return super.getDataService(guid, field);
    }

    @Override
    public List<DataService> getDataServices(List<String> guids) throws BaseException
    {
        return super.getDataServices(guids);
    }

    @Override
    public List<DataService> getAllDataServices() throws BaseException
    {
        return super.getAllDataServices();
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllDataServiceKeys(ordering, offset, count);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findDataServices(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createDataService(DataService dataService) throws BaseException
    {
        // TBD:
        dataService = processDataService(dataService);
        // ...

        String guid = super.createDataService(dataService);
        return guid;
    }

    @Override
    public DataService constructDataService(DataService dataService) throws BaseException
    {
        // TBD:
        dataService = processDataService(dataService);
        // ...

        dataService = super.constructDataService(dataService);
        return dataService;
    }


    @Override
    public Boolean updateDataService(DataService dataService) throws BaseException
    {
        return super.updateDataService(dataService);
    }
        
    @Override
    public DataService refreshDataService(DataService dataService) throws BaseException
    {
        return super.refreshDataService(dataService);
    }

    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        return super.deleteDataService(guid);
    }

    @Override
    public Boolean deleteDataService(DataService dataService) throws BaseException
    {
        return super.deleteDataService(dataService);
    }

    @Override
    public Integer createDataServices(List<DataService> dataServices) throws BaseException
    {
        return super.createDataServices(dataServices);
    }

    // TBD
    //@Override
    //public Boolean updateDataServices(List<DataService> dataServices) throws BaseException
    //{
    //}


    private DataServiceBean processDataService(DataService dataService) throws BaseException
    {
        log.fine("ENTERING: processDataService()");

        DataServiceBean bean = null;
        if(dataService instanceof DataServiceBean) {
            bean = (DataServiceBean) dataService;
        } else {
            // ????? Can this happen????
            bean = new DataServiceBean(dataService);
        }


        return bean;
    }

}
