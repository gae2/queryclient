package com.queryclient.util.parser;

public enum QueryMethod
{
    SELECT ("SELECT"),
    CREATE ("CREATE"),
    UPDATE ("UPDATE"),
    DELETE ("DELETE");
    
    private final String method;
    QueryMethod(String method)
    {
        //this.method = method.trim().toUpperCase();
        //this.method = method.trim();
        this.method = method;
    }

}
