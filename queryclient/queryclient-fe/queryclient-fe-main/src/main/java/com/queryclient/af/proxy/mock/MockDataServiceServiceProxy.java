package com.queryclient.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.ws.service.DataServiceService;
import com.queryclient.af.proxy.DataServiceServiceProxy;


// MockDataServiceServiceProxy is a decorator.
// It can be used as a base class to mock DataServiceServiceProxy objects.
public abstract class MockDataServiceServiceProxy implements DataServiceServiceProxy
{
    private static final Logger log = Logger.getLogger(MockDataServiceServiceProxy.class.getName());

    // MockDataServiceServiceProxy uses the decorator design pattern.
    private DataServiceServiceProxy decoratedProxy;

    public MockDataServiceServiceProxy(DataServiceServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected DataServiceServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(DataServiceServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public DataService getDataService(String guid) throws BaseException
    {
        return decoratedProxy.getDataService(guid);
    }

    @Override
    public Object getDataService(String guid, String field) throws BaseException
    {
        return decoratedProxy.getDataService(guid, field);       
    }

    @Override
    public List<DataService> getDataServices(List<String> guids) throws BaseException
    {
        return decoratedProxy.getDataServices(guids);
    }

    @Override
    public List<DataService> getAllDataServices() throws BaseException
    {
        return getAllDataServices(null, null, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllDataServices(ordering, offset, count);
        return getAllDataServices(ordering, offset, count, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDataServices(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllDataServiceKeys(ordering, offset, count);
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDataServiceKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findDataServices(filter, ordering, params, values, grouping, unique, offset, count);
        return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDataService(String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        return decoratedProxy.createDataService(user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status);
    }

    @Override
    public String createDataService(DataService dataService) throws BaseException
    {
        return decoratedProxy.createDataService(dataService);
    }

    @Override
    public Boolean updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        return decoratedProxy.updateDataService(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status);
    }

    @Override
    public Boolean updateDataService(DataService dataService) throws BaseException
    {
        return decoratedProxy.updateDataService(dataService);
    }

    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        return decoratedProxy.deleteDataService(guid);
    }

    @Override
    public Boolean deleteDataService(DataService dataService) throws BaseException
    {
        String guid = dataService.getGuid();
        return deleteDataService(guid);
    }

    @Override
    public Long deleteDataServices(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteDataServices(filter, params, values);
    }

}
