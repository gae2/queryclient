package com.queryclient.af.auth.googleglass;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.auth.common.OAuthConstants;


public class GoogleGlassAuthUtil
{
    private static final Logger log = Logger.getLogger(GoogleGlassAuthUtil.class.getName());

    // Constants

    // This does not make sense.
    // All Google APIs will require the same client ID/Secret. 
    // (and, hence we can use the same callback URLs, etc.)
    // We would not need separate client ID/Secret, callbck URLs, etc. for Google Glass....
    // The only exception is the probably the "auth scope" list....
    public static final String CONFIG_KEY_APPLICATION_NAME = "queryclientapp.googleglass.application.name";
    public static final String CONFIG_KEY_CLIENT_ID = "queryclientapp.googleglass.clientid";
    public static final String CONFIG_KEY_CLIENT_SECRET = "queryclientapp.googleglass.clientsecret";
    // ...
    public static final String CONFIG_KEY_CALLBACK_TOKENHANDLER_URLPATH = "queryclientapp.googleglass.callback.tokenhandler.urlpath";
    public static final String CONFIG_KEY_CALLBACK_AUTHAJAX_URLPATH = "queryclientapp.googleglass.callback.authajax.urlpath";
    public static final String CONFIG_KEY_OAUTH_REDIRECTURLPATH = "queryclientapp.googleglass.oauth.redirecturlpath";
    public static final String CONFIG_KEY_JAVASCRIPT_SIGNIN_CALLBACK = "queryclientapp.googleglass.javascript.signin.callback";     // Javascript callback function name
    public static final String CONFIG_KEY_OAUTH_DEFAULT_SCOPES = "queryclientapp.googleglass.oauth.default.scopes";
    // ...

    public static final String PARAM_CALLBACK = OAuthConstants.PARAM_OAUTH_CALLBACK;
    public static final String PARAM_CONSUMER_KEY = OAuthConstants.PARAM_OAUTH_CONSUMER_KEY;
    public static final String PARAM_NONCE = OAuthConstants.PARAM_OAUTH_NONCE;
    public static final String PARAM_SIGNATURE = OAuthConstants.PARAM_OAUTH_SIGNATURE;
    public static final String PARAM_SIGNATURE_METHOD = OAuthConstants.PARAM_OAUTH_SIGNATURE_METHOD;
    public static final String PARAM_TIMESTAMP = OAuthConstants.PARAM_OAUTH_TIMESTAMP;
    public static final String PARAM_VERSION = OAuthConstants.PARAM_OAUTH_VERSION;
    public static final String PARAM_REQUEST_TOKEN = OAuthConstants.PARAM_OAUTH_REQUEST_TOKEN;
    public static final String PARAM_TOKEN_VERIFIER = OAuthConstants.PARAM_OAUTH_TOKEN_VERIFIER;
    // These are specific to "google+ signin", which is a variation of OAuth2....
    // public static final String PARAM_CLIENT_ID = "client_id";
    // public static final String PARAM_APPLICATION_NAME = "application_name";
    public static final String PARAM_ACCESS_TOKEN = "access_token";
    public static final String PARAM_REFRESH_TOKEN = "refresh_token";
    public static final String PARAM_CODE = "code";              // Equivalent to OAuth request token ???
    // ...

    // Always the same value for the google glass signin???
    public static final String DEFAULT_DATA_SCOPE = 
            "https://www.googleapis.com/auth/plus.login, " +          // ????
            "https://www.googleapis.com/auth/glass.timeline, " +
            "https://www.googleapis.com/auth/glass.location, " +
            "https://www.googleapis.com/auth/userinfo.profile";       // ????
    public static final String DEFAULT_DATA_REDIRECT_URL = "postmessage";                                 // ???
    public static final String DEFAULT_DATA_ACCESS_TYPE = "offline";                                      // ???
    public static final String DEFAULT_DATA_COOKIE_POLICY = "single_host_origin";                         // ???
    // ...

    // To be used for generating session attribute name for csrf state...
    public static final String DEFAULT_FORM_ID_GOOGLEGLASS_SIGNIN = "_googleglass_signin_form";         // ???
    // ...

    // "TokenResponse" object returned from Google+.
    public static final String SESSION_ATTR_TOKENRESPONSE = "com.queryclient.af.auth.googleglass.tokenresponse";
    // ...
    
    private GoogleGlassAuthUtil() {}

    
    // temporary
    public static String getDefaultGoogleGlassSignInFormId()
    {
        return DEFAULT_FORM_ID_GOOGLEGLASS_SIGNIN;
    }


    // TBD:
    // ...
    
}
