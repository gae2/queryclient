package com.queryclient.af.service;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ServiceEndpointService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ServiceEndpoint getServiceEndpoint(String guid) throws BaseException;
    Object getServiceEndpoint(String guid, String field) throws BaseException;
    List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException;
    List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException;
    /* @Deprecated */ List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException;
    List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createServiceEndpoint(String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException;
    //String createServiceEndpoint(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ServiceEndpoint?)
    String createServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException;
    ServiceEndpoint constructServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException;
    Boolean updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException;
    //Boolean updateServiceEndpoint(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException;
    ServiceEndpoint refreshServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException;
    Boolean deleteServiceEndpoint(String guid) throws BaseException;
    Boolean deleteServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException;
    Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException;
//    Boolean updateServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException;

}
