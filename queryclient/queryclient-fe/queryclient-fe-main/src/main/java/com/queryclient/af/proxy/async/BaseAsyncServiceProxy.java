package com.queryclient.af.proxy.async;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.config.Config;


// Place holder, for now.
public abstract class BaseAsyncServiceProxy
{
    private static final Logger log = Logger.getLogger(BaseAsyncServiceProxy.class.getName());

    // TBD
    public static final String TASK_QUEUE_NAME = "task-r";
    public static final String TASK_URIPATH_PREFIX = "/rs/_task/r/";

    // TBD
    private static final String CONFIG_KEY_USE_DUMMY_PAYLOAD = "queryclientapp.dataservice.async.dummypayload";
    private static final Boolean CONFIG_DEFAULT_ALWAYS_USE_DUMMY_PAYLOAD = true;

    // TBD
    private static final String CONFIG_KEY_MAX_PAYLOAD_SIZE = "queryclientapp.dataservice.async.maxpayloadsize";
    private static final Integer CONFIG_DEFAULT_MAX_PAYLOAD_SIZE = 75000;   // App engine task object size limit = 100k.

    // Cache service
    private Cache mCache = null;

    public BaseAsyncServiceProxy()
    {
    }

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    protected String getAsyncTaskQueueName()
    {
        // TBD: Pick a random queue from a set of task queues (eg, ~10) 
        // (Queues should be pre-configured in queue.xml.)
        return TASK_QUEUE_NAME;
    }

    protected boolean isAlwaysUseDummyPayload()
    {
        Boolean useDummy = Config.getInstance().getBoolean(CONFIG_KEY_USE_DUMMY_PAYLOAD, CONFIG_DEFAULT_ALWAYS_USE_DUMMY_PAYLOAD);
        return useDummy;
    }

    protected int getMaxPayloadSize()
    {
        Integer maxSize = Config.getInstance().getInteger(CONFIG_KEY_MAX_PAYLOAD_SIZE, CONFIG_DEFAULT_MAX_PAYLOAD_SIZE);
        return maxSize;
    }

}
