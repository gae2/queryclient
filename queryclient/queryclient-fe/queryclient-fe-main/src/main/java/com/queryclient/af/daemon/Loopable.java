package com.queryclient.af.daemon;

// Modeled after Runnable...
public interface Loopable
{
    // "setup"
    void preLoops();
    
    // loopable job should be "idempotent"
    // because it may be executed multiple times by different tasks at exactly the same time...
    // Returns true if the processing was successful (app dependent)
    boolean doOneLoop();
    // ...
    
}
