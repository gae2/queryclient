package com.queryclient.app.auth;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aeryid.af.bean.AeryUserBean;
import com.aeryid.af.bean.GaeUserStructBean;
import com.aeryid.af.service.AeryUserService;
import com.aeryid.rf.proxy.AeryUserServiceProxy;
import com.aeryid.ws.AeryUser;
import com.queryclient.af.auth.user.AuthUser;
import com.queryclient.ws.core.GUID;


public class AeryUserHelper
{
    private static final Logger log = Logger.getLogger(AeryUserHelper.class.getName());
    
    // temporary
    private static final String APP_ID = "Query Client";

    // ...
    private AeryUserService mAeryUserService = null;

    
    private AeryUserHelper() {}

    // Initialization-on-demand holder.
    private static final class AeryUserHelperHolder
    {
        private static final AeryUserHelper INSTANCE = new AeryUserHelper();
    }

    // Singleton method
    public static AeryUserHelper getInstance()
    {
        return AeryUserHelperHolder.INSTANCE;
    }
   
    
    private AeryUserService getAeryUserService()
    {
        if(mAeryUserService == null) {
            mAeryUserService = new AeryUserServiceProxy();
        }
        return mAeryUserService;
    }

    public AeryUser getAeryUser(String guid)
    {
        AeryUser user = null;
        try {
            user = getAeryUserService().getAeryUser(guid);
        } catch (com.aeryid.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to get the user.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to get the user due to unexpected error.", e);
        }
        return user;
    }
    
    public String createAeryUser(AeryUser aeryUser)
    {
        String userGuid = null;
        try {
            userGuid = getAeryUserService().createAeryUser(aeryUser);
        } catch (com.aeryid.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to create the user.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to create the user due to unexpected error.", e);
        }
        return userGuid;
    }
    public AeryUser constructAeryUser(AeryUser aeryUser)
    {
        try {
            aeryUser = getAeryUserService().constructAeryUser(aeryUser);
        } catch (com.aeryid.ws.BaseException e) {
            aeryUser = null;
            log.log(Level.WARNING, "Failed to construct the user.", e);
        } catch (Exception e) {
            aeryUser = null;
            log.log(Level.WARNING, "Failed to construct the user due to unexpected error.", e);
        }
        return aeryUser;
    }

    public Boolean updateAeryUser(AeryUser aeryUser)
    {
        Boolean suc = false;
        try {
            suc = getAeryUserService().updateAeryUser(aeryUser);
        } catch (com.aeryid.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to update the user.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to update the user due to unexpected error.", e);
        }
        return suc;
    }
    public AeryUser refreshAeryUser(AeryUser aeryUser)
    {
        try {
            aeryUser = getAeryUserService().refreshAeryUser(aeryUser);
        } catch (com.aeryid.ws.BaseException e) {
            aeryUser = null;
            log.log(Level.WARNING, "Failed to refresh the user.", e);
        } catch (Exception e) {
            aeryUser = null;
            log.log(Level.WARNING, "Failed to refresh the user due to unexpected error.", e);
        }
        return aeryUser;
    }

    public AeryUser constructAeryUser(String sessionGuid, String userGuid)
    {
        // Create a user object,
        AeryUserBean aeryUser = new AeryUserBean();
        if(userGuid == null) {
            userGuid = GUID.generate();
        }
        aeryUser.setGuid(userGuid);
        aeryUser.setSessionId(sessionGuid);
        aeryUser.setAppId(APP_ID);
        try {
            aeryUser = (AeryUserBean) getAeryUserService().constructAeryUser(aeryUser);  // TBD: Validate the result?
            log.info("New aery user object created. userGuid = " + userGuid);
        } catch (com.aeryid.ws.BaseException e) {
            aeryUser = null;
            log.log(Level.WARNING, "Failed to save the aery user object.", e);
        } catch (Exception e) {
            aeryUser = null;
            log.log(Level.WARNING, "Failed to save the aery user object due to unexpected error.", e);
        }
        return aeryUser;
    }

    public String createAeryUser(String sessionGuid, String userGuid)
    {
        // Create a user object,
        AeryUserBean aeryUser = new AeryUserBean();
        if(userGuid == null) {
            userGuid = GUID.generate();
        }
        aeryUser.setGuid(userGuid);
        aeryUser.setSessionId(sessionGuid);
        aeryUser.setAppId(APP_ID);
        try {
            userGuid = getAeryUserService().createAeryUser(aeryUser);  // TBD: Validate the result?
            log.info("New aery user object created. userGuid = " + userGuid);
        } catch (com.aeryid.ws.BaseException e) {
            userGuid = null;
            log.log(Level.WARNING, "Failed to save an aeryUser object.", e);
        } catch (Exception e) {
            aeryUser = null;
            log.log(Level.WARNING, "Failed to save an aeryUser object due to unexpected error.", e);
        }
        return userGuid;
    }


    public Boolean createOrUpdateAeryUser(String currentUserGuid, String currentSessionId, AuthUser authUser)
    {
        Boolean created = null;
        
        // TBD: Move this to a util class.....
        GaeUserStructBean gaeUser = new GaeUserStructBean();
        gaeUser.setFederatedIdentity(authUser.getFederatedIdentity());
        gaeUser.setAuthDomain(authUser.getAuthDomain());
        gaeUser.setEmail(authUser.getEmail());
        gaeUser.setNickname(authUser.getNickname());
        gaeUser.setUserId(authUser.getUserId());
        // etc. ???
        
        AeryUserBean userBean = (AeryUserBean) getAeryUser(currentUserGuid);
        if(userBean == null) {
            log.info("Failed to find the aery user with currentUserGuid = " + currentUserGuid);

            userBean = new AeryUserBean();
            userBean.setGuid(currentUserGuid);
            userBean.setSessionId(currentSessionId);
            userBean.setGaeUser(gaeUser);
            userBean.setAppId(APP_ID);
            String newUserGuid = createAeryUser(userBean);
            if(newUserGuid != null) {
                created = true;
            }
        } else {
            userBean.setGaeUser(gaeUser);                            
            userBean.setSessionId(currentSessionId);
            Boolean suc = updateAeryUser(userBean);
            if(suc) {
                created = false;
            }
        }

        return created;
    }

    public Boolean updateAeryUserIfNecessary(String currentUserGuid, String currentSessionId)
    {
        Boolean updated = null;

        AeryUserBean userBean = (AeryUserBean) getAeryUser(currentUserGuid);  // why does this always (seem to) fail???????
        if(userBean == null) {
            // ???
            log.info("Failed to retrieve the aery user: currentUserGuid = " + currentUserGuid + "; Trying one more time....");
            userBean = (AeryUserBean) getAeryUser(currentUserGuid);
        }

        if(userBean != null) {
            String userSessionId = userBean.getSessionId();
            if(! currentSessionId.equals(userSessionId)) {  // Probably from different browsers/devices, etc...
                userBean.setSessionId(currentSessionId);
                updated = updateAeryUser(userBean);
            } else {
                // nothing to do...
                log.finer("Not necessary to update the user: currentUserGuid = " + currentUserGuid);
                updated = false;
            }
        } else {
            // This cannot happen!!!
            log.warning("Aery user not found for currentUserGuid = " + currentUserGuid);
        }

        return updated;
    }


    public AeryUser findAeryUserByUsername(String username)
    {
        AeryUser user = null;
        try {
            List<AeryUser> beans = null;
            String filter = "username=='" + username + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 2;
            beans = getAeryUserService().findAeryUsers(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && beans.size() > 0) {
                user = beans.get(0);
                // For debugging/diagnostic purposes.
                if(beans.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one user with the same username = " + username + ". Needs further investigation!");
                }
            } else {
                log.log(Level.SEVERE, "AeryUser does not exist with given username = " + username);                
            }

        } catch (com.aeryid.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to get the aery user: username = " + username, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to get the aery user due to unexpected error: username = " + username, e);
        }
        return user;
    }

    public AeryUser findAeryUserByEmail(String email)
    {
        AeryUser user = null;
        try {
            List<AeryUser> beans = null;
            String filter = "email=='" + email + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 2;
            beans = getAeryUserService().findAeryUsers(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && beans.size() > 0) {
                user = beans.get(0);
                // For debugging/diagnostic purposes.
                if(beans.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one user with the same email = " + email + ". Needs further investigation!");
                }
            } else {
                log.log(Level.SEVERE, "AeryUser does not exist with given email = " + email);                
            }

        } catch (com.aeryid.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to get the aery user: email = " + email, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to get the aery user due to unexpected error: email = " + email, e);
        }
        return user;
    }

    public AeryUser findAeryUserByGaeNickname(String gaeNickname)
    {
        return findAeryUserByGaeNicknameAndAuthDomain(gaeNickname, null);
    }
    public AeryUser findAeryUserByGaeNicknameAndAuthDomain(String gaeNickname, String authDomain)
    {
        AeryUser user = null;
        try {
            List<AeryUser> beans = null;
            String filter = "gaeUser.nickname=='" + gaeNickname + "'";
            if(authDomain != null && !authDomain.isEmpty()) {
                filter += " && gaeUser.authDomain=='" + authDomain + "'";
            }
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 2;
            beans = getAeryUserService().findAeryUsers(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && beans.size() > 0) {
                user = beans.get(0);
                // For debugging/diagnostic purposes.
                if(beans.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one user with the same gaeNickname = " + gaeNickname + ". Needs further investigation!");
                }
            } else {
                log.log(Level.SEVERE, "AeryUser does not exist with given gaeNickname = " + gaeNickname);                
            }

        } catch (com.aeryid.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to get the aery user: gaeNickname = " + gaeNickname, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to get the aery user due to unexpected error: gaeNickname = " + gaeNickname, e);
        }
        return user;
    }

    public AeryUser findAeryUserByGaeUserId(String gaeUserId)
    {
        AeryUser user = null;
        try {
            List<AeryUser> beans = null;
            String filter = "gaeUser.userId=='" + gaeUserId + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 2;
            beans = getAeryUserService().findAeryUsers(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && beans.size() > 0) {
                user = beans.get(0);
                // For debugging/diagnostic purposes.
                if(beans.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one user with the same gaeUserId = " + gaeUserId + ". Needs further investigation!");
                }
            } else {
                log.log(Level.SEVERE, "AeryUser does not exist with given gaeUserId = " + gaeUserId);                
            }

        } catch (com.aeryid.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to get the aery user: gaeUserId = " + gaeUserId, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to get the aery user due to unexpected error: gaeUserId = " + gaeUserId, e);
        }
        return user;
    }

    public AeryUser findAeryUserByGaeEmail(String gaeEmail)
    {
        AeryUser user = null;
        try {
            List<AeryUser> beans = null;
            String filter = "gaeUser.email=='" + gaeEmail + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 2;
            beans = getAeryUserService().findAeryUsers(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && beans.size() > 0) {
                user = beans.get(0);
                // For debugging/diagnostic purposes.
                if(beans.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one user with the same gaeEmail = " + gaeEmail + ". Needs further investigation!");
                }
            } else {
                log.log(Level.SEVERE, "AeryUser does not exist with given gaeEmail = " + gaeEmail);                
            }

        } catch (com.aeryid.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to get the aery user: gaeEmail = " + gaeEmail, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to get the aery user due to unexpected error: gaeEmail = " + gaeEmail, e);
        }
        return user;
    }

}
