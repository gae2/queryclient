package com.queryclient.util;

import java.util.logging.Level;
import java.util.logging.Logger;


// Note:
// Page number starts from 0, not from 1...
// Negative numbers mean pages from the last page, starting from -1.
public class PagerUtil
{
    private static final Logger log = Logger.getLogger(PagerUtil.class.getName());

	// Special constants.
    public static final String PAGER_PAGE_FIRST = "first";
    public static final String PAGER_PAGE_LAST = "last";
    // ....


    private PagerUtil() {}
	

    // e.g., from URL ".../delegate/page/3"...
    public static Long getPageIndexFromPathInfo(String pathInfo)
    {
        Long page = null;
        if(pathInfo != null && !pathInfo.isEmpty()) {
            if(pathInfo.equals(PAGER_PAGE_FIRST)) {
                page = 0L;
            } else if(pathInfo.equals(PAGER_PAGE_LAST)) {
                // ????
                page = -1L;  // == last page ????
            } else {
                String strPage = null;
                if(pathInfo.startsWith("/")) {
                    strPage = pathInfo.substring(1);
                } else {
                    strPage = pathInfo;
                }
                if(strPage != null) {
                    try {
                        page = Long.valueOf(strPage);
                    } catch(NumberFormatException e) {
                        // ignore
                        log.log(Level.INFO, "Invalid param: page = " + strPage, e);
                    }
                }
            }
        }
        return page;
    }
 
    

}
