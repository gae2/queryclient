package com.queryclient.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.KeyValueRelationStruct;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.fe.bean.KeyValueRelationStructJsBean;


public class KeyValueRelationStructWebUtil
{
    private static final Logger log = Logger.getLogger(KeyValueRelationStructWebUtil.class.getName());

    // Static methods only.
    private KeyValueRelationStructWebUtil() {}
    

    public static KeyValueRelationStructJsBean convertKeyValueRelationStructToJsBean(KeyValueRelationStruct keyValueRelationStruct)
    {
        KeyValueRelationStructJsBean jsBean = null;
        if(keyValueRelationStruct != null) {
            jsBean = new KeyValueRelationStructJsBean();
            jsBean.setUuid(keyValueRelationStruct.getUuid());
            jsBean.setKey(keyValueRelationStruct.getKey());
            jsBean.setValue(keyValueRelationStruct.getValue());
            jsBean.setNote(keyValueRelationStruct.getNote());
            jsBean.setRelation(keyValueRelationStruct.getRelation());
        }
        return jsBean;
    }

    public static KeyValueRelationStruct convertKeyValueRelationStructJsBeanToBean(KeyValueRelationStructJsBean jsBean)
    {
        KeyValueRelationStructBean keyValueRelationStruct = null;
        if(jsBean != null) {
            keyValueRelationStruct = new KeyValueRelationStructBean();
            keyValueRelationStruct.setUuid(jsBean.getUuid());
            keyValueRelationStruct.setKey(jsBean.getKey());
            keyValueRelationStruct.setValue(jsBean.getValue());
            keyValueRelationStruct.setNote(jsBean.getNote());
            keyValueRelationStruct.setRelation(jsBean.getRelation());
        }
        return keyValueRelationStruct;
    }

}
