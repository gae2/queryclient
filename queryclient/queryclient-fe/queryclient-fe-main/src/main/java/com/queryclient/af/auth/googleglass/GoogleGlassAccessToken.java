package com.queryclient.af.auth.googleglass;

import java.io.Serializable;
import java.util.logging.Logger;
import java.util.logging.Level;


// Currently, not being used...
public class GoogleGlassAccessToken implements Serializable
{
    private static final Logger log = Logger.getLogger(GoogleGlassAccessToken.class.getName());
    private static final long serialVersionUID = 1L;

    // "Read-only" variables. 
    private final String accessToken;
    private final String refreshToken;
    private final long expirationTime;

    public GoogleGlassAccessToken(String accessToken, String refreshToken)
    {
        this(accessToken, refreshToken, 0L);
    }
    public GoogleGlassAccessToken(String accessToken, String refreshToken, long expirationTime)
    {
        super();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        if(expirationTime > 0L) {
            this.expirationTime = expirationTime;
        } else {
            this.expirationTime = System.currentTimeMillis();    // ????
        }
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public String getRefreshToken()
    {
        return refreshToken;
    }

    public long getExpirationTime()
    {
        return expirationTime;
    }
    

}
