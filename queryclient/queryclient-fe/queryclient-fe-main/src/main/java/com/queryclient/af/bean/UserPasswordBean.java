package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.stub.GaeAppStructStub;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.stub.UserPasswordStub;


// Wrapper class + bean combo.
public class UserPasswordBean implements UserPassword, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserPasswordBean.class.getName());

    // [1] With an embedded object.
    private UserPasswordStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String admin;
    private String user;
    private String username;
    private String email;
    private String openId;
    private String plainPassword;
    private String hashedPassword;
    private String salt;
    private String hashMethod;
    private Boolean resetRequired;
    private String challengeQuestion;
    private String challengeAnswer;
    private String status;
    private Long lastResetTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserPasswordBean()
    {
        //this((String) null);
    }
    public UserPasswordBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserPasswordBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime, null, null);
    }
    public UserPasswordBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.admin = admin;
        this.user = user;
        this.username = username;
        this.email = email;
        this.openId = openId;
        this.plainPassword = plainPassword;
        this.hashedPassword = hashedPassword;
        this.salt = salt;
        this.hashMethod = hashMethod;
        this.resetRequired = resetRequired;
        this.challengeQuestion = challengeQuestion;
        this.challengeAnswer = challengeAnswer;
        this.status = status;
        this.lastResetTime = lastResetTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserPasswordBean(UserPassword stub)
    {
        if(stub instanceof UserPasswordStub) {
            this.stub = (UserPasswordStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setManagerApp(stub.getManagerApp());   
            setAppAcl(stub.getAppAcl());   
            GaeAppStruct gaeApp = stub.getGaeApp();
            if(gaeApp instanceof GaeAppStructBean) {
                setGaeApp((GaeAppStructBean) gaeApp);   
            } else {
                setGaeApp(new GaeAppStructBean(gaeApp));   
            }
            setOwnerUser(stub.getOwnerUser());   
            setUserAcl(stub.getUserAcl());   
            setAdmin(stub.getAdmin());   
            setUser(stub.getUser());   
            setUsername(stub.getUsername());   
            setEmail(stub.getEmail());   
            setOpenId(stub.getOpenId());   
            setPlainPassword(stub.getPlainPassword());   
            setHashedPassword(stub.getHashedPassword());   
            setSalt(stub.getSalt());   
            setHashMethod(stub.getHashMethod());   
            setResetRequired(stub.isResetRequired());   
            setChallengeQuestion(stub.getChallengeQuestion());   
            setChallengeAnswer(stub.getChallengeAnswer());   
            setStatus(stub.getStatus());   
            setLastResetTime(stub.getLastResetTime());   
            setExpirationTime(stub.getExpirationTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getManagerApp()
    {
        if(getStub() != null) {
            return getStub().getManagerApp();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.managerApp;
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getStub() != null) {
            getStub().setManagerApp(managerApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.managerApp = managerApp;
        }
    }

    public Long getAppAcl()
    {
        if(getStub() != null) {
            return getStub().getAppAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appAcl;
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getStub() != null) {
            getStub().setAppAcl(appAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appAcl = appAcl;
        }
    }

    public GaeAppStruct getGaeApp()
    {  
        if(getStub() != null) {
            // Note the object type.
            GaeAppStruct _stub_field = getStub().getGaeApp();
            if(_stub_field == null) {
                return null;
            } else {
                return new GaeAppStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.gaeApp;
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGaeApp(gaeApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(gaeApp == null) {
                this.gaeApp = null;
            } else {
                if(gaeApp instanceof GaeAppStructBean) {
                    this.gaeApp = (GaeAppStructBean) gaeApp;
                } else {
                    this.gaeApp = new GaeAppStructBean(gaeApp);
                }
            }
        }
    }

    public String getOwnerUser()
    {
        if(getStub() != null) {
            return getStub().getOwnerUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ownerUser;
        }
    }
    public void setOwnerUser(String ownerUser)
    {
        if(getStub() != null) {
            getStub().setOwnerUser(ownerUser);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ownerUser = ownerUser;
        }
    }

    public Long getUserAcl()
    {
        if(getStub() != null) {
            return getStub().getUserAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userAcl;
        }
    }
    public void setUserAcl(Long userAcl)
    {
        if(getStub() != null) {
            getStub().setUserAcl(userAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userAcl = userAcl;
        }
    }

    public String getAdmin()
    {
        if(getStub() != null) {
            return getStub().getAdmin();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.admin;
        }
    }
    public void setAdmin(String admin)
    {
        if(getStub() != null) {
            getStub().setAdmin(admin);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.admin = admin;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getUsername()
    {
        if(getStub() != null) {
            return getStub().getUsername();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.username;
        }
    }
    public void setUsername(String username)
    {
        if(getStub() != null) {
            getStub().setUsername(username);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.username = username;
        }
    }

    public String getEmail()
    {
        if(getStub() != null) {
            return getStub().getEmail();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.email;
        }
    }
    public void setEmail(String email)
    {
        if(getStub() != null) {
            getStub().setEmail(email);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.email = email;
        }
    }

    public String getOpenId()
    {
        if(getStub() != null) {
            return getStub().getOpenId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.openId;
        }
    }
    public void setOpenId(String openId)
    {
        if(getStub() != null) {
            getStub().setOpenId(openId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.openId = openId;
        }
    }

    public String getPlainPassword()
    {
        if(getStub() != null) {
            return getStub().getPlainPassword();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.plainPassword;
        }
    }
    public void setPlainPassword(String plainPassword)
    {
        if(getStub() != null) {
            getStub().setPlainPassword(plainPassword);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.plainPassword = plainPassword;
        }
    }

    public String getHashedPassword()
    {
        if(getStub() != null) {
            return getStub().getHashedPassword();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.hashedPassword;
        }
    }
    public void setHashedPassword(String hashedPassword)
    {
        if(getStub() != null) {
            getStub().setHashedPassword(hashedPassword);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.hashedPassword = hashedPassword;
        }
    }

    public String getSalt()
    {
        if(getStub() != null) {
            return getStub().getSalt();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.salt;
        }
    }
    public void setSalt(String salt)
    {
        if(getStub() != null) {
            getStub().setSalt(salt);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.salt = salt;
        }
    }

    public String getHashMethod()
    {
        if(getStub() != null) {
            return getStub().getHashMethod();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.hashMethod;
        }
    }
    public void setHashMethod(String hashMethod)
    {
        if(getStub() != null) {
            getStub().setHashMethod(hashMethod);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.hashMethod = hashMethod;
        }
    }

    public Boolean isResetRequired()
    {
        if(getStub() != null) {
            return getStub().isResetRequired();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.resetRequired;
        }
    }
    public void setResetRequired(Boolean resetRequired)
    {
        if(getStub() != null) {
            getStub().setResetRequired(resetRequired);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.resetRequired = resetRequired;
        }
    }

    public String getChallengeQuestion()
    {
        if(getStub() != null) {
            return getStub().getChallengeQuestion();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.challengeQuestion;
        }
    }
    public void setChallengeQuestion(String challengeQuestion)
    {
        if(getStub() != null) {
            getStub().setChallengeQuestion(challengeQuestion);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.challengeQuestion = challengeQuestion;
        }
    }

    public String getChallengeAnswer()
    {
        if(getStub() != null) {
            return getStub().getChallengeAnswer();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.challengeAnswer;
        }
    }
    public void setChallengeAnswer(String challengeAnswer)
    {
        if(getStub() != null) {
            getStub().setChallengeAnswer(challengeAnswer);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.challengeAnswer = challengeAnswer;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getLastResetTime()
    {
        if(getStub() != null) {
            return getStub().getLastResetTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastResetTime;
        }
    }
    public void setLastResetTime(Long lastResetTime)
    {
        if(getStub() != null) {
            getStub().setLastResetTime(lastResetTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastResetTime = lastResetTime;
        }
    }

    public Long getExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getExpirationTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationTime;
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getStub() != null) {
            getStub().setExpirationTime(expirationTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationTime = expirationTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public UserPasswordStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UserPasswordStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("managerApp = " + this.managerApp).append(";");
            sb.append("appAcl = " + this.appAcl).append(";");
            sb.append("gaeApp = " + this.gaeApp).append(";");
            sb.append("ownerUser = " + this.ownerUser).append(";");
            sb.append("userAcl = " + this.userAcl).append(";");
            sb.append("admin = " + this.admin).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("username = " + this.username).append(";");
            sb.append("email = " + this.email).append(";");
            sb.append("openId = " + this.openId).append(";");
            sb.append("plainPassword = " + this.plainPassword).append(";");
            sb.append("hashedPassword = " + this.hashedPassword).append(";");
            sb.append("salt = " + this.salt).append(";");
            sb.append("hashMethod = " + this.hashMethod).append(";");
            sb.append("resetRequired = " + this.resetRequired).append(";");
            sb.append("challengeQuestion = " + this.challengeQuestion).append(";");
            sb.append("challengeAnswer = " + this.challengeAnswer).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("lastResetTime = " + this.lastResetTime).append(";");
            sb.append("expirationTime = " + this.expirationTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = managerApp == null ? 0 : managerApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = appAcl == null ? 0 : appAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = gaeApp == null ? 0 : gaeApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = ownerUser == null ? 0 : ownerUser.hashCode();
            _hash = 31 * _hash + delta;
            delta = userAcl == null ? 0 : userAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = admin == null ? 0 : admin.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = username == null ? 0 : username.hashCode();
            _hash = 31 * _hash + delta;
            delta = email == null ? 0 : email.hashCode();
            _hash = 31 * _hash + delta;
            delta = openId == null ? 0 : openId.hashCode();
            _hash = 31 * _hash + delta;
            delta = plainPassword == null ? 0 : plainPassword.hashCode();
            _hash = 31 * _hash + delta;
            delta = hashedPassword == null ? 0 : hashedPassword.hashCode();
            _hash = 31 * _hash + delta;
            delta = salt == null ? 0 : salt.hashCode();
            _hash = 31 * _hash + delta;
            delta = hashMethod == null ? 0 : hashMethod.hashCode();
            _hash = 31 * _hash + delta;
            delta = resetRequired == null ? 0 : resetRequired.hashCode();
            _hash = 31 * _hash + delta;
            delta = challengeQuestion == null ? 0 : challengeQuestion.hashCode();
            _hash = 31 * _hash + delta;
            delta = challengeAnswer == null ? 0 : challengeAnswer.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastResetTime == null ? 0 : lastResetTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationTime == null ? 0 : expirationTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
