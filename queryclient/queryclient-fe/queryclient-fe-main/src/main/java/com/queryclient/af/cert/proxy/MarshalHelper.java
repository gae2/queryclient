package com.queryclient.af.cert.proxy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.cert.PublicCertificateInfo;
import com.queryclient.ws.cert.stub.PublicCertificateInfoStub;
import com.queryclient.ws.cert.stub.PublicCertificateInfoListStub;
import com.queryclient.af.cert.bean.PublicCertificateInfoBean;


public final class MarshalHelper
{
    private static final Logger log = Logger.getLogger(MarshalHelper.class.getName());

    // Static methods only.
    private MarshalHelper() {}

    // temporary
    public static PublicCertificateInfoBean convertPublicCertificateInfoToBean(PublicCertificateInfo stub)
    {
        PublicCertificateInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new PublicCertificateInfoBean();
        } else if(stub instanceof PublicCertificateInfoBean) {
        	bean = (PublicCertificateInfoBean) stub;
        } else {
        	bean = new PublicCertificateInfoBean(stub);
        }
        return bean;
    }
    public static List<PublicCertificateInfoBean> convertPublicCertificateInfoListToBeanList(List<PublicCertificateInfo> stubList)
    {
        List<PublicCertificateInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<PublicCertificateInfo>();
        } else {
            beanList = new ArrayList<PublicCertificateInfoBean>();
            for(PublicCertificateInfo stub : stubList) {
                beanList.add(convertPublicCertificateInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<PublicCertificateInfo> convertPublicCertificateInfoListStubToBeanList(PublicCertificateInfoListStub listStub)
    {
        List<PublicCertificateInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<PublicCertificateInfo>();
        } else {
            beanList = new ArrayList<PublicCertificateInfo>();
            for(PublicCertificateInfoStub stub : listStub.getList()) {
            	beanList.add(convertPublicCertificateInfoToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static PublicCertificateInfoStub convertPublicCertificateInfoToStub(PublicCertificateInfo bean)
    {
        PublicCertificateInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new PublicCertificateInfoStub();
        } else if(bean instanceof PublicCertificateInfoStub) {
            stub = (PublicCertificateInfoStub) bean;
        } else if(bean instanceof PublicCertificateInfoBean && ((PublicCertificateInfoBean) bean).isWrapper()) {
            stub = ((PublicCertificateInfoBean) bean).getStub();
        } else {
            stub = PublicCertificateInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }
    
}
