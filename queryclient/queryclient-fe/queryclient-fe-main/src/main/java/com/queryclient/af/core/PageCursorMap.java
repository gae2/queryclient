package com.queryclient.af.core;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.Serializable;

import com.queryclient.ws.util.HashUtil;
import com.queryclient.ws.util.TokenUtil;


// This class serves two purposes:
// (1) To support pagination through cursors (e.g., prev page, next page, etc.)
// (2) To be able to use a token value, not the original cursor string, as URL query params.... 
// Note: we do not make any assumptions as to how many items are in a "page".
//       In general, a page may contain a different number of items.
public final class PageCursorMap implements Serializable
{
    private static final Logger log = Logger.getLogger(PageCursorMap.class.getName());

    // Note that the first page (pageIndex == 0) is always present (possibly with an empty list).
    // The corresponding cursor (and, hence the token) is null, which is a bit inconvenient to use as a key of a map.
    // This constant will be used to represent null cursor/token.
    // private static final String NULL_CURSOR = "_null_";
    // -->
    // Instead of doing this,
    // we will assume that the first page record (0L, null, null) is implicitly present.
    // The caller should check page==0L, cursor/token==null conditions...
    // ...

    // page number -> cursor string.
    private final SortedMap<Long, String> pageMap;
    // reverse map, for convenience.
    private final Map<String, Long> reverseMap;
    // token -> cursor string
    // (the key does not have to be a token value of cursor, any random token will do.)
    private final Map<String, String> tokenMap;
    // reverse map, for convenience.
    private final Map<String, String> cursorMap;
    
    public PageCursorMap()
    {
        pageMap = new TreeMap<Long, String>();
        reverseMap = new HashMap<String, Long>();
        tokenMap = new HashMap<String, String>();
        cursorMap = new HashMap<String, String>();

        // Add the first page.
        // addPageCursor(0L, NULL_CURSOR);
        // ...
    }

//    public boolean isEmpty()
//    {
//        return pageMap.isEmpty();
//    }
 
    public synchronized void reset()
    {
        pageMap.clear();
        reverseMap.clear();
        tokenMap.clear();
        cursorMap.clear();

        // Add the first page.
        // addPageCursor(0L, NULL_CURSOR);
        // ...
    }
    

    public synchronized String addPageCursor(long page, String cursor)
    {
        return addPageCursor(page, cursor, null);
    }
    // TBD: We do not currently support explicitly specified tokens.
    private synchronized String addPageCursor(long page, String cursor, String token)
    {
        if(page == 0L) {
            // return NULL_CURSOR;   // ???
            return null;
        }
        pageMap.put(page, cursor);
        reverseMap.put(cursor, page);
        if(token == null) {
            token = generateToken(cursor);
        }
        tokenMap.put(token, cursor);
        cursorMap.put(cursor, token);
        return token;
    }

    // Note that we will need to support adding cursor-token pair without page info
    //     once we allow explicitly specified tokens...
    private synchronized String addPageCursor(String cursor)
    {
        return addPageCursor(cursor, null);
    }
    private synchronized String addPageCursor(String cursor, String token)
    {
        if(cursor == null || cursor.isEmpty()) {  // || cursor.equals(NULL_CURSOR)) {
            return cursor;
        }
        if(token == null) {
            token = generateToken(cursor);
        }
        tokenMap.put(token, cursor);
        cursorMap.put(cursor, token);
        return token;
    }


    // Note that this function should be "deterministic", that is,
    //    when given the same cursor, the generated token should always be the same.
    // (TBD: token may be explicitly specified. 
    //    In such cases, there is no deterministic relationship between cursor and token.)
    public static String generateToken(String cursor)
    {
        if(cursor == null || cursor.isEmpty()) {  // || cursor.equals(NULL_CURSOR)) {
            return cursor;
        }
        String hash = HashUtil.generateMd5Hash(cursor);
        String token = TokenUtil.generateBase62Token(hash);
        return token;
    }


    public String getToken(String cursor)
    {
        return cursorMap.get(cursor);
    }

    public String getToken(long page)
    {
        String cursor = pageMap.get(page);
        if(cursor != null) {
            return tokenMap.get(cursor);
        } else {
            return null;
        }
    }

    public String getCursor(long page)
    {
        return pageMap.get(page);
    }

    public String getCursor(String token)
    {
        return tokenMap.get(token);
    }

    public Long getPageForCursor(String cursor)
    {
        Long page = reverseMap.get(cursor);
        if(page == null) {
            return null;
        } else {
            return page;
        }
    }

    public Long getPageForToken(String token)
    {
        String cursor = tokenMap.get(token);
        if(cursor == null) {
            return null;
        } else {
            return getPageForCursor(cursor);
        }
    }


    // Note:
    // If we decide to support arbitrary tokens (non-related to cursor),
    //   then toString() and fromString() logic should be augmented as well.

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("[");
        if(pageMap != null && !pageMap.isEmpty()) {
            for(Long page : pageMap.keySet()) {
                String cursor = pageMap.get(page);
                // Note that we are assuming that cursor strings do not contain ",",
                //     which should be true since they are base64 encoded.
                sb.append(page).append(":").append(cursor).append(",");
            }
        }
        sb.append("]");

        return sb.toString();
    }

    public static PageCursorMap fromString(String value)
    {
        PageCursorMap pageCursorMap = new PageCursorMap();

        // TBD: Need to validate the string format...
        // For now, we just assume that the arg is in the correct format.
        if(value != null && !value.isEmpty()) {
            int idx1 = 1;
            int idx2 = value.length() - 1;
            String pageStr = value.substring(idx1, idx2);            
            if(pageStr != null && !pageStr.isEmpty()) {
                String[] parts = pageStr.trim().split(",");
                for(String part : parts) {
                    if(!part.isEmpty()) {
                        String[] pair = part.trim().split(":", 2);
                        if(pair != null && pair.length == 2) {
                            try {
                                Long page = Long.valueOf(pair[0]);
                                String cursor = pair[1];
                                pageCursorMap.addPageCursor(page, cursor);
                            } catch(NumberFormatException e) {
                                // ignore.
                                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Incorrect format. pair = " + pair, e);
                            }
                        } else {
                            // error??
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Incorrect format. pair = " + pair);
                        }
                    } else {
                        // ignore.
                    }
                }
            }
        }

        return pageCursorMap;
    }

}
