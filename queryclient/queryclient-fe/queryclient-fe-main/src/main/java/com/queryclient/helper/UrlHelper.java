package com.queryclient.helper;

import java.util.logging.Logger;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;


// ...
public class UrlHelper
{
    private static final Logger log = Logger.getLogger(UrlHelper.class.getName());

    private UrlHelper() {}

    // Initialization-on-demand holder.
    private static final class UrlHelperHolder
    {
        private static final UrlHelper INSTANCE = new UrlHelper();
    }

    // Singleton method
    public static UrlHelper getInstance()
    {
        return UrlHelperHolder.INSTANCE;
    }

    
    public String getGuidFromPathInfo(String pathInfo)
    {
        String guid = null;
        if(pathInfo != null && !pathInfo.isEmpty()) {
            if(pathInfo.startsWith("/")) {
                guid = pathInfo.substring(1);
            } else {
                guid = pathInfo;
            }
            // TBD:
            // Check if guid is a valid GUID ????
        }
        return guid;
    }
 
    // temporary implementation.
    public String getTopLevelURLFromRequestURL(String requestURL)
    {
        if(requestURL == null || requestURL.length() < 10) {
            log.warning("Invalid requestURL.");
            return "/";  // ???
        }
        
        String topLevelURL = null;
        int idx = requestURL.indexOf("/", 9);
        if(idx < 0) {
            log.warning("Invalid requestURL = " + requestURL);
            topLevelURL = requestURL;  // ???          
        } else {
            topLevelURL = requestURL.substring(0, idx + 1);  // Include the trailing "/".
        }
        log.info("getTopLevelURLFromRequestURL(): topLevelURL = " + topLevelURL);

        return topLevelURL;
    }

    // temporary implementation.
    public String getTopLevelURLFromRequest(HttpServletRequest request)
    {
        if(request == null) {
            log.warning("Invalid request.");
            return "/";  // ???
        }
        
        String topLevelURL = null;

        //String protocol = request.getProtocol();
        String scheme = request.getScheme();
        String hostname = request.getServerName();
        int port = request.getServerPort();
        StringBuffer uriSb = new StringBuffer();
        uriSb.append(scheme);
        uriSb.append("://");
        uriSb.append(hostname);
        if(("http".equals(scheme) && port != 80) || ("https".equals(scheme) && port != 443)) {
            uriSb.append(":");
            uriSb.append(port);
        }
        uriSb.append("/");
        topLevelURL = uriSb.toString();
        log.info("getTopLevelURLFromRequest(): topLevelURL = " + topLevelURL);

        return topLevelURL;
    }

}
