package com.queryclient.af.proxy.remote;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.ApiConsumerServiceProxy;
import com.queryclient.af.proxy.UserServiceProxy;
import com.queryclient.af.proxy.UserPasswordServiceProxy;
import com.queryclient.af.proxy.ExternalUserAuthServiceProxy;
import com.queryclient.af.proxy.UserAuthStateServiceProxy;
import com.queryclient.af.proxy.DataServiceServiceProxy;
import com.queryclient.af.proxy.ServiceEndpointServiceProxy;
import com.queryclient.af.proxy.QuerySessionServiceProxy;
import com.queryclient.af.proxy.QueryRecordServiceProxy;
import com.queryclient.af.proxy.DummyEntityServiceProxy;
import com.queryclient.af.proxy.ServiceInfoServiceProxy;
import com.queryclient.af.proxy.FiveTenServiceProxy;

public class RemoteProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(RemoteProxyFactory.class.getName());

    private RemoteProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class RemoteProxyFactoryHolder
    {
        private static final RemoteProxyFactory INSTANCE = new RemoteProxyFactory();
    }

    // Singleton method
    public static RemoteProxyFactory getInstance()
    {
        return RemoteProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new RemoteApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new RemoteUserServiceProxy();
    }

    @Override
    public UserPasswordServiceProxy getUserPasswordServiceProxy()
    {
        return new RemoteUserPasswordServiceProxy();
    }

    @Override
    public ExternalUserAuthServiceProxy getExternalUserAuthServiceProxy()
    {
        return new RemoteExternalUserAuthServiceProxy();
    }

    @Override
    public UserAuthStateServiceProxy getUserAuthStateServiceProxy()
    {
        return new RemoteUserAuthStateServiceProxy();
    }

    @Override
    public DataServiceServiceProxy getDataServiceServiceProxy()
    {
        return new RemoteDataServiceServiceProxy();
    }

    @Override
    public ServiceEndpointServiceProxy getServiceEndpointServiceProxy()
    {
        return new RemoteServiceEndpointServiceProxy();
    }

    @Override
    public QuerySessionServiceProxy getQuerySessionServiceProxy()
    {
        return new RemoteQuerySessionServiceProxy();
    }

    @Override
    public QueryRecordServiceProxy getQueryRecordServiceProxy()
    {
        return new RemoteQueryRecordServiceProxy();
    }

    @Override
    public DummyEntityServiceProxy getDummyEntityServiceProxy()
    {
        return new RemoteDummyEntityServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new RemoteServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new RemoteFiveTenServiceProxy();
    }

}
