package com.queryclient.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.PagerStateStruct;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.fe.bean.PagerStateStructJsBean;


public class PagerStateStructWebUtil
{
    private static final Logger log = Logger.getLogger(PagerStateStructWebUtil.class.getName());

    // Static methods only.
    private PagerStateStructWebUtil() {}
    

    public static PagerStateStructJsBean convertPagerStateStructToJsBean(PagerStateStruct pagerStateStruct)
    {
        PagerStateStructJsBean jsBean = null;
        if(pagerStateStruct != null) {
            jsBean = new PagerStateStructJsBean();
            jsBean.setPagerMode(pagerStateStruct.getPagerMode());
            jsBean.setPrimaryOrdering(pagerStateStruct.getPrimaryOrdering());
            jsBean.setSecondaryOrdering(pagerStateStruct.getSecondaryOrdering());
            jsBean.setCurrentOffset(pagerStateStruct.getCurrentOffset());
            jsBean.setCurrentPage(pagerStateStruct.getCurrentPage());
            jsBean.setPageSize(pagerStateStruct.getPageSize());
            jsBean.setTotalCount(pagerStateStruct.getTotalCount());
            jsBean.setLowerBoundTotalCount(pagerStateStruct.getLowerBoundTotalCount());
            jsBean.setPreviousPageOffset(pagerStateStruct.getPreviousPageOffset());
            jsBean.setNextPageOffset(pagerStateStruct.getNextPageOffset());
            jsBean.setLastPageOffset(pagerStateStruct.getLastPageOffset());
            jsBean.setLastPageIndex(pagerStateStruct.getLastPageIndex());
            jsBean.setFirstActionEnabled(pagerStateStruct.isFirstActionEnabled());
            jsBean.setPreviousActionEnabled(pagerStateStruct.isPreviousActionEnabled());
            jsBean.setNextActionEnabled(pagerStateStruct.isNextActionEnabled());
            jsBean.setLastActionEnabled(pagerStateStruct.isLastActionEnabled());
            jsBean.setNote(pagerStateStruct.getNote());
        }
        return jsBean;
    }

    public static PagerStateStruct convertPagerStateStructJsBeanToBean(PagerStateStructJsBean jsBean)
    {
        PagerStateStructBean pagerStateStruct = null;
        if(jsBean != null) {
            pagerStateStruct = new PagerStateStructBean();
            pagerStateStruct.setPagerMode(jsBean.getPagerMode());
            pagerStateStruct.setPrimaryOrdering(jsBean.getPrimaryOrdering());
            pagerStateStruct.setSecondaryOrdering(jsBean.getSecondaryOrdering());
            pagerStateStruct.setCurrentOffset(jsBean.getCurrentOffset());
            pagerStateStruct.setCurrentPage(jsBean.getCurrentPage());
            pagerStateStruct.setPageSize(jsBean.getPageSize());
            pagerStateStruct.setTotalCount(jsBean.getTotalCount());
            pagerStateStruct.setLowerBoundTotalCount(jsBean.getLowerBoundTotalCount());
            pagerStateStruct.setPreviousPageOffset(jsBean.getPreviousPageOffset());
            pagerStateStruct.setNextPageOffset(jsBean.getNextPageOffset());
            pagerStateStruct.setLastPageOffset(jsBean.getLastPageOffset());
            pagerStateStruct.setLastPageIndex(jsBean.getLastPageIndex());
            pagerStateStruct.setFirstActionEnabled(jsBean.isFirstActionEnabled());
            pagerStateStruct.setPreviousActionEnabled(jsBean.isPreviousActionEnabled());
            pagerStateStruct.setNextActionEnabled(jsBean.isNextActionEnabled());
            pagerStateStruct.setLastActionEnabled(jsBean.isLastActionEnabled());
            pagerStateStruct.setNote(jsBean.getNote());
        }
        return pagerStateStruct;
    }

}
