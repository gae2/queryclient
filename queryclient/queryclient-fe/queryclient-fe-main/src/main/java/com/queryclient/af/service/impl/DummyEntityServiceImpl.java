package com.queryclient.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.DummyEntity;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.DummyEntityService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DummyEntityServiceImpl implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(DummyEntityServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "DummyEntity-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("DummyEntity:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public DummyEntityServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // DummyEntity related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DummyEntity getDummyEntity(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDummyEntity(): guid = " + guid);

        DummyEntityBean bean = null;
        if(getCache() != null) {
            bean = (DummyEntityBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (DummyEntityBean) getProxyFactory().getDummyEntityServiceProxy().getDummyEntity(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "DummyEntityBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DummyEntityBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getDummyEntity(String guid, String field) throws BaseException
    {
        DummyEntityBean bean = null;
        if(getCache() != null) {
            bean = (DummyEntityBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (DummyEntityBean) getProxyFactory().getDummyEntityServiceProxy().getDummyEntity(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "DummyEntityBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DummyEntityBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("name")) {
            return bean.getName();
        } else if(field.equals("content")) {
            return bean.getContent();
        } else if(field.equals("maxLength")) {
            return bean.getMaxLength();
        } else if(field.equals("expired")) {
            return bean.isExpired();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException
    {
        log.fine("getDummyEntities()");

        // TBD: Is there a better way????
        List<DummyEntity> dummyEntities = getProxyFactory().getDummyEntityServiceProxy().getDummyEntities(guids);
        if(dummyEntities == null) {
            log.log(Level.WARNING, "Failed to retrieve DummyEntityBean list.");
        }

        log.finer("END");
        return dummyEntities;
    }

    @Override
    public List<DummyEntity> getAllDummyEntities() throws BaseException
    {
        return getAllDummyEntities(null, null, null);
    }


    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDummyEntities(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DummyEntity> dummyEntities = getProxyFactory().getDummyEntityServiceProxy().getAllDummyEntities(ordering, offset, count, forwardCursor);
        if(dummyEntities == null) {
            log.log(Level.WARNING, "Failed to retrieve DummyEntityBean list.");
        }

        log.finer("END");
        return dummyEntities;
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDummyEntityKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getDummyEntityServiceProxy().getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve DummyEntityBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty DummyEntityBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityServiceImpl.findDummyEntities(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DummyEntity> dummyEntities = getProxyFactory().getDummyEntityServiceProxy().findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dummyEntities == null) {
            log.log(Level.WARNING, "Failed to find dummyEntities for the given criterion.");
        }

        log.finer("END");
        return dummyEntities;
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityServiceImpl.findDummyEntityKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getDummyEntityServiceProxy().findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find DummyEntity keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty DummyEntity key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getDummyEntityServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        DummyEntityBean bean = new DummyEntityBean(null, user, name, content, maxLength, expired, status);
        return createDummyEntity(bean);
    }

    @Override
    public String createDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //DummyEntity bean = constructDummyEntity(dummyEntity);
        //return bean.getGuid();

        // Param dummyEntity cannot be null.....
        if(dummyEntity == null) {
            log.log(Level.INFO, "Param dummyEntity is null!");
            throw new BadRequestException("Param dummyEntity object is null!");
        }
        DummyEntityBean bean = null;
        if(dummyEntity instanceof DummyEntityBean) {
            bean = (DummyEntityBean) dummyEntity;
        } else if(dummyEntity instanceof DummyEntity) {
            // bean = new DummyEntityBean(null, dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new DummyEntityBean(dummyEntity.getGuid(), dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
        } else {
            log.log(Level.WARNING, "createDummyEntity(): Arg dummyEntity is of an unknown type.");
            //bean = new DummyEntityBean();
            bean = new DummyEntityBean(dummyEntity.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getDummyEntityServiceProxy().createDummyEntity(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public DummyEntity constructDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");

        // Param dummyEntity cannot be null.....
        if(dummyEntity == null) {
            log.log(Level.INFO, "Param dummyEntity is null!");
            throw new BadRequestException("Param dummyEntity object is null!");
        }
        DummyEntityBean bean = null;
        if(dummyEntity instanceof DummyEntityBean) {
            bean = (DummyEntityBean) dummyEntity;
        } else if(dummyEntity instanceof DummyEntity) {
            // bean = new DummyEntityBean(null, dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new DummyEntityBean(dummyEntity.getGuid(), dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
        } else {
            log.log(Level.WARNING, "createDummyEntity(): Arg dummyEntity is of an unknown type.");
            //bean = new DummyEntityBean();
            bean = new DummyEntityBean(dummyEntity.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getDummyEntityServiceProxy().createDummyEntity(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        DummyEntityBean bean = new DummyEntityBean(guid, user, name, content, maxLength, expired, status);
        return updateDummyEntity(bean);
    }
        
    @Override
    public Boolean updateDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //DummyEntity bean = refreshDummyEntity(dummyEntity);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param dummyEntity cannot be null.....
        if(dummyEntity == null || dummyEntity.getGuid() == null) {
            log.log(Level.INFO, "Param dummyEntity or its guid is null!");
            throw new BadRequestException("Param dummyEntity object or its guid is null!");
        }
        DummyEntityBean bean = null;
        if(dummyEntity instanceof DummyEntityBean) {
            bean = (DummyEntityBean) dummyEntity;
        } else {  // if(dummyEntity instanceof DummyEntity)
            bean = new DummyEntityBean(dummyEntity.getGuid(), dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
        }
        Boolean suc = getProxyFactory().getDummyEntityServiceProxy().updateDummyEntity(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public DummyEntity refreshDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");

        // Param dummyEntity cannot be null.....
        if(dummyEntity == null || dummyEntity.getGuid() == null) {
            log.log(Level.INFO, "Param dummyEntity or its guid is null!");
            throw new BadRequestException("Param dummyEntity object or its guid is null!");
        }
        DummyEntityBean bean = null;
        if(dummyEntity instanceof DummyEntityBean) {
            bean = (DummyEntityBean) dummyEntity;
        } else {  // if(dummyEntity instanceof DummyEntity)
            bean = new DummyEntityBean(dummyEntity.getGuid(), dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
        }
        Boolean suc = getProxyFactory().getDummyEntityServiceProxy().updateDummyEntity(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getDummyEntityServiceProxy().deleteDummyEntity(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            DummyEntity dummyEntity = null;
            try {
                dummyEntity = getProxyFactory().getDummyEntityServiceProxy().getDummyEntity(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch dummyEntity with a key, " + guid);
                return false;
            }
            if(dummyEntity != null) {
                String beanGuid = dummyEntity.getGuid();
                Boolean suc1 = getProxyFactory().getDummyEntityServiceProxy().deleteDummyEntity(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("dummyEntity with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");

        // Param dummyEntity cannot be null.....
        if(dummyEntity == null || dummyEntity.getGuid() == null) {
            log.log(Level.INFO, "Param dummyEntity or its guid is null!");
            throw new BadRequestException("Param dummyEntity object or its guid is null!");
        }
        DummyEntityBean bean = null;
        if(dummyEntity instanceof DummyEntityBean) {
            bean = (DummyEntityBean) dummyEntity;
        } else {  // if(dummyEntity instanceof DummyEntity)
            // ????
            log.warning("dummyEntity is not an instance of DummyEntityBean.");
            bean = new DummyEntityBean(dummyEntity.getGuid(), dummyEntity.getUser(), dummyEntity.getName(), dummyEntity.getContent(), dummyEntity.getMaxLength(), dummyEntity.isExpired(), dummyEntity.getStatus());
        }
        Boolean suc = getProxyFactory().getDummyEntityServiceProxy().deleteDummyEntity(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getDummyEntityServiceProxy().deleteDummyEntities(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    {
        log.finer("BEGIN");

        if(dummyEntities == null) {
            log.log(Level.WARNING, "createDummyEntities() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = dummyEntities.size();
        if(size == 0) {
            log.log(Level.WARNING, "createDummyEntities() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(DummyEntity dummyEntity : dummyEntities) {
            String guid = createDummyEntity(dummyEntity);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createDummyEntities() failed for at least one dummyEntity. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    //{
    //}

}
