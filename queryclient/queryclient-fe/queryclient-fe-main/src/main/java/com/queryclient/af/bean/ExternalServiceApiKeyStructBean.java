package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ExternalServiceApiKeyStruct;
import com.queryclient.ws.stub.ExternalServiceApiKeyStructStub;


// Wrapper class + bean combo.
public class ExternalServiceApiKeyStructBean implements ExternalServiceApiKeyStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExternalServiceApiKeyStructBean.class.getName());

    // [1] With an embedded object.
    private ExternalServiceApiKeyStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String service;
    private String key;
    private String secret;
    private String note;

    // Ctors.
    public ExternalServiceApiKeyStructBean()
    {
        //this((String) null);
    }
    public ExternalServiceApiKeyStructBean(String uuid, String service, String key, String secret, String note)
    {
        this.uuid = uuid;
        this.service = service;
        this.key = key;
        this.secret = secret;
        this.note = note;
    }
    public ExternalServiceApiKeyStructBean(ExternalServiceApiKeyStruct stub)
    {
        if(stub instanceof ExternalServiceApiKeyStructStub) {
            this.stub = (ExternalServiceApiKeyStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setService(stub.getService());   
            setKey(stub.getKey());   
            setSecret(stub.getSecret());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getService()
    {
        if(getStub() != null) {
            return getStub().getService();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.service;
        }
    }
    public void setService(String service)
    {
        if(getStub() != null) {
            getStub().setService(service);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.service = service;
        }
    }

    public String getKey()
    {
        if(getStub() != null) {
            return getStub().getKey();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.key;
        }
    }
    public void setKey(String key)
    {
        if(getStub() != null) {
            getStub().setKey(key);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.key = key;
        }
    }

    public String getSecret()
    {
        if(getStub() != null) {
            return getStub().getSecret();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.secret;
        }
    }
    public void setSecret(String secret)
    {
        if(getStub() != null) {
            getStub().setSecret(secret);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.secret = secret;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getService() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecret() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public ExternalServiceApiKeyStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ExternalServiceApiKeyStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("service = " + this.service).append(";");
            sb.append("key = " + this.key).append(";");
            sb.append("secret = " + this.secret).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = service == null ? 0 : service.hashCode();
            _hash = 31 * _hash + delta;
            delta = key == null ? 0 : key.hashCode();
            _hash = 31 * _hash + delta;
            delta = secret == null ? 0 : secret.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
