package com.queryclient.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferrerInfoStructJsBean implements Serializable, Cloneable  //, ReferrerInfoStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ReferrerInfoStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String referer;
    private String userAgent;
    private String language;
    private String hostname;
    private String ipAddress;
    private String note;

    // Ctors.
    public ReferrerInfoStructJsBean()
    {
        //this((String) null);
    }
    public ReferrerInfoStructJsBean(String referer, String userAgent, String language, String hostname, String ipAddress, String note)
    {
        this.referer = referer;
        this.userAgent = userAgent;
        this.language = language;
        this.hostname = hostname;
        this.ipAddress = ipAddress;
        this.note = note;
    }
    public ReferrerInfoStructJsBean(ReferrerInfoStructJsBean bean)
    {
        if(bean != null) {
            setReferer(bean.getReferer());
            setUserAgent(bean.getUserAgent());
            setLanguage(bean.getLanguage());
            setHostname(bean.getHostname());
            setIpAddress(bean.getIpAddress());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ReferrerInfoStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ReferrerInfoStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ReferrerInfoStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ReferrerInfoStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getReferer()
    {
        return this.referer;
    }
    public void setReferer(String referer)
    {
        this.referer = referer;
    }

    public String getUserAgent()
    {
        return this.userAgent;
    }
    public void setUserAgent(String userAgent)
    {
        this.userAgent = userAgent;
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getHostname()
    {
        return this.hostname;
    }
    public void setHostname(String hostname)
    {
        this.hostname = hostname;
    }

    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getReferer() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUserAgent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLanguage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHostname() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getIpAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("referer:null, ");
        sb.append("userAgent:null, ");
        sb.append("language:null, ");
        sb.append("hostname:null, ");
        sb.append("ipAddress:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("referer:");
        if(this.getReferer() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferer()).append("\", ");
        }
        sb.append("userAgent:");
        if(this.getUserAgent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUserAgent()).append("\", ");
        }
        sb.append("language:");
        if(this.getLanguage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLanguage()).append("\", ");
        }
        sb.append("hostname:");
        if(this.getHostname() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHostname()).append("\", ");
        }
        sb.append("ipAddress:");
        if(this.getIpAddress() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getIpAddress()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getReferer() != null) {
            sb.append("\"referer\":").append("\"").append(this.getReferer()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"referer\":").append("null, ");
        }
        if(this.getUserAgent() != null) {
            sb.append("\"userAgent\":").append("\"").append(this.getUserAgent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userAgent\":").append("null, ");
        }
        if(this.getLanguage() != null) {
            sb.append("\"language\":").append("\"").append(this.getLanguage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"language\":").append("null, ");
        }
        if(this.getHostname() != null) {
            sb.append("\"hostname\":").append("\"").append(this.getHostname()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"hostname\":").append("null, ");
        }
        if(this.getIpAddress() != null) {
            sb.append("\"ipAddress\":").append("\"").append(this.getIpAddress()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ipAddress\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("referer = " + this.referer).append(";");
        sb.append("userAgent = " + this.userAgent).append(";");
        sb.append("language = " + this.language).append(";");
        sb.append("hostname = " + this.hostname).append(";");
        sb.append("ipAddress = " + this.ipAddress).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ReferrerInfoStructJsBean cloned = new ReferrerInfoStructJsBean();
        cloned.setReferer(this.getReferer());   
        cloned.setUserAgent(this.getUserAgent());   
        cloned.setLanguage(this.getLanguage());   
        cloned.setHostname(this.getHostname());   
        cloned.setIpAddress(this.getIpAddress());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
