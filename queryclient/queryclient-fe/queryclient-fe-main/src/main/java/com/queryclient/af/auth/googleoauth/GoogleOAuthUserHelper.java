package com.queryclient.af.auth.googleoauth;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.queryclient.af.config.Config;
import com.queryclient.af.util.URLUtil;
import com.queryclient.af.util.CsrfHelper;
import com.queryclient.af.auth.common.CommonAuthUtil;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.util.JsonUtil;


// Work in progress...
public final class GoogleOAuthUserHelper
{
    private static final Logger log = Logger.getLogger(GoogleOAuthUserHelper.class.getName());

    // GoogleOAuthUserHelper is a singleton..
    private GoogleOAuthUserHelper() {}

    // Initialization-on-demand holder.
    private static final class GoogleOAuthUserHelperHolder
    {
        private static final GoogleOAuthUserHelper INSTANCE = new GoogleOAuthUserHelper();
    }

    // Singleton method
    public static GoogleOAuthUserHelper getInstance()
    {
        return GoogleOAuthUserHelperHolder.INSTANCE;
    }
    


    public String getGoogleOAuthUserInfoEndpointUrl(String accessToken)
    {
        return getGoogleOAuthUserInfoEndpointUrl(accessToken, null);
    }
    public String getGoogleOAuthUserInfoEndpointUrl(String accessToken, String format)
    {
        return getGoogleOAuthUserInfoEndpointUrl(accessToken, format, null);
    }
    public String getGoogleOAuthUserInfoEndpointUrl(String accessToken, String format, Map<String,Object> params)
    {
        String baseUrl = GoogleOAuthUserUtil.GOOGLEOAUTH_USERINFO_ENDPOINT_URL;
        Map<String,Object> defaultParams = new LinkedHashMap<>();  // Ordered, for debugging purposes.

        defaultParams.put(GoogleOAuthUserUtil.PARAM_ACCESS_TOKEN, accessToken);

        if(format == null || format.isEmpty()) {
            format = "json";   // ???
        }
        defaultParams.put(GoogleOAuthUserUtil.PARAM_ALT, format);
        
        // Overwrites the default param values.
        if(params != null && !params.isEmpty()) {
            for(String key : params.keySet()) {
                // TBD: Check if the key is a valid param ????
                Object value = params.get(key);
                if(value instanceof String) {
                    defaultParams.put(key, value);
                } else {
                    // error. Ignore.
                    if(log.isLoggable(Level.INFO)) log.info("Invalid param value: key = " + key + "; value = " + value);
                }
            }
        }
        
        String url = URLUtil.buildUrl(baseUrl, defaultParams);
        return url;
    }


    // Get UserInfo using the accessToken.
    public GoogleOAuthUserInfo getOAuthUserInfo(String accessToken)
    {
        return getOAuthUserInfo(accessToken, null);
    }
    public GoogleOAuthUserInfo getOAuthUserInfo(String accessToken, String format)
    {
        return getOAuthUserInfo(accessToken, format, null);
    }
    public GoogleOAuthUserInfo getOAuthUserInfo(String accessToken, String format, Map<String,Object> params)
    {
        GoogleOAuthUserInfo userInfo = null;

        String endpointUrl = getGoogleOAuthUserInfoEndpointUrl(accessToken, format, params);
        try {
            URL refreshTokenEndpointURL = new URL(endpointUrl);
            HttpURLConnection conn = (HttpURLConnection) refreshTokenEndpointURL.openConnection();
            conn.setRequestMethod("GET");

            // conn.setInstanceFollowRedirects(false);
            conn.setConnectTimeout(30 * 1000);   // ????
            
            conn.setDoInput(true);
            conn.setDoOutput(false);
            conn.setUseCaches(false);
            conn.setRequestProperty("Content-Type", "application/json");
            
            // ????
            conn.connect();

            int statusCode = conn.getResponseCode();
            if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for endpointUrl, " + endpointUrl);

            // String contentType = conn.getContentType();
            // int contentLength = conn.getContentLength();
            // String contentLanguage = conn.getHeaderField("Content-Language");

            BufferedReader input = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            StringBuilder sb = new StringBuilder();
            while (null != (line = input.readLine())) {
                sb.append(line);
            }
            input.close();
            String content = sb.toString();
            // temporary.
            if(log.isLoggable(Level.FINE)) log.fine("content received = " + content + " for endpointUrl, " + endpointUrl);
            // temporary

            // TBD:
            // The return accessToken should be 200 for success, I think,
            // but we check this more broadly (for 2xx), for now.
            if(StatusCode.isSuccessful(statusCode)) { 

                userInfo = getUserInfoFromResponseContent(content);
                // temporary.
                if(log.isLoggable(Level.FINE)) log.fine("userInfo = " + userInfo);
                // temporary
                
            } else {
                // ???
                log.warning("Get failed: statusCode = " + statusCode + " for endpointUrl, " + endpointUrl);
            }

            // ???
            conn.disconnect();

        } catch (MalformedURLException e) {
            log.log(Level.WARNING, "Bad endpointUrl, " + endpointUrl, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Get failed for endpointUrl, " + endpointUrl, e);
        }
        
        return userInfo;
    }


    // The response (for access/refresh token request) is in a JSON format.
    public GoogleOAuthUserInfo getUserInfoFromResponseContent(String content)
    {
        Map<String,String> map = JsonUtil.parseJsonObjectString(content);
        if(map != null && !map.isEmpty()) {
            GoogleOAuthUserInfo userInfo = new GoogleOAuthUserInfo();

            userInfo.setId(map.get(GoogleOAuthUserUtil.PARAM_ID));
            userInfo.setEmail(map.get(GoogleOAuthUserUtil.PARAM_EMAIL));
            String emailVerifiedStr = map.get(GoogleOAuthUserUtil.PARAM_EMAIL_VERIFIED);
            boolean emailVerified = Boolean.parseBoolean(emailVerifiedStr);
            userInfo.setEmailVerified(emailVerified);
            userInfo.setName(map.get(GoogleOAuthUserUtil.PARAM_NAME));
            userInfo.setGivenName(map.get(GoogleOAuthUserUtil.PARAM_GIVEN_NAME));
            userInfo.setFamilyName(map.get(GoogleOAuthUserUtil.PARAM_FAMILY_NAME));
            userInfo.setProfile(map.get(GoogleOAuthUserUtil.PARAM_PROFILE));
            userInfo.setPicture(map.get(GoogleOAuthUserUtil.PARAM_PICTURE));
            userInfo.setGender(map.get(GoogleOAuthUserUtil.PARAM_GENDER));
            userInfo.setBirthdate(map.get(GoogleOAuthUserUtil.PARAM_BIRTHDATE));
            userInfo.setLocale(map.get(GoogleOAuthUserUtil.PARAM_LOCALE));

            return userInfo;
        }
        return null;
    }


}
