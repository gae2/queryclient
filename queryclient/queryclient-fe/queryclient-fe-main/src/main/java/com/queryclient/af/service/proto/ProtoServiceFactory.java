package com.queryclient.af.service.proto;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.service.AbstractServiceFactory;
import com.queryclient.af.service.ApiConsumerService;
import com.queryclient.af.service.UserService;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.af.service.ExternalUserAuthService;
import com.queryclient.af.service.UserAuthStateService;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.service.ServiceInfoService;
import com.queryclient.af.service.FiveTenService;

public class ProtoServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ProtoServiceFactory.class.getName());

    private ProtoServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ProtoServiceFactoryHolder
    {
        private static final ProtoServiceFactory INSTANCE = new ProtoServiceFactory();
    }

    // Singleton method
    public static ProtoServiceFactory getInstance()
    {
        return ProtoServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerProtoService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserProtoService();
    }

    @Override
    public UserPasswordService getUserPasswordService()
    {
        return new UserPasswordProtoService();
    }

    @Override
    public ExternalUserAuthService getExternalUserAuthService()
    {
        return new ExternalUserAuthProtoService();
    }

    @Override
    public UserAuthStateService getUserAuthStateService()
    {
        return new UserAuthStateProtoService();
    }

    @Override
    public DataServiceService getDataServiceService()
    {
        return new DataServiceProtoService();
    }

    @Override
    public ServiceEndpointService getServiceEndpointService()
    {
        return new ServiceEndpointProtoService();
    }

    @Override
    public QuerySessionService getQuerySessionService()
    {
        return new QuerySessionProtoService();
    }

    @Override
    public QueryRecordService getQueryRecordService()
    {
        return new QueryRecordProtoService();
    }

    @Override
    public DummyEntityService getDummyEntityService()
    {
        return new DummyEntityProtoService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoProtoService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenProtoService();
    }


}
