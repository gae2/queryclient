package com.queryclient.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.UserPasswordBean;
import com.queryclient.ws.service.UserPasswordService;
import com.queryclient.af.proxy.UserPasswordServiceProxy;


// MockUserPasswordServiceProxy is a decorator.
// It can be used as a base class to mock UserPasswordServiceProxy objects.
public abstract class MockUserPasswordServiceProxy implements UserPasswordServiceProxy
{
    private static final Logger log = Logger.getLogger(MockUserPasswordServiceProxy.class.getName());

    // MockUserPasswordServiceProxy uses the decorator design pattern.
    private UserPasswordServiceProxy decoratedProxy;

    public MockUserPasswordServiceProxy(UserPasswordServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected UserPasswordServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(UserPasswordServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public UserPassword getUserPassword(String guid) throws BaseException
    {
        return decoratedProxy.getUserPassword(guid);
    }

    @Override
    public Object getUserPassword(String guid, String field) throws BaseException
    {
        return decoratedProxy.getUserPassword(guid, field);       
    }

    @Override
    public List<UserPassword> getUserPasswords(List<String> guids) throws BaseException
    {
        return decoratedProxy.getUserPasswords(guids);
    }

    @Override
    public List<UserPassword> getAllUserPasswords() throws BaseException
    {
        return getAllUserPasswords(null, null, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllUserPasswords(ordering, offset, count);
        return getAllUserPasswords(ordering, offset, count, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUserPasswords(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllUserPasswordKeys(ordering, offset, count);
        return getAllUserPasswordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserPassword(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        return decoratedProxy.createUserPassword(managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

    @Override
    public String createUserPassword(UserPassword userPassword) throws BaseException
    {
        return decoratedProxy.createUserPassword(userPassword);
    }

    @Override
    public Boolean updateUserPassword(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        return decoratedProxy.updateUserPassword(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

    @Override
    public Boolean updateUserPassword(UserPassword userPassword) throws BaseException
    {
        return decoratedProxy.updateUserPassword(userPassword);
    }

    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        return decoratedProxy.deleteUserPassword(guid);
    }

    @Override
    public Boolean deleteUserPassword(UserPassword userPassword) throws BaseException
    {
        String guid = userPassword.getGuid();
        return deleteUserPassword(guid);
    }

    @Override
    public Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteUserPasswords(filter, params, values);
    }

}
