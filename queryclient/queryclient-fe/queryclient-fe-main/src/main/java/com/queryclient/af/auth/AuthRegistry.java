package com.queryclient.af.auth;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// Singleton.
// For caching the authTokens.
public class AuthRegistry
{
    private static final Logger log = Logger.getLogger(AuthRegistry.class.getName());

    private static final String STATUS_UNKNOWN = "status_unknown";
    private static final String STATUS_AUTHENTICATED = "status_authenticated";  // Logged in.
    private static final String STATUS_INVALID = "status_invalid";              // Logged out.

    private static final String KEY_AUTH_TOKEN = "key_auth_token";
    private static final String KEY_EXPIRATION_TIME = "key_expiration_time";
    private static final String KEY_STATUS = "key_status";

    // Initialization-on-demand holder.
    private static class AuthRegistryHolder
    {
        private static final AuthRegistry INSTANCE = new AuthRegistry();
    }

    // Singleton method
    public static AuthRegistry getInstance()
    {
        return AuthRegistryHolder.INSTANCE;
    }

    // "Cache" table.
    // { clientKey -> { "authToken" -> authToken, "status" -> status, "expirationTime" -> expirationTime }
    Map<String, Map<String, Object>> registry;
    
    private AuthRegistry()
    {
        registry = new HashMap<String, Map<String, Object>>();
        
        // temporary
        String clientKey = "AeryTestClient";
        Map<String, Object> value = new HashMap<String, Object>();
        value.put(KEY_AUTH_TOKEN, "AeryTestAuthToken");
        value.put(KEY_EXPIRATION_TIME, 0L);
        value.put(KEY_STATUS, STATUS_AUTHENTICATED);   
        registry.put(clientKey, value);
        // temporary
    }
    
    // Create/update/... etc.
    public boolean put(String clientKey, String authToken)
    {
        return put(clientKey, authToken, null);
    }
    public boolean put(String clientKey, String authToken, Long expirationTime)
    {
        return put(clientKey, authToken, expirationTime, STATUS_AUTHENTICATED);
    }
    public boolean put(String clientKey, String authToken, Long expirationTime, String status)
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AuthRegistry.put(): clientKey = " + clientKey + "; authToken = " + authToken + "; expirationTime = " + expirationTime + "; status = " + status);
        
        Map<String, Object> value = new HashMap<String, Object>();
        value.put(KEY_AUTH_TOKEN, authToken);
        value.put(KEY_EXPIRATION_TIME, expirationTime);
        value.put(KEY_STATUS, status);
        
        registry.put(clientKey, value);

        // TBD:
        return true;
    }
    
    public boolean updateStatus(String clientKey, String status)
    {
        if(!registry.containsKey(clientKey)) {
            return false;
        }

        Map<String, Object> value = registry.get(clientKey);
        value.put(KEY_STATUS, status);
        //registry.put(clientKey, value);

        // TBD:
        return true;
    }

    // Returns null, if the client has not been authenticated, or the authToken has expired, etc...
    public String getAuthToken(String clientKey)
    {
        if(!registry.containsKey(clientKey)) {
            return null;
        }
        
        Long now = (new Date()).getTime();
        String authToken = (String) registry.get(clientKey).get(KEY_AUTH_TOKEN);
        Long expirationTime = (Long) registry.get(clientKey).get(KEY_EXPIRATION_TIME);
        String status = (String) registry.get(clientKey).get(KEY_STATUS);
        if( (expirationTime == null || expirationTime == 0L || now < expirationTime) && STATUS_AUTHENTICATED.equals(status)) {
            //return authToken;
        } else {
            authToken = null;
            // TBD: Clear the entry from the registry map ????
        }
        
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AuthRegistry.getAuthToken(): clientKey = " + clientKey + "; authToken = " + authToken);
        return authToken;
    }
    
    // TBD
    public boolean isValid(String clientKey, String authToken)
    {
        if(clientKey == null || authToken == null) {
            return false;
        }
        String token = getAuthToken(clientKey);
        if(authToken.equals(token)) {
            return true;
        } else {
            return false;
        }
    }
    
    public String remove(String clientKey)
    {
        if(!registry.containsKey(clientKey)) {
            return null;
        }

        String authToken = null;
        Map<String, Object> value = registry.remove(clientKey);
        if(value != null) {
            authToken = (String) value.get(KEY_AUTH_TOKEN);
        }
        return authToken;
    }    

}
