package com.queryclient.util.parser;

import java.util.logging.Logger;

public class Token
{
    private static final Logger log = Logger.getLogger(Token.class.getName());
    
    public static final String TYPE_KEYWORD = "KEYWORD";
    public static final String TYPE_TEXT = "TEXT";

    private String type;
    private String content;

    public Token()
    {
        this(null);
    }
    public Token(String content)
    {
        this(TYPE_TEXT, content);
    }
    public Token(String type, String content)
    {
        this.type = type;
        this.content = content;
    }

    public static Token keywordToken(String content)
    {
        return new Token(TYPE_KEYWORD, content);
    }

    public static Token textToken(String content)
    {
        return new Token(TYPE_TEXT, content);
    }

    public static Token textToken(String content, boolean removeQuotes)
    {
        if(removeQuotes && content.length()>2 && content.startsWith("\"") && content.endsWith("\"")) {
            content = content.substring(1, content.length()-1);
        }
        return textToken(content);
    }

    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getContent()
    {
        return content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }
    
    public boolean isKeyword()
    {
        return (TYPE_KEYWORD.equals(type));
    }

    @Override
    public String toString()
    {
        return "Token [type=" + type + ", content=" + content + "]";
    }
    
}
