package com.queryclient.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class QuerySessionJsBean implements Serializable, Cloneable  //, QuerySession
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QuerySessionJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String user;
    private String dataService;
    private String serviceUrl;
    private String inputFormat;
    private String outputFormat;
    private ReferrerInfoStructJsBean referrerInfo;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public QuerySessionJsBean()
    {
        //this((String) null);
    }
    public QuerySessionJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public QuerySessionJsBean(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStructJsBean referrerInfo, String status, String note)
    {
        this(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note, null, null);
    }
    public QuerySessionJsBean(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStructJsBean referrerInfo, String status, String note, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.dataService = dataService;
        this.serviceUrl = serviceUrl;
        this.inputFormat = inputFormat;
        this.outputFormat = outputFormat;
        this.referrerInfo = referrerInfo;
        this.status = status;
        this.note = note;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public QuerySessionJsBean(QuerySessionJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setDataService(bean.getDataService());
            setServiceUrl(bean.getServiceUrl());
            setInputFormat(bean.getInputFormat());
            setOutputFormat(bean.getOutputFormat());
            setReferrerInfo(bean.getReferrerInfo());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static QuerySessionJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        QuerySessionJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(QuerySessionJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, QuerySessionJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getDataService()
    {
        return this.dataService;
    }
    public void setDataService(String dataService)
    {
        this.dataService = dataService;
    }

    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    public String getInputFormat()
    {
        return this.inputFormat;
    }
    public void setInputFormat(String inputFormat)
    {
        this.inputFormat = inputFormat;
    }

    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    public ReferrerInfoStructJsBean getReferrerInfo()
    {  
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStructJsBean referrerInfo)
    {
        this.referrerInfo = referrerInfo;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("dataService:null, ");
        sb.append("serviceUrl:null, ");
        sb.append("inputFormat:null, ");
        sb.append("outputFormat:null, ");
        sb.append("referrerInfo:{}, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("dataService:");
        if(this.getDataService() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDataService()).append("\", ");
        }
        sb.append("serviceUrl:");
        if(this.getServiceUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getServiceUrl()).append("\", ");
        }
        sb.append("inputFormat:");
        if(this.getInputFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getInputFormat()).append("\", ");
        }
        sb.append("outputFormat:");
        if(this.getOutputFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputFormat()).append("\", ");
        }
        sb.append("referrerInfo:");
        if(this.getReferrerInfo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferrerInfo()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getDataService() != null) {
            sb.append("\"dataService\":").append("\"").append(this.getDataService()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"dataService\":").append("null, ");
        }
        if(this.getServiceUrl() != null) {
            sb.append("\"serviceUrl\":").append("\"").append(this.getServiceUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"serviceUrl\":").append("null, ");
        }
        if(this.getInputFormat() != null) {
            sb.append("\"inputFormat\":").append("\"").append(this.getInputFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"inputFormat\":").append("null, ");
        }
        if(this.getOutputFormat() != null) {
            sb.append("\"outputFormat\":").append("\"").append(this.getOutputFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputFormat\":").append("null, ");
        }
        sb.append("\"referrerInfo\":").append(this.referrerInfo.toJsonString()).append(", ");
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("dataService = " + this.dataService).append(";");
        sb.append("serviceUrl = " + this.serviceUrl).append(";");
        sb.append("inputFormat = " + this.inputFormat).append(";");
        sb.append("outputFormat = " + this.outputFormat).append(";");
        sb.append("referrerInfo = " + this.referrerInfo).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        QuerySessionJsBean cloned = new QuerySessionJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setDataService(this.getDataService());   
        cloned.setServiceUrl(this.getServiceUrl());   
        cloned.setInputFormat(this.getInputFormat());   
        cloned.setOutputFormat(this.getOutputFormat());   
        cloned.setReferrerInfo( (ReferrerInfoStructJsBean) this.getReferrerInfo().clone() );
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
