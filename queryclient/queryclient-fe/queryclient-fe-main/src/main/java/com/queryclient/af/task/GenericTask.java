package com.queryclient.af.task;

import java.util.Map;

// Place holder for now.
// Task definition including the task handler (web service endpoint) information.
public class GenericTask
{
    String name;
    String url;
    Map<String, String> params; 
    // Body/Post data?
    // Task handler??? (Servlet or Jersey resource ...)
    // ...
    
    // TBD
    public GenericTask()
    {
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Map<String, String> getParams()
    {
        return params;
    }

    public void setParams(Map<String, String> params)
    {
        this.params = params;
    }

    // TBD
    // ...
    
}
