package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.stub.ConsumerKeySecretPairStub;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.ws.DataService;
import com.queryclient.ws.stub.DataServiceStub;


// Wrapper class + bean combo.
public class DataServiceBean implements DataService, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DataServiceBean.class.getName());

    // [1] With an embedded object.
    private DataServiceStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String name;
    private String description;
    private String type;
    private String mode;
    private String serviceUrl;
    private Boolean authRequired;
    private ConsumerKeySecretPairBean authCredential;
    private ReferrerInfoStructBean referrerInfo;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public DataServiceBean()
    {
        //this((String) null);
    }
    public DataServiceBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public DataServiceBean(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairBean authCredential, ReferrerInfoStructBean referrerInfo, String status)
    {
        this(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status, null, null);
    }
    public DataServiceBean(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairBean authCredential, ReferrerInfoStructBean referrerInfo, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.name = name;
        this.description = description;
        this.type = type;
        this.mode = mode;
        this.serviceUrl = serviceUrl;
        this.authRequired = authRequired;
        this.authCredential = authCredential;
        this.referrerInfo = referrerInfo;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public DataServiceBean(DataService stub)
    {
        if(stub instanceof DataServiceStub) {
            this.stub = (DataServiceStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setName(stub.getName());   
            setDescription(stub.getDescription());   
            setType(stub.getType());   
            setMode(stub.getMode());   
            setServiceUrl(stub.getServiceUrl());   
            setAuthRequired(stub.isAuthRequired());   
            ConsumerKeySecretPair authCredential = stub.getAuthCredential();
            if(authCredential instanceof ConsumerKeySecretPairBean) {
                setAuthCredential((ConsumerKeySecretPairBean) authCredential);   
            } else {
                setAuthCredential(new ConsumerKeySecretPairBean(authCredential));   
            }
            ReferrerInfoStruct referrerInfo = stub.getReferrerInfo();
            if(referrerInfo instanceof ReferrerInfoStructBean) {
                setReferrerInfo((ReferrerInfoStructBean) referrerInfo);   
            } else {
                setReferrerInfo(new ReferrerInfoStructBean(referrerInfo));   
            }
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getName()
    {
        if(getStub() != null) {
            return getStub().getName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.name;
        }
    }
    public void setName(String name)
    {
        if(getStub() != null) {
            getStub().setName(name);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.name = name;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }

    public String getMode()
    {
        if(getStub() != null) {
            return getStub().getMode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.mode;
        }
    }
    public void setMode(String mode)
    {
        if(getStub() != null) {
            getStub().setMode(mode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.mode = mode;
        }
    }

    public String getServiceUrl()
    {
        if(getStub() != null) {
            return getStub().getServiceUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceUrl;
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getStub() != null) {
            getStub().setServiceUrl(serviceUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceUrl = serviceUrl;
        }
    }

    public Boolean isAuthRequired()
    {
        if(getStub() != null) {
            return getStub().isAuthRequired();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.authRequired;
        }
    }
    public void setAuthRequired(Boolean authRequired)
    {
        if(getStub() != null) {
            getStub().setAuthRequired(authRequired);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.authRequired = authRequired;
        }
    }

    public ConsumerKeySecretPair getAuthCredential()
    {  
        if(getStub() != null) {
            // Note the object type.
            ConsumerKeySecretPair _stub_field = getStub().getAuthCredential();
            if(_stub_field == null) {
                return null;
            } else {
                return new ConsumerKeySecretPairBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.authCredential;
        }
    }
    public void setAuthCredential(ConsumerKeySecretPair authCredential)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setAuthCredential(authCredential);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(authCredential == null) {
                this.authCredential = null;
            } else {
                if(authCredential instanceof ConsumerKeySecretPairBean) {
                    this.authCredential = (ConsumerKeySecretPairBean) authCredential;
                } else {
                    this.authCredential = new ConsumerKeySecretPairBean(authCredential);
                }
            }
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {  
        if(getStub() != null) {
            // Note the object type.
            ReferrerInfoStruct _stub_field = getStub().getReferrerInfo();
            if(_stub_field == null) {
                return null;
            } else {
                return new ReferrerInfoStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referrerInfo;
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setReferrerInfo(referrerInfo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(referrerInfo == null) {
                this.referrerInfo = null;
            } else {
                if(referrerInfo instanceof ReferrerInfoStructBean) {
                    this.referrerInfo = (ReferrerInfoStructBean) referrerInfo;
                } else {
                    this.referrerInfo = new ReferrerInfoStructBean(referrerInfo);
                }
            }
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public DataServiceStub getStub()
    {
        return this.stub;
    }
    protected void setStub(DataServiceStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("name = " + this.name).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("type = " + this.type).append(";");
            sb.append("mode = " + this.mode).append(";");
            sb.append("serviceUrl = " + this.serviceUrl).append(";");
            sb.append("authRequired = " + this.authRequired).append(";");
            sb.append("authCredential = " + this.authCredential).append(";");
            sb.append("referrerInfo = " + this.referrerInfo).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = name == null ? 0 : name.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            delta = mode == null ? 0 : mode.hashCode();
            _hash = 31 * _hash + delta;
            delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = authRequired == null ? 0 : authRequired.hashCode();
            _hash = 31 * _hash + delta;
            delta = authCredential == null ? 0 : authCredential.hashCode();
            _hash = 31 * _hash + delta;
            delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
