package com.queryclient.af.cron;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Parses the cron expression:
 * http://en.wikipedia.org/wiki/Cron
 */
public final class CronExpressionParser 
{
    private static final Logger log = Logger.getLogger(CronExpressionParser.class.getName());

    // CronTab field indices.
    public static final int MINUTES = 0;
    public static final int HOURS = 1;
    public static final int DAYS_OF_MONTH = 2;
    public static final int MONTHS = 3;
    public static final int DAYS_OF_WEEK = 4;
    public static final int YEARS = 5;
    public static final int NUM_FIELDS = 6;

    // Field value ranges.
    // Range == min ~ max (inclusive on both ends)
    private static final int MIN_MINUTES = 0;
    private static final int MAX_MINUTES = 59;
    private static final int MIN_HOURS = 0;
    private static final int MAX_HOURS = 23;
    private static final int MIN_DAYS_OF_MONTH = 1;
    private static final int MAX_DAYS_OF_MONTH = 31;
    private static final int MIN_MONTHS = 1;
    private static final int MAX_MONTHS = 12;
    private static final int MIN_DAYS_OF_WEEK = 0;
    private static final int MAX_DAYS_OF_WEEK = 6; // Sun = 0 or 7
    private static final int MIN_YEARS = 1970;
    private static final int MAX_YEARS = 2099;
    private static final int THIS_YEAR = Calendar.getInstance().get(Calendar.YEAR);
    private static final int[][] RANGES = {
         {MIN_MINUTES, MAX_MINUTES},
         {MIN_HOURS, MAX_HOURS},
         {MIN_DAYS_OF_MONTH, MAX_DAYS_OF_MONTH},
         {MIN_MONTHS, MAX_MONTHS},
         {MIN_DAYS_OF_WEEK, MAX_DAYS_OF_WEEK},
         //{MIN_YEARS, MAX_YEARS}
         //{THIS_YEAR, MAX_YEARS}   // ???
         {THIS_YEAR, (THIS_YEAR + 10 < MAX_YEARS) ? THIS_YEAR + 10 : MAX_YEARS}   // ???
    };

    // String constants for Months and Days of Week.
    private static final Map<String, Integer> sMapMonths = new HashMap<String, Integer>();
    static {
        sMapMonths.put("JAN", 1);
        sMapMonths.put("FEB", 2);
        sMapMonths.put("MAR", 3);
        sMapMonths.put("APR", 4);
        sMapMonths.put("MAY", 5);
        sMapMonths.put("JUN", 6);
        sMapMonths.put("JUL", 7);
        sMapMonths.put("AUG", 8);
        sMapMonths.put("SEP", 9);
        sMapMonths.put("OCT", 10);
        sMapMonths.put("NOV", 11);
        sMapMonths.put("DEC", 12);
    }
    private static final Map<String, Integer> sMapDaysOfWeek = new HashMap<String, Integer>();
    static {
        sMapDaysOfWeek.put("SUN", 0);
        sMapDaysOfWeek.put("MON", 1);
        sMapDaysOfWeek.put("TUE", 2);
        sMapDaysOfWeek.put("WED", 3);
        sMapDaysOfWeek.put("THU", 4);
        sMapDaysOfWeek.put("FRI", 5);
        sMapDaysOfWeek.put("SAT", 6);
    }


    // Input cron expression
    private final String expression;
    
    // Time is to be interpreted based on this TimeZone
    private final TimeZone timezone;
    
    // Set to true once the expression is parsed. (We could just use cronFields == null?)
    private boolean parsed = false;

    // The parsed cron expression fields. Array of sorted sets.
    private NavigableSet<Integer>[] cronFields = null;


    // Ctor. Expression/Timezone are read-only vars.
    public CronExpressionParser(String expression) 
    {
        this(expression, null);
    }
    public CronExpressionParser(String expression, String timezoneID) 
    {
        this.expression = expression;
        if(timezoneID != null && !timezoneID.isEmpty()) {
            this.timezone = TimeZone.getTimeZone(timezoneID);
        } else {
            // Use GMT, by default.
            this.timezone = TimeZone.getTimeZone("UTC");
            //this.timezone = TimeZone.getDefault();
        }
        try {
            cronFields = parse(this.expression);
            parsed = true;
        } catch(CronExpressionParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse the cron expression: " + this.expression, e);
        }
    }

    // No setters.
    public String getExpression()
    {
        return expression;
    }
    public String getTimeZoneID()
    {
        return timezone.getID();
    }
    public boolean isParsed()
    {
        return parsed;
    }
    public NavigableSet<Integer>[] getCronFields()
    {
        return cronFields;
    }


    // Parses the crontab expression,
    // and returns an array of sorted-sets of enumerated field values.
    // Note: This function can be used "stand-alone" without instantiating a CronExpressionParser.
    public static NavigableSet<Integer>[] parse(String expression) throws CronExpressionParseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("parse() BEGIN: expression = " + expression);
        if(expression == null || expression.isEmpty()) {
            throw new CronExpressionParseException("Null/Empty cron expression.");
        }
        
        @SuppressWarnings("unchecked")
        NavigableSet<Integer>[] cronFieldSets = new NavigableSet[NUM_FIELDS];
        //String[] cronElements = expression.split("\\s+", NUM_FIELDS);
        String[] cronElements = expression.split("\\s+");
        int length = cronElements.length;
        if(length < NUM_FIELDS - 1) {
            throw new CronExpressionParseException("Incorrect field count: " + length);
        } else if(length < NUM_FIELDS) {
            // Year field is optional.
            log.info("(Optional) Year field is missing.");
        } else if(length > NUM_FIELDS) {
            // ignore the trailing fields...
            if(log.isLoggable(Level.WARNING)) log.warning("Incorrect field count: " + length + ". Trailing fields will be ignored.");
        }
        for(int f = 0; f < length; ++f) {
            cronFieldSets[f] = new TreeSet<Integer>();
            // [1] "Atoms" - Separated by commas
            String[] atoms = cronElements[f].split(",");
            for (String atom : atoms) {
                // [2] "Steps" - Specified as a division
                String numerator = atom;
                int step = 1;
                if(atom.contains("/")) {
                    String[] division = atom.split("/", 2);
                    numerator = division[0];
                    try {
                        step = Integer.valueOf(division[1]);
                    } catch (Exception e) {
                        throw new CronExpressionParseException("Incorrect division format: atom = " + atom, e);
                    }
                }
                // [3] "Ranges" - Separated by dash
                int rangeStart;
                int rangeEnd;
                if("*".equals(numerator)) {
                    rangeStart = RANGES[f][0];
                    rangeEnd = RANGES[f][1];
                } else if("?".equals(numerator) && (f == DAYS_OF_MONTH || f == DAYS_OF_WEEK)) {
                    // ?????
                    rangeStart = RANGES[f][0];
                    rangeEnd = RANGES[f][1];
                } else {
                    if(numerator.contains("-")) {
                        String[] ranges = numerator.split("-", 2);
                        try {
                            rangeStart = Integer.valueOf(ranges[0]);
                            rangeEnd = Integer.valueOf(ranges[1]);
                        } catch (Exception e) {
                            if(f == MONTHS && (sMapMonths.containsKey(ranges[0].toUpperCase()) && sMapMonths.containsKey(ranges[1].toUpperCase()))) {
                                rangeStart = sMapMonths.get(ranges[0].toUpperCase());
                                rangeEnd = sMapMonths.get(ranges[1].toUpperCase());
                            } else if(f == DAYS_OF_WEEK && (sMapDaysOfWeek.containsKey(ranges[0].toUpperCase()) && sMapDaysOfWeek.containsKey(ranges[1].toUpperCase()))) {
                                rangeStart = sMapDaysOfWeek.get(ranges[0].toUpperCase());
                                rangeEnd = sMapDaysOfWeek.get(ranges[1].toUpperCase());
                            } else {
                                throw new CronExpressionParseException("Incorrect range format: numerator = " + numerator, e);
                            }
                        }
                    } else {   // Single value.
                        try {
                            rangeStart = Integer.valueOf(numerator);
                        } catch (Exception e) {
                            if(f == MONTHS && sMapMonths.containsKey(numerator.toUpperCase())) {
                                rangeStart = sMapMonths.get(numerator.toUpperCase());
                            } else if(f == DAYS_OF_WEEK && sMapDaysOfWeek.containsKey(numerator.toUpperCase())) {
                                rangeStart = sMapDaysOfWeek.get(numerator.toUpperCase());
                            } else {
                                throw new CronExpressionParseException("Incorrect range format: numerator = " + numerator, e);
                            }
                        }
                        rangeEnd = rangeStart;
                    }
                }
                if(f == DAYS_OF_WEEK) {  // Note: We support formats like "0-3", "5-7"=="0,5-6", "6-8"=="0-1,6", etc...
                    // To support formats like SAT-SUN, 5-1, etc.
                    if(rangeStart > rangeEnd) {
                        rangeEnd += 7;
                    }
                    for(int i = rangeStart; i <= rangeEnd; i += step ) {
                        cronFieldSets[DAYS_OF_WEEK].add(i % 7);   // 7 -> 0
                    }
                } else {
                    for(int i = rangeStart; i <= rangeEnd; i += step ) {
                        cronFieldSets[f].add(i);
                    }
                }
            }
        }
        // This is not really necessary, but doing it here is kind of convenient for later use...
        if(cronFieldSets[YEARS] == null) {
            cronFieldSets[YEARS] = new TreeSet<Integer>();
            for(int i = RANGES[YEARS][0]; i <= RANGES[YEARS][0]; i++) {
                cronFieldSets[YEARS].add(i);
            }
        }
        
        // For debugging purposes...
        if(log.isLoggable(Level.FINER)) log.finer("cronFieldSets = " + toDebugString(cronFieldSets));
        
        log.fine("parse() END:");
        return cronFieldSets;
    }

    
    public NavigableSet<Integer> getMinutes() 
    {
        return cronFields[MINUTES];
    }
    public NavigableSet<Integer> getHours() 
    {
        return cronFields[HOURS];
    }
    public NavigableSet<Integer> getDays() 
    {
        return cronFields[DAYS_OF_MONTH];
    }
    public NavigableSet<Integer> getMonths() 
    {
        return cronFields[MONTHS];
    }
    public NavigableSet<Integer> getDaysOfWeek() 
    {
        return cronFields[DAYS_OF_WEEK];
    }
    public NavigableSet<Integer> getYears() 
    {
        return cronFields[YEARS];
    }


    // When is the earliest time when the cron job is scheduled to run (after "now")?
    public Long getNextCronTime()
    {
        return getNextCronTime(System.currentTimeMillis());
    }
    public Long getNextCronTime(long now)
    {
        if(log.isLoggable(Level.FINE)) log.fine("getNextCronTime() BEGIN: now = " + now);
        if(!isParsed()) {
            log.warning("Parser not initialized!");
            return null;   // ???
        }
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(this.timezone);
        calendar.setTimeInMillis(now);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        //int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        //int second = calendar.get(Calendar.SECOND);

        // Target value.
        Long nextCronTime = null;
        
        NavigableSet<Integer> tailYears = cronFields[YEARS].tailSet(year, true);
        Iterator<Integer> itYears = tailYears.iterator();
        
        // TBD: To avoid too many iterations (which is unlikely, but possible in certain pathological cases),
        //      we need to do either one or both of the following:
        //      (1) Set the max iterations.
        //      (2) Set a certain limit/delta after now, and returns null if nextCronTime is not found within the delta.
        // For now,
        //      Just log it....
        int counter = 0;

        GIANT_NESTED_LOOP:
        while(itYears.hasNext()) {
            int y = itYears.next();
            NavigableSet<Integer> tailMonths = null;
            if(y==year) {
                tailMonths = cronFields[MONTHS].tailSet(month, true);
            } else {  // y > year
                tailMonths = cronFields[MONTHS];                
            }
            Iterator<Integer> itMonths = tailMonths.iterator();
            while(itMonths.hasNext()) {
                int m = itMonths.next();
                NavigableSet<Integer> tailDays = null;
                if(y==year && m==month) {
                    tailDays = cronFields[DAYS_OF_MONTH].tailSet(dayOfMonth, true);
                } else {  // y > year
                    tailDays = cronFields[DAYS_OF_MONTH];                
                }
                Iterator<Integer> itDays = tailDays.iterator();
                while(itDays.hasNext()) {
                    int d = itDays.next();
                    int dayOfWeek = getDayOfWeek(y,m,d);   // 0~6.
                    //if(isValidDate(y,m,d)) {
                    if(dayOfWeek >= 0 && cronFields[DAYS_OF_WEEK].contains(dayOfWeek)) {
                        NavigableSet<Integer> tailHours = null;                        
                        if(y==year && m==month && d==dayOfMonth) {
                            tailHours = cronFields[HOURS].tailSet(hour, true);
                        } else {  // y > year
                            tailHours = cronFields[HOURS];                
                        }
                        Iterator<Integer> itHours = tailHours.iterator();
                        while(itHours.hasNext()) {
                            int hr = itHours.next();                            
                            NavigableSet<Integer> tailMinutes = null;                        
                            if(y==year && m==month && d==dayOfMonth && hr==hour) {
                                tailMinutes = cronFields[MINUTES].tailSet(minute, true);
                            } else {  // y > year
                                tailMinutes = cronFields[MINUTES];                
                            }
                            Iterator<Integer> itMinutes = tailMinutes.iterator();
                            while(itMinutes.hasNext()) {
                                int mn = itMinutes.next();

                                // OK, all these loops to test this one thing... :)
                                long t = getTime(y, m, d, hr, mn);
                                if(t >= now) {
                                    nextCronTime = t;
                                    break GIANT_NESTED_LOOP;
                                }
                                
                                counter++;
                                if(log.isLoggable(Level.FINER)) {
                                    if(counter % 10 == counter) {
                                        log.finer("Iteration counter: " + counter);
                                    }
                                }
                            }
                        }
                    } else {
                        // ignore, and continue...
                    }
                }
            }
        }
        // End of GIANT_NESTED_LOOP

        if(log.isLoggable(Level.FINE)) log.fine("getNextCronTime() END: nextCronTime = " + nextCronTime);
        return nextCronTime;
    }

    // When was the last time when the cron job was scheduled to run (prior to "now")?
    public Long getPreviousCronTime()
    {
        return getPreviousCronTime(System.currentTimeMillis());
    }
    public Long getPreviousCronTime(long now)
    {
        if(log.isLoggable(Level.FINE)) log.fine("getPreviousCronTime() BEGIN: now = " + now);
        if(!isParsed()) {
            log.warning("Parser not initialized!");
            return null;   // ???
        }
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(this.timezone);
        calendar.setTimeInMillis(now);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        //int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        //int second = calendar.get(Calendar.SECOND);

        // Target value.
        Long previousCronTime = null;
        
        NavigableSet<Integer> headYears = cronFields[YEARS].headSet(year, true);
        Iterator<Integer> itYears = headYears.descendingIterator();
        
        // Ditto...
        int counter = 0;

        GIANT_NESTED_LOOP:
        while(itYears.hasNext()) {
            int y = itYears.next();
            NavigableSet<Integer> headMonths = null;
            if(y==year) {
                headMonths = cronFields[MONTHS].headSet(month, true);
            } else {  // y > year
                headMonths = cronFields[MONTHS];                
            }
            Iterator<Integer> itMonths = headMonths.descendingIterator();
            while(itMonths.hasNext()) {
                int m = itMonths.next();
                NavigableSet<Integer> headDays = null;
                if(y==year && m==month) {
                    headDays = cronFields[DAYS_OF_MONTH].headSet(dayOfMonth, true);
                } else {  // y > year
                    headDays = cronFields[DAYS_OF_MONTH];                
                }
                Iterator<Integer> itDays = headDays.descendingIterator();
                while(itDays.hasNext()) {
                    int d = itDays.next();
                    int dayOfWeek = getDayOfWeek(y,m,d);   // 0~6.
                    //if(isValidDate(y,m,d)) {
                    if(dayOfWeek >= 0 && cronFields[DAYS_OF_WEEK].contains(dayOfWeek)) {
                        NavigableSet<Integer> headHours = null;                        
                        if(y==year && m==month && d==dayOfMonth) {
                            headHours = cronFields[HOURS].headSet(hour, true);
                        } else {  // y > year
                            headHours = cronFields[HOURS];                
                        }
                        Iterator<Integer> itHours = headHours.descendingIterator();
                        while(itHours.hasNext()) {
                            int hr = itHours.next();                            
                            NavigableSet<Integer> headMinutes = null;                        
                            if(y==year && m==month && d==dayOfMonth && hr==hour) {
                                headMinutes = cronFields[MINUTES].headSet(minute, true);
                            } else {  // y > year
                                headMinutes = cronFields[MINUTES];                
                            }
                            Iterator<Integer> itMinutes = headMinutes.descendingIterator();
                            while(itMinutes.hasNext()) {
                                int mn = itMinutes.next();

                                // OK, all these loops to test this one thing... :)
                                long t = getTime(y, m, d, hr, mn);
                                if(t <= now) {
                                    previousCronTime = t;
                                    break GIANT_NESTED_LOOP;
                                }
                                
                                counter++;
                                if(log.isLoggable(Level.FINER)) {
                                    if(counter % 10 == counter) {
                                        log.finer("Iteration counter: " + counter);
                                    }
                                }
                            }
                        }
                    } else {
                        // ignore, and continue...
                    }
                }
            }
        }
        // End of GIANT_NESTED_LOOP

        if(log.isLoggable(Level.FINE)) log.fine("getPreviousCronTime() END: previousCronTime = " + previousCronTime);
        return previousCronTime;
    }


    @Override
    public String toString() 
    {
        if(isParsed() && cronFields != null) {
            // TBD:
            return toDebugString(cronFields);
        } else {
            // TBD:
            return expression;
        }
    }
    private static String toDebugString(NavigableSet<Integer>[] cronFieldSets)
    {
        if(cronFieldSets != null) {
            // TBD: Validate cronFieldSets...
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            for(int j=0; j<cronFieldSets.length; j++) {
                sb.append("[");
                NavigableSet<Integer> fieldSet = cronFieldSets[j];
                if(fieldSet != null) {
                    Iterator<Integer> itFields = fieldSet.iterator();
                    while(itFields.hasNext()) {
                        Integer val = itFields.next();
                        sb.append(val);
                        if(itFields.hasNext()) {
                            sb.append(",");
                        }
                    }
                }
                sb.append("]");
                if(j < cronFieldSets.length-1) {
                    sb.append(",");
                }
            }
            sb.append("}");
            return sb.toString();
        } else {
            return null;
        }
    }

    
    // Note:
    // If we need to use timezone at all, it should be UTC
    // ....
    
    // m: 1-12
    public static boolean isValidDate(int y, int m, int d) 
    {
        try {
            Calendar c = Calendar.getInstance();
            c.setLenient(false);
            c.set(y, m-1, d);   // 0 for January
            c.getTime();        // To trigger the exception if the date is invalid...
        } catch(IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    // m: 1-12
    // day of week: 0-6
    // returns -1 if isValidDate(y,m,d)==false;
    public static int getDayOfWeek(int y, int m, int d) 
    {
        int dayOfWeek = -1;
        try {
            Calendar c = Calendar.getInstance();
            c.setLenient(false);
            c.set(y, m-1, d);   // 0 for January
            dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - 1;   // Calendar.SUNDAY==1
        } catch(IllegalArgumentException e) {
            // ignore
        }
        return dayOfWeek;
    }

    // m: 1-12
    public static long getTime(int y, int m, int d, int hr, int mn)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(y, m-1, d, hr, mn);
        long time = cal.getTimeInMillis();
        return time;
    }
    
    
    // Simple wrapper around IllegalArgumentException.
    // Place holder for now...
    public static final class CronExpressionParseException extends IllegalArgumentException
    {
        private static final long serialVersionUID = 1L;

        public CronExpressionParseException()
        {
            super();
        }
        public CronExpressionParseException(String message, Throwable cause)
        {
            super(message, cause);
        }
        public CronExpressionParseException(String s)
        {
            super(s);
        }
        public CronExpressionParseException(Throwable cause)
        {
            super(cause);
        }
        
    }
    
    
}