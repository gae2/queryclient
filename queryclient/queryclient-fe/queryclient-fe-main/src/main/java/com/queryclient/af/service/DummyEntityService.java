package com.queryclient.af.service;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.DummyEntity;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface DummyEntityService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    DummyEntity getDummyEntity(String guid) throws BaseException;
    Object getDummyEntity(String guid, String field) throws BaseException;
    List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException;
    List<DummyEntity> getAllDummyEntities() throws BaseException;
    /* @Deprecated */ List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException;
    List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException;
    //String createDummyEntity(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return DummyEntity?)
    String createDummyEntity(DummyEntity dummyEntity) throws BaseException;
    DummyEntity constructDummyEntity(DummyEntity dummyEntity) throws BaseException;
    Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException;
    //Boolean updateDummyEntity(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateDummyEntity(DummyEntity dummyEntity) throws BaseException;
    DummyEntity refreshDummyEntity(DummyEntity dummyEntity) throws BaseException;
    Boolean deleteDummyEntity(String guid) throws BaseException;
    Boolean deleteDummyEntity(DummyEntity dummyEntity) throws BaseException;
    Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createDummyEntities(List<DummyEntity> dummyEntities) throws BaseException;
//    Boolean updateDummyEntities(List<DummyEntity> dummyEntities) throws BaseException;

}
