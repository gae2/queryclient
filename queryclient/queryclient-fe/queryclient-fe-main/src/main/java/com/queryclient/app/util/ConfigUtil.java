package com.queryclient.app.util;

import java.util.logging.Logger;

import com.queryclient.af.config.Config;


// For fetching system default values.
// User-specific default values/settings should be read from somewhere else...
public class ConfigUtil
{
    private static final Logger log = Logger.getLogger(ConfigUtil.class.getName());
    private static Config sConfig = null;

    // temporary
    private static final int DEFAULT_PAGE_SIZE = 10;

    // Config keys... 
    private static final String KEY_PAGER_PAGESIZE = "queryclientapp.pager.pagesize";
    // ...
    // etc...

    // "Cache" values.  (TBD: Convert types to enums????)
    private static Integer sPagerPageSize = null;
    // ...
 
    
    // Static methods only.
    private ConfigUtil() {}

    
    public static int getDefaultPageSize()
    {
        return DEFAULT_PAGE_SIZE;
    }


    // Page size.
    public static Integer getPagerPageSize()
    {
        if(sPagerPageSize == null) {
            sPagerPageSize = Config.getInstance().getInteger(KEY_PAGER_PAGESIZE, getDefaultPageSize());
        }
        return sPagerPageSize;
    }

    
}
