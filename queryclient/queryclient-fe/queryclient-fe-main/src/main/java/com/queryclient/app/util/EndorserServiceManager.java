package com.queryclient.app.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniauth.credential.AccessCredential;
import org.miniauth.exception.CredentialStoreException;
import org.miniauth.oauth.credential.mapper.OAuthLocalConsumerCredentialMapper;
import org.miniauth.oauth.credential.mapper.OAuthSingleConsumerCredentialMapper;
import org.miniauth.oauth.service.OAuthConsumerEndorserService;
import org.miniauth.oauth.service.OAuthSingleConsumerEndorserService;
import org.miniauth.service.EndorserService;

import com.queryclient.app.endpoint.TargetServiceManager;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.exception.InternalServerErrorException;


/**
 * EndorserService "registry"/cache.
 */
public final class EndorserServiceManager
{
    private static final Logger log = Logger.getLogger(EndorserServiceManager.class.getName());
    
    // TBD:
    // We can use either a single OAuthConsumerEndorserService (with OAuthLocalConsumerCredentialMapper)
    // or we can use a map of <String, OAuthSinglCosnumerEndorserService> (with each endorserService associated with a OAuthSingleConsumerCredentialMapper).
    // ....

    // temporary
    private Map<String, OAuthSingleConsumerEndorserService> endorserMapCache = null;

    private OAuthConsumerEndorserService endorserService = null;
    private OAuthLocalConsumerCredentialMapper credentialMapper = null;
    
    private EndorserServiceManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static final class EndorserServiceHolder
    {
        private static final EndorserServiceManager INSTANCE = new EndorserServiceManager();
    }
    // Singleton method
    public static EndorserServiceManager getInstance()
    {
        return EndorserServiceHolder.INSTANCE;
    }

    
    private void init()
    {
        // temporary
        endorserMapCache = new HashMap<>();

        credentialMapper = OAuthLocalConsumerCredentialMapper.getInstance();
        endorserService = new OAuthConsumerEndorserService(credentialMapper);
        // ...
    }

    
    // TBD: ...
    // Which implementation is better???
    public EndorserService getSingleConsumerEndorserService(String service) throws BaseException
    {
        OAuthSingleConsumerEndorserService endorser = null;
        if(endorserMapCache.containsKey(service)) {
            endorser = endorserMapCache.get(service);
        } else {
            // temporary
            ConsumerKeySecretPair pair = TargetServiceManager.getInstance().getKeySecretPair(service);
            if(pair != null) {
                String consumerKey = pair.getConsumerKey();
                String consumerSecret = pair.getConsumerSecret();
                OAuthSingleConsumerCredentialMapper mapper = new OAuthSingleConsumerCredentialMapper(consumerKey, consumerSecret);
                endorser = new OAuthSingleConsumerEndorserService(mapper);
                endorserMapCache.put(service, endorser);
            } else {
                log.log(Level.WARNING, "Failed to create an EndorserService for service = " + service);
                throw new BaseException("Failed to create an EndorserService for service = " + service);
            }
        }
        return endorser;
    }

    public EndorserService getConsumerEndorserService(String service) throws BaseException
    {
        // temporary
        ConsumerKeySecretPair pair = TargetServiceManager.getInstance().getKeySecretPair(service);
        if(pair != null) {
            String consumerKey = pair.getConsumerKey();
            String consumerSecret = pair.getConsumerSecret();
            try {
                AccessCredential accessCredential = credentialMapper.getAccesssCredential(consumerKey);
                if(accessCredential == null) {
                    credentialMapper.putConsumerSecret(consumerKey, consumerSecret);
                } else {
                    // endorserService already includes the consumer key/secret pair for the given service.
                }
            } catch (CredentialStoreException e) {
                throw new InternalServerErrorException("Error while adding consumer key/secret for service = " + service, e);
            }
        } else {
            log.log(Level.WARNING, "Failed to get an EndorserService for service = " + service);
            throw new BaseException("Failed to get an EndorserService for service = " + service);
        }
        return endorserService;
    }

}
