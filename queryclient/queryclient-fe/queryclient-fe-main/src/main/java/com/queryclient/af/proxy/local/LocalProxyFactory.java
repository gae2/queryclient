package com.queryclient.af.proxy.local;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.ApiConsumerServiceProxy;
import com.queryclient.af.proxy.UserServiceProxy;
import com.queryclient.af.proxy.UserPasswordServiceProxy;
import com.queryclient.af.proxy.ExternalUserAuthServiceProxy;
import com.queryclient.af.proxy.UserAuthStateServiceProxy;
import com.queryclient.af.proxy.DataServiceServiceProxy;
import com.queryclient.af.proxy.ServiceEndpointServiceProxy;
import com.queryclient.af.proxy.QuerySessionServiceProxy;
import com.queryclient.af.proxy.QueryRecordServiceProxy;
import com.queryclient.af.proxy.DummyEntityServiceProxy;
import com.queryclient.af.proxy.ServiceInfoServiceProxy;
import com.queryclient.af.proxy.FiveTenServiceProxy;
import com.queryclient.ws.service.ApiConsumerService;
import com.queryclient.ws.service.UserService;
import com.queryclient.ws.service.UserPasswordService;
import com.queryclient.ws.service.ExternalUserAuthService;
import com.queryclient.ws.service.UserAuthStateService;
import com.queryclient.ws.service.DataServiceService;
import com.queryclient.ws.service.ServiceEndpointService;
import com.queryclient.ws.service.QuerySessionService;
import com.queryclient.ws.service.QueryRecordService;
import com.queryclient.ws.service.DummyEntityService;
import com.queryclient.ws.service.ServiceInfoService;
import com.queryclient.ws.service.FiveTenService;
import com.queryclient.ws.service.impl.ApiConsumerServiceImpl;
import com.queryclient.ws.service.impl.UserServiceImpl;
import com.queryclient.ws.service.impl.UserPasswordServiceImpl;
import com.queryclient.ws.service.impl.ExternalUserAuthServiceImpl;
import com.queryclient.ws.service.impl.UserAuthStateServiceImpl;
import com.queryclient.ws.service.impl.DataServiceServiceImpl;
import com.queryclient.ws.service.impl.ServiceEndpointServiceImpl;
import com.queryclient.ws.service.impl.QuerySessionServiceImpl;
import com.queryclient.ws.service.impl.QueryRecordServiceImpl;
import com.queryclient.ws.service.impl.DummyEntityServiceImpl;
import com.queryclient.ws.service.impl.ServiceInfoServiceImpl;
import com.queryclient.ws.service.impl.FiveTenServiceImpl;


// TBD: How to best inject the ws service instances?
public class LocalProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(LocalProxyFactory.class.getName());

    // Lazy initialization.
    private ApiConsumerServiceProxy apiConsumerServiceProxy = null;
    private UserServiceProxy userServiceProxy = null;
    private UserPasswordServiceProxy userPasswordServiceProxy = null;
    private ExternalUserAuthServiceProxy externalUserAuthServiceProxy = null;
    private UserAuthStateServiceProxy userAuthStateServiceProxy = null;
    private DataServiceServiceProxy dataServiceServiceProxy = null;
    private ServiceEndpointServiceProxy serviceEndpointServiceProxy = null;
    private QuerySessionServiceProxy querySessionServiceProxy = null;
    private QueryRecordServiceProxy queryRecordServiceProxy = null;
    private DummyEntityServiceProxy dummyEntityServiceProxy = null;
    private ServiceInfoServiceProxy serviceInfoServiceProxy = null;
    private FiveTenServiceProxy fiveTenServiceProxy = null;


    private LocalProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class LocalProxyFactoryHolder
    {
        private static final LocalProxyFactory INSTANCE = new LocalProxyFactory();
    }

    // Singleton method
    public static LocalProxyFactory getInstance()
    {
        return LocalProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        if(apiConsumerServiceProxy == null) {
            apiConsumerServiceProxy = new LocalApiConsumerServiceProxy();
            ApiConsumerService apiConsumerService = new ApiConsumerServiceImpl();
            ((LocalApiConsumerServiceProxy) apiConsumerServiceProxy).setApiConsumerService(apiConsumerService);
        }
        return apiConsumerServiceProxy;
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        if(userServiceProxy == null) {
            userServiceProxy = new LocalUserServiceProxy();
            UserService userService = new UserServiceImpl();
            ((LocalUserServiceProxy) userServiceProxy).setUserService(userService);
        }
        return userServiceProxy;
    }

    @Override
    public UserPasswordServiceProxy getUserPasswordServiceProxy()
    {
        if(userPasswordServiceProxy == null) {
            userPasswordServiceProxy = new LocalUserPasswordServiceProxy();
            UserPasswordService userPasswordService = new UserPasswordServiceImpl();
            ((LocalUserPasswordServiceProxy) userPasswordServiceProxy).setUserPasswordService(userPasswordService);
        }
        return userPasswordServiceProxy;
    }

    @Override
    public ExternalUserAuthServiceProxy getExternalUserAuthServiceProxy()
    {
        if(externalUserAuthServiceProxy == null) {
            externalUserAuthServiceProxy = new LocalExternalUserAuthServiceProxy();
            ExternalUserAuthService externalUserAuthService = new ExternalUserAuthServiceImpl();
            ((LocalExternalUserAuthServiceProxy) externalUserAuthServiceProxy).setExternalUserAuthService(externalUserAuthService);
        }
        return externalUserAuthServiceProxy;
    }

    @Override
    public UserAuthStateServiceProxy getUserAuthStateServiceProxy()
    {
        if(userAuthStateServiceProxy == null) {
            userAuthStateServiceProxy = new LocalUserAuthStateServiceProxy();
            UserAuthStateService userAuthStateService = new UserAuthStateServiceImpl();
            ((LocalUserAuthStateServiceProxy) userAuthStateServiceProxy).setUserAuthStateService(userAuthStateService);
        }
        return userAuthStateServiceProxy;
    }

    @Override
    public DataServiceServiceProxy getDataServiceServiceProxy()
    {
        if(dataServiceServiceProxy == null) {
            dataServiceServiceProxy = new LocalDataServiceServiceProxy();
            DataServiceService dataServiceService = new DataServiceServiceImpl();
            ((LocalDataServiceServiceProxy) dataServiceServiceProxy).setDataServiceService(dataServiceService);
        }
        return dataServiceServiceProxy;
    }

    @Override
    public ServiceEndpointServiceProxy getServiceEndpointServiceProxy()
    {
        if(serviceEndpointServiceProxy == null) {
            serviceEndpointServiceProxy = new LocalServiceEndpointServiceProxy();
            ServiceEndpointService serviceEndpointService = new ServiceEndpointServiceImpl();
            ((LocalServiceEndpointServiceProxy) serviceEndpointServiceProxy).setServiceEndpointService(serviceEndpointService);
        }
        return serviceEndpointServiceProxy;
    }

    @Override
    public QuerySessionServiceProxy getQuerySessionServiceProxy()
    {
        if(querySessionServiceProxy == null) {
            querySessionServiceProxy = new LocalQuerySessionServiceProxy();
            QuerySessionService querySessionService = new QuerySessionServiceImpl();
            ((LocalQuerySessionServiceProxy) querySessionServiceProxy).setQuerySessionService(querySessionService);
        }
        return querySessionServiceProxy;
    }

    @Override
    public QueryRecordServiceProxy getQueryRecordServiceProxy()
    {
        if(queryRecordServiceProxy == null) {
            queryRecordServiceProxy = new LocalQueryRecordServiceProxy();
            QueryRecordService queryRecordService = new QueryRecordServiceImpl();
            ((LocalQueryRecordServiceProxy) queryRecordServiceProxy).setQueryRecordService(queryRecordService);
        }
        return queryRecordServiceProxy;
    }

    @Override
    public DummyEntityServiceProxy getDummyEntityServiceProxy()
    {
        if(dummyEntityServiceProxy == null) {
            dummyEntityServiceProxy = new LocalDummyEntityServiceProxy();
            DummyEntityService dummyEntityService = new DummyEntityServiceImpl();
            ((LocalDummyEntityServiceProxy) dummyEntityServiceProxy).setDummyEntityService(dummyEntityService);
        }
        return dummyEntityServiceProxy;
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoServiceProxy == null) {
            serviceInfoServiceProxy = new LocalServiceInfoServiceProxy();
            ServiceInfoService serviceInfoService = new ServiceInfoServiceImpl();
            ((LocalServiceInfoServiceProxy) serviceInfoServiceProxy).setServiceInfoService(serviceInfoService);
        }
        return serviceInfoServiceProxy;
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenServiceProxy == null) {
            fiveTenServiceProxy = new LocalFiveTenServiceProxy();
            FiveTenService fiveTenService = new FiveTenServiceImpl();
            ((LocalFiveTenServiceProxy) fiveTenServiceProxy).setFiveTenService(fiveTenService);
        }
        return fiveTenServiceProxy;
    }

}
