package com.queryclient.af.util;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


// Note: Using formId is not really necessary.
// Generally, the same CSRF state can be used across the same session/user.
// In many cases, using a different CSRF state (for a different form) is not even really feasible
//    since the form generation code (often form-specific) 
//    and csrf state verification code (often common/shared) should agree on the value of CSRF state,
//    which is not always practical....
// (One way to do this is, if absolutely necessary, 
//    to use a global/static var of a common class or use config var, etc.,
//    for EACH form that requires form-specific csrf state value,
//    AND add some form-specific code into the csrf state verification routine...) 
public final class CsrfHelper
{
    private static final Logger log = Logger.getLogger(CsrfHelper.class.getName());

    // TBD:
    public static final String REQUEST_PARAM_CSRF_STATE = "_csrf_state";    // For CSRF protection....
    public static final String SESSION_ATTR_CSRF_STATE = "com.queryclient.af.util.csrf.state";
    // ...

    private CsrfHelper() 
    {
    }

    private static final class CsrfHelperHolder
    {
        private static final CsrfHelper INSTANCE = new CsrfHelper();
    }
    public static CsrfHelper getInstance()
    {
        return CsrfHelperHolder.INSTANCE;
    }

    
    // temporary
    public String generateRandomCsrfState()
    {
        String csrfState = null;
        try {
            // Note: this particular algo. Copied from Google OAuth2 doc.
            //       We can just use UUID, or any value sufficiently random.
            csrfState = new BigInteger(128, new SecureRandom()).toString(32);
        } catch(Exception e) {
            log.log(Level.WARNING, "Failed to generate csrfState.", e);
        }
        if(log.isLoggable(Level.FINER)) log.finer("csrfState generated: " + csrfState);
        return csrfState;
    }

    public String getCsrfStateFromRequest(HttpServletRequest request)
    {
        if(request == null) {
            return null;
        }
        String csrfState = request.getParameter(REQUEST_PARAM_CSRF_STATE);
        return csrfState;
    }

    // temporary
    public String getCsrfStateSessionAttributeName(String formId)
    {
        String attrName = SESSION_ATTR_CSRF_STATE;
        if(formId != null && !formId.isEmpty()) {
            attrName += "." + formId;
        }
        return attrName;
    }

    // Note: Somewhat unusual implementation
    // If non null/empty csrfState is given,it is stored in the session.
    // If null/empty csrfState is given, then 
    //         if the session attribute already exists, nothing changes. the existing value is returned.
    //         if none exists, then, and only then, a new random value is generated. 
    public String setCsrfStateSessionAttribute(HttpSession session)
    {
        return setCsrfStateSessionAttribute(session, null);
    }
    public String setCsrfStateSessionAttribute(HttpSession session, String csrfState)
    {
        return setCsrfStateSessionAttribute(session, csrfState, null);
    }
    public String setCsrfStateSessionAttribute(HttpSession session, String csrfState, String formId)
    {
        if(session == null) {
            return null;
        }
        String csrfStateAttrName = getCsrfStateSessionAttributeName(formId);
        if(csrfState == null || csrfState.isEmpty()) {
            // return false;    // ???
            // If it's already in the session, nothing else to do...
            // Note that if the input csrfState was not null, we overwrite the existing value, if any.
            csrfState = (String) session.getAttribute(csrfStateAttrName);
            if(csrfState != null && !csrfState.isEmpty()) {
                return csrfState;
            }
            csrfState = generateRandomCsrfState();
        }
        session.setAttribute(csrfStateAttrName, csrfState);
        return csrfState;
    }

    // "Reset" versions.
    public String resetCsrfStateSessionAttribute(HttpSession session)
    {
        return resetCsrfStateSessionAttribute(session, null);
    }
    public String resetCsrfStateSessionAttribute(HttpSession session, String csrfState)
    {
        return resetCsrfStateSessionAttribute(session, csrfState, null);
    }
    public String resetCsrfStateSessionAttribute(HttpSession session, String csrfState, String formId)
    {
        if(session == null) {
            return null;
        }
        String csrfStateAttrName = getCsrfStateSessionAttributeName(formId);
        if(csrfState == null || csrfState.isEmpty()) {
            csrfState = generateRandomCsrfState();
        }
        session.setAttribute(csrfStateAttrName, csrfState);
        return csrfState;
    }


    public String getCsrfStateFromSession(HttpServletRequest request)
    {
        return getCsrfStateFromSession(request, null);
    }
    public String getCsrfStateFromSession(HttpServletRequest request, String formId)
    {
        if(request == null) {
            return null;
        }
        HttpSession session = request.getSession();
        return getCsrfStateFromSession(session, formId);
    }
    public String getCsrfStateFromSession(HttpSession session)
    {
        return getCsrfStateFromSession(session, null);
    }
    public String getCsrfStateFromSession(HttpSession session, String formId)
    {
        if(session == null) {
            return null;
        }
        String csrfState = (String) session.getAttribute(getCsrfStateSessionAttributeName(formId));
        return csrfState;
    }

    public boolean verifyCsrfStateFromRequest(HttpServletRequest request, String sessionCsrfState)
    {
        return verifyCsrfStateFromRequest(request, null, sessionCsrfState);
    }
    public boolean verifyCsrfStateFromRequest(HttpServletRequest request, String reqCsrfState, String sessionCsrfState)
    {
        String requestCsrfState = reqCsrfState;
        if(requestCsrfState == null || requestCsrfState.isEmpty()) {
            requestCsrfState = getCsrfStateFromRequest(request);
        }
        if(log.isLoggable(Level.INFO)) log.info(">>>>> requestCsrfState = " + requestCsrfState + "; sessionCsrfState = " + sessionCsrfState);
        if(requestCsrfState == null || ! requestCsrfState.equals(sessionCsrfState)) {
            return false;
        } else {
            return true;
        }
    }
    public boolean verifyCsrfState(HttpServletRequest request)
    {
        return verifyCsrfState(request, null);
    }
    public boolean verifyCsrfState(HttpServletRequest request, String reqCsrfState)
    {
        return verifyCsrfState(request, reqCsrfState, null);
    }
    public boolean verifyCsrfState(HttpServletRequest request, String reqCsrfState, String formId)
    {
        if(request == null) {
            return false;
        }
        String sessionCsrfState = getCsrfStateFromSession(request, formId);
        return verifyCsrfStateFromRequest(request, reqCsrfState, sessionCsrfState);
    }

}
