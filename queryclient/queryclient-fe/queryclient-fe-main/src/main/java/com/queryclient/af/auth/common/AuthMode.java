package com.queryclient.af.auth.common;

import java.util.logging.Logger;


// temporary
public final class AuthMode
{
    private static final Logger log = Logger.getLogger(AuthMode.class.getName());

    // No auth required.
    public static final String NONE = "None";
    
    // Only the api client needs to be authenticated/authorized.
    public static final String CLIENT = "Client";
    
    // Per-user auth is required. (e.g., OAuth2).
    public static final String USER = "User";

    // etc...
    
    private AuthMode() {}


    
}
