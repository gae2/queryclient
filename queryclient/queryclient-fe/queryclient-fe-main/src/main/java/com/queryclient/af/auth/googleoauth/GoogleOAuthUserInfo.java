package com.queryclient.af.auth.googleoauth;

import java.io.Serializable;
import java.util.logging.Logger;
import java.util.logging.Level;


// Encapsulates the Google UserInfo data.
public final class GoogleOAuthUserInfo implements Serializable
{
    private static final Logger log = Logger.getLogger(GoogleOAuthUserInfo.class.getName());
    private static final long serialVersionUID = 1L;

    private String id;
    private String email;
    private Boolean emailVerified;
    private String name;
    private String givenName;
    private String familyName;
    private String profile;
    private String picture;
    private String gender;
    private String birthdate;
    private String locale;


    public GoogleOAuthUserInfo()
    {
        this(null);
    }
    public GoogleOAuthUserInfo(String id)
    {
        this(id, null);
    }
    public GoogleOAuthUserInfo(String id, String email)
    {
        this(id, email, null, null, null, null, null, null, null, null, null);
    }
    public GoogleOAuthUserInfo(String id, String email, Boolean emailVerified,
            String name, String givenName, String familyName, String profile,
            String picture, String gender, String birthdate, String locale)
    {
        super();
        this.id = id;
        this.email = email;
        this.emailVerified = emailVerified;
        this.name = name;
        this.givenName = givenName;
        this.familyName = familyName;
        this.profile = profile;
        this.picture = picture;
        this.gender = gender;
        this.birthdate = birthdate;
        this.locale = locale;
    }

    // The object should contain at least accessToken, otherwise it is "invalid".
    // Non-null/non-empty accessToken does not mean, the token is valid. (E.g., it might have expired.)
    // This method simply returns true if the accessToken is set.
    public boolean isValid()
    {
        return ( (id != null && !id.isEmpty()) || (email != null && !email.isEmpty()) );
    }


    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public String getEmail()
    {
        return email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public Boolean getEmailVerified()
    {
        return emailVerified;
    }
    public void setEmailVerified(Boolean emailVerified)
    {
        this.emailVerified = emailVerified;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getGivenName()
    {
        return givenName;
    }
    public void setGivenName(String givenName)
    {
        this.givenName = givenName;
    }

    public String getFamilyName()
    {
        return familyName;
    }
    public void setFamilyName(String familyName)
    {
        this.familyName = familyName;
    }

    public String getProfile()
    {
        return profile;
    }
    public void setProfile(String profile)
    {
        this.profile = profile;
    }

    public String getPicture()
    {
        return picture;
    }
    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getGender()
    {
        return gender;
    }
    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public String getBirthdate()
    {
        return birthdate;
    }
    public void setBirthdate(String birthdate)
    {
        this.birthdate = birthdate;
    }

    public String getLocale()
    {
        return locale;
    }
    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    @Override
    public String toString()
    {
        return "GoogleOAuthUserInfo [id=" + id + ", email=" + email
                + ", emailVerified=" + emailVerified + ", name=" + name
                + ", givenName=" + givenName + ", familyName=" + familyName
                + ", profile=" + profile + ", picture=" + picture + ", gender="
                + gender + ", birthdate=" + birthdate + ", locale=" + locale
                + "]";
    }
    
}
