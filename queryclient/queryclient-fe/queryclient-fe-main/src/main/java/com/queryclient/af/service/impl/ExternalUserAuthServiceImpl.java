package com.queryclient.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.ExternalUserAuthBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.ExternalUserAuthService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ExternalUserAuthServiceImpl implements ExternalUserAuthService
{
    private static final Logger log = Logger.getLogger(ExternalUserAuthServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "ExternalUserAuth-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("ExternalUserAuth:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public ExternalUserAuthServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // ExternalUserAuth related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ExternalUserAuth getExternalUserAuth(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getExternalUserAuth(): guid = " + guid);

        ExternalUserAuthBean bean = null;
        if(getCache() != null) {
            bean = (ExternalUserAuthBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ExternalUserAuthBean) getProxyFactory().getExternalUserAuthServiceProxy().getExternalUserAuth(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ExternalUserAuthBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ExternalUserAuthBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getExternalUserAuth(String guid, String field) throws BaseException
    {
        ExternalUserAuthBean bean = null;
        if(getCache() != null) {
            bean = (ExternalUserAuthBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ExternalUserAuthBean) getProxyFactory().getExternalUserAuthServiceProxy().getExternalUserAuth(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ExternalUserAuthBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ExternalUserAuthBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return bean.getManagerApp();
        } else if(field.equals("appAcl")) {
            return bean.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return bean.getGaeApp();
        } else if(field.equals("ownerUser")) {
            return bean.getOwnerUser();
        } else if(field.equals("userAcl")) {
            return bean.getUserAcl();
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("providerId")) {
            return bean.getProviderId();
        } else if(field.equals("externalUserId")) {
            return bean.getExternalUserId();
        } else if(field.equals("requestToken")) {
            return bean.getRequestToken();
        } else if(field.equals("accessToken")) {
            return bean.getAccessToken();
        } else if(field.equals("accessTokenSecret")) {
            return bean.getAccessTokenSecret();
        } else if(field.equals("email")) {
            return bean.getEmail();
        } else if(field.equals("firstName")) {
            return bean.getFirstName();
        } else if(field.equals("lastName")) {
            return bean.getLastName();
        } else if(field.equals("fullName")) {
            return bean.getFullName();
        } else if(field.equals("displayName")) {
            return bean.getDisplayName();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("gender")) {
            return bean.getGender();
        } else if(field.equals("dateOfBirth")) {
            return bean.getDateOfBirth();
        } else if(field.equals("profileImageUrl")) {
            return bean.getProfileImageUrl();
        } else if(field.equals("timeZone")) {
            return bean.getTimeZone();
        } else if(field.equals("postalCode")) {
            return bean.getPostalCode();
        } else if(field.equals("location")) {
            return bean.getLocation();
        } else if(field.equals("country")) {
            return bean.getCountry();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("authTime")) {
            return bean.getAuthTime();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ExternalUserAuth> getExternalUserAuths(List<String> guids) throws BaseException
    {
        log.fine("getExternalUserAuths()");

        // TBD: Is there a better way????
        List<ExternalUserAuth> externalUserAuths = getProxyFactory().getExternalUserAuthServiceProxy().getExternalUserAuths(guids);
        if(externalUserAuths == null) {
            log.log(Level.WARNING, "Failed to retrieve ExternalUserAuthBean list.");
        }

        log.finer("END");
        return externalUserAuths;
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths() throws BaseException
    {
        return getAllExternalUserAuths(null, null, null);
    }


    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllExternalUserAuths(ordering, offset, count, null);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllExternalUserAuths(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ExternalUserAuth> externalUserAuths = getProxyFactory().getExternalUserAuthServiceProxy().getAllExternalUserAuths(ordering, offset, count, forwardCursor);
        if(externalUserAuths == null) {
            log.log(Level.WARNING, "Failed to retrieve ExternalUserAuthBean list.");
        }

        log.finer("END");
        return externalUserAuths;
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllExternalUserAuthKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllExternalUserAuthKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getExternalUserAuthServiceProxy().getAllExternalUserAuthKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ExternalUserAuthBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ExternalUserAuthBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findExternalUserAuths(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ExternalUserAuthServiceImpl.findExternalUserAuths(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ExternalUserAuth> externalUserAuths = getProxyFactory().getExternalUserAuthServiceProxy().findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(externalUserAuths == null) {
            log.log(Level.WARNING, "Failed to find externalUserAuths for the given criterion.");
        }

        log.finer("END");
        return externalUserAuths;
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ExternalUserAuthServiceImpl.findExternalUserAuthKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getExternalUserAuthServiceProxy().findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ExternalUserAuth keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ExternalUserAuth key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ExternalUserAuthServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getExternalUserAuthServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createExternalUserAuth(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        ExternalUserIdStructBean externalUserIdBean = null;
        if(externalUserId instanceof ExternalUserIdStructBean) {
            externalUserIdBean = (ExternalUserIdStructBean) externalUserId;
        } else if(externalUserId instanceof ExternalUserIdStruct) {
            externalUserIdBean = new ExternalUserIdStructBean(externalUserId.getUuid(), externalUserId.getId(), externalUserId.getName(), externalUserId.getEmail(), externalUserId.getUsername(), externalUserId.getOpenId(), externalUserId.getNote());
        } else {
            externalUserIdBean = null;   // ????
        }
        ExternalUserAuthBean bean = new ExternalUserAuthBean(null, managerApp, appAcl, gaeAppBean, ownerUser, userAcl, user, providerId, externalUserIdBean, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
        return createExternalUserAuth(bean);
    }

    @Override
    public String createExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ExternalUserAuth bean = constructExternalUserAuth(externalUserAuth);
        //return bean.getGuid();

        // Param externalUserAuth cannot be null.....
        if(externalUserAuth == null) {
            log.log(Level.INFO, "Param externalUserAuth is null!");
            throw new BadRequestException("Param externalUserAuth object is null!");
        }
        ExternalUserAuthBean bean = null;
        if(externalUserAuth instanceof ExternalUserAuthBean) {
            bean = (ExternalUserAuthBean) externalUserAuth;
        } else if(externalUserAuth instanceof ExternalUserAuth) {
            // bean = new ExternalUserAuthBean(null, externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), (GaeAppStructBean) externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), (ExternalUserIdStructBean) externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ExternalUserAuthBean(externalUserAuth.getGuid(), externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), (GaeAppStructBean) externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), (ExternalUserIdStructBean) externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createExternalUserAuth(): Arg externalUserAuth is of an unknown type.");
            //bean = new ExternalUserAuthBean();
            bean = new ExternalUserAuthBean(externalUserAuth.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getExternalUserAuthServiceProxy().createExternalUserAuth(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ExternalUserAuth constructExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("BEGIN");

        // Param externalUserAuth cannot be null.....
        if(externalUserAuth == null) {
            log.log(Level.INFO, "Param externalUserAuth is null!");
            throw new BadRequestException("Param externalUserAuth object is null!");
        }
        ExternalUserAuthBean bean = null;
        if(externalUserAuth instanceof ExternalUserAuthBean) {
            bean = (ExternalUserAuthBean) externalUserAuth;
        } else if(externalUserAuth instanceof ExternalUserAuth) {
            // bean = new ExternalUserAuthBean(null, externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), (GaeAppStructBean) externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), (ExternalUserIdStructBean) externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ExternalUserAuthBean(externalUserAuth.getGuid(), externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), (GaeAppStructBean) externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), (ExternalUserIdStructBean) externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createExternalUserAuth(): Arg externalUserAuth is of an unknown type.");
            //bean = new ExternalUserAuthBean();
            bean = new ExternalUserAuthBean(externalUserAuth.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getExternalUserAuthServiceProxy().createExternalUserAuth(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateExternalUserAuth(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        ExternalUserIdStructBean externalUserIdBean = null;
        if(externalUserId instanceof ExternalUserIdStructBean) {
            externalUserIdBean = (ExternalUserIdStructBean) externalUserId;
        } else if(externalUserId instanceof ExternalUserIdStruct) {
            externalUserIdBean = new ExternalUserIdStructBean(externalUserId.getUuid(), externalUserId.getId(), externalUserId.getName(), externalUserId.getEmail(), externalUserId.getUsername(), externalUserId.getOpenId(), externalUserId.getNote());
        } else {
            externalUserIdBean = null;   // ????
        }
        ExternalUserAuthBean bean = new ExternalUserAuthBean(guid, managerApp, appAcl, gaeAppBean, ownerUser, userAcl, user, providerId, externalUserIdBean, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
        return updateExternalUserAuth(bean);
    }
        
    @Override
    public Boolean updateExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ExternalUserAuth bean = refreshExternalUserAuth(externalUserAuth);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param externalUserAuth cannot be null.....
        if(externalUserAuth == null || externalUserAuth.getGuid() == null) {
            log.log(Level.INFO, "Param externalUserAuth or its guid is null!");
            throw new BadRequestException("Param externalUserAuth object or its guid is null!");
        }
        ExternalUserAuthBean bean = null;
        if(externalUserAuth instanceof ExternalUserAuthBean) {
            bean = (ExternalUserAuthBean) externalUserAuth;
        } else {  // if(externalUserAuth instanceof ExternalUserAuth)
            bean = new ExternalUserAuthBean(externalUserAuth.getGuid(), externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), (GaeAppStructBean) externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), (ExternalUserIdStructBean) externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getExternalUserAuthServiceProxy().updateExternalUserAuth(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public ExternalUserAuth refreshExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("BEGIN");

        // Param externalUserAuth cannot be null.....
        if(externalUserAuth == null || externalUserAuth.getGuid() == null) {
            log.log(Level.INFO, "Param externalUserAuth or its guid is null!");
            throw new BadRequestException("Param externalUserAuth object or its guid is null!");
        }
        ExternalUserAuthBean bean = null;
        if(externalUserAuth instanceof ExternalUserAuthBean) {
            bean = (ExternalUserAuthBean) externalUserAuth;
        } else {  // if(externalUserAuth instanceof ExternalUserAuth)
            bean = new ExternalUserAuthBean(externalUserAuth.getGuid(), externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), (GaeAppStructBean) externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), (ExternalUserIdStructBean) externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getExternalUserAuthServiceProxy().updateExternalUserAuth(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteExternalUserAuth(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getExternalUserAuthServiceProxy().deleteExternalUserAuth(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            ExternalUserAuth externalUserAuth = null;
            try {
                externalUserAuth = getProxyFactory().getExternalUserAuthServiceProxy().getExternalUserAuth(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch externalUserAuth with a key, " + guid);
                return false;
            }
            if(externalUserAuth != null) {
                String beanGuid = externalUserAuth.getGuid();
                Boolean suc1 = getProxyFactory().getExternalUserAuthServiceProxy().deleteExternalUserAuth(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("externalUserAuth with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("BEGIN");

        // Param externalUserAuth cannot be null.....
        if(externalUserAuth == null || externalUserAuth.getGuid() == null) {
            log.log(Level.INFO, "Param externalUserAuth or its guid is null!");
            throw new BadRequestException("Param externalUserAuth object or its guid is null!");
        }
        ExternalUserAuthBean bean = null;
        if(externalUserAuth instanceof ExternalUserAuthBean) {
            bean = (ExternalUserAuthBean) externalUserAuth;
        } else {  // if(externalUserAuth instanceof ExternalUserAuth)
            // ????
            log.warning("externalUserAuth is not an instance of ExternalUserAuthBean.");
            bean = new ExternalUserAuthBean(externalUserAuth.getGuid(), externalUserAuth.getManagerApp(), externalUserAuth.getAppAcl(), (GaeAppStructBean) externalUserAuth.getGaeApp(), externalUserAuth.getOwnerUser(), externalUserAuth.getUserAcl(), externalUserAuth.getUser(), externalUserAuth.getProviderId(), (ExternalUserIdStructBean) externalUserAuth.getExternalUserId(), externalUserAuth.getRequestToken(), externalUserAuth.getAccessToken(), externalUserAuth.getAccessTokenSecret(), externalUserAuth.getEmail(), externalUserAuth.getFirstName(), externalUserAuth.getLastName(), externalUserAuth.getFullName(), externalUserAuth.getDisplayName(), externalUserAuth.getDescription(), externalUserAuth.getGender(), externalUserAuth.getDateOfBirth(), externalUserAuth.getProfileImageUrl(), externalUserAuth.getTimeZone(), externalUserAuth.getPostalCode(), externalUserAuth.getLocation(), externalUserAuth.getCountry(), externalUserAuth.getLanguage(), externalUserAuth.getStatus(), externalUserAuth.getAuthTime(), externalUserAuth.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getExternalUserAuthServiceProxy().deleteExternalUserAuth(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getExternalUserAuthServiceProxy().deleteExternalUserAuths(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createExternalUserAuths(List<ExternalUserAuth> externalUserAuths) throws BaseException
    {
        log.finer("BEGIN");

        if(externalUserAuths == null) {
            log.log(Level.WARNING, "createExternalUserAuths() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = externalUserAuths.size();
        if(size == 0) {
            log.log(Level.WARNING, "createExternalUserAuths() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ExternalUserAuth externalUserAuth : externalUserAuths) {
            String guid = createExternalUserAuth(externalUserAuth);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createExternalUserAuths() failed for at least one externalUserAuth. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateExternalUserAuths(List<ExternalUserAuth> externalUserAuths) throws BaseException
    //{
    //}

}
