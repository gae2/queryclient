package com.queryclient.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UserPasswordJsBean implements Serializable, Cloneable  //, UserPassword
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserPasswordJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructJsBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String admin;
    private String user;
    private String username;
    private String email;
    private String openId;
    private String plainPassword;
    private String hashedPassword;
    private String salt;
    private String hashMethod;
    private Boolean resetRequired;
    private String challengeQuestion;
    private String challengeAnswer;
    private String status;
    private Long lastResetTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserPasswordJsBean()
    {
        //this((String) null);
    }
    public UserPasswordJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserPasswordJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime, null, null);
    }
    public UserPasswordJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.admin = admin;
        this.user = user;
        this.username = username;
        this.email = email;
        this.openId = openId;
        this.plainPassword = plainPassword;
        this.hashedPassword = hashedPassword;
        this.salt = salt;
        this.hashMethod = hashMethod;
        this.resetRequired = resetRequired;
        this.challengeQuestion = challengeQuestion;
        this.challengeAnswer = challengeAnswer;
        this.status = status;
        this.lastResetTime = lastResetTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserPasswordJsBean(UserPasswordJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setManagerApp(bean.getManagerApp());
            setAppAcl(bean.getAppAcl());
            setGaeApp(bean.getGaeApp());
            setOwnerUser(bean.getOwnerUser());
            setUserAcl(bean.getUserAcl());
            setAdmin(bean.getAdmin());
            setUser(bean.getUser());
            setUsername(bean.getUsername());
            setEmail(bean.getEmail());
            setOpenId(bean.getOpenId());
            setPlainPassword(bean.getPlainPassword());
            setHashedPassword(bean.getHashedPassword());
            setSalt(bean.getSalt());
            setHashMethod(bean.getHashMethod());
            setResetRequired(bean.isResetRequired());
            setChallengeQuestion(bean.getChallengeQuestion());
            setChallengeAnswer(bean.getChallengeAnswer());
            setStatus(bean.getStatus());
            setLastResetTime(bean.getLastResetTime());
            setExpirationTime(bean.getExpirationTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static UserPasswordJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        UserPasswordJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(UserPasswordJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, UserPasswordJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStructJsBean getGaeApp()
    {  
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStructJsBean gaeApp)
    {
        this.gaeApp = gaeApp;
    }

    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    public String getAdmin()
    {
        return this.admin;
    }
    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getOpenId()
    {
        return this.openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    public String getPlainPassword()
    {
        return this.plainPassword;
    }
    public void setPlainPassword(String plainPassword)
    {
        this.plainPassword = plainPassword;
    }

    public String getHashedPassword()
    {
        return this.hashedPassword;
    }
    public void setHashedPassword(String hashedPassword)
    {
        this.hashedPassword = hashedPassword;
    }

    public String getSalt()
    {
        return this.salt;
    }
    public void setSalt(String salt)
    {
        this.salt = salt;
    }

    public String getHashMethod()
    {
        return this.hashMethod;
    }
    public void setHashMethod(String hashMethod)
    {
        this.hashMethod = hashMethod;
    }

    public Boolean isResetRequired()
    {
        return this.resetRequired;
    }
    public void setResetRequired(Boolean resetRequired)
    {
        this.resetRequired = resetRequired;
    }

    public String getChallengeQuestion()
    {
        return this.challengeQuestion;
    }
    public void setChallengeQuestion(String challengeQuestion)
    {
        this.challengeQuestion = challengeQuestion;
    }

    public String getChallengeAnswer()
    {
        return this.challengeAnswer;
    }
    public void setChallengeAnswer(String challengeAnswer)
    {
        this.challengeAnswer = challengeAnswer;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getLastResetTime()
    {
        return this.lastResetTime;
    }
    public void setLastResetTime(Long lastResetTime)
    {
        this.lastResetTime = lastResetTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("managerApp:null, ");
        sb.append("appAcl:0, ");
        sb.append("gaeApp:{}, ");
        sb.append("ownerUser:null, ");
        sb.append("userAcl:0, ");
        sb.append("admin:null, ");
        sb.append("user:null, ");
        sb.append("username:null, ");
        sb.append("email:null, ");
        sb.append("openId:null, ");
        sb.append("plainPassword:null, ");
        sb.append("hashedPassword:null, ");
        sb.append("salt:null, ");
        sb.append("hashMethod:null, ");
        sb.append("resetRequired:false, ");
        sb.append("challengeQuestion:null, ");
        sb.append("challengeAnswer:null, ");
        sb.append("status:null, ");
        sb.append("lastResetTime:0, ");
        sb.append("expirationTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("managerApp:");
        if(this.getManagerApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getManagerApp()).append("\", ");
        }
        sb.append("appAcl:" + this.getAppAcl()).append(", ");
        sb.append("gaeApp:");
        if(this.getGaeApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGaeApp()).append("\", ");
        }
        sb.append("ownerUser:");
        if(this.getOwnerUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOwnerUser()).append("\", ");
        }
        sb.append("userAcl:" + this.getUserAcl()).append(", ");
        sb.append("admin:");
        if(this.getAdmin() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAdmin()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("username:");
        if(this.getUsername() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUsername()).append("\", ");
        }
        sb.append("email:");
        if(this.getEmail() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getEmail()).append("\", ");
        }
        sb.append("openId:");
        if(this.getOpenId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOpenId()).append("\", ");
        }
        sb.append("plainPassword:");
        if(this.getPlainPassword() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPlainPassword()).append("\", ");
        }
        sb.append("hashedPassword:");
        if(this.getHashedPassword() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHashedPassword()).append("\", ");
        }
        sb.append("salt:");
        if(this.getSalt() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSalt()).append("\", ");
        }
        sb.append("hashMethod:");
        if(this.getHashMethod() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHashMethod()).append("\", ");
        }
        sb.append("resetRequired:" + this.isResetRequired()).append(", ");
        sb.append("challengeQuestion:");
        if(this.getChallengeQuestion() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChallengeQuestion()).append("\", ");
        }
        sb.append("challengeAnswer:");
        if(this.getChallengeAnswer() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChallengeAnswer()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("lastResetTime:" + this.getLastResetTime()).append(", ");
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getManagerApp() != null) {
            sb.append("\"managerApp\":").append("\"").append(this.getManagerApp()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"managerApp\":").append("null, ");
        }
        if(this.getAppAcl() != null) {
            sb.append("\"appAcl\":").append("").append(this.getAppAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appAcl\":").append("null, ");
        }
        sb.append("\"gaeApp\":").append(this.gaeApp.toJsonString()).append(", ");
        if(this.getOwnerUser() != null) {
            sb.append("\"ownerUser\":").append("\"").append(this.getOwnerUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ownerUser\":").append("null, ");
        }
        if(this.getUserAcl() != null) {
            sb.append("\"userAcl\":").append("").append(this.getUserAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userAcl\":").append("null, ");
        }
        if(this.getAdmin() != null) {
            sb.append("\"admin\":").append("\"").append(this.getAdmin()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"admin\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getUsername() != null) {
            sb.append("\"username\":").append("\"").append(this.getUsername()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"username\":").append("null, ");
        }
        if(this.getEmail() != null) {
            sb.append("\"email\":").append("\"").append(this.getEmail()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"email\":").append("null, ");
        }
        if(this.getOpenId() != null) {
            sb.append("\"openId\":").append("\"").append(this.getOpenId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"openId\":").append("null, ");
        }
        if(this.getPlainPassword() != null) {
            sb.append("\"plainPassword\":").append("\"").append(this.getPlainPassword()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"plainPassword\":").append("null, ");
        }
        if(this.getHashedPassword() != null) {
            sb.append("\"hashedPassword\":").append("\"").append(this.getHashedPassword()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"hashedPassword\":").append("null, ");
        }
        if(this.getSalt() != null) {
            sb.append("\"salt\":").append("\"").append(this.getSalt()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"salt\":").append("null, ");
        }
        if(this.getHashMethod() != null) {
            sb.append("\"hashMethod\":").append("\"").append(this.getHashMethod()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"hashMethod\":").append("null, ");
        }
        if(this.isResetRequired() != null) {
            sb.append("\"resetRequired\":").append("").append(this.isResetRequired()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"resetRequired\":").append("null, ");
        }
        if(this.getChallengeQuestion() != null) {
            sb.append("\"challengeQuestion\":").append("\"").append(this.getChallengeQuestion()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"challengeQuestion\":").append("null, ");
        }
        if(this.getChallengeAnswer() != null) {
            sb.append("\"challengeAnswer\":").append("\"").append(this.getChallengeAnswer()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"challengeAnswer\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getLastResetTime() != null) {
            sb.append("\"lastResetTime\":").append("").append(this.getLastResetTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastResetTime\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("managerApp = " + this.managerApp).append(";");
        sb.append("appAcl = " + this.appAcl).append(";");
        sb.append("gaeApp = " + this.gaeApp).append(";");
        sb.append("ownerUser = " + this.ownerUser).append(";");
        sb.append("userAcl = " + this.userAcl).append(";");
        sb.append("admin = " + this.admin).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("username = " + this.username).append(";");
        sb.append("email = " + this.email).append(";");
        sb.append("openId = " + this.openId).append(";");
        sb.append("plainPassword = " + this.plainPassword).append(";");
        sb.append("hashedPassword = " + this.hashedPassword).append(";");
        sb.append("salt = " + this.salt).append(";");
        sb.append("hashMethod = " + this.hashMethod).append(";");
        sb.append("resetRequired = " + this.resetRequired).append(";");
        sb.append("challengeQuestion = " + this.challengeQuestion).append(";");
        sb.append("challengeAnswer = " + this.challengeAnswer).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("lastResetTime = " + this.lastResetTime).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UserPasswordJsBean cloned = new UserPasswordJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setManagerApp(this.getManagerApp());   
        cloned.setAppAcl(this.getAppAcl());   
        cloned.setGaeApp( (GaeAppStructJsBean) this.getGaeApp().clone() );
        cloned.setOwnerUser(this.getOwnerUser());   
        cloned.setUserAcl(this.getUserAcl());   
        cloned.setAdmin(this.getAdmin());   
        cloned.setUser(this.getUser());   
        cloned.setUsername(this.getUsername());   
        cloned.setEmail(this.getEmail());   
        cloned.setOpenId(this.getOpenId());   
        cloned.setPlainPassword(this.getPlainPassword());   
        cloned.setHashedPassword(this.getHashedPassword());   
        cloned.setSalt(this.getSalt());   
        cloned.setHashMethod(this.getHashMethod());   
        cloned.setResetRequired(this.isResetRequired());   
        cloned.setChallengeQuestion(this.getChallengeQuestion());   
        cloned.setChallengeAnswer(this.getChallengeAnswer());   
        cloned.setStatus(this.getStatus());   
        cloned.setLastResetTime(this.getLastResetTime());   
        cloned.setExpirationTime(this.getExpirationTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
