package com.queryclient.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.fe.Validateable;
import com.queryclient.fe.core.StringEscapeUtil;
import com.queryclient.fe.bean.ConsumerKeySecretPairJsBean;
import com.queryclient.fe.bean.ServiceEndpointJsBean;


// Place holder...
public class ServiceEndpointFormBean extends ServiceEndpointJsBean implements Serializable, Cloneable, Validateable  //, ServiceEndpoint
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ServiceEndpointFormBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public ServiceEndpointFormBean()
    {
        super();
    }
    public ServiceEndpointFormBean(String guid)
    {
       super(guid);
    }
    public ServiceEndpointFormBean(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairJsBean authCredential, String status)
    {
        super(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredential, status);
    }
    public ServiceEndpointFormBean(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairJsBean authCredential, String status, Long createdTime, Long modifiedTime)
    {
        super(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredential, status, createdTime, modifiedTime);
    }
    public ServiceEndpointFormBean(ServiceEndpointJsBean bean)
    {
        super(bean);
    }

    public static ServiceEndpointFormBean fromJsonString(String jsonStr)
    {
        ServiceEndpointFormBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, ServiceEndpointFormBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUser() == null) {
//            addError("user", "user is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDataService() == null) {
//            addError("dataService", "dataService is null");
//            allOK = false;
//        }
//        // TBD
//        if(getServiceName() == null) {
//            addError("serviceName", "serviceName is null");
//            allOK = false;
//        }
//        // TBD
//        if(getServiceUrl() == null) {
//            addError("serviceUrl", "serviceUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(isAuthRequired() == null) {
//            addError("authRequired", "authRequired is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAuthCredential() == null) {
//            addError("authCredential", "authCredential is null");
//            allOK = false;
//        } else {
//            ConsumerKeySecretPairJsBean authCredential = getAuthCredential();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ServiceEndpointFormBean cloned = new ServiceEndpointFormBean((ServiceEndpointJsBean) super.clone());
        return cloned;
    }

}
