package com.queryclient.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.service.manager.ServiceManager;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.fe.bean.QueryRecordJsBean;
import com.queryclient.wa.util.ReferrerInfoStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class QueryRecordWebService // implements QueryRecordService
{
    private static final Logger log = Logger.getLogger(QueryRecordWebService.class.getName());
     
    // Af service interface.
    private QueryRecordService mService = null;

    public QueryRecordWebService()
    {
        this(ServiceManager.getQueryRecordService());
    }
    public QueryRecordWebService(QueryRecordService service)
    {
        mService = service;
    }
    
    private QueryRecordService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getQueryRecordService();
        }
        return mService;
    }
    
    
    public QueryRecordJsBean getQueryRecord(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            QueryRecord queryRecord = getService().getQueryRecord(guid);
            QueryRecordJsBean bean = convertQueryRecordToJsBean(queryRecord);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getQueryRecord(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getQueryRecord(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QueryRecordJsBean> getQueryRecords(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QueryRecordJsBean> jsBeans = new ArrayList<QueryRecordJsBean>();
            List<QueryRecord> queryRecords = getService().getQueryRecords(guids);
            if(queryRecords != null) {
                for(QueryRecord queryRecord : queryRecords) {
                    jsBeans.add(convertQueryRecordToJsBean(queryRecord));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QueryRecordJsBean> getAllQueryRecords() throws WebException
    {
        return getAllQueryRecords(null, null, null);
    }

    // @Deprecated
    public List<QueryRecordJsBean> getAllQueryRecords(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllQueryRecords(ordering, offset, count, null);
    }

    public List<QueryRecordJsBean> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QueryRecordJsBean> jsBeans = new ArrayList<QueryRecordJsBean>();
            List<QueryRecord> queryRecords = getService().getAllQueryRecords(ordering, offset, count, forwardCursor);
            if(queryRecords != null) {
                for(QueryRecord queryRecord : queryRecords) {
                    jsBeans.add(convertQueryRecordToJsBean(queryRecord));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<QueryRecordJsBean> findQueryRecords(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findQueryRecords(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<QueryRecordJsBean> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<QueryRecordJsBean> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QueryRecordJsBean> jsBeans = new ArrayList<QueryRecordJsBean>();
            List<QueryRecord> queryRecords = getService().findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(queryRecords != null) {
                for(QueryRecord queryRecord : queryRecords) {
                    jsBeans.add(convertQueryRecordToJsBean(queryRecord));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createQueryRecord(String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStructJsBean referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws WebException
    {
        try {
            return getService().createQueryRecord(querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, extra, note, scheduledTime, processedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createQueryRecord(String jsonStr) throws WebException
    {
        return createQueryRecord(QueryRecordJsBean.fromJsonString(jsonStr));
    }

    public String createQueryRecord(QueryRecordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QueryRecord queryRecord = convertQueryRecordJsBeanToBean(jsBean);
            return getService().createQueryRecord(queryRecord);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public QueryRecordJsBean constructQueryRecord(String jsonStr) throws WebException
    {
        return constructQueryRecord(QueryRecordJsBean.fromJsonString(jsonStr));
    }

    public QueryRecordJsBean constructQueryRecord(QueryRecordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QueryRecord queryRecord = convertQueryRecordJsBeanToBean(jsBean);
            queryRecord = getService().constructQueryRecord(queryRecord);
            jsBean = convertQueryRecordToJsBean(queryRecord);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStructJsBean referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws WebException
    {
        try {
            return getService().updateQueryRecord(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, extra, note, scheduledTime, processedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateQueryRecord(String jsonStr) throws WebException
    {
        return updateQueryRecord(QueryRecordJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateQueryRecord(QueryRecordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QueryRecord queryRecord = convertQueryRecordJsBeanToBean(jsBean);
            return getService().updateQueryRecord(queryRecord);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public QueryRecordJsBean refreshQueryRecord(String jsonStr) throws WebException
    {
        return refreshQueryRecord(QueryRecordJsBean.fromJsonString(jsonStr));
    }

    public QueryRecordJsBean refreshQueryRecord(QueryRecordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QueryRecord queryRecord = convertQueryRecordJsBeanToBean(jsBean);
            queryRecord = getService().refreshQueryRecord(queryRecord);
            jsBean = convertQueryRecordToJsBean(queryRecord);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteQueryRecord(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteQueryRecord(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteQueryRecord(QueryRecordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QueryRecord queryRecord = convertQueryRecordJsBeanToBean(jsBean);
            return getService().deleteQueryRecord(queryRecord);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteQueryRecords(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteQueryRecords(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static QueryRecordJsBean convertQueryRecordToJsBean(QueryRecord queryRecord)
    {
        QueryRecordJsBean jsBean = null;
        if(queryRecord != null) {
            jsBean = new QueryRecordJsBean();
            jsBean.setGuid(queryRecord.getGuid());
            jsBean.setQuerySession(queryRecord.getQuerySession());
            jsBean.setQueryId(queryRecord.getQueryId());
            jsBean.setDataService(queryRecord.getDataService());
            jsBean.setServiceUrl(queryRecord.getServiceUrl());
            jsBean.setDelayed(queryRecord.isDelayed());
            jsBean.setQuery(queryRecord.getQuery());
            jsBean.setInputFormat(queryRecord.getInputFormat());
            jsBean.setInputFile(queryRecord.getInputFile());
            jsBean.setInputContent(queryRecord.getInputContent());
            jsBean.setTargetOutputFormat(queryRecord.getTargetOutputFormat());
            jsBean.setOutputFormat(queryRecord.getOutputFormat());
            jsBean.setOutputFile(queryRecord.getOutputFile());
            jsBean.setOutputContent(queryRecord.getOutputContent());
            jsBean.setResponseCode(queryRecord.getResponseCode());
            jsBean.setResult(queryRecord.getResult());
            jsBean.setReferrerInfo(ReferrerInfoStructWebUtil.convertReferrerInfoStructToJsBean(queryRecord.getReferrerInfo()));
            jsBean.setStatus(queryRecord.getStatus());
            jsBean.setExtra(queryRecord.getExtra());
            jsBean.setNote(queryRecord.getNote());
            jsBean.setScheduledTime(queryRecord.getScheduledTime());
            jsBean.setProcessedTime(queryRecord.getProcessedTime());
            jsBean.setCreatedTime(queryRecord.getCreatedTime());
            jsBean.setModifiedTime(queryRecord.getModifiedTime());
        }
        return jsBean;
    }

    public static QueryRecord convertQueryRecordJsBeanToBean(QueryRecordJsBean jsBean)
    {
        QueryRecordBean queryRecord = null;
        if(jsBean != null) {
            queryRecord = new QueryRecordBean();
            queryRecord.setGuid(jsBean.getGuid());
            queryRecord.setQuerySession(jsBean.getQuerySession());
            queryRecord.setQueryId(jsBean.getQueryId());
            queryRecord.setDataService(jsBean.getDataService());
            queryRecord.setServiceUrl(jsBean.getServiceUrl());
            queryRecord.setDelayed(jsBean.isDelayed());
            queryRecord.setQuery(jsBean.getQuery());
            queryRecord.setInputFormat(jsBean.getInputFormat());
            queryRecord.setInputFile(jsBean.getInputFile());
            queryRecord.setInputContent(jsBean.getInputContent());
            queryRecord.setTargetOutputFormat(jsBean.getTargetOutputFormat());
            queryRecord.setOutputFormat(jsBean.getOutputFormat());
            queryRecord.setOutputFile(jsBean.getOutputFile());
            queryRecord.setOutputContent(jsBean.getOutputContent());
            queryRecord.setResponseCode(jsBean.getResponseCode());
            queryRecord.setResult(jsBean.getResult());
            queryRecord.setReferrerInfo(ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            queryRecord.setStatus(jsBean.getStatus());
            queryRecord.setExtra(jsBean.getExtra());
            queryRecord.setNote(jsBean.getNote());
            queryRecord.setScheduledTime(jsBean.getScheduledTime());
            queryRecord.setProcessedTime(jsBean.getProcessedTime());
            queryRecord.setCreatedTime(jsBean.getCreatedTime());
            queryRecord.setModifiedTime(jsBean.getModifiedTime());
        }
        return queryRecord;
    }

}
