package com.queryclient.app.service;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.service.AbstractServiceFactory;
import com.queryclient.af.service.ApiConsumerService;
import com.queryclient.af.service.UserService;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.af.service.ExternalUserAuthService;
import com.queryclient.af.service.UserAuthStateService;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.service.ServiceInfoService;
import com.queryclient.af.service.FiveTenService;

public class AppServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(AppServiceFactory.class.getName());

    private AppServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AppServiceFactoryHolder
    {
        private static final AppServiceFactory INSTANCE = new AppServiceFactory();
    }

    // Singleton method
    public static AppServiceFactory getInstance()
    {
        return AppServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerAppService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserAppService();
    }

    @Override
    public UserPasswordService getUserPasswordService()
    {
        return new UserPasswordAppService();
    }

    @Override
    public ExternalUserAuthService getExternalUserAuthService()
    {
        return new ExternalUserAuthAppService();
    }

    @Override
    public UserAuthStateService getUserAuthStateService()
    {
        return new UserAuthStateAppService();
    }

    @Override
    public DataServiceService getDataServiceService()
    {
        return new DataServiceAppService();
    }

    @Override
    public ServiceEndpointService getServiceEndpointService()
    {
        return new ServiceEndpointAppService();
    }

    @Override
    public QuerySessionService getQuerySessionService()
    {
        return new QuerySessionAppService();
    }

    @Override
    public QueryRecordService getQueryRecordService()
    {
        return new QueryRecordAppService();
    }

    @Override
    public DummyEntityService getDummyEntityService()
    {
        return new DummyEntityAppService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoAppService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenAppService();
    }


}
