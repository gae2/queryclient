package com.queryclient.af.auth.googleplus;

import java.io.Serializable;
import java.util.logging.Logger;
import java.util.logging.Level;


public class GooglePlusAccessToken implements Serializable
{
    private static final Logger log = Logger.getLogger(GooglePlusAccessToken.class.getName());
    private static final long serialVersionUID = 1L;

    // "Read-only" variables. 
    private final String accessToken;
    private final String refreshToken;
    private final long expirationTime;

    public GooglePlusAccessToken(String accessToken, String refreshToken)
    {
        this(accessToken, refreshToken, 0L);
    }
    public GooglePlusAccessToken(String accessToken, String refreshToken, long expirationTime)
    {
        super();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        if(expirationTime > 0L) {
            this.expirationTime = expirationTime;
        } else {
            this.expirationTime = System.currentTimeMillis();    // ????
        }
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public String getRefreshToken()
    {
        return refreshToken;
    }

    public long getExpirationTime()
    {
        return expirationTime;
    }
    

}
