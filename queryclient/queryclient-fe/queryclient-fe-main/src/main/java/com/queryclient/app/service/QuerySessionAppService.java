package com.queryclient.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.QuerySession;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.service.impl.QuerySessionServiceImpl;
import com.queryclient.af.util.URLUtil;
import com.queryclient.app.endpoint.TargetServiceManager;


// Updated
public class QuerySessionAppService extends QuerySessionServiceImpl implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(QuerySessionAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public QuerySessionAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // QuerySession related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        return super.getQuerySession(guid);
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        return super.getQuerySession(guid, field);
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        return super.getQuerySessions(guids);
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return super.getAllQuerySessions();
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllQuerySessionKeys(ordering, offset, count);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createQuerySession(QuerySession querySession) throws BaseException
    {
        // TBD:
        querySession = validateQuerySession(querySession);
        // ...

        String guid = super.createQuerySession(querySession);
        return guid;
    }

    @Override
    public QuerySession constructQuerySession(QuerySession querySession) throws BaseException
    {
        // TBD:
        querySession = validateQuerySession(querySession);
        // ...

        querySession = super.constructQuerySession(querySession);
        return querySession;
    }


    @Override
    public Boolean updateQuerySession(QuerySession querySession) throws BaseException
    {
        return super.updateQuerySession(querySession);
    }
        
    @Override
    public QuerySession refreshQuerySession(QuerySession querySession) throws BaseException
    {
        return super.refreshQuerySession(querySession);
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        return super.deleteQuerySession(guid);
    }

    @Override
    public Boolean deleteQuerySession(QuerySession querySession) throws BaseException
    {
        return super.deleteQuerySession(querySession);
    }

    @Override
    public Integer createQuerySessions(List<QuerySession> querySessions) throws BaseException
    {
        return super.createQuerySessions(querySessions);
    }

    // TBD
    //@Override
    //public Boolean updateQuerySessions(List<QuerySession> querySessions) throws BaseException
    //{
    //}

    
    private QuerySessionBean validateQuerySession(QuerySession querySession) throws BaseException
    {
        log.fine("ENTERING: validateQuerySession()");

        QuerySessionBean bean = null;
        if(querySession instanceof QuerySessionBean) {
            bean = (QuerySessionBean) querySession;
        } else {
            // ????? Can this happen????
            bean = new QuerySessionBean(querySession);
        }

        // TBD:
        String serviceUrl = bean.getServiceUrl();
        if(serviceUrl != null && !serviceUrl.isEmpty()) {
            // Validate...
            boolean isValid = URLUtil.isValidUrl(serviceUrl);
            if(isValid == false) {
                String primaryServiceUrl = TargetServiceManager.getInstance().getPrimaryServiceUrl(serviceUrl);
                if(URLUtil.isValidUrl(primaryServiceUrl)) {
                    bean.setServiceUrl(primaryServiceUrl);
                } else {
                    // ???
                }
            }
        }
        // ...        
        
        // etc..
        
        return bean;
    }

}
