package com.queryclient.af.cert.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.cert.PublicCertificateInfo;
import com.queryclient.ws.cert.stub.PublicCertificateInfoStub;

// Wrapper class + bean combo.
public class PublicCertificateInfoBean implements PublicCertificateInfo, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PublicCertificateInfoBean.class.getName());

    // [1] With an embedded object.
    private PublicCertificateInfoStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String appId;
    private String appUrl;
    private String certName;
    private String certInPemFormat;
    private String note;
    private String status;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public PublicCertificateInfoBean()
    {
        //this((String) null);
    }
    public PublicCertificateInfoBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public PublicCertificateInfoBean(String guid, String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime)
    {
        this(guid, appId, appUrl, certName, certInPemFormat, note, status, expirationTime, null, null);
    }
    public PublicCertificateInfoBean(String guid, String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.appId = appId;
        this.appUrl = appUrl;
        this.certName = certName;
        this.certInPemFormat = certInPemFormat;
        this.note = note;
        this.status = status;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public PublicCertificateInfoBean(PublicCertificateInfo stub)
    {
        if(stub instanceof PublicCertificateInfoStub) {
            this.stub = (PublicCertificateInfoStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAppId(stub.getAppId());   
            setAppUrl(stub.getAppUrl());   
            setCertName(stub.getCertName());   
            setCertInPemFormat(stub.getCertInPemFormat());   
            setNote(stub.getNote());   
            setStatus(stub.getStatus());   
            setExpirationTime(stub.getExpirationTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            log.log(Level.WARNING, "The arg stub object is null.");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getAppId()
    {
        if(getStub() != null) {
            return getStub().getAppId();
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            return this.appId;
        }
    }
    public void setAppId(String appId)
    {
        if(getStub() != null) {
            getStub().setAppId(appId);
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            this.appId = appId;
        }
    }

    public String getAppUrl()
    {
        if(getStub() != null) {
            return getStub().getAppUrl();
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            return this.appUrl;
        }
    }
    public void setAppUrl(String appUrl)
    {
        if(getStub() != null) {
            getStub().setAppUrl(appUrl);
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            this.appUrl = appUrl;
        }
    }

    public String getCertName()
    {
        if(getStub() != null) {
            return getStub().getCertName();
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            return this.certName;
        }
    }
    public void setCertName(String certName)
    {
        if(getStub() != null) {
            getStub().setCertName(certName);
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            this.certName = certName;
        }
    }

    public String getCertInPemFormat()
    {
        if(getStub() != null) {
            return getStub().getCertInPemFormat();
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            return this.certInPemFormat;
        }
    }
    public void setCertInPemFormat(String certInPemFormat)
    {
        if(getStub() != null) {
            getStub().setCertInPemFormat(certInPemFormat);
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            this.certInPemFormat = certInPemFormat;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            this.note = note;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getExpirationTime();
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            return this.expirationTime;
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getStub() != null) {
            getStub().setExpirationTime(expirationTime);
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            this.expirationTime = expirationTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINE, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public PublicCertificateInfoStub getStub()
    {
        return this.stub;
    }
    protected void setStub(PublicCertificateInfoStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("appId = " + this.appId).append(";");
            sb.append("appUrl = " + this.appUrl).append(";");
            sb.append("certName = " + this.certName).append(";");
            sb.append("certInPemFormat = " + this.certInPemFormat).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("expirationTime = " + this.expirationTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            hash = 31 * hash + delta;
            delta = appId == null ? 0 : appId.hashCode();
            hash = 31 * hash + delta;
            delta = appUrl == null ? 0 : appUrl.hashCode();
            hash = 31 * hash + delta;
            delta = certName == null ? 0 : certName.hashCode();
            hash = 31 * hash + delta;
            delta = certInPemFormat == null ? 0 : certInPemFormat.hashCode();
            hash = 31 * hash + delta;
            delta = note == null ? 0 : note.hashCode();
            hash = 31 * hash + delta;
            delta = status == null ? 0 : status.hashCode();
            hash = 31 * hash + delta;
            delta = expirationTime == null ? 0 : expirationTime.hashCode();
            hash = 31 * hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            hash = 31 * hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            hash = 31 * hash + delta;
            return hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
