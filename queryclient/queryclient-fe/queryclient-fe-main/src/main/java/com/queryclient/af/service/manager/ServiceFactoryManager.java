package com.queryclient.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.service.AbstractServiceFactory;
import com.queryclient.app.service.AppServiceFactory;


// We use Abstract Factory pattern.
// This "manager" class provides a way to choose a concrete factory.
public final class ServiceFactoryManager
{
    private static final Logger log = Logger.getLogger(ServiceFactoryManager.class.getName());

    // Prevents instantiation.
    private ServiceFactoryManager() {}

    // Returns a service factory.
    public static AbstractServiceFactory getServiceFactory() 
    {
        // For now, hard-coded.
        // TBD: Read it from a config.
        return AppServiceFactory.getInstance();
    }

}
