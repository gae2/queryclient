package com.queryclient.util.parser;

import java.util.List;
import java.util.logging.Logger;

public class QueryObject
{
    private static final Logger log = Logger.getLogger(QueryObject.class.getName());

    // "Tokens"
    private QueryMethod method = null;
    private String entity = null;
    private String guid = null;
    private Boolean unique = null;
    private String fields = null;
    private String aggregate = null;
    private String filter = null;
    private String ordering = null;
    private String params = null;
    private List<String> values = null;
    private String grouping = null;
    private Long offset = null;
    private Integer count = null;
    private String payload = null;
    // mimetype, etc...
    // ...
    
    public QueryObject()
    {
    }
    
    // TBD:
    // method for validating the queryObject ???
    // ...

    public QueryMethod getMethod()
    {
        return method;
    }
    public void setMethod(QueryMethod method)
    {
        this.method = method;
    }

    public String getGuid()
    {
        return guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getEntity()
    {
        return entity;
    }
    public void setEntity(String entity)
    {
        this.entity = entity;
    }

    public String getFields()
    {
        return fields;
    }
    public void setFields(String fields)
    {
        this.fields = fields;
    }

    public String getFilter()
    {
        return filter;
    }
    public void setFilter(String filter)
    {
        this.filter = filter;
    }

    public String getOrdering()
    {
        return ordering;
    }
    public void setOrdering(String ordering)
    {
        this.ordering = ordering;
    }

    public String getParams()
    {
        return params;
    }
    public void setParams(String params)
    {
        this.params = params;
    }

    public List<String> getValues()
    {
        return values;
    }
    public void setValues(List<String> values)
    {
        this.values = values;
    }

    public String getGrouping()
    {
        return grouping;
    }
    public void setGrouping(String grouping)
    {
        this.grouping = grouping;
    }

    public Boolean getUnique()
    {
        return unique;
    }
    public void setUnique(Boolean unique)
    {
        this.unique = unique;
    }

    public String getAggregate()
    {
        return aggregate;
    }
    public void setAggregate(String aggregate)
    {
        this.aggregate = aggregate;
    }

    public Long getOffset()
    {
        return offset;
    }
    public void setOffset(Long offset)
    {
        this.offset = offset;
    }

    public Integer getCount()
    {
        return count;
    }
    public void setCount(Integer count)
    {
        this.count = count;
    }

    public String getPayload()
    {
        return payload;
    }
    public void setPayload(String payload)
    {
        this.payload = payload;
    }

    @Override
    public String toString()
    {
        return "QueryObject [method=" + method + ", entity=" + entity
                + ", guid=" + guid + ", unique=" + unique + ", fields="
                + fields + ", aggregate=" + aggregate + ", filter=" + filter
                + ", ordering=" + ordering + ", params=" + params + ", values="
                + values + ", grouping=" + grouping + ", offset=" + offset
                + ", count=" + count + ", payload=" + payload + "]";
    }

}
