package com.queryclient.af.auth.common;

import java.io.Serializable;
import java.util.logging.Logger;
import java.util.logging.Level;


// OAuth RequestToken wrapper ( == { token, tokenSecret } + timestamp when the request token was obtained )
// Named OAuthRequestToken, to avoid the name collision with twitter4j.auth.RequestToken
public class OAuthRequestToken implements Serializable
{
    private static final Logger log = Logger.getLogger(OAuthRequestToken.class.getName());
    private static final long serialVersionUID = 1L;

    // "Read-only" variables. 
    private final String token;
    private final String tokenSecret;
    private final long timestamp;

    public OAuthRequestToken(String token, String tokenSecret)
    {
        this(token, tokenSecret, 0L);
    }
    public OAuthRequestToken(String token, String tokenSecret, long timestamp)
    {
        super();
        this.token = token;
        this.tokenSecret = tokenSecret;
        if(timestamp > 0L) {
            this.timestamp = timestamp;
        } else {
            this.timestamp = System.currentTimeMillis();
        }
    }

    public String getToken()
    {
        return token;
    }

    public String getTokenSecret()
    {
        return tokenSecret;
    }

    public long getTimestamp()
    {
        return timestamp;
    }
    

}
