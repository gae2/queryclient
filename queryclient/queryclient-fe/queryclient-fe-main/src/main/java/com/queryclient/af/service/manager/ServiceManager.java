package com.queryclient.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.service.AbstractServiceFactory;
import com.queryclient.af.service.ApiConsumerService;
import com.queryclient.af.service.UserService;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.af.service.ExternalUserAuthService;
import com.queryclient.af.service.UserAuthStateService;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.service.ServiceInfoService;
import com.queryclient.af.service.FiveTenService;

// TBD:
// Factory? DI? (Does the current "dual" approach make sense?)
// Make it a singleton? Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

    // Reference to the Abstract factory.
    private static AbstractServiceFactory serviceFactory = ServiceFactoryManager.getServiceFactory();

    // All service getters/setters are delegated to ServiceController.
    // TBD: Use a DI framework such as Spring or Guice....
    private static ServiceController serviceController = new ServiceController(null, null, null, null, null, null, null, null, null, null, null, null);

    // Static methods only.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(serviceController.getApiConsumerService() == null) {
            serviceController.setApiConsumerService(serviceFactory.getApiConsumerService());
        }
        return serviceController.getApiConsumerService();
    }
    // Injects a ApiConsumerService instance.
	public static void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        serviceController.setApiConsumerService(apiConsumerService);
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(serviceController.getUserService() == null) {
            serviceController.setUserService(serviceFactory.getUserService());
        }
        return serviceController.getUserService();
    }
    // Injects a UserService instance.
	public static void setUserService(UserService userService) 
    {
        serviceController.setUserService(userService);
    }

    // Returns a UserPasswordService instance.
	public static UserPasswordService getUserPasswordService() 
    {
        if(serviceController.getUserPasswordService() == null) {
            serviceController.setUserPasswordService(serviceFactory.getUserPasswordService());
        }
        return serviceController.getUserPasswordService();
    }
    // Injects a UserPasswordService instance.
	public static void setUserPasswordService(UserPasswordService userPasswordService) 
    {
        serviceController.setUserPasswordService(userPasswordService);
    }

    // Returns a ExternalUserAuthService instance.
	public static ExternalUserAuthService getExternalUserAuthService() 
    {
        if(serviceController.getExternalUserAuthService() == null) {
            serviceController.setExternalUserAuthService(serviceFactory.getExternalUserAuthService());
        }
        return serviceController.getExternalUserAuthService();
    }
    // Injects a ExternalUserAuthService instance.
	public static void setExternalUserAuthService(ExternalUserAuthService externalUserAuthService) 
    {
        serviceController.setExternalUserAuthService(externalUserAuthService);
    }

    // Returns a UserAuthStateService instance.
	public static UserAuthStateService getUserAuthStateService() 
    {
        if(serviceController.getUserAuthStateService() == null) {
            serviceController.setUserAuthStateService(serviceFactory.getUserAuthStateService());
        }
        return serviceController.getUserAuthStateService();
    }
    // Injects a UserAuthStateService instance.
	public static void setUserAuthStateService(UserAuthStateService userAuthStateService) 
    {
        serviceController.setUserAuthStateService(userAuthStateService);
    }

    // Returns a DataServiceService instance.
	public static DataServiceService getDataServiceService() 
    {
        if(serviceController.getDataServiceService() == null) {
            serviceController.setDataServiceService(serviceFactory.getDataServiceService());
        }
        return serviceController.getDataServiceService();
    }
    // Injects a DataServiceService instance.
	public static void setDataServiceService(DataServiceService dataServiceService) 
    {
        serviceController.setDataServiceService(dataServiceService);
    }

    // Returns a ServiceEndpointService instance.
	public static ServiceEndpointService getServiceEndpointService() 
    {
        if(serviceController.getServiceEndpointService() == null) {
            serviceController.setServiceEndpointService(serviceFactory.getServiceEndpointService());
        }
        return serviceController.getServiceEndpointService();
    }
    // Injects a ServiceEndpointService instance.
	public static void setServiceEndpointService(ServiceEndpointService serviceEndpointService) 
    {
        serviceController.setServiceEndpointService(serviceEndpointService);
    }

    // Returns a QuerySessionService instance.
	public static QuerySessionService getQuerySessionService() 
    {
        if(serviceController.getQuerySessionService() == null) {
            serviceController.setQuerySessionService(serviceFactory.getQuerySessionService());
        }
        return serviceController.getQuerySessionService();
    }
    // Injects a QuerySessionService instance.
	public static void setQuerySessionService(QuerySessionService querySessionService) 
    {
        serviceController.setQuerySessionService(querySessionService);
    }

    // Returns a QueryRecordService instance.
	public static QueryRecordService getQueryRecordService() 
    {
        if(serviceController.getQueryRecordService() == null) {
            serviceController.setQueryRecordService(serviceFactory.getQueryRecordService());
        }
        return serviceController.getQueryRecordService();
    }
    // Injects a QueryRecordService instance.
	public static void setQueryRecordService(QueryRecordService queryRecordService) 
    {
        serviceController.setQueryRecordService(queryRecordService);
    }

    // Returns a DummyEntityService instance.
	public static DummyEntityService getDummyEntityService() 
    {
        if(serviceController.getDummyEntityService() == null) {
            serviceController.setDummyEntityService(serviceFactory.getDummyEntityService());
        }
        return serviceController.getDummyEntityService();
    }
    // Injects a DummyEntityService instance.
	public static void setDummyEntityService(DummyEntityService dummyEntityService) 
    {
        serviceController.setDummyEntityService(dummyEntityService);
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(serviceController.getServiceInfoService() == null) {
            serviceController.setServiceInfoService(serviceFactory.getServiceInfoService());
        }
        return serviceController.getServiceInfoService();
    }
    // Injects a ServiceInfoService instance.
	public static void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        serviceController.setServiceInfoService(serviceInfoService);
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(serviceController.getFiveTenService() == null) {
            serviceController.setFiveTenService(serviceFactory.getFiveTenService());
        }
        return serviceController.getFiveTenService();
    }
    // Injects a FiveTenService instance.
	public static void setFiveTenService(FiveTenService fiveTenService) 
    {
        serviceController.setFiveTenService(fiveTenService);
    }

}
