package com.queryclient.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.FiveTen;
import com.queryclient.af.config.Config;

import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.FiveTenService;


// The primary purpose of FiveTenDummyService is to fake the service api, FiveTenService.
// It has no real implementation.
public class FiveTenDummyService implements FiveTenService
{
    private static final Logger log = Logger.getLogger(FiveTenDummyService.class.getName());

    public FiveTenDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // FiveTen related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFiveTen(): guid = " + guid);
        return null;
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFiveTen(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        log.fine("getFiveTens()");
        return null;
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return getAllFiveTens(null, null, null);
    }


    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFiveTens(ordering, offset, count, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFiveTens(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFiveTenKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFiveTenKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FiveTenDummyService.findFiveTens(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FiveTenDummyService.findFiveTenKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FiveTenDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException
    {
        log.finer("createFiveTen()");
        return null;
    }

    @Override
    public String createFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("createFiveTen()");
        return null;
    }

    @Override
    public FiveTen constructFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("constructFiveTen()");
        return null;
    }

    @Override
    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException
    {
        log.finer("updateFiveTen()");
        return null;
    }
        
    @Override
    public Boolean updateFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("updateFiveTen()");
        return null;
    }

    @Override
    public FiveTen refreshFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("refreshFiveTen()");
        return null;
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteFiveTen(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("deleteFiveTen()");
        return null;
    }

    // TBD
    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteFiveTen(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createFiveTens(List<FiveTen> fiveTens) throws BaseException
    {
        log.finer("createFiveTens()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateFiveTens(List<FiveTen> fiveTens) throws BaseException
    //{
    //}

}
