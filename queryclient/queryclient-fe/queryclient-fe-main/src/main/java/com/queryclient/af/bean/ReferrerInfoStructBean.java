package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.stub.ReferrerInfoStructStub;


// Wrapper class + bean combo.
public class ReferrerInfoStructBean implements ReferrerInfoStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ReferrerInfoStructBean.class.getName());

    // [1] With an embedded object.
    private ReferrerInfoStructStub stub = null;

    // [2] Or, without an embedded object.
    private String referer;
    private String userAgent;
    private String language;
    private String hostname;
    private String ipAddress;
    private String note;

    // Ctors.
    public ReferrerInfoStructBean()
    {
        //this((String) null);
    }
    public ReferrerInfoStructBean(String referer, String userAgent, String language, String hostname, String ipAddress, String note)
    {
        this.referer = referer;
        this.userAgent = userAgent;
        this.language = language;
        this.hostname = hostname;
        this.ipAddress = ipAddress;
        this.note = note;
    }
    public ReferrerInfoStructBean(ReferrerInfoStruct stub)
    {
        if(stub instanceof ReferrerInfoStructStub) {
            this.stub = (ReferrerInfoStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setReferer(stub.getReferer());   
            setUserAgent(stub.getUserAgent());   
            setLanguage(stub.getLanguage());   
            setHostname(stub.getHostname());   
            setIpAddress(stub.getIpAddress());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getReferer()
    {
        if(getStub() != null) {
            return getStub().getReferer();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referer;
        }
    }
    public void setReferer(String referer)
    {
        if(getStub() != null) {
            getStub().setReferer(referer);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.referer = referer;
        }
    }

    public String getUserAgent()
    {
        if(getStub() != null) {
            return getStub().getUserAgent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userAgent;
        }
    }
    public void setUserAgent(String userAgent)
    {
        if(getStub() != null) {
            getStub().setUserAgent(userAgent);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userAgent = userAgent;
        }
    }

    public String getLanguage()
    {
        if(getStub() != null) {
            return getStub().getLanguage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.language;
        }
    }
    public void setLanguage(String language)
    {
        if(getStub() != null) {
            getStub().setLanguage(language);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.language = language;
        }
    }

    public String getHostname()
    {
        if(getStub() != null) {
            return getStub().getHostname();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.hostname;
        }
    }
    public void setHostname(String hostname)
    {
        if(getStub() != null) {
            getStub().setHostname(hostname);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.hostname = hostname;
        }
    }

    public String getIpAddress()
    {
        if(getStub() != null) {
            return getStub().getIpAddress();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ipAddress;
        }
    }
    public void setIpAddress(String ipAddress)
    {
        if(getStub() != null) {
            getStub().setIpAddress(ipAddress);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ipAddress = ipAddress;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getReferer() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUserAgent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLanguage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHostname() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getIpAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public ReferrerInfoStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ReferrerInfoStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("referer = " + this.referer).append(";");
            sb.append("userAgent = " + this.userAgent).append(";");
            sb.append("language = " + this.language).append(";");
            sb.append("hostname = " + this.hostname).append(";");
            sb.append("ipAddress = " + this.ipAddress).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = referer == null ? 0 : referer.hashCode();
            _hash = 31 * _hash + delta;
            delta = userAgent == null ? 0 : userAgent.hashCode();
            _hash = 31 * _hash + delta;
            delta = language == null ? 0 : language.hashCode();
            _hash = 31 * _hash + delta;
            delta = hostname == null ? 0 : hostname.hashCode();
            _hash = 31 * _hash + delta;
            delta = ipAddress == null ? 0 : ipAddress.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
