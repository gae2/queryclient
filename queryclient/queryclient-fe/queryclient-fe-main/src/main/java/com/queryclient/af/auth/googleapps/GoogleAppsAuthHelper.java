package com.queryclient.af.auth.googleapps;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.config.Config;
import com.queryclient.af.auth.common.CommonAuthUtil;


public class GoogleAppsAuthHelper
{
    private static final Logger log = Logger.getLogger(GoogleAppsAuthHelper.class.getName());

    // "Lazy initialization"
    private String mDeaultAppsDomain = null;
    private String mLoginRealm = null;
    // private List<String[]> mDataAccessScopes = null;   // Scope == ["id", "url", "reason"]
    private String mConsumerKey = null;
    private String mConsumerSecret = null;
    private String mPageSetup = null;
    private String mPageManage = null;
    private String mPageSupport = null;
    private String mPageDeletionPolicy = null;
    private String mPageAddSeats = null;
    private String mPageNavLink = null;
    private String mPageLandingPage = null;
    

    private GoogleAppsAuthHelper() {}

    // Initialization-on-demand holder.
    private static final class GoogleAppsAuthHelperHolder
    {
        private static final GoogleAppsAuthHelper INSTANCE = new GoogleAppsAuthHelper();
    }

    // Singleton method
    public static GoogleAppsAuthHelper getInstance()
    {
        return GoogleAppsAuthHelperHolder.INSTANCE;
    }

    public String getDeaultAppsDomain()
    {
        if(mDeaultAppsDomain == null) {
            mDeaultAppsDomain = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_DEFAULT_APPSDOMAIN);
        }
        return mDeaultAppsDomain;
    }

    public String getLoginRealm()
    {
        if(mLoginRealm == null) {
            mLoginRealm = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_LOGINREALM);
        }
        return mLoginRealm;
    }

    public String getConsumerKey()
    {
        if(mConsumerKey == null) {
            mConsumerKey = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_CONSUMERKEY);
        }
        return mConsumerKey;
    }
    public String getConsumerSecret()
    {
        if(mConsumerSecret == null) {
            mConsumerSecret = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_CONSUMERSECRET);
        }
        return mConsumerSecret;
    }


    public String getSetupPageURL()
    {
        if(mPageSetup == null) {
            mPageSetup = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_APPPAGE_SETUP);
        }
        return mPageSetup;
    }

    public String getManagePageURL()
    {
        if(mPageManage == null) {
            mPageManage = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_APPPAGE_MANAGE);
        }
        return mPageManage;
    }

    public String getSupportPageURL()
    {
        if(mPageSupport == null) {
            mPageSupport = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_APPPAGE_SUPPORT);
        }
        return mPageSupport;
    }

    public String getDeletionPolicyPageURL()
    {
        if(mPageDeletionPolicy == null) {
            mPageDeletionPolicy = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_APPPAGE_DELETION_POLICY);
        }
        return mPageDeletionPolicy;
    }

    public String getAddSeatsPageURL()
    {
        if(mPageAddSeats == null) {
            mPageAddSeats = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_APPPAGE_ADD_SEATS);
        }
        return mPageAddSeats;
    }

    public String getAppPageNavLink()
    {
        if(mPageNavLink == null) {
            mPageNavLink = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_APPPAGE_NAVLINK);
        }
        return mPageNavLink;
    }

    public String getLandingPageURL()
    {
        if(mPageLandingPage == null) {
            mPageLandingPage = Config.getInstance().getString(GoogleAppsAuthUtil.CONFIG_KEY_APPPAGE_LANDING_PAGE);
        }
        return mPageLandingPage;
    }


}
