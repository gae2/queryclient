package com.queryclient.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;
import com.queryclient.ws.stub.UserAuthStateStub;
import com.queryclient.ws.stub.UserAuthStateListStub;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.UserAuthStateBean;
import com.queryclient.af.resource.UserAuthStateResource;
import com.queryclient.af.resource.util.GaeAppStructResourceUtil;
import com.queryclient.af.resource.util.ExternalUserIdStructResourceUtil;


// MockUserAuthStateResource is a decorator.
// It can be used as a base class to mock UserAuthStateResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/userAuthStates/")
public abstract class MockUserAuthStateResource implements UserAuthStateResource
{
    private static final Logger log = Logger.getLogger(MockUserAuthStateResource.class.getName());

    // MockUserAuthStateResource uses the decorator design pattern.
    private UserAuthStateResource decoratedResource;

    public MockUserAuthStateResource(UserAuthStateResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected UserAuthStateResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(UserAuthStateResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllUserAuthStates(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUserAuthStates(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllUserAuthStateKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUserAuthStateKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findUserAuthStatesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findUserAuthStatesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getUserAuthStateAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getUserAuthStateAsHtml(guid);
//    }

    @Override
    public Response getUserAuthState(String guid) throws BaseResourceException
    {
        return decoratedResource.getUserAuthState(guid);
    }

    @Override
    public Response getUserAuthStateAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getUserAuthStateAsJsonp(guid, callback);
    }

    @Override
    public Response getUserAuthState(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getUserAuthState(guid, field);
    }

    // TBD
    @Override
    public Response constructUserAuthState(UserAuthStateStub userAuthState) throws BaseResourceException
    {
        return decoratedResource.constructUserAuthState(userAuthState);
    }

    @Override
    public Response createUserAuthState(UserAuthStateStub userAuthState) throws BaseResourceException
    {
        return decoratedResource.createUserAuthState(userAuthState);
    }

//    @Override
//    public Response createUserAuthState(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createUserAuthState(formParams);
//    }

    // TBD
    @Override
    public Response refreshUserAuthState(String guid, UserAuthStateStub userAuthState) throws BaseResourceException
    {
        return decoratedResource.refreshUserAuthState(guid, userAuthState);
    }

    @Override
    public Response updateUserAuthState(String guid, UserAuthStateStub userAuthState) throws BaseResourceException
    {
        return decoratedResource.updateUserAuthState(guid, userAuthState);
    }

    @Override
    public Response updateUserAuthState(String guid, String managerApp, Long appAcl, String gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, String externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime)
    {
        return decoratedResource.updateUserAuthState(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime);
    }

//    @Override
//    public Response updateUserAuthState(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateUserAuthState(guid, formParams);
//    }

    @Override
    public Response deleteUserAuthState(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteUserAuthState(guid);
    }

    @Override
    public Response deleteUserAuthStates(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteUserAuthStates(filter, params, values);
    }


// TBD ....
    @Override
    public Response createUserAuthStates(UserAuthStateListStub userAuthStates) throws BaseResourceException
    {
        return decoratedResource.createUserAuthStates(userAuthStates);
    }


}
