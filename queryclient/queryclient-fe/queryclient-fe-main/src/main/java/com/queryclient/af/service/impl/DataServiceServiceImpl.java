package com.queryclient.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.DataServiceService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DataServiceServiceImpl implements DataServiceService
{
    private static final Logger log = Logger.getLogger(DataServiceServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "DataService-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("DataService:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public DataServiceServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // DataService related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DataService getDataService(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDataService(): guid = " + guid);

        DataServiceBean bean = null;
        if(getCache() != null) {
            bean = (DataServiceBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (DataServiceBean) getProxyFactory().getDataServiceServiceProxy().getDataService(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "DataServiceBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DataServiceBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getDataService(String guid, String field) throws BaseException
    {
        DataServiceBean bean = null;
        if(getCache() != null) {
            bean = (DataServiceBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (DataServiceBean) getProxyFactory().getDataServiceServiceProxy().getDataService(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "DataServiceBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DataServiceBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("name")) {
            return bean.getName();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("mode")) {
            return bean.getMode();
        } else if(field.equals("serviceUrl")) {
            return bean.getServiceUrl();
        } else if(field.equals("authRequired")) {
            return bean.isAuthRequired();
        } else if(field.equals("authCredential")) {
            return bean.getAuthCredential();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<DataService> getDataServices(List<String> guids) throws BaseException
    {
        log.fine("getDataServices()");

        // TBD: Is there a better way????
        List<DataService> dataServices = getProxyFactory().getDataServiceServiceProxy().getDataServices(guids);
        if(dataServices == null) {
            log.log(Level.WARNING, "Failed to retrieve DataServiceBean list.");
        }

        log.finer("END");
        return dataServices;
    }

    @Override
    public List<DataService> getAllDataServices() throws BaseException
    {
        return getAllDataServices(null, null, null);
    }


    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServices(ordering, offset, count, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDataServices(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DataService> dataServices = getProxyFactory().getDataServiceServiceProxy().getAllDataServices(ordering, offset, count, forwardCursor);
        if(dataServices == null) {
            log.log(Level.WARNING, "Failed to retrieve DataServiceBean list.");
        }

        log.finer("END");
        return dataServices;
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDataServiceKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getDataServiceServiceProxy().getAllDataServiceKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve DataServiceBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty DataServiceBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DataServiceServiceImpl.findDataServices(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DataService> dataServices = getProxyFactory().getDataServiceServiceProxy().findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataServices == null) {
            log.log(Level.WARNING, "Failed to find dataServices for the given criterion.");
        }

        log.finer("END");
        return dataServices;
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DataServiceServiceImpl.findDataServiceKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getDataServiceServiceProxy().findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find DataService keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty DataService key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DataServiceServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getDataServiceServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDataService(String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ConsumerKeySecretPairBean authCredentialBean = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialBean = (ConsumerKeySecretPairBean) authCredential;
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialBean = new ConsumerKeySecretPairBean(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialBean = null;   // ????
        }
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        DataServiceBean bean = new DataServiceBean(null, user, name, description, type, mode, serviceUrl, authRequired, authCredentialBean, referrerInfoBean, status);
        return createDataService(bean);
    }

    @Override
    public String createDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //DataService bean = constructDataService(dataService);
        //return bean.getGuid();

        // Param dataService cannot be null.....
        if(dataService == null) {
            log.log(Level.INFO, "Param dataService is null!");
            throw new BadRequestException("Param dataService object is null!");
        }
        DataServiceBean bean = null;
        if(dataService instanceof DataServiceBean) {
            bean = (DataServiceBean) dataService;
        } else if(dataService instanceof DataService) {
            // bean = new DataServiceBean(null, dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), (ConsumerKeySecretPairBean) dataService.getAuthCredential(), (ReferrerInfoStructBean) dataService.getReferrerInfo(), dataService.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new DataServiceBean(dataService.getGuid(), dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), (ConsumerKeySecretPairBean) dataService.getAuthCredential(), (ReferrerInfoStructBean) dataService.getReferrerInfo(), dataService.getStatus());
        } else {
            log.log(Level.WARNING, "createDataService(): Arg dataService is of an unknown type.");
            //bean = new DataServiceBean();
            bean = new DataServiceBean(dataService.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getDataServiceServiceProxy().createDataService(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public DataService constructDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");

        // Param dataService cannot be null.....
        if(dataService == null) {
            log.log(Level.INFO, "Param dataService is null!");
            throw new BadRequestException("Param dataService object is null!");
        }
        DataServiceBean bean = null;
        if(dataService instanceof DataServiceBean) {
            bean = (DataServiceBean) dataService;
        } else if(dataService instanceof DataService) {
            // bean = new DataServiceBean(null, dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), (ConsumerKeySecretPairBean) dataService.getAuthCredential(), (ReferrerInfoStructBean) dataService.getReferrerInfo(), dataService.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new DataServiceBean(dataService.getGuid(), dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), (ConsumerKeySecretPairBean) dataService.getAuthCredential(), (ReferrerInfoStructBean) dataService.getReferrerInfo(), dataService.getStatus());
        } else {
            log.log(Level.WARNING, "createDataService(): Arg dataService is of an unknown type.");
            //bean = new DataServiceBean();
            bean = new DataServiceBean(dataService.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getDataServiceServiceProxy().createDataService(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ConsumerKeySecretPairBean authCredentialBean = null;
        if(authCredential instanceof ConsumerKeySecretPairBean) {
            authCredentialBean = (ConsumerKeySecretPairBean) authCredential;
        } else if(authCredential instanceof ConsumerKeySecretPair) {
            authCredentialBean = new ConsumerKeySecretPairBean(authCredential.getConsumerKey(), authCredential.getConsumerSecret());
        } else {
            authCredentialBean = null;   // ????
        }
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        DataServiceBean bean = new DataServiceBean(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredentialBean, referrerInfoBean, status);
        return updateDataService(bean);
    }
        
    @Override
    public Boolean updateDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //DataService bean = refreshDataService(dataService);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param dataService cannot be null.....
        if(dataService == null || dataService.getGuid() == null) {
            log.log(Level.INFO, "Param dataService or its guid is null!");
            throw new BadRequestException("Param dataService object or its guid is null!");
        }
        DataServiceBean bean = null;
        if(dataService instanceof DataServiceBean) {
            bean = (DataServiceBean) dataService;
        } else {  // if(dataService instanceof DataService)
            bean = new DataServiceBean(dataService.getGuid(), dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), (ConsumerKeySecretPairBean) dataService.getAuthCredential(), (ReferrerInfoStructBean) dataService.getReferrerInfo(), dataService.getStatus());
        }
        Boolean suc = getProxyFactory().getDataServiceServiceProxy().updateDataService(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public DataService refreshDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");

        // Param dataService cannot be null.....
        if(dataService == null || dataService.getGuid() == null) {
            log.log(Level.INFO, "Param dataService or its guid is null!");
            throw new BadRequestException("Param dataService object or its guid is null!");
        }
        DataServiceBean bean = null;
        if(dataService instanceof DataServiceBean) {
            bean = (DataServiceBean) dataService;
        } else {  // if(dataService instanceof DataService)
            bean = new DataServiceBean(dataService.getGuid(), dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), (ConsumerKeySecretPairBean) dataService.getAuthCredential(), (ReferrerInfoStructBean) dataService.getReferrerInfo(), dataService.getStatus());
        }
        Boolean suc = getProxyFactory().getDataServiceServiceProxy().updateDataService(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getDataServiceServiceProxy().deleteDataService(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            DataService dataService = null;
            try {
                dataService = getProxyFactory().getDataServiceServiceProxy().getDataService(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch dataService with a key, " + guid);
                return false;
            }
            if(dataService != null) {
                String beanGuid = dataService.getGuid();
                Boolean suc1 = getProxyFactory().getDataServiceServiceProxy().deleteDataService(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("dataService with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteDataService(DataService dataService) throws BaseException
    {
        log.finer("BEGIN");

        // Param dataService cannot be null.....
        if(dataService == null || dataService.getGuid() == null) {
            log.log(Level.INFO, "Param dataService or its guid is null!");
            throw new BadRequestException("Param dataService object or its guid is null!");
        }
        DataServiceBean bean = null;
        if(dataService instanceof DataServiceBean) {
            bean = (DataServiceBean) dataService;
        } else {  // if(dataService instanceof DataService)
            // ????
            log.warning("dataService is not an instance of DataServiceBean.");
            bean = new DataServiceBean(dataService.getGuid(), dataService.getUser(), dataService.getName(), dataService.getDescription(), dataService.getType(), dataService.getMode(), dataService.getServiceUrl(), dataService.isAuthRequired(), (ConsumerKeySecretPairBean) dataService.getAuthCredential(), (ReferrerInfoStructBean) dataService.getReferrerInfo(), dataService.getStatus());
        }
        Boolean suc = getProxyFactory().getDataServiceServiceProxy().deleteDataService(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDataServices(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getDataServiceServiceProxy().deleteDataServices(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDataServices(List<DataService> dataServices) throws BaseException
    {
        log.finer("BEGIN");

        if(dataServices == null) {
            log.log(Level.WARNING, "createDataServices() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = dataServices.size();
        if(size == 0) {
            log.log(Level.WARNING, "createDataServices() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(DataService dataService : dataServices) {
            String guid = createDataService(dataService);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createDataServices() failed for at least one dataService. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateDataServices(List<DataService> dataServices) throws BaseException
    //{
    //}

}
