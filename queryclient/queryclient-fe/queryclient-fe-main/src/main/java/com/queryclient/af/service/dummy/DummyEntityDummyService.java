package com.queryclient.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.DummyEntity;
import com.queryclient.af.config.Config;

import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.DummyEntityService;


// The primary purpose of DummyEntityDummyService is to fake the service api, DummyEntityService.
// It has no real implementation.
public class DummyEntityDummyService implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(DummyEntityDummyService.class.getName());

    public DummyEntityDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // DummyEntity related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DummyEntity getDummyEntity(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDummyEntity(): guid = " + guid);
        return null;
    }

    @Override
    public Object getDummyEntity(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDummyEntity(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException
    {
        log.fine("getDummyEntities()");
        return null;
    }

    @Override
    public List<DummyEntity> getAllDummyEntities() throws BaseException
    {
        return getAllDummyEntities(null, null, null);
    }


    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDummyEntities(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDummyEntityKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityDummyService.findDummyEntities(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityDummyService.findDummyEntityKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        log.finer("createDummyEntity()");
        return null;
    }

    @Override
    public String createDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("createDummyEntity()");
        return null;
    }

    @Override
    public DummyEntity constructDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("constructDummyEntity()");
        return null;
    }

    @Override
    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        log.finer("updateDummyEntity()");
        return null;
    }
        
    @Override
    public Boolean updateDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("updateDummyEntity()");
        return null;
    }

    @Override
    public DummyEntity refreshDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("refreshDummyEntity()");
        return null;
    }

    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteDummyEntity(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("deleteDummyEntity()");
        return null;
    }

    // TBD
    @Override
    public Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteDummyEntity(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    {
        log.finer("createDummyEntities()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    //{
    //}

}
