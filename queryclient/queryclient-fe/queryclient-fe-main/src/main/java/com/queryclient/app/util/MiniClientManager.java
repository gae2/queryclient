package com.queryclient.app.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;

import com.queryclient.ws.BaseException;


// TBD:
// ....
public final class MiniClientManager
{
    private static final Logger log = Logger.getLogger(MiniClientManager.class.getName());
    
    // temporary
    private Map<String, ApiServiceClient> clientMapCache = null;

    
    private MiniClientManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static final class MiniClientManagerHolder
    {
        private static final MiniClientManager INSTANCE = new MiniClientManager();
    }
    // Singleton method
    public static MiniClientManager getInstance()
    {
        return MiniClientManagerHolder.INSTANCE;
    }

    
    private void init()
    {
        
        // temporary
        clientMapCache = new HashMap<String, ApiServiceClient>();

        // ...
        
    }


    // TBD: ...
    public ApiServiceClient getApiServiceClient(String service) throws BaseException
    {
        ApiServiceClient client = null;

//        OAuthConsumer consumer = null;
//        if(clientMapCache.containsKey(service)) {
//            consumer = clientMapCache.get(service);
//        } else {
//            // temporary
//            ConsumerKeySecretPair pair = TargetServiceManager.getInstance().getKeySecretPair(service);
//            if(pair != null) {
//                consumer = new DefaultOAuthConsumer(pair.getConsumerKey(), pair.getConsumerSecret());
//                clientMapCache.put(service, consumer);
//            } else {
//                log.log(Level.WARNING, "Failed to create an OAuthConsumer for service = " + service);
//                throw new BaseException("Failed to create an OAuthConsumer for service = " + service);
//            }
//        }
//        
//        return consumer;
        
        return client;
    }

    
}
