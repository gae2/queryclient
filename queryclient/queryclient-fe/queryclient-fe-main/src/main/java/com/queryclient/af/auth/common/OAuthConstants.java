package com.queryclient.af.auth.common;

import java.util.logging.Logger;
import java.util.logging.Level;


public final class OAuthConstants
{
    // OAuth? OAuth2?
    public static final String PARAM_OAUTH_CALLBACK = "oauth_callback";
    public static final String PARAM_OAUTH_CONSUMER_KEY = "oauth_consumer_key";
    public static final String PARAM_OAUTH_NONCE = "oauth_nonce";
    public static final String PARAM_OAUTH_SIGNATURE = "oauth_signature";
    public static final String PARAM_OAUTH_SIGNATURE_METHOD = "oauth_signature_method";
    public static final String PARAM_OAUTH_TIMESTAMP = "oauth_timestamp";
    public static final String PARAM_OAUTH_VERSION = "oauth_version";
    public static final String PARAM_OAUTH_REQUEST_TOKEN = "oauth_token";
    public static final String PARAM_OAUTH_TOKEN_VERIFIER = "oauth_verifier";
    // ...
 
    
    private OAuthConstants() {}

}
