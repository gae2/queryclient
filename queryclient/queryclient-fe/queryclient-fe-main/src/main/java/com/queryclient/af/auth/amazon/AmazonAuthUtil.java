package com.queryclient.af.auth.amazon;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.auth.common.OAuthConstants;


public class AmazonAuthUtil
{
    private static final Logger log = Logger.getLogger(AmazonAuthUtil.class.getName());

    // Constants
    private static final String CALLBACK_URL_OOB = "oob";    // Out of band.
    // ...

    public static final String CONFIG_KEY_CONSUMERKEY = "queryclientapp.amazon.consumerkey";
    public static final String CONFIG_KEY_CONSUMERSECRET = "queryclientapp.amazon.consumersecret";
    public static final String CONFIG_KEY_DEFAULT_ACCESSTOKEN = "queryclientapp.amazon.default.accesstoken";
    public static final String CONFIG_KEY_DEFAULT_ACCESSTOKENSECRET = "queryclientapp.amazon.default.accesstokensecret";
    public static final String CONFIG_KEY_OAUTH_REDIRECTURLPATH = "queryclientapp.amazon.oauth.redirecturlpath";
    // public static final String CONFIG_KEY_REQUESTTOKEN_REDIRECTURLPATH = "queryclientapp.amazon.requesttoken.redirecturlpath";
    // public static final String CONFIG_KEY_ACCESSTOKEN_REDIRECTURLPATH = "queryclientapp.amazon.accesstoken.redirecturlpath"; 
    // ...
    
    public static final String PARAM_CALLBACK = OAuthConstants.PARAM_OAUTH_CALLBACK;
    public static final String PARAM_CONSUMER_KEY = OAuthConstants.PARAM_OAUTH_CONSUMER_KEY;
    public static final String PARAM_NONCE = OAuthConstants.PARAM_OAUTH_NONCE;
    public static final String PARAM_SIGNATURE = OAuthConstants.PARAM_OAUTH_SIGNATURE;
    public static final String PARAM_SIGNATURE_METHOD = OAuthConstants.PARAM_OAUTH_SIGNATURE_METHOD;
    public static final String PARAM_TIMESTAMP = OAuthConstants.PARAM_OAUTH_TIMESTAMP;
    public static final String PARAM_VERSION = OAuthConstants.PARAM_OAUTH_VERSION;
    public static final String PARAM_REQUEST_TOKEN = OAuthConstants.PARAM_OAUTH_REQUEST_TOKEN;
    public static final String PARAM_TOKEN_VERIFIER = OAuthConstants.PARAM_OAUTH_TOKEN_VERIFIER;
    // ...
    
    public static final String BASE_URL_AUTHORIZE = "https://api.amazon.com/oauth/authorize";         // ???
    public static final String BASE_URL_AUTHENTICATE = "https://api.amazon.com/oauth/authenticate";   // ???
    public static final String BASE_URL_REQUEST_TOKEN_FETCH = "https://api.amazon.com/oauth/request_token";
    public static final String BASE_URL_ACCESS_TOKEN_EXCHANGE = "https://api.amazon.com/oauth/access_token";
    // ...

    public static final String SESSION_ATTR_REQUESTTOKEN = "com.queryclient.af.auth.amazon.oauth.requesttoken";
    // ...
    
    private AmazonAuthUtil() {}

    
    // TBD:
    // ...
    
}
