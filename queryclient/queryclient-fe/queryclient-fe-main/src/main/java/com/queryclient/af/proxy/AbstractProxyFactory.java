package com.queryclient.af.proxy;

public abstract class AbstractProxyFactory
{
    public abstract ApiConsumerServiceProxy getApiConsumerServiceProxy();
    public abstract UserServiceProxy getUserServiceProxy();
    public abstract UserPasswordServiceProxy getUserPasswordServiceProxy();
    public abstract ExternalUserAuthServiceProxy getExternalUserAuthServiceProxy();
    public abstract UserAuthStateServiceProxy getUserAuthStateServiceProxy();
    public abstract DataServiceServiceProxy getDataServiceServiceProxy();
    public abstract ServiceEndpointServiceProxy getServiceEndpointServiceProxy();
    public abstract QuerySessionServiceProxy getQuerySessionServiceProxy();
    public abstract QueryRecordServiceProxy getQueryRecordServiceProxy();
    public abstract DummyEntityServiceProxy getDummyEntityServiceProxy();
    public abstract ServiceInfoServiceProxy getServiceInfoServiceProxy();
    public abstract FiveTenServiceProxy getFiveTenServiceProxy();
}
