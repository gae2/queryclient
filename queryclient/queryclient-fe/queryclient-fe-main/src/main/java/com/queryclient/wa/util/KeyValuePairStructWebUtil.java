package com.queryclient.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.KeyValuePairStruct;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.fe.bean.KeyValuePairStructJsBean;


public class KeyValuePairStructWebUtil
{
    private static final Logger log = Logger.getLogger(KeyValuePairStructWebUtil.class.getName());

    // Static methods only.
    private KeyValuePairStructWebUtil() {}
    

    public static KeyValuePairStructJsBean convertKeyValuePairStructToJsBean(KeyValuePairStruct keyValuePairStruct)
    {
        KeyValuePairStructJsBean jsBean = null;
        if(keyValuePairStruct != null) {
            jsBean = new KeyValuePairStructJsBean();
            jsBean.setUuid(keyValuePairStruct.getUuid());
            jsBean.setKey(keyValuePairStruct.getKey());
            jsBean.setValue(keyValuePairStruct.getValue());
            jsBean.setNote(keyValuePairStruct.getNote());
        }
        return jsBean;
    }

    public static KeyValuePairStruct convertKeyValuePairStructJsBeanToBean(KeyValuePairStructJsBean jsBean)
    {
        KeyValuePairStructBean keyValuePairStruct = null;
        if(jsBean != null) {
            keyValuePairStruct = new KeyValuePairStructBean();
            keyValuePairStruct.setUuid(jsBean.getUuid());
            keyValuePairStruct.setKey(jsBean.getKey());
            keyValuePairStruct.setValue(jsBean.getValue());
            keyValuePairStruct.setNote(jsBean.getNote());
        }
        return keyValuePairStruct;
    }

}
