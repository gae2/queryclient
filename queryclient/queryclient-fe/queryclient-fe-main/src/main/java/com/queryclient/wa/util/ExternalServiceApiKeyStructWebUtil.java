package com.queryclient.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.ExternalServiceApiKeyStruct;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.fe.bean.ExternalServiceApiKeyStructJsBean;


public class ExternalServiceApiKeyStructWebUtil
{
    private static final Logger log = Logger.getLogger(ExternalServiceApiKeyStructWebUtil.class.getName());

    // Static methods only.
    private ExternalServiceApiKeyStructWebUtil() {}
    

    public static ExternalServiceApiKeyStructJsBean convertExternalServiceApiKeyStructToJsBean(ExternalServiceApiKeyStruct externalServiceApiKeyStruct)
    {
        ExternalServiceApiKeyStructJsBean jsBean = null;
        if(externalServiceApiKeyStruct != null) {
            jsBean = new ExternalServiceApiKeyStructJsBean();
            jsBean.setUuid(externalServiceApiKeyStruct.getUuid());
            jsBean.setService(externalServiceApiKeyStruct.getService());
            jsBean.setKey(externalServiceApiKeyStruct.getKey());
            jsBean.setSecret(externalServiceApiKeyStruct.getSecret());
            jsBean.setNote(externalServiceApiKeyStruct.getNote());
        }
        return jsBean;
    }

    public static ExternalServiceApiKeyStruct convertExternalServiceApiKeyStructJsBeanToBean(ExternalServiceApiKeyStructJsBean jsBean)
    {
        ExternalServiceApiKeyStructBean externalServiceApiKeyStruct = null;
        if(jsBean != null) {
            externalServiceApiKeyStruct = new ExternalServiceApiKeyStructBean();
            externalServiceApiKeyStruct.setUuid(jsBean.getUuid());
            externalServiceApiKeyStruct.setService(jsBean.getService());
            externalServiceApiKeyStruct.setKey(jsBean.getKey());
            externalServiceApiKeyStruct.setSecret(jsBean.getSecret());
            externalServiceApiKeyStruct.setNote(jsBean.getNote());
        }
        return externalServiceApiKeyStruct;
    }

}
