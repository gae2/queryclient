package com.queryclient.af.cert.proxy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.cert.PublicCertificateInfo;
import com.queryclient.ws.cert.stub.PublicCertificateInfoStub;
import com.queryclient.ws.cert.stub.PublicCertificateInfoListStub;
import com.queryclient.af.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.af.cert.bean.PublicCertificateInfoBean;
import com.queryclient.af.cert.proxy.PublicCertificateInfoServiceProxy;

import com.queryclient.af.config.Config;


// Note on retries, upon arbitrary app engine urlfetch timeout (5 secs):
// In our current implementation, create (POST) and update (PUT) have essentially the same semantics.
// If an object with the client-supplied guid does not exist in the data store, it will be created.
// If an object with the given guid already exists, it will be replaced with the supplied object.
// The only difference is, the update operation (PUT) will modify the modifiedTime field.
// (Note: Ccreate does not require guid (pk) although we will generally try to supply guid from the client side.)
// Therefore, retries are relatively safe for POST and PUT.
// GET is by definition idempotent, and hence retry is safe.
// DELETE, on the other hand, will likely cause an error if the same request is repeated.
// The consequence of this is that, a user may be told there was an error deleting an object
// when in fact the object was already deleted during the retry sequence.
public class RemotePublicCertificateInfoServiceProxy extends BaseRemoteServiceProxy implements PublicCertificateInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(RemotePublicCertificateInfoServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_PUBLICCERTIFICATEINFO = "publicCertificateInfos";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public RemotePublicCertificateInfoServiceProxy()
    {
        initCache();
    }


    protected WebResource getPublicCertificateInfoWebResource()
    {
        return getWebResource(RESOURCE_PUBLICCERTIFICATEINFO);
    }
    protected WebResource getPublicCertificateInfoWebResource(String path)
    {
        return getWebResource(RESOURCE_PUBLICCERTIFICATEINFO, path);
    }
    protected WebResource getPublicCertificateInfoWebResourceByGuid(String guid)
    {
        return getPublicCertificateInfoWebResource(guid);
    }


    @Override
    public PublicCertificateInfo getPublicCertificateInfo(String guid) throws BaseException
    {
        PublicCertificateInfo bean = null;

        String key = getResourcePath(RESOURCE_PUBLICCERTIFICATEINFO, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getPublicCertificateInfoWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
 	            clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                PublicCertificateInfoStub stub = clientResponse.getEntity(PublicCertificateInfoStub.class);
                bean = MarshalHelper.convertPublicCertificateInfoToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "PublicCertificateInfo bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = clientResponse.getEntity(ErrorStub.class);
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Object getPublicCertificateInfo(String guid, String field) throws BaseException
    {
        PublicCertificateInfo bean = getPublicCertificateInfo(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appId")) {
            return bean.getAppId();
        } else if(field.equals("appUrl")) {
            return bean.getAppUrl();
        } else if(field.equals("certName")) {
            return bean.getCertName();
        } else if(field.equals("certInPemFormat")) {
            return bean.getCertInPemFormat();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<PublicCertificateInfo> getAllPublicCertificateInfos() throws BaseException
    {
        return getAllPublicCertificateInfos(null, null, null);
    }

    @Override
    public List<PublicCertificateInfo> getAllPublicCertificateInfos(String ordering, Long offset, Integer count) throws BaseException
    {
    	List<PublicCertificateInfo> list = null;

     	WebResource webResource = getPublicCertificateInfoWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
    	
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                PublicCertificateInfoListStub stub = clientResponse.getEntity(PublicCertificateInfoListStub.class);
                list = MarshalHelper.convertPublicCertificateInfoListStubToBeanList(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "PublicCertificateInfo list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = clientResponse.getEntity(ErrorStub.class);
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<PublicCertificateInfo> findPublicCertificateInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findPublicCertificateInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<PublicCertificateInfo> findPublicCertificateInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
    	List<PublicCertificateInfo> list = null;
    	
//        ClientResponse clientResponse = getPublicCertificateInfoWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getPublicCertificateInfoWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                PublicCertificateInfoListStub stub = clientResponse.getEntity(PublicCertificateInfoListStub.class);
                list = MarshalHelper.convertPublicCertificateInfoListStubToBeanList(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "PublicCertificateInfo list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = clientResponse.getEntity(ErrorStub.class);
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getPublicCertificateInfoWebResource(RESOURCE_PATH_COUNT);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                // Order of the mime type ???
                //clientResponse = webResource.accept(MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML).get(ClientResponse.class);
                clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "PublicCertificateInfo count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = clientResponse.getEntity(ErrorStub.class); // ???
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }

    @Override
    public String createPublicCertificateInfo(String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime) throws BaseException
    {
        PublicCertificateInfoBean bean = new PublicCertificateInfoBean(null, appId, appUrl, certName, certInPemFormat, note, status, expirationTime);
        return createPublicCertificateInfo(bean);        
    }

    @Override
    public String createPublicCertificateInfo(PublicCertificateInfo bean) throws BaseException
    {
        String guid = null;
        PublicCertificateInfoStub stub = MarshalHelper.convertPublicCertificateInfoToStub(bean);
        WebResource webResource = getPublicCertificateInfoWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.type(getInputMediaType()).post(ClientResponse.class, stub);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New PublicCertificateInfo guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New PublicCertificateInfo resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = clientResponse.getEntity(ErrorStub.class);
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return guid;
    }

    @Override
    public Boolean updatePublicCertificateInfo(String guid, String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "PublicCertificateInfo guid is invalid.");
        	throw new BaseException("PublicCertificateInfo guid is invalid.");
        }
        PublicCertificateInfoBean bean = new PublicCertificateInfoBean(guid, appId, appUrl, certName, certInPemFormat, note, status, expirationTime);
        return updatePublicCertificateInfo(bean);        
    }

    @Override
    public Boolean updatePublicCertificateInfo(PublicCertificateInfo bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "PublicCertificateInfo object is invalid.");
        	throw new BaseException("PublicCertificateInfo object is invalid.");
        }
        PublicCertificateInfoStub stub = MarshalHelper.convertPublicCertificateInfoToStub(bean);

        WebResource webResource = getPublicCertificateInfoWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);
        
        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully updated the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = clientResponse.getEntity(ErrorStub.class);
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public Boolean deletePublicCertificateInfo(String guid) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "PublicCertificateInfo guid is invalid.");
        	throw new BaseException("PublicCertificateInfo guid is invalid.");
        }

        WebResource webResource = getPublicCertificateInfoWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.delete(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: guid = " + guid);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: guid = " + guid);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully deleted the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = clientResponse.getEntity(ErrorStub.class);
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public Boolean deletePublicCertificateInfo(PublicCertificateInfo bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deletePublicCertificateInfo(guid);
    }

    @Override
    public Long deletePublicCertificateInfos(String filter, String params, List<String> values) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getPublicCertificateInfoWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exceptionif the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                // Order of the mime types ???
                clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: delete-filter = " + filter);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: delete-filter = " + filter);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Deleted PublicCertificateInfos: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = clientResponse.getEntity(ErrorStub.class); // ???
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        return count;
    }

}
