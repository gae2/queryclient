package com.queryclient.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.af.config.Config;

import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.ExternalUserAuthService;


// The primary purpose of ExternalUserAuthDummyService is to fake the service api, ExternalUserAuthService.
// It has no real implementation.
public class ExternalUserAuthDummyService implements ExternalUserAuthService
{
    private static final Logger log = Logger.getLogger(ExternalUserAuthDummyService.class.getName());

    public ExternalUserAuthDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // ExternalUserAuth related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ExternalUserAuth getExternalUserAuth(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getExternalUserAuth(): guid = " + guid);
        return null;
    }

    @Override
    public Object getExternalUserAuth(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getExternalUserAuth(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<ExternalUserAuth> getExternalUserAuths(List<String> guids) throws BaseException
    {
        log.fine("getExternalUserAuths()");
        return null;
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths() throws BaseException
    {
        return getAllExternalUserAuths(null, null, null);
    }


    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllExternalUserAuths(ordering, offset, count, null);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllExternalUserAuths(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllExternalUserAuthKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllExternalUserAuthKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findExternalUserAuths(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ExternalUserAuthDummyService.findExternalUserAuths(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ExternalUserAuthDummyService.findExternalUserAuthKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ExternalUserAuthDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createExternalUserAuth(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        log.finer("createExternalUserAuth()");
        return null;
    }

    @Override
    public String createExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("createExternalUserAuth()");
        return null;
    }

    @Override
    public ExternalUserAuth constructExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("constructExternalUserAuth()");
        return null;
    }

    @Override
    public Boolean updateExternalUserAuth(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        log.finer("updateExternalUserAuth()");
        return null;
    }
        
    @Override
    public Boolean updateExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("updateExternalUserAuth()");
        return null;
    }

    @Override
    public ExternalUserAuth refreshExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("refreshExternalUserAuth()");
        return null;
    }

    @Override
    public Boolean deleteExternalUserAuth(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteExternalUserAuth(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("deleteExternalUserAuth()");
        return null;
    }

    // TBD
    @Override
    public Long deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteExternalUserAuth(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createExternalUserAuths(List<ExternalUserAuth> externalUserAuths) throws BaseException
    {
        log.finer("createExternalUserAuths()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateExternalUserAuths(List<ExternalUserAuth> externalUserAuths) throws BaseException
    //{
    //}

}
