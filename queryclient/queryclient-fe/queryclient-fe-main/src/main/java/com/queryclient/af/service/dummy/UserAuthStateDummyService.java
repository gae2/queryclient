package com.queryclient.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;
import com.queryclient.af.config.Config;

import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.UserAuthStateService;


// The primary purpose of UserAuthStateDummyService is to fake the service api, UserAuthStateService.
// It has no real implementation.
public class UserAuthStateDummyService implements UserAuthStateService
{
    private static final Logger log = Logger.getLogger(UserAuthStateDummyService.class.getName());

    public UserAuthStateDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // UserAuthState related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserAuthState getUserAuthState(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserAuthState(): guid = " + guid);
        return null;
    }

    @Override
    public Object getUserAuthState(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserAuthState(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<UserAuthState> getUserAuthStates(List<String> guids) throws BaseException
    {
        log.fine("getUserAuthStates()");
        return null;
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates() throws BaseException
    {
        return getAllUserAuthStates(null, null, null);
    }


    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserAuthStates(ordering, offset, count, null);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserAuthStates(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserAuthStateKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserAuthStateKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserAuthStates(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserAuthStateDummyService.findUserAuthStates(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserAuthStateDummyService.findUserAuthStateKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserAuthStateDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createUserAuthState(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        log.finer("createUserAuthState()");
        return null;
    }

    @Override
    public String createUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("createUserAuthState()");
        return null;
    }

    @Override
    public UserAuthState constructUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("constructUserAuthState()");
        return null;
    }

    @Override
    public Boolean updateUserAuthState(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        log.finer("updateUserAuthState()");
        return null;
    }
        
    @Override
    public Boolean updateUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("updateUserAuthState()");
        return null;
    }

    @Override
    public UserAuthState refreshUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("refreshUserAuthState()");
        return null;
    }

    @Override
    public Boolean deleteUserAuthState(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteUserAuthState(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("deleteUserAuthState()");
        return null;
    }

    // TBD
    @Override
    public Long deleteUserAuthStates(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteUserAuthState(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUserAuthStates(List<UserAuthState> userAuthStates) throws BaseException
    {
        log.finer("createUserAuthStates()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateUserAuthStates(List<UserAuthState> userAuthStates) throws BaseException
    //{
    //}

}
