package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.User;
// import com.queryclient.ws.bean.UserBean;
import com.queryclient.ws.service.UserService;
import com.queryclient.af.bean.UserBean;
import com.queryclient.af.proxy.UserServiceProxy;
import com.queryclient.af.proxy.util.GaeAppStructProxyUtil;
import com.queryclient.af.proxy.util.GaeUserStructProxyUtil;


public class LocalUserServiceProxy extends BaseLocalServiceProxy implements UserServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserServiceProxy.class.getName());

    public LocalUserServiceProxy()
    {
    }

    @Override
    public User getUser(String guid) throws BaseException
    {
        User serverBean = getUserService().getUser(guid);
        User appBean = convertServerUserBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        return getUserService().getUser(guid, field);       
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        List<User> serverBeanList = getUserService().getUsers(guids);
        List<User> appBeanList = convertServerUserBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUserService().getAllUsers(ordering, offset, count);
        return getAllUsers(ordering, offset, count, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<User> serverBeanList = getUserService().getAllUsers(ordering, offset, count, forwardCursor);
        List<User> appBeanList = convertServerUserBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUserService().getAllUserKeys(ordering, offset, count);
        return getAllUserKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserService().getAllUserKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUserService().findUsers(filter, ordering, params, values, grouping, unique, offset, count);
        return findUsers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<User> serverBeanList = getUserService().findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<User> appBeanList = convertServerUserBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUserService().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserService().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String address, String location, String ipAddress, String referer, Boolean obsolete, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return getUserService().createUser(managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, address, location, ipAddress, referer, obsolete, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        com.queryclient.ws.bean.UserBean serverBean =  convertAppUserBeanToServerBean(user);
        return getUserService().createUser(serverBean);
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String address, String location, String ipAddress, String referer, Boolean obsolete, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return getUserService().updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, address, location, ipAddress, referer, obsolete, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        com.queryclient.ws.bean.UserBean serverBean =  convertAppUserBeanToServerBean(user);
        return getUserService().updateUser(serverBean);
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        return getUserService().deleteUser(guid);
    }

    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        com.queryclient.ws.bean.UserBean serverBean =  convertAppUserBeanToServerBean(user);
        return getUserService().deleteUser(serverBean);
    }

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        return getUserService().deleteUsers(filter, params, values);
    }




    public static UserBean convertServerUserBeanToAppBean(User serverBean)
    {
        UserBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new UserBean();
            bean.setGuid(serverBean.getGuid());
            bean.setManagerApp(serverBean.getManagerApp());
            bean.setAppAcl(serverBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertServerGaeAppStructBeanToAppBean(serverBean.getGaeApp()));
            bean.setAeryId(serverBean.getAeryId());
            bean.setSessionId(serverBean.getSessionId());
            bean.setUsername(serverBean.getUsername());
            bean.setNickname(serverBean.getNickname());
            bean.setAvatar(serverBean.getAvatar());
            bean.setEmail(serverBean.getEmail());
            bean.setOpenId(serverBean.getOpenId());
            bean.setGaeUser(GaeUserStructProxyUtil.convertServerGaeUserStructBeanToAppBean(serverBean.getGaeUser()));
            bean.setTimeZone(serverBean.getTimeZone());
            bean.setAddress(serverBean.getAddress());
            bean.setLocation(serverBean.getLocation());
            bean.setIpAddress(serverBean.getIpAddress());
            bean.setReferer(serverBean.getReferer());
            bean.setObsolete(serverBean.isObsolete());
            bean.setStatus(serverBean.getStatus());
            bean.setEmailVerifiedTime(serverBean.getEmailVerifiedTime());
            bean.setOpenIdVerifiedTime(serverBean.getOpenIdVerifiedTime());
            bean.setAuthenticatedTime(serverBean.getAuthenticatedTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<User> convertServerUserBeanListToAppBeanList(List<User> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<User> beanList = new ArrayList<User>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(User sb : serverBeanList) {
                UserBean bean = convertServerUserBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.UserBean convertAppUserBeanToServerBean(User appBean)
    {
        com.queryclient.ws.bean.UserBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.UserBean();
            bean.setGuid(appBean.getGuid());
            bean.setManagerApp(appBean.getManagerApp());
            bean.setAppAcl(appBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertAppGaeAppStructBeanToServerBean(appBean.getGaeApp()));
            bean.setAeryId(appBean.getAeryId());
            bean.setSessionId(appBean.getSessionId());
            bean.setUsername(appBean.getUsername());
            bean.setNickname(appBean.getNickname());
            bean.setAvatar(appBean.getAvatar());
            bean.setEmail(appBean.getEmail());
            bean.setOpenId(appBean.getOpenId());
            bean.setGaeUser(GaeUserStructProxyUtil.convertAppGaeUserStructBeanToServerBean(appBean.getGaeUser()));
            bean.setTimeZone(appBean.getTimeZone());
            bean.setAddress(appBean.getAddress());
            bean.setLocation(appBean.getLocation());
            bean.setIpAddress(appBean.getIpAddress());
            bean.setReferer(appBean.getReferer());
            bean.setObsolete(appBean.isObsolete());
            bean.setStatus(appBean.getStatus());
            bean.setEmailVerifiedTime(appBean.getEmailVerifiedTime());
            bean.setOpenIdVerifiedTime(appBean.getOpenIdVerifiedTime());
            bean.setAuthenticatedTime(appBean.getAuthenticatedTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<User> convertAppUserBeanListToServerBeanList(List<User> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<User> beanList = new ArrayList<User>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(User sb : appBeanList) {
                com.queryclient.ws.bean.UserBean bean = convertAppUserBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
