package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.stub.DummyEntityStub;


// Wrapper class + bean combo.
public class DummyEntityBean implements DummyEntity, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DummyEntityBean.class.getName());

    // [1] With an embedded object.
    private DummyEntityStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String name;
    private String content;
    private Integer maxLength;
    private Boolean expired;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public DummyEntityBean()
    {
        //this((String) null);
    }
    public DummyEntityBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public DummyEntityBean(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status)
    {
        this(guid, user, name, content, maxLength, expired, status, null, null);
    }
    public DummyEntityBean(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.name = name;
        this.content = content;
        this.maxLength = maxLength;
        this.expired = expired;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public DummyEntityBean(DummyEntity stub)
    {
        if(stub instanceof DummyEntityStub) {
            this.stub = (DummyEntityStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setName(stub.getName());   
            setContent(stub.getContent());   
            setMaxLength(stub.getMaxLength());   
            setExpired(stub.isExpired());   
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getName()
    {
        if(getStub() != null) {
            return getStub().getName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.name;
        }
    }
    public void setName(String name)
    {
        if(getStub() != null) {
            getStub().setName(name);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.name = name;
        }
    }

    public String getContent()
    {
        if(getStub() != null) {
            return getStub().getContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.content;
        }
    }
    public void setContent(String content)
    {
        if(getStub() != null) {
            getStub().setContent(content);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.content = content;
        }
    }

    public Integer getMaxLength()
    {
        if(getStub() != null) {
            return getStub().getMaxLength();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.maxLength;
        }
    }
    public void setMaxLength(Integer maxLength)
    {
        if(getStub() != null) {
            getStub().setMaxLength(maxLength);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.maxLength = maxLength;
        }
    }

    public Boolean isExpired()
    {
        if(getStub() != null) {
            return getStub().isExpired();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expired;
        }
    }
    public void setExpired(Boolean expired)
    {
        if(getStub() != null) {
            getStub().setExpired(expired);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expired = expired;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public DummyEntityStub getStub()
    {
        return this.stub;
    }
    protected void setStub(DummyEntityStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("name = " + this.name).append(";");
            sb.append("content = " + this.content).append(";");
            sb.append("maxLength = " + this.maxLength).append(";");
            sb.append("expired = " + this.expired).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = name == null ? 0 : name.hashCode();
            _hash = 31 * _hash + delta;
            delta = content == null ? 0 : content.hashCode();
            _hash = 31 * _hash + delta;
            delta = maxLength == null ? 0 : maxLength.hashCode();
            _hash = 31 * _hash + delta;
            delta = expired == null ? 0 : expired.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
