package com.queryclient.af.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


// Need to be careful with blacklist-based filter in case of errors, etc.
public class RefererBlacklistFilter implements Filter
{
    private static final Logger log = Logger.getLogger(RefererBlacklistFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    
    public RefererBlacklistFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // Pass-through access by default
        boolean accessAllowed = true;

        // Check blacklist.
        String referer = ((HttpServletRequest) req).getHeader("referer");
        // TBD:
        // Change accessAllowed to false if referer in the blacklist...
        // ...        

        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "RefererBlacklistFilter.doFilter(): accessAllowed = " + accessAllowed);

        // TBD
        // Redirect to the error or login page. ???
        config.getServletContext().getRequestDispatcher("/").forward(req, res);
    }
    
}
