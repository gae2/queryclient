package com.queryclient.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.CommonConstants;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.resource.BaseResourceException;
import com.queryclient.ws.resource.exception.BadRequestRsException;
import com.queryclient.ws.resource.exception.InternalServerErrorRsException;
import com.queryclient.ws.resource.exception.NotImplementedRsException;
import com.queryclient.ws.resource.exception.RequestConflictRsException;
import com.queryclient.ws.resource.exception.RequestForbiddenRsException;
import com.queryclient.ws.resource.exception.ResourceGoneRsException;
import com.queryclient.ws.resource.exception.ResourceNotFoundRsException;
import com.queryclient.ws.resource.exception.ServiceUnavailableRsException;

import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.stub.QuerySessionStub;
import com.queryclient.ws.stub.QuerySessionListStub;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.resource.QuerySessionResource;
import com.queryclient.af.resource.util.ReferrerInfoStructResourceUtil;


// MockQuerySessionResource is a decorator.
// It can be used as a base class to mock QuerySessionResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/querySessions/")
public abstract class MockQuerySessionResource implements QuerySessionResource
{
    private static final Logger log = Logger.getLogger(MockQuerySessionResource.class.getName());

    // MockQuerySessionResource uses the decorator design pattern.
    private QuerySessionResource decoratedResource;

    public MockQuerySessionResource(QuerySessionResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected QuerySessionResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(QuerySessionResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllQuerySessions(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findQuerySessionsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findQuerySessionsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getQuerySessionAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getQuerySessionAsHtml(guid);
//    }

    @Override
    public Response getQuerySession(String guid) throws BaseResourceException
    {
        return decoratedResource.getQuerySession(guid);
    }

    @Override
    public Response getQuerySessionAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getQuerySessionAsJsonp(guid, callback);
    }

    @Override
    public Response getQuerySession(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getQuerySession(guid, field);
    }

    // TBD
    @Override
    public Response constructQuerySession(QuerySessionStub querySession) throws BaseResourceException
    {
        return decoratedResource.constructQuerySession(querySession);
    }

    @Override
    public Response createQuerySession(QuerySessionStub querySession) throws BaseResourceException
    {
        return decoratedResource.createQuerySession(querySession);
    }

//    @Override
//    public Response createQuerySession(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createQuerySession(formParams);
//    }

    // TBD
    @Override
    public Response refreshQuerySession(String guid, QuerySessionStub querySession) throws BaseResourceException
    {
        return decoratedResource.refreshQuerySession(guid, querySession);
    }

    @Override
    public Response updateQuerySession(String guid, QuerySessionStub querySession) throws BaseResourceException
    {
        return decoratedResource.updateQuerySession(guid, querySession);
    }

    @Override
    public Response updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, String referrerInfo, String status, String note)
    {
        return decoratedResource.updateQuerySession(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note);
    }

//    @Override
//    public Response updateQuerySession(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateQuerySession(guid, formParams);
//    }

    @Override
    public Response deleteQuerySession(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteQuerySession(guid);
    }

    @Override
    public Response deleteQuerySessions(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteQuerySessions(filter, params, values);
    }


// TBD ....
    @Override
    public Response createQuerySessions(QuerySessionListStub querySessions) throws BaseResourceException
    {
        return decoratedResource.createQuerySessions(querySessions);
    }


}
