package com.queryclient.af.resource.impl;

import com.queryclient.af.config.Config;
import com.queryclient.af.auth.common.AuthMethod;


// Place holder, for now.
public abstract class BaseResourceImpl
{
    // These need to be moved to somewhere else (e.g., common util class) rather than keeping here.

    // temporary
    private static final String CONFIG_KEY_IGNORE_AUTH = "queryclientapp.webservice.ignoreauth";
    private static final Boolean CONFIG_DEFAULT_IGNORE_AUTH = false;
    private static final String CONFIG_KEY_ANON_READ = "queryclientapp.webservice.anonread";
    private static final Boolean CONFIG_DEFAULT_ANON_READ = false;

    // temporary
    private static final String CONFIG_KEY_AUTH_METHOD = "queryclientapp.webservice.auth.method";
    private static final String CONFIG_DEFAULT_AUTH_METHOD = AuthMethod.TWOLEGGED;

    // "instance cache"
    private Boolean ignoreAuth = null;
    private Boolean anonReadAllowed = null;
    private String authMethod = null;
    
    
    public BaseResourceImpl()
    {
    }

    // Note that this only works in the devel. environment.
    // You cannot bypass auth in the production environment.
    public boolean isIgnoreAuth()
    {
        if(ignoreAuth == null) {
            ignoreAuth = Config.getInstance().getBoolean(CONFIG_KEY_IGNORE_AUTH, CONFIG_DEFAULT_IGNORE_AUTH);
        }
        return ignoreAuth;
    }

    // If true, clients can GET resources without being authenticated (even in prod env).
    public boolean isAnonReadAllowed()
    {
        if(anonReadAllowed == null) {
            anonReadAllowed = Config.getInstance().getBoolean(CONFIG_KEY_ANON_READ, CONFIG_DEFAULT_ANON_READ);
        }
        return anonReadAllowed;
    }

    public String getAuthMethod()
    {
        if(authMethod == null) {
            authMethod = Config.getInstance().getString(CONFIG_KEY_AUTH_METHOD, CONFIG_DEFAULT_AUTH_METHOD);
            if(authMethod == null || authMethod.isEmpty()) {    // Validation??
                authMethod = AuthMethod.TWOLEGGED; 
            }
        }
        return authMethod;
    }

    // TBD:
    // Place holder.
    // This should be really done in a separate utility class/package.
    // Need to return some kind of Auth token object (including userId, accessToken, expirationTime, etc...)
    protected String getUserOAuth2AccessToken(String user)
    {
        // temporary
        return "universalaccesstokenfortesting";
    }

    protected String getHttpBasicAuthUsernameAndPassword(String user)
    {
        // temporary
        return "testuser:testpass";
    }

}
