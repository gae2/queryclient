package com.queryclient.af.search.gae;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.IndexSpec;
import com.google.appengine.api.search.SearchServiceFactory;
import com.google.appengine.api.search.StatusCode;


public class BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(BaseQueryHelper.class.getName());

    // temporary
    protected static final int MAX_COUNT = 10000;     // ???
    protected static final int DEFAULT_COUNT = 100;   // ???

    public BaseQueryHelper()
    {
    }

 
}
