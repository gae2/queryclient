package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;
// import com.queryclient.ws.bean.UserAuthStateBean;
import com.queryclient.ws.service.UserAuthStateService;
import com.queryclient.af.bean.UserAuthStateBean;
import com.queryclient.af.proxy.UserAuthStateServiceProxy;
import com.queryclient.af.proxy.util.GaeAppStructProxyUtil;
import com.queryclient.af.proxy.util.ExternalUserIdStructProxyUtil;


public class LocalUserAuthStateServiceProxy extends BaseLocalServiceProxy implements UserAuthStateServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserAuthStateServiceProxy.class.getName());

    public LocalUserAuthStateServiceProxy()
    {
    }

    @Override
    public UserAuthState getUserAuthState(String guid) throws BaseException
    {
        UserAuthState serverBean = getUserAuthStateService().getUserAuthState(guid);
        UserAuthState appBean = convertServerUserAuthStateBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getUserAuthState(String guid, String field) throws BaseException
    {
        return getUserAuthStateService().getUserAuthState(guid, field);       
    }

    @Override
    public List<UserAuthState> getUserAuthStates(List<String> guids) throws BaseException
    {
        List<UserAuthState> serverBeanList = getUserAuthStateService().getUserAuthStates(guids);
        List<UserAuthState> appBeanList = convertServerUserAuthStateBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates() throws BaseException
    {
        return getAllUserAuthStates(null, null, null);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUserAuthStateService().getAllUserAuthStates(ordering, offset, count);
        return getAllUserAuthStates(ordering, offset, count, null);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<UserAuthState> serverBeanList = getUserAuthStateService().getAllUserAuthStates(ordering, offset, count, forwardCursor);
        List<UserAuthState> appBeanList = convertServerUserAuthStateBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUserAuthStateService().getAllUserAuthStateKeys(ordering, offset, count);
        return getAllUserAuthStateKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserAuthStateService().getAllUserAuthStateKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserAuthStates(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUserAuthStateService().findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<UserAuthState> serverBeanList = getUserAuthStateService().findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<UserAuthState> appBeanList = convertServerUserAuthStateBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUserAuthStateService().findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserAuthStateService().findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserAuthStateService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserAuthState(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        return getUserAuthStateService().createUserAuthState(managerApp, appAcl, gaeApp, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime);
    }

    @Override
    public String createUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        com.queryclient.ws.bean.UserAuthStateBean serverBean =  convertAppUserAuthStateBeanToServerBean(userAuthState);
        return getUserAuthStateService().createUserAuthState(serverBean);
    }

    @Override
    public Boolean updateUserAuthState(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        return getUserAuthStateService().updateUserAuthState(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime);
    }

    @Override
    public Boolean updateUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        com.queryclient.ws.bean.UserAuthStateBean serverBean =  convertAppUserAuthStateBeanToServerBean(userAuthState);
        return getUserAuthStateService().updateUserAuthState(serverBean);
    }

    @Override
    public Boolean deleteUserAuthState(String guid) throws BaseException
    {
        return getUserAuthStateService().deleteUserAuthState(guid);
    }

    @Override
    public Boolean deleteUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        com.queryclient.ws.bean.UserAuthStateBean serverBean =  convertAppUserAuthStateBeanToServerBean(userAuthState);
        return getUserAuthStateService().deleteUserAuthState(serverBean);
    }

    @Override
    public Long deleteUserAuthStates(String filter, String params, List<String> values) throws BaseException
    {
        return getUserAuthStateService().deleteUserAuthStates(filter, params, values);
    }




    public static UserAuthStateBean convertServerUserAuthStateBeanToAppBean(UserAuthState serverBean)
    {
        UserAuthStateBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new UserAuthStateBean();
            bean.setGuid(serverBean.getGuid());
            bean.setManagerApp(serverBean.getManagerApp());
            bean.setAppAcl(serverBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertServerGaeAppStructBeanToAppBean(serverBean.getGaeApp()));
            bean.setOwnerUser(serverBean.getOwnerUser());
            bean.setUserAcl(serverBean.getUserAcl());
            bean.setProviderId(serverBean.getProviderId());
            bean.setUser(serverBean.getUser());
            bean.setUsername(serverBean.getUsername());
            bean.setEmail(serverBean.getEmail());
            bean.setOpenId(serverBean.getOpenId());
            bean.setDeviceId(serverBean.getDeviceId());
            bean.setSessionId(serverBean.getSessionId());
            bean.setAuthToken(serverBean.getAuthToken());
            bean.setAuthStatus(serverBean.getAuthStatus());
            bean.setExternalAuth(serverBean.getExternalAuth());
            bean.setExternalId(ExternalUserIdStructProxyUtil.convertServerExternalUserIdStructBeanToAppBean(serverBean.getExternalId()));
            bean.setStatus(serverBean.getStatus());
            bean.setFirstAuthTime(serverBean.getFirstAuthTime());
            bean.setLastAuthTime(serverBean.getLastAuthTime());
            bean.setExpirationTime(serverBean.getExpirationTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<UserAuthState> convertServerUserAuthStateBeanListToAppBeanList(List<UserAuthState> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<UserAuthState> beanList = new ArrayList<UserAuthState>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(UserAuthState sb : serverBeanList) {
                UserAuthStateBean bean = convertServerUserAuthStateBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.UserAuthStateBean convertAppUserAuthStateBeanToServerBean(UserAuthState appBean)
    {
        com.queryclient.ws.bean.UserAuthStateBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.UserAuthStateBean();
            bean.setGuid(appBean.getGuid());
            bean.setManagerApp(appBean.getManagerApp());
            bean.setAppAcl(appBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertAppGaeAppStructBeanToServerBean(appBean.getGaeApp()));
            bean.setOwnerUser(appBean.getOwnerUser());
            bean.setUserAcl(appBean.getUserAcl());
            bean.setProviderId(appBean.getProviderId());
            bean.setUser(appBean.getUser());
            bean.setUsername(appBean.getUsername());
            bean.setEmail(appBean.getEmail());
            bean.setOpenId(appBean.getOpenId());
            bean.setDeviceId(appBean.getDeviceId());
            bean.setSessionId(appBean.getSessionId());
            bean.setAuthToken(appBean.getAuthToken());
            bean.setAuthStatus(appBean.getAuthStatus());
            bean.setExternalAuth(appBean.getExternalAuth());
            bean.setExternalId(ExternalUserIdStructProxyUtil.convertAppExternalUserIdStructBeanToServerBean(appBean.getExternalId()));
            bean.setStatus(appBean.getStatus());
            bean.setFirstAuthTime(appBean.getFirstAuthTime());
            bean.setLastAuthTime(appBean.getLastAuthTime());
            bean.setExpirationTime(appBean.getExpirationTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<UserAuthState> convertAppUserAuthStateBeanListToServerBeanList(List<UserAuthState> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<UserAuthState> beanList = new ArrayList<UserAuthState>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(UserAuthState sb : appBeanList) {
                com.queryclient.ws.bean.UserAuthStateBean bean = convertAppUserAuthStateBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
