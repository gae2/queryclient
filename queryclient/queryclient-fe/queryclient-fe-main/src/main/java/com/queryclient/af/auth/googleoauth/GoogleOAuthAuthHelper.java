package com.queryclient.af.auth.googleoauth;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.util.JsonUtil;
import com.queryclient.af.config.Config;
import com.queryclient.af.util.URLUtil;
import com.queryclient.af.util.CsrfHelper;
import com.queryclient.af.auth.common.AuthPromptParam;
import com.queryclient.af.auth.common.CommonAuthUtil;


/**
 * Helper methods for implementing Google OAuth2 Login flow.
 * Cf. https://developers.google.com/accounts/docs/OAuth2WebServer
 * Work in progress!!!
 * 
 * Usage:
 * (1a) Create a URL via getAuthorizationCodeEndpointUrl() including rediret_uri.
 * (1b) Send GET request to this URL.
 * (2a) Implement a web service endpoint at the specified redirect_uri.
 * (2b) Process the callback POST from the Google Auth server. You can use parseAuthCallbackContent() to parse the request body.
 * (3a) Exchange the auth code received from (2) for Refresh Token, e.g., using exchangeAuthCodeForRefreshToken().
 * (3b) Get UserInfo from Google User API Web service (e.g., using GoogleOAuthUserHelper class).
 * (3c) Store the user data and the access/refresh tokens in DB.
 * (4) Refresh the access token, when needed, using getNewAccessToken().
 */
public final class GoogleOAuthAuthHelper
{
    private static final Logger log = Logger.getLogger(GoogleOAuthAuthHelper.class.getName());

    // For debugging purposes.
    // Set it to true during testing/debugging only.
    private static final boolean LOGGING_ENABLED = false;
    // ....

    // "Lazy initialization"
    private String mApplicationName = null;
    private String mClientId = null;
    private String mClientSecret = null;

    // Array[2] of space-delimited scope lists.
    // mOAuthDataScopeArr[0] for includeEmailScope == false.
    // mOAuthDataScopeArr[1] for includeEmailScope == true.
    private String[] mOAuthDataScopeArr = new String[2];
    
    // Map {customScopeKey -> Scope}
    private Map<String,String> mCustomOAuthDataScopeMap = new HashMap<>();

    // Callback URL path (e.g., /callback).
    // We dynamically add the top level URL part (e.g., http://beta.glassmemo.com/) based on the request.
    // This is necessary since this app (same codebase) can be deployed to a number of different domains.
    private String mCallbackAuthCodeHandlerUrlPath = null;


    // GoogleOAuthAuthHelper is a singleton..
    private GoogleOAuthAuthHelper() {}

    // Initialization-on-demand holder.
    private static final class GoogleOAuthAuthHelperHolder
    {
        private static final GoogleOAuthAuthHelper INSTANCE = new GoogleOAuthAuthHelper();
    }

    // Singleton method
    public static GoogleOAuthAuthHelper getInstance()
    {
        return GoogleOAuthAuthHelperHolder.INSTANCE;
    }
    
    
    public String getApplicationName()
    {
        if(mApplicationName == null) {
            mApplicationName = Config.getInstance().getString(GoogleOAuthAuthUtil.CONFIG_KEY_APPLICATION_NAME);
        }
        return mApplicationName;
    }
    public String getClientId()
    {
        if(mClientId == null) {
            mClientId = Config.getInstance().getString(GoogleOAuthAuthUtil.CONFIG_KEY_CLIENT_ID);
        }
        return mClientId;
    }
    public String getClientSecret()
    {
        if(mClientSecret == null) {
            mClientSecret = Config.getInstance().getString(GoogleOAuthAuthUtil.CONFIG_KEY_CLIENT_SECRET);
        }
        return mClientSecret;
    }


    // temporary
    private String getOAuthDataScope()
    {
        return getOAuthDataScope(GoogleOAuthAuthUtil.includeEmailScopeByDefault());
    }
    /**
     * @param includeEmailScope If set to true, the scope will include userinfo.email scope (in addition to userinfo.profile).
     * @return Space-separated list of scope.
     */
    private String getOAuthDataScope(boolean includeEmailScope)
    {
        if(includeEmailScope == true) {
            if(mOAuthDataScopeArr[1] == null) {
                Set<String> dataScopeSet = GoogleOAuthAuthUtil.readOAuthScopeSetFromConfig();
                if(dataScopeSet != null && !dataScopeSet.isEmpty()) {
                    dataScopeSet.add(GoogleOAuthAuthUtil.DATA_SCOPE_USERINFO_PROFILE);
                    dataScopeSet.add(GoogleOAuthAuthUtil.DATA_SCOPE_USERINFO_EMAIL);
                    mOAuthDataScopeArr[1] = GoogleOAuthAuthUtil.buildOAuthScope(dataScopeSet);
                }
                if(mOAuthDataScopeArr[1] == null || mOAuthDataScopeArr[1].isEmpty()) {
                    mOAuthDataScopeArr[1] = GoogleOAuthAuthUtil.getDefaultDataScope(includeEmailScope);
                }
            }
            return mOAuthDataScopeArr[1];
        } else {
            if(mOAuthDataScopeArr[0] == null) {
                Set<String> dataScopeSet = GoogleOAuthAuthUtil.readOAuthScopeSetFromConfig();
                if(dataScopeSet != null && !dataScopeSet.isEmpty()) {
                    dataScopeSet.remove(GoogleOAuthAuthUtil.DATA_SCOPE_USERINFO_EMAIL);
                    mOAuthDataScopeArr[0] = GoogleOAuthAuthUtil.buildOAuthScope(dataScopeSet);
                }
                if(mOAuthDataScopeArr[0] == null || mOAuthDataScopeArr[0].isEmpty()) {
                    mOAuthDataScopeArr[0] = GoogleOAuthAuthUtil.getDefaultDataScope(includeEmailScope);
                }
            }
            return mOAuthDataScopeArr[0];
        }
    }

    private String getCustomOAuthDataScope(String customScopeKey)
    {
        return getCustomOAuthDataScope(customScopeKey, GoogleOAuthAuthUtil.includeEmailScopeByDefault());
    }
    private String getCustomOAuthDataScope(String customScopeKey, boolean includeEmailScope)
    {
        if(mCustomOAuthDataScopeMap.get(customScopeKey) == null) {
            String scopes = null;
            Set<String> dataScopeSet = getCustomOAuthDataScopeSet(customScopeKey);
            if(dataScopeSet != null && !dataScopeSet.isEmpty()) {
                scopes = GoogleOAuthAuthUtil.buildOAuthScope(dataScopeSet);
            }
            if(scopes == null || scopes.isEmpty()) {
                scopes = GoogleOAuthAuthUtil.getDefaultDataScope(includeEmailScope);
            }
            mCustomOAuthDataScopeMap.put(customScopeKey, scopes);
        }
        return mCustomOAuthDataScopeMap.get(customScopeKey);
    }
    private Set<String> getCustomOAuthDataScopeSet(String customScopeKey)
    {
        Set<String> dataScopeSet = GoogleOAuthAuthUtil.readCustomOAuthScopeSetFromConfig(customScopeKey);
        return dataScopeSet;
    }


    private String getCallbackAuthCodeHandlerUrlPath()
    {
        if(mCallbackAuthCodeHandlerUrlPath == null) {
            mCallbackAuthCodeHandlerUrlPath = Config.getInstance().getString(GoogleOAuthAuthUtil.CONFIG_KEY_CALLBACK_AUTHCODE_HANDLER_URLPATH);
        }
        return mCallbackAuthCodeHandlerUrlPath;
    }
    public String getCallbackAuthCodeHandlerUri(String topLevelUrl)
    {
        return CommonAuthUtil.constructUrl(topLevelUrl, getCallbackAuthCodeHandlerUrlPath());
    }


    /**
     * Returns Google Auth API webservice endpoint URL.
     * Need to send GET request to get an authorization code for the current user.
     * customScopeKey is a space-separated list of scopes OR "scope keys".
     */
    public String getAuthorizationCodeEndpointUrl(String topLevelUrl)
    {
        return getAuthorizationCodeEndpointUrl(topLevelUrl, null, null);
    }
    public String getAuthorizationCodeEndpointUrl(String topLevelUrl, String comebackUrl, String fallbackUrl)
    {
        return getAuthorizationCodeEndpointUrl(topLevelUrl, comebackUrl, fallbackUrl, null, null, 0);
    }
    public String getAuthorizationCodeEndpointUrl(String topLevelUrl, String comebackUrl, String fallbackUrl, String authMode, String authProvider, int authPrompt)
    {
        return getAuthorizationCodeEndpointUrl(topLevelUrl, comebackUrl, fallbackUrl, authMode, authProvider, authPrompt, null);
    }
    public String getAuthorizationCodeEndpointUrl(String topLevelUrl, String comebackUrl, String fallbackUrl, String authMode, String authProvider, int authPrompt, String customScopeKey)
    {
        return getAuthorizationCodeEndpointUrl(topLevelUrl, comebackUrl, fallbackUrl, authMode, authProvider, authPrompt, customScopeKey, null);
    }
    public String getAuthorizationCodeEndpointUrl(String topLevelUrl, String comebackUrl, String fallbackUrl, String authMode, String authProvider, int authPrompt, String customScopeKey, Boolean includeEmailScope)
    {
        return getAuthorizationCodeEndpointUrl(topLevelUrl, comebackUrl, fallbackUrl, authMode, authProvider, authPrompt, customScopeKey, includeEmailScope, null);
    }
    public String getAuthorizationCodeEndpointUrl(String topLevelUrl, String comebackUrl, String fallbackUrl, String authMode, String authProvider, int authPrompt, String customScopeKey, Boolean includeEmailScope, String userGuid)
    {
        return getAuthorizationCodeEndpointUrl(topLevelUrl, comebackUrl, fallbackUrl, authMode, authProvider, authPrompt, customScopeKey, includeEmailScope, userGuid, 0L);
    }
    public String getAuthorizationCodeEndpointUrl(String topLevelUrl, String comebackUrl, String fallbackUrl, String authMode, String authProvider, int authPrompt, String customScopeKey, Boolean includeEmailScope, String userGuid, long userRole)
    {
        return getAuthorizationCodeEndpointUrl(topLevelUrl, comebackUrl, fallbackUrl, authMode, authProvider, authPrompt, customScopeKey, includeEmailScope, userGuid, userRole, null);
    }
    public String getAuthorizationCodeEndpointUrl(String topLevelUrl, String comebackUrl, String fallbackUrl, String authMode, String authProvider, int authPrompt, String customScopeKey, Boolean includeEmailScope, String userGuid, long userRole, String csrfState)
    {
        return getAuthorizationCodeEndpointUrl(topLevelUrl, comebackUrl, fallbackUrl, authMode, authProvider, authPrompt, customScopeKey, includeEmailScope, userGuid, userRole, csrfState, null);
    }
    public String getAuthorizationCodeEndpointUrl(String topLevelUrl, String comebackUrl, String fallbackUrl, String authMode, String authProvider, int authPrompt, String customScopeKey, Boolean includeEmailScope, String userGuid, long userRole, String csrfState, Map<String,Object> params)
    {
        String baseUrl = GoogleOAuthAuthUtil.BASE_URL_AUTHORIZATION_CODE;
        Map<String,Object> defaultParams = new LinkedHashMap<>();  // Ordered, for debugging purposes.

        defaultParams.put(GoogleOAuthAuthUtil.PARAM_RESPONSE_TYPE, "code");
        defaultParams.put(GoogleOAuthAuthUtil.PARAM_ACCESS_TYPE, "offline");   // Always offline!!!
        // defaultParams.put(GoogleOAuthAuthUtil.PARAM_APPROVAL_PROMPT, "auto");
        // defaultParams.put(GoogleOAuthAuthUtil.PARAM_LOGIN_HINT, "");  // ???
        defaultParams.put(GoogleOAuthAuthUtil.PARAM_CLIENT_ID, getClientId());
        
        String redirectUri = getCallbackAuthCodeHandlerUri(topLevelUrl);
        if(log.isLoggable(Level.INFO)) log.info("redirectUri = " + redirectUri);
        if(redirectUri == null || redirectUri.isEmpty()) {
            // Error ???? 
            // What to do ??? Throw exception???
            // This should not happen....
            log.warning("redirectUri is null/empty. Something's wrong.");
            return null;
        }
        defaultParams.put(GoogleOAuthAuthUtil.PARAM_REDIRECT_URI, redirectUri);

        if(AuthPromptParam.isForcePromptApproval(authPrompt)) {
            defaultParams.put(GoogleOAuthAuthUtil.PARAM_APPROVAL_PROMPT, "force");
        } else {
            defaultParams.put(GoogleOAuthAuthUtil.PARAM_APPROVAL_PROMPT, "auto");
        }

        if(includeEmailScope == null) {
            includeEmailScope = GoogleOAuthAuthUtil.includeEmailScopeByDefault();
        }

        // TBD: How can you tell if the element is a key or a scope????
        String scope = null;
        if(customScopeKey != null && !customScopeKey.isEmpty()) {
            Set<String> scopeSet = new HashSet<>();
            Set<String> scopeOrKeySet = GoogleOAuthAuthUtil.parseOAuthScope(customScopeKey);
            for(String k : scopeOrKeySet) {
                if(k == null || k.isEmpty()) {
                    continue;
                }
                Set<String> sss = getCustomOAuthDataScopeSet(k);
                if(sss == null) {    // It likely means k is a scope not a scope key.  Note. we treat an empty list as valid. --> need to check whether this makes sense...
                    // TBD: check if this is a valid scope (e.g., whether it starts with "https://", etc. ???)
                    scopeSet.add(k);
                } else {
                    scopeSet = GoogleOAuthAuthUtil.addScopes(scopeSet, sss);
                }                
            }
            scope = GoogleOAuthAuthUtil.buildOAuthScope(scopeSet);
            // TBD:
            // if scope is empty, at this point,
            //    use the default scope list ?????
        } else {
            scope = getOAuthDataScope(includeEmailScope);
        }
        if(log.isLoggable(Level.FINE)) log.fine("The final scope = " + scope);
        if(scope != null && !scope.isEmpty()) {
            if(log.isLoggable(Level.INFO)) log.info("OAuth2 scope set to: " + scope);
            // TBD: What happens if the URL becomes too long???
            defaultParams.put(GoogleOAuthAuthUtil.PARAM_SCOPE, scope);
            // Save it to session "flash" attr as well.... ????
            // ....
        } else {
            // ????
        }

        String state = buildStateParam(comebackUrl, fallbackUrl, authMode, authProvider, authPrompt, scope, userGuid, userRole, csrfState);
        if(state != null && !state.isEmpty()) {
            defaultParams.put(GoogleOAuthAuthUtil.PARAM_STATE, state);  // ???
        }
        
        // Overwrites the default param values.
        if(params != null && !params.isEmpty()) {
            for(String key : params.keySet()) {
                // TBD: Check if the key is a valid param ????
                Object value = params.get(key);
                // This is a bit strange. We use Map<String,Object> as an arg, but we only support Map<String,String>.
                // This is due to the "compatibility" reasons, and this also allows us 
                //   to change later (if necessary) the implementations without having to change the interface.
                if(value instanceof String) {   // TBD: exclude PARAM_STATE ???
                    // Is param key valid??? Need to check.
                    // For now, just add it or overwrite the existing value...
                    defaultParams.put(key, value);
                    // ...
                } else {
                    // error
                    if(LOGGING_ENABLED) {
                        if(log.isLoggable(Level.INFO)) log.info("Invalid param value: key = " + key + "; value = " + value);
                    } else {
                        if(log.isLoggable(Level.INFO)) log.info("Invalid param value: key = " + key);
                    }
                }
            }
        }
        
        String url = URLUtil.buildUrl(baseUrl, defaultParams);
        return url;
    }


    // To get accessToken from code.
    // It returns refreshToken as well, if the original auth code request was offline access_type....
    // "POST" will be used.
    public String getRefreshTokenEndpointUrl()
    {
        String baseUrl = GoogleOAuthAuthUtil.BASE_URL_REFRESH_TOKEN;
        return baseUrl;
    }
    public String buildRefreshTokenPostData(String code, String topLevelUrl)
    {
        return buildRefreshTokenPostData(code, topLevelUrl, null);
    }
    public String buildRefreshTokenPostData(String code, String topLevelUrl, Map<String,Object> params)
    {
        Map<String,Object> defaultParams = new LinkedHashMap<String,Object>();  // Ordered, for debugging purposes.
        
        if(code == null || code.isEmpty()) {
            // Error ????
            // What to do ???
            log.warning("Input authorization code is null/empty.");
            return null;   // ??? Throw exception?
        }
        defaultParams.put(GoogleOAuthAuthUtil.PARAM_CODE, code);

        defaultParams.put(GoogleOAuthAuthUtil.PARAM_GRANT_TYPE, "authorization_code"); 
        defaultParams.put(GoogleOAuthAuthUtil.PARAM_CLIENT_ID, getClientId());
        defaultParams.put(GoogleOAuthAuthUtil.PARAM_CLIENT_SECRET, getClientSecret());
        
        String redirectUri = getCallbackAuthCodeHandlerUri(topLevelUrl);
        if(log.isLoggable(Level.INFO)) log.info("redirectUri = " + redirectUri);
        if(redirectUri == null || redirectUri.isEmpty()) {
            // Error ????
            // What to do ???
            log.warning("redirectUri is null/empty.");
        }
        defaultParams.put(GoogleOAuthAuthUtil.PARAM_REDIRECT_URI, redirectUri);
        
        // Overwrites the default param values.
        if(params != null && !params.isEmpty()) {
            for(String key : params.keySet()) {
                // TBD: Check if the key is a valid param ????
                Object value = params.get(key);
                if(value instanceof String) {   // TBD: exclude PARAM_STATE ???
                    // Is param key valid???
                    // For now, just add it or overwrite the existing value...
                    defaultParams.put(key, value);
                    // ...
                } else {
                    // error
                    if(LOGGING_ENABLED) {
                        if(log.isLoggable(Level.INFO)) log.info("Invalid param value: key = " + key + "; value = " + value);
                    } else {
                        if(log.isLoggable(Level.INFO)) log.info("Invalid param value: key = " + key);
                    }
                }
            }
        }
        
        String queryStr = URLUtil.buildQueryString(defaultParams);
        return queryStr;
    }


    // The doc, https://developers.google.com/accounts/docs/OAuth2WebServer, is not very clear,
    //       but the redirect_uri param should be the same as the one used when getting the auth code,
    //       and this URL is NOT used for code exchange.
    // This POST appears to be a synchronous call.
    public GoogleOAuthAccessToken exchangeAuthCodeForRefreshToken(String code, String topLevelUrl, Map<String,Object> params)
    {
        return exchangeAuthCodeForRefreshToken(code, topLevelUrl, params, null);
    }
    public GoogleOAuthAccessToken exchangeAuthCodeForRefreshToken(String code, String topLevelUrl, Map<String,Object> params, String dataScopes)
    {
        GoogleOAuthAccessToken oauthToken = null;

        String endpointUrl = getRefreshTokenEndpointUrl();
        try {
            URL refreshTokenEndpointURL = new URL(endpointUrl);
            HttpURLConnection conn = (HttpURLConnection) refreshTokenEndpointURL.openConnection();
            conn.setRequestMethod("POST");

            // conn.setInstanceFollowRedirects(false);
            conn.setConnectTimeout(30 * 1000);   // ????
            
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            
            //conn.connect();

            String postData = buildRefreshTokenPostData(code, topLevelUrl, params);
            // temporary
            if(LOGGING_ENABLED && log.isLoggable(Level.INFO)) log.info("postData = " + postData + " for endpointUrl, " + endpointUrl);
            // temporary
            if(postData == null || postData.isEmpty()) {
                log.warning("Failed to construct postData.");
                return null;   // ????
            }
            
            DataOutputStream output = new DataOutputStream(conn.getOutputStream());
            output.writeBytes(postData);
            output.flush();
            output.close();

            int statusCode = conn.getResponseCode();
            if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for endpointUrl, " + endpointUrl);

            // String contentType = conn.getContentType();
            // int contentLength = conn.getContentLength();
            // String contentLanguage = conn.getHeaderField("Content-Language");

            BufferedReader input = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            StringBuilder sb = new StringBuilder();
            while (null != (line = input.readLine())) {
                sb.append(line);
            }
            input.close();
            String content = sb.toString();
            // temporary.
            // The "content" contains sensitive data. Do NOT log it.
            if(LOGGING_ENABLED && log.isLoggable(Level.FINE)) log.fine("content received = " + content + " for endpointUrl, " + endpointUrl);
            // temporary

            // TBD:
            // The return code should be 200 for success, I think,
            // but we check this more broadly (for 2xx), for now.
            if(StatusCode.isSuccessful(statusCode)) { 

                oauthToken = getOAuthAccessTokenFromResponseContent(content);

                if(dataScopes != null) {  // !dataScopes.isEmpty() ???
                    Set<String> scopeSet = GoogleOAuthAuthUtil.parseOAuthScope(dataScopes);
                    if(scopeSet != null) {  // !scopeSet.isEmpty() ???
                        oauthToken.setDataScopes(scopeSet);
                    }
                }

                // temporary.
                // The oauthToken contains sensitive data. Do NOT log it.
                if(LOGGING_ENABLED && log.isLoggable(Level.FINE)) log.fine("oauthToken = " + oauthToken);
                // temporary
                
            } else {
                // ???
                log.warning("Post failed: statusCode = " + statusCode + " for endpointUrl, " + endpointUrl);
            }

            // ???
            conn.disconnect();

        } catch (MalformedURLException e) {
            log.log(Level.WARNING, "Bad endpointUrl, " + endpointUrl, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Post failed for endpointUrl, " + endpointUrl, e);
        }
        
        return oauthToken;
    }


    // To get access token from the refresh token.
    public String getAccessTokenEndpointUrl()
    {
        String baseUrl = GoogleOAuthAuthUtil.BASE_URL_ACCESS_TOKEN;
        return baseUrl;
    }
    public String buildAccessTokenPostData(String refreshToken, String topLevelUrl)
    {
        return buildAccessTokenPostData(refreshToken, topLevelUrl, null);
    }
    public String buildAccessTokenPostData(String refreshToken, String topLevelUrl, Map<String,Object> params)
    {
        Map<String,Object> defaultParams = new LinkedHashMap<>();  // Ordered, for debugging purposes.
        
        if(refreshToken == null || refreshToken.isEmpty()) {
            // Error ????
            // What to do ???
            log.warning("Input refreshToken is null/empty.");
            return null;   // ??? Throw exception?
        }
        defaultParams.put(GoogleOAuthAuthUtil.PARAM_REFRESH_TOKEN, refreshToken);

        defaultParams.put(GoogleOAuthAuthUtil.PARAM_GRANT_TYPE, "refresh_token"); 
        defaultParams.put(GoogleOAuthAuthUtil.PARAM_CLIENT_ID, getClientId());
        defaultParams.put(GoogleOAuthAuthUtil.PARAM_CLIENT_SECRET, getClientSecret());

        // Overwrites the default param values.
        if(params != null && !params.isEmpty()) {
            for(String key : params.keySet()) {
                // TBD: Check if the key is a valid param ????
                Object value = params.get(key);
                if(value instanceof String) {   // TBD: exclude PARAM_STATE ???
                    // Is param key valid???
                    // For now, just add it or overwrite the existing value...
                    defaultParams.put(key, value);
                    // ...
                } else {
                    // error
                    if(LOGGING_ENABLED) {
                        if(log.isLoggable(Level.INFO)) log.info("Invalid param value: key = " + key + "; value = " + value);
                    } else {
                        if(log.isLoggable(Level.INFO)) log.info("Invalid param value: key = " + key);
                    }
                }
            }
        }

        String queryStr = URLUtil.buildQueryString(defaultParams);
        return queryStr;
    }

    // Get a new access token from refresh token.
    // This function is rather similar to exchangeAuthCodeForRefreshToken().
    // They may be refactored to include some common methods, etc....
    public GoogleOAuthAccessToken getNewAccessToken(String refreshToken, String topLevelUrl, Map<String,Object> params)
    {
        return getNewAccessToken(refreshToken, topLevelUrl, params, null);
    }
    public GoogleOAuthAccessToken getNewAccessToken(String refreshToken, String topLevelUrl, Map<String,Object> params, String dataScopes)
    {
        GoogleOAuthAccessToken oauthToken = null;

        String endpointUrl = getAccessTokenEndpointUrl();
        try {
            URL accessTokenEndpointURL = new URL(endpointUrl);
            HttpURLConnection conn = (HttpURLConnection) accessTokenEndpointURL.openConnection();
            conn.setRequestMethod("POST");

            // conn.setInstanceFollowRedirects(false);
            conn.setConnectTimeout(30 * 1000);   // ????
            
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            
            //conn.connect();

            String postData = buildAccessTokenPostData(refreshToken, topLevelUrl, params);
            // temporary
            if(LOGGING_ENABLED && log.isLoggable(Level.INFO)) log.info("postData = " + postData + " for endpointUrl, " + endpointUrl);
            // temporary
            if(postData == null || postData.isEmpty()) {
                log.warning("Failed to construct postData.");
                return null;   // ????
            }
            
            DataOutputStream output = new DataOutputStream(conn.getOutputStream());
            output.writeBytes(postData);
            output.flush();
            output.close();

            int statusCode = conn.getResponseCode();
            if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for endpointUrl, " + endpointUrl);

            // String contentType = conn.getContentType();
            // int contentLength = conn.getContentLength();
            // String contentLanguage = conn.getHeaderField("Content-Language");

            BufferedReader input = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            StringBuilder sb = new StringBuilder();
            while (null != (line = input.readLine())) {
                sb.append(line);
            }
            input.close();
            String content = sb.toString();
            // temporary.
            // The "content" contains sensitive data. Do NOT log it.
            if(LOGGING_ENABLED && log.isLoggable(Level.FINE)) log.fine("content received = " + content + " for endpointUrl, " + endpointUrl);
            // temporary

            // TBD
            if(StatusCode.isSuccessful(statusCode)) { 

                oauthToken = getOAuthAccessTokenFromResponseContent(content, refreshToken);

                if(dataScopes != null) {  // !dataScopes.isEmpty() ???
                    Set<String> scopeSet = GoogleOAuthAuthUtil.parseOAuthScope(dataScopes);
                    if(scopeSet != null) {  // !scopeSet.isEmpty() ???
                        oauthToken.setDataScopes(scopeSet);
                    }
                }

                // temporary.
                // The oauthToken contains sensitive data. Do NOT log it.
                if(LOGGING_ENABLED && log.isLoggable(Level.FINE)) log.fine("oauthToken = " + oauthToken);
                // temporary
                
            } else {
                // ???
                log.warning("Post failed: statusCode = " + statusCode + " for endpointUrl, " + endpointUrl);
            }

            // ???
            conn.disconnect();

        } catch (MalformedURLException e) {
            log.log(Level.WARNING, "Bad endpointUrl, " + endpointUrl, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Post failed for endpointUrl, " + endpointUrl, e);
        }
        
        return oauthToken;
    }


    // TBD:
    // Revoke has not been tested yet...
    
    // token can be accessToken or refreshToken.
    public String getRevokeTokenEndpointUrl(String token, String topLevelUrl)
    {
        return getRevokeTokenEndpointUrl(token, topLevelUrl, null);
    }
    public String getRevokeTokenEndpointUrl(String token, String topLevelUrl, Map<String,Object> params)
    {
        String baseUrl = GoogleOAuthAuthUtil.BASE_URL_REVOKE_TOKEN;
        Map<String,Object> defaultParams = new LinkedHashMap<String,Object>();  // Ordered, for debugging purposes.

        defaultParams.put(GoogleOAuthAuthUtil.PARAM_TOKEN, token);

        // Overwrites the default param values.
        if(params != null && !params.isEmpty()) {
            for(String key : params.keySet()) {
                // TBD: Check if the key is a valid param ????
                Object value = params.get(key);
                if(value instanceof String) {   // TBD: exclude PARAM_STATE ???
                    // Is param key valid???
                    // For now, just add it or overwrite the existing value...
                    defaultParams.put(key, value);
                    // ...
                } else {
                    // error
                    if(LOGGING_ENABLED) {
                        if(log.isLoggable(Level.INFO)) log.info("Invalid param value: key = " + key + "; value = " + value);
                    } else {
                        if(log.isLoggable(Level.INFO)) log.info("Invalid param value: key = " + key);
                    }
                }
            }
        }
        
        String url = URLUtil.buildUrl(baseUrl, defaultParams);
        return url;
    }

    // Returns the status code.
    public int revokeToken(String token, String topLevelUrl, Map<String,Object> params)
    {
        int statusCode = 0;

        String endpointUrl = getRevokeTokenEndpointUrl(token, topLevelUrl);
        try {
            URL revokeTokenEndpointURL = new URL(endpointUrl);
            HttpURLConnection conn = (HttpURLConnection) revokeTokenEndpointURL.openConnection();
            conn.setRequestMethod("GET");

            // conn.setInstanceFollowRedirects(false);
            conn.setConnectTimeout(30 * 1000);   // ????
            
            // conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            
            conn.connect();

            statusCode = conn.getResponseCode();
            if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for endpointUrl, " + endpointUrl);

            // String contentType = conn.getContentType();
            // int contentLength = conn.getContentLength();
            // String contentLanguage = conn.getHeaderField("Content-Language");

            BufferedReader input = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            StringBuilder sb = new StringBuilder();
            while (null != (line = input.readLine())) {
                sb.append(line);
            }
            input.close();
            String content = sb.toString();
            // temporary.
            // The "content" contains sensitive data. Do NOT log it.
            if(LOGGING_ENABLED && log.isLoggable(Level.FINE)) log.fine("content received = " + content + " for endpointUrl, " + endpointUrl);
            // temporary
            
            // TBD:
            // What to do with content?

            // TBD
            if(StatusCode.isSuccessful(statusCode)) { 
                // Success...
                if(log.isLoggable(Level.INFO)) log.info("Revoke succeeded: statusCode =  " + statusCode);
            } else {
                // ???
                log.warning("Revoke failed: statusCode = " + statusCode + " for endpointUrl, " + endpointUrl);
            }

            // ???
            conn.disconnect();

        } catch (MalformedURLException e) {
            log.log(Level.WARNING, "Bad endpointUrl, " + endpointUrl, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Post failed for endpointUrl, " + endpointUrl, e);
        }
        
        return statusCode;
    }

    
    
    // temporary
    // We use URL query param format here, but that is not necessary...
    // The algo here should be consistent with the parser routine below.
    private String buildStateParam(String comebackUrl, String fallbackUrl, String authMode, String authProvider, int authPrompt, String dataScope, String userGuid, long userRole, String csrfState, String ... extraVars)
    {
        // URL-encode URLs ???
        // Otherwise, "&" cannot be used as a delimiter.
        String state = "";
        if(comebackUrl != null && !comebackUrl.isEmpty()) {
            try {
                String encodedComebackUrl = URLEncoder.encode(comebackUrl, "UTF-8");
                state += CommonAuthUtil.PARAM_COMEBACKURL + "=" + encodedComebackUrl;
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to encode comebackUrl = " + comebackUrl, e);
            }
        }
        if(fallbackUrl != null && !fallbackUrl.isEmpty()) {
            try {
                String encodedFallbackUrl = URLEncoder.encode(fallbackUrl, "UTF-8");
                if(!state.isEmpty()) {
                    state += "&";
                }
                state += CommonAuthUtil.PARAM_FALLBACKURL + "=" + encodedFallbackUrl;
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to encode fallbackUrl = " + fallbackUrl, e);
            }
        }
        if(authMode != null && !authMode.isEmpty()) {
            try {
                String encodedAuthMode = URLEncoder.encode(authMode, "UTF-8");
                if(!state.isEmpty()) {
                    state += "&";
                }
                state += CommonAuthUtil.PARAM_AUTHMODE + "=" + encodedAuthMode;
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to encode authMode = " + authMode, e);
            }
        }
        if(authProvider != null && !authProvider.isEmpty()) {
            try {
                String encodedAuthProvider = URLEncoder.encode(authProvider, "UTF-8");
                if(!state.isEmpty()) {
                    state += "&";
                }
                state += CommonAuthUtil.PARAM_AUTHPROVIDER + "=" + encodedAuthProvider;
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to encode authProvider = " + authProvider, e);
            }
        }
        if(authPrompt > 0) {
            if(!state.isEmpty()) {
                state += "&";
            }
            state += CommonAuthUtil.PARAM_AUTHPROMPT + "=" + authPrompt;
        }
        // TBD: The state var (and, hence the URL) can become very long.
        //      Is that a problem????
        if(dataScope != null && !dataScope.isEmpty()) {
            try {
                String encodedScope = URLEncoder.encode(dataScope, "UTF-8");
                if(!state.isEmpty()) {
                    state += "&";
                }
                state += CommonAuthUtil.PARAM_DATASCOPE + "=" + encodedScope;
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to encode dataScope = " + dataScope, e);
            }
        }
        if(userGuid != null && !userGuid.isEmpty()) {
            try {
                String encodedUserGuid = URLEncoder.encode(userGuid, "UTF-8");
                if(!state.isEmpty()) {
                    state += "&";
                }
                state += CommonAuthUtil.PARAM_USERGUID + "=" + encodedUserGuid;
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to encode userGuid = " + userGuid, e);
            }
        }
        if(userRole > 0L) {
            if(!state.isEmpty()) {
                state += "&";
            }
            state += CommonAuthUtil.PARAM_USERROLE + "=" + userRole;
        }
        if(csrfState != null && !csrfState.isEmpty()) {
            try {
                String encodedCsrfState = URLEncoder.encode(csrfState, "UTF-8");
                if(!state.isEmpty()) {
                    state += "&";
                }
                state += CsrfHelper.REQUEST_PARAM_CSRF_STATE + "=" + encodedCsrfState;
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to encode csrfState = " + csrfState, e);
            }
        }

        // "extra params"
        // Note: The extraVars array is ordered.
        if(extraVars != null && extraVars.length > 0) {
            // Note: We currently support up to 3 extra vars only....
            // TBD: Is there a better way to do this?
            // (We can probably use CommonAuthUtil.PARAM_EXTRAVAR_PREFIX and dynamically generate the variable name.) 
            // (Note: we have to pass around all X extraVars in the auth JSP/servlets. --> Big number is a bit inconvinient.)
            int size = (extraVars.length < 3) ? extraVars.length : 3;
            for(int i=0; i<size; i++) {
                String var = null;
                switch(i) {
                case 0: 
                default:
                    var = CommonAuthUtil.PARAM_EXTRAVAR1;
                    break;
                case 1: 
                    var = CommonAuthUtil.PARAM_EXTRAVAR2;
                    break;
                case 2: 
                    var = CommonAuthUtil.PARAM_EXTRAVAR3;
                    break;
                } 
                String val = extraVars[i];
                try {
                    String encodedVal = URLEncoder.encode(csrfState, "UTF-8");
                    if(!state.isEmpty()) {
                        state += "&";
                    }
                    state += var + "=" + encodedVal;
                } catch (UnsupportedEncodingException e) {
                    log.log(Level.WARNING, "Failed to encode var = " + val, e);
                }
            }
        }

        return state;
    }
    // Parses the state param and returns it as a map.
    // This routine should match the corresponding builder routine.
    public Map<String,String> parseStateParam(String state)
    {
        Map<String,String> params = new HashMap<>();
        if(state != null && !state.isEmpty()) {
            // String[] parts = state.split("&", 6);
            String[] parts = state.split("&");
            if(parts != null) {
                for(String part : parts) {
                    if(part != null && !part.isEmpty()) {
                        String[] pair = part.split("=", 2);
                        if(pair != null && pair.length == 2) {
                            if(pair[0] != null) {
                                try {
                                    String val = URLDecoder.decode(pair[1], "UTF-8");
                                    params.put(pair[0], val);
                                } catch (UnsupportedEncodingException e) {
                                    log.log(Level.WARNING, "Failed to decode a pair = " + part, e);
                                }                                
                            }
                        }
                    }
                }
            }
        }
        return params;
    }


    // Parses url-encoded POST content from the Google Auth server.
    // This is the body/content of the response from the Google Auth server
    // when auth code GET request was sent to getAuthorizationCodeEndpointUrl().
    public Map<String,Object> parseAuthCallbackContent(String content)
    {
        if(content == null) {
            log.info("Input content is null. Just returning...");
            return null;
        }
        // Note: value should be either a String or an int.
        Map<String,Object> map = new HashMap<String,Object>();
        if(!content.isEmpty()) {
            String[] parts = content.split("&");
            if(parts != null && parts.length > 0) {
                for(String part : parts) {
                    String[] pair = part.split("=", 2);
                    if(pair != null && pair.length > 0) {
                        String key = pair[0];
                        Object val = null;
                        if(pair.length > 1) {
                            val = pair[1];
                            if(key.equals(GoogleOAuthAuthUtil.PARAM_EXPIRES_IN)) {
                                try {
                                    val = Integer.parseInt((String) val);
                                } catch(Exception e) {
                                    log.log(Level.WARNING, "Failed to parse the expires_in param.", e);
                                    val = null;   // ????
                                }
                            } else {
                                try {
                                    val = URLDecoder.decode((String) val, "UTF-8");
                                } catch(Exception e) {
                                    if(LOGGING_ENABLED) {
                                        log.log(Level.WARNING, "Failed to url decode a param value. val = " + val, e);
                                    } else {
                                        log.log(Level.WARNING, "Failed to url decode a param value.", e);
                                    }
                                    // val = null;   // ????
                                }
                            }
                        }
                        map.put(key, val);
                    }
                }
            }
        }

//      // temporary
//      if(log.isLoggable(Level.FINE)) {                
//          log.fine("Content map = ");
//          for(String k : map.keySet()) {
//              String v = map.get(k);
//              log.fine("    k = " + k + "; v = " + v);
//          }
//      }
//      // temporary

        return map;
    }

    // Get the auth code from the response content (from getAuthorizationCodeEndpointUrl()).
    public String getAuthCodeFromAuthCallbackContent(String content)
    {
        String code = null;
        Map<String,Object> map = parseAuthCallbackContent(content);
        if(map != null && !map.isEmpty()) {
            code = (String) map.get(GoogleOAuthAuthUtil.PARAM_CODE);
        }
        return code;
    }

    // Get the initial access_token, if necessary, from the response (from getAuthorizationCodeEndpointUrl()).
    // Note that the response does not include refresh_token value.
    // Note also that the GoogleOAuthAccessToken struct does not include auth_code field.
    public GoogleOAuthAccessToken getInitialOAuthAccessTokenFromAuthCallbackContent(String content)
    {
        Map<String,Object> map = parseAuthCallbackContent(content);
        if(map != null && !map.isEmpty()) {
            String accessToken = (String) map.get(GoogleOAuthAuthUtil.PARAM_ACCESS_TOKEN);
            String refreshToken = (String) map.get(GoogleOAuthAuthUtil.PARAM_REFRESH_TOKEN);   // This is always null.
            long expirationTime = System.currentTimeMillis();    // Default value = now ????
            Integer expiresIn = (Integer) map.get(GoogleOAuthAuthUtil.PARAM_EXPIRES_IN);
            if(expiresIn != null) {
                expirationTime += expiresIn * 1000L;
            }
            String tokenType = (String) map.get(GoogleOAuthAuthUtil.PARAM_TOKEN_TYPE);
            String idToken = (String) map.get(GoogleOAuthAuthUtil.PARAM_ID_TOKEN);    // What is this ???
            GoogleOAuthAccessToken oauthToken = new GoogleOAuthAccessToken(accessToken, refreshToken, expirationTime, tokenType, idToken);
            return oauthToken;
        }
        return null;
    }
    

    // The response (for access/refresh token request) is in a JSON format.
    // This is the response from the getRefreshTokenEndpointUrl() when
    // the getRefreshTokenPostData() is POST'ed to that endpoint URL.
    public GoogleOAuthAccessToken getOAuthAccessTokenFromResponseContent(String content)
    {
        return getOAuthAccessTokenFromResponseContent(content, null);
    }
    // Note that, when refreshing the access token, the server response does not include refreshToken.
    // This implementation seems a bit convoluted, but because GoogleOAuthAccessToken is immutable,
    //      the only way we can do this is to pass the refreshToken to this method.
    // We do not always need to include refresh_token in GoogleOAuthAccessToken, but tt's convenient
    //       (because we do not have to carry around extra param refresh_token separately).  
    public GoogleOAuthAccessToken getOAuthAccessTokenFromResponseContent(String content, String currentRefreshToken)
    {
        Map<String,String> map = JsonUtil.parseJsonObjectString(content);
        if(map != null && !map.isEmpty()) {
            String accessToken = map.get(GoogleOAuthAuthUtil.PARAM_ACCESS_TOKEN);
            String refreshToken = map.get(GoogleOAuthAuthUtil.PARAM_REFRESH_TOKEN);
            if( (refreshToken == null || refreshToken.isEmpty()) && !(currentRefreshToken == null || currentRefreshToken.isEmpty()) ) {
                // If the response does not include refreshToken, use the "current" value.
                refreshToken = currentRefreshToken;
            }
            long expirationTime = System.currentTimeMillis();    // Default value = now ????
            String expiresInStr = map.get(GoogleOAuthAuthUtil.PARAM_EXPIRES_IN);
            if(expiresInStr != null) {
                try {
                    int expiresIn = Integer.parseInt(expiresInStr);
                    expirationTime += expiresIn * 1000L;
                } catch(Exception e) {
                    // ignore. this should not really happen.
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse expiresIn = " + expiresInStr, e);
                }
            }
            String tokenType = (String) map.get(GoogleOAuthAuthUtil.PARAM_TOKEN_TYPE);
            String idToken = (String) map.get(GoogleOAuthAuthUtil.PARAM_ID_TOKEN);    // What is this ???
            GoogleOAuthAccessToken oauthToken = new GoogleOAuthAccessToken(accessToken, refreshToken, expirationTime, tokenType, idToken);
            return oauthToken;
        }
        return null;
    }


    // For anti-forgery... 
    
    public String getDefaultSignInFormId()
    {
        return GoogleOAuthAuthUtil.getDefaultGoogleOAuthSignInFormId();
    }

    // Returns the csrf in the current session or the generated csrf state string.
    public String setCsrfStateSessionAttribute(HttpSession session)
    {
        return setCsrfStateSessionAttribute(session, null);
    }
    public String setCsrfStateSessionAttribute(HttpSession session, String csrfState)
    {
        return setCsrfStateSessionAttribute(session, csrfState, null);
    }
    public String setCsrfStateSessionAttribute(HttpSession session, String csrfState, String formId)
    {
        return setCsrfStateSessionAttribute(session, csrfState, formId, false);
    }
    public String setCsrfStateSessionAttribute(HttpSession session, String csrfState, String formId, boolean reset)
    {
        if(session == null) {
            // ????
            return null;
        }
        if(formId == null || formId.isEmpty()) {
            formId = getDefaultSignInFormId();
        }
        if(reset) {
            csrfState = CsrfHelper.getInstance().resetCsrfStateSessionAttribute(session, csrfState, formId);
        } else {
            csrfState = CsrfHelper.getInstance().setCsrfStateSessionAttribute(session, csrfState, formId);
        }
        if(log.isLoggable(Level.INFO)) log.info("csrfState = " + csrfState);
        return csrfState;
    }

    public boolean verifyCsrfStateFromRequest(HttpServletRequest request)
    {
        return verifyCsrfStateFromRequest(request, null);
    }
    public boolean verifyCsrfStateFromRequest(HttpServletRequest request, String reqCsrfState)
    {
        if(request == null) {
            // ????
            return false;
        }
        String formId = GoogleOAuthAuthUtil.getDefaultGoogleOAuthSignInFormId();
        boolean isVerified = CsrfHelper.getInstance().verifyCsrfState(request, reqCsrfState, formId);
        return isVerified;
    }


}
