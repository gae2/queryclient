package com.queryclient.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.FiveTen;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.FiveTenStub;
import com.queryclient.ws.stub.FiveTenListStub;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.bean.FiveTenBean;
import com.queryclient.ws.service.FiveTenService;
import com.queryclient.af.proxy.FiveTenServiceProxy;
import com.queryclient.af.proxy.remote.RemoteFiveTenServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncFiveTenServiceProxy extends BaseAsyncServiceProxy implements FiveTenServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncFiveTenServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteFiveTenServiceProxy remoteProxy;

    public AsyncFiveTenServiceProxy()
    {
        remoteProxy = new RemoteFiveTenServiceProxy();
    }

    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        return remoteProxy.getFiveTen(guid);
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        return remoteProxy.getFiveTen(guid, field);       
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        return remoteProxy.getFiveTens(guids);
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return getAllFiveTens(null, null, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllFiveTens(ordering, offset, count);
        return getAllFiveTens(ordering, offset, count, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllFiveTens(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllFiveTenKeys(ordering, offset, count);
        return getAllFiveTenKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllFiveTenKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findFiveTens(filter, ordering, params, values, grouping, unique, offset, count);
        return findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException
    {
        FiveTenBean bean = new FiveTenBean(null, counter, requesterIpAddress);
        return createFiveTen(bean);        
    }

    @Override
    public String createFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        String guid = fiveTen.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((FiveTenBean) fiveTen).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateFiveTen-" + guid;
        String taskName = "RsCreateFiveTen-" + guid + "-" + (new Date()).getTime();
        FiveTenStub stub = MarshalHelper.convertFiveTenToStub(fiveTen);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(FiveTenStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = fiveTen.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    FiveTenStub dummyStub = new FiveTenStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createFiveTen(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "fiveTens/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createFiveTen(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "fiveTens/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "FiveTen guid is invalid.");
        	throw new BaseException("FiveTen guid is invalid.");
        }
        FiveTenBean bean = new FiveTenBean(guid, counter, requesterIpAddress);
        return updateFiveTen(bean);        
    }

    @Override
    public Boolean updateFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        String guid = fiveTen.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "FiveTen object is invalid.");
        	throw new BaseException("FiveTen object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateFiveTen-" + guid;
        String taskName = "RsUpdateFiveTen-" + guid + "-" + (new Date()).getTime();
        FiveTenStub stub = MarshalHelper.convertFiveTenToStub(fiveTen);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(FiveTenStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = fiveTen.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    FiveTenStub dummyStub = new FiveTenStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateFiveTen(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "fiveTens/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateFiveTen(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "fiveTens/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteFiveTen-" + guid;
        String taskName = "RsDeleteFiveTen-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "fiveTens/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException
    {
        String guid = fiveTen.getGuid();
        return deleteFiveTen(guid);
    }

    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteFiveTens(filter, params, values);
    }

}
