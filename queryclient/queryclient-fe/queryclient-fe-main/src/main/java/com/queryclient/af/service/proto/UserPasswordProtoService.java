package com.queryclient.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.UserPasswordBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.af.service.impl.UserPasswordServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserPasswordProtoService extends UserPasswordServiceImpl implements UserPasswordService
{
    private static final Logger log = Logger.getLogger(UserPasswordProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserPasswordProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserPassword related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserPassword getUserPassword(String guid) throws BaseException
    {
        return super.getUserPassword(guid);
    }

    @Override
    public Object getUserPassword(String guid, String field) throws BaseException
    {
        return super.getUserPassword(guid, field);
    }

    @Override
    public List<UserPassword> getUserPasswords(List<String> guids) throws BaseException
    {
        return super.getUserPasswords(guids);
    }

    @Override
    public List<UserPassword> getAllUserPasswords() throws BaseException
    {
        return super.getAllUserPasswords();
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswords(ordering, offset, count, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllUserPasswords(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUserPassword(UserPassword userPassword) throws BaseException
    {
        return super.createUserPassword(userPassword);
    }

    @Override
    public UserPassword constructUserPassword(UserPassword userPassword) throws BaseException
    {
        return super.constructUserPassword(userPassword);
    }


    @Override
    public Boolean updateUserPassword(UserPassword userPassword) throws BaseException
    {
        return super.updateUserPassword(userPassword);
    }
        
    @Override
    public UserPassword refreshUserPassword(UserPassword userPassword) throws BaseException
    {
        return super.refreshUserPassword(userPassword);
    }

    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        return super.deleteUserPassword(guid);
    }

    @Override
    public Boolean deleteUserPassword(UserPassword userPassword) throws BaseException
    {
        return super.deleteUserPassword(userPassword);
    }

    @Override
    public Integer createUserPasswords(List<UserPassword> userPasswords) throws BaseException
    {
        return super.createUserPasswords(userPasswords);
    }

    // TBD
    //@Override
    //public Boolean updateUserPasswords(List<UserPassword> userPasswords) throws BaseException
    //{
    //}

}
