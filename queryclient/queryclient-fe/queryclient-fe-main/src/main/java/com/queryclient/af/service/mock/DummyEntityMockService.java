package com.queryclient.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.DummyEntity;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.DummyEntityService;


// DummyEntityMockService is a decorator.
// It can be used as a base class to mock DummyEntityService objects.
public abstract class DummyEntityMockService implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(DummyEntityMockService.class.getName());

    // DummyEntityMockService uses the decorator design pattern.
    private DummyEntityService decoratedService;

    public DummyEntityMockService(DummyEntityService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected DummyEntityService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(DummyEntityService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // DummyEntity related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DummyEntity getDummyEntity(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDummyEntity(): guid = " + guid);
        DummyEntity bean = decoratedService.getDummyEntity(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getDummyEntity(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getDummyEntity(guid, field);
        return obj;
    }

    @Override
    public List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException
    {
        log.fine("getDummyEntities()");
        List<DummyEntity> dummyEntities = decoratedService.getDummyEntities(guids);
        log.finer("END");
        return dummyEntities;
    }

    @Override
    public List<DummyEntity> getAllDummyEntities() throws BaseException
    {
        return getAllDummyEntities(null, null, null);
    }


    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDummyEntities(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<DummyEntity> dummyEntities = decoratedService.getAllDummyEntities(ordering, offset, count, forwardCursor);
        log.finer("END");
        return dummyEntities;
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDummyEntityKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityMockService.findDummyEntities(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<DummyEntity> dummyEntities = decoratedService.findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return dummyEntities;
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityMockService.findDummyEntityKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DummyEntityMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        DummyEntityBean bean = new DummyEntityBean(null, user, name, content, maxLength, expired, status);
        return createDummyEntity(bean);
    }

    @Override
    public String createDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createDummyEntity(dummyEntity);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public DummyEntity constructDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");
        DummyEntity bean = decoratedService.constructDummyEntity(dummyEntity);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        DummyEntityBean bean = new DummyEntityBean(guid, user, name, content, maxLength, expired, status);
        return updateDummyEntity(bean);
    }
        
    @Override
    public Boolean updateDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateDummyEntity(dummyEntity);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public DummyEntity refreshDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");
        DummyEntity bean = decoratedService.refreshDummyEntity(dummyEntity);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDummyEntity(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteDummyEntity(DummyEntity dummyEntity) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDummyEntity(dummyEntity);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteDummyEntities(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createDummyEntities(dummyEntities);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    //{
    //}

}
