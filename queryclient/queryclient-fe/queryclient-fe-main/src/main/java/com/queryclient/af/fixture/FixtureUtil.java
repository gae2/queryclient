package com.queryclient.af.fixture;

import java.io.File;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.ApiConsumer;
import com.queryclient.ws.User;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.ws.UserAuthState;
import com.queryclient.ws.DataService;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.ServiceInfo;
import com.queryclient.ws.FiveTen;
import com.queryclient.ws.BaseException;


// Note:
// Fixture file name conventions.
// File path may in the form of "/a/b/c/[Type]Yyy.[suf]"
// The [Type] part determins the object type (class),
// whereas the file suffix [suf] determines the media type, xml or json.
// ...
public class FixtureUtil
{
    private static final Logger log = Logger.getLogger(FixtureUtil.class.getName());

    // temporary
    public static final String MEDIA_TYPE_XML = "XML";
    public static final String MEDIA_TYPE_JSON = "JSON";
    // ...
    private static final String DEFAULT_MEDIA_TYPE = MEDIA_TYPE_JSON;
    // ...
    // Fixture dir is, in the current implementation, under "/war" dir. 
    // Using "/war/WEB-INF/classes" (e.g., copied from "src" dir during build) is more secure,
    // but it requires using a class loader (whereas we can just use File API for files under "/war").
    private static final String DEFAULT_FIXTURE_DIR = "fixture";
    private static final String DEFAULT_FIXTURE_FILE = DEFAULT_FIXTURE_DIR + File.separator + "fixture.json";   // ????
    // ...
    private static final boolean DEFAULT_FIXTURE_LOAD = false;
    
    
    private FixtureUtil() {}

    
    public static boolean getDefaultFixtureLoad()
    {
        return DEFAULT_FIXTURE_LOAD;
    }

//    public static String getDefaultObjectType()
//    {
//        // ????
//        return null;
//    }
    
    public static String getDefaultMediaType()
    {
        // ????
        return DEFAULT_MEDIA_TYPE;
    }

    public static String getMediaType(String filePath)
    {
        if(filePath == null || filePath.isEmpty()) {
            return getDefaultMediaType();  // ???
        }
        int idx = filePath.lastIndexOf(".");
        if(idx >= 0) {
            String suffix = filePath.substring(idx + 1);
            if(suffix.equalsIgnoreCase(MEDIA_TYPE_XML)) {
                return MEDIA_TYPE_XML;
            } else if(suffix.equalsIgnoreCase(MEDIA_TYPE_JSON)) {
                return MEDIA_TYPE_JSON;
            } else {
                return getDefaultMediaType();  // ???
            }
        } else {
            return getDefaultMediaType();  // ???            
        }
    }

    // Returns the class name..
    // TBD: Or, return Class ????
    public static String getObjectType(String filePath)
    {
        if(filePath == null || filePath.isEmpty()) {
            return null;   // ???
        }
        int idx1 = filePath.lastIndexOf(File.separator);
        if(idx1 >= 0) {
            filePath = filePath.substring(idx1 + 1);
        }
        int idx2 = filePath.lastIndexOf(".");
        if(idx2 >= 0) {
            filePath = filePath.substring(0, idx2);
        }
        // At this point,
        // "filePath" refers to the root part of the file name. Just reusing the name filePath...

        // temporary
        // TBD: Type names should be sorted and longer type names should be put in front... 
        if(filePath.toLowerCase().startsWith("ApiConsumer".toLowerCase())) {
            String className = ApiConsumer.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ApiConsumer".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("User".toLowerCase())) {
            String className = User.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("User".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserPassword".toLowerCase())) {
            String className = UserPassword.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserPassword".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ExternalUserAuth".toLowerCase())) {
            String className = ExternalUserAuth.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ExternalUserAuth".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserAuthState".toLowerCase())) {
            String className = UserAuthState.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserAuthState".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("DataService".toLowerCase())) {
            String className = DataService.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("DataService".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ServiceEndpoint".toLowerCase())) {
            String className = ServiceEndpoint.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ServiceEndpoint".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("QuerySession".toLowerCase())) {
            String className = QuerySession.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("QuerySession".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("QueryRecord".toLowerCase())) {
            String className = QueryRecord.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("QueryRecord".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("DummyEntity".toLowerCase())) {
            String className = DummyEntity.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("DummyEntity".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ServiceInfo".toLowerCase())) {
            String className = ServiceInfo.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ServiceInfo".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("FiveTen".toLowerCase())) {
            String className = FiveTen.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("FiveTen".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        
        //...
        return null;        
    }

    // TBD
    public static String getDefaultFixtureDir()
    {
        // temporary
        return DEFAULT_FIXTURE_DIR;
    }
    public static String getDefaultFixtureFile()
    {
        // temporary
        return DEFAULT_FIXTURE_FILE;
    }

}
