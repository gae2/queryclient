package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
// import com.queryclient.ws.bean.UserPasswordBean;
import com.queryclient.ws.service.UserPasswordService;
import com.queryclient.af.bean.UserPasswordBean;
import com.queryclient.af.proxy.UserPasswordServiceProxy;
import com.queryclient.af.proxy.util.GaeAppStructProxyUtil;


public class LocalUserPasswordServiceProxy extends BaseLocalServiceProxy implements UserPasswordServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserPasswordServiceProxy.class.getName());

    public LocalUserPasswordServiceProxy()
    {
    }

    @Override
    public UserPassword getUserPassword(String guid) throws BaseException
    {
        UserPassword serverBean = getUserPasswordService().getUserPassword(guid);
        UserPassword appBean = convertServerUserPasswordBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getUserPassword(String guid, String field) throws BaseException
    {
        return getUserPasswordService().getUserPassword(guid, field);       
    }

    @Override
    public List<UserPassword> getUserPasswords(List<String> guids) throws BaseException
    {
        List<UserPassword> serverBeanList = getUserPasswordService().getUserPasswords(guids);
        List<UserPassword> appBeanList = convertServerUserPasswordBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<UserPassword> getAllUserPasswords() throws BaseException
    {
        return getAllUserPasswords(null, null, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUserPasswordService().getAllUserPasswords(ordering, offset, count);
        return getAllUserPasswords(ordering, offset, count, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<UserPassword> serverBeanList = getUserPasswordService().getAllUserPasswords(ordering, offset, count, forwardCursor);
        List<UserPassword> appBeanList = convertServerUserPasswordBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUserPasswordService().getAllUserPasswordKeys(ordering, offset, count);
        return getAllUserPasswordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserPasswordService().getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUserPasswordService().findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<UserPassword> serverBeanList = getUserPasswordService().findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<UserPassword> appBeanList = convertServerUserPasswordBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUserPasswordService().findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserPasswordService().findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserPasswordService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserPassword(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        return getUserPasswordService().createUserPassword(managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

    @Override
    public String createUserPassword(UserPassword userPassword) throws BaseException
    {
        com.queryclient.ws.bean.UserPasswordBean serverBean =  convertAppUserPasswordBeanToServerBean(userPassword);
        return getUserPasswordService().createUserPassword(serverBean);
    }

    @Override
    public Boolean updateUserPassword(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        return getUserPasswordService().updateUserPassword(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

    @Override
    public Boolean updateUserPassword(UserPassword userPassword) throws BaseException
    {
        com.queryclient.ws.bean.UserPasswordBean serverBean =  convertAppUserPasswordBeanToServerBean(userPassword);
        return getUserPasswordService().updateUserPassword(serverBean);
    }

    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        return getUserPasswordService().deleteUserPassword(guid);
    }

    @Override
    public Boolean deleteUserPassword(UserPassword userPassword) throws BaseException
    {
        com.queryclient.ws.bean.UserPasswordBean serverBean =  convertAppUserPasswordBeanToServerBean(userPassword);
        return getUserPasswordService().deleteUserPassword(serverBean);
    }

    @Override
    public Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException
    {
        return getUserPasswordService().deleteUserPasswords(filter, params, values);
    }




    public static UserPasswordBean convertServerUserPasswordBeanToAppBean(UserPassword serverBean)
    {
        UserPasswordBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new UserPasswordBean();
            bean.setGuid(serverBean.getGuid());
            bean.setManagerApp(serverBean.getManagerApp());
            bean.setAppAcl(serverBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertServerGaeAppStructBeanToAppBean(serverBean.getGaeApp()));
            bean.setOwnerUser(serverBean.getOwnerUser());
            bean.setUserAcl(serverBean.getUserAcl());
            bean.setAdmin(serverBean.getAdmin());
            bean.setUser(serverBean.getUser());
            bean.setUsername(serverBean.getUsername());
            bean.setEmail(serverBean.getEmail());
            bean.setOpenId(serverBean.getOpenId());
            bean.setPlainPassword(serverBean.getPlainPassword());
            bean.setHashedPassword(serverBean.getHashedPassword());
            bean.setSalt(serverBean.getSalt());
            bean.setHashMethod(serverBean.getHashMethod());
            bean.setResetRequired(serverBean.isResetRequired());
            bean.setChallengeQuestion(serverBean.getChallengeQuestion());
            bean.setChallengeAnswer(serverBean.getChallengeAnswer());
            bean.setStatus(serverBean.getStatus());
            bean.setLastResetTime(serverBean.getLastResetTime());
            bean.setExpirationTime(serverBean.getExpirationTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<UserPassword> convertServerUserPasswordBeanListToAppBeanList(List<UserPassword> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<UserPassword> beanList = new ArrayList<UserPassword>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(UserPassword sb : serverBeanList) {
                UserPasswordBean bean = convertServerUserPasswordBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.UserPasswordBean convertAppUserPasswordBeanToServerBean(UserPassword appBean)
    {
        com.queryclient.ws.bean.UserPasswordBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.UserPasswordBean();
            bean.setGuid(appBean.getGuid());
            bean.setManagerApp(appBean.getManagerApp());
            bean.setAppAcl(appBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertAppGaeAppStructBeanToServerBean(appBean.getGaeApp()));
            bean.setOwnerUser(appBean.getOwnerUser());
            bean.setUserAcl(appBean.getUserAcl());
            bean.setAdmin(appBean.getAdmin());
            bean.setUser(appBean.getUser());
            bean.setUsername(appBean.getUsername());
            bean.setEmail(appBean.getEmail());
            bean.setOpenId(appBean.getOpenId());
            bean.setPlainPassword(appBean.getPlainPassword());
            bean.setHashedPassword(appBean.getHashedPassword());
            bean.setSalt(appBean.getSalt());
            bean.setHashMethod(appBean.getHashMethod());
            bean.setResetRequired(appBean.isResetRequired());
            bean.setChallengeQuestion(appBean.getChallengeQuestion());
            bean.setChallengeAnswer(appBean.getChallengeAnswer());
            bean.setStatus(appBean.getStatus());
            bean.setLastResetTime(appBean.getLastResetTime());
            bean.setExpirationTime(appBean.getExpirationTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<UserPassword> convertAppUserPasswordBeanListToServerBeanList(List<UserPassword> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<UserPassword> beanList = new ArrayList<UserPassword>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(UserPassword sb : appBeanList) {
                com.queryclient.ws.bean.UserPasswordBean bean = convertAppUserPasswordBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
