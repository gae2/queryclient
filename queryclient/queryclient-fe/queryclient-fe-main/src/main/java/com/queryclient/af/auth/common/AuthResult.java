package com.queryclient.af.auth.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;


// Primarily to be used as conditions for PostAuth processing task.
public final class AuthResult
{
    private static final Logger log = Logger.getLogger(AuthResult.class.getName());

    private AuthResult() {}

    
    // Note: the first 8 bits are "reserved"
    //       App-specific bits should start from 9-th bit (index: 8).
    
    // Temporary
    public static final long BITMASK_LOGIN_SUCCESS = (1L << 0);
    public static final long BITMASK_NEW_LOGIN = (1L << 1);
    public static final long BITMASK_NEW_USER = (1L << 2);
    // etc...
    // ...

    
    // authResult: bitmask of auth result flags...
    public static boolean isLoginSuccess(long authResult)
    {
        return (authResult & BITMASK_LOGIN_SUCCESS) != 0;
    }
    public static boolean isLoginSuccessNotRequiredOrTrue(long authResult, long authResultMask)
    {
        return ( (authResultMask & BITMASK_LOGIN_SUCCESS) == 0 ||
               (authResult & BITMASK_LOGIN_SUCCESS) != 0 );
    }
    public static boolean isLoginSuccessRequiredAndTrue(long authResult, long authResultMask)
    {
        return ( (authResultMask & BITMASK_LOGIN_SUCCESS) & authResult ) != 0;
    }

    public static boolean isNewLogin(long authResult)
    {
        return (authResult & BITMASK_NEW_LOGIN) != 0;
    }
    public static boolean isNewLoginNotRequiredOrTrue(long authResult, long authResultMask)
    {
        return ( (authResultMask & BITMASK_NEW_LOGIN) == 0 ||
               (authResult & BITMASK_NEW_LOGIN) != 0 );
    }
    public static boolean isNewLoginRequiredAndTrue(long authResult, long authResultMask)
    {
        return ( (authResultMask & BITMASK_NEW_LOGIN) & authResult ) != 0;
    }

    public static boolean isNewUser(long authResult)
    {
        return (authResult & BITMASK_NEW_USER) != 0;
    }
    public static boolean isNewUserNotRequiredOrTrue(long authResult, long authResultMask)
    {
        return ( (authResultMask & BITMASK_NEW_USER) == 0 ||
               (authResult & BITMASK_NEW_USER) != 0 );
    }
    public static boolean isNewUserRequiredAndTrue(long authResult, long authResultMask)
    {
        return ( (authResultMask & BITMASK_NEW_USER) & authResult ) != 0;
    }
    
    // etc...
    // ...

}
