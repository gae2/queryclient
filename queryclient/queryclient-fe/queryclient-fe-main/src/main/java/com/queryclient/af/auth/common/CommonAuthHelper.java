package com.queryclient.af.auth.common;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.config.Config;


// Note:
public class CommonAuthHelper
{
    private static final Logger log = Logger.getLogger(CommonAuthHelper.class.getName());

    // "Lazy initialization"
    private Boolean mCustomCredentialEnabled = null;
    // ...
    

    private CommonAuthHelper() {}

    // Initialization-on-demand holder.
    private static final class CommonAuthHelperHolder
    {
        private static final CommonAuthHelper INSTANCE = new CommonAuthHelper();
    }

    // Singleton method
    public static CommonAuthHelper getInstance()
    {
        return CommonAuthHelperHolder.INSTANCE;
    }
    
    
    public boolean isCustomCredentialEnabled()
    {
        if(mCustomCredentialEnabled == null) {
            mCustomCredentialEnabled = Config.getInstance().getBoolean(CommonAuthUtil.CONFIG_KEY_CUSTOMCREDENTIAL_ENABLED, false);
        }
        return mCustomCredentialEnabled;
    }

}
