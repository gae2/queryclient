package com.queryclient.app.auth;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.af.auth.user.AuthUser;
import com.queryclient.af.bean.GaeUserStructBean;
import com.queryclient.af.bean.UserBean;
import com.queryclient.app.service.UserAppService;
import com.queryclient.ws.BaseException;
import com.queryclient.ws.User;
import com.queryclient.ws.core.GUID;


public class AuthUserHelper
{
    private static final Logger log = Logger.getLogger(AuthUserHelper.class.getName());

    // ...
    private UserAppService userAppService = null;

    private AuthUserHelper() {}

    // Initialization-on-demand holder.
    private static final class AuthUserHelperHolder
    {
        private static final AuthUserHelper INSTANCE = new AuthUserHelper();
    }

    // Singleton method
    public static AuthUserHelper getInstance()
    {
        return AuthUserHelperHolder.INSTANCE;
    }
   
    
    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }

    
    public UserBean getUser(String guid)
    {
        UserBean user = null;
        try {
            user = (UserBean) getUserAppService().getUser(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to get the user.", e);
        }
        return user;
    }

    public String createUser(UserBean userBean)
    {
        String userGuid = null;
        try {
            userGuid = getUserAppService().createUser(userBean);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to create the user.", e);
        }
        return userGuid;
    }
    public UserBean constructUser(UserBean userBean)
    {
        try {
            userBean = (UserBean) getUserAppService().constructUser(userBean);
        } catch (BaseException e) {
            userBean = null;
            log.log(Level.WARNING, "Failed to construct the user.", e);
        }
        return userBean;
    }

    public Boolean updateUser(UserBean userBean)
    {
        Boolean suc = false;
        try {
            suc = getUserAppService().updateUser(userBean);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to update the user.", e);
        }
        return suc;
    }
    public UserBean refreshUser(UserBean userBean)
    {
        try {
            userBean = (UserBean) getUserAppService().refreshUser(userBean);
        } catch (BaseException e) {
            userBean = null;
            log.log(Level.WARNING, "Failed to refresh the user.", e);
        }
        return userBean;
    }

    public UserBean constructUser(String sessionGuid, String userGuid)
    {
        // Create a user object,
        UserBean userBean = new UserBean();
        if(userGuid == null) {
            userGuid = GUID.generate();
        }
        userBean.setGuid(userGuid);
        userBean.setSessionId(sessionGuid);
        try {
            userBean = (UserBean) getUserAppService().constructUser(userBean);  // TBD: Validate the result?
            log.info("New user object created. userGuid = " + userGuid);
        } catch (BaseException e) {
            userBean = null;
            log.log(Level.WARNING, "Failed to save a user object.", e);
        }
        return userBean;
    }

    public String createUser(String sessionGuid, String userGuid)
    {
        // Create a user object,
        UserBean userBean = new UserBean();
        if(userGuid == null) {
            userGuid = GUID.generate();
        }
        userBean.setGuid(userGuid);
        userBean.setSessionId(sessionGuid);
        try {
            userGuid = getUserAppService().createUser(userBean);  // TBD: Validate the result?
            log.info("New user object created. userGuid = " + userGuid);
        } catch (BaseException e) {
            userGuid = null;
            log.log(Level.WARNING, "Failed to save a user object.", e);
        }
        return userGuid;
    }

    
    public Boolean createOrUpdateUserBean(String currentUserGuid, String currentSessionId, AuthUser authUser)
    {
        Boolean created = null;
        
        // TBD: Move this to a util class.....
        GaeUserStructBean gaeUser = new GaeUserStructBean();
        gaeUser.setFederatedIdentity(authUser.getFederatedIdentity());
        gaeUser.setAuthDomain(authUser.getAuthDomain());
        gaeUser.setEmail(authUser.getEmail());
        gaeUser.setNickname(authUser.getNickname());
        gaeUser.setUserId(authUser.getUserId());
        // etc. ???
        
        UserBean userBean = getUser(currentUserGuid);
        if(userBean == null) {
            log.info("Failed to find the user with currentUserGuid = " + currentUserGuid);

            userBean = new UserBean();
            userBean.setGuid(currentUserGuid);
            userBean.setSessionId(currentSessionId);
            userBean.setGaeUser(gaeUser);                            
            String newUserGuid = createUser(userBean);
            if(newUserGuid != null) {
                created = true;
            }
        } else {
            userBean.setGaeUser(gaeUser);                            
            userBean.setSessionId(currentSessionId);
            Boolean suc = updateUser(userBean);
            if(suc) {
                created = false;
            }
        }

        return created;
    }

    public Boolean updateUserBeanIfNecessary(String currentUserGuid, String currentSessionId)
    {
        Boolean updated = null;

        UserBean userBean = getUser(currentUserGuid);
        if(userBean != null) {
            String userSessionId = userBean.getSessionId();
            if(! currentSessionId.equals(userSessionId)) {  // Probably from different browsers/devices, etc...
                userBean.setSessionId(currentSessionId);
                updated = updateUser(userBean);
            } else {
                // nothing to do...
                updated = false;
            }
        } else {
            // This cannot happen!!!
            log.info("User not found for currentUserGuid = " + currentUserGuid);
        }

        return updated;
    }


    public UserBean findUserByEmail(String email)
    {
        UserBean user = null;
        try {
            List<User> beans = null;
            String filter = "email=='" + email + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 2;
            beans = getUserAppService().findUsers(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && beans.size() > 0) {
                user = (UserBean) beans.get(0);
                // For debugging/diagnostic purposes.
                if(beans.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one user with the same email = " + email + ". Needs further investigation!");
                }
            } else {
                log.log(Level.SEVERE, "User does not exist with given email = " + email);                
            }

        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to get the user.", e);
        }
        return user;
    }
    
    public UserBean findUserByGaeNicknameAndAuthDomain(String gaeOpenId, String authDomain)
    {
        UserBean user = null;
        try {
            List<User> beans = null;
            String filter = "gaeUser.nickname=='" + gaeOpenId + "'";
            if(authDomain != null && !authDomain.isEmpty()) {
                filter += " && gaeUser.authDomain=='" + authDomain + "'";
            }
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 2;
            beans = getUserAppService().findUsers(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && beans.size() > 0) {
                user = (UserBean) beans.get(0);
                // For debugging/diagnostic purposes.
                if(beans.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one user with the same gaeOpenId = " + gaeOpenId + ". Needs further investigation!");
                }
            } else {
                log.log(Level.SEVERE, "User does not exist with given gaeOpenId = " + gaeOpenId);                
            }

        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to get the user.", e);
        }
        return user;
    }
    
    public UserBean findUserByGaeNickname(String gaeOpenId)
    {
        return findUserByGaeNicknameAndAuthDomain(gaeOpenId, null);
    }

    public UserBean findUserByGaeUserId(String gaeUserId)
    {
        UserBean user = null;
        try {
            List<User> beans = null;
            String filter = "gaeUser.userId=='" + gaeUserId + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 2;
            beans = getUserAppService().findUsers(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && beans.size() > 0) {
                user = (UserBean) beans.get(0);
                // For debugging/diagnostic purposes.
                if(beans.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one user with the same gaeUserId = " + gaeUserId + ". Needs further investigation!");
                }
            } else {
                log.log(Level.SEVERE, "User does not exist with given gaeUserId = " + gaeUserId);                
            }

        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to get the user.", e);
        }
        return user;
    }

    public UserBean findUserByGaeEmail(String gaeEmail)
    {
        UserBean user = null;
        try {
            List<User> beans = null;
            String filter = "gaeUser.email=='" + gaeEmail + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 2;
            beans = getUserAppService().findUsers(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && beans.size() > 0) {
                user = (UserBean) beans.get(0);
                // For debugging/diagnostic purposes.
                if(beans.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one user with the same gaeEmail = " + gaeEmail + ". Needs further investigation!");
                }
            } else {
                log.log(Level.SEVERE, "User does not exist with given gaeEmail = " + gaeEmail);                
            }

        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to get the user.", e);
        }
        return user;
    }
    
    public UserBean findUserByAncestorGuid(String ancestorGuid)
    {
        UserBean user = null;
        try {
            List<User> beans = null;
            String filter = "ancestorGuid=='" + ancestorGuid + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 2;
            beans = getUserAppService().findUsers(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && beans.size() > 0) {
                user = (UserBean) beans.get(0);
                // For debugging/diagnostic purposes.
                if(beans.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one user with the same ancestorGuid = " + ancestorGuid + ". Needs further investigation!");
                }
            } else {
                log.log(Level.SEVERE, "User does not exist with given ancestorGuid = " + ancestorGuid);                
            }

        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to get the user.", e);
        }
        return user;
    }

    
    
}
