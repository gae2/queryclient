package com.queryclient.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.QueryRecordJsBean;
import com.queryclient.wa.service.UserWebService;
import com.queryclient.wa.service.QueryRecordWebService;


public class SitemapHelper
{
    private static final Logger log = Logger.getLogger(SitemapHelper.class.getName());
    
    // temporary
    private static final int MAX_MEMO_COUNT = 5000;   // Sitemap.xml max count...
    private static final int DEFAULT_MAX_MEMO_COUNT = 2500;
    // temporary

    private UserWebService userWebService = null;
    private QueryRecordWebService queryRecordService = null;
    // ...

    private SitemapHelper() {}

    // Initialization-on-demand holder.
    private static final class SitemapHelperHolder
    {
        private static final SitemapHelper INSTANCE = new SitemapHelper();
    }

    // Singleton method
    public static SitemapHelper getInstance()
    {
        return SitemapHelperHolder.INSTANCE;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private QueryRecordWebService getQueryRecordWebService()
    {
        if(queryRecordService == null) {
            queryRecordService = new QueryRecordWebService();
        }
        return queryRecordService;
    }

    public List<QueryRecordJsBean> findRecentQueryRecords()
    {
        return findRecentQueryRecords(DEFAULT_MAX_MEMO_COUNT);
    }

    public List<QueryRecordJsBean> findRecentQueryRecords(int maxCount)
    {
        List<QueryRecordJsBean> queryRecords = null;
        try {
            String filter = null;
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = maxCount;   // TBD: Validation??? ( maxCount < MAX_MEMO_COUNT ???? )
            
            // Restore this later....
            //queryRecords = getQueryRecordWebService().findQueryRecords(filter, ordering, null, null, null, null, offset, count);
            
            // temporary
            // this is needed because we changed the permalink generation algorithm...
            // this is only temporary.... (Note: some filtering might still be necessary...)
            List<QueryRecordJsBean> list = getQueryRecordWebService().findQueryRecords(filter, ordering, null, null, null, null, offset, count);
            if(list != null) {    // && ! list.isEmpty()) {
                queryRecords = new ArrayList<QueryRecordJsBean>();
                for(QueryRecordJsBean m : list) {
                    queryRecords.add(m);
                }
            }
            // temporary

        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find queryRecords.", e);
            return null;
        }

        return queryRecords;
    }

    
    // Format the timestamp to W3C date format: "yyyy-mm-dd".
    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date(time));
        return date;
    }
    
}
