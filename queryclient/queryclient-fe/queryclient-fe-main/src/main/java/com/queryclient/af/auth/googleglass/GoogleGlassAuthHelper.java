package com.queryclient.af.auth.googleglass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.queryclient.af.config.Config;
import com.queryclient.af.util.URLUtil;
import com.queryclient.af.util.CsrfHelper;
import com.queryclient.af.auth.common.CommonAuthUtil;


// Note:
public class GoogleGlassAuthHelper
{
    private static final Logger log = Logger.getLogger(GoogleGlassAuthHelper.class.getName());

    // "Lazy initialization"
    private String mApplicationName = null;
    private String mClientId = null;
    private String mClientSecret = null;
    private String mCallbackTokenHandlerUrlPath = null;
    private String mCallbackAuthAjaxUrlPath = null;
    private String mOAuthRedirectUrlPath = null;
    private String mJavascriptSignInCallback = null;
    // private String mDefaultSignInFormId = null;
    private List<String> mDefaultScopeList = null;


    private GoogleGlassAuthHelper() {}

    // Initialization-on-demand holder.
    private static final class GoogleGlassAuthHelperHolder
    {
        private static final GoogleGlassAuthHelper INSTANCE = new GoogleGlassAuthHelper();
    }

    // Singleton method
    public static GoogleGlassAuthHelper getInstance()
    {
        return GoogleGlassAuthHelperHolder.INSTANCE;
    }
    
    
    public String getApplicationName()
    {
        if(mApplicationName == null) {
            mApplicationName = Config.getInstance().getString(GoogleGlassAuthUtil.CONFIG_KEY_APPLICATION_NAME);
        }
        return mApplicationName;
    }
    public String getClientId()
    {
        if(mClientId == null) {
            mClientId = Config.getInstance().getString(GoogleGlassAuthUtil.CONFIG_KEY_CLIENT_ID);
        }
        return mClientId;
    }
    public String getClientSecret()
    {
        if(mClientSecret == null) {
            mClientSecret = Config.getInstance().getString(GoogleGlassAuthUtil.CONFIG_KEY_CLIENT_SECRET);
        }
        return mClientSecret;
    }

    private String getCallbackTokenHandlerUrlPath()
    {
        if(mCallbackTokenHandlerUrlPath == null) {
            mCallbackTokenHandlerUrlPath = Config.getInstance().getString(GoogleGlassAuthUtil.CONFIG_KEY_CALLBACK_TOKENHANDLER_URLPATH);
        }
        return mCallbackTokenHandlerUrlPath;
    }
    public String getCallbackTokenHandlerUri(String topLevelUrl)
    {
        return CommonAuthUtil.constructUrl(topLevelUrl, getCallbackTokenHandlerUrlPath());
    }
    private String getCallbackAuthAjaxUrlPath()
    {
        if(mCallbackAuthAjaxUrlPath == null) {
            mCallbackAuthAjaxUrlPath = Config.getInstance().getString(GoogleGlassAuthUtil.CONFIG_KEY_CALLBACK_AUTHAJAX_URLPATH);
        }
        return mCallbackAuthAjaxUrlPath;
    }
    public String getCallbackAuthAjaxUri(String topLevelUrl)
    {
        return CommonAuthUtil.constructUrl(topLevelUrl, getCallbackAuthAjaxUrlPath());
    }

    private String getOAuthRedirectUrlPath()
    {
        if(mOAuthRedirectUrlPath == null) {
            mOAuthRedirectUrlPath = Config.getInstance().getString(GoogleGlassAuthUtil.CONFIG_KEY_OAUTH_REDIRECTURLPATH);
        }
        return mOAuthRedirectUrlPath;
    }
    public String getDefaultOAuthRedirectUri(String topLevelUrl)
    {
        return CommonAuthUtil.constructUrl(topLevelUrl, getOAuthRedirectUrlPath());
    }

    public String getJavascriptSignInCallback()
    {
        if(mJavascriptSignInCallback == null) {
            mJavascriptSignInCallback = Config.getInstance().getString(GoogleGlassAuthUtil.CONFIG_KEY_JAVASCRIPT_SIGNIN_CALLBACK);
        }
        return mJavascriptSignInCallback;
    }

    public String getDefaultSignInFormId()
    {
        return GoogleGlassAuthUtil.DEFAULT_FORM_ID_GOOGLEGLASS_SIGNIN;
    }


    // Returns the csrf in the current session or the generated csrf state string.
    public String setCsrfStateSessionAttribute(HttpSession session)
    {
        return setCsrfStateSessionAttribute(session, null);
    }
    public String setCsrfStateSessionAttribute(HttpSession session, String formId)
    {
        if(session == null) {
            // ????
            return null;
        }
        if(formId == null || formId.isEmpty()) {
            formId = GoogleGlassAuthUtil.getDefaultGoogleGlassSignInFormId();
        }
        String csrfState = CsrfHelper.getInstance().setCsrfStateSessionAttribute(session, null, formId);
//        String csrfState = CsrfHelper.getInstance().getCsrfStateFromSession(session, formId);
//        if(csrfState != null && !csrfState.isEmpty()) {
//            if(log.isLoggable(Level.FINE)) log.fine("csrfState found in the current session: " + csrfState);
//            return csrfState;
//        }
//        csrfState = CsrfHelper.getInstance().generateRandomCsrfState();
//        csrfState = CsrfHelper.getInstance().setCsrfStateSessionAttribute(session, csrfState, formId);
        return csrfState;
    }


    public boolean verifyCsrfStateFromRequest(HttpServletRequest request)
    {
        if(request == null) {
            // ????
            return false;
        }
        String formId = GoogleGlassAuthUtil.getDefaultGoogleGlassSignInFormId();
        boolean isVerified = CsrfHelper.getInstance().verifyCsrfState(request, null, formId);
        return isVerified;
    }


    public List<String> getDefaultScopeList()
    {
        if(mDefaultScopeList == null) {
            mDefaultScopeList = new ArrayList<String>();
            String scopes = Config.getInstance().getString(GoogleGlassAuthUtil.CONFIG_KEY_OAUTH_DEFAULT_SCOPES);
            if(scopes != null && !scopes.isEmpty()) {
                String[] scopeArr = scopes.split("\\s*,\\s*");
                mDefaultScopeList.addAll(Arrays.asList(scopeArr));
            }
        }
        return mDefaultScopeList;
    }


}
