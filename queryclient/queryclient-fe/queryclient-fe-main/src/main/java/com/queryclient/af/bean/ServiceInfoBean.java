package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ServiceInfo;
import com.queryclient.ws.stub.ServiceInfoStub;


// Wrapper class + bean combo.
public class ServiceInfoBean implements ServiceInfo, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ServiceInfoBean.class.getName());

    // [1] With an embedded object.
    private ServiceInfoStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String title;
    private String content;
    private String type;
    private String status;
    private Long scheduledTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ServiceInfoBean()
    {
        //this((String) null);
    }
    public ServiceInfoBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null);
    }
    public ServiceInfoBean(String guid, String title, String content, String type, String status, Long scheduledTime)
    {
        this(guid, title, content, type, status, scheduledTime, null, null);
    }
    public ServiceInfoBean(String guid, String title, String content, String type, String status, Long scheduledTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.title = title;
        this.content = content;
        this.type = type;
        this.status = status;
        this.scheduledTime = scheduledTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ServiceInfoBean(ServiceInfo stub)
    {
        if(stub instanceof ServiceInfoStub) {
            this.stub = (ServiceInfoStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setTitle(stub.getTitle());   
            setContent(stub.getContent());   
            setType(stub.getType());   
            setStatus(stub.getStatus());   
            setScheduledTime(stub.getScheduledTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getContent()
    {
        if(getStub() != null) {
            return getStub().getContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.content;
        }
    }
    public void setContent(String content)
    {
        if(getStub() != null) {
            getStub().setContent(content);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.content = content;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getScheduledTime()
    {
        if(getStub() != null) {
            return getStub().getScheduledTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.scheduledTime;
        }
    }
    public void setScheduledTime(Long scheduledTime)
    {
        if(getStub() != null) {
            getStub().setScheduledTime(scheduledTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.scheduledTime = scheduledTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public ServiceInfoStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ServiceInfoStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("content = " + this.content).append(";");
            sb.append("type = " + this.type).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("scheduledTime = " + this.scheduledTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = content == null ? 0 : content.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = scheduledTime == null ? 0 : scheduledTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
