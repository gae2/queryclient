package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
// import com.queryclient.ws.bean.DataServiceBean;
import com.queryclient.ws.service.DataServiceService;
import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.af.proxy.DataServiceServiceProxy;
import com.queryclient.af.proxy.util.ConsumerKeySecretPairProxyUtil;
import com.queryclient.af.proxy.util.ReferrerInfoStructProxyUtil;


public class LocalDataServiceServiceProxy extends BaseLocalServiceProxy implements DataServiceServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalDataServiceServiceProxy.class.getName());

    public LocalDataServiceServiceProxy()
    {
    }

    @Override
    public DataService getDataService(String guid) throws BaseException
    {
        DataService serverBean = getDataServiceService().getDataService(guid);
        DataService appBean = convertServerDataServiceBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getDataService(String guid, String field) throws BaseException
    {
        return getDataServiceService().getDataService(guid, field);       
    }

    @Override
    public List<DataService> getDataServices(List<String> guids) throws BaseException
    {
        List<DataService> serverBeanList = getDataServiceService().getDataServices(guids);
        List<DataService> appBeanList = convertServerDataServiceBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<DataService> getAllDataServices() throws BaseException
    {
        return getAllDataServices(null, null, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getDataServiceService().getAllDataServices(ordering, offset, count);
        return getAllDataServices(ordering, offset, count, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<DataService> serverBeanList = getDataServiceService().getAllDataServices(ordering, offset, count, forwardCursor);
        List<DataService> appBeanList = convertServerDataServiceBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getDataServiceService().getAllDataServiceKeys(ordering, offset, count);
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDataServiceService().getAllDataServiceKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getDataServiceService().findDataServices(filter, ordering, params, values, grouping, unique, offset, count);
        return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<DataService> serverBeanList = getDataServiceService().findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<DataService> appBeanList = convertServerDataServiceBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getDataServiceService().findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDataServiceService().findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getDataServiceService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDataService(String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        return getDataServiceService().createDataService(user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status);
    }

    @Override
    public String createDataService(DataService dataService) throws BaseException
    {
        com.queryclient.ws.bean.DataServiceBean serverBean =  convertAppDataServiceBeanToServerBean(dataService);
        return getDataServiceService().createDataService(serverBean);
    }

    @Override
    public Boolean updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        return getDataServiceService().updateDataService(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status);
    }

    @Override
    public Boolean updateDataService(DataService dataService) throws BaseException
    {
        com.queryclient.ws.bean.DataServiceBean serverBean =  convertAppDataServiceBeanToServerBean(dataService);
        return getDataServiceService().updateDataService(serverBean);
    }

    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        return getDataServiceService().deleteDataService(guid);
    }

    @Override
    public Boolean deleteDataService(DataService dataService) throws BaseException
    {
        com.queryclient.ws.bean.DataServiceBean serverBean =  convertAppDataServiceBeanToServerBean(dataService);
        return getDataServiceService().deleteDataService(serverBean);
    }

    @Override
    public Long deleteDataServices(String filter, String params, List<String> values) throws BaseException
    {
        return getDataServiceService().deleteDataServices(filter, params, values);
    }




    public static DataServiceBean convertServerDataServiceBeanToAppBean(DataService serverBean)
    {
        DataServiceBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new DataServiceBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setName(serverBean.getName());
            bean.setDescription(serverBean.getDescription());
            bean.setType(serverBean.getType());
            bean.setMode(serverBean.getMode());
            bean.setServiceUrl(serverBean.getServiceUrl());
            bean.setAuthRequired(serverBean.isAuthRequired());
            bean.setAuthCredential(ConsumerKeySecretPairProxyUtil.convertServerConsumerKeySecretPairBeanToAppBean(serverBean.getAuthCredential()));
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertServerReferrerInfoStructBeanToAppBean(serverBean.getReferrerInfo()));
            bean.setStatus(serverBean.getStatus());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<DataService> convertServerDataServiceBeanListToAppBeanList(List<DataService> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<DataService> beanList = new ArrayList<DataService>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(DataService sb : serverBeanList) {
                DataServiceBean bean = convertServerDataServiceBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.DataServiceBean convertAppDataServiceBeanToServerBean(DataService appBean)
    {
        com.queryclient.ws.bean.DataServiceBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.DataServiceBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setName(appBean.getName());
            bean.setDescription(appBean.getDescription());
            bean.setType(appBean.getType());
            bean.setMode(appBean.getMode());
            bean.setServiceUrl(appBean.getServiceUrl());
            bean.setAuthRequired(appBean.isAuthRequired());
            bean.setAuthCredential(ConsumerKeySecretPairProxyUtil.convertAppConsumerKeySecretPairBeanToServerBean(appBean.getAuthCredential()));
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertAppReferrerInfoStructBeanToServerBean(appBean.getReferrerInfo()));
            bean.setStatus(appBean.getStatus());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<DataService> convertAppDataServiceBeanListToServerBeanList(List<DataService> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<DataService> beanList = new ArrayList<DataService>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(DataService sb : appBeanList) {
                com.queryclient.ws.bean.DataServiceBean bean = convertAppDataServiceBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
