package com.queryclient.app.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.DeferredTask;
import com.queryclient.ws.BaseException;


public class QueryTask implements DeferredTask
{
    private static final Logger log = Logger.getLogger(QueryTask.class.getName());

    private String queryGuid = null;
    private URL requestUrl = null;
    private String httpMethod = null;
    private Map<String, String> httpHeaders = null;
    private String payload = null;

    public QueryTask(String queryGuid, URL requestUrl, String httpMethod,
            Map<String, String> httpHeaders, String payload)
    {
        this.queryGuid = queryGuid;
        this.requestUrl = requestUrl;
        this.httpMethod = httpMethod;
        this.httpHeaders = httpHeaders;
        this.payload = payload;
    }
    
    @Override
    public void run()
    {
        log.log(Level.INFO, "QueryTask.run() called: Task = " + this.toString());
        
        try {
            QueryResultStruct res = QueryUtil.processWebServiceQuery(queryGuid, requestUrl, httpMethod, httpHeaders, payload);
            int responseCode = res.getResponseCode();
            String result = res.getResult();
            String output = res.getOutput();
            // ...

        } catch (BaseException e) {
            log.log(Level.WARNING, "Could not process the task for queryGuid = " + queryGuid, e);
            // What to do???
        }

        
        // TBD:
        // find the queryRecord with queryGuid
        // and, update it.
        // ...
        

//            // Store the result
//            try {
//                OutputRecordWebService outputRecordService = new OutputRecordWebService();
//                OutputRecordJsBean outputRecordBean = new OutputRecordJsBean();
//                outputRecordBean.setQuery(queryGuid);
//                outputRecordBean.setResult(result + responseCode);
//                if(output != null) {
//                    outputRecordBean.setOutput(output);
//                }
//                // ...
//                String outputRecordGuid = outputRecordService.createOutputRecord(outputRecordBean);
//                log.info("Output record has been sucessfully stored: outputRecordGuid = " + outputRecordGuid);
//            } catch (WebException e) {
//                log.log(Level.WARNING, "Error storing the output record", e);
//            }
        
        // TBD:
        // When responseCode == 202
        // Fetch the output through GET ????
        // ....
    }

    @Override
    public String toString()
    {
        return "QueryTask [queryGuid=" + queryGuid + ", requestUrl="
                + requestUrl + ", httpMethod=" + httpMethod
                + ", httpHeaders=" + httpHeaders + ", payload=" + payload
                + "]";
    }
    
}