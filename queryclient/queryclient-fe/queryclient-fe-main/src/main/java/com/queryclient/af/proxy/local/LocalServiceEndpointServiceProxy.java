package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
// import com.queryclient.ws.bean.ServiceEndpointBean;
import com.queryclient.ws.service.ServiceEndpointService;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.proxy.ServiceEndpointServiceProxy;
import com.queryclient.af.proxy.util.ConsumerKeySecretPairProxyUtil;


public class LocalServiceEndpointServiceProxy extends BaseLocalServiceProxy implements ServiceEndpointServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalServiceEndpointServiceProxy.class.getName());

    public LocalServiceEndpointServiceProxy()
    {
    }

    @Override
    public ServiceEndpoint getServiceEndpoint(String guid) throws BaseException
    {
        ServiceEndpoint serverBean = getServiceEndpointService().getServiceEndpoint(guid);
        ServiceEndpoint appBean = convertServerServiceEndpointBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getServiceEndpoint(String guid, String field) throws BaseException
    {
        return getServiceEndpointService().getServiceEndpoint(guid, field);       
    }

    @Override
    public List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException
    {
        List<ServiceEndpoint> serverBeanList = getServiceEndpointService().getServiceEndpoints(guids);
        List<ServiceEndpoint> appBeanList = convertServerServiceEndpointBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException
    {
        return getAllServiceEndpoints(null, null, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getServiceEndpointService().getAllServiceEndpoints(ordering, offset, count);
        return getAllServiceEndpoints(ordering, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ServiceEndpoint> serverBeanList = getServiceEndpointService().getAllServiceEndpoints(ordering, offset, count, forwardCursor);
        List<ServiceEndpoint> appBeanList = convertServerServiceEndpointBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getServiceEndpointService().getAllServiceEndpointKeys(ordering, offset, count);
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getServiceEndpointService().getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getServiceEndpointService().findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count);
        return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ServiceEndpoint> serverBeanList = getServiceEndpointService().findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<ServiceEndpoint> appBeanList = convertServerServiceEndpointBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getServiceEndpointService().findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getServiceEndpointService().findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getServiceEndpointService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createServiceEndpoint(String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        return getServiceEndpointService().createServiceEndpoint(user, dataService, serviceName, serviceUrl, authRequired, authCredential, status);
    }

    @Override
    public String createServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        com.queryclient.ws.bean.ServiceEndpointBean serverBean =  convertAppServiceEndpointBeanToServerBean(serviceEndpoint);
        return getServiceEndpointService().createServiceEndpoint(serverBean);
    }

    @Override
    public Boolean updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        return getServiceEndpointService().updateServiceEndpoint(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredential, status);
    }

    @Override
    public Boolean updateServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        com.queryclient.ws.bean.ServiceEndpointBean serverBean =  convertAppServiceEndpointBeanToServerBean(serviceEndpoint);
        return getServiceEndpointService().updateServiceEndpoint(serverBean);
    }

    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        return getServiceEndpointService().deleteServiceEndpoint(guid);
    }

    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpoint serviceEndpoint) throws BaseException
    {
        com.queryclient.ws.bean.ServiceEndpointBean serverBean =  convertAppServiceEndpointBeanToServerBean(serviceEndpoint);
        return getServiceEndpointService().deleteServiceEndpoint(serverBean);
    }

    @Override
    public Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException
    {
        return getServiceEndpointService().deleteServiceEndpoints(filter, params, values);
    }




    public static ServiceEndpointBean convertServerServiceEndpointBeanToAppBean(ServiceEndpoint serverBean)
    {
        ServiceEndpointBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new ServiceEndpointBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setDataService(serverBean.getDataService());
            bean.setServiceName(serverBean.getServiceName());
            bean.setServiceUrl(serverBean.getServiceUrl());
            bean.setAuthRequired(serverBean.isAuthRequired());
            bean.setAuthCredential(ConsumerKeySecretPairProxyUtil.convertServerConsumerKeySecretPairBeanToAppBean(serverBean.getAuthCredential()));
            bean.setStatus(serverBean.getStatus());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ServiceEndpoint> convertServerServiceEndpointBeanListToAppBeanList(List<ServiceEndpoint> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<ServiceEndpoint> beanList = new ArrayList<ServiceEndpoint>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(ServiceEndpoint sb : serverBeanList) {
                ServiceEndpointBean bean = convertServerServiceEndpointBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.ServiceEndpointBean convertAppServiceEndpointBeanToServerBean(ServiceEndpoint appBean)
    {
        com.queryclient.ws.bean.ServiceEndpointBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.ServiceEndpointBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setDataService(appBean.getDataService());
            bean.setServiceName(appBean.getServiceName());
            bean.setServiceUrl(appBean.getServiceUrl());
            bean.setAuthRequired(appBean.isAuthRequired());
            bean.setAuthCredential(ConsumerKeySecretPairProxyUtil.convertAppConsumerKeySecretPairBeanToServerBean(appBean.getAuthCredential()));
            bean.setStatus(appBean.getStatus());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ServiceEndpoint> convertAppServiceEndpointBeanListToServerBeanList(List<ServiceEndpoint> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<ServiceEndpoint> beanList = new ArrayList<ServiceEndpoint>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(ServiceEndpoint sb : appBeanList) {
                com.queryclient.ws.bean.ServiceEndpointBean bean = convertAppServiceEndpointBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
