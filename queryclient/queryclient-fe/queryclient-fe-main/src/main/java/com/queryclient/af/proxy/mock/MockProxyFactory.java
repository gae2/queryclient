package com.queryclient.af.proxy.mock;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.ApiConsumerServiceProxy;
import com.queryclient.af.proxy.UserServiceProxy;
import com.queryclient.af.proxy.UserPasswordServiceProxy;
import com.queryclient.af.proxy.ExternalUserAuthServiceProxy;
import com.queryclient.af.proxy.UserAuthStateServiceProxy;
import com.queryclient.af.proxy.DataServiceServiceProxy;
import com.queryclient.af.proxy.ServiceEndpointServiceProxy;
import com.queryclient.af.proxy.QuerySessionServiceProxy;
import com.queryclient.af.proxy.QueryRecordServiceProxy;
import com.queryclient.af.proxy.DummyEntityServiceProxy;
import com.queryclient.af.proxy.ServiceInfoServiceProxy;
import com.queryclient.af.proxy.FiveTenServiceProxy;


// Create your own mock object factory using MockProxyFactory as a template.
public class MockProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(MockProxyFactory.class.getName());

    // Using the Decorator pattern.
    private AbstractProxyFactory decoratedProxyFactory;
    private MockProxyFactory()
    {
        this(null);   // ????
    }
    private MockProxyFactory(AbstractProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }

    // Initialization-on-demand holder.
    private static class MockProxyFactoryHolder
    {
        private static final MockProxyFactory INSTANCE = new MockProxyFactory();
    }

    // Singleton method
    public static MockProxyFactory getInstance()
    {
        return MockProxyFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public AbstractProxyFactory getDecoratedProxyFactory()
    {
        return decoratedProxyFactory;
    }
    public void setDecoratedProxyFactory(AbstractProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }


    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new MockApiConsumerServiceProxy(decoratedProxyFactory.getApiConsumerServiceProxy()) {};
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new MockUserServiceProxy(decoratedProxyFactory.getUserServiceProxy()) {};
    }

    @Override
    public UserPasswordServiceProxy getUserPasswordServiceProxy()
    {
        return new MockUserPasswordServiceProxy(decoratedProxyFactory.getUserPasswordServiceProxy()) {};
    }

    @Override
    public ExternalUserAuthServiceProxy getExternalUserAuthServiceProxy()
    {
        return new MockExternalUserAuthServiceProxy(decoratedProxyFactory.getExternalUserAuthServiceProxy()) {};
    }

    @Override
    public UserAuthStateServiceProxy getUserAuthStateServiceProxy()
    {
        return new MockUserAuthStateServiceProxy(decoratedProxyFactory.getUserAuthStateServiceProxy()) {};
    }

    @Override
    public DataServiceServiceProxy getDataServiceServiceProxy()
    {
        return new MockDataServiceServiceProxy(decoratedProxyFactory.getDataServiceServiceProxy()) {};
    }

    @Override
    public ServiceEndpointServiceProxy getServiceEndpointServiceProxy()
    {
        return new MockServiceEndpointServiceProxy(decoratedProxyFactory.getServiceEndpointServiceProxy()) {};
    }

    @Override
    public QuerySessionServiceProxy getQuerySessionServiceProxy()
    {
        return new MockQuerySessionServiceProxy(decoratedProxyFactory.getQuerySessionServiceProxy()) {};
    }

    @Override
    public QueryRecordServiceProxy getQueryRecordServiceProxy()
    {
        return new MockQueryRecordServiceProxy(decoratedProxyFactory.getQueryRecordServiceProxy()) {};
    }

    @Override
    public DummyEntityServiceProxy getDummyEntityServiceProxy()
    {
        return new MockDummyEntityServiceProxy(decoratedProxyFactory.getDummyEntityServiceProxy()) {};
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new MockServiceInfoServiceProxy(decoratedProxyFactory.getServiceInfoServiceProxy()) {};
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new MockFiveTenServiceProxy(decoratedProxyFactory.getFiveTenServiceProxy()) {};
    }

}
