package com.queryclient.af.auth.googleoauth;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.auth.common.OAuthConstants;
import com.queryclient.af.config.Config;


public final class GoogleOAuthAuthUtil
{
    private static final Logger log = Logger.getLogger(GoogleOAuthAuthUtil.class.getName());

    // Constants

    public static final String CONFIG_KEY_APPLICATION_NAME = "queryclientapp.googleoauth.application.name";
    public static final String CONFIG_KEY_CLIENT_ID = "queryclientapp.googleoauth.clientid";
    public static final String CONFIG_KEY_CLIENT_SECRET = "queryclientapp.googleoauth.clientsecret";

    // Note: only one and the same callback url can be used during Google OAuth2 authentication...
    // public static final String CONFIG_KEY_CALLBACK_RESPONSEHANDLER_URLPATH = "queryclientapp.googleoauth.callback.responsehandler.urlpath";   // Default "parent" path for all three handler urls.
    public static final String CONFIG_KEY_CALLBACK_AUTHCODE_HANDLER_URLPATH = "queryclientapp.googleoauth.callback.authcodehandler.urlpath";
    // public static final String CONFIG_KEY_CALLBACK_REFRESHTOKEN_HANDLER_URLPATH = "queryclientapp.googleoauth.callback.refrehtokenhandler.urlpath";
    // ...

    // space-delimited list
    public static final String CONFIG_KEY_OAUTH_SCOPE = "queryclientapp.googleoauth.data.scope";
    public static final String CONFIG_KEY_OAUTH_SCOPE_INCLUDE_EMAIL = "queryclientapp.googleoauth.data.scope.includeemail";
    public static final String CONFIG_KEY_OAUTH_CUSTOM_SCOPE_PREFIX = "queryclientapp.googleoauth.data.scope.";
    // ...

    // Not being used...
    public static final String PARAM_CALLBACK = OAuthConstants.PARAM_OAUTH_CALLBACK;
    public static final String PARAM_CONSUMER_KEY = OAuthConstants.PARAM_OAUTH_CONSUMER_KEY;
    public static final String PARAM_NONCE = OAuthConstants.PARAM_OAUTH_NONCE;
    public static final String PARAM_SIGNATURE = OAuthConstants.PARAM_OAUTH_SIGNATURE;
    public static final String PARAM_SIGNATURE_METHOD = OAuthConstants.PARAM_OAUTH_SIGNATURE_METHOD;
    public static final String PARAM_TIMESTAMP = OAuthConstants.PARAM_OAUTH_TIMESTAMP;
    public static final String PARAM_VERSION = OAuthConstants.PARAM_OAUTH_VERSION;
    public static final String PARAM_REQUEST_TOKEN = OAuthConstants.PARAM_OAUTH_REQUEST_TOKEN;
    public static final String PARAM_TOKEN_VERIFIER = OAuthConstants.PARAM_OAUTH_TOKEN_VERIFIER;

    public static final String PARAM_RESPONSE_TYPE = "response_type";
    public static final String PARAM_CLIENT_ID = "client_id";
    public static final String PARAM_CLIENT_SECRET = "client_secret";
    public static final String PARAM_REDIRECT_URI = "redirect_uri";
    // public static final String PARAM_APPLICATION_NAME = "application_name";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_SCOPE = "scope";
    public static final String PARAM_ACCESS_TYPE = "access_type";
    public static final String PARAM_APPROVAL_PROMPT = "approval_prompt";
    public static final String PARAM_LOGIN_HINT = "login_hint";
    // ...
    // For response fields...
    public static final String PARAM_CODE = "code";
    public static final String PARAM_TOKEN = "token";
    public static final String PARAM_ACCESS_TOKEN = "access_token";
    public static final String PARAM_REFRESH_TOKEN = "refresh_token";
    public static final String PARAM_ID_TOKEN = "id_token";      // ????
    public static final String PARAM_TOKEN_TYPE = "token_type";
    public static final String PARAM_GRANT_TYPE = "grant_type";
    public static final String PARAM_EXPIRES_IN = "expires_in";
    public static final String PARAM_ERROR = "error";
    // ...

    // public static final String GOOGLEOAUTH_ENDPOINT_URL = "https://accounts.google.com/o/oauth2/auth";
    public static final String BASE_URL_AUTHORIZATION_CODE = "https://accounts.google.com/o/oauth2/auth";
    public static final String BASE_URL_REFRESH_TOKEN = "https://accounts.google.com/o/oauth2/token";
    public static final String BASE_URL_ACCESS_TOKEN = "https://accounts.google.com/o/oauth2/token";
    public static final String BASE_URL_REVOKE_TOKEN = "https://accounts.google.com/o/oauth2/revoke";


    // TBD: Always using the same values ???
    public static final String DEFAULT_DATA_REDIRECT_URL = "postmessage";                                 // ???
    public static final String DEFAULT_DATA_ACCESS_TYPE = "offline";                                      // ???
    public static final String DEFAULT_DATA_COOKIE_POLICY = "single_host_origin";                         // ???

    // Scope/Permissions
//    public static final String DATA_SCOPE_PLUS_LOGIN = "https://www.googleapis.com/auth/oauth.login";
//    public static final String DATA_SCOPE_PLUS_ME = "https://www.googleapis.com/auth/oauth.me";
    public static final String DATA_SCOPE_USERINFO_EMAIL = "https://www.googleapis.com/auth/userinfo.email"; 
    public static final String DATA_SCOPE_USERINFO_PROFILE = "https://www.googleapis.com/auth/userinfo.profile"; 
    public static final String DEFAULT_SCOPE_PROFILE_NO_EMAIL = DATA_SCOPE_USERINFO_PROFILE;
    public static final String DEFAULT_SCOPE_PROFILE_WITH_EMAIL = DATA_SCOPE_USERINFO_PROFILE + " " + DATA_SCOPE_USERINFO_EMAIL;
    // ...

    // Need to use the same default value across different methods/classes for consistency.
    private static final boolean INCLUDE_EMAIL_SCOPE_BY_DEFAULT = true;
    // ....

    // To be used for generating session attribute name for csrf state...
    public static final String DEFAULT_FORM_ID_GOOGLEOAUTH_SIGNIN = "_googleoauth_signin_form";         // ???
    // ...

    // Request attr to store the GoogleOAuthAccessToken object.
    public static final String REQUEST_ATTR_OAUTH_TOKEN = "com.queryclient.af.auth.googleoauth.oauthtoken";
    // "TokenResponse" object returned from Google using Google Client library.
    public static final String SESSION_ATTR_TOKENRESPONSE = "com.queryclient.af.auth.googleoauth.tokenresponse";
    // ...
    
    private GoogleOAuthAuthUtil() {}

    
    // temporary
    public static String getDefaultGoogleOAuthSignInFormId()
    {
        return DEFAULT_FORM_ID_GOOGLEOAUTH_SIGNIN;
    }

    // temporary
    // This is mainly used for default scope only.
    // If custom scope is used, the specified set of scope is used regardless of this flag.
    public static boolean includeEmailScopeByDefault()
    {
        boolean includeEmail = Config.getInstance().getBoolean(CONFIG_KEY_OAUTH_SCOPE_INCLUDE_EMAIL, INCLUDE_EMAIL_SCOPE_BY_DEFAULT);
        return includeEmail;
    }

    // temporary
    public static String getDefaultDataScope()
    {
        return getDefaultDataScope(true);
    }
    public static String getDefaultDataScope(boolean includeEmail)
    {
        if(includeEmail == true) {
            return DEFAULT_SCOPE_PROFILE_WITH_EMAIL;
        } else {
            return DEFAULT_SCOPE_PROFILE_NO_EMAIL;
        }
    }

    // Read the space-separated list of scopes, and convert it into a set.
    public static Set<String> readOAuthScopeSetFromConfig()
    {
        String scopes = Config.getInstance().getString(CONFIG_KEY_OAUTH_SCOPE, "");
        Set<String> scopeSet = parseOAuthScope(scopes);
        if(log.isLoggable(Level.INFO)) {
            log.log(Level.INFO, "Google OAuth scope list read from the config: ");
            for(String d : scopeSet) {
                log.log(Level.INFO, "-- scope: " + d);
            }
        }
        return scopeSet;
    }

    public static Set<String> readCustomOAuthScopeSetFromConfig(String customScopeKey)
    {
        String customScopeConfigKey = CONFIG_KEY_OAUTH_CUSTOM_SCOPE_PREFIX + customScopeKey;
        String scopes = Config.getInstance().getString(customScopeConfigKey);
        if(scopes == null) {
            return null;
        }
        Set<String> scopeSet = parseOAuthScope(scopes);
        if(log.isLoggable(Level.INFO)) {
            log.log(Level.INFO, "Google OAuth custom scope list read from the config for key = " + customScopeConfigKey);
            for(String d : scopeSet) {
                log.log(Level.INFO, "-- scope: " + d);
            }
        }
        return scopeSet;
    }


    // TBD: Move these to a generic util class ???
    
    public static Set<String> parseOAuthScope(String scopes)
    {
        if(scopes == null) {
            return null;
        }
        Set<String> scopeSet = new HashSet<>();
        if(scopes.isEmpty()) {
            return scopeSet;
        }
        String[] scopeArr = scopes.split("\\s+");
        scopeSet.addAll(Arrays.asList(scopeArr));            
        return scopeSet;
    }
    public static String buildOAuthScope(Collection<String> scopeSet)
    {
        if(scopeSet == null) {
            return null;
        } else if(scopeSet.isEmpty()) {
            return "";   // ???
        } else {
            StringBuilder sb = new StringBuilder();
            for(String s : scopeSet) {
                sb.append(s).append(" ");
            }
            String scope = sb.toString();
            if(scope.endsWith(" ")) {     // Always true
                scope = scope.substring(0, scope.length() - 1);
            }
            return scope;
        }
    }


    // Note that the arg scope can be an actual scope or a "scope key".
    public static Set<String> addScopes(Set<String> baseScopes, Set<String> secondScopes)
    {
        if(baseScopes == null) {
            baseScopes = new HashSet<>();
        }
        baseScopes.addAll(secondScopes);
        return baseScopes;
    }
    public static Set<String> removeScopes(Set<String> baseScopes, Set<String> secondScopes)
    {
        if(baseScopes == null) {
            baseScopes = new HashSet<>();
        }
        baseScopes.removeAll(secondScopes);
        return baseScopes;
    }


    // TBD:
    // ...
    
}
