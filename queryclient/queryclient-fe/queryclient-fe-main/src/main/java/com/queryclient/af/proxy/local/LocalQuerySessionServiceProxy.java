package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
// import com.queryclient.ws.bean.QuerySessionBean;
import com.queryclient.ws.service.QuerySessionService;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.proxy.QuerySessionServiceProxy;
import com.queryclient.af.proxy.util.ReferrerInfoStructProxyUtil;


public class LocalQuerySessionServiceProxy extends BaseLocalServiceProxy implements QuerySessionServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalQuerySessionServiceProxy.class.getName());

    public LocalQuerySessionServiceProxy()
    {
    }

    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        QuerySession serverBean = getQuerySessionService().getQuerySession(guid);
        QuerySession appBean = convertServerQuerySessionBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        return getQuerySessionService().getQuerySession(guid, field);       
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        List<QuerySession> serverBeanList = getQuerySessionService().getQuerySessions(guids);
        List<QuerySession> appBeanList = convertServerQuerySessionBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return getAllQuerySessions(null, null, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getQuerySessionService().getAllQuerySessions(ordering, offset, count);
        return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<QuerySession> serverBeanList = getQuerySessionService().getAllQuerySessions(ordering, offset, count, forwardCursor);
        List<QuerySession> appBeanList = convertServerQuerySessionBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getQuerySessionService().getAllQuerySessionKeys(ordering, offset, count);
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getQuerySessionService().getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getQuerySessionService().findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count);
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<QuerySession> serverBeanList = getQuerySessionService().findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<QuerySession> appBeanList = convertServerQuerySessionBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getQuerySessionService().findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getQuerySessionService().findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getQuerySessionService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        return getQuerySessionService().createQuerySession(user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note);
    }

    @Override
    public String createQuerySession(QuerySession querySession) throws BaseException
    {
        com.queryclient.ws.bean.QuerySessionBean serverBean =  convertAppQuerySessionBeanToServerBean(querySession);
        return getQuerySessionService().createQuerySession(serverBean);
    }

    @Override
    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        return getQuerySessionService().updateQuerySession(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note);
    }

    @Override
    public Boolean updateQuerySession(QuerySession querySession) throws BaseException
    {
        com.queryclient.ws.bean.QuerySessionBean serverBean =  convertAppQuerySessionBeanToServerBean(querySession);
        return getQuerySessionService().updateQuerySession(serverBean);
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        return getQuerySessionService().deleteQuerySession(guid);
    }

    @Override
    public Boolean deleteQuerySession(QuerySession querySession) throws BaseException
    {
        com.queryclient.ws.bean.QuerySessionBean serverBean =  convertAppQuerySessionBeanToServerBean(querySession);
        return getQuerySessionService().deleteQuerySession(serverBean);
    }

    @Override
    public Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException
    {
        return getQuerySessionService().deleteQuerySessions(filter, params, values);
    }




    public static QuerySessionBean convertServerQuerySessionBeanToAppBean(QuerySession serverBean)
    {
        QuerySessionBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new QuerySessionBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setDataService(serverBean.getDataService());
            bean.setServiceUrl(serverBean.getServiceUrl());
            bean.setInputFormat(serverBean.getInputFormat());
            bean.setOutputFormat(serverBean.getOutputFormat());
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertServerReferrerInfoStructBeanToAppBean(serverBean.getReferrerInfo()));
            bean.setStatus(serverBean.getStatus());
            bean.setNote(serverBean.getNote());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<QuerySession> convertServerQuerySessionBeanListToAppBeanList(List<QuerySession> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<QuerySession> beanList = new ArrayList<QuerySession>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(QuerySession sb : serverBeanList) {
                QuerySessionBean bean = convertServerQuerySessionBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.QuerySessionBean convertAppQuerySessionBeanToServerBean(QuerySession appBean)
    {
        com.queryclient.ws.bean.QuerySessionBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.QuerySessionBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setDataService(appBean.getDataService());
            bean.setServiceUrl(appBean.getServiceUrl());
            bean.setInputFormat(appBean.getInputFormat());
            bean.setOutputFormat(appBean.getOutputFormat());
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertAppReferrerInfoStructBeanToServerBean(appBean.getReferrerInfo()));
            bean.setStatus(appBean.getStatus());
            bean.setNote(appBean.getNote());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<QuerySession> convertAppQuerySessionBeanListToServerBeanList(List<QuerySession> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<QuerySession> beanList = new ArrayList<QuerySession>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(QuerySession sb : appBeanList) {
                com.queryclient.ws.bean.QuerySessionBean bean = convertAppQuerySessionBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
