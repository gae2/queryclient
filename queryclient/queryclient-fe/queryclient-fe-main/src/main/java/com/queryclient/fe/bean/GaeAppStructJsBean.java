package com.queryclient.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.queryclient.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class GaeAppStructJsBean implements Serializable, Cloneable  //, GaeAppStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GaeAppStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String groupId;
    private String appId;
    private String appDomain;
    private String namespace;
    private Long acl;
    private String note;

    // Ctors.
    public GaeAppStructJsBean()
    {
        //this((String) null);
    }
    public GaeAppStructJsBean(String groupId, String appId, String appDomain, String namespace, Long acl, String note)
    {
        this.groupId = groupId;
        this.appId = appId;
        this.appDomain = appDomain;
        this.namespace = namespace;
        this.acl = acl;
        this.note = note;
    }
    public GaeAppStructJsBean(GaeAppStructJsBean bean)
    {
        if(bean != null) {
            setGroupId(bean.getGroupId());
            setAppId(bean.getAppId());
            setAppDomain(bean.getAppDomain());
            setNamespace(bean.getNamespace());
            setAcl(bean.getAcl());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static GaeAppStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        GaeAppStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(GaeAppStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, GaeAppStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGroupId()
    {
        return this.groupId;
    }
    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getAppId()
    {
        return this.appId;
    }
    public void setAppId(String appId)
    {
        this.appId = appId;
    }

    public String getAppDomain()
    {
        return this.appDomain;
    }
    public void setAppDomain(String appDomain)
    {
        this.appDomain = appDomain;
    }

    public String getNamespace()
    {
        return this.namespace;
    }
    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    public Long getAcl()
    {
        return this.acl;
    }
    public void setAcl(Long acl)
    {
        this.acl = acl;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getGroupId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAppId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAppDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNamespace() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAcl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("groupId:null, ");
        sb.append("appId:null, ");
        sb.append("appDomain:null, ");
        sb.append("namespace:null, ");
        sb.append("acl:0, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("groupId:");
        if(this.getGroupId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGroupId()).append("\", ");
        }
        sb.append("appId:");
        if(this.getAppId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppId()).append("\", ");
        }
        sb.append("appDomain:");
        if(this.getAppDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppDomain()).append("\", ");
        }
        sb.append("namespace:");
        if(this.getNamespace() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNamespace()).append("\", ");
        }
        sb.append("acl:" + this.getAcl()).append(", ");
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGroupId() != null) {
            sb.append("\"groupId\":").append("\"").append(this.getGroupId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"groupId\":").append("null, ");
        }
        if(this.getAppId() != null) {
            sb.append("\"appId\":").append("\"").append(this.getAppId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appId\":").append("null, ");
        }
        if(this.getAppDomain() != null) {
            sb.append("\"appDomain\":").append("\"").append(this.getAppDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appDomain\":").append("null, ");
        }
        if(this.getNamespace() != null) {
            sb.append("\"namespace\":").append("\"").append(this.getNamespace()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"namespace\":").append("null, ");
        }
        if(this.getAcl() != null) {
            sb.append("\"acl\":").append("").append(this.getAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"acl\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("groupId = " + this.groupId).append(";");
        sb.append("appId = " + this.appId).append(";");
        sb.append("appDomain = " + this.appDomain).append(";");
        sb.append("namespace = " + this.namespace).append(";");
        sb.append("acl = " + this.acl).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        GaeAppStructJsBean cloned = new GaeAppStructJsBean();
        cloned.setGroupId(this.getGroupId());   
        cloned.setAppId(this.getAppId());   
        cloned.setAppDomain(this.getAppDomain());   
        cloned.setNamespace(this.getNamespace());   
        cloned.setAcl(this.getAcl());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
