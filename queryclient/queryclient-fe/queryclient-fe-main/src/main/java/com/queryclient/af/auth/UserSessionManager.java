package com.queryclient.af.auth;

import java.util.Date;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.queryclient.ws.core.GUID;


// Utility methods for managing 
//   both "session" (browser session) and "persistent session" (sessionCookie).
public class UserSessionManager
{
    private static final Logger log = Logger.getLogger(UserSessionManager.class.getName());
    
    private UserSessionManager()
    {
        // TBD.
    }

    // Initialization-on-demand holder.
    private static final class UserSessionManagerHolder
    {
        private static final UserSessionManager INSTANCE = new UserSessionManager();
    }

    // Singleton method
    public static UserSessionManager getInstance()
    {
        return UserSessionManagerHolder.INSTANCE;
    }

    
    public String getSessionToken(HttpServletRequest request, HttpServletResponse response)
    {
        HttpSession session = request.getSession(true);
        if(session == null) {
            log.warning("Session arg is null. Cannot set session bean.");
            return null;
        }
        SessionBean sBean = setSessionBean(request, response);
        return sBean.getToken();
    }
    
    private static SessionBean generateSessionBean()
    {
        String guid = GUID.generate();
        String token = GUID.convertToShortString(guid);

        SessionBean sBean = new SessionBean();
        sBean.setGuid(guid);
        sBean.setToken(token);
        sBean.setCreatedTime(new Date().getTime());
     
        return sBean;
    }

    public SessionBean setSessionBean(HttpServletRequest request, HttpServletResponse response)
    {
        // persistent==true by default.
        return setSessionBean(request, response, true);
    }

    public SessionBean setSessionBean(HttpServletRequest request, HttpServletResponse response, boolean persistent)
    {
        HttpSession session = request.getSession(true);
        if(session == null) {
            log.warning("Session arg is null. Cannot set session bean.");
            return null;   // TBD: Throw exception?
        }

        SessionBean sBean = null;
        Object obj = session.getAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN );
        if(obj != null) {
            sBean = (SessionBean) obj;
            if(log.isLoggable(Level.FINE)) log.fine("sessionBean found in the current session: " + sBean);
        } else {
            // Ignore
            log.info("sessionBean object not found in the current session.");
        }

        if(sBean == null) {
            if(persistent) {
                // Look for "persistent session" cookie.
                SessionCookie sCookie = SessionCookieUtil.findSessionCookie(request);
                if(sCookie != null) {
                    sBean = sCookie.getSessionBean();
                    if(log.isLoggable(Level.INFO)) log.info("Found persistent sessionBean in the cookie: " + sBean);
                } else {
                    // ignore
                    log.info("Persistent sessionBean object not found in the cookie.");
                }
            }
            if(sBean == null) {
                // Create a new sessionBean.
                sBean = generateSessionBean();            
            }
            session.setAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN, sBean );
            if(log.isLoggable(Level.INFO)) log.info("Setting session.sessionBean to " + sBean);
            if(persistent) {
                SessionCookieUtil.addSessionCookie(response, sBean);
                if(log.isLoggable(Level.INFO)) log.info("Persistent sessionBean added to the session cookie: " + sBean);
            }
        } else {
            // Ignore.
        }

//        // TBD: Should we set the cookie for every request?
//        if(persistent) {
//            SessionCookieUtil.addSessionCookie(response, sBean);
//            if(log.isLoggable(Level.INFO)) log.info("Persistent sessionBean added to the session cookie: " + sBean);
//        }

        return sBean;
    }
    
    public SessionBean refreshSessionBean(HttpServletRequest request, HttpServletResponse response)
    {
        return refreshSessionBean(request, response, null);
    }

    public SessionBean refreshSessionBean(HttpServletRequest request, HttpServletResponse response, SessionBean newBean)
    {
        // persistent==true by default.
        return refreshSessionBean(request, response, newBean, true);
    }

    public SessionBean refreshSessionBean(HttpServletRequest request, HttpServletResponse response, SessionBean newBean, boolean persistent)
    {
        HttpSession session = request.getSession(true);
        if(session == null) {
            log.warning("Session arg is null. Cannot refresh session guid.");
            return null;   // TBD: Throw exception?
        }

        SessionBean oldBean = null;
        Object obj = session.getAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN );
        if(obj != null) {
            oldBean = (SessionBean) obj;
        }
        if(newBean == null) {
            newBean = generateSessionBean();
        }
        session.setAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN, newBean );
        if(log.isLoggable(Level.FINE)) log.fine("Changing sessionBean from " + oldBean + " to " + newBean);
        if(persistent) {
            SessionCookieUtil.addSessionCookie(response, newBean);
            if(log.isLoggable(Level.INFO)) log.info("Persistent sessionBean added to the session cookie (updated): " + newBean);
        }

        // TBD:
        // Update the DB user table .... ???
        // ...

        return newBean;
    }

    public boolean removeSessionBean(HttpServletRequest request, HttpServletResponse response)
    {
        // persistent == true, by default.
        return removeSessionBean(request, response, true);
    }

    // Returns true if the existing session bean is removed. Returns false, otherwise.
    // if persistent == true, the sessionBean is removed from the cookie.
    public boolean removeSessionBean(HttpServletRequest request, HttpServletResponse response, boolean persistent)
    {
        HttpSession session = request.getSession(true);
        if(session == null) {
            log.warning("Session arg is null. Cannot remove session bean.");
            return false;   // TBD: Throw exception?
        }

        boolean result;
        Object obj = session.getAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN );
        if(obj != null) {
            // TBD: At least validate the object type???
            session.removeAttribute(AuthConstants.SESSION_ATTR_SESSION_BEAN);
            result = true;
        } else {
            // Ignore
            log.fine("sessionBean object not found in the current session.");
            result = false;
        }
        if(persistent) {
            SessionCookieUtil.removeSessionCookie(request, response);
            log.info("Removing persistent sessionBean from the session cookie.");
        }

        return result;
    }


/*
    @Deprecated
    public SessionBean setSessionBean(HttpSession session)
    {
        if(session == null) {
            log.warning("Session arg is null. Cannot set session bean.");
            return null;   // TBD: Throw exception?
        }

        SessionBean sBean = null;
        Object obj = session.getAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN );
        if(obj != null) {
            sBean = (SessionBean) obj;
        } else {
            // Ignore
            log.fine("sessionBean object not found in the current session.");
        }
        if(sBean == null) {
            sBean = generateSessionBean();
            session.setAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN, sBean );
            if(log.isLoggable(Level.INFO)) log.info("Setting sessionBean to " + sBean);
        } else {
            // Ignore.
            if(log.isLoggable(Level.FINE)) log.fine("sessionBean found: " + sBean);
        }

        return sBean;
    }

    @Deprecated
    public SessionBean refreshSessionBean(HttpSession session)
    {
        return refreshSessionBean(session, null);
    }

    @Deprecated
    public SessionBean refreshSessionBean(HttpSession session, SessionBean newBean)
    {
        if(session == null) {
            log.warning("Session arg is null. Cannot refresh session guid.");
            return null;   // TBD: Throw exception?
        }

        SessionBean oldBean = null;
        Object obj = session.getAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN );
        if(obj != null) {
            oldBean = (SessionBean) obj;
        }
        if(newBean == null) {
            newBean = generateSessionBean();
        }
        session.setAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN, newBean );
        if(log.isLoggable(Level.FINE)) log.fine("Changing sessionBean from " + oldBean + " to " + newBean);

        // TBD:
        // Update the DB user table .... ???
        // ...

        return newBean;
    }

    @Deprecated
    public boolean removeSessionBean(HttpSession session)
    {
        if(session == null) {
            log.warning("Session arg is null. Cannot remove session bean.");
            return false;   // TBD: Throw exception?
        }

        Object obj = session.getAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN );
        if(obj != null) {
            // TBD: At least validate the object type???
            session.removeAttribute(AuthConstants.SESSION_ATTR_SESSION_BEAN);
            return true;
        } else {
            // Ignore
            log.fine("sessionBean object not found in the current session.");
            return false;
        }
    }
*/

}
