package com.queryclient.af.proxy.remote;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.exception.MultipleChoiceException;
import com.queryclient.ws.exception.MovedPermanentlyException;
import com.queryclient.ws.exception.FoundElsewhereException;
import com.queryclient.ws.exception.TemporaryRedirectException;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.exception.InternalServerErrorException;
import com.queryclient.ws.exception.MethodNotAllowedException;
import com.queryclient.ws.exception.NotAcceptableException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.exception.RequestConflictException;
import com.queryclient.ws.exception.RequestForbiddenException;
import com.queryclient.ws.exception.RequestTimeoutException;
import com.queryclient.ws.exception.ResourceGoneException;
import com.queryclient.ws.exception.ResourceNotFoundException;
import com.queryclient.ws.exception.ServiceUnavailableException;
import com.queryclient.ws.exception.UnauthorizedException;
import com.queryclient.ws.exception.UnsupportedMediaTypeException;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.af.core.JerseyClient;
import com.queryclient.af.config.Config;


// TBD: Make it configurable.
//      And/or use dependency injection.
public abstract class BaseRemoteServiceProxy
{
    private static final Logger log = Logger.getLogger(BaseRemoteServiceProxy.class.getName());

    // When a request times out against App Engine web service, we can retry so many times.
    protected static final int MAX_RETRIES = 10;

    // Path constant for getXXX() methods.
    protected static final String RESOURCE_PATH_ALL = "all";
    protected static final String RESOURCE_PATH_ALLKEYS = "allkeys";
    protected static final String RESOURCE_PATH_KEYS = "keys";
    protected static final String RESOURCE_PATH_SUBSET = "subset";
    protected static final String RESOURCE_PATH_COUNT = "count";

    // temporary
    private static final String CONFIG_KEY_BASE_URI = "queryclientapp.remote.baseuri";
    private static final String CONFIG_KEY_OUTPUT_MIME_TYPE = "queryclientapp.remote.outputtype";
    private static final String CONFIG_KEY_INPUT_MIME_TYPE = "queryclientapp.remote.inputtype";
    // temporary
    private static final String DEFAULT_BASE_URI = "http://localhost:8888/v1";
    private static final String DEFAULT_OUTPUT_MIME_TYPE = MediaType.APPLICATION_XML;
    private static final String DEFAULT_INPUT_MIME_TYPE = MediaType.APPLICATION_XML;
    

    // WS client.
    private Client wsClient;
    
    // WebService Base URI
    private String serviceBaseUri;

    // Desired output (from server) media type. (To be used in ACCEPT header.)
    private String outputMediaType;
    
    // Input (to server) media type. (To be used in MEDIA_TYPE(?) header.)
    private String inputMediaType;
    
    
    // Ctor
    public BaseRemoteServiceProxy()
    {
        //wsClient = Client.create();
        wsClient = JerseyClient.getInstance().getClient();
        
        // Timeout == 10 + 10 == 20 seconds...
        wsClient.setConnectTimeout(10 * 1000);
        wsClient.setReadTimeout(10 * 1000);

        serviceBaseUri = Config.getInstance().getString(CONFIG_KEY_BASE_URI, DEFAULT_BASE_URI);
        outputMediaType = Config.getInstance().getString(CONFIG_KEY_OUTPUT_MIME_TYPE, DEFAULT_OUTPUT_MIME_TYPE);
        inputMediaType = Config.getInstance().getString(CONFIG_KEY_INPUT_MIME_TYPE, DEFAULT_INPUT_MIME_TYPE);
    }

    
    public String getServiceBaseUri()
    {
        return serviceBaseUri;
    }
    public void setServiceBaseUri(String serviceBaseUri)
    {
        this.serviceBaseUri = serviceBaseUri;
    }
    
    public String getOutputMediaType()
    {
        return outputMediaType;
    }
    public void setOutputMediaType(String outputMediaType)
    {
        this.outputMediaType = outputMediaType;
    }
    
    public String getInputMediaType()
    {
        return inputMediaType;
    }
    public void setInputMediaType(String inputMediaType)
    {
        this.inputMediaType = inputMediaType;
    }


    protected String getResourcePath()
    {
        return getResourcePath(null);
    }
    protected String getResourcePath(String resourceName)
    {
        return getResourcePath(resourceName, null);
    }
    protected String getResourcePath(String resourceName, String guid)
    {
        if(resourceName == null) {
            return getServiceBaseUri();
        } else {
            String resourceCollectionPath = null;
            String baseUri = getServiceBaseUri();
            if(baseUri.isEmpty() || ! baseUri.substring(baseUri.length() - 1).equals("/")) {
                resourceCollectionPath = baseUri + "/" + resourceName;
            } else {
                resourceCollectionPath = baseUri + resourceName;                
            }
            if(guid == null) {
                return resourceCollectionPath;
            } else {
                return resourceCollectionPath + "/" + guid; 
            }
        }
    }
    
    protected WebResource getWebResource(String resource)
    {
        WebResource webResource = wsClient.resource(getResourcePath(resource));
        return webResource;
    }
    protected WebResource getWebResource(String resource, String path)
    {
        WebResource webResource = wsClient.resource(getResourcePath(resource)).path(path);
        return webResource;
    }
    protected WebResource getWebResource(String resource, String path1, String path2)
    {
        WebResource webResource = wsClient.resource(getResourcePath(resource)).path(path1).path(path2);
        return webResource;
    }

    
    protected void handleError(int status, ErrorStub error) throws BaseException
    {
        if(error == null) {
            throw new BaseException("Unknown error");
        }

        String message = error.getMessage();
        String resource = error.getResource();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Error message = " + message + "; resource = " + resource);

        switch(status) {
        // case StatusCode.MULTIPLE_CHOICES:
        //     throw new MultipleChoiceException(message);
        // case StatusCode.MOVED_PERMANENTLY:
        //     throw new MovedPermanentlyException(message);
        // case StatusCode.FOUND:
        //     throw new FoundElsewhereException(message);
        // case StatusCode.TEMPORARY_REDIRECT:
        //     throw new TemporaryRedirectException(message);
        case StatusCode.BAD_REQUEST:
            throw new BadRequestException(message);
        case StatusCode.UNAUTHORIZED:
            throw new UnauthorizedException(message);
        case StatusCode.FORBIDDEN:
            throw new RequestForbiddenException(message);
        case StatusCode.NOT_FOUND:
            throw new ResourceNotFoundException(message);
        case StatusCode.METHOD_NOT_ALLOWED:
            throw new MethodNotAllowedException(message);
        case StatusCode.NOT_ACCEPTABLE:
            throw new NotAcceptableException(message);
        case StatusCode.REQUEST_TIMEOUT:
            throw new RequestTimeoutException(message);
        case StatusCode.CONFLICT:
            throw new RequestConflictException(message);
        case StatusCode.GONE:
            throw new ResourceGoneException(message);
        case StatusCode.UNSUPPORTED_MEDIA_TYPE:
            throw new UnsupportedMediaTypeException(message);
        case StatusCode.INTERNAL_SERVER_ERROR:
            throw new InternalServerErrorException(message);
        case StatusCode.NOT_IMPLEMENTED:
            throw new NotImplementedException(message);
        case StatusCode.SERVICE_UNAVAILABLE:
            throw new ServiceUnavailableException(message);
        default:
            throw new BaseException("Unexpected error:" + message);
        }        
    }


    // For debugging
    protected void printClientResponseInfo(ClientResponse clientResponse)
    {
        int status = clientResponse.getStatus();
        System.out.println("Status = " + status);
        MultivaluedMap<String,String> headers = clientResponse.getHeaders();
        Iterator<MultivaluedMap.Entry<String,List<String>>> it = headers.entrySet().iterator();
        while(it.hasNext()) {
            MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
            String k = (String) m.getKey();
            List<String> l = (m.getValue() == null) ? new ArrayList<String>() : m.getValue(); // ???
            System.out.println("Header key = " + k);
            for(String v : l) {
                System.out.println("       value = " + v);                    
            }
        }
    }

}
