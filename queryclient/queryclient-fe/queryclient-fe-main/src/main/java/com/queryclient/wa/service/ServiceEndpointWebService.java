package com.queryclient.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.service.manager.ServiceManager;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ConsumerKeySecretPairJsBean;
import com.queryclient.fe.bean.ServiceEndpointJsBean;
import com.queryclient.wa.util.ConsumerKeySecretPairWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ServiceEndpointWebService // implements ServiceEndpointService
{
    private static final Logger log = Logger.getLogger(ServiceEndpointWebService.class.getName());
     
    // Af service interface.
    private ServiceEndpointService mService = null;

    public ServiceEndpointWebService()
    {
        this(ServiceManager.getServiceEndpointService());
    }
    public ServiceEndpointWebService(ServiceEndpointService service)
    {
        mService = service;
    }
    
    private ServiceEndpointService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getServiceEndpointService();
        }
        return mService;
    }
    
    
    public ServiceEndpointJsBean getServiceEndpoint(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            ServiceEndpoint serviceEndpoint = getService().getServiceEndpoint(guid);
            ServiceEndpointJsBean bean = convertServiceEndpointToJsBean(serviceEndpoint);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getServiceEndpoint(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getServiceEndpoint(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ServiceEndpointJsBean> getServiceEndpoints(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ServiceEndpointJsBean> jsBeans = new ArrayList<ServiceEndpointJsBean>();
            List<ServiceEndpoint> serviceEndpoints = getService().getServiceEndpoints(guids);
            if(serviceEndpoints != null) {
                for(ServiceEndpoint serviceEndpoint : serviceEndpoints) {
                    jsBeans.add(convertServiceEndpointToJsBean(serviceEndpoint));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ServiceEndpointJsBean> getAllServiceEndpoints() throws WebException
    {
        return getAllServiceEndpoints(null, null, null);
    }

    // @Deprecated
    public List<ServiceEndpointJsBean> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllServiceEndpoints(ordering, offset, count, null);
    }

    public List<ServiceEndpointJsBean> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ServiceEndpointJsBean> jsBeans = new ArrayList<ServiceEndpointJsBean>();
            List<ServiceEndpoint> serviceEndpoints = getService().getAllServiceEndpoints(ordering, offset, count, forwardCursor);
            if(serviceEndpoints != null) {
                for(ServiceEndpoint serviceEndpoint : serviceEndpoints) {
                    jsBeans.add(convertServiceEndpointToJsBean(serviceEndpoint));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<ServiceEndpointJsBean> findServiceEndpoints(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findServiceEndpoints(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<ServiceEndpointJsBean> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<ServiceEndpointJsBean> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ServiceEndpointJsBean> jsBeans = new ArrayList<ServiceEndpointJsBean>();
            List<ServiceEndpoint> serviceEndpoints = getService().findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(serviceEndpoints != null) {
                for(ServiceEndpoint serviceEndpoint : serviceEndpoints) {
                    jsBeans.add(convertServiceEndpointToJsBean(serviceEndpoint));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createServiceEndpoint(String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairJsBean authCredential, String status) throws WebException
    {
        try {
            return getService().createServiceEndpoint(user, dataService, serviceName, serviceUrl, authRequired, ConsumerKeySecretPairWebUtil.convertConsumerKeySecretPairJsBeanToBean(authCredential), status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createServiceEndpoint(String jsonStr) throws WebException
    {
        return createServiceEndpoint(ServiceEndpointJsBean.fromJsonString(jsonStr));
    }

    public String createServiceEndpoint(ServiceEndpointJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ServiceEndpoint serviceEndpoint = convertServiceEndpointJsBeanToBean(jsBean);
            return getService().createServiceEndpoint(serviceEndpoint);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ServiceEndpointJsBean constructServiceEndpoint(String jsonStr) throws WebException
    {
        return constructServiceEndpoint(ServiceEndpointJsBean.fromJsonString(jsonStr));
    }

    public ServiceEndpointJsBean constructServiceEndpoint(ServiceEndpointJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ServiceEndpoint serviceEndpoint = convertServiceEndpointJsBeanToBean(jsBean);
            serviceEndpoint = getService().constructServiceEndpoint(serviceEndpoint);
            jsBean = convertServiceEndpointToJsBean(serviceEndpoint);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairJsBean authCredential, String status) throws WebException
    {
        try {
            return getService().updateServiceEndpoint(guid, user, dataService, serviceName, serviceUrl, authRequired, ConsumerKeySecretPairWebUtil.convertConsumerKeySecretPairJsBeanToBean(authCredential), status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateServiceEndpoint(String jsonStr) throws WebException
    {
        return updateServiceEndpoint(ServiceEndpointJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateServiceEndpoint(ServiceEndpointJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ServiceEndpoint serviceEndpoint = convertServiceEndpointJsBeanToBean(jsBean);
            return getService().updateServiceEndpoint(serviceEndpoint);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ServiceEndpointJsBean refreshServiceEndpoint(String jsonStr) throws WebException
    {
        return refreshServiceEndpoint(ServiceEndpointJsBean.fromJsonString(jsonStr));
    }

    public ServiceEndpointJsBean refreshServiceEndpoint(ServiceEndpointJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ServiceEndpoint serviceEndpoint = convertServiceEndpointJsBeanToBean(jsBean);
            serviceEndpoint = getService().refreshServiceEndpoint(serviceEndpoint);
            jsBean = convertServiceEndpointToJsBean(serviceEndpoint);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteServiceEndpoint(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteServiceEndpoint(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteServiceEndpoint(ServiceEndpointJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ServiceEndpoint serviceEndpoint = convertServiceEndpointJsBeanToBean(jsBean);
            return getService().deleteServiceEndpoint(serviceEndpoint);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteServiceEndpoints(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteServiceEndpoints(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static ServiceEndpointJsBean convertServiceEndpointToJsBean(ServiceEndpoint serviceEndpoint)
    {
        ServiceEndpointJsBean jsBean = null;
        if(serviceEndpoint != null) {
            jsBean = new ServiceEndpointJsBean();
            jsBean.setGuid(serviceEndpoint.getGuid());
            jsBean.setUser(serviceEndpoint.getUser());
            jsBean.setDataService(serviceEndpoint.getDataService());
            jsBean.setServiceName(serviceEndpoint.getServiceName());
            jsBean.setServiceUrl(serviceEndpoint.getServiceUrl());
            jsBean.setAuthRequired(serviceEndpoint.isAuthRequired());
            jsBean.setAuthCredential(ConsumerKeySecretPairWebUtil.convertConsumerKeySecretPairToJsBean(serviceEndpoint.getAuthCredential()));
            jsBean.setStatus(serviceEndpoint.getStatus());
            jsBean.setCreatedTime(serviceEndpoint.getCreatedTime());
            jsBean.setModifiedTime(serviceEndpoint.getModifiedTime());
        }
        return jsBean;
    }

    public static ServiceEndpoint convertServiceEndpointJsBeanToBean(ServiceEndpointJsBean jsBean)
    {
        ServiceEndpointBean serviceEndpoint = null;
        if(jsBean != null) {
            serviceEndpoint = new ServiceEndpointBean();
            serviceEndpoint.setGuid(jsBean.getGuid());
            serviceEndpoint.setUser(jsBean.getUser());
            serviceEndpoint.setDataService(jsBean.getDataService());
            serviceEndpoint.setServiceName(jsBean.getServiceName());
            serviceEndpoint.setServiceUrl(jsBean.getServiceUrl());
            serviceEndpoint.setAuthRequired(jsBean.isAuthRequired());
            serviceEndpoint.setAuthCredential(ConsumerKeySecretPairWebUtil.convertConsumerKeySecretPairJsBeanToBean(jsBean.getAuthCredential()));
            serviceEndpoint.setStatus(jsBean.getStatus());
            serviceEndpoint.setCreatedTime(jsBean.getCreatedTime());
            serviceEndpoint.setModifiedTime(jsBean.getModifiedTime());
        }
        return serviceEndpoint;
    }

}
