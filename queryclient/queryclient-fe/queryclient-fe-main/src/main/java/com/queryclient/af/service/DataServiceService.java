package com.queryclient.af.service;

import java.util.List;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface DataServiceService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    DataService getDataService(String guid) throws BaseException;
    Object getDataService(String guid, String field) throws BaseException;
    List<DataService> getDataServices(List<String> guids) throws BaseException;
    List<DataService> getAllDataServices() throws BaseException;
    /* @Deprecated */ List<DataService> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException;
    List<DataService> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<DataService> findDataServices(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createDataService(String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException;
    //String createDataService(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return DataService?)
    String createDataService(DataService dataService) throws BaseException;
    DataService constructDataService(DataService dataService) throws BaseException;
    Boolean updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException;
    //Boolean updateDataService(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateDataService(DataService dataService) throws BaseException;
    DataService refreshDataService(DataService dataService) throws BaseException;
    Boolean deleteDataService(String guid) throws BaseException;
    Boolean deleteDataService(DataService dataService) throws BaseException;
    Long deleteDataServices(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createDataServices(List<DataService> dataServices) throws BaseException;
//    Boolean updateDataServices(List<DataService> dataServices) throws BaseException;

}
