package com.queryclient.af.proxy.async;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.ApiConsumerServiceProxy;
import com.queryclient.af.proxy.UserServiceProxy;
import com.queryclient.af.proxy.UserPasswordServiceProxy;
import com.queryclient.af.proxy.ExternalUserAuthServiceProxy;
import com.queryclient.af.proxy.UserAuthStateServiceProxy;
import com.queryclient.af.proxy.DataServiceServiceProxy;
import com.queryclient.af.proxy.ServiceEndpointServiceProxy;
import com.queryclient.af.proxy.QuerySessionServiceProxy;
import com.queryclient.af.proxy.QueryRecordServiceProxy;
import com.queryclient.af.proxy.DummyEntityServiceProxy;
import com.queryclient.af.proxy.ServiceInfoServiceProxy;
import com.queryclient.af.proxy.FiveTenServiceProxy;

public class AsyncProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(AsyncProxyFactory.class.getName());

    private AsyncProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AsyncProxyFactoryHolder
    {
        private static final AsyncProxyFactory INSTANCE = new AsyncProxyFactory();
    }

    // Singleton method
    public static AsyncProxyFactory getInstance()
    {
        return AsyncProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new AsyncApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new AsyncUserServiceProxy();
    }

    @Override
    public UserPasswordServiceProxy getUserPasswordServiceProxy()
    {
        return new AsyncUserPasswordServiceProxy();
    }

    @Override
    public ExternalUserAuthServiceProxy getExternalUserAuthServiceProxy()
    {
        return new AsyncExternalUserAuthServiceProxy();
    }

    @Override
    public UserAuthStateServiceProxy getUserAuthStateServiceProxy()
    {
        return new AsyncUserAuthStateServiceProxy();
    }

    @Override
    public DataServiceServiceProxy getDataServiceServiceProxy()
    {
        return new AsyncDataServiceServiceProxy();
    }

    @Override
    public ServiceEndpointServiceProxy getServiceEndpointServiceProxy()
    {
        return new AsyncServiceEndpointServiceProxy();
    }

    @Override
    public QuerySessionServiceProxy getQuerySessionServiceProxy()
    {
        return new AsyncQuerySessionServiceProxy();
    }

    @Override
    public QueryRecordServiceProxy getQueryRecordServiceProxy()
    {
        return new AsyncQueryRecordServiceProxy();
    }

    @Override
    public DummyEntityServiceProxy getDummyEntityServiceProxy()
    {
        return new AsyncDummyEntityServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new AsyncServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new AsyncFiveTenServiceProxy();
    }

}
