package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
// import com.queryclient.ws.bean.ExternalUserAuthBean;
import com.queryclient.ws.service.ExternalUserAuthService;
import com.queryclient.af.bean.ExternalUserAuthBean;
import com.queryclient.af.proxy.ExternalUserAuthServiceProxy;
import com.queryclient.af.proxy.util.GaeAppStructProxyUtil;
import com.queryclient.af.proxy.util.ExternalUserIdStructProxyUtil;


public class LocalExternalUserAuthServiceProxy extends BaseLocalServiceProxy implements ExternalUserAuthServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalExternalUserAuthServiceProxy.class.getName());

    public LocalExternalUserAuthServiceProxy()
    {
    }

    @Override
    public ExternalUserAuth getExternalUserAuth(String guid) throws BaseException
    {
        ExternalUserAuth serverBean = getExternalUserAuthService().getExternalUserAuth(guid);
        ExternalUserAuth appBean = convertServerExternalUserAuthBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getExternalUserAuth(String guid, String field) throws BaseException
    {
        return getExternalUserAuthService().getExternalUserAuth(guid, field);       
    }

    @Override
    public List<ExternalUserAuth> getExternalUserAuths(List<String> guids) throws BaseException
    {
        List<ExternalUserAuth> serverBeanList = getExternalUserAuthService().getExternalUserAuths(guids);
        List<ExternalUserAuth> appBeanList = convertServerExternalUserAuthBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths() throws BaseException
    {
        return getAllExternalUserAuths(null, null, null);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getExternalUserAuthService().getAllExternalUserAuths(ordering, offset, count);
        return getAllExternalUserAuths(ordering, offset, count, null);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ExternalUserAuth> serverBeanList = getExternalUserAuthService().getAllExternalUserAuths(ordering, offset, count, forwardCursor);
        List<ExternalUserAuth> appBeanList = convertServerExternalUserAuthBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getExternalUserAuthService().getAllExternalUserAuthKeys(ordering, offset, count);
        return getAllExternalUserAuthKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getExternalUserAuthService().getAllExternalUserAuthKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findExternalUserAuths(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getExternalUserAuthService().findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count);
        return findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ExternalUserAuth> serverBeanList = getExternalUserAuthService().findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<ExternalUserAuth> appBeanList = convertServerExternalUserAuthBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getExternalUserAuthService().findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getExternalUserAuthService().findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getExternalUserAuthService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createExternalUserAuth(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        return getExternalUserAuthService().createExternalUserAuth(managerApp, appAcl, gaeApp, ownerUser, userAcl, user, providerId, externalUserId, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
    }

    @Override
    public String createExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        com.queryclient.ws.bean.ExternalUserAuthBean serverBean =  convertAppExternalUserAuthBeanToServerBean(externalUserAuth);
        return getExternalUserAuthService().createExternalUserAuth(serverBean);
    }

    @Override
    public Boolean updateExternalUserAuth(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String providerId, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        return getExternalUserAuthService().updateExternalUserAuth(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, providerId, externalUserId, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
    }

    @Override
    public Boolean updateExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        com.queryclient.ws.bean.ExternalUserAuthBean serverBean =  convertAppExternalUserAuthBeanToServerBean(externalUserAuth);
        return getExternalUserAuthService().updateExternalUserAuth(serverBean);
    }

    @Override
    public Boolean deleteExternalUserAuth(String guid) throws BaseException
    {
        return getExternalUserAuthService().deleteExternalUserAuth(guid);
    }

    @Override
    public Boolean deleteExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        com.queryclient.ws.bean.ExternalUserAuthBean serverBean =  convertAppExternalUserAuthBeanToServerBean(externalUserAuth);
        return getExternalUserAuthService().deleteExternalUserAuth(serverBean);
    }

    @Override
    public Long deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseException
    {
        return getExternalUserAuthService().deleteExternalUserAuths(filter, params, values);
    }




    public static ExternalUserAuthBean convertServerExternalUserAuthBeanToAppBean(ExternalUserAuth serverBean)
    {
        ExternalUserAuthBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new ExternalUserAuthBean();
            bean.setGuid(serverBean.getGuid());
            bean.setManagerApp(serverBean.getManagerApp());
            bean.setAppAcl(serverBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertServerGaeAppStructBeanToAppBean(serverBean.getGaeApp()));
            bean.setOwnerUser(serverBean.getOwnerUser());
            bean.setUserAcl(serverBean.getUserAcl());
            bean.setUser(serverBean.getUser());
            bean.setProviderId(serverBean.getProviderId());
            bean.setExternalUserId(ExternalUserIdStructProxyUtil.convertServerExternalUserIdStructBeanToAppBean(serverBean.getExternalUserId()));
            bean.setRequestToken(serverBean.getRequestToken());
            bean.setAccessToken(serverBean.getAccessToken());
            bean.setAccessTokenSecret(serverBean.getAccessTokenSecret());
            bean.setEmail(serverBean.getEmail());
            bean.setFirstName(serverBean.getFirstName());
            bean.setLastName(serverBean.getLastName());
            bean.setFullName(serverBean.getFullName());
            bean.setDisplayName(serverBean.getDisplayName());
            bean.setDescription(serverBean.getDescription());
            bean.setGender(serverBean.getGender());
            bean.setDateOfBirth(serverBean.getDateOfBirth());
            bean.setProfileImageUrl(serverBean.getProfileImageUrl());
            bean.setTimeZone(serverBean.getTimeZone());
            bean.setPostalCode(serverBean.getPostalCode());
            bean.setLocation(serverBean.getLocation());
            bean.setCountry(serverBean.getCountry());
            bean.setLanguage(serverBean.getLanguage());
            bean.setStatus(serverBean.getStatus());
            bean.setAuthTime(serverBean.getAuthTime());
            bean.setExpirationTime(serverBean.getExpirationTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ExternalUserAuth> convertServerExternalUserAuthBeanListToAppBeanList(List<ExternalUserAuth> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<ExternalUserAuth> beanList = new ArrayList<ExternalUserAuth>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(ExternalUserAuth sb : serverBeanList) {
                ExternalUserAuthBean bean = convertServerExternalUserAuthBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.ExternalUserAuthBean convertAppExternalUserAuthBeanToServerBean(ExternalUserAuth appBean)
    {
        com.queryclient.ws.bean.ExternalUserAuthBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.ExternalUserAuthBean();
            bean.setGuid(appBean.getGuid());
            bean.setManagerApp(appBean.getManagerApp());
            bean.setAppAcl(appBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertAppGaeAppStructBeanToServerBean(appBean.getGaeApp()));
            bean.setOwnerUser(appBean.getOwnerUser());
            bean.setUserAcl(appBean.getUserAcl());
            bean.setUser(appBean.getUser());
            bean.setProviderId(appBean.getProviderId());
            bean.setExternalUserId(ExternalUserIdStructProxyUtil.convertAppExternalUserIdStructBeanToServerBean(appBean.getExternalUserId()));
            bean.setRequestToken(appBean.getRequestToken());
            bean.setAccessToken(appBean.getAccessToken());
            bean.setAccessTokenSecret(appBean.getAccessTokenSecret());
            bean.setEmail(appBean.getEmail());
            bean.setFirstName(appBean.getFirstName());
            bean.setLastName(appBean.getLastName());
            bean.setFullName(appBean.getFullName());
            bean.setDisplayName(appBean.getDisplayName());
            bean.setDescription(appBean.getDescription());
            bean.setGender(appBean.getGender());
            bean.setDateOfBirth(appBean.getDateOfBirth());
            bean.setProfileImageUrl(appBean.getProfileImageUrl());
            bean.setTimeZone(appBean.getTimeZone());
            bean.setPostalCode(appBean.getPostalCode());
            bean.setLocation(appBean.getLocation());
            bean.setCountry(appBean.getCountry());
            bean.setLanguage(appBean.getLanguage());
            bean.setStatus(appBean.getStatus());
            bean.setAuthTime(appBean.getAuthTime());
            bean.setExpirationTime(appBean.getExpirationTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ExternalUserAuth> convertAppExternalUserAuthBeanListToServerBeanList(List<ExternalUserAuth> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<ExternalUserAuth> beanList = new ArrayList<ExternalUserAuth>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(ExternalUserAuth sb : appBeanList) {
                com.queryclient.ws.bean.ExternalUserAuthBean bean = convertAppExternalUserAuthBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
