package com.queryclient.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
// import com.queryclient.ws.bean.QueryRecordBean;
import com.queryclient.ws.service.QueryRecordService;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.proxy.QueryRecordServiceProxy;
import com.queryclient.af.proxy.util.ReferrerInfoStructProxyUtil;


public class LocalQueryRecordServiceProxy extends BaseLocalServiceProxy implements QueryRecordServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalQueryRecordServiceProxy.class.getName());

    public LocalQueryRecordServiceProxy()
    {
    }

    @Override
    public QueryRecord getQueryRecord(String guid) throws BaseException
    {
        QueryRecord serverBean = getQueryRecordService().getQueryRecord(guid);
        QueryRecord appBean = convertServerQueryRecordBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getQueryRecord(String guid, String field) throws BaseException
    {
        return getQueryRecordService().getQueryRecord(guid, field);       
    }

    @Override
    public List<QueryRecord> getQueryRecords(List<String> guids) throws BaseException
    {
        List<QueryRecord> serverBeanList = getQueryRecordService().getQueryRecords(guids);
        List<QueryRecord> appBeanList = convertServerQueryRecordBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<QueryRecord> getAllQueryRecords() throws BaseException
    {
        return getAllQueryRecords(null, null, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getQueryRecordService().getAllQueryRecords(ordering, offset, count);
        return getAllQueryRecords(ordering, offset, count, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<QueryRecord> serverBeanList = getQueryRecordService().getAllQueryRecords(ordering, offset, count, forwardCursor);
        List<QueryRecord> appBeanList = convertServerQueryRecordBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getQueryRecordService().getAllQueryRecordKeys(ordering, offset, count);
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getQueryRecordService().getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getQueryRecordService().findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count);
        return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<QueryRecord> serverBeanList = getQueryRecordService().findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<QueryRecord> appBeanList = convertServerQueryRecordBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getQueryRecordService().findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getQueryRecordService().findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getQueryRecordService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createQueryRecord(String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        return getQueryRecordService().createQueryRecord(querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime);
    }

    @Override
    public String createQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        com.queryclient.ws.bean.QueryRecordBean serverBean =  convertAppQueryRecordBeanToServerBean(queryRecord);
        return getQueryRecordService().createQueryRecord(serverBean);
    }

    @Override
    public Boolean updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        return getQueryRecordService().updateQueryRecord(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime);
    }

    @Override
    public Boolean updateQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        com.queryclient.ws.bean.QueryRecordBean serverBean =  convertAppQueryRecordBeanToServerBean(queryRecord);
        return getQueryRecordService().updateQueryRecord(serverBean);
    }

    @Override
    public Boolean deleteQueryRecord(String guid) throws BaseException
    {
        return getQueryRecordService().deleteQueryRecord(guid);
    }

    @Override
    public Boolean deleteQueryRecord(QueryRecord queryRecord) throws BaseException
    {
        com.queryclient.ws.bean.QueryRecordBean serverBean =  convertAppQueryRecordBeanToServerBean(queryRecord);
        return getQueryRecordService().deleteQueryRecord(serverBean);
    }

    @Override
    public Long deleteQueryRecords(String filter, String params, List<String> values) throws BaseException
    {
        return getQueryRecordService().deleteQueryRecords(filter, params, values);
    }




    public static QueryRecordBean convertServerQueryRecordBeanToAppBean(QueryRecord serverBean)
    {
        QueryRecordBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new QueryRecordBean();
            bean.setGuid(serverBean.getGuid());
            bean.setQuerySession(serverBean.getQuerySession());
            bean.setQueryId(serverBean.getQueryId());
            bean.setDataService(serverBean.getDataService());
            bean.setServiceUrl(serverBean.getServiceUrl());
            bean.setDelayed(serverBean.isDelayed());
            bean.setQuery(serverBean.getQuery());
            bean.setInputFormat(serverBean.getInputFormat());
            bean.setInputFile(serverBean.getInputFile());
            bean.setInputContent(serverBean.getInputContent());
            bean.setTargetOutputFormat(serverBean.getTargetOutputFormat());
            bean.setOutputFormat(serverBean.getOutputFormat());
            bean.setOutputFile(serverBean.getOutputFile());
            bean.setOutputContent(serverBean.getOutputContent());
            bean.setResponseCode(serverBean.getResponseCode());
            bean.setResult(serverBean.getResult());
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertServerReferrerInfoStructBeanToAppBean(serverBean.getReferrerInfo()));
            bean.setStatus(serverBean.getStatus());
            bean.setExtra(serverBean.getExtra());
            bean.setNote(serverBean.getNote());
            bean.setScheduledTime(serverBean.getScheduledTime());
            bean.setProcessedTime(serverBean.getProcessedTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<QueryRecord> convertServerQueryRecordBeanListToAppBeanList(List<QueryRecord> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<QueryRecord> beanList = new ArrayList<QueryRecord>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(QueryRecord sb : serverBeanList) {
                QueryRecordBean bean = convertServerQueryRecordBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.queryclient.ws.bean.QueryRecordBean convertAppQueryRecordBeanToServerBean(QueryRecord appBean)
    {
        com.queryclient.ws.bean.QueryRecordBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.queryclient.ws.bean.QueryRecordBean();
            bean.setGuid(appBean.getGuid());
            bean.setQuerySession(appBean.getQuerySession());
            bean.setQueryId(appBean.getQueryId());
            bean.setDataService(appBean.getDataService());
            bean.setServiceUrl(appBean.getServiceUrl());
            bean.setDelayed(appBean.isDelayed());
            bean.setQuery(appBean.getQuery());
            bean.setInputFormat(appBean.getInputFormat());
            bean.setInputFile(appBean.getInputFile());
            bean.setInputContent(appBean.getInputContent());
            bean.setTargetOutputFormat(appBean.getTargetOutputFormat());
            bean.setOutputFormat(appBean.getOutputFormat());
            bean.setOutputFile(appBean.getOutputFile());
            bean.setOutputContent(appBean.getOutputContent());
            bean.setResponseCode(appBean.getResponseCode());
            bean.setResult(appBean.getResult());
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertAppReferrerInfoStructBeanToServerBean(appBean.getReferrerInfo()));
            bean.setStatus(appBean.getStatus());
            bean.setExtra(appBean.getExtra());
            bean.setNote(appBean.getNote());
            bean.setScheduledTime(appBean.getScheduledTime());
            bean.setProcessedTime(appBean.getProcessedTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<QueryRecord> convertAppQueryRecordBeanListToServerBeanList(List<QueryRecord> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<QueryRecord> beanList = new ArrayList<QueryRecord>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(QueryRecord sb : appBeanList) {
                com.queryclient.ws.bean.QueryRecordBean bean = convertAppQueryRecordBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
