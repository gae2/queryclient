package com.queryclient.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.stub.ConsumerKeySecretPairStub;


// Wrapper class + bean combo.
public class ConsumerKeySecretPairBean implements ConsumerKeySecretPair, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ConsumerKeySecretPairBean.class.getName());

    // [1] With an embedded object.
    private ConsumerKeySecretPairStub stub = null;

    // [2] Or, without an embedded object.
    private String consumerKey;
    private String consumerSecret;

    // Ctors.
    public ConsumerKeySecretPairBean()
    {
        //this((String) null);
    }
    public ConsumerKeySecretPairBean(String consumerKey, String consumerSecret)
    {
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
    }
    public ConsumerKeySecretPairBean(ConsumerKeySecretPair stub)
    {
        if(stub instanceof ConsumerKeySecretPairStub) {
            this.stub = (ConsumerKeySecretPairStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setConsumerKey(stub.getConsumerKey());   
            setConsumerSecret(stub.getConsumerSecret());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getConsumerKey()
    {
        if(getStub() != null) {
            return getStub().getConsumerKey();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.consumerKey;
        }
    }
    public void setConsumerKey(String consumerKey)
    {
        if(getStub() != null) {
            getStub().setConsumerKey(consumerKey);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.consumerKey = consumerKey;
        }
    }

    public String getConsumerSecret()
    {
        if(getStub() != null) {
            return getStub().getConsumerSecret();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.consumerSecret;
        }
    }
    public void setConsumerSecret(String consumerSecret)
    {
        if(getStub() != null) {
            getStub().setConsumerSecret(consumerSecret);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.consumerSecret = consumerSecret;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getConsumerKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getConsumerSecret() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public ConsumerKeySecretPairStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ConsumerKeySecretPairStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("consumerKey = " + this.consumerKey).append(";");
            sb.append("consumerSecret = " + this.consumerSecret).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = consumerKey == null ? 0 : consumerKey.hashCode();
            _hash = 31 * _hash + delta;
            delta = consumerSecret == null ? 0 : consumerSecret.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
