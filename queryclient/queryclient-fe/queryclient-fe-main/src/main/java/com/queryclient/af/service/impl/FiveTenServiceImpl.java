package com.queryclient.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.exception.BadRequestException;
import com.queryclient.ws.core.GUID;
import com.queryclient.ws.FiveTen;
import com.queryclient.af.config.Config;

import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.KeyValuePairStructBean;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.af.bean.GaeUserStructBean;

import com.queryclient.af.bean.FiveTenBean;
import com.queryclient.af.proxy.AbstractProxyFactory;
import com.queryclient.af.proxy.manager.ProxyFactoryManager;
import com.queryclient.af.service.ServiceConstants;
import com.queryclient.af.service.FiveTenService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FiveTenServiceImpl implements FiveTenService
{
    private static final Logger log = Logger.getLogger(FiveTenServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "FiveTen-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("FiveTen:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public FiveTenServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // FiveTen related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFiveTen(): guid = " + guid);

        FiveTenBean bean = null;
        if(getCache() != null) {
            bean = (FiveTenBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (FiveTenBean) getProxyFactory().getFiveTenServiceProxy().getFiveTen(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "FiveTenBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FiveTenBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        FiveTenBean bean = null;
        if(getCache() != null) {
            bean = (FiveTenBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (FiveTenBean) getProxyFactory().getFiveTenServiceProxy().getFiveTen(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "FiveTenBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FiveTenBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("counter")) {
            return bean.getCounter();
        } else if(field.equals("requesterIpAddress")) {
            return bean.getRequesterIpAddress();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        log.fine("getFiveTens()");

        // TBD: Is there a better way????
        List<FiveTen> fiveTens = getProxyFactory().getFiveTenServiceProxy().getFiveTens(guids);
        if(fiveTens == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenBean list.");
        }

        log.finer("END");
        return fiveTens;
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return getAllFiveTens(null, null, null);
    }


    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFiveTens(ordering, offset, count, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFiveTens(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FiveTen> fiveTens = getProxyFactory().getFiveTenServiceProxy().getAllFiveTens(ordering, offset, count, forwardCursor);
        if(fiveTens == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenBean list.");
        }

        log.finer("END");
        return fiveTens;
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFiveTenKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFiveTenKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getFiveTenServiceProxy().getAllFiveTenKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty FiveTenBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FiveTenServiceImpl.findFiveTens(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FiveTen> fiveTens = getProxyFactory().getFiveTenServiceProxy().findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(fiveTens == null) {
            log.log(Level.WARNING, "Failed to find fiveTens for the given criterion.");
        }

        log.finer("END");
        return fiveTens;
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FiveTenServiceImpl.findFiveTenKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getFiveTenServiceProxy().findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find FiveTen keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty FiveTen key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FiveTenServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getFiveTenServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        FiveTenBean bean = new FiveTenBean(null, counter, requesterIpAddress);
        return createFiveTen(bean);
    }

    @Override
    public String createFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //FiveTen bean = constructFiveTen(fiveTen);
        //return bean.getGuid();

        // Param fiveTen cannot be null.....
        if(fiveTen == null) {
            log.log(Level.INFO, "Param fiveTen is null!");
            throw new BadRequestException("Param fiveTen object is null!");
        }
        FiveTenBean bean = null;
        if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else if(fiveTen instanceof FiveTen) {
            // bean = new FiveTenBean(null, fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new FiveTenBean(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        } else {
            log.log(Level.WARNING, "createFiveTen(): Arg fiveTen is of an unknown type.");
            //bean = new FiveTenBean();
            bean = new FiveTenBean(fiveTen.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getFiveTenServiceProxy().createFiveTen(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public FiveTen constructFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // Param fiveTen cannot be null.....
        if(fiveTen == null) {
            log.log(Level.INFO, "Param fiveTen is null!");
            throw new BadRequestException("Param fiveTen object is null!");
        }
        FiveTenBean bean = null;
        if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else if(fiveTen instanceof FiveTen) {
            // bean = new FiveTenBean(null, fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new FiveTenBean(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        } else {
            log.log(Level.WARNING, "createFiveTen(): Arg fiveTen is of an unknown type.");
            //bean = new FiveTenBean();
            bean = new FiveTenBean(fiveTen.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getFiveTenServiceProxy().createFiveTen(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        FiveTenBean bean = new FiveTenBean(guid, counter, requesterIpAddress);
        return updateFiveTen(bean);
    }
        
    @Override
    public Boolean updateFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //FiveTen bean = refreshFiveTen(fiveTen);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param fiveTen cannot be null.....
        if(fiveTen == null || fiveTen.getGuid() == null) {
            log.log(Level.INFO, "Param fiveTen or its guid is null!");
            throw new BadRequestException("Param fiveTen object or its guid is null!");
        }
        FiveTenBean bean = null;
        if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else {  // if(fiveTen instanceof FiveTen)
            bean = new FiveTenBean(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        }
        Boolean suc = getProxyFactory().getFiveTenServiceProxy().updateFiveTen(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public FiveTen refreshFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // Param fiveTen cannot be null.....
        if(fiveTen == null || fiveTen.getGuid() == null) {
            log.log(Level.INFO, "Param fiveTen or its guid is null!");
            throw new BadRequestException("Param fiveTen object or its guid is null!");
        }
        FiveTenBean bean = null;
        if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else {  // if(fiveTen instanceof FiveTen)
            bean = new FiveTenBean(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        }
        Boolean suc = getProxyFactory().getFiveTenServiceProxy().updateFiveTen(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getFiveTenServiceProxy().deleteFiveTen(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            FiveTen fiveTen = null;
            try {
                fiveTen = getProxyFactory().getFiveTenServiceProxy().getFiveTen(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch fiveTen with a key, " + guid);
                return false;
            }
            if(fiveTen != null) {
                String beanGuid = fiveTen.getGuid();
                Boolean suc1 = getProxyFactory().getFiveTenServiceProxy().deleteFiveTen(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("fiveTen with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // Param fiveTen cannot be null.....
        if(fiveTen == null || fiveTen.getGuid() == null) {
            log.log(Level.INFO, "Param fiveTen or its guid is null!");
            throw new BadRequestException("Param fiveTen object or its guid is null!");
        }
        FiveTenBean bean = null;
        if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else {  // if(fiveTen instanceof FiveTen)
            // ????
            log.warning("fiveTen is not an instance of FiveTenBean.");
            bean = new FiveTenBean(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        }
        Boolean suc = getProxyFactory().getFiveTenServiceProxy().deleteFiveTen(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getFiveTenServiceProxy().deleteFiveTens(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createFiveTens(List<FiveTen> fiveTens) throws BaseException
    {
        log.finer("BEGIN");

        if(fiveTens == null) {
            log.log(Level.WARNING, "createFiveTens() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = fiveTens.size();
        if(size == 0) {
            log.log(Level.WARNING, "createFiveTens() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(FiveTen fiveTen : fiveTens) {
            String guid = createFiveTen(fiveTen);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createFiveTens() failed for at least one fiveTen. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateFiveTens(List<FiveTen> fiveTens) throws BaseException
    //{
    //}

}
