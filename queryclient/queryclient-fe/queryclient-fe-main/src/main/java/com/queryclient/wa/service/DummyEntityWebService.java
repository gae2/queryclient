package com.queryclient.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.DummyEntity;
import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.service.manager.ServiceManager;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.DummyEntityJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DummyEntityWebService // implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(DummyEntityWebService.class.getName());
     
    // Af service interface.
    private DummyEntityService mService = null;

    public DummyEntityWebService()
    {
        this(ServiceManager.getDummyEntityService());
    }
    public DummyEntityWebService(DummyEntityService service)
    {
        mService = service;
    }
    
    private DummyEntityService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getDummyEntityService();
        }
        return mService;
    }
    
    
    public DummyEntityJsBean getDummyEntity(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            DummyEntity dummyEntity = getService().getDummyEntity(guid);
            DummyEntityJsBean bean = convertDummyEntityToJsBean(dummyEntity);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getDummyEntity(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getDummyEntity(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<DummyEntityJsBean> getDummyEntities(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<DummyEntityJsBean> jsBeans = new ArrayList<DummyEntityJsBean>();
            List<DummyEntity> dummyEntities = getService().getDummyEntities(guids);
            if(dummyEntities != null) {
                for(DummyEntity dummyEntity : dummyEntities) {
                    jsBeans.add(convertDummyEntityToJsBean(dummyEntity));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<DummyEntityJsBean> getAllDummyEntities() throws WebException
    {
        return getAllDummyEntities(null, null, null);
    }

    // @Deprecated
    public List<DummyEntityJsBean> getAllDummyEntities(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllDummyEntities(ordering, offset, count, null);
    }

    public List<DummyEntityJsBean> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<DummyEntityJsBean> jsBeans = new ArrayList<DummyEntityJsBean>();
            List<DummyEntity> dummyEntities = getService().getAllDummyEntities(ordering, offset, count, forwardCursor);
            if(dummyEntities != null) {
                for(DummyEntity dummyEntity : dummyEntities) {
                    jsBeans.add(convertDummyEntityToJsBean(dummyEntity));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<DummyEntityJsBean> findDummyEntities(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<DummyEntityJsBean> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<DummyEntityJsBean> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<DummyEntityJsBean> jsBeans = new ArrayList<DummyEntityJsBean>();
            List<DummyEntity> dummyEntities = getService().findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(dummyEntities != null) {
                for(DummyEntity dummyEntity : dummyEntities) {
                    jsBeans.add(convertDummyEntityToJsBean(dummyEntity));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws WebException
    {
        try {
            return getService().createDummyEntity(user, name, content, maxLength, expired, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createDummyEntity(String jsonStr) throws WebException
    {
        return createDummyEntity(DummyEntityJsBean.fromJsonString(jsonStr));
    }

    public String createDummyEntity(DummyEntityJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            DummyEntity dummyEntity = convertDummyEntityJsBeanToBean(jsBean);
            return getService().createDummyEntity(dummyEntity);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public DummyEntityJsBean constructDummyEntity(String jsonStr) throws WebException
    {
        return constructDummyEntity(DummyEntityJsBean.fromJsonString(jsonStr));
    }

    public DummyEntityJsBean constructDummyEntity(DummyEntityJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            DummyEntity dummyEntity = convertDummyEntityJsBeanToBean(jsBean);
            dummyEntity = getService().constructDummyEntity(dummyEntity);
            jsBean = convertDummyEntityToJsBean(dummyEntity);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws WebException
    {
        try {
            return getService().updateDummyEntity(guid, user, name, content, maxLength, expired, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateDummyEntity(String jsonStr) throws WebException
    {
        return updateDummyEntity(DummyEntityJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateDummyEntity(DummyEntityJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            DummyEntity dummyEntity = convertDummyEntityJsBeanToBean(jsBean);
            return getService().updateDummyEntity(dummyEntity);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public DummyEntityJsBean refreshDummyEntity(String jsonStr) throws WebException
    {
        return refreshDummyEntity(DummyEntityJsBean.fromJsonString(jsonStr));
    }

    public DummyEntityJsBean refreshDummyEntity(DummyEntityJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            DummyEntity dummyEntity = convertDummyEntityJsBeanToBean(jsBean);
            dummyEntity = getService().refreshDummyEntity(dummyEntity);
            jsBean = convertDummyEntityToJsBean(dummyEntity);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteDummyEntity(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteDummyEntity(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteDummyEntity(DummyEntityJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            DummyEntity dummyEntity = convertDummyEntityJsBeanToBean(jsBean);
            return getService().deleteDummyEntity(dummyEntity);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteDummyEntities(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteDummyEntities(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static DummyEntityJsBean convertDummyEntityToJsBean(DummyEntity dummyEntity)
    {
        DummyEntityJsBean jsBean = null;
        if(dummyEntity != null) {
            jsBean = new DummyEntityJsBean();
            jsBean.setGuid(dummyEntity.getGuid());
            jsBean.setUser(dummyEntity.getUser());
            jsBean.setName(dummyEntity.getName());
            jsBean.setContent(dummyEntity.getContent());
            jsBean.setMaxLength(dummyEntity.getMaxLength());
            jsBean.setExpired(dummyEntity.isExpired());
            jsBean.setStatus(dummyEntity.getStatus());
            jsBean.setCreatedTime(dummyEntity.getCreatedTime());
            jsBean.setModifiedTime(dummyEntity.getModifiedTime());
        }
        return jsBean;
    }

    public static DummyEntity convertDummyEntityJsBeanToBean(DummyEntityJsBean jsBean)
    {
        DummyEntityBean dummyEntity = null;
        if(jsBean != null) {
            dummyEntity = new DummyEntityBean();
            dummyEntity.setGuid(jsBean.getGuid());
            dummyEntity.setUser(jsBean.getUser());
            dummyEntity.setName(jsBean.getName());
            dummyEntity.setContent(jsBean.getContent());
            dummyEntity.setMaxLength(jsBean.getMaxLength());
            dummyEntity.setExpired(jsBean.isExpired());
            dummyEntity.setStatus(jsBean.getStatus());
            dummyEntity.setCreatedTime(jsBean.getCreatedTime());
            dummyEntity.setModifiedTime(jsBean.getModifiedTime());
        }
        return dummyEntity;
    }

}
