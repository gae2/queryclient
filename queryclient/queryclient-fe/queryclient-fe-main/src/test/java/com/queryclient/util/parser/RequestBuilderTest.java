package com.queryclient.util.parser;

import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.GUID;


public class RequestBuilderTest
{
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void testGetRequestUrl()
    {
        System.out.println(">>>>>>>>>> Begin: testGetRequestUrl()");
        
        String rootUri = null;
        QueryObject queryObj = null;
        RequestBuilder reqBuilder = null;
        URL requestUrl = null;
        String httpMethod = null;
        String payload = null;
        
        rootUri = "http://localhost/v1";
        queryObj = new QueryObject();
        queryObj.setMethod(QueryMethod.SELECT);
        queryObj.setEntity("tinyMemos");
        queryObj.setGuid(GUID.generate());
        reqBuilder = new RequestBuilder(rootUri, queryObj);
        try {
            requestUrl = reqBuilder.getRequestUrl();
            httpMethod = reqBuilder.getHttpMethod();
            payload = reqBuilder.getPayload();
        } catch (BaseException e) {
            e.printStackTrace();
        }
        System.out.println("requestUrl = " + requestUrl);
        System.out.println("httpMethod = " + httpMethod);
        System.out.println("payload = " + payload);
        
        rootUri = "http://localhost/v1/";
        queryObj = new QueryObject();
        queryObj.setMethod(QueryMethod.SELECT);
        queryObj.setEntity("tinyMemos");
        queryObj.setFilter("guid='abc'");
        queryObj.setUnique(true);
        queryObj.setOrdering("createdTime asc");
        reqBuilder = new RequestBuilder(rootUri, queryObj);
        try {
            requestUrl = reqBuilder.getRequestUrl();
            httpMethod = reqBuilder.getHttpMethod();
            payload = reqBuilder.getPayload();
        } catch (BaseException e) {
            e.printStackTrace();
        }
        System.out.println("requestUrl = " + requestUrl);
        System.out.println("httpMethod = " + httpMethod);
        System.out.println("payload = " + payload);

        rootUri = "http://localhost/v1/";
        queryObj = new QueryObject();
        queryObj.setMethod(QueryMethod.CREATE);
        queryObj.setEntity("tinyMemos");
        queryObj.setGuid(GUID.generate());
        queryObj.setPayload("{'abc':3, 'xyz':'hhh'}");
        reqBuilder = new RequestBuilder(rootUri, queryObj);
        try {
            requestUrl = reqBuilder.getRequestUrl();
            httpMethod = reqBuilder.getHttpMethod();
            payload = reqBuilder.getPayload();
        } catch (BaseException e) {
            e.printStackTrace();
        }
        System.out.println("requestUrl = " + requestUrl);
        System.out.println("httpMethod = " + httpMethod);
        System.out.println("payload = " + payload);

        rootUri = "http://localhost/v1";
        queryObj = new QueryObject();
        queryObj.setMethod(QueryMethod.CREATE);
        queryObj.setEntity("tinyMemos");
        queryObj.setPayload("{'abc':3, 'xyz':'hhh'}");
        reqBuilder = new RequestBuilder(rootUri, queryObj);
        try {
            requestUrl = reqBuilder.getRequestUrl();
            httpMethod = reqBuilder.getHttpMethod();
            payload = reqBuilder.getPayload();
        } catch (BaseException e) {
            e.printStackTrace();
        }
        System.out.println("requestUrl = " + requestUrl);
        System.out.println("httpMethod = " + httpMethod);
        System.out.println("payload = " + payload);

        rootUri = "http://localhost/v1/";
        queryObj = new QueryObject();
        queryObj.setMethod(QueryMethod.UPDATE);
        queryObj.setEntity("tinyMemos");
        queryObj.setGuid(GUID.generate());
        queryObj.setPayload("{'abc':3, 'xyz':'hhh'}");
        reqBuilder = new RequestBuilder(rootUri, queryObj);
        try {
            requestUrl = reqBuilder.getRequestUrl();
            httpMethod = reqBuilder.getHttpMethod();
            payload = reqBuilder.getPayload();
        } catch (BaseException e) {
            e.printStackTrace();
        }
        System.out.println("requestUrl = " + requestUrl);
        System.out.println("httpMethod = " + httpMethod);
        System.out.println("payload = " + payload);

        rootUri = "http://localhost/v1";
        queryObj = new QueryObject();
        queryObj.setMethod(QueryMethod.DELETE);
        queryObj.setEntity("tinyMemos");
        queryObj.setGuid(GUID.generate());
        reqBuilder = new RequestBuilder(rootUri, queryObj);
        try {
            requestUrl = reqBuilder.getRequestUrl();
            httpMethod = reqBuilder.getHttpMethod();
            payload = reqBuilder.getPayload();
        } catch (BaseException e) {
            e.printStackTrace();
        }
        System.out.println("requestUrl = " + requestUrl);
        System.out.println("httpMethod = " + httpMethod);
        System.out.println("payload = " + payload);

        rootUri = "http://localhost/v1/";
        queryObj = new QueryObject();
        queryObj.setMethod(QueryMethod.DELETE);
        queryObj.setEntity("tinyMemos");
        queryObj.setFilter("createdTime > 1000");
        reqBuilder = new RequestBuilder(rootUri, queryObj);
        try {
            requestUrl = reqBuilder.getRequestUrl();
            httpMethod = reqBuilder.getHttpMethod();
            payload = reqBuilder.getPayload();
        } catch (BaseException e) {
            e.printStackTrace();
        }
        System.out.println("requestUrl = " + requestUrl);
        System.out.println("httpMethod = " + httpMethod);
        System.out.println("payload = " + payload);
        
        
    }
}
