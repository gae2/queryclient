package com.queryclient.util.parser;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import com.queryclient.ws.BaseException;


public class QueryParserTest
{
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void testGetQueryObject()
    {
        System.out.println(">>>>>>>>>> Begin: testGetQueryObject()");
        
        String queryStmt = null;
        QueryObject queryObj = null;
        QueryParser parser = new QueryParser();

        queryStmt = "select from Memos where guid='abc-xyz' order by createdTime";
        System.out.println("queryStmt = " + queryStmt);
        parser.setQueryStatement(queryStmt);
        try {
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            System.out.println("*** Exception: " + e.getMessage());
            //e.printStackTrace();
        }
        System.out.println("queryObj = " + queryObj);

        queryObj = null;
        queryStmt = "select unique from Memos where guid='abc-xyz' group by createdTime offset 10 count 5";
        System.out.println("queryStmt = " + queryStmt);
        parser.setQueryStatement(queryStmt);
        try {
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            System.out.println("*** Exception: " + e.getMessage());
            //e.printStackTrace();
        }
        System.out.println("queryObj = " + queryObj);

        queryObj = null;
        queryStmt = "select unique f1, f2 from Memos where guid='abc-xyz' range 30, 40";
        System.out.println("queryStmt = " + queryStmt);
        parser.setQueryStatement(queryStmt);
        try {
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            System.out.println("*** Exception: " + e.getMessage());
            //e.printStackTrace();
        }
        System.out.println("queryObj = " + queryObj);

        queryObj = null;
        queryStmt = "select count( Memos.guid )   from Memos order   by guid";
        System.out.println("queryStmt = " + queryStmt);
        parser.setQueryStatement(queryStmt);
        try {
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            System.out.println("*** Exception: " + e.getMessage());
            //e.printStackTrace();
        }
        System.out.println("queryObj = " + queryObj);

        queryObj = null;
        queryStmt = "create Memos value {'abc':'xyz', 'kkk':[1,2,3]}";
        System.out.println("queryStmt = " + queryStmt);
        parser.setQueryStatement(queryStmt);
        try {
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            System.out.println("*** Exception: " + e.getMessage());
            //e.printStackTrace();
        }
        System.out.println("queryObj = " + queryObj);

        queryObj = null;
        queryStmt = "update Memos guid ab-xy-12 value {'abc':'xyz', 'kkk':[1,2,3]}";
        System.out.println("queryStmt = " + queryStmt);
        parser.setQueryStatement(queryStmt);
        try {
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            System.out.println("*** Exception: " + e.getMessage());
            //e.printStackTrace();
        }
        System.out.println("queryObj = " + queryObj);

        queryObj = null;
        queryStmt = "update Memos value {'abc':'xyz', 'kkk':[1,2,3]}";
        System.out.println("queryStmt = " + queryStmt);
        parser.setQueryStatement(queryStmt);
        try {
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            System.out.println("*** Exception: " + e.getMessage());
            //e.printStackTrace();
        }
        System.out.println("queryObj = " + queryObj);

        queryObj = null;
        queryStmt = "delete Memos guid \"ab-xy-12\"";
        System.out.println("queryStmt = " + queryStmt);
        parser.setQueryStatement(queryStmt);
        try {
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            System.out.println("*** Exception: " + e.getMessage());
            //e.printStackTrace();
        }
        System.out.println("queryObj = " + queryObj);

        queryObj = null;
        queryStmt = "delete Memos guid where createdDate >   'May 10th 2011'  ";
        System.out.println("queryStmt = " + queryStmt);
        parser.setQueryStatement(queryStmt);
        try {
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            System.out.println("*** Exception: " + e.getMessage());
            //e.printStackTrace();
        }
        System.out.println("queryObj = " + queryObj);

        queryObj = null;
        queryStmt = "delete    Memos  ";
        System.out.println("queryStmt = " + queryStmt);
        parser.setQueryStatement(queryStmt);
        try {
            queryObj = parser.getQueryObject();
        } catch (BaseException e) {
            System.out.println("*** Exception: " + e.getMessage());
            //e.printStackTrace();
        }
        System.out.println("queryObj = " + queryObj);
        
    }

}
