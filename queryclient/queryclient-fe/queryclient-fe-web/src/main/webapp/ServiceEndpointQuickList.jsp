<%@ page import="com.queryclient.af.util.*, com.queryclient.af.auth.*, com.queryclient.fe.*, com.queryclient.fe.bean.*, com.queryclient.wa.service.*, com.queryclient.util.*, com.queryclient.helper.*" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page errorPage="/error/UnknownError.jsp" 
%><%
//{1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

//[2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String serverName = request.getServerName();
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();

%><%

// TBD:
// url pattern: /query/<query session id> ????
// ...
%><%

// ...
String serviceURL = TargetServiceHelper.getInstance().getServiceUrlFromServletPath(servletPath);
// This could be null...

%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Web Service Endpoint Quick List</title>
    <meta name="author" content="Aery Software">
    <meta name="keywords" content="Query Client, REST, Web service, WSQL">
    <meta name="description" content="QueryClient is an online query application for Web services. Web Service Query Language. App Engine Studio.">

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">


    <!-- Le styles -->
    <link href="/css/bootstrap-2.0.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="/css/redmond/jquery-ui-1.8.16.custom.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/queryclient.css"/>
    <link rel="stylesheet" type="text/css" href="/css/queryclient-screen.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/queryclient-mobile.css" media="only screen and (max-device-width: 480px)"/>

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-queryclient.ico" />
	<link rel="icon" href="/img/favicon-queryclient.ico" type="image/x-icon" />

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="/js/libs/modernizr-2.0.6.min.js"></script>

    <!--  Twitter @anywhere API  -->
    <script src="http://platform.twitter.com/anywhere.js?id=nFIDbtGohh1rblnAC3dhQA&v=1" type="text/javascript"></script>
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="/">Query Client</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li><a href="/">Home</a></li>
<!-- 
              <li><a href="/about">About</a></li>
 -->
              <li><a href="/contact">Contact</a></li>
              <li><a href="http://blog.queryclient.com/">Blog</a></li>
            </ul>
            <p class="navbar-text pull-right">
              &nbsp;
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">

 <div id="queryclient_main_header">
 <h2 id="queryclient_title">
Service Endpoints
 </h2>
 </div>


<div id="serviceendpoints_table_div">

<table class="table">
<tr>
<td>Service Name</td>
<td>Service URL</td>
<td>Created Time</td>
</tr>
<%
java.util.List<ServiceEndpointJsBean> endpoints = ServiceEndpointHelper.getInstance().findRecentServiceEndpoints(0L, 200);   // ???
if(endpoints != null && !endpoints.isEmpty()) {
    for(ServiceEndpointJsBean p : endpoints) {
        // ?????
        String serviceName = p.getServiceName();
        String serviceUrl = p.getServiceUrl();
        Long createdTime = p.getCreatedTime();
        String createdDateTime = DateUtil.formatDate(createdTime);
%>
   <tr>
      <td><%=serviceName%></td>
      <td><%=serviceUrl%></td>
      <td><%=createdDateTime%></td>
   </tr>
<%
    }
}
%>

<!--  
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
-->

</table>

</div>
</div>

      <hr>

      <footer>
        <p>&copy; Query Client 2012</p>
      </footer>

    </div> <!-- /container -->


    
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://www.filestoa.com/js/jquery/jquery-1.7.1.min.js"><\/script>')</script>
	<script src="http://www.filestoa.com/js/jquery/jquery-ui-1.8.16.all.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://www.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script src="/js/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="/js/history.js/uncompressed/history.js"></script>
    <script src="/js/history.js/uncompressed/history.html4.js"></script>
	<script defer src="/js/plugins.js"></script>
    <script defer src="/js/script.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/dataservicejsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/serviceendpointjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/querysessionjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/queryrecordjsbean-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/statushelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/emailhelper-1.0.js"></script>    
    <!-- end scripts-->


    </body>
</html>