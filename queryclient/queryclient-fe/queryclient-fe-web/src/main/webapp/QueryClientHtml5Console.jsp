<%@ page import="com.queryclient.af.util.*, com.queryclient.af.auth.*, com.queryclient.fe.*, com.queryclient.fe.bean.*, com.queryclient.wa.service.*, com.queryclient.util.*, com.queryclient.helper.*" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page errorPage="/error/UnknownError.jsp" 
%><%
//{1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

//[2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String serverName = request.getServerName();
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();

%><%

// TBD:
// url pattern: /query/<query session id> ????
// ...
%><%

// ...
String serviceURL = TargetServiceHelper.getInstance().getServiceUrlFromServletPath(servletPath);
// This could be null...

%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title>Query Console</title>
    <meta name="author" content="Aery Software">
    <meta name="keywords" content="Query Client, REST, Web service, WSQL">
    <meta name="description" content="QueryClient is an online query application for Web services. Web Service Query Language. App Engine Studio.">

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">


    <!-- Le styles -->
    <link href="/css/bootstrap-2.0.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="/css/redmond/jquery-ui-1.8.16.custom.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/queryclient.css"/>
    <link rel="stylesheet" type="text/css" href="/css/queryclient-screen.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/queryclient-mobile.css" media="only screen and (max-device-width: 480px)"/>
<!-- 
    <link href="/css/bootstrap.responsive-2.0.css" rel="stylesheet">
 -->

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-queryclient.ico" />
	<link rel="icon" href="/img/favicon-queryclient.ico" type="image/x-icon" />

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="/js/libs/modernizr-2.0.6.min.js"></script>

    <!--  Twitter @anywhere API  -->
    <script src="http://platform.twitter.com/anywhere.js?id=nFIDbtGohh1rblnAC3dhQA&v=1" type="text/javascript"></script>
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="/">Query Client</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li><a href="/">Home</a></li>
<!-- 
              <li><a href="/about">About</a></li>
 -->
              <li><a href="/contact">Contact</a></li>
              <li><a href="http://blog.queryclient.com/">Blog</a></li>
            </ul>
            <p class="navbar-text pull-right">
              <a id="anchor_topmenu_tweet" href="#" title="Share it on Twitter"><i class="icon-retweet icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_share" href="#" title="Email it to your friend"><i class="icon-share icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_email" href="contact:info+queryclient+com?subject=Re: Query Client" title="Email us"><i class="icon-envelope icon-white"></i></a>
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>


    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
      <form id="form_queryclient_console" method="POST" action="">

      <div id="queryclient_console_header">
      <fieldset name="fieldset_console_header">
        <table>
        <tr>
        <td><label for="input_console_serviceurl">Service URL:</label></td>
        <td>
        <div id="queryclient_console_serviceurl">
<%
if(serviceURL != null) {
%>
          <input type="text" name="input_console_serviceurl" id="input_console_serviceurl" size="85" readonly="readonly" value="<%=serviceURL%>" style="width:300px"></input>
<%
} else {
%>
          <input type="text" name="input_console_serviceurl" id="input_console_serviceurl" size="85" readonly="readonly" value="(Required)" style="width:300px"></input>
          <button class="btn" type="button" name="button_console_serviceurl" id="button_console_serviceurl" title="Set Service URL" style="width:80px">Set</button>
          <button class="btn" type="button" name="button_console_dataservice" id="button_console_dataservice" title="Add a new Service" style="width:80px">Add</button>
<%
}
%>
        </div>
        </td>
        </tr>
        </table>
      </fieldset>
      </div>

      <div id="queryclient_console_body">
      <fieldset name="fieldset_console_body">
        <div id="console_body_top">
        <table>
          <tr>
          <td>
            <label for="input_console_query" id="input_console_query_label">Query:</label>
          </td>
          <td>
            <div id="console_query">
              <textarea name="input_console_query" id="input_console_query" cols="120" rows="6" style="width:550px"></textarea>
            </div>
          </td>
          <td style="vertical-align:bottom;">
            <button class="btn" type="button" name="button_console_query" id="button_console_query" title="Submit the query" style="width:80px">Query</button>
          </td>
          </tr>           
        </table>
        </div>
        <div id="console_body_main" class="console_body_main_output">
          <div id="console_query_response">
              <span id="console_query_response_status">(Response status)</span>
          </div>
          <div id="console_output">
              <textarea name="input_console_output" id="input_console_output" readonly="readonly" cols="125" rows="24" style="width:700px"></textarea>
          </div>
        </div>
      </fieldset>
      </div>
      
      <div id="console_footer">
      </div>

      </form>
      </div>

      <hr>

      <footer>
        <p>&copy; Query Client 2012&nbsp; <a id="anchor_feedback_footer" href="#" title="Feedback"><i class="icon-comment"></i></a></p>
      </footer>

    </div> <!-- /container -->



	<div id="div_serviceinfo_dialog" title="Set Service Info">
		<H3>
		Service Endpoint URL
		</H3>
		<form id="form_serviceinfo_dialog">
		<fieldset name="fieldset_serviceinfo_dialog">
			<div id="div_serviceinfo_serviceurl">
	          <label for="input_serviceinfo_serviceurl">Service URL:</label>
<%
if(serviceURL != null) {
%>
	          <input type="text" name="input_serviceinfo_serviceurl" id="input_serviceinfo_serviceurl" readonly="readonly" size="40" value="<%=serviceURL%>"></input>
<%
} else {
%>
	          <input type="text" name="input_serviceinfo_serviceurl" id="input_serviceinfo_serviceurl" size="40" value=""></input>
<%
}
%>
			</div>
			<div id="div_serviceinfo_inputformat">
	          <label for="input_serviceinfo_inputformat">Input MIME Type:</label>
	          <input type="text" name="input_serviceinfo_inputformat" id="input_serviceinfo_inputformat" size="30" value="JSON" readonly></input>
			</div>
			<div id="div_serviceinfo_outputformat">
	          <label for="input_serviceinfo_outputformat">Output MIME Type:</label>
	          <input type="text" name="input_serviceinfo_outputformat" id="input_serviceinfo_outputformat" size="30" value="JSON" readonly></input>
			</div>
		</fieldset>
	    </form>
	</div>



<%
if(serviceURL == null) {
%>
	<div id="div_dataservice_dialog" title="Add New Service">
		<H3>
		Web Service Endpoint
		</H3>
		<form id="form_dataservice_dialog">
		<fieldset name="fieldset_dataservice_dialog">
			<div id="div_dataservice_servicename">
	          <label for="input_dataservice_servicename">Service Name:</label>
	          <input type="text" name="input_dataservice_servicename" id="input_dataservice_servicename" size="50" value=""></input>
			</div>
			<div id="div_dataservice_serviceurl">
	          <label for="input_dataservice_serviceurl">Service Endpoint URL:</label>
	          <input type="text" name="input_dataservice_serviceurl" id="input_dataservice_serviceurl" size="50" value=""></input>
			</div>
			<div id="div_dataservice_consumerkey">
	          <label for="input_dataservice_consumerkey">Consumer Key:</label>
	          <input type="text" name="input_dataservice_consumerkey" id="input_dataservice_consumerkey" size="40" value=""></input>
			</div>
			<div id="div_dataservice_consumersecret">
	          <label for="input_dataservice_consumersecret">Consumer Secret:</label>
	          <input type="text" name="input_dataservice_consumersecret" id="input_dataservice_consumersecret" size="40" value=""></input>
			</div>
		</fieldset>
	    </form>
	</div>
<%
}
%>



    <div id="div_status_floater">
      <div id="status_message">
        <span id="span_status_message">(Status)</span>
      </div>
    </div>
    

    
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://www.filestoa.com/js/jquery/jquery-1.7.1.min.js"><\/script>')</script>
	<script src="http://www.filestoa.com/js/jquery/jquery-ui-1.8.16.all.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://www.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script src="/js/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="/js/history.js/uncompressed/history.js"></script>
    <script src="/js/history.js/uncompressed/history.html4.js"></script>
	<script defer src="/js/plugins.js"></script>
    <script defer src="/js/script.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/dataservicejsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/serviceendpointjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/querysessionjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/queryrecordjsbean-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/statushelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/emailhelper-1.0.js"></script>    
    <!-- end scripts-->


    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "Query Client";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "contact";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);

    	var targetService = "Query Client";
    	var targetPage = "contact";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "Query Client";
    	var subject = "Interesting site: Query Client";
    	var defaultMessage = "Hi,\n\nQuery Client, http://www.queryclient.com/, is a cloud-based wsql query client. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_mainmenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_share").click(function() {
            if(DEBUG_ENABLED) console.log("Share button clicked."); 
       	    emailHelper.email();
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_tweet").click(function() {
            if(DEBUG_ENABLED) console.log("Tweet button clicked."); 

            if(!tweetHelper) {
                tweetHelper = new webstoa.TweetHelper();
            }
        	if(tweetHelper) {
	            var tweetTitle = 'Query Client';
	            var tweetMessage = 'Query Client, http://www.queryclient.com/, is a cloud-based wsql query client. You can contact them at @queryclient.';
	            tweetHelper.tweet(tweetTitle, tweetMessage);
        	} else {
        		// ????
        	}
		    return false;
        });
    });
    </script>



    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // "Init"
    $(function() {
    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Hide the service info dialog.
    	$("#div_serviceinfo_dialog").hide();
    	$("#div_dataservice_dialog").hide();
    	
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        //$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 530, y: 65, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// Focus the url set button.
    	$("#button_console_serviceurl").focus();
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(21889, 12);  // ???
    	//fiveTenTimer.start();
    });
    </script>

    <script>
    // Global vars...
    var queryId = 0;
    var querySessionJsBean = new queryclient.wa.bean.QuerySessionJsBean();
    //var querySessionGuid = querySessionJsBean.getGuid();
    var userGuid;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
    if(userGuid) {
    	querySessionJsBean.setUser(userGuid);
    }
<%
if(serviceURL != null) {
%>
    querySessionJsBean.setServiceUrl("<%=serviceURL%>");
<%
}
%>
    // TBD:
    // Save querySessionJsBean
    // ....
    </script>

<%
if(serviceURL == null) {
%>
    <script>
    // Event handlers
    $(function() {
    	$("#button_console_serviceurl").click(function() {
    	    if(DEBUG_ENABLED) console.log("ServiceUrl button clicked.");
    					    
			$( "#div_serviceinfo_dialog" ).dialog({
				height: 300,
				width: 400,
				modal: true,
			    buttons: {
					 Update: function() {
						 
						// Creat a new bean...  ????
						querySessionJsBean = new queryclient.wa.bean.QuerySessionJsBean();
					    if(userGuid) {   
					    	querySessionJsBean.setUser(userGuid);
					    }
						 
			    		var serviceUrl = $("#input_serviceinfo_serviceurl").val();
			    		if(serviceUrl != '') {
			        	    if(DEBUG_ENABLED) console.log('serviceUrl set to ' + serviceUrl);
			   		        //updateStatus('serviceUrl set to ' + serviceUrl, 'info', 5550);
			   		        // TBD: validation???
			   		        // TBD: If a serviceName is provided, fetch serviceUrl first from the server ???
			   		        querySessionJsBean.setServiceUrl(serviceUrl);
			    		} else {
			    			// ???
			    		}
			    		var inputFormat = $("#input_serviceinfo_inputformat").val();
			    		if(inputFormat != '') {
			        	    if(DEBUG_ENABLED) console.log('inputFormat set to ' + inputFormat);
			   		        //updateStatus('inputFormat set to ' + inputFormat, 'info', 5550);
			   		        // TBD: validation???
			   		        querySessionJsBean.setInputFormat(inputFormat);
			    		} else {
			    			// ???
			    		}
			    		var outputFormat = $("#input_serviceinfo_outputformat").val();
			    		if(outputFormat != '') {
			        	    if(DEBUG_ENABLED) console.log('outputFormat set to ' + outputFormat);
			   		        //updateStatus('outputFormat set to ' + outputFormat, 'info', 5550);
			   		        // TBD: validation???
			   		        querySessionJsBean.setOutputFormat(outputFormat);
			    		} else {
			    			// ???
			    		}
			    		
			    		// Update the bean.
			    		updateQuerySessionBean();
			    		
			    		// Update the main form fields...
						// $("#input_console_serviceurl").val(serviceUrl);
						$("#input_console_serviceurl").val('');
				        resetAllFormFields();

			    		// Close the dialog....
    					$(this).dialog('close'); 
						 
					 },
					 Cancel: function() {
						 $(this).dialog('close'); 
						 //aform.resetForm();
					 }
				}
			});

    		return false;
    	});

    });
    </script>
    
    
    <script>
    // Event handlers
    $(function() {
    	$("#button_console_dataservice").click(function() {
    	    if(DEBUG_ENABLED) console.log("DataService button clicked.");
    	    
    	    $("#input_dataservice_servicename").val('');
    	    $("#input_dataservice_serviceurl").val('');
    	    $("#input_dataservice_consumerkey").val('');
    	    $("#input_dataservice_consumersecret").val('');
    	    
			$( "#div_dataservice_dialog" ).dialog({
				height: 360,
				width: 400,
				modal: true,
			    buttons: {
					 'Create Service': function() {

			    	    var serviceName = $("#input_dataservice_servicename").val().trim();
			    	    var serviceUrl = $("#input_dataservice_serviceurl").val().trim();
			    	    var consumerKey = $("#input_dataservice_consumerkey").val().trim();
			    	    var consumerSecret = $("#input_dataservice_consumersecret").val().trim();
			    	    // TBD: Can name be empty ???
			    	    if(! serviceUrl) {
			    	    	updateStatus('Service URL is not set.', 'error', 5550);
	    					$(this).dialog('close'); 
			    	    	return false;
			    	    }
			    	    // if(consumerKey=='' || consumerSecret=='') {
			    	    //	updateStatus('Auth credential is not set.', 'error', 5550);
	    				//	$(this).dialog('close'); 
			    	    //	return false;
			    	    //}

				    	// Creat a new bean...  ????
						var dataServiceJsBean = new queryclient.wa.bean.DataServiceJsBean();
					    if(userGuid) {   
					        dataServiceJsBean.setUser(userGuid);
					    }
						 
			    		if(serviceName != '') {
			        	    if(DEBUG_ENABLED) console.log('serviceName set to ' + serviceName);
			   		        //updateStatus('serviceName set to ' + serviceName, 'info', 5550);
			   		        // TBD: validation???
			   		        dataServiceJsBean.setName(serviceName);
			    		}
			    		if(serviceUrl != '') {
			        	    if(DEBUG_ENABLED) console.log('serviceUrl set to ' + serviceUrl);
			   		        //updateStatus('serviceUrl set to ' + serviceUrl, 'info', 5550);
			   		        // TBD: validation???
			   		        dataServiceJsBean.setServiceUrl(serviceUrl);
			    		}
			    		if(consumerKey != '' && consumerSecret != '') {
			        	    if(DEBUG_ENABLED) console.log('consumerKey set to ' + consumerKey);
			        	    if(DEBUG_ENABLED) console.log('consumerSecret set to ' + consumerSecret);
			   		        //updateStatus('consumerKey set to ' + consumerKey, 'info', 5550);
			   		        //updateStatus('consumerSecret set to ' + consumerSecret, 'info', 5550);
			   		        // TBD: validation???
			   		        var authCredential = {'consumerKey': consumerKey, 'consumerSecret': consumerSecret};
			   		        // var authCredential = {};
			   		        // authCredential['consumerKey'] = consumerKey;
			   		        // authCredential['consumerSecret'] = consumerSecret;
			   		        dataServiceJsBean.setAuthCredential(authCredential);
			    		}
			    		
			    		// Update the bean.
			    		createDataServiceBean(dataServiceJsBean);
			    		
			    		// TBD: Update the query session with the new serviceUrl ????
			    		// Update the main form fields...
						// $("#input_console_serviceurl").val(serviceUrl);
				        // resetAllFormFields();

			    		// Close the dialog....
    					$(this).dialog('close'); 
						 
					 },
					 'Add Service URL': function() {

				    	    var serviceName = $("#input_dataservice_servicename").val();
				    	    var serviceUrl = $("#input_dataservice_serviceurl").val();
				    	    var consumerKey = $("#input_dataservice_consumerkey").val();
				    	    var consumerSecret = $("#input_dataservice_consumersecret").val();
				    	    if(serviceName == '') {
					    	    if(! serviceUrl) {
					    	    	updateStatus('Service URL is not set.', 'error', 5550);
			    					$(this).dialog('close'); 
					    	    	return false;
					    	    }
					    	    if(consumerKey=='' || consumerSecret=='') {
					    	    	updateStatus('Auth credential is not set.', 'error', 5550);
			    					$(this).dialog('close'); 
					    	    	return false;
					    	    }
				    	    }

				    	    // Creat a new bean...  ????
							var serviceEndpointJsBean = new queryclient.wa.bean.ServiceEndpointJsBean();
						    if(userGuid) {   
						    	serviceEndpointJsBean.setUser(userGuid);
						    }
							 
				    		if(serviceName != '') {
				        	    if(DEBUG_ENABLED) console.log('serviceName set to ' + serviceName);
				   		        //updateStatus('serviceName set to ' + serviceName, 'info', 5550);
				   		        // TBD: validation???
				   		        serviceEndpointJsBean.setServiceName(serviceName);
				    		}
				    		if(serviceUrl != '') {
				        	    if(DEBUG_ENABLED) console.log('serviceUrl set to ' + serviceUrl);
				   		        //updateStatus('serviceUrl set to ' + serviceUrl, 'info', 5550);
				   		        // TBD: validation???
				   		        serviceEndpointJsBean.setServiceUrl(serviceUrl);
				    		}
				    		if(consumerKey != '' && consumerSecret != '') {
				        	    if(DEBUG_ENABLED) console.log('consumerKey set to ' + consumerKey);
				        	    if(DEBUG_ENABLED) console.log('consumerSecret set to ' + consumerSecret);
				   		        //updateStatus('consumerKey set to ' + consumerKey, 'info', 5550);
				   		        //updateStatus('consumerSecret set to ' + consumerSecret, 'info', 5550);
				   		        // TBD: validation???
				   		        var authCredential = {'consumerKey': consumerKey, 'consumerSecret': consumerSecret};
				   		        // var authCredential = {};
				   		        // authCredential['consumerKey'] = consumerKey;
				   		        // authCredential['consumerSecret'] = consumerSecret;
				   		        serviceEndpointJsBean.setAuthCredential(authCredential);
				    		}
				    		
				    		// Update the bean.
				    		createServiceEndpointBean(serviceEndpointJsBean);
				    		
				    		// TBD: Update the query session with the new serviceUrl ????
				    		// Update the main form fields...
							// $("#input_console_serviceurl").val(serviceUrl);
					        // resetAllFormFields();

				    		// Close the dialog....
	    					$(this).dialog('close'); 
							 
					 },
					 Cancel: function() {
						 $(this).dialog('close'); 
						 //aform.resetForm();
					 }
				}
			});

    		return false;
    	});

    });
    </script>
 
<%
}
%>
    <script>
    var createDataServiceBean = function(dataServiceJsBean) {
  	    if(DEBUG_ENABLED) console.log("createDataServiceBean() called......");

  	    // Payload...
        var payload = JSON.stringify(dataServiceJsBean);
        if(DEBUG_ENABLED) console.log("payload = " + payload);

        // Web service settings.
        var dataServiceEndPoint = "<%=topLevelUrl%>" + "wa/dataservice";
        if(DEBUG_ENABLED) console.log("dataServiceEndPoint =" + dataServiceEndPoint);

          $.ajax({
      	    type: "POST",
      	    url: dataServiceEndPoint,
      	    data: payload,
      	    dataType: "json",
    	    contentType: "application/json; charset=UTF-8",
      	    success: function(data, textStatus) {
      	      // TBD
      	      if(DEBUG_ENABLED) console.log("dataServiceJsBean successfully saved. textStatus = " + textStatus);
      	      if(DEBUG_ENABLED) console.log("data = " + data);
      	      if(DEBUG_ENABLED) console.log(data);

      	      // TBD: Browser History ???

      	      // Parse data...
      	      if(data) {   // POST only..  	    	      
  	    	      // TBD:
  	              updateStatus('dataServiceJsBean successfully created.', 'info', 5550);
      	      }
      	    }
  	   	  });

    };
    </script>

    <script>
    var createServiceEndpointBean = function(serviceEndpointJsBean) {
  	    if(DEBUG_ENABLED) console.log("createServiceEndpointBean() called......");

  	    // Payload...
        var payload = JSON.stringify(serviceEndpointJsBean);
        if(DEBUG_ENABLED) console.log("payload = " + payload);

        // Web service settings.
        var serviceEndpointEndPoint = "<%=topLevelUrl%>" + "wa/serviceendpoint";
        if(DEBUG_ENABLED) console.log("serviceEndpointEndPoint =" + serviceEndpointEndPoint);

          $.ajax({
      	    type: "POST",
      	    url: serviceEndpointEndPoint,
      	    data: payload,
      	    dataType: "json",
    	    contentType: "application/json; charset=UTF-8",
      	    success: function(data, textStatus) {
      	      // TBD
      	      if(DEBUG_ENABLED) console.log("serviceEndpointJsBean successfully saved. textStatus = " + textStatus);
      	      if(DEBUG_ENABLED) console.log("data = " + data);
      	      if(DEBUG_ENABLED) console.log(data);

      	      // TBD: Browser History ???

      	      // Parse data...
      	      if(data) {   // POST only..  	    	      
  	    	      // TBD:
  	              updateStatus('serviceEndpointJsBean successfully created.', 'info', 5550);
      	      }    	      
      	    }
  	   	  });
  	    
    };
    </script>

    <script>
    // Refresh input fields...
    var updateQuerySessionBean = function() {
  	    if(DEBUG_ENABLED) console.log("updateQuerySessionBean() called......");

  	    // Payload...
        var payload = JSON.stringify(querySessionJsBean);
        if(DEBUG_ENABLED) console.log("payload = " + payload);

        // Web service settings.
        var sessionPostEndPoint = "<%=topLevelUrl%>" + "wa/querysession";
        if(DEBUG_ENABLED) console.log("sessionPostEndPoint =" + sessionPostEndPoint);

          $.ajax({
      	    type: "POST",    // Always post/create rather than put/update ????
      	    url: sessionPostEndPoint,
      	    data: payload,
      	    dataType: "json",
    	    contentType: "application/json; charset=UTF-8",
      	    success: function(data, textStatus) {
      	      // TBD
      	      if(DEBUG_ENABLED) console.log("sessionJsBean successfully saved. textStatus = " + textStatus);
      	      if(DEBUG_ENABLED) console.log("data = " + data);
      	      if(DEBUG_ENABLED) console.log(data);

      	      // TBD: Browser History ???

      	      // Parse data...
      	      if(data) {   // POST only..
  	    	      // Replace/Update the JsBeans ...

  	    	      var querySessionObj = data;
  	    	      if(DEBUG_ENABLED) console.log("querySessionObj = " + querySessionObj);
  	    	      if(DEBUG_ENABLED) console.log(querySessionObj);
  	    	      querySessionJsBean = queryclient.wa.bean.QuerySessionJsBean.create(querySessionObj);
  	    	      if(DEBUG_ENABLED) console.log("New querySessionJsBean = " + querySessionJsBean);
  	    	      if(DEBUG_ENABLED) console.log(querySessionJsBean);
  	    	      
  	    	      // update/reset relevant vars???
  	    	      queryId = 0;  // ???
  	    	      
  	    	      // TBD:
  	              updateStatus('Query Session successfully updated.', 'info', 5550);

	    		  // Update the main form fields...
  	    	      var serviceUrl = querySessionJsBean.getServiceUrl();
				  $("#input_console_serviceurl").val(serviceUrl);
			      // resetAllFormFields();
			      // ...

      	      }    	      
      	    }
  	   	  });
  	    
    };
    </script>

    <script>
    $(function() {
    	$(window).bind('statechange', function() {
    		if(DEBUG_ENABLED) console.log('>>> statechange called.');

    		var History = window.History;
  	        if(History) {
        	    var State = History.getState();
        	    if(State) {
        	    	//var queryRecordJsBean = State.recordBean;
        	    	//if(queryRecordJsBean) {
					//	// TBD: ....
        	    	//	refreshFormFields(queryRecordJsBean);
        	    	//}
        	    	var stateObj = State.data;
        	    	if(stateObj) {
        	    		var query = stateObj.query;        	    	
        	    		if(query) {
        	    			if(DEBUG_ENABLED) console.log('>>> query = ' + query);
							// TBD: ....
        	    			$("#input_console_query").val(query);
        	    			$("#input_console_output").val('');
       	    		        $("#console_query_response_status").text('...');
        	    			// ....
        	    		} else {
        	    			if(DEBUG_ENABLED) console.log('>>> query is null');
        	    		}
        	    	} else {
    	    			if(DEBUG_ENABLED) console.log('>>> stateObj is null');        	    		
        	    	}
        	    } else {
    	    		if(DEBUG_ENABLED) console.log('>>> state is null');
        	    }
  	        } else {
	    		if(DEBUG_ENABLED) console.log('>>> history is null');
  	        }
    	});
    });
    </script>

    <script>
    // Ajax form handlers.
  $(function() {
    $("#button_console_query").click(function() {
      // TBD:
      // Disable the button here
      // and enable again (some delay) after ajax call returns
      //     or it times out (because the call has failed, etc.).
      
    
      if(DEBUG_ENABLED) console.log("Query button clicked.");
      updateStatus('Sending the query...', 'info', 5550);
      
      resetOutputFormFields();
      // TBD: Disable the query button here???
      //      and enable it after a certain time or when the output returns...

      // TBD: get data from querySession
      //var serviceUrl = $("#input_console_serviceurl").val();
      var serviceUrl = querySessionJsBean.getServiceUrl();
      var inputFormat = querySessionJsBean.getInputFormat();
      var outputFormat = querySessionJsBean.getOutputFormat();
      
      if(serviceUrl == null || serviceUrl == '') {
          if(DEBUG_ENABLED) console.log("serviceUrl is empty");
          updateStatus('serviceUrl cannot be empty.', 'error', 5550);
    	  return false;
      }

      // validate and process form here      
      var query = $("#input_console_query").val().trim();
      if(DEBUG_ENABLED) console.log("query = " + query);
      if(query == '') {
          if(DEBUG_ENABLED) console.log("query is empty");
          updateStatus('Query cannot be empty.', 'error', 5550);
    	  return false;
      }
      // etc.

      
      // Create a queryrecord bean
      // Update the beans with input values
      var queryRecordJsBean = new queryclient.wa.bean.QueryRecordJsBean();
      var queryRecordGuid = queryRecordJsBean.getGuid();
      queryRecordJsBean.setServiceUrl(serviceUrl);   // Service url cannot be null..
      if(inputFormat != null && inputFormat == '') {
    	  queryRecordJsBean.setInputFormat(inputFormat);
      }
      if(outputFormat != null && outputFormat == '') {
    	  queryRecordJsBean.setOutputFormat(outputFormat);
      }
      queryRecordJsBean.setQuery(query);
      //if(userGuid) {   
      //  queryRecordJsBean.setUser(userGuid);
      //}
      queryRecordJsBean.setQuerySession(querySessionJsBean.getGuid());
      queryRecordJsBean.setQueryId(++queryId);
      queryRecordJsBean.setDelayed(false);
      // ...
      
   	  // Update the modifiedTime field ....
//   	  //if(queryId > 0) {
//   		  var modifiedTime = (new Date()).getTime();
//          querySessionJsBean.setModifiedTime(modifiedTime);
//   	  //}
      // TBD: save querySessionJsBean ???

      // Payload...
      var payload = JSON.stringify(queryRecordJsBean);
      if(DEBUG_ENABLED) console.log("payload = " + payload);
      
      
      // Web service settings.
      var queryPostEndPoint = "<%=topLevelUrl%>" + "wa/queryrecord";
      if(DEBUG_ENABLED) console.log("queryPostEndPoint =" + queryPostEndPoint);
      var httpMethod = "POST";
      var webServiceUrl = queryPostEndPoint;

        $.ajax({
    	    type: httpMethod,
    	    url: webServiceUrl,
    	    data: payload,
    	    dataType: "json",
    	    contentType: "application/json",
    	    success: function(data, textStatus) {
    	      // TBD
    	      if(DEBUG_ENABLED) console.log("queryJsBean successfully saved. textStatus = " + textStatus);
    	      if(DEBUG_ENABLED) console.log("data = " + data);
    	      if(DEBUG_ENABLED) console.log(data);

    	      // History. TBD....
    	      var History = window.History;
    	      if(History) {
   	    	    	// TBD: Need to html escape????
    				////var htmlPageTitle = 'Query - ' + queryRecordJsBean.getGuid();
    				var htmlPageTitle = 'Query - ' + queryRecordJsBean.getQueryId();
    	            // TBD: This does not work on HTML4 browsers (e.g., IE)
			        ////History.pushState({recordBean:queryRecordJsBean}, htmlPageTitle, "/query/" + queryRecordJsBean.getGuid());
			        //History.pushState({recordBean:queryRecordJsBean}, htmlPageTitle, "/query/" + queryRecordJsBean.getQueryId());
			        
			        var stateObj = {id:queryRecordJsBean.getQueryId(), query:queryRecordJsBean.getQuery()};
    				History.pushState(stateObj, htmlPageTitle, "<%=servletPath%>" + "/" + queryRecordJsBean.getQueryId());
    	      }

    	      // Parse data...
    	      if(data) {   // POST only..
	    	      // Replace/Update the JsBeans ...

	    	      var queryRecordObj = data;
	    	      if(DEBUG_ENABLED) console.log("queryRecordObj = " + queryRecordObj);
	    	      if(DEBUG_ENABLED) console.log(queryRecordObj);
	    	      queryRecordJsBean = queryclient.wa.bean.QueryRecordJsBean.create(queryRecordObj);
	    	      if(DEBUG_ENABLED) console.log("New queryRecordJsBean = " + queryRecordJsBean);
	    	      if(DEBUG_ENABLED) console.log(queryRecordJsBean);
	    	      
	    	      // TBD:
	    	      refreshFormFields(queryRecordJsBean); 
	              updateStatus('Query successfully processed.', 'info', 5550);
    	      } else {
    	    	  // ???????
	              updateStatus('Query processed.', 'info', 5550);
    	      }    	      
    	    }
	   	  });
    });
  });
  </script>

  <script>
  // Reset input fields...
  var resetAllFormFields = function() {
      if(DEBUG_ENABLED) console.log("resetAllFormFields() called......");

      $("#input_console_query").text('');
      $("#console_query_response_status").text('...');
      $("#input_console_output").val('');
  };
  var resetOutputFormFields = function() {
      if(DEBUG_ENABLED) console.log("resetOutputFormFields() called......");

      $("#console_query_response_status").text('...');
      $("#input_console_output").val('');
  };
  // Refresh input fields...
  var refreshFormFields = function(queryRecordJsBean) {
	if(DEBUG_ENABLED) console.log("refreshFormFields() called......");

		if(queryRecordJsBean) {
		    var status = queryRecordJsBean.getStatus();	
		    var responseCode = queryRecordJsBean.getResponseCode();
		    var responseDisplay = '';
		    if(status) {
		    	responseDisplay += status;  
		    }
		    if(responseCode) {
		    	responseDisplay += ' (' + responseCode + ')';
		    }
		    $("#console_query_response_status").text(responseDisplay);
		    
		    var query = queryRecordJsBean.getQuery();
	    	$("#input_console_query").val(query);

		    var output = queryRecordJsBean.getOutputContent();
            if(DEBUG_ENABLED) console.log("output = " + output);
		    if(output) {
		    	if(stringEndsWith(output, "...")) {
		    		// truncated output.... (-> invalid json)
		    	    $("#input_console_output").val(output);
		    	} else {
    			    // Hack! for output formatting....
	    		    var outputJsonObj = JSON.parse(output);
		    	    var outputJsonStr = JSON.stringify(outputJsonObj, null, 4);
		        	// Hack!

		    	    $("#input_console_output").val(outputJsonStr);
		    	}
		    } else {
		    	$("#input_console_output").val('');  // ???
		    }

		} else {
			// ???
		    // this cannot happen.
			if(DEBUG_ENABLED) console.log("queryRecordJsBean is null or empty");
		}	

  };
  </script>

  <script>
function stringEndsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}
  </script>


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

  </body>
</html>