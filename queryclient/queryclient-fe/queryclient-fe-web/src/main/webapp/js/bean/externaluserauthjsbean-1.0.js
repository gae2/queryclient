//////////////////////////////////////////////////////////
// <script src="/js/bean/externaluserauthjsbean-1.0.js"></script>
// Last modified time: 1389495277768.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.ExternalUserAuthJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var managerApp;
    var appAcl;
    var gaeApp;
    var ownerUser;
    var userAcl;
    var user;
    var providerId;
    var externalUserId;
    var requestToken;
    var accessToken;
    var accessTokenSecret;
    var email;
    var firstName;
    var lastName;
    var fullName;
    var displayName;
    var description;
    var gender;
    var dateOfBirth;
    var profileImageUrl;
    var timeZone;
    var postalCode;
    var location;
    var country;
    var language;
    var status;
    var authTime;
    var expirationTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getManagerApp = function() { return managerApp; };
    this.setManagerApp = function(value) { managerApp = value; };
    this.getAppAcl = function() { return appAcl; };
    this.setAppAcl = function(value) { appAcl = value; };
    this.getGaeApp = function() { return gaeApp; };
    this.setGaeApp = function(value) { gaeApp = value; };
    this.getOwnerUser = function() { return ownerUser; };
    this.setOwnerUser = function(value) { ownerUser = value; };
    this.getUserAcl = function() { return userAcl; };
    this.setUserAcl = function(value) { userAcl = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getProviderId = function() { return providerId; };
    this.setProviderId = function(value) { providerId = value; };
    this.getExternalUserId = function() { return externalUserId; };
    this.setExternalUserId = function(value) { externalUserId = value; };
    this.getRequestToken = function() { return requestToken; };
    this.setRequestToken = function(value) { requestToken = value; };
    this.getAccessToken = function() { return accessToken; };
    this.setAccessToken = function(value) { accessToken = value; };
    this.getAccessTokenSecret = function() { return accessTokenSecret; };
    this.setAccessTokenSecret = function(value) { accessTokenSecret = value; };
    this.getEmail = function() { return email; };
    this.setEmail = function(value) { email = value; };
    this.getFirstName = function() { return firstName; };
    this.setFirstName = function(value) { firstName = value; };
    this.getLastName = function() { return lastName; };
    this.setLastName = function(value) { lastName = value; };
    this.getFullName = function() { return fullName; };
    this.setFullName = function(value) { fullName = value; };
    this.getDisplayName = function() { return displayName; };
    this.setDisplayName = function(value) { displayName = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getGender = function() { return gender; };
    this.setGender = function(value) { gender = value; };
    this.getDateOfBirth = function() { return dateOfBirth; };
    this.setDateOfBirth = function(value) { dateOfBirth = value; };
    this.getProfileImageUrl = function() { return profileImageUrl; };
    this.setProfileImageUrl = function(value) { profileImageUrl = value; };
    this.getTimeZone = function() { return timeZone; };
    this.setTimeZone = function(value) { timeZone = value; };
    this.getPostalCode = function() { return postalCode; };
    this.setPostalCode = function(value) { postalCode = value; };
    this.getLocation = function() { return location; };
    this.setLocation = function(value) { location = value; };
    this.getCountry = function() { return country; };
    this.setCountry = function(value) { country = value; };
    this.getLanguage = function() { return language; };
    this.setLanguage = function(value) { language = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getAuthTime = function() { return authTime; };
    this.setAuthTime = function(value) { authTime = value; };
    this.getExpirationTime = function() { return expirationTime; };
    this.setExpirationTime = function(value) { expirationTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.ExternalUserAuthJsBean();

      o.setGuid(generateUuid());
      if(managerApp !== undefined && managerApp != null) {
        o.setManagerApp(managerApp);
      }
      if(appAcl !== undefined && appAcl != null) {
        o.setAppAcl(appAcl);
      }
      //o.setGaeApp(gaeApp.clone());
      if(gaeApp !== undefined && gaeApp != null) {
        o.setGaeApp(gaeApp);
      }
      if(ownerUser !== undefined && ownerUser != null) {
        o.setOwnerUser(ownerUser);
      }
      if(userAcl !== undefined && userAcl != null) {
        o.setUserAcl(userAcl);
      }
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(providerId !== undefined && providerId != null) {
        o.setProviderId(providerId);
      }
      //o.setExternalUserId(externalUserId.clone());
      if(externalUserId !== undefined && externalUserId != null) {
        o.setExternalUserId(externalUserId);
      }
      if(requestToken !== undefined && requestToken != null) {
        o.setRequestToken(requestToken);
      }
      if(accessToken !== undefined && accessToken != null) {
        o.setAccessToken(accessToken);
      }
      if(accessTokenSecret !== undefined && accessTokenSecret != null) {
        o.setAccessTokenSecret(accessTokenSecret);
      }
      if(email !== undefined && email != null) {
        o.setEmail(email);
      }
      if(firstName !== undefined && firstName != null) {
        o.setFirstName(firstName);
      }
      if(lastName !== undefined && lastName != null) {
        o.setLastName(lastName);
      }
      if(fullName !== undefined && fullName != null) {
        o.setFullName(fullName);
      }
      if(displayName !== undefined && displayName != null) {
        o.setDisplayName(displayName);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(gender !== undefined && gender != null) {
        o.setGender(gender);
      }
      if(dateOfBirth !== undefined && dateOfBirth != null) {
        o.setDateOfBirth(dateOfBirth);
      }
      if(profileImageUrl !== undefined && profileImageUrl != null) {
        o.setProfileImageUrl(profileImageUrl);
      }
      if(timeZone !== undefined && timeZone != null) {
        o.setTimeZone(timeZone);
      }
      if(postalCode !== undefined && postalCode != null) {
        o.setPostalCode(postalCode);
      }
      if(location !== undefined && location != null) {
        o.setLocation(location);
      }
      if(country !== undefined && country != null) {
        o.setCountry(country);
      }
      if(language !== undefined && language != null) {
        o.setLanguage(language);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(authTime !== undefined && authTime != null) {
        o.setAuthTime(authTime);
      }
      if(expirationTime !== undefined && expirationTime != null) {
        o.setExpirationTime(expirationTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(managerApp !== undefined && managerApp != null) {
        jsonObj.managerApp = managerApp;
      } // Otherwise ignore...
      if(appAcl !== undefined && appAcl != null) {
        jsonObj.appAcl = appAcl;
      } // Otherwise ignore...
      if(gaeApp !== undefined && gaeApp != null) {
        jsonObj.gaeApp = gaeApp;
      } // Otherwise ignore...
      if(ownerUser !== undefined && ownerUser != null) {
        jsonObj.ownerUser = ownerUser;
      } // Otherwise ignore...
      if(userAcl !== undefined && userAcl != null) {
        jsonObj.userAcl = userAcl;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(providerId !== undefined && providerId != null) {
        jsonObj.providerId = providerId;
      } // Otherwise ignore...
      if(externalUserId !== undefined && externalUserId != null) {
        jsonObj.externalUserId = externalUserId;
      } // Otherwise ignore...
      if(requestToken !== undefined && requestToken != null) {
        jsonObj.requestToken = requestToken;
      } // Otherwise ignore...
      if(accessToken !== undefined && accessToken != null) {
        jsonObj.accessToken = accessToken;
      } // Otherwise ignore...
      if(accessTokenSecret !== undefined && accessTokenSecret != null) {
        jsonObj.accessTokenSecret = accessTokenSecret;
      } // Otherwise ignore...
      if(email !== undefined && email != null) {
        jsonObj.email = email;
      } // Otherwise ignore...
      if(firstName !== undefined && firstName != null) {
        jsonObj.firstName = firstName;
      } // Otherwise ignore...
      if(lastName !== undefined && lastName != null) {
        jsonObj.lastName = lastName;
      } // Otherwise ignore...
      if(fullName !== undefined && fullName != null) {
        jsonObj.fullName = fullName;
      } // Otherwise ignore...
      if(displayName !== undefined && displayName != null) {
        jsonObj.displayName = displayName;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(gender !== undefined && gender != null) {
        jsonObj.gender = gender;
      } // Otherwise ignore...
      if(dateOfBirth !== undefined && dateOfBirth != null) {
        jsonObj.dateOfBirth = dateOfBirth;
      } // Otherwise ignore...
      if(profileImageUrl !== undefined && profileImageUrl != null) {
        jsonObj.profileImageUrl = profileImageUrl;
      } // Otherwise ignore...
      if(timeZone !== undefined && timeZone != null) {
        jsonObj.timeZone = timeZone;
      } // Otherwise ignore...
      if(postalCode !== undefined && postalCode != null) {
        jsonObj.postalCode = postalCode;
      } // Otherwise ignore...
      if(location !== undefined && location != null) {
        jsonObj.location = location;
      } // Otherwise ignore...
      if(country !== undefined && country != null) {
        jsonObj.country = country;
      } // Otherwise ignore...
      if(language !== undefined && language != null) {
        jsonObj.language = language;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(authTime !== undefined && authTime != null) {
        jsonObj.authTime = authTime;
      } // Otherwise ignore...
      if(expirationTime !== undefined && expirationTime != null) {
        jsonObj.expirationTime = expirationTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(managerApp) {
        str += "\"managerApp\":\"" + managerApp + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"managerApp\":null, ";
      }
      if(appAcl) {
        str += "\"appAcl\":" + appAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appAcl\":null, ";
      }
      str += "\"gaeApp\":" + gaeApp.toJsonString() + ", ";
      if(ownerUser) {
        str += "\"ownerUser\":\"" + ownerUser + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ownerUser\":null, ";
      }
      if(userAcl) {
        str += "\"userAcl\":" + userAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userAcl\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(providerId) {
        str += "\"providerId\":\"" + providerId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"providerId\":null, ";
      }
      str += "\"externalUserId\":" + externalUserId.toJsonString() + ", ";
      if(requestToken) {
        str += "\"requestToken\":\"" + requestToken + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"requestToken\":null, ";
      }
      if(accessToken) {
        str += "\"accessToken\":\"" + accessToken + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"accessToken\":null, ";
      }
      if(accessTokenSecret) {
        str += "\"accessTokenSecret\":\"" + accessTokenSecret + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"accessTokenSecret\":null, ";
      }
      if(email) {
        str += "\"email\":\"" + email + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"email\":null, ";
      }
      if(firstName) {
        str += "\"firstName\":\"" + firstName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"firstName\":null, ";
      }
      if(lastName) {
        str += "\"lastName\":\"" + lastName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastName\":null, ";
      }
      if(fullName) {
        str += "\"fullName\":\"" + fullName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fullName\":null, ";
      }
      if(displayName) {
        str += "\"displayName\":\"" + displayName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"displayName\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(gender) {
        str += "\"gender\":\"" + gender + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"gender\":null, ";
      }
      if(dateOfBirth) {
        str += "\"dateOfBirth\":\"" + dateOfBirth + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"dateOfBirth\":null, ";
      }
      if(profileImageUrl) {
        str += "\"profileImageUrl\":\"" + profileImageUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"profileImageUrl\":null, ";
      }
      if(timeZone) {
        str += "\"timeZone\":\"" + timeZone + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"timeZone\":null, ";
      }
      if(postalCode) {
        str += "\"postalCode\":\"" + postalCode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"postalCode\":null, ";
      }
      if(location) {
        str += "\"location\":\"" + location + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"location\":null, ";
      }
      if(country) {
        str += "\"country\":\"" + country + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"country\":null, ";
      }
      if(language) {
        str += "\"language\":\"" + language + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"language\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(authTime) {
        str += "\"authTime\":" + authTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authTime\":null, ";
      }
      if(expirationTime) {
        str += "\"expirationTime\":" + expirationTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"expirationTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "managerApp:" + managerApp + ", ";
      str += "appAcl:" + appAcl + ", ";
      str += "gaeApp:" + gaeApp + ", ";
      str += "ownerUser:" + ownerUser + ", ";
      str += "userAcl:" + userAcl + ", ";
      str += "user:" + user + ", ";
      str += "providerId:" + providerId + ", ";
      str += "externalUserId:" + externalUserId + ", ";
      str += "requestToken:" + requestToken + ", ";
      str += "accessToken:" + accessToken + ", ";
      str += "accessTokenSecret:" + accessTokenSecret + ", ";
      str += "email:" + email + ", ";
      str += "firstName:" + firstName + ", ";
      str += "lastName:" + lastName + ", ";
      str += "fullName:" + fullName + ", ";
      str += "displayName:" + displayName + ", ";
      str += "description:" + description + ", ";
      str += "gender:" + gender + ", ";
      str += "dateOfBirth:" + dateOfBirth + ", ";
      str += "profileImageUrl:" + profileImageUrl + ", ";
      str += "timeZone:" + timeZone + ", ";
      str += "postalCode:" + postalCode + ", ";
      str += "location:" + location + ", ";
      str += "country:" + country + ", ";
      str += "language:" + language + ", ";
      str += "status:" + status + ", ";
      str += "authTime:" + authTime + ", ";
      str += "expirationTime:" + expirationTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.ExternalUserAuthJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.ExternalUserAuthJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.managerApp !== undefined && obj.managerApp != null) {
    o.setManagerApp(obj.managerApp);
  }
  if(obj.appAcl !== undefined && obj.appAcl != null) {
    o.setAppAcl(obj.appAcl);
  }
  if(obj.gaeApp !== undefined && obj.gaeApp != null) {
    o.setGaeApp(obj.gaeApp);
  }
  if(obj.ownerUser !== undefined && obj.ownerUser != null) {
    o.setOwnerUser(obj.ownerUser);
  }
  if(obj.userAcl !== undefined && obj.userAcl != null) {
    o.setUserAcl(obj.userAcl);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.providerId !== undefined && obj.providerId != null) {
    o.setProviderId(obj.providerId);
  }
  if(obj.externalUserId !== undefined && obj.externalUserId != null) {
    o.setExternalUserId(obj.externalUserId);
  }
  if(obj.requestToken !== undefined && obj.requestToken != null) {
    o.setRequestToken(obj.requestToken);
  }
  if(obj.accessToken !== undefined && obj.accessToken != null) {
    o.setAccessToken(obj.accessToken);
  }
  if(obj.accessTokenSecret !== undefined && obj.accessTokenSecret != null) {
    o.setAccessTokenSecret(obj.accessTokenSecret);
  }
  if(obj.email !== undefined && obj.email != null) {
    o.setEmail(obj.email);
  }
  if(obj.firstName !== undefined && obj.firstName != null) {
    o.setFirstName(obj.firstName);
  }
  if(obj.lastName !== undefined && obj.lastName != null) {
    o.setLastName(obj.lastName);
  }
  if(obj.fullName !== undefined && obj.fullName != null) {
    o.setFullName(obj.fullName);
  }
  if(obj.displayName !== undefined && obj.displayName != null) {
    o.setDisplayName(obj.displayName);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.gender !== undefined && obj.gender != null) {
    o.setGender(obj.gender);
  }
  if(obj.dateOfBirth !== undefined && obj.dateOfBirth != null) {
    o.setDateOfBirth(obj.dateOfBirth);
  }
  if(obj.profileImageUrl !== undefined && obj.profileImageUrl != null) {
    o.setProfileImageUrl(obj.profileImageUrl);
  }
  if(obj.timeZone !== undefined && obj.timeZone != null) {
    o.setTimeZone(obj.timeZone);
  }
  if(obj.postalCode !== undefined && obj.postalCode != null) {
    o.setPostalCode(obj.postalCode);
  }
  if(obj.location !== undefined && obj.location != null) {
    o.setLocation(obj.location);
  }
  if(obj.country !== undefined && obj.country != null) {
    o.setCountry(obj.country);
  }
  if(obj.language !== undefined && obj.language != null) {
    o.setLanguage(obj.language);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.authTime !== undefined && obj.authTime != null) {
    o.setAuthTime(obj.authTime);
  }
  if(obj.expirationTime !== undefined && obj.expirationTime != null) {
    o.setExpirationTime(obj.expirationTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

queryclient.wa.bean.ExternalUserAuthJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.ExternalUserAuthJsBean.create(jsonObj);
  return obj;
};
