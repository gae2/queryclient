//////////////////////////////////////////////////////////
// <script src="/js/bean/keyvaluerelationstructjsbean-1.0.js"></script>
// Last modified time: 1389495277498.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.KeyValueRelationStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var key;
    var value;
    var note;
    var relation;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getKey = function() { return key; };
    this.setKey = function(value) { key = value; };
    this.getValue = function() { return value; };
    this.setValue = function(value) { value = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getRelation = function() { return relation; };
    this.setRelation = function(value) { relation = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.KeyValueRelationStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(key !== undefined && key != null) {
        o.setKey(key);
      }
      if(value !== undefined && value != null) {
        o.setValue(value);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(relation !== undefined && relation != null) {
        o.setRelation(relation);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(key !== undefined && key != null) {
        jsonObj.key = key;
      } // Otherwise ignore...
      if(value !== undefined && value != null) {
        jsonObj.value = value;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(relation !== undefined && relation != null) {
        jsonObj.relation = relation;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(key) {
        str += "\"key\":\"" + key + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"key\":null, ";
      }
      if(value) {
        str += "\"value\":\"" + value + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"value\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(relation) {
        str += "\"relation\":\"" + relation + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"relation\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "key:" + key + ", ";
      str += "value:" + value + ", ";
      str += "note:" + note + ", ";
      str += "relation:" + relation + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.KeyValueRelationStructJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.KeyValueRelationStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.key !== undefined && obj.key != null) {
    o.setKey(obj.key);
  }
  if(obj.value !== undefined && obj.value != null) {
    o.setValue(obj.value);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.relation !== undefined && obj.relation != null) {
    o.setRelation(obj.relation);
  }
    
  return o;
};

queryclient.wa.bean.KeyValueRelationStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.KeyValueRelationStructJsBean.create(jsonObj);
  return obj;
};
