//////////////////////////////////////////////////////////
// <script src="/js/bean/queryrecordjsbean-1.0.js"></script>
// Last modified time: 1389495277833.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.QueryRecordJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var querySession;
    var queryId;
    var dataService;
    var serviceUrl;
    var delayed;
    var query;
    var inputFormat;
    var inputFile;
    var inputContent;
    var targetOutputFormat;
    var outputFormat;
    var outputFile;
    var outputContent;
    var responseCode;
    var result;
    var referrerInfo;
    var status;
    var extra;
    var note;
    var scheduledTime;
    var processedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getQuerySession = function() { return querySession; };
    this.setQuerySession = function(value) { querySession = value; };
    this.getQueryId = function() { return queryId; };
    this.setQueryId = function(value) { queryId = value; };
    this.getDataService = function() { return dataService; };
    this.setDataService = function(value) { dataService = value; };
    this.getServiceUrl = function() { return serviceUrl; };
    this.setServiceUrl = function(value) { serviceUrl = value; };
    this.getDelayed = function() { return delayed; };
    this.setDelayed = function(value) { delayed = value; };
    this.getQuery = function() { return query; };
    this.setQuery = function(value) { query = value; };
    this.getInputFormat = function() { return inputFormat; };
    this.setInputFormat = function(value) { inputFormat = value; };
    this.getInputFile = function() { return inputFile; };
    this.setInputFile = function(value) { inputFile = value; };
    this.getInputContent = function() { return inputContent; };
    this.setInputContent = function(value) { inputContent = value; };
    this.getTargetOutputFormat = function() { return targetOutputFormat; };
    this.setTargetOutputFormat = function(value) { targetOutputFormat = value; };
    this.getOutputFormat = function() { return outputFormat; };
    this.setOutputFormat = function(value) { outputFormat = value; };
    this.getOutputFile = function() { return outputFile; };
    this.setOutputFile = function(value) { outputFile = value; };
    this.getOutputContent = function() { return outputContent; };
    this.setOutputContent = function(value) { outputContent = value; };
    this.getResponseCode = function() { return responseCode; };
    this.setResponseCode = function(value) { responseCode = value; };
    this.getResult = function() { return result; };
    this.setResult = function(value) { result = value; };
    this.getReferrerInfo = function() { return referrerInfo; };
    this.setReferrerInfo = function(value) { referrerInfo = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getExtra = function() { return extra; };
    this.setExtra = function(value) { extra = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getScheduledTime = function() { return scheduledTime; };
    this.setScheduledTime = function(value) { scheduledTime = value; };
    this.getProcessedTime = function() { return processedTime; };
    this.setProcessedTime = function(value) { processedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.QueryRecordJsBean();

      o.setGuid(generateUuid());
      if(querySession !== undefined && querySession != null) {
        o.setQuerySession(querySession);
      }
      if(queryId !== undefined && queryId != null) {
        o.setQueryId(queryId);
      }
      if(dataService !== undefined && dataService != null) {
        o.setDataService(dataService);
      }
      if(serviceUrl !== undefined && serviceUrl != null) {
        o.setServiceUrl(serviceUrl);
      }
      if(delayed !== undefined && delayed != null) {
        o.setDelayed(delayed);
      }
      if(query !== undefined && query != null) {
        o.setQuery(query);
      }
      if(inputFormat !== undefined && inputFormat != null) {
        o.setInputFormat(inputFormat);
      }
      if(inputFile !== undefined && inputFile != null) {
        o.setInputFile(inputFile);
      }
      if(inputContent !== undefined && inputContent != null) {
        o.setInputContent(inputContent);
      }
      if(targetOutputFormat !== undefined && targetOutputFormat != null) {
        o.setTargetOutputFormat(targetOutputFormat);
      }
      if(outputFormat !== undefined && outputFormat != null) {
        o.setOutputFormat(outputFormat);
      }
      if(outputFile !== undefined && outputFile != null) {
        o.setOutputFile(outputFile);
      }
      if(outputContent !== undefined && outputContent != null) {
        o.setOutputContent(outputContent);
      }
      if(responseCode !== undefined && responseCode != null) {
        o.setResponseCode(responseCode);
      }
      if(result !== undefined && result != null) {
        o.setResult(result);
      }
      //o.setReferrerInfo(referrerInfo.clone());
      if(referrerInfo !== undefined && referrerInfo != null) {
        o.setReferrerInfo(referrerInfo);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(extra !== undefined && extra != null) {
        o.setExtra(extra);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(scheduledTime !== undefined && scheduledTime != null) {
        o.setScheduledTime(scheduledTime);
      }
      if(processedTime !== undefined && processedTime != null) {
        o.setProcessedTime(processedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(querySession !== undefined && querySession != null) {
        jsonObj.querySession = querySession;
      } // Otherwise ignore...
      if(queryId !== undefined && queryId != null) {
        jsonObj.queryId = queryId;
      } // Otherwise ignore...
      if(dataService !== undefined && dataService != null) {
        jsonObj.dataService = dataService;
      } // Otherwise ignore...
      if(serviceUrl !== undefined && serviceUrl != null) {
        jsonObj.serviceUrl = serviceUrl;
      } // Otherwise ignore...
      if(delayed !== undefined && delayed != null) {
        jsonObj.delayed = delayed;
      } // Otherwise ignore...
      if(query !== undefined && query != null) {
        jsonObj.query = query;
      } // Otherwise ignore...
      if(inputFormat !== undefined && inputFormat != null) {
        jsonObj.inputFormat = inputFormat;
      } // Otherwise ignore...
      if(inputFile !== undefined && inputFile != null) {
        jsonObj.inputFile = inputFile;
      } // Otherwise ignore...
      if(inputContent !== undefined && inputContent != null) {
        jsonObj.inputContent = inputContent;
      } // Otherwise ignore...
      if(targetOutputFormat !== undefined && targetOutputFormat != null) {
        jsonObj.targetOutputFormat = targetOutputFormat;
      } // Otherwise ignore...
      if(outputFormat !== undefined && outputFormat != null) {
        jsonObj.outputFormat = outputFormat;
      } // Otherwise ignore...
      if(outputFile !== undefined && outputFile != null) {
        jsonObj.outputFile = outputFile;
      } // Otherwise ignore...
      if(outputContent !== undefined && outputContent != null) {
        jsonObj.outputContent = outputContent;
      } // Otherwise ignore...
      if(responseCode !== undefined && responseCode != null) {
        jsonObj.responseCode = responseCode;
      } // Otherwise ignore...
      if(result !== undefined && result != null) {
        jsonObj.result = result;
      } // Otherwise ignore...
      if(referrerInfo !== undefined && referrerInfo != null) {
        jsonObj.referrerInfo = referrerInfo;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(extra !== undefined && extra != null) {
        jsonObj.extra = extra;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(scheduledTime !== undefined && scheduledTime != null) {
        jsonObj.scheduledTime = scheduledTime;
      } // Otherwise ignore...
      if(processedTime !== undefined && processedTime != null) {
        jsonObj.processedTime = processedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(querySession) {
        str += "\"querySession\":\"" + querySession + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"querySession\":null, ";
      }
      if(queryId) {
        str += "\"queryId\":" + queryId + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"queryId\":null, ";
      }
      if(dataService) {
        str += "\"dataService\":\"" + dataService + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"dataService\":null, ";
      }
      if(serviceUrl) {
        str += "\"serviceUrl\":\"" + serviceUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"serviceUrl\":null, ";
      }
      if(delayed) {
        str += "\"delayed\":" + delayed + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"delayed\":null, ";
      }
      if(query) {
        str += "\"query\":\"" + query + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"query\":null, ";
      }
      if(inputFormat) {
        str += "\"inputFormat\":\"" + inputFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"inputFormat\":null, ";
      }
      if(inputFile) {
        str += "\"inputFile\":\"" + inputFile + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"inputFile\":null, ";
      }
      if(inputContent) {
        str += "\"inputContent\":\"" + inputContent + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"inputContent\":null, ";
      }
      if(targetOutputFormat) {
        str += "\"targetOutputFormat\":\"" + targetOutputFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"targetOutputFormat\":null, ";
      }
      if(outputFormat) {
        str += "\"outputFormat\":\"" + outputFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputFormat\":null, ";
      }
      if(outputFile) {
        str += "\"outputFile\":\"" + outputFile + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputFile\":null, ";
      }
      if(outputContent) {
        str += "\"outputContent\":\"" + outputContent + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputContent\":null, ";
      }
      if(responseCode) {
        str += "\"responseCode\":" + responseCode + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"responseCode\":null, ";
      }
      if(result) {
        str += "\"result\":\"" + result + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"result\":null, ";
      }
      str += "\"referrerInfo\":" + referrerInfo.toJsonString() + ", ";
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(extra) {
        str += "\"extra\":\"" + extra + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"extra\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(scheduledTime) {
        str += "\"scheduledTime\":" + scheduledTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"scheduledTime\":null, ";
      }
      if(processedTime) {
        str += "\"processedTime\":" + processedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"processedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "querySession:" + querySession + ", ";
      str += "queryId:" + queryId + ", ";
      str += "dataService:" + dataService + ", ";
      str += "serviceUrl:" + serviceUrl + ", ";
      str += "delayed:" + delayed + ", ";
      str += "query:" + query + ", ";
      str += "inputFormat:" + inputFormat + ", ";
      str += "inputFile:" + inputFile + ", ";
      str += "inputContent:" + inputContent + ", ";
      str += "targetOutputFormat:" + targetOutputFormat + ", ";
      str += "outputFormat:" + outputFormat + ", ";
      str += "outputFile:" + outputFile + ", ";
      str += "outputContent:" + outputContent + ", ";
      str += "responseCode:" + responseCode + ", ";
      str += "result:" + result + ", ";
      str += "referrerInfo:" + referrerInfo + ", ";
      str += "status:" + status + ", ";
      str += "extra:" + extra + ", ";
      str += "note:" + note + ", ";
      str += "scheduledTime:" + scheduledTime + ", ";
      str += "processedTime:" + processedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.QueryRecordJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.QueryRecordJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.querySession !== undefined && obj.querySession != null) {
    o.setQuerySession(obj.querySession);
  }
  if(obj.queryId !== undefined && obj.queryId != null) {
    o.setQueryId(obj.queryId);
  }
  if(obj.dataService !== undefined && obj.dataService != null) {
    o.setDataService(obj.dataService);
  }
  if(obj.serviceUrl !== undefined && obj.serviceUrl != null) {
    o.setServiceUrl(obj.serviceUrl);
  }
  if(obj.delayed !== undefined && obj.delayed != null) {
    o.setDelayed(obj.delayed);
  }
  if(obj.query !== undefined && obj.query != null) {
    o.setQuery(obj.query);
  }
  if(obj.inputFormat !== undefined && obj.inputFormat != null) {
    o.setInputFormat(obj.inputFormat);
  }
  if(obj.inputFile !== undefined && obj.inputFile != null) {
    o.setInputFile(obj.inputFile);
  }
  if(obj.inputContent !== undefined && obj.inputContent != null) {
    o.setInputContent(obj.inputContent);
  }
  if(obj.targetOutputFormat !== undefined && obj.targetOutputFormat != null) {
    o.setTargetOutputFormat(obj.targetOutputFormat);
  }
  if(obj.outputFormat !== undefined && obj.outputFormat != null) {
    o.setOutputFormat(obj.outputFormat);
  }
  if(obj.outputFile !== undefined && obj.outputFile != null) {
    o.setOutputFile(obj.outputFile);
  }
  if(obj.outputContent !== undefined && obj.outputContent != null) {
    o.setOutputContent(obj.outputContent);
  }
  if(obj.responseCode !== undefined && obj.responseCode != null) {
    o.setResponseCode(obj.responseCode);
  }
  if(obj.result !== undefined && obj.result != null) {
    o.setResult(obj.result);
  }
  if(obj.referrerInfo !== undefined && obj.referrerInfo != null) {
    o.setReferrerInfo(obj.referrerInfo);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.extra !== undefined && obj.extra != null) {
    o.setExtra(obj.extra);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.scheduledTime !== undefined && obj.scheduledTime != null) {
    o.setScheduledTime(obj.scheduledTime);
  }
  if(obj.processedTime !== undefined && obj.processedTime != null) {
    o.setProcessedTime(obj.processedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

queryclient.wa.bean.QueryRecordJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.QueryRecordJsBean.create(jsonObj);
  return obj;
};
