//////////////////////////////////////////////////////////
// <script src="/js/bean/querysessionjsbean-1.0.js"></script>
// Last modified time: 1389495277822.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.QuerySessionJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var dataService;
    var serviceUrl;
    var inputFormat;
    var outputFormat;
    var referrerInfo;
    var status;
    var note;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getDataService = function() { return dataService; };
    this.setDataService = function(value) { dataService = value; };
    this.getServiceUrl = function() { return serviceUrl; };
    this.setServiceUrl = function(value) { serviceUrl = value; };
    this.getInputFormat = function() { return inputFormat; };
    this.setInputFormat = function(value) { inputFormat = value; };
    this.getOutputFormat = function() { return outputFormat; };
    this.setOutputFormat = function(value) { outputFormat = value; };
    this.getReferrerInfo = function() { return referrerInfo; };
    this.setReferrerInfo = function(value) { referrerInfo = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.QuerySessionJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(dataService !== undefined && dataService != null) {
        o.setDataService(dataService);
      }
      if(serviceUrl !== undefined && serviceUrl != null) {
        o.setServiceUrl(serviceUrl);
      }
      if(inputFormat !== undefined && inputFormat != null) {
        o.setInputFormat(inputFormat);
      }
      if(outputFormat !== undefined && outputFormat != null) {
        o.setOutputFormat(outputFormat);
      }
      //o.setReferrerInfo(referrerInfo.clone());
      if(referrerInfo !== undefined && referrerInfo != null) {
        o.setReferrerInfo(referrerInfo);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(dataService !== undefined && dataService != null) {
        jsonObj.dataService = dataService;
      } // Otherwise ignore...
      if(serviceUrl !== undefined && serviceUrl != null) {
        jsonObj.serviceUrl = serviceUrl;
      } // Otherwise ignore...
      if(inputFormat !== undefined && inputFormat != null) {
        jsonObj.inputFormat = inputFormat;
      } // Otherwise ignore...
      if(outputFormat !== undefined && outputFormat != null) {
        jsonObj.outputFormat = outputFormat;
      } // Otherwise ignore...
      if(referrerInfo !== undefined && referrerInfo != null) {
        jsonObj.referrerInfo = referrerInfo;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(dataService) {
        str += "\"dataService\":\"" + dataService + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"dataService\":null, ";
      }
      if(serviceUrl) {
        str += "\"serviceUrl\":\"" + serviceUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"serviceUrl\":null, ";
      }
      if(inputFormat) {
        str += "\"inputFormat\":\"" + inputFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"inputFormat\":null, ";
      }
      if(outputFormat) {
        str += "\"outputFormat\":\"" + outputFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputFormat\":null, ";
      }
      str += "\"referrerInfo\":" + referrerInfo.toJsonString() + ", ";
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "dataService:" + dataService + ", ";
      str += "serviceUrl:" + serviceUrl + ", ";
      str += "inputFormat:" + inputFormat + ", ";
      str += "outputFormat:" + outputFormat + ", ";
      str += "referrerInfo:" + referrerInfo + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.QuerySessionJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.QuerySessionJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.dataService !== undefined && obj.dataService != null) {
    o.setDataService(obj.dataService);
  }
  if(obj.serviceUrl !== undefined && obj.serviceUrl != null) {
    o.setServiceUrl(obj.serviceUrl);
  }
  if(obj.inputFormat !== undefined && obj.inputFormat != null) {
    o.setInputFormat(obj.inputFormat);
  }
  if(obj.outputFormat !== undefined && obj.outputFormat != null) {
    o.setOutputFormat(obj.outputFormat);
  }
  if(obj.referrerInfo !== undefined && obj.referrerInfo != null) {
    o.setReferrerInfo(obj.referrerInfo);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

queryclient.wa.bean.QuerySessionJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.QuerySessionJsBean.create(jsonObj);
  return obj;
};
