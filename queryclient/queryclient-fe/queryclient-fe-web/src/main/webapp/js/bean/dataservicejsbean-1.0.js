//////////////////////////////////////////////////////////
// <script src="/js/bean/dataservicejsbean-1.0.js"></script>
// Last modified time: 1389495277804.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.DataServiceJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var name;
    var description;
    var type;
    var mode;
    var serviceUrl;
    var authRequired;
    var authCredential;
    var referrerInfo;
    var status;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getName = function() { return name; };
    this.setName = function(value) { name = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getType = function() { return type; };
    this.setType = function(value) { type = value; };
    this.getMode = function() { return mode; };
    this.setMode = function(value) { mode = value; };
    this.getServiceUrl = function() { return serviceUrl; };
    this.setServiceUrl = function(value) { serviceUrl = value; };
    this.getAuthRequired = function() { return authRequired; };
    this.setAuthRequired = function(value) { authRequired = value; };
    this.getAuthCredential = function() { return authCredential; };
    this.setAuthCredential = function(value) { authCredential = value; };
    this.getReferrerInfo = function() { return referrerInfo; };
    this.setReferrerInfo = function(value) { referrerInfo = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.DataServiceJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(name !== undefined && name != null) {
        o.setName(name);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(type !== undefined && type != null) {
        o.setType(type);
      }
      if(mode !== undefined && mode != null) {
        o.setMode(mode);
      }
      if(serviceUrl !== undefined && serviceUrl != null) {
        o.setServiceUrl(serviceUrl);
      }
      if(authRequired !== undefined && authRequired != null) {
        o.setAuthRequired(authRequired);
      }
      //o.setAuthCredential(authCredential.clone());
      if(authCredential !== undefined && authCredential != null) {
        o.setAuthCredential(authCredential);
      }
      //o.setReferrerInfo(referrerInfo.clone());
      if(referrerInfo !== undefined && referrerInfo != null) {
        o.setReferrerInfo(referrerInfo);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(name !== undefined && name != null) {
        jsonObj.name = name;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(type !== undefined && type != null) {
        jsonObj.type = type;
      } // Otherwise ignore...
      if(mode !== undefined && mode != null) {
        jsonObj.mode = mode;
      } // Otherwise ignore...
      if(serviceUrl !== undefined && serviceUrl != null) {
        jsonObj.serviceUrl = serviceUrl;
      } // Otherwise ignore...
      if(authRequired !== undefined && authRequired != null) {
        jsonObj.authRequired = authRequired;
      } // Otherwise ignore...
      if(authCredential !== undefined && authCredential != null) {
        jsonObj.authCredential = authCredential;
      } // Otherwise ignore...
      if(referrerInfo !== undefined && referrerInfo != null) {
        jsonObj.referrerInfo = referrerInfo;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(name) {
        str += "\"name\":\"" + name + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"name\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(type) {
        str += "\"type\":\"" + type + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"type\":null, ";
      }
      if(mode) {
        str += "\"mode\":\"" + mode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"mode\":null, ";
      }
      if(serviceUrl) {
        str += "\"serviceUrl\":\"" + serviceUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"serviceUrl\":null, ";
      }
      if(authRequired) {
        str += "\"authRequired\":" + authRequired + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authRequired\":null, ";
      }
      str += "\"authCredential\":" + authCredential.toJsonString() + ", ";
      str += "\"referrerInfo\":" + referrerInfo.toJsonString() + ", ";
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "name:" + name + ", ";
      str += "description:" + description + ", ";
      str += "type:" + type + ", ";
      str += "mode:" + mode + ", ";
      str += "serviceUrl:" + serviceUrl + ", ";
      str += "authRequired:" + authRequired + ", ";
      str += "authCredential:" + authCredential + ", ";
      str += "referrerInfo:" + referrerInfo + ", ";
      str += "status:" + status + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.DataServiceJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.DataServiceJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.name !== undefined && obj.name != null) {
    o.setName(obj.name);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.type !== undefined && obj.type != null) {
    o.setType(obj.type);
  }
  if(obj.mode !== undefined && obj.mode != null) {
    o.setMode(obj.mode);
  }
  if(obj.serviceUrl !== undefined && obj.serviceUrl != null) {
    o.setServiceUrl(obj.serviceUrl);
  }
  if(obj.authRequired !== undefined && obj.authRequired != null) {
    o.setAuthRequired(obj.authRequired);
  }
  if(obj.authCredential !== undefined && obj.authCredential != null) {
    o.setAuthCredential(obj.authCredential);
  }
  if(obj.referrerInfo !== undefined && obj.referrerInfo != null) {
    o.setReferrerInfo(obj.referrerInfo);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

queryclient.wa.bean.DataServiceJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.DataServiceJsBean.create(jsonObj);
  return obj;
};
