//////////////////////////////////////////////////////////
// <script src="/js/bean/pagerstatestructjsbean-1.0.js"></script>
// Last modified time: 1389495277598.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.PagerStateStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var pagerMode;
    var primaryOrdering;
    var secondaryOrdering;
    var currentOffset;
    var currentPage;
    var pageSize;
    var totalCount;
    var lowerBoundTotalCount;
    var previousPageOffset;
    var nextPageOffset;
    var lastPageOffset;
    var lastPageIndex;
    var firstActionEnabled;
    var previousActionEnabled;
    var nextActionEnabled;
    var lastActionEnabled;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getPagerMode = function() { return pagerMode; };
    this.setPagerMode = function(value) { pagerMode = value; };
    this.getPrimaryOrdering = function() { return primaryOrdering; };
    this.setPrimaryOrdering = function(value) { primaryOrdering = value; };
    this.getSecondaryOrdering = function() { return secondaryOrdering; };
    this.setSecondaryOrdering = function(value) { secondaryOrdering = value; };
    this.getCurrentOffset = function() { return currentOffset; };
    this.setCurrentOffset = function(value) { currentOffset = value; };
    this.getCurrentPage = function() { return currentPage; };
    this.setCurrentPage = function(value) { currentPage = value; };
    this.getPageSize = function() { return pageSize; };
    this.setPageSize = function(value) { pageSize = value; };
    this.getTotalCount = function() { return totalCount; };
    this.setTotalCount = function(value) { totalCount = value; };
    this.getLowerBoundTotalCount = function() { return lowerBoundTotalCount; };
    this.setLowerBoundTotalCount = function(value) { lowerBoundTotalCount = value; };
    this.getPreviousPageOffset = function() { return previousPageOffset; };
    this.setPreviousPageOffset = function(value) { previousPageOffset = value; };
    this.getNextPageOffset = function() { return nextPageOffset; };
    this.setNextPageOffset = function(value) { nextPageOffset = value; };
    this.getLastPageOffset = function() { return lastPageOffset; };
    this.setLastPageOffset = function(value) { lastPageOffset = value; };
    this.getLastPageIndex = function() { return lastPageIndex; };
    this.setLastPageIndex = function(value) { lastPageIndex = value; };
    this.getFirstActionEnabled = function() { return firstActionEnabled; };
    this.setFirstActionEnabled = function(value) { firstActionEnabled = value; };
    this.getPreviousActionEnabled = function() { return previousActionEnabled; };
    this.setPreviousActionEnabled = function(value) { previousActionEnabled = value; };
    this.getNextActionEnabled = function() { return nextActionEnabled; };
    this.setNextActionEnabled = function(value) { nextActionEnabled = value; };
    this.getLastActionEnabled = function() { return lastActionEnabled; };
    this.setLastActionEnabled = function(value) { lastActionEnabled = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.PagerStateStructJsBean();

      if(pagerMode !== undefined && pagerMode != null) {
        o.setPagerMode(pagerMode);
      }
      if(primaryOrdering !== undefined && primaryOrdering != null) {
        o.setPrimaryOrdering(primaryOrdering);
      }
      if(secondaryOrdering !== undefined && secondaryOrdering != null) {
        o.setSecondaryOrdering(secondaryOrdering);
      }
      if(currentOffset !== undefined && currentOffset != null) {
        o.setCurrentOffset(currentOffset);
      }
      if(currentPage !== undefined && currentPage != null) {
        o.setCurrentPage(currentPage);
      }
      if(pageSize !== undefined && pageSize != null) {
        o.setPageSize(pageSize);
      }
      if(totalCount !== undefined && totalCount != null) {
        o.setTotalCount(totalCount);
      }
      if(lowerBoundTotalCount !== undefined && lowerBoundTotalCount != null) {
        o.setLowerBoundTotalCount(lowerBoundTotalCount);
      }
      if(previousPageOffset !== undefined && previousPageOffset != null) {
        o.setPreviousPageOffset(previousPageOffset);
      }
      if(nextPageOffset !== undefined && nextPageOffset != null) {
        o.setNextPageOffset(nextPageOffset);
      }
      if(lastPageOffset !== undefined && lastPageOffset != null) {
        o.setLastPageOffset(lastPageOffset);
      }
      if(lastPageIndex !== undefined && lastPageIndex != null) {
        o.setLastPageIndex(lastPageIndex);
      }
      if(firstActionEnabled !== undefined && firstActionEnabled != null) {
        o.setFirstActionEnabled(firstActionEnabled);
      }
      if(previousActionEnabled !== undefined && previousActionEnabled != null) {
        o.setPreviousActionEnabled(previousActionEnabled);
      }
      if(nextActionEnabled !== undefined && nextActionEnabled != null) {
        o.setNextActionEnabled(nextActionEnabled);
      }
      if(lastActionEnabled !== undefined && lastActionEnabled != null) {
        o.setLastActionEnabled(lastActionEnabled);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(pagerMode !== undefined && pagerMode != null) {
        jsonObj.pagerMode = pagerMode;
      } // Otherwise ignore...
      if(primaryOrdering !== undefined && primaryOrdering != null) {
        jsonObj.primaryOrdering = primaryOrdering;
      } // Otherwise ignore...
      if(secondaryOrdering !== undefined && secondaryOrdering != null) {
        jsonObj.secondaryOrdering = secondaryOrdering;
      } // Otherwise ignore...
      if(currentOffset !== undefined && currentOffset != null) {
        jsonObj.currentOffset = currentOffset;
      } // Otherwise ignore...
      if(currentPage !== undefined && currentPage != null) {
        jsonObj.currentPage = currentPage;
      } // Otherwise ignore...
      if(pageSize !== undefined && pageSize != null) {
        jsonObj.pageSize = pageSize;
      } // Otherwise ignore...
      if(totalCount !== undefined && totalCount != null) {
        jsonObj.totalCount = totalCount;
      } // Otherwise ignore...
      if(lowerBoundTotalCount !== undefined && lowerBoundTotalCount != null) {
        jsonObj.lowerBoundTotalCount = lowerBoundTotalCount;
      } // Otherwise ignore...
      if(previousPageOffset !== undefined && previousPageOffset != null) {
        jsonObj.previousPageOffset = previousPageOffset;
      } // Otherwise ignore...
      if(nextPageOffset !== undefined && nextPageOffset != null) {
        jsonObj.nextPageOffset = nextPageOffset;
      } // Otherwise ignore...
      if(lastPageOffset !== undefined && lastPageOffset != null) {
        jsonObj.lastPageOffset = lastPageOffset;
      } // Otherwise ignore...
      if(lastPageIndex !== undefined && lastPageIndex != null) {
        jsonObj.lastPageIndex = lastPageIndex;
      } // Otherwise ignore...
      if(firstActionEnabled !== undefined && firstActionEnabled != null) {
        jsonObj.firstActionEnabled = firstActionEnabled;
      } // Otherwise ignore...
      if(previousActionEnabled !== undefined && previousActionEnabled != null) {
        jsonObj.previousActionEnabled = previousActionEnabled;
      } // Otherwise ignore...
      if(nextActionEnabled !== undefined && nextActionEnabled != null) {
        jsonObj.nextActionEnabled = nextActionEnabled;
      } // Otherwise ignore...
      if(lastActionEnabled !== undefined && lastActionEnabled != null) {
        jsonObj.lastActionEnabled = lastActionEnabled;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(pagerMode) {
        str += "\"pagerMode\":\"" + pagerMode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pagerMode\":null, ";
      }
      if(primaryOrdering) {
        str += "\"primaryOrdering\":\"" + primaryOrdering + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"primaryOrdering\":null, ";
      }
      if(secondaryOrdering) {
        str += "\"secondaryOrdering\":\"" + secondaryOrdering + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"secondaryOrdering\":null, ";
      }
      if(currentOffset) {
        str += "\"currentOffset\":" + currentOffset + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"currentOffset\":null, ";
      }
      if(currentPage) {
        str += "\"currentPage\":" + currentPage + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"currentPage\":null, ";
      }
      if(pageSize) {
        str += "\"pageSize\":" + pageSize + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pageSize\":null, ";
      }
      if(totalCount) {
        str += "\"totalCount\":" + totalCount + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"totalCount\":null, ";
      }
      if(lowerBoundTotalCount) {
        str += "\"lowerBoundTotalCount\":" + lowerBoundTotalCount + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lowerBoundTotalCount\":null, ";
      }
      if(previousPageOffset) {
        str += "\"previousPageOffset\":" + previousPageOffset + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"previousPageOffset\":null, ";
      }
      if(nextPageOffset) {
        str += "\"nextPageOffset\":" + nextPageOffset + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nextPageOffset\":null, ";
      }
      if(lastPageOffset) {
        str += "\"lastPageOffset\":" + lastPageOffset + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastPageOffset\":null, ";
      }
      if(lastPageIndex) {
        str += "\"lastPageIndex\":" + lastPageIndex + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastPageIndex\":null, ";
      }
      if(firstActionEnabled) {
        str += "\"firstActionEnabled\":" + firstActionEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"firstActionEnabled\":null, ";
      }
      if(previousActionEnabled) {
        str += "\"previousActionEnabled\":" + previousActionEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"previousActionEnabled\":null, ";
      }
      if(nextActionEnabled) {
        str += "\"nextActionEnabled\":" + nextActionEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nextActionEnabled\":null, ";
      }
      if(lastActionEnabled) {
        str += "\"lastActionEnabled\":" + lastActionEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastActionEnabled\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "pagerMode:" + pagerMode + ", ";
      str += "primaryOrdering:" + primaryOrdering + ", ";
      str += "secondaryOrdering:" + secondaryOrdering + ", ";
      str += "currentOffset:" + currentOffset + ", ";
      str += "currentPage:" + currentPage + ", ";
      str += "pageSize:" + pageSize + ", ";
      str += "totalCount:" + totalCount + ", ";
      str += "lowerBoundTotalCount:" + lowerBoundTotalCount + ", ";
      str += "previousPageOffset:" + previousPageOffset + ", ";
      str += "nextPageOffset:" + nextPageOffset + ", ";
      str += "lastPageOffset:" + lastPageOffset + ", ";
      str += "lastPageIndex:" + lastPageIndex + ", ";
      str += "firstActionEnabled:" + firstActionEnabled + ", ";
      str += "previousActionEnabled:" + previousActionEnabled + ", ";
      str += "nextActionEnabled:" + nextActionEnabled + ", ";
      str += "lastActionEnabled:" + lastActionEnabled + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.PagerStateStructJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.PagerStateStructJsBean();

  if(obj.pagerMode !== undefined && obj.pagerMode != null) {
    o.setPagerMode(obj.pagerMode);
  }
  if(obj.primaryOrdering !== undefined && obj.primaryOrdering != null) {
    o.setPrimaryOrdering(obj.primaryOrdering);
  }
  if(obj.secondaryOrdering !== undefined && obj.secondaryOrdering != null) {
    o.setSecondaryOrdering(obj.secondaryOrdering);
  }
  if(obj.currentOffset !== undefined && obj.currentOffset != null) {
    o.setCurrentOffset(obj.currentOffset);
  }
  if(obj.currentPage !== undefined && obj.currentPage != null) {
    o.setCurrentPage(obj.currentPage);
  }
  if(obj.pageSize !== undefined && obj.pageSize != null) {
    o.setPageSize(obj.pageSize);
  }
  if(obj.totalCount !== undefined && obj.totalCount != null) {
    o.setTotalCount(obj.totalCount);
  }
  if(obj.lowerBoundTotalCount !== undefined && obj.lowerBoundTotalCount != null) {
    o.setLowerBoundTotalCount(obj.lowerBoundTotalCount);
  }
  if(obj.previousPageOffset !== undefined && obj.previousPageOffset != null) {
    o.setPreviousPageOffset(obj.previousPageOffset);
  }
  if(obj.nextPageOffset !== undefined && obj.nextPageOffset != null) {
    o.setNextPageOffset(obj.nextPageOffset);
  }
  if(obj.lastPageOffset !== undefined && obj.lastPageOffset != null) {
    o.setLastPageOffset(obj.lastPageOffset);
  }
  if(obj.lastPageIndex !== undefined && obj.lastPageIndex != null) {
    o.setLastPageIndex(obj.lastPageIndex);
  }
  if(obj.firstActionEnabled !== undefined && obj.firstActionEnabled != null) {
    o.setFirstActionEnabled(obj.firstActionEnabled);
  }
  if(obj.previousActionEnabled !== undefined && obj.previousActionEnabled != null) {
    o.setPreviousActionEnabled(obj.previousActionEnabled);
  }
  if(obj.nextActionEnabled !== undefined && obj.nextActionEnabled != null) {
    o.setNextActionEnabled(obj.nextActionEnabled);
  }
  if(obj.lastActionEnabled !== undefined && obj.lastActionEnabled != null) {
    o.setLastActionEnabled(obj.lastActionEnabled);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

queryclient.wa.bean.PagerStateStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.PagerStateStructJsBean.create(jsonObj);
  return obj;
};
