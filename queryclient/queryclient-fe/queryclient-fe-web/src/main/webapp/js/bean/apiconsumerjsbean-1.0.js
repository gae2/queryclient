//////////////////////////////////////////////////////////
// <script src="/js/bean/apiconsumerjsbean-1.0.js"></script>
// Last modified time: 1389495277612.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.ApiConsumerJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var aeryId;
    var name;
    var description;
    var appKey;
    var appSecret;
    var status;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getAeryId = function() { return aeryId; };
    this.setAeryId = function(value) { aeryId = value; };
    this.getName = function() { return name; };
    this.setName = function(value) { name = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getAppKey = function() { return appKey; };
    this.setAppKey = function(value) { appKey = value; };
    this.getAppSecret = function() { return appSecret; };
    this.setAppSecret = function(value) { appSecret = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.ApiConsumerJsBean();

      o.setGuid(generateUuid());
      if(aeryId !== undefined && aeryId != null) {
        o.setAeryId(aeryId);
      }
      if(name !== undefined && name != null) {
        o.setName(name);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(appKey !== undefined && appKey != null) {
        o.setAppKey(appKey);
      }
      if(appSecret !== undefined && appSecret != null) {
        o.setAppSecret(appSecret);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(aeryId !== undefined && aeryId != null) {
        jsonObj.aeryId = aeryId;
      } // Otherwise ignore...
      if(name !== undefined && name != null) {
        jsonObj.name = name;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(appKey !== undefined && appKey != null) {
        jsonObj.appKey = appKey;
      } // Otherwise ignore...
      if(appSecret !== undefined && appSecret != null) {
        jsonObj.appSecret = appSecret;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(aeryId) {
        str += "\"aeryId\":\"" + aeryId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"aeryId\":null, ";
      }
      if(name) {
        str += "\"name\":\"" + name + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"name\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(appKey) {
        str += "\"appKey\":\"" + appKey + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appKey\":null, ";
      }
      if(appSecret) {
        str += "\"appSecret\":\"" + appSecret + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appSecret\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "aeryId:" + aeryId + ", ";
      str += "name:" + name + ", ";
      str += "description:" + description + ", ";
      str += "appKey:" + appKey + ", ";
      str += "appSecret:" + appSecret + ", ";
      str += "status:" + status + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.ApiConsumerJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.ApiConsumerJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.aeryId !== undefined && obj.aeryId != null) {
    o.setAeryId(obj.aeryId);
  }
  if(obj.name !== undefined && obj.name != null) {
    o.setName(obj.name);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.appKey !== undefined && obj.appKey != null) {
    o.setAppKey(obj.appKey);
  }
  if(obj.appSecret !== undefined && obj.appSecret != null) {
    o.setAppSecret(obj.appSecret);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

queryclient.wa.bean.ApiConsumerJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.ApiConsumerJsBean.create(jsonObj);
  return obj;
};
