//////////////////////////////////////////////////////////
// <script src="/js/bean/externaluseridstructjsbean-1.0.js"></script>
// Last modified time: 1389495277756.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.ExternalUserIdStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var id;
    var name;
    var email;
    var username;
    var openId;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getId = function() { return id; };
    this.setId = function(value) { id = value; };
    this.getName = function() { return name; };
    this.setName = function(value) { name = value; };
    this.getEmail = function() { return email; };
    this.setEmail = function(value) { email = value; };
    this.getUsername = function() { return username; };
    this.setUsername = function(value) { username = value; };
    this.getOpenId = function() { return openId; };
    this.setOpenId = function(value) { openId = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.ExternalUserIdStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(id !== undefined && id != null) {
        o.setId(id);
      }
      if(name !== undefined && name != null) {
        o.setName(name);
      }
      if(email !== undefined && email != null) {
        o.setEmail(email);
      }
      if(username !== undefined && username != null) {
        o.setUsername(username);
      }
      if(openId !== undefined && openId != null) {
        o.setOpenId(openId);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(id !== undefined && id != null) {
        jsonObj.id = id;
      } // Otherwise ignore...
      if(name !== undefined && name != null) {
        jsonObj.name = name;
      } // Otherwise ignore...
      if(email !== undefined && email != null) {
        jsonObj.email = email;
      } // Otherwise ignore...
      if(username !== undefined && username != null) {
        jsonObj.username = username;
      } // Otherwise ignore...
      if(openId !== undefined && openId != null) {
        jsonObj.openId = openId;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(id) {
        str += "\"id\":\"" + id + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"id\":null, ";
      }
      if(name) {
        str += "\"name\":\"" + name + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"name\":null, ";
      }
      if(email) {
        str += "\"email\":\"" + email + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"email\":null, ";
      }
      if(username) {
        str += "\"username\":\"" + username + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"username\":null, ";
      }
      if(openId) {
        str += "\"openId\":\"" + openId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"openId\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "id:" + id + ", ";
      str += "name:" + name + ", ";
      str += "email:" + email + ", ";
      str += "username:" + username + ", ";
      str += "openId:" + openId + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.ExternalUserIdStructJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.ExternalUserIdStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.id !== undefined && obj.id != null) {
    o.setId(obj.id);
  }
  if(obj.name !== undefined && obj.name != null) {
    o.setName(obj.name);
  }
  if(obj.email !== undefined && obj.email != null) {
    o.setEmail(obj.email);
  }
  if(obj.username !== undefined && obj.username != null) {
    o.setUsername(obj.username);
  }
  if(obj.openId !== undefined && obj.openId != null) {
    o.setOpenId(obj.openId);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

queryclient.wa.bean.ExternalUserIdStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.ExternalUserIdStructJsBean.create(jsonObj);
  return obj;
};
