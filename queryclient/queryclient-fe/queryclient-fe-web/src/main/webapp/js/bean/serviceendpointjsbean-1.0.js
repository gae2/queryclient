//////////////////////////////////////////////////////////
// <script src="/js/bean/serviceendpointjsbean-1.0.js"></script>
// Last modified time: 1389495277813.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.ServiceEndpointJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var dataService;
    var serviceName;
    var serviceUrl;
    var authRequired;
    var authCredential;
    var status;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getDataService = function() { return dataService; };
    this.setDataService = function(value) { dataService = value; };
    this.getServiceName = function() { return serviceName; };
    this.setServiceName = function(value) { serviceName = value; };
    this.getServiceUrl = function() { return serviceUrl; };
    this.setServiceUrl = function(value) { serviceUrl = value; };
    this.getAuthRequired = function() { return authRequired; };
    this.setAuthRequired = function(value) { authRequired = value; };
    this.getAuthCredential = function() { return authCredential; };
    this.setAuthCredential = function(value) { authCredential = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.ServiceEndpointJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(dataService !== undefined && dataService != null) {
        o.setDataService(dataService);
      }
      if(serviceName !== undefined && serviceName != null) {
        o.setServiceName(serviceName);
      }
      if(serviceUrl !== undefined && serviceUrl != null) {
        o.setServiceUrl(serviceUrl);
      }
      if(authRequired !== undefined && authRequired != null) {
        o.setAuthRequired(authRequired);
      }
      //o.setAuthCredential(authCredential.clone());
      if(authCredential !== undefined && authCredential != null) {
        o.setAuthCredential(authCredential);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(dataService !== undefined && dataService != null) {
        jsonObj.dataService = dataService;
      } // Otherwise ignore...
      if(serviceName !== undefined && serviceName != null) {
        jsonObj.serviceName = serviceName;
      } // Otherwise ignore...
      if(serviceUrl !== undefined && serviceUrl != null) {
        jsonObj.serviceUrl = serviceUrl;
      } // Otherwise ignore...
      if(authRequired !== undefined && authRequired != null) {
        jsonObj.authRequired = authRequired;
      } // Otherwise ignore...
      if(authCredential !== undefined && authCredential != null) {
        jsonObj.authCredential = authCredential;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(dataService) {
        str += "\"dataService\":\"" + dataService + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"dataService\":null, ";
      }
      if(serviceName) {
        str += "\"serviceName\":\"" + serviceName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"serviceName\":null, ";
      }
      if(serviceUrl) {
        str += "\"serviceUrl\":\"" + serviceUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"serviceUrl\":null, ";
      }
      if(authRequired) {
        str += "\"authRequired\":" + authRequired + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authRequired\":null, ";
      }
      str += "\"authCredential\":" + authCredential.toJsonString() + ", ";
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "dataService:" + dataService + ", ";
      str += "serviceName:" + serviceName + ", ";
      str += "serviceUrl:" + serviceUrl + ", ";
      str += "authRequired:" + authRequired + ", ";
      str += "authCredential:" + authCredential + ", ";
      str += "status:" + status + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.ServiceEndpointJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.ServiceEndpointJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.dataService !== undefined && obj.dataService != null) {
    o.setDataService(obj.dataService);
  }
  if(obj.serviceName !== undefined && obj.serviceName != null) {
    o.setServiceName(obj.serviceName);
  }
  if(obj.serviceUrl !== undefined && obj.serviceUrl != null) {
    o.setServiceUrl(obj.serviceUrl);
  }
  if(obj.authRequired !== undefined && obj.authRequired != null) {
    o.setAuthRequired(obj.authRequired);
  }
  if(obj.authCredential !== undefined && obj.authCredential != null) {
    o.setAuthCredential(obj.authCredential);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

queryclient.wa.bean.ServiceEndpointJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.ServiceEndpointJsBean.create(jsonObj);
  return obj;
};
