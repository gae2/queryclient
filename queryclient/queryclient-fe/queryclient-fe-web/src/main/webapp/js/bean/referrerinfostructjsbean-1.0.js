//////////////////////////////////////////////////////////
// <script src="/js/bean/referrerinfostructjsbean-1.0.js"></script>
// Last modified time: 1389495277559.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.ReferrerInfoStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var referer;
    var userAgent;
    var language;
    var hostname;
    var ipAddress;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getReferer = function() { return referer; };
    this.setReferer = function(value) { referer = value; };
    this.getUserAgent = function() { return userAgent; };
    this.setUserAgent = function(value) { userAgent = value; };
    this.getLanguage = function() { return language; };
    this.setLanguage = function(value) { language = value; };
    this.getHostname = function() { return hostname; };
    this.setHostname = function(value) { hostname = value; };
    this.getIpAddress = function() { return ipAddress; };
    this.setIpAddress = function(value) { ipAddress = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.ReferrerInfoStructJsBean();

      if(referer !== undefined && referer != null) {
        o.setReferer(referer);
      }
      if(userAgent !== undefined && userAgent != null) {
        o.setUserAgent(userAgent);
      }
      if(language !== undefined && language != null) {
        o.setLanguage(language);
      }
      if(hostname !== undefined && hostname != null) {
        o.setHostname(hostname);
      }
      if(ipAddress !== undefined && ipAddress != null) {
        o.setIpAddress(ipAddress);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(referer !== undefined && referer != null) {
        jsonObj.referer = referer;
      } // Otherwise ignore...
      if(userAgent !== undefined && userAgent != null) {
        jsonObj.userAgent = userAgent;
      } // Otherwise ignore...
      if(language !== undefined && language != null) {
        jsonObj.language = language;
      } // Otherwise ignore...
      if(hostname !== undefined && hostname != null) {
        jsonObj.hostname = hostname;
      } // Otherwise ignore...
      if(ipAddress !== undefined && ipAddress != null) {
        jsonObj.ipAddress = ipAddress;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(referer) {
        str += "\"referer\":\"" + referer + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"referer\":null, ";
      }
      if(userAgent) {
        str += "\"userAgent\":\"" + userAgent + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userAgent\":null, ";
      }
      if(language) {
        str += "\"language\":\"" + language + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"language\":null, ";
      }
      if(hostname) {
        str += "\"hostname\":\"" + hostname + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"hostname\":null, ";
      }
      if(ipAddress) {
        str += "\"ipAddress\":\"" + ipAddress + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ipAddress\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "referer:" + referer + ", ";
      str += "userAgent:" + userAgent + ", ";
      str += "language:" + language + ", ";
      str += "hostname:" + hostname + ", ";
      str += "ipAddress:" + ipAddress + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.ReferrerInfoStructJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.ReferrerInfoStructJsBean();

  if(obj.referer !== undefined && obj.referer != null) {
    o.setReferer(obj.referer);
  }
  if(obj.userAgent !== undefined && obj.userAgent != null) {
    o.setUserAgent(obj.userAgent);
  }
  if(obj.language !== undefined && obj.language != null) {
    o.setLanguage(obj.language);
  }
  if(obj.hostname !== undefined && obj.hostname != null) {
    o.setHostname(obj.hostname);
  }
  if(obj.ipAddress !== undefined && obj.ipAddress != null) {
    o.setIpAddress(obj.ipAddress);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

queryclient.wa.bean.ReferrerInfoStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.ReferrerInfoStructJsBean.create(jsonObj);
  return obj;
};
