//////////////////////////////////////////////////////////
// <script src="/js/bean/consumerkeysecretpairjsbean-1.0.js"></script>
// Last modified time: 1389495277796.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.ConsumerKeySecretPairJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var consumerKey;
    var consumerSecret;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getConsumerKey = function() { return consumerKey; };
    this.setConsumerKey = function(value) { consumerKey = value; };
    this.getConsumerSecret = function() { return consumerSecret; };
    this.setConsumerSecret = function(value) { consumerSecret = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.ConsumerKeySecretPairJsBean();

      if(consumerKey !== undefined && consumerKey != null) {
        o.setConsumerKey(consumerKey);
      }
      if(consumerSecret !== undefined && consumerSecret != null) {
        o.setConsumerSecret(consumerSecret);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(consumerKey !== undefined && consumerKey != null) {
        jsonObj.consumerKey = consumerKey;
      } // Otherwise ignore...
      if(consumerSecret !== undefined && consumerSecret != null) {
        jsonObj.consumerSecret = consumerSecret;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(consumerKey) {
        str += "\"consumerKey\":\"" + consumerKey + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"consumerKey\":null, ";
      }
      if(consumerSecret) {
        str += "\"consumerSecret\":\"" + consumerSecret + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"consumerSecret\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "consumerKey:" + consumerKey + ", ";
      str += "consumerSecret:" + consumerSecret + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.ConsumerKeySecretPairJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.ConsumerKeySecretPairJsBean();

  if(obj.consumerKey !== undefined && obj.consumerKey != null) {
    o.setConsumerKey(obj.consumerKey);
  }
  if(obj.consumerSecret !== undefined && obj.consumerSecret != null) {
    o.setConsumerSecret(obj.consumerSecret);
  }
    
  return o;
};

queryclient.wa.bean.ConsumerKeySecretPairJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.ConsumerKeySecretPairJsBean.create(jsonObj);
  return obj;
};
