//////////////////////////////////////////////////////////
// <script src="/js/bean/userauthstatejsbean-1.0.js"></script>
// Last modified time: 1389495277783.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.UserAuthStateJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var managerApp;
    var appAcl;
    var gaeApp;
    var ownerUser;
    var userAcl;
    var providerId;
    var user;
    var username;
    var email;
    var openId;
    var deviceId;
    var sessionId;
    var authToken;
    var authStatus;
    var externalAuth;
    var externalId;
    var status;
    var firstAuthTime;
    var lastAuthTime;
    var expirationTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getManagerApp = function() { return managerApp; };
    this.setManagerApp = function(value) { managerApp = value; };
    this.getAppAcl = function() { return appAcl; };
    this.setAppAcl = function(value) { appAcl = value; };
    this.getGaeApp = function() { return gaeApp; };
    this.setGaeApp = function(value) { gaeApp = value; };
    this.getOwnerUser = function() { return ownerUser; };
    this.setOwnerUser = function(value) { ownerUser = value; };
    this.getUserAcl = function() { return userAcl; };
    this.setUserAcl = function(value) { userAcl = value; };
    this.getProviderId = function() { return providerId; };
    this.setProviderId = function(value) { providerId = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getUsername = function() { return username; };
    this.setUsername = function(value) { username = value; };
    this.getEmail = function() { return email; };
    this.setEmail = function(value) { email = value; };
    this.getOpenId = function() { return openId; };
    this.setOpenId = function(value) { openId = value; };
    this.getDeviceId = function() { return deviceId; };
    this.setDeviceId = function(value) { deviceId = value; };
    this.getSessionId = function() { return sessionId; };
    this.setSessionId = function(value) { sessionId = value; };
    this.getAuthToken = function() { return authToken; };
    this.setAuthToken = function(value) { authToken = value; };
    this.getAuthStatus = function() { return authStatus; };
    this.setAuthStatus = function(value) { authStatus = value; };
    this.getExternalAuth = function() { return externalAuth; };
    this.setExternalAuth = function(value) { externalAuth = value; };
    this.getExternalId = function() { return externalId; };
    this.setExternalId = function(value) { externalId = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getFirstAuthTime = function() { return firstAuthTime; };
    this.setFirstAuthTime = function(value) { firstAuthTime = value; };
    this.getLastAuthTime = function() { return lastAuthTime; };
    this.setLastAuthTime = function(value) { lastAuthTime = value; };
    this.getExpirationTime = function() { return expirationTime; };
    this.setExpirationTime = function(value) { expirationTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.UserAuthStateJsBean();

      o.setGuid(generateUuid());
      if(managerApp !== undefined && managerApp != null) {
        o.setManagerApp(managerApp);
      }
      if(appAcl !== undefined && appAcl != null) {
        o.setAppAcl(appAcl);
      }
      //o.setGaeApp(gaeApp.clone());
      if(gaeApp !== undefined && gaeApp != null) {
        o.setGaeApp(gaeApp);
      }
      if(ownerUser !== undefined && ownerUser != null) {
        o.setOwnerUser(ownerUser);
      }
      if(userAcl !== undefined && userAcl != null) {
        o.setUserAcl(userAcl);
      }
      if(providerId !== undefined && providerId != null) {
        o.setProviderId(providerId);
      }
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(username !== undefined && username != null) {
        o.setUsername(username);
      }
      if(email !== undefined && email != null) {
        o.setEmail(email);
      }
      if(openId !== undefined && openId != null) {
        o.setOpenId(openId);
      }
      if(deviceId !== undefined && deviceId != null) {
        o.setDeviceId(deviceId);
      }
      if(sessionId !== undefined && sessionId != null) {
        o.setSessionId(sessionId);
      }
      if(authToken !== undefined && authToken != null) {
        o.setAuthToken(authToken);
      }
      if(authStatus !== undefined && authStatus != null) {
        o.setAuthStatus(authStatus);
      }
      if(externalAuth !== undefined && externalAuth != null) {
        o.setExternalAuth(externalAuth);
      }
      //o.setExternalId(externalId.clone());
      if(externalId !== undefined && externalId != null) {
        o.setExternalId(externalId);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(firstAuthTime !== undefined && firstAuthTime != null) {
        o.setFirstAuthTime(firstAuthTime);
      }
      if(lastAuthTime !== undefined && lastAuthTime != null) {
        o.setLastAuthTime(lastAuthTime);
      }
      if(expirationTime !== undefined && expirationTime != null) {
        o.setExpirationTime(expirationTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(managerApp !== undefined && managerApp != null) {
        jsonObj.managerApp = managerApp;
      } // Otherwise ignore...
      if(appAcl !== undefined && appAcl != null) {
        jsonObj.appAcl = appAcl;
      } // Otherwise ignore...
      if(gaeApp !== undefined && gaeApp != null) {
        jsonObj.gaeApp = gaeApp;
      } // Otherwise ignore...
      if(ownerUser !== undefined && ownerUser != null) {
        jsonObj.ownerUser = ownerUser;
      } // Otherwise ignore...
      if(userAcl !== undefined && userAcl != null) {
        jsonObj.userAcl = userAcl;
      } // Otherwise ignore...
      if(providerId !== undefined && providerId != null) {
        jsonObj.providerId = providerId;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(username !== undefined && username != null) {
        jsonObj.username = username;
      } // Otherwise ignore...
      if(email !== undefined && email != null) {
        jsonObj.email = email;
      } // Otherwise ignore...
      if(openId !== undefined && openId != null) {
        jsonObj.openId = openId;
      } // Otherwise ignore...
      if(deviceId !== undefined && deviceId != null) {
        jsonObj.deviceId = deviceId;
      } // Otherwise ignore...
      if(sessionId !== undefined && sessionId != null) {
        jsonObj.sessionId = sessionId;
      } // Otherwise ignore...
      if(authToken !== undefined && authToken != null) {
        jsonObj.authToken = authToken;
      } // Otherwise ignore...
      if(authStatus !== undefined && authStatus != null) {
        jsonObj.authStatus = authStatus;
      } // Otherwise ignore...
      if(externalAuth !== undefined && externalAuth != null) {
        jsonObj.externalAuth = externalAuth;
      } // Otherwise ignore...
      if(externalId !== undefined && externalId != null) {
        jsonObj.externalId = externalId;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(firstAuthTime !== undefined && firstAuthTime != null) {
        jsonObj.firstAuthTime = firstAuthTime;
      } // Otherwise ignore...
      if(lastAuthTime !== undefined && lastAuthTime != null) {
        jsonObj.lastAuthTime = lastAuthTime;
      } // Otherwise ignore...
      if(expirationTime !== undefined && expirationTime != null) {
        jsonObj.expirationTime = expirationTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(managerApp) {
        str += "\"managerApp\":\"" + managerApp + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"managerApp\":null, ";
      }
      if(appAcl) {
        str += "\"appAcl\":" + appAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appAcl\":null, ";
      }
      str += "\"gaeApp\":" + gaeApp.toJsonString() + ", ";
      if(ownerUser) {
        str += "\"ownerUser\":\"" + ownerUser + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ownerUser\":null, ";
      }
      if(userAcl) {
        str += "\"userAcl\":" + userAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userAcl\":null, ";
      }
      if(providerId) {
        str += "\"providerId\":\"" + providerId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"providerId\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(username) {
        str += "\"username\":\"" + username + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"username\":null, ";
      }
      if(email) {
        str += "\"email\":\"" + email + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"email\":null, ";
      }
      if(openId) {
        str += "\"openId\":\"" + openId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"openId\":null, ";
      }
      if(deviceId) {
        str += "\"deviceId\":\"" + deviceId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"deviceId\":null, ";
      }
      if(sessionId) {
        str += "\"sessionId\":\"" + sessionId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sessionId\":null, ";
      }
      if(authToken) {
        str += "\"authToken\":\"" + authToken + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authToken\":null, ";
      }
      if(authStatus) {
        str += "\"authStatus\":\"" + authStatus + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authStatus\":null, ";
      }
      if(externalAuth) {
        str += "\"externalAuth\":\"" + externalAuth + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"externalAuth\":null, ";
      }
      str += "\"externalId\":" + externalId.toJsonString() + ", ";
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(firstAuthTime) {
        str += "\"firstAuthTime\":" + firstAuthTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"firstAuthTime\":null, ";
      }
      if(lastAuthTime) {
        str += "\"lastAuthTime\":" + lastAuthTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastAuthTime\":null, ";
      }
      if(expirationTime) {
        str += "\"expirationTime\":" + expirationTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"expirationTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "managerApp:" + managerApp + ", ";
      str += "appAcl:" + appAcl + ", ";
      str += "gaeApp:" + gaeApp + ", ";
      str += "ownerUser:" + ownerUser + ", ";
      str += "userAcl:" + userAcl + ", ";
      str += "providerId:" + providerId + ", ";
      str += "user:" + user + ", ";
      str += "username:" + username + ", ";
      str += "email:" + email + ", ";
      str += "openId:" + openId + ", ";
      str += "deviceId:" + deviceId + ", ";
      str += "sessionId:" + sessionId + ", ";
      str += "authToken:" + authToken + ", ";
      str += "authStatus:" + authStatus + ", ";
      str += "externalAuth:" + externalAuth + ", ";
      str += "externalId:" + externalId + ", ";
      str += "status:" + status + ", ";
      str += "firstAuthTime:" + firstAuthTime + ", ";
      str += "lastAuthTime:" + lastAuthTime + ", ";
      str += "expirationTime:" + expirationTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.UserAuthStateJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.UserAuthStateJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.managerApp !== undefined && obj.managerApp != null) {
    o.setManagerApp(obj.managerApp);
  }
  if(obj.appAcl !== undefined && obj.appAcl != null) {
    o.setAppAcl(obj.appAcl);
  }
  if(obj.gaeApp !== undefined && obj.gaeApp != null) {
    o.setGaeApp(obj.gaeApp);
  }
  if(obj.ownerUser !== undefined && obj.ownerUser != null) {
    o.setOwnerUser(obj.ownerUser);
  }
  if(obj.userAcl !== undefined && obj.userAcl != null) {
    o.setUserAcl(obj.userAcl);
  }
  if(obj.providerId !== undefined && obj.providerId != null) {
    o.setProviderId(obj.providerId);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.username !== undefined && obj.username != null) {
    o.setUsername(obj.username);
  }
  if(obj.email !== undefined && obj.email != null) {
    o.setEmail(obj.email);
  }
  if(obj.openId !== undefined && obj.openId != null) {
    o.setOpenId(obj.openId);
  }
  if(obj.deviceId !== undefined && obj.deviceId != null) {
    o.setDeviceId(obj.deviceId);
  }
  if(obj.sessionId !== undefined && obj.sessionId != null) {
    o.setSessionId(obj.sessionId);
  }
  if(obj.authToken !== undefined && obj.authToken != null) {
    o.setAuthToken(obj.authToken);
  }
  if(obj.authStatus !== undefined && obj.authStatus != null) {
    o.setAuthStatus(obj.authStatus);
  }
  if(obj.externalAuth !== undefined && obj.externalAuth != null) {
    o.setExternalAuth(obj.externalAuth);
  }
  if(obj.externalId !== undefined && obj.externalId != null) {
    o.setExternalId(obj.externalId);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.firstAuthTime !== undefined && obj.firstAuthTime != null) {
    o.setFirstAuthTime(obj.firstAuthTime);
  }
  if(obj.lastAuthTime !== undefined && obj.lastAuthTime != null) {
    o.setLastAuthTime(obj.lastAuthTime);
  }
  if(obj.expirationTime !== undefined && obj.expirationTime != null) {
    o.setExpirationTime(obj.expirationTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

queryclient.wa.bean.UserAuthStateJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.UserAuthStateJsBean.create(jsonObj);
  return obj;
};
