//////////////////////////////////////////////////////////
// <script src="/js/bean/gaeuserstructjsbean-1.0.js"></script>
// Last modified time: 1389495277588.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.GaeUserStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var authDomain;
    var federatedIdentity;
    var nickname;
    var userId;
    var email;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getAuthDomain = function() { return authDomain; };
    this.setAuthDomain = function(value) { authDomain = value; };
    this.getFederatedIdentity = function() { return federatedIdentity; };
    this.setFederatedIdentity = function(value) { federatedIdentity = value; };
    this.getNickname = function() { return nickname; };
    this.setNickname = function(value) { nickname = value; };
    this.getUserId = function() { return userId; };
    this.setUserId = function(value) { userId = value; };
    this.getEmail = function() { return email; };
    this.setEmail = function(value) { email = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.GaeUserStructJsBean();

      if(authDomain !== undefined && authDomain != null) {
        o.setAuthDomain(authDomain);
      }
      if(federatedIdentity !== undefined && federatedIdentity != null) {
        o.setFederatedIdentity(federatedIdentity);
      }
      if(nickname !== undefined && nickname != null) {
        o.setNickname(nickname);
      }
      if(userId !== undefined && userId != null) {
        o.setUserId(userId);
      }
      if(email !== undefined && email != null) {
        o.setEmail(email);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(authDomain !== undefined && authDomain != null) {
        jsonObj.authDomain = authDomain;
      } // Otherwise ignore...
      if(federatedIdentity !== undefined && federatedIdentity != null) {
        jsonObj.federatedIdentity = federatedIdentity;
      } // Otherwise ignore...
      if(nickname !== undefined && nickname != null) {
        jsonObj.nickname = nickname;
      } // Otherwise ignore...
      if(userId !== undefined && userId != null) {
        jsonObj.userId = userId;
      } // Otherwise ignore...
      if(email !== undefined && email != null) {
        jsonObj.email = email;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(authDomain) {
        str += "\"authDomain\":\"" + authDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authDomain\":null, ";
      }
      if(federatedIdentity) {
        str += "\"federatedIdentity\":\"" + federatedIdentity + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"federatedIdentity\":null, ";
      }
      if(nickname) {
        str += "\"nickname\":\"" + nickname + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nickname\":null, ";
      }
      if(userId) {
        str += "\"userId\":\"" + userId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userId\":null, ";
      }
      if(email) {
        str += "\"email\":\"" + email + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"email\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "authDomain:" + authDomain + ", ";
      str += "federatedIdentity:" + federatedIdentity + ", ";
      str += "nickname:" + nickname + ", ";
      str += "userId:" + userId + ", ";
      str += "email:" + email + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.GaeUserStructJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.GaeUserStructJsBean();

  if(obj.authDomain !== undefined && obj.authDomain != null) {
    o.setAuthDomain(obj.authDomain);
  }
  if(obj.federatedIdentity !== undefined && obj.federatedIdentity != null) {
    o.setFederatedIdentity(obj.federatedIdentity);
  }
  if(obj.nickname !== undefined && obj.nickname != null) {
    o.setNickname(obj.nickname);
  }
  if(obj.userId !== undefined && obj.userId != null) {
    o.setUserId(obj.userId);
  }
  if(obj.email !== undefined && obj.email != null) {
    o.setEmail(obj.email);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

queryclient.wa.bean.GaeUserStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.GaeUserStructJsBean.create(jsonObj);
  return obj;
};
