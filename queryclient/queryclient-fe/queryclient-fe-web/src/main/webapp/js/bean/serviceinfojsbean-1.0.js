//////////////////////////////////////////////////////////
// <script src="/js/bean/serviceinfojsbean-1.0.js"></script>
// Last modified time: 1389495277853.
//////////////////////////////////////////////////////////

var queryclient = queryclient || {};
queryclient.wa = queryclient.wa || {};
queryclient.wa.bean = queryclient.wa.bean || {};
queryclient.wa.bean.ServiceInfoJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var title;
    var content;
    var type;
    var status;
    var scheduledTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getContent = function() { return content; };
    this.setContent = function(value) { content = value; };
    this.getType = function() { return type; };
    this.setType = function(value) { type = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getScheduledTime = function() { return scheduledTime; };
    this.setScheduledTime = function(value) { scheduledTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new queryclient.wa.bean.ServiceInfoJsBean();

      o.setGuid(generateUuid());
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(content !== undefined && content != null) {
        o.setContent(content);
      }
      if(type !== undefined && type != null) {
        o.setType(type);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(scheduledTime !== undefined && scheduledTime != null) {
        o.setScheduledTime(scheduledTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(content !== undefined && content != null) {
        jsonObj.content = content;
      } // Otherwise ignore...
      if(type !== undefined && type != null) {
        jsonObj.type = type;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(scheduledTime !== undefined && scheduledTime != null) {
        jsonObj.scheduledTime = scheduledTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(content) {
        str += "\"content\":\"" + content + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"content\":null, ";
      }
      if(type) {
        str += "\"type\":\"" + type + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"type\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(scheduledTime) {
        str += "\"scheduledTime\":" + scheduledTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"scheduledTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "title:" + title + ", ";
      str += "content:" + content + ", ";
      str += "type:" + type + ", ";
      str += "status:" + status + ", ";
      str += "scheduledTime:" + scheduledTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

queryclient.wa.bean.ServiceInfoJsBean.create = function(obj) {
  var o = new queryclient.wa.bean.ServiceInfoJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.content !== undefined && obj.content != null) {
    o.setContent(obj.content);
  }
  if(obj.type !== undefined && obj.type != null) {
    o.setType(obj.type);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.scheduledTime !== undefined && obj.scheduledTime != null) {
    o.setScheduledTime(obj.scheduledTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

queryclient.wa.bean.ServiceInfoJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = queryclient.wa.bean.ServiceInfoJsBean.create(jsonObj);
  return obj;
};
