/*
Utility functions.
*/


// EmailUtil object.
var webstoa = webstoa || {};
webstoa.EmailUtil = webstoa.EmailUtil || {};


// Email address de-obfuscator
webstoa.EmailUtil.decodeEmailAddress = function(email) 
{
    if(! email) {
    	return '';
    }
	var exp = /([a-z0-9._%-]+)\+([a-z0-9._%-]+)\+([a-z.]+)/ig;
    return email.replace(exp, '$1' + '@' + '$2' + '.' + '$3'); 
};
webstoa.EmailUtil.decodeMailtoHref = function(href) 
{
    if(! href) {
    	return '';
    }
    if(href.length > 8 && href.substring(0,8) == 'contact:') {
        var exp = /contact:([a-z0-9._%-]+)\+([a-z0-9._%-]+)\+([a-z.]+)/ig;
        return href.replace(exp, 'mailto:$1' + '@' + '$2' + '.' + '$3');
    } else {
    	return href;
    }
};
webstoa.EmailUtil.decodeMailtoAnchor = function(anchor) 
{
    if(! anchor) {
    	return;
    }
	var href = anchor.attr("href");
	if(href) {
        var newHref = webstoa.EmailUtil.decodeMailtoHref(href);
        anchor.attr("href", newHref);
	}
};

