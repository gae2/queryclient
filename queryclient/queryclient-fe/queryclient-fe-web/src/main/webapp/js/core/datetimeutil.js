/*
Date/Time-related utility functions.
TBD: Add namespace...
*/

// temporary
function getUTCStringFromTime(tm, dateOrder, datePartSep)
{
    var str = '';
    if(tm) {
        var d = new Date(parseInt(tm));
        if(d) {
            if(! dateOrder) { 
            	dateOrder = 'ymd';   // default.  Valid format: 'ymd' or 'mdy' or 'dmy'
            }  // else.. TBD: Validation????
            if(typeof datePartSep === 'undefined' || datePartSep == null) {
                datePartSep = '-';   // default.  Note: '' is a valid datePartSep.
            }
            var dateTimeSep = ' ';
            var timePartSep = ':';

            var yr = d.getUTCFullYear();
            var mo = d.getUTCMonth()+1;
            var dy = d.getUTCDate();
            var hr = d.getUTCHours();
            var mi = d.getUTCMinutes();

            if(dateOrder === 'mdy') {
                if(mo < 10) {
                    str += '0' + mo + datePartSep;
                } else {
                    str += mo + datePartSep; 
                }
                if(dy < 10) {
                    str += '0' + dy + datePartSep;
                } else {
                    str += dy + datePartSep; 
                }
                str += yr + dateTimeSep;
            } else if(dateOrder === 'dmy') {
                if(dy < 10) {
                    str += '0' + dy + datePartSep;
                } else {
                    str += dy + datePartSep; 
                }
                if(mo < 10) {
                    str += '0' + mo + datePartSep;
                } else {
                    str += mo + datePartSep; 
                }
                str += yr + dateTimeSep;
            } else {    // 'ymd'
                str += yr + datePartSep;
                if(mo < 10) {
                    str += '0' + mo + datePartSep;
                } else {
                    str += mo + datePartSep; 
                }
                if(dy < 10) {
                    str += '0' + dy + dateTimeSep;
                } else {
                    str += dy + dateTimeSep; 
                }
            }

            if(hr < 10) {
                str += '0' + hr + timePartSep;
            } else {
                str += hr + timePartSep;
            }
            if(mi < 10) {
                str += '0' + mi;
            } else {
                str += mi;
            }
        }
    }
    return str;
}
function getStringFromTime(tm, dateOrder, datePartSep)
{
    var str = '';
    if(tm) {
        var d = new Date(parseInt(tm));
        if(d) {
            if(! dateOrder) { 
            	dateOrder = 'ymd';   // default.  Valid format: 'ymd' or 'mdy' or 'dmy'
            }  // else.. TBD: Validation????
            if(typeof datePartSep === 'undefined' || datePartSep == null) {
                datePartSep = '-';   // default.  Note: '' is a valid datePartSep.
            }
            var dateTimeSep = ' ';
            var timePartSep = ':';

            var yr = d.getFullYear();
            var mo = d.getMonth()+1;
            var dy = d.getDate();
            var hr = d.getHours();
            var mi = d.getMinutes();

            if(dateOrder === 'mdy') {
                if(mo < 10) {
                    str += '0' + mo + datePartSep;
                } else {
                    str += mo + datePartSep; 
                }
                if(dy < 10) {
                    str += '0' + dy + datePartSep;
                } else {
                    str += dy + datePartSep; 
                }
                str += yr + dateTimeSep;
            } else if(dateOrder === 'dmy') {
                if(dy < 10) {
                    str += '0' + dy + datePartSep;
                } else {
                    str += dy + datePartSep; 
                }
                if(mo < 10) {
                    str += '0' + mo + datePartSep;
                } else {
                    str += mo + datePartSep; 
                }
                str += yr + dateTimeSep;
            } else {    // 'ymd'
                str += yr + datePartSep;
                if(mo < 10) {
                    str += '0' + mo + datePartSep;
                } else {
                    str += mo + datePartSep; 
                }
                if(dy < 10) {
                    str += '0' + dy + dateTimeSep;
                } else {
                    str += dy + dateTimeSep; 
                }
            }

            if(hr < 10) {
                str += '0' + hr + timePartSep;
            } else {
                str += hr + timePartSep;
            }
            if(mi < 10) {
                str += '0' + mi;
            } else {
                str += mi;
            }
        }
    }
    return str;
}
