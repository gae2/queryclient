/*
"Status message" helper.
Requires jquery.floatobject plugin.
*/


//$(function() {
    var $statusDiv = $('<div id="statushelper_status_floater" style="display: none;"><div id="statushelper_div_status_message"><span id="statushelper_span_status_message">(Status)</span></div></div>');
    $('body').append($statusDiv);
//});


// StatusHelper object.
var webstoa = webstoa || {};
webstoa.StatusHelper = ( function() {

    var cls = function(x, y) {

	    if(!x) {
	    	x = 400; // temporary
	    }
	    if(!y) {
	    	y = 100; // temporary
	    }

	    var params = {x: x, y: y, speed: 'normal' };
	    $("#statushelper_status_floater").makeFloat(params);

    this.update = function(msg, type, time) {
    	if(DEBUG_ENABLED) console.log("Updating status with msg = " + msg);

    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#statushelper_status_floater").text(msg);
    	//$("#statushelper_status_floater").fadeIn().delay(time).fadeOut();
		$("#statushelper_status_floater").textInOut(msg, type, time);
    };
  };

  return cls;
})();

