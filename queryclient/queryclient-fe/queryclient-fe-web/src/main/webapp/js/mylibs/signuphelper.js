/*
SignupBook
It's to be used without "launch definition" (on DB).
Just specify the launch name, and let the user provide his/her email or twitter handle.
*/


//$(function() {
    var $signupDialog = $('<div id="signuphelper_signup_dialog" title="Sign Up Dialog" style="display: none;"><div id="signuphelper_signup_main" style="width:100%;"><form id="signuphelper_signup_form">' 
    		+ '<div id="signuphelper_signup_head" style="width:100%;padding:5px 3px;"><span id="signuphelper_signup_servicename" style="font-weight:bold;font-size:1.2em;">Service</span><br/><span id="signuphelper_signup_launchname" style="font-size:1.05em;">Launch</span></div>' 
    		+ '<div id="signuphelper_signup_body1" style="width:100%;padding:5px 3px;"><label for="signuphelper_signup_email" style="font-size:0.9em;">Your Email Address:</label><input id="signuphelper_signup_email" type="email" style="width:350px;"></input></div>' 
    		+ '<div id="signuphelper_signup_body2" style="width:100%;padding:5px 3px;"><label for="signuphelper_signup_comment" style="font-size:0.9em;">Comment (optional):</label><textarea id="signuphelper_signup_comment" cols="25" rows="3" style="width:350px;"></textarea>' 
    		+ '<input type="hidden" id="signuphelper_signup_serviceguid"></input><input type="hidden" id="signuphelper_signup_launchguid"></input><input type="hidden" id="signuphelper_signup_pageid"></input><input type="hidden" id="signuphelper_signup_buttonid"></input></div>'
            + '</form></div></div>');
    $('body').append($signupDialog);
//});


// SignupHelper object.
var webstoa = webstoa || {};
webstoa.SignupHelper = ( function() {

  var onSignupSuccess = function(result) {
      if(DEBUG_ENABLED) console.log("onSignupSuccess(): result = " + result);
      //updateStatus('Your message has been successfully signuped', 'info', 5550);
  	  $( "#signuphelper_signup_dialog" ).dialog('close');
  };

  var cls = function(servicename, launchname, pageid, buttonid) {
      this.serviceName = servicename ? servicename : '';
      this.launchName = launchname ? launchname : '';
      this.pageId = pageid ? pageid : '';
      this.buttonId = buttonid ? buttonid : '';

      this.signup = function(buttonid) {
    	if(DEBUG_ENABLED) console.log("Signup() called with buttonid = " + buttonid);

    	$('#signuphelper_signup_servicename').text(this.serviceName);
    	$('#signuphelper_signup_launchname').text(this.launchName);
    	$('#signuphelper_signup_pageid').val(this.pageId);
    	if(buttonid) {
            $('#signuphelper_signup_buttonid').val(buttonid);    		
    	} else {
            $('#signuphelper_signup_buttonid').val(this.buttonId);
    	}
    	// ...    	
    	
    	// Reset user input values?
        $('#signuphelper_signup_email').val('');
        $('#signuphelper_signup_comment').val('');

	    $( "#signuphelper_signup_dialog" ).dialog({
            height: 350,
            width: 425,
            modal: true,
            buttons: {
                Submit: function() {
                    // ... 
                	var email = $('#signuphelper_signup_email').val();
                	var comment = $('#signuphelper_signup_comment').val();
                	if(DEBUG_ENABLED) console.log("Signup: email = " + email + "; comment = " + comment);
                	var commentLen = comment.length;
                	if(commentLen > 500) {  // Max 500 chars
                		comment = comment.substring(0, 500);
                    	if(DEBUG_ENABLED) console.log("Comment has been truncated to: " + comment);
                	}
                	
                	if(!email) {  // Or, invalid
                		// TBD:
                		// Error...
                		window.alert("Please specify email address.");
                		return; // ???
                	}
                	
                	var signupObj = {};
                	signupObj.serviceName = $('#signuphelper_signup_servicename').text();
                	signupObj.launchName = $('#signuphelper_signup_launchname').text();
                	signupObj.originDomain = window.location.hostname;
                	signupObj.originPage = $('#signuphelper_signup_pageid').val();
                	signupObj.originButton = $('#signuphelper_signup_buttonid').val();
                	signupObj.referralPage = window.location.href;
                	signupObj.comment = comment;
                	signupObj.signupTime = (new Date()).getTime();
                	var notificationPref = {};
                	notificationPref.preferredMode = 'email';  // ???
                	notificationPref.emailAddress = email;
                	signupObj.notificationPref = notificationPref;
                	
                    var referrerInfo = {};
                    referrerInfo.referer = document.referrer;
                    referrerInfo.userAgent = navigator.userAgent;
                    referrerInfo.language = navigator.language;
                    //referrerInfo.hostname = "";
                    //referrerInfo.ipAddress = "";
                    signupObj.referrerInfo = referrerInfo;
                	
                	// temporary
                	//var signupBookUrl = 'http://localhost:8899/wp/signup';
                	var signupBookUrl = 'http://beta.signupbook.com/wp/signup';
                	var payload = JSON.stringify(signupObj);
                	var payloadParam = '?payload=' + payload
                	signupBookUrl += payloadParam;
                	$.ajax({
                  	    type: 'GET',
                  	    url: signupBookUrl,
                  	    dataType: "jsonp",
                  	    success: function(data, textStatus) {
                  	      // TBD
                  	      if(DEBUG_ENABLED) console.log("Successfully signed up. textStatus = " + textStatus);
                  	      if(DEBUG_ENABLED) console.log("data = " + data);
                  	      if(DEBUG_ENABLED) console.log(data);

            	          //updateStatus('Thanks for signing up.', 'info', 5550);

                  	      // TBD:
                  		  window.alert("Thanks for signing up.");
                  	    },
                  	    error: function(req, textStatus) {
                   	      if(DEBUG_ENABLED) console.log("Failed to sign up. textStatus = " + textStatus);

              	          //updateStatus('Failed to sign up: status = ' + textStatus, 'error', 5550);

                  	      // TBD:
                  		  window.alert("Signup failed. Please try again.");
                  	    }
              	   	  });

                      // Close the dialog....
                      $(this).dialog('close');
                },
                Cancel: function() {
                    $(this).dialog('close'); 
                }
            }
        });    	
    };
  };

  return cls;
})();

