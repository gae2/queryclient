/*
Feedtrail
*/


//$(function() {
    var $feedbackDialog = $('<div id="feedbackhelper_feedback_dialog" title="Feedback Dialog" style="display: none;"><div id="feedbackhelper_feedback_main" style="width:100%;"><form id="feedbackhelper_feedback_form">' 
    		+ '<div id="feedbackhelper_feedback_head" style="width:100%;padding:5px 3px;"><span id="feedbackhelper_feedback_targetservice" style="font-weight:bold;font-size:1.2em;">Service</span><br/><span id="feedbackhelper_feedback_description" style="font-size:1.05em;">Description</span></div>' 
    		+ '<div id="feedbackhelper_feedback_body" style="width:100%;padding:5px 3px;"><label for="feedbackhelper_feedback_longcomment" style="font-size:0.9em;">Comment:</label><textarea id="feedbackhelper_feedback_longcomment" cols="50" rows="8" style="width:400px;"></textarea>' 
    		+ '<input type="hidden" id="feedbackhelper_feedback_targetpage"></input><input type="hidden" id="feedbackhelper_feedback_targetitem"></input></div>'
            + '</form></div></div>');
    $('body').append($feedbackDialog);
//});


// FeedbackHelper object.
var webstoa = webstoa || {};
webstoa.FeedbackHelper = ( function() {

  var onFeedbackSuccess = function(result) {
      if(DEBUG_ENABLED) console.log("onFeedbackSuccess(): result = " + result);
      //updateStatus('Your message has been successfully feedbacked', 'info', 5550);
  	  $( "#feedbackhelper_feedback_dialog" ).dialog('close');
  };

  var cls = function(targetService, targetPage, description) {
      this.targetService = targetService ? targetService : '';
      this.targetPage = targetPage ? targetPage : '';
      this.description = description ? description : 'Please include your comment, and your contact information, if necessary, in the text box below. Thanks in advance for your feedback!';

      this.feedback = function(targetItem) {
    	if(DEBUG_ENABLED) console.log("Feedback() called with targetItem = " + targetItem);

    	$('#feedbackhelper_feedback_targetservice').text(this.targetService);
    	$('#feedbackhelper_feedback_description').text(this.description);
    	$('#feedbackhelper_feedback_targetpage').val(this.targetPage);
        targetItem = targetItem ? targetItem : '';
    	$('#feedbackhelper_feedback_targetitem').val(targetItem);
    	// ...    	
    	
    	// Reset user input values?
        $('#feedbackhelper_feedback_longcomment').val('');

	    $( "#feedbackhelper_feedback_dialog" ).dialog({
            height: 400,
            width: 450,
            modal: true,
            buttons: {
                Send: function() {
                    // ... 
                	var comment = $('#feedbackhelper_feedback_longcomment').val().trim();
                	if(DEBUG_ENABLED) console.log("Feedback: comment = " + comment);
                	var commentLen = comment.length;
                	//if(commentLen > 500) {  // Max 500 chars
                	//	comment = comment.substring(0, 500);
                    //	if(DEBUG_ENABLED) console.log("Comment has been truncated to: " + comment);
                	//}
                	
                	if(commentLen == 0) {  // Or, invalid
                		// TBD:
                		// Error...
                		window.alert("Please include any comment.");
                		return; // ???
                	}
                	
                	var feedbackObj = {};
                	feedbackObj.targetService = $('#feedbackhelper_feedback_targetservice').text();
                	feedbackObj.targetPage = $('#feedbackhelper_feedback_targetpage').val();
                	feedbackObj.targetItem = $('#feedbackhelper_feedback_targetitem').val();
                	//feedbackObj.targetSite = window.location.href;
                	feedbackObj.originDomain = window.location.hostname;
                	feedbackObj.originUrl = window.location.href;
                	feedbackObj.longComment = comment;
                	feedbackObj.commentTime = (new Date()).getTime();
                	
                    var referrerInfo = {};
                    referrerInfo.referer = document.referrer;
                    referrerInfo.userAgent = navigator.userAgent;
                    referrerInfo.language = navigator.language;
                    //referrerInfo.hostname = "";
                    //referrerInfo.ipAddress = "";
                    feedbackObj.referrerInfo = referrerInfo;
                	
                	// temporary
                	//var feedtrailUrl = 'http://localhost:8899/wp/feedback';
                	var feedtrailUrl = 'http://beta.feedtrail.com/wp/feedback';
                	var payload = JSON.stringify(feedbackObj);
                	var payloadParam = '?payload=' + payload
                	feedtrailUrl += payloadParam;
                	$.ajax({
                  	    type: 'GET',
                  	    url: feedtrailUrl,
                  	    dataType: "jsonp",
                  	    success: function(data, textStatus) {
                  	      // TBD
                  	      if(DEBUG_ENABLED) console.log("Successfully signed up. textStatus = " + textStatus);
                  	      if(DEBUG_ENABLED) console.log("data = " + data);
                  	      if(DEBUG_ENABLED) console.log(data);

            	          //updateStatus('Thanks for signing up.', 'info', 5550);

                  	      // TBD:
                  		  window.alert("Thanks for sending us your feedback.");
                  	    },
                  	    error: function(req, textStatus) {
                   	      if(DEBUG_ENABLED) console.log("Failed to send feedback. textStatus = " + textStatus);

              	          //updateStatus('Failed to sign up: status = ' + textStatus, 'error', 5550);

                  	      // TBD:
                  		  window.alert("Feedback submission failed. Please try again.");
                  	    }
              	   	  });

                      // Close the dialog....
                      $(this).dialog('close');
                },
                Cancel: function() {
                    $(this).dialog('close'); 
                }
            }
        });    	
    };
  };

  return cls;
})();

