<%@ page import="com.queryclient.util.*, com.queryclient.helper.*" %>
<div id="app_page_footer" class="page_footer_menu">
    <hr id="page_footer_hr" align="left">
    <div id="page_bottom">
    <span id="app_info"><a id="app_info_anchor" href="#" title="App Info">Info</a></span>
    | <span id="app_help"><a id="app_help_anchor" href="#" title="Quick Help">Help</a></span>
    | <span id="app_about"><a id="app_about_anchor" href="#" title="About Query Client">About</a></span>
    | <span id="app_powered"><a id="app_powered_anchor" href="#" title="Powered by Google App Engine">GAE</a></span>
    | <span id="app_faqs"><a id="app_faqs_anchor" href="#" title="Frequently Asked Questions">FAQs</a></span>
    </div>
</div>

	<div id="app_info_dialog" title="Application Info">
		<p>
		QueryClient is a monitoring tool for web applications deployed to Google App Engine platform.
		</p>
	</div>
	<div id="app_help_dialog" title="Quick Help">
	    <h3>Getting Started</h3>
		<p>
		Coming soon...
		</p>
	</div>
	<div id="app_about_dialog" title="About Query Client">
		<p>
		QueryClient is currently in a "beta" release.
		</p>
	</div>
	<div id="app_powered_dialog" title="Powered by Google App Engine">
		<p>
		Query Client is built and being run on <a href="http://code.google.com/appengine">Google App Engine.</a>
		</p>
	</div>
	<div id="app_faqs_dialog" title="Frequently Asked Questions">
		<p>
		Coming soon...
		</p>
	</div>

<script>
    // "Init"
    $(function() {
    	$("#app_info_dialog").hide();
    	$("#app_help_dialog").hide();
    	$("#app_about_dialog").hide();
    	$("#app_powered_dialog").hide();
    	$("#app_faqs_dialog").hide();
    });

    // Event handlers
    $(function() {
	    $("#app_info_anchor").click(function() {
		    if(DEBUG_ENABLED) console.log("Info anchor clicked.");
		    
			$( "#app_info_dialog" ).dialog({
				height: 150,
				width: 300,
				modal: true
			});
		    
		    return false;
	    });
	    $("#app_help_anchor").click(function() {
		    if(DEBUG_ENABLED) console.log("Help anchor clicked.");
		    
			$( "#app_help_dialog" ).dialog({
				height: 150,
				width: 300,
				modal: true
			});
		    
		    return false;
	    });
	    $("#app_about_anchor").click(function() {
		    if(DEBUG_ENABLED) console.log("About anchor clicked.");
		    
			$( "#app_about_dialog" ).dialog({
				height: 150,
				width: 300,
				modal: true
			});
		    
		    return false;
	    });
	    $("#app_powered_anchor").click(function() {
		    if(DEBUG_ENABLED) console.log("Powered anchor clicked.");
		    
			$( "#app_powered_dialog" ).dialog({
				height: 150,
				width: 300,
				modal: true
			});
		    
		    return false;
	    });
	    $("#app_faqs_anchor").click(function() {
		    if(DEBUG_ENABLED) console.log("FAQs anchor clicked.");
		    
			$( "#app_faqs_dialog" ).dialog({
				height: 150,
				width: 300,
				modal: true
			});
		    
		    return false;
	    });
    });
</script>
