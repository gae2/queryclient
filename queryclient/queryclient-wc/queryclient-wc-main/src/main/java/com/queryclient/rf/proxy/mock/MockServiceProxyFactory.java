package com.queryclient.rf.proxy.mock;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.rf.proxy.ServiceProxyFactory;
import com.queryclient.rf.proxy.UserServiceProxy;
import com.queryclient.rf.proxy.UserPasswordServiceProxy;
import com.queryclient.rf.proxy.ExternalUserAuthServiceProxy;
import com.queryclient.rf.proxy.UserAuthStateServiceProxy;
import com.queryclient.rf.proxy.DataServiceServiceProxy;
import com.queryclient.rf.proxy.ServiceEndpointServiceProxy;
import com.queryclient.rf.proxy.QuerySessionServiceProxy;
import com.queryclient.rf.proxy.QueryRecordServiceProxy;
import com.queryclient.rf.proxy.DummyEntityServiceProxy;
import com.queryclient.rf.proxy.ServiceInfoServiceProxy;
import com.queryclient.rf.proxy.FiveTenServiceProxy;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Create your own mock object factory using MockServiceProxyFactory as a template.
public class MockServiceProxyFactory extends ServiceProxyFactory
{
    private static final Logger log = Logger.getLogger(MockServiceProxyFactory.class.getName());

    private UserServiceProxy userService = null;
    private UserPasswordServiceProxy userPasswordService = null;
    private ExternalUserAuthServiceProxy externalUserAuthService = null;
    private UserAuthStateServiceProxy userAuthStateService = null;
    private DataServiceServiceProxy dataServiceService = null;
    private ServiceEndpointServiceProxy serviceEndpointService = null;
    private QuerySessionServiceProxy querySessionService = null;
    private QueryRecordServiceProxy queryRecordService = null;
    private DummyEntityServiceProxy dummyEntityService = null;
    private ServiceInfoServiceProxy serviceInfoService = null;
    private FiveTenServiceProxy fiveTenService = null;

    // Using the Decorator pattern.
    private ServiceProxyFactory decoratedProxyFactory;
    private MockServiceProxyFactory()
    {
        this(null);   // ????
    }
    private MockServiceProxyFactory(ServiceProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }

    // Initialization-on-demand holder.
    private static class MockServiceProxyFactoryHolder
    {
        private static final MockServiceProxyFactory INSTANCE = new MockServiceProxyFactory();
    }

    // Singleton method
    public static MockServiceProxyFactory getInstance()
    {
        return MockServiceProxyFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public ServiceProxyFactory getDecoratedProxyFactory()
    {
        return decoratedProxyFactory;
    }
    public void setDecoratedProxyFactory(ServiceProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }


    public UserServiceProxy getUserServiceProxy()
    {
        if(userService == null) {
            userService = new MockUserServiceProxy(decoratedProxyFactory.getUserServiceProxy()) {};
        }
        return userService;
    }

    public UserPasswordServiceProxy getUserPasswordServiceProxy()
    {
        if(userPasswordService == null) {
            userPasswordService = new MockUserPasswordServiceProxy(decoratedProxyFactory.getUserPasswordServiceProxy()) {};
        }
        return userPasswordService;
    }

    public ExternalUserAuthServiceProxy getExternalUserAuthServiceProxy()
    {
        if(externalUserAuthService == null) {
            externalUserAuthService = new MockExternalUserAuthServiceProxy(decoratedProxyFactory.getExternalUserAuthServiceProxy()) {};
        }
        return externalUserAuthService;
    }

    public UserAuthStateServiceProxy getUserAuthStateServiceProxy()
    {
        if(userAuthStateService == null) {
            userAuthStateService = new MockUserAuthStateServiceProxy(decoratedProxyFactory.getUserAuthStateServiceProxy()) {};
        }
        return userAuthStateService;
    }

    public DataServiceServiceProxy getDataServiceServiceProxy()
    {
        if(dataServiceService == null) {
            dataServiceService = new MockDataServiceServiceProxy(decoratedProxyFactory.getDataServiceServiceProxy()) {};
        }
        return dataServiceService;
    }

    public ServiceEndpointServiceProxy getServiceEndpointServiceProxy()
    {
        if(serviceEndpointService == null) {
            serviceEndpointService = new MockServiceEndpointServiceProxy(decoratedProxyFactory.getServiceEndpointServiceProxy()) {};
        }
        return serviceEndpointService;
    }

    public QuerySessionServiceProxy getQuerySessionServiceProxy()
    {
        if(querySessionService == null) {
            querySessionService = new MockQuerySessionServiceProxy(decoratedProxyFactory.getQuerySessionServiceProxy()) {};
        }
        return querySessionService;
    }

    public QueryRecordServiceProxy getQueryRecordServiceProxy()
    {
        if(queryRecordService == null) {
            queryRecordService = new MockQueryRecordServiceProxy(decoratedProxyFactory.getQueryRecordServiceProxy()) {};
        }
        return queryRecordService;
    }

    public DummyEntityServiceProxy getDummyEntityServiceProxy()
    {
        if(dummyEntityService == null) {
            dummyEntityService = new MockDummyEntityServiceProxy(decoratedProxyFactory.getDummyEntityServiceProxy()) {};
        }
        return dummyEntityService;
    }

    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new MockServiceInfoServiceProxy(decoratedProxyFactory.getServiceInfoServiceProxy()) {};
        }
        return serviceInfoService;
    }

    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenService == null) {
            fiveTenService = new MockFiveTenServiceProxy(decoratedProxyFactory.getFiveTenServiceProxy()) {};
        }
        return fiveTenService;
    }

}
