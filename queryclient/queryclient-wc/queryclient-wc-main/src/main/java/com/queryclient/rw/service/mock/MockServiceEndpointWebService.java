package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ConsumerKeySecretPairJsBean;
import com.queryclient.fe.bean.ServiceEndpointJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.ServiceEndpointWebService;


// MockServiceEndpointWebService is a mock object.
// It can be used as a base class to mock ServiceEndpointWebService objects.
public abstract class MockServiceEndpointWebService extends ServiceEndpointWebService  // implements ServiceEndpointService
{
    private static final Logger log = Logger.getLogger(MockServiceEndpointWebService.class.getName());
     
    // Af service interface.
    private ServiceEndpointWebService mService = null;

    public MockServiceEndpointWebService()
    {
        this(MockServiceProxyFactory.getInstance().getServiceEndpointServiceProxy());
    }
    public MockServiceEndpointWebService(ServiceEndpointService service)
    {
        super(service);
    }


}
