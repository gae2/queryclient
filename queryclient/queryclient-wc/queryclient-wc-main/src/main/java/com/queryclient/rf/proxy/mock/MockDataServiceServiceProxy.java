package com.queryclient.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairListStub;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.ws.stub.ReferrerInfoStructListStub;
import com.queryclient.ws.stub.DataServiceStub;
import com.queryclient.ws.stub.DataServiceListStub;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.rf.config.Config;
import com.queryclient.rf.proxy.DataServiceServiceProxy;


// MockDataServiceServiceProxy is a decorator.
// It can be used as a base class to mock DataServiceService objects.
public abstract class MockDataServiceServiceProxy extends DataServiceServiceProxy implements DataServiceService
{
    private static final Logger log = Logger.getLogger(MockDataServiceServiceProxy.class.getName());

    // MockDataServiceServiceProxy uses the decorator design pattern.
    private DataServiceService decoratedProxy;

    public MockDataServiceServiceProxy(DataServiceService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected DataServiceService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(DataServiceService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public DataService getDataService(String guid) throws BaseException
    {
        return decoratedProxy.getDataService(guid);
    }

    @Override
    public Object getDataService(String guid, String field) throws BaseException
    {
        return decoratedProxy.getDataService(guid, field);
    }

    @Override
    public List<DataService> getDataServices(List<String> guids) throws BaseException
    {
        return decoratedProxy.getDataServices(guids);
    }

    @Override
    public List<DataService> getAllDataServices() throws BaseException
    {
        return getAllDataServices(null, null, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServices(ordering, offset, count, null);
    }

    @Override
    public List<DataService> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDataServices(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDataServiceKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DataService> findDataServices(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDataServices(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDataServiceKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDataService(String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        return decoratedProxy.createDataService(user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status);
    }

    @Override
    public String createDataService(DataService bean) throws BaseException
    {
        return decoratedProxy.createDataService(bean);
    }

    @Override
    public DataService constructDataService(DataService bean) throws BaseException
    {
        return decoratedProxy.constructDataService(bean);
    }

    @Override
    public Boolean updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, ReferrerInfoStruct referrerInfo, String status) throws BaseException
    {
        return decoratedProxy.updateDataService(guid, user, name, description, type, mode, serviceUrl, authRequired, authCredential, referrerInfo, status);
    }

    @Override
    public Boolean updateDataService(DataService bean) throws BaseException
    {
        return decoratedProxy.updateDataService(bean);
    }

    @Override
    public DataService refreshDataService(DataService bean) throws BaseException
    {
        return decoratedProxy.refreshDataService(bean);
    }

    @Override
    public Boolean deleteDataService(String guid) throws BaseException
    {
        return decoratedProxy.deleteDataService(guid);
    }

    @Override
    public Boolean deleteDataService(DataService bean) throws BaseException
    {
        return decoratedProxy.deleteDataService(bean);
    }

    @Override
    public Long deleteDataServices(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteDataServices(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDataServices(List<DataService> dataServices) throws BaseException
    {
        return decoratedProxy.createDataServices(dataServices);
    }

    // TBD
    //@Override
    //public Boolean updateDataServices(List<DataService> dataServices) throws BaseException
    //{
    //}

}
