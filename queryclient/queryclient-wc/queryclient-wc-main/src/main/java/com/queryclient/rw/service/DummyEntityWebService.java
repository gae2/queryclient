package com.queryclient.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.DummyEntity;
import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.DummyEntityJsBean;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DummyEntityWebService // implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(DummyEntityWebService.class.getName());
     
    // Af service interface.
    private DummyEntityService mService = null;

    public DummyEntityWebService()
    {
        this(ServiceProxyFactory.getInstance().getDummyEntityServiceProxy());
    }
    public DummyEntityWebService(DummyEntityService service)
    {
        mService = service;
    }
    
    protected DummyEntityService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getDummyEntityServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(DummyEntityService service)
    {
        mService = service;
    }
    
    
    public DummyEntityJsBean getDummyEntity(String guid) throws WebException
    {
        try {
            DummyEntity dummyEntity = getServiceProxy().getDummyEntity(guid);
            DummyEntityJsBean bean = convertDummyEntityToJsBean(dummyEntity);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getDummyEntity(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getDummyEntity(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<DummyEntityJsBean> getDummyEntities(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<DummyEntityJsBean> jsBeans = new ArrayList<DummyEntityJsBean>();
            List<DummyEntity> dummyEntities = getServiceProxy().getDummyEntities(guids);
            if(dummyEntities != null) {
                for(DummyEntity dummyEntity : dummyEntities) {
                    jsBeans.add(convertDummyEntityToJsBean(dummyEntity));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<DummyEntityJsBean> getAllDummyEntities() throws WebException
    {
        return getAllDummyEntities(null, null, null);
    }

    // @Deprecated
    public List<DummyEntityJsBean> getAllDummyEntities(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllDummyEntities(ordering, offset, count, null);
    }

    public List<DummyEntityJsBean> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<DummyEntityJsBean> jsBeans = new ArrayList<DummyEntityJsBean>();
            List<DummyEntity> dummyEntities = getServiceProxy().getAllDummyEntities(ordering, offset, count, forwardCursor);
            if(dummyEntities != null) {
                for(DummyEntity dummyEntity : dummyEntities) {
                    jsBeans.add(convertDummyEntityToJsBean(dummyEntity));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<DummyEntityJsBean> findDummyEntities(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<DummyEntityJsBean> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<DummyEntityJsBean> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<DummyEntityJsBean> jsBeans = new ArrayList<DummyEntityJsBean>();
            List<DummyEntity> dummyEntities = getServiceProxy().findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(dummyEntities != null) {
                for(DummyEntity dummyEntity : dummyEntities) {
                    jsBeans.add(convertDummyEntityToJsBean(dummyEntity));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws WebException
    {
        try {
            return getServiceProxy().createDummyEntity(user, name, content, maxLength, expired, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createDummyEntity(DummyEntityJsBean jsBean) throws WebException
    {
        try {
            DummyEntity dummyEntity = convertDummyEntityJsBeanToBean(jsBean);
            return getServiceProxy().createDummyEntity(dummyEntity);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public DummyEntityJsBean constructDummyEntity(DummyEntityJsBean jsBean) throws WebException
    {
        try {
            DummyEntity dummyEntity = convertDummyEntityJsBeanToBean(jsBean);
            dummyEntity = getServiceProxy().constructDummyEntity(dummyEntity);
            jsBean = convertDummyEntityToJsBean(dummyEntity);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws WebException
    {
        try {
            return getServiceProxy().updateDummyEntity(guid, user, name, content, maxLength, expired, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateDummyEntity(DummyEntityJsBean jsBean) throws WebException
    {
        try {
            DummyEntity dummyEntity = convertDummyEntityJsBeanToBean(jsBean);
            return getServiceProxy().updateDummyEntity(dummyEntity);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public DummyEntityJsBean refreshDummyEntity(DummyEntityJsBean jsBean) throws WebException
    {
        try {
            DummyEntity dummyEntity = convertDummyEntityJsBeanToBean(jsBean);
            dummyEntity = getServiceProxy().refreshDummyEntity(dummyEntity);
            jsBean = convertDummyEntityToJsBean(dummyEntity);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteDummyEntity(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteDummyEntity(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteDummyEntity(DummyEntityJsBean jsBean) throws WebException
    {
        try {
            DummyEntity dummyEntity = convertDummyEntityJsBeanToBean(jsBean);
            return getServiceProxy().deleteDummyEntity(dummyEntity);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteDummyEntities(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteDummyEntities(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static DummyEntityJsBean convertDummyEntityToJsBean(DummyEntity dummyEntity)
    {
        DummyEntityJsBean jsBean = null;
        if(dummyEntity != null) {
            jsBean = new DummyEntityJsBean();
            jsBean.setGuid(dummyEntity.getGuid());
            jsBean.setUser(dummyEntity.getUser());
            jsBean.setName(dummyEntity.getName());
            jsBean.setContent(dummyEntity.getContent());
            jsBean.setMaxLength(dummyEntity.getMaxLength());
            jsBean.setExpired(dummyEntity.isExpired());
            jsBean.setStatus(dummyEntity.getStatus());
            jsBean.setCreatedTime(dummyEntity.getCreatedTime());
            jsBean.setModifiedTime(dummyEntity.getModifiedTime());
        }
        return jsBean;
    }

    public static DummyEntity convertDummyEntityJsBeanToBean(DummyEntityJsBean jsBean)
    {
        DummyEntityBean dummyEntity = null;
        if(jsBean != null) {
            dummyEntity = new DummyEntityBean();
            dummyEntity.setGuid(jsBean.getGuid());
            dummyEntity.setUser(jsBean.getUser());
            dummyEntity.setName(jsBean.getName());
            dummyEntity.setContent(jsBean.getContent());
            dummyEntity.setMaxLength(jsBean.getMaxLength());
            dummyEntity.setExpired(jsBean.isExpired());
            dummyEntity.setStatus(jsBean.getStatus());
            dummyEntity.setCreatedTime(jsBean.getCreatedTime());
            dummyEntity.setModifiedTime(jsBean.getModifiedTime());
        }
        return dummyEntity;
    }

}
