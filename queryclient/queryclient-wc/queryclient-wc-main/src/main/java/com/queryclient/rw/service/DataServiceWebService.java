package com.queryclient.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ConsumerKeySecretPairJsBean;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.fe.bean.DataServiceJsBean;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DataServiceWebService // implements DataServiceService
{
    private static final Logger log = Logger.getLogger(DataServiceWebService.class.getName());
     
    // Af service interface.
    private DataServiceService mService = null;

    public DataServiceWebService()
    {
        this(ServiceProxyFactory.getInstance().getDataServiceServiceProxy());
    }
    public DataServiceWebService(DataServiceService service)
    {
        mService = service;
    }
    
    protected DataServiceService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getDataServiceServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(DataServiceService service)
    {
        mService = service;
    }
    
    
    public DataServiceJsBean getDataService(String guid) throws WebException
    {
        try {
            DataService dataService = getServiceProxy().getDataService(guid);
            DataServiceJsBean bean = convertDataServiceToJsBean(dataService);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getDataService(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getDataService(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<DataServiceJsBean> getDataServices(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<DataServiceJsBean> jsBeans = new ArrayList<DataServiceJsBean>();
            List<DataService> dataServices = getServiceProxy().getDataServices(guids);
            if(dataServices != null) {
                for(DataService dataService : dataServices) {
                    jsBeans.add(convertDataServiceToJsBean(dataService));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<DataServiceJsBean> getAllDataServices() throws WebException
    {
        return getAllDataServices(null, null, null);
    }

    // @Deprecated
    public List<DataServiceJsBean> getAllDataServices(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllDataServices(ordering, offset, count, null);
    }

    public List<DataServiceJsBean> getAllDataServices(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<DataServiceJsBean> jsBeans = new ArrayList<DataServiceJsBean>();
            List<DataService> dataServices = getServiceProxy().getAllDataServices(ordering, offset, count, forwardCursor);
            if(dataServices != null) {
                for(DataService dataService : dataServices) {
                    jsBeans.add(convertDataServiceToJsBean(dataService));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllDataServiceKeys(ordering, offset, count, null);
    }

    public List<String> getAllDataServiceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllDataServiceKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<DataServiceJsBean> findDataServices(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findDataServices(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<DataServiceJsBean> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findDataServices(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<DataServiceJsBean> findDataServices(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<DataServiceJsBean> jsBeans = new ArrayList<DataServiceJsBean>();
            List<DataService> dataServices = getServiceProxy().findDataServices(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(dataServices != null) {
                for(DataService dataService : dataServices) {
                    jsBeans.add(convertDataServiceToJsBean(dataService));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findDataServiceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findDataServiceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createDataService(String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairJsBean authCredential, ReferrerInfoStructJsBean referrerInfo, String status) throws WebException
    {
        try {
            return getServiceProxy().createDataService(user, name, description, type, mode, serviceUrl, authRequired, ConsumerKeySecretPairWebService.convertConsumerKeySecretPairJsBeanToBean(authCredential), ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createDataService(DataServiceJsBean jsBean) throws WebException
    {
        try {
            DataService dataService = convertDataServiceJsBeanToBean(jsBean);
            return getServiceProxy().createDataService(dataService);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public DataServiceJsBean constructDataService(DataServiceJsBean jsBean) throws WebException
    {
        try {
            DataService dataService = convertDataServiceJsBeanToBean(jsBean);
            dataService = getServiceProxy().constructDataService(dataService);
            jsBean = convertDataServiceToJsBean(dataService);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateDataService(String guid, String user, String name, String description, String type, String mode, String serviceUrl, Boolean authRequired, ConsumerKeySecretPairJsBean authCredential, ReferrerInfoStructJsBean referrerInfo, String status) throws WebException
    {
        try {
            return getServiceProxy().updateDataService(guid, user, name, description, type, mode, serviceUrl, authRequired, ConsumerKeySecretPairWebService.convertConsumerKeySecretPairJsBeanToBean(authCredential), ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateDataService(DataServiceJsBean jsBean) throws WebException
    {
        try {
            DataService dataService = convertDataServiceJsBeanToBean(jsBean);
            return getServiceProxy().updateDataService(dataService);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public DataServiceJsBean refreshDataService(DataServiceJsBean jsBean) throws WebException
    {
        try {
            DataService dataService = convertDataServiceJsBeanToBean(jsBean);
            dataService = getServiceProxy().refreshDataService(dataService);
            jsBean = convertDataServiceToJsBean(dataService);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteDataService(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteDataService(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteDataService(DataServiceJsBean jsBean) throws WebException
    {
        try {
            DataService dataService = convertDataServiceJsBeanToBean(jsBean);
            return getServiceProxy().deleteDataService(dataService);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteDataServices(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteDataServices(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static DataServiceJsBean convertDataServiceToJsBean(DataService dataService)
    {
        DataServiceJsBean jsBean = null;
        if(dataService != null) {
            jsBean = new DataServiceJsBean();
            jsBean.setGuid(dataService.getGuid());
            jsBean.setUser(dataService.getUser());
            jsBean.setName(dataService.getName());
            jsBean.setDescription(dataService.getDescription());
            jsBean.setType(dataService.getType());
            jsBean.setMode(dataService.getMode());
            jsBean.setServiceUrl(dataService.getServiceUrl());
            jsBean.setAuthRequired(dataService.isAuthRequired());
            jsBean.setAuthCredential(ConsumerKeySecretPairWebService.convertConsumerKeySecretPairToJsBean(dataService.getAuthCredential()));
            jsBean.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructToJsBean(dataService.getReferrerInfo()));
            jsBean.setStatus(dataService.getStatus());
            jsBean.setCreatedTime(dataService.getCreatedTime());
            jsBean.setModifiedTime(dataService.getModifiedTime());
        }
        return jsBean;
    }

    public static DataService convertDataServiceJsBeanToBean(DataServiceJsBean jsBean)
    {
        DataServiceBean dataService = null;
        if(jsBean != null) {
            dataService = new DataServiceBean();
            dataService.setGuid(jsBean.getGuid());
            dataService.setUser(jsBean.getUser());
            dataService.setName(jsBean.getName());
            dataService.setDescription(jsBean.getDescription());
            dataService.setType(jsBean.getType());
            dataService.setMode(jsBean.getMode());
            dataService.setServiceUrl(jsBean.getServiceUrl());
            dataService.setAuthRequired(jsBean.isAuthRequired());
            dataService.setAuthCredential(ConsumerKeySecretPairWebService.convertConsumerKeySecretPairJsBeanToBean(jsBean.getAuthCredential()));
            dataService.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            dataService.setStatus(jsBean.getStatus());
            dataService.setCreatedTime(jsBean.getCreatedTime());
            dataService.setModifiedTime(jsBean.getModifiedTime());
        }
        return dataService;
    }

}
