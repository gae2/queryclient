package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.af.bean.UserPasswordBean;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.GaeAppStructJsBean;
import com.queryclient.fe.bean.UserPasswordJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.UserPasswordWebService;


// MockUserPasswordWebService is a mock object.
// It can be used as a base class to mock UserPasswordWebService objects.
public abstract class MockUserPasswordWebService extends UserPasswordWebService  // implements UserPasswordService
{
    private static final Logger log = Logger.getLogger(MockUserPasswordWebService.class.getName());
     
    // Af service interface.
    private UserPasswordWebService mService = null;

    public MockUserPasswordWebService()
    {
        this(MockServiceProxyFactory.getInstance().getUserPasswordServiceProxy());
    }
    public MockUserPasswordWebService(UserPasswordService service)
    {
        super(service);
    }


}
