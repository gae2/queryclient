package com.queryclient.rf.proxy;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.ws.stub.ReferrerInfoStructListStub;
import com.queryclient.ws.stub.QuerySessionStub;
import com.queryclient.ws.stub.QuerySessionListStub;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;

import com.queryclient.rf.config.Config;


public class QuerySessionServiceProxy extends AbstractBaseServiceProxy implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(QuerySessionServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_QUERYSESSION = "querySessions";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public QuerySessionServiceProxy()
    {
        initCache();
    }


    protected WebResource getQuerySessionWebResource()
    {
        return getWebResource(RESOURCE_QUERYSESSION);
    }
    protected WebResource getQuerySessionWebResource(String path)
    {
        return getWebResource(RESOURCE_QUERYSESSION, path);
    }
    protected WebResource getQuerySessionWebResourceByGuid(String guid)
    {
        return getQuerySessionWebResource(guid);
    }


    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        QuerySession bean = null;

        String key = getResourcePath(RESOURCE_QUERYSESSION, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getQuerySessionWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                QuerySessionStub stub = clientResponse.getEntity(QuerySessionStub.class);
                bean = MarshalHelper.convertQuerySessionToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "QuerySession bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        QuerySession bean = getQuerySession(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("dataService")) {
            return bean.getDataService();
        } else if(field.equals("serviceUrl")) {
            return bean.getServiceUrl();
        } else if(field.equals("inputFormat")) {
            return bean.getInputFormat();
        } else if(field.equals("outputFormat")) {
            return bean.getOutputFormat();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        throw new NotImplementedException("To be implemented.");
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return getAllQuerySessions(null, null, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<QuerySession> list = null;
    	
        WebResource webResource = getQuerySessionWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                QuerySessionListStub stub = clientResponse.getEntity(QuerySessionListStub.class);
                list = MarshalHelper.convertQuerySessionListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "QuerySession list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<String> list = null;
    	
        WebResource webResource = getQuerySessionWebResource(RESOURCE_PATH_ALLKEYS);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "QuerySession key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<QuerySession> list = null;
    	
//        ClientResponse clientResponse = getQuerySessionWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getQuerySessionWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                QuerySessionListStub stub = clientResponse.getEntity(QuerySessionListStub.class);
                list = MarshalHelper.convertQuerySessionListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "QuerySession list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<String> list = null;

    	WebResource webResource = getQuerySessionWebResource(RESOURCE_PATH_KEYS);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "QuerySession key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getQuerySessionWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "QuerySession count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }

    @Override
    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        QuerySessionBean bean = new QuerySessionBean(null, user, dataService, serviceUrl, inputFormat, outputFormat, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), status, note);
        return createQuerySession(bean);        
    }

    @Override
    public String createQuerySession(QuerySession bean) throws BaseException
    {
        String guid = null;
        QuerySessionStub stub = MarshalHelper.convertQuerySessionToStub(bean);
        WebResource webResource = getQuerySessionWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New QuerySession guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New QuerySession resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return guid;
    }

    @Override
    public QuerySession constructQuerySession(QuerySession bean) throws BaseException
    {
        QuerySessionStub stub = MarshalHelper.convertQuerySessionToStub(bean);
        WebResource webResource = getQuerySessionWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                stub = clientResponse.getEntity(QuerySessionStub.class);
                bean = MarshalHelper.convertQuerySessionToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New QuerySession bean = " + bean);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New QuerySession resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "QuerySession guid is invalid.");
        	throw new BaseException("QuerySession guid is invalid.");
        }
        QuerySessionBean bean = new QuerySessionBean(guid, user, dataService, serviceUrl, inputFormat, outputFormat, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), status, note);
        return updateQuerySession(bean);        
    }

    @Override
    public Boolean updateQuerySession(QuerySession bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "QuerySession object is invalid.");
        	throw new BaseException("QuerySession object is invalid.");
        }
        QuerySessionStub stub = MarshalHelper.convertQuerySessionToStub(bean);

        WebResource webResource = getQuerySessionWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        //ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).put(ClientResponse.class, stub);
        ClientResponse clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully updated the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public QuerySession refreshQuerySession(QuerySession bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "QuerySession object is invalid.");
        	throw new BaseException("QuerySession object is invalid.");
        }
        QuerySessionStub stub = MarshalHelper.convertQuerySessionToStub(bean);

        WebResource webResource = getQuerySessionWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                stub = clientResponse.getEntity(QuerySessionStub.class);
                bean = MarshalHelper.convertQuerySessionToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully refreshed the QuerySession bean = " + bean);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "QuerySession guid is invalid.");
        	throw new BaseException("QuerySession guid is invalid.");
        }

        WebResource webResource = getQuerySessionWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully deleted the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public Boolean deleteQuerySession(QuerySession bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deleteQuerySession(guid);
    }

    @Override
    public Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getQuerySessionWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exception if the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        // Order of the mime types ???
        ClientResponse clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Deleted QuerySessions: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_QUERYSESSION);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createQuerySessions(List<QuerySession> querySessions) throws BaseException
    {
        log.finer("BEGIN");

        if(querySessions == null) {
            log.log(Level.WARNING, "createQuerySessions() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = querySessions.size();
        if(size == 0) {
            log.log(Level.WARNING, "createQuerySessions() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(QuerySession querySession : querySessions) {
            String guid = createQuerySession(querySession);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createQuerySessions() failed for at least one querySession. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateQuerySessions(List<QuerySession> querySessions) throws BaseException
    //{
    //}

}
