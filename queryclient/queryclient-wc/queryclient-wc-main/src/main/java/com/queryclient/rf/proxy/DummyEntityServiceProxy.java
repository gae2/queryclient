package com.queryclient.rf.proxy;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.DummyEntityStub;
import com.queryclient.ws.stub.DummyEntityListStub;
import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;

import com.queryclient.rf.config.Config;


public class DummyEntityServiceProxy extends AbstractBaseServiceProxy implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(DummyEntityServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_DUMMYENTITY = "dummyEntities";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public DummyEntityServiceProxy()
    {
        initCache();
    }


    protected WebResource getDummyEntityWebResource()
    {
        return getWebResource(RESOURCE_DUMMYENTITY);
    }
    protected WebResource getDummyEntityWebResource(String path)
    {
        return getWebResource(RESOURCE_DUMMYENTITY, path);
    }
    protected WebResource getDummyEntityWebResourceByGuid(String guid)
    {
        return getDummyEntityWebResource(guid);
    }


    @Override
    public DummyEntity getDummyEntity(String guid) throws BaseException
    {
        DummyEntity bean = null;

        String key = getResourcePath(RESOURCE_DUMMYENTITY, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getDummyEntityWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                DummyEntityStub stub = clientResponse.getEntity(DummyEntityStub.class);
                bean = MarshalHelper.convertDummyEntityToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "DummyEntity bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Object getDummyEntity(String guid, String field) throws BaseException
    {
        DummyEntity bean = getDummyEntity(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("name")) {
            return bean.getName();
        } else if(field.equals("content")) {
            return bean.getContent();
        } else if(field.equals("maxLength")) {
            return bean.getMaxLength();
        } else if(field.equals("expired")) {
            return bean.isExpired();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException
    {
        throw new NotImplementedException("To be implemented.");
    }

    @Override
    public List<DummyEntity> getAllDummyEntities() throws BaseException
    {
        return getAllDummyEntities(null, null, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<DummyEntity> list = null;
    	
        WebResource webResource = getDummyEntityWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                DummyEntityListStub stub = clientResponse.getEntity(DummyEntityListStub.class);
                list = MarshalHelper.convertDummyEntityListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "DummyEntity list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<String> list = null;
    	
        WebResource webResource = getDummyEntityWebResource(RESOURCE_PATH_ALLKEYS);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "DummyEntity key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<DummyEntity> list = null;
    	
//        ClientResponse clientResponse = getDummyEntityWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getDummyEntityWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                DummyEntityListStub stub = clientResponse.getEntity(DummyEntityListStub.class);
                list = MarshalHelper.convertDummyEntityListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "DummyEntity list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<String> list = null;

    	WebResource webResource = getDummyEntityWebResource(RESOURCE_PATH_KEYS);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "DummyEntity key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getDummyEntityWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "DummyEntity count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }

    @Override
    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        DummyEntityBean bean = new DummyEntityBean(null, user, name, content, maxLength, expired, status);
        return createDummyEntity(bean);        
    }

    @Override
    public String createDummyEntity(DummyEntity bean) throws BaseException
    {
        String guid = null;
        DummyEntityStub stub = MarshalHelper.convertDummyEntityToStub(bean);
        WebResource webResource = getDummyEntityWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New DummyEntity guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New DummyEntity resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return guid;
    }

    @Override
    public DummyEntity constructDummyEntity(DummyEntity bean) throws BaseException
    {
        DummyEntityStub stub = MarshalHelper.convertDummyEntityToStub(bean);
        WebResource webResource = getDummyEntityWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                stub = clientResponse.getEntity(DummyEntityStub.class);
                bean = MarshalHelper.convertDummyEntityToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New DummyEntity bean = " + bean);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New DummyEntity resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "DummyEntity guid is invalid.");
        	throw new BaseException("DummyEntity guid is invalid.");
        }
        DummyEntityBean bean = new DummyEntityBean(guid, user, name, content, maxLength, expired, status);
        return updateDummyEntity(bean);        
    }

    @Override
    public Boolean updateDummyEntity(DummyEntity bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "DummyEntity object is invalid.");
        	throw new BaseException("DummyEntity object is invalid.");
        }
        DummyEntityStub stub = MarshalHelper.convertDummyEntityToStub(bean);

        WebResource webResource = getDummyEntityWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        //ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).put(ClientResponse.class, stub);
        ClientResponse clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully updated the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public DummyEntity refreshDummyEntity(DummyEntity bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "DummyEntity object is invalid.");
        	throw new BaseException("DummyEntity object is invalid.");
        }
        DummyEntityStub stub = MarshalHelper.convertDummyEntityToStub(bean);

        WebResource webResource = getDummyEntityWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                stub = clientResponse.getEntity(DummyEntityStub.class);
                bean = MarshalHelper.convertDummyEntityToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully refreshed the DummyEntity bean = " + bean);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "DummyEntity guid is invalid.");
        	throw new BaseException("DummyEntity guid is invalid.");
        }

        WebResource webResource = getDummyEntityWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully deleted the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public Boolean deleteDummyEntity(DummyEntity bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deleteDummyEntity(guid);
    }

    @Override
    public Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getDummyEntityWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exception if the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        // Order of the mime types ???
        ClientResponse clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Deleted DummyEntities: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_DUMMYENTITY);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    {
        log.finer("BEGIN");

        if(dummyEntities == null) {
            log.log(Level.WARNING, "createDummyEntities() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = dummyEntities.size();
        if(size == 0) {
            log.log(Level.WARNING, "createDummyEntities() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(DummyEntity dummyEntity : dummyEntities) {
            String guid = createDummyEntity(dummyEntity);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createDummyEntities() failed for at least one dummyEntity. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    //{
    //}

}
