package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.DataService;
import com.queryclient.af.bean.DataServiceBean;
import com.queryclient.af.service.DataServiceService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ConsumerKeySecretPairJsBean;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.fe.bean.DataServiceJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.DataServiceWebService;


// MockDataServiceWebService is a mock object.
// It can be used as a base class to mock DataServiceWebService objects.
public abstract class MockDataServiceWebService extends DataServiceWebService  // implements DataServiceService
{
    private static final Logger log = Logger.getLogger(MockDataServiceWebService.class.getName());
     
    // Af service interface.
    private DataServiceWebService mService = null;

    public MockDataServiceWebService()
    {
        this(MockServiceProxyFactory.getInstance().getDataServiceServiceProxy());
    }
    public MockDataServiceWebService(DataServiceService service)
    {
        super(service);
    }


}
