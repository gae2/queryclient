package com.queryclient.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.FiveTen;
import com.queryclient.af.bean.FiveTenBean;
import com.queryclient.af.service.FiveTenService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.FiveTenJsBean;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FiveTenWebService // implements FiveTenService
{
    private static final Logger log = Logger.getLogger(FiveTenWebService.class.getName());
     
    // Af service interface.
    private FiveTenService mService = null;

    public FiveTenWebService()
    {
        this(ServiceProxyFactory.getInstance().getFiveTenServiceProxy());
    }
    public FiveTenWebService(FiveTenService service)
    {
        mService = service;
    }
    
    protected FiveTenService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getFiveTenServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(FiveTenService service)
    {
        mService = service;
    }
    
    
    public FiveTenJsBean getFiveTen(String guid) throws WebException
    {
        try {
            FiveTen fiveTen = getServiceProxy().getFiveTen(guid);
            FiveTenJsBean bean = convertFiveTenToJsBean(fiveTen);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getFiveTen(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getFiveTen(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FiveTenJsBean> getFiveTens(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<FiveTenJsBean> jsBeans = new ArrayList<FiveTenJsBean>();
            List<FiveTen> fiveTens = getServiceProxy().getFiveTens(guids);
            if(fiveTens != null) {
                for(FiveTen fiveTen : fiveTens) {
                    jsBeans.add(convertFiveTenToJsBean(fiveTen));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FiveTenJsBean> getAllFiveTens() throws WebException
    {
        return getAllFiveTens(null, null, null);
    }

    // @Deprecated
    public List<FiveTenJsBean> getAllFiveTens(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllFiveTens(ordering, offset, count, null);
    }

    public List<FiveTenJsBean> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<FiveTenJsBean> jsBeans = new ArrayList<FiveTenJsBean>();
            List<FiveTen> fiveTens = getServiceProxy().getAllFiveTens(ordering, offset, count, forwardCursor);
            if(fiveTens != null) {
                for(FiveTen fiveTen : fiveTens) {
                    jsBeans.add(convertFiveTenToJsBean(fiveTen));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllFiveTenKeys(ordering, offset, count, null);
    }

    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllFiveTenKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<FiveTenJsBean> findFiveTens(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<FiveTenJsBean> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<FiveTenJsBean> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<FiveTenJsBean> jsBeans = new ArrayList<FiveTenJsBean>();
            List<FiveTen> fiveTens = getServiceProxy().findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(fiveTens != null) {
                for(FiveTen fiveTen : fiveTens) {
                    jsBeans.add(convertFiveTenToJsBean(fiveTen));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFiveTen(Integer counter, String requesterIpAddress) throws WebException
    {
        try {
            return getServiceProxy().createFiveTen(counter, requesterIpAddress);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFiveTen(FiveTenJsBean jsBean) throws WebException
    {
        try {
            FiveTen fiveTen = convertFiveTenJsBeanToBean(jsBean);
            return getServiceProxy().createFiveTen(fiveTen);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FiveTenJsBean constructFiveTen(FiveTenJsBean jsBean) throws WebException
    {
        try {
            FiveTen fiveTen = convertFiveTenJsBeanToBean(jsBean);
            fiveTen = getServiceProxy().constructFiveTen(fiveTen);
            jsBean = convertFiveTenToJsBean(fiveTen);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws WebException
    {
        try {
            return getServiceProxy().updateFiveTen(guid, counter, requesterIpAddress);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateFiveTen(FiveTenJsBean jsBean) throws WebException
    {
        try {
            FiveTen fiveTen = convertFiveTenJsBeanToBean(jsBean);
            return getServiceProxy().updateFiveTen(fiveTen);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FiveTenJsBean refreshFiveTen(FiveTenJsBean jsBean) throws WebException
    {
        try {
            FiveTen fiveTen = convertFiveTenJsBeanToBean(jsBean);
            fiveTen = getServiceProxy().refreshFiveTen(fiveTen);
            jsBean = convertFiveTenToJsBean(fiveTen);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFiveTen(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteFiveTen(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFiveTen(FiveTenJsBean jsBean) throws WebException
    {
        try {
            FiveTen fiveTen = convertFiveTenJsBeanToBean(jsBean);
            return getServiceProxy().deleteFiveTen(fiveTen);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteFiveTens(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteFiveTens(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static FiveTenJsBean convertFiveTenToJsBean(FiveTen fiveTen)
    {
        FiveTenJsBean jsBean = null;
        if(fiveTen != null) {
            jsBean = new FiveTenJsBean();
            jsBean.setGuid(fiveTen.getGuid());
            jsBean.setCounter(fiveTen.getCounter());
            jsBean.setRequesterIpAddress(fiveTen.getRequesterIpAddress());
            jsBean.setCreatedTime(fiveTen.getCreatedTime());
            jsBean.setModifiedTime(fiveTen.getModifiedTime());
        }
        return jsBean;
    }

    public static FiveTen convertFiveTenJsBeanToBean(FiveTenJsBean jsBean)
    {
        FiveTenBean fiveTen = null;
        if(jsBean != null) {
            fiveTen = new FiveTenBean();
            fiveTen.setGuid(jsBean.getGuid());
            fiveTen.setCounter(jsBean.getCounter());
            fiveTen.setRequesterIpAddress(jsBean.getRequesterIpAddress());
            fiveTen.setCreatedTime(jsBean.getCreatedTime());
            fiveTen.setModifiedTime(jsBean.getModifiedTime());
        }
        return fiveTen;
    }

}
