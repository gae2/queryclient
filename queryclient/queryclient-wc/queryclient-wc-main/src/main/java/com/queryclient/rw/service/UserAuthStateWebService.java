package com.queryclient.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;
import com.queryclient.af.bean.UserAuthStateBean;
import com.queryclient.af.service.UserAuthStateService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.GaeAppStructJsBean;
import com.queryclient.fe.bean.ExternalUserIdStructJsBean;
import com.queryclient.fe.bean.UserAuthStateJsBean;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserAuthStateWebService // implements UserAuthStateService
{
    private static final Logger log = Logger.getLogger(UserAuthStateWebService.class.getName());
     
    // Af service interface.
    private UserAuthStateService mService = null;

    public UserAuthStateWebService()
    {
        this(ServiceProxyFactory.getInstance().getUserAuthStateServiceProxy());
    }
    public UserAuthStateWebService(UserAuthStateService service)
    {
        mService = service;
    }
    
    protected UserAuthStateService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getUserAuthStateServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(UserAuthStateService service)
    {
        mService = service;
    }
    
    
    public UserAuthStateJsBean getUserAuthState(String guid) throws WebException
    {
        try {
            UserAuthState userAuthState = getServiceProxy().getUserAuthState(guid);
            UserAuthStateJsBean bean = convertUserAuthStateToJsBean(userAuthState);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUserAuthState(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getUserAuthState(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserAuthStateJsBean> getUserAuthStates(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserAuthStateJsBean> jsBeans = new ArrayList<UserAuthStateJsBean>();
            List<UserAuthState> userAuthStates = getServiceProxy().getUserAuthStates(guids);
            if(userAuthStates != null) {
                for(UserAuthState userAuthState : userAuthStates) {
                    jsBeans.add(convertUserAuthStateToJsBean(userAuthState));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserAuthStateJsBean> getAllUserAuthStates() throws WebException
    {
        return getAllUserAuthStates(null, null, null);
    }

    // @Deprecated
    public List<UserAuthStateJsBean> getAllUserAuthStates(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllUserAuthStates(ordering, offset, count, null);
    }

    public List<UserAuthStateJsBean> getAllUserAuthStates(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<UserAuthStateJsBean> jsBeans = new ArrayList<UserAuthStateJsBean>();
            List<UserAuthState> userAuthStates = getServiceProxy().getAllUserAuthStates(ordering, offset, count, forwardCursor);
            if(userAuthStates != null) {
                for(UserAuthState userAuthState : userAuthStates) {
                    jsBeans.add(convertUserAuthStateToJsBean(userAuthState));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllUserAuthStateKeys(ordering, offset, count, null);
    }

    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllUserAuthStateKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<UserAuthStateJsBean> findUserAuthStates(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUserAuthStates(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<UserAuthStateJsBean> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<UserAuthStateJsBean> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<UserAuthStateJsBean> jsBeans = new ArrayList<UserAuthStateJsBean>();
            List<UserAuthState> userAuthStates = getServiceProxy().findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(userAuthStates != null) {
                for(UserAuthState userAuthState : userAuthStates) {
                    jsBeans.add(convertUserAuthStateToJsBean(userAuthState));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserAuthState(String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStructJsBean externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws WebException
    {
        try {
            return getServiceProxy().createUserAuthState(managerApp, appAcl, GaeAppStructWebService.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, ExternalUserIdStructWebService.convertExternalUserIdStructJsBeanToBean(externalId), status, firstAuthTime, lastAuthTime, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserAuthState(UserAuthStateJsBean jsBean) throws WebException
    {
        try {
            UserAuthState userAuthState = convertUserAuthStateJsBeanToBean(jsBean);
            return getServiceProxy().createUserAuthState(userAuthState);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserAuthStateJsBean constructUserAuthState(UserAuthStateJsBean jsBean) throws WebException
    {
        try {
            UserAuthState userAuthState = convertUserAuthStateJsBeanToBean(jsBean);
            userAuthState = getServiceProxy().constructUserAuthState(userAuthState);
            jsBean = convertUserAuthStateToJsBean(userAuthState);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUserAuthState(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStructJsBean externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws WebException
    {
        try {
            return getServiceProxy().updateUserAuthState(guid, managerApp, appAcl, GaeAppStructWebService.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, ExternalUserIdStructWebService.convertExternalUserIdStructJsBeanToBean(externalId), status, firstAuthTime, lastAuthTime, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUserAuthState(UserAuthStateJsBean jsBean) throws WebException
    {
        try {
            UserAuthState userAuthState = convertUserAuthStateJsBeanToBean(jsBean);
            return getServiceProxy().updateUserAuthState(userAuthState);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserAuthStateJsBean refreshUserAuthState(UserAuthStateJsBean jsBean) throws WebException
    {
        try {
            UserAuthState userAuthState = convertUserAuthStateJsBeanToBean(jsBean);
            userAuthState = getServiceProxy().refreshUserAuthState(userAuthState);
            jsBean = convertUserAuthStateToJsBean(userAuthState);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserAuthState(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteUserAuthState(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserAuthState(UserAuthStateJsBean jsBean) throws WebException
    {
        try {
            UserAuthState userAuthState = convertUserAuthStateJsBeanToBean(jsBean);
            return getServiceProxy().deleteUserAuthState(userAuthState);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUserAuthStates(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteUserAuthStates(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static UserAuthStateJsBean convertUserAuthStateToJsBean(UserAuthState userAuthState)
    {
        UserAuthStateJsBean jsBean = null;
        if(userAuthState != null) {
            jsBean = new UserAuthStateJsBean();
            jsBean.setGuid(userAuthState.getGuid());
            jsBean.setManagerApp(userAuthState.getManagerApp());
            jsBean.setAppAcl(userAuthState.getAppAcl());
            jsBean.setGaeApp(GaeAppStructWebService.convertGaeAppStructToJsBean(userAuthState.getGaeApp()));
            jsBean.setOwnerUser(userAuthState.getOwnerUser());
            jsBean.setUserAcl(userAuthState.getUserAcl());
            jsBean.setProviderId(userAuthState.getProviderId());
            jsBean.setUser(userAuthState.getUser());
            jsBean.setUsername(userAuthState.getUsername());
            jsBean.setEmail(userAuthState.getEmail());
            jsBean.setOpenId(userAuthState.getOpenId());
            jsBean.setDeviceId(userAuthState.getDeviceId());
            jsBean.setSessionId(userAuthState.getSessionId());
            jsBean.setAuthToken(userAuthState.getAuthToken());
            jsBean.setAuthStatus(userAuthState.getAuthStatus());
            jsBean.setExternalAuth(userAuthState.getExternalAuth());
            jsBean.setExternalId(ExternalUserIdStructWebService.convertExternalUserIdStructToJsBean(userAuthState.getExternalId()));
            jsBean.setStatus(userAuthState.getStatus());
            jsBean.setFirstAuthTime(userAuthState.getFirstAuthTime());
            jsBean.setLastAuthTime(userAuthState.getLastAuthTime());
            jsBean.setExpirationTime(userAuthState.getExpirationTime());
            jsBean.setCreatedTime(userAuthState.getCreatedTime());
            jsBean.setModifiedTime(userAuthState.getModifiedTime());
        }
        return jsBean;
    }

    public static UserAuthState convertUserAuthStateJsBeanToBean(UserAuthStateJsBean jsBean)
    {
        UserAuthStateBean userAuthState = null;
        if(jsBean != null) {
            userAuthState = new UserAuthStateBean();
            userAuthState.setGuid(jsBean.getGuid());
            userAuthState.setManagerApp(jsBean.getManagerApp());
            userAuthState.setAppAcl(jsBean.getAppAcl());
            userAuthState.setGaeApp(GaeAppStructWebService.convertGaeAppStructJsBeanToBean(jsBean.getGaeApp()));
            userAuthState.setOwnerUser(jsBean.getOwnerUser());
            userAuthState.setUserAcl(jsBean.getUserAcl());
            userAuthState.setProviderId(jsBean.getProviderId());
            userAuthState.setUser(jsBean.getUser());
            userAuthState.setUsername(jsBean.getUsername());
            userAuthState.setEmail(jsBean.getEmail());
            userAuthState.setOpenId(jsBean.getOpenId());
            userAuthState.setDeviceId(jsBean.getDeviceId());
            userAuthState.setSessionId(jsBean.getSessionId());
            userAuthState.setAuthToken(jsBean.getAuthToken());
            userAuthState.setAuthStatus(jsBean.getAuthStatus());
            userAuthState.setExternalAuth(jsBean.getExternalAuth());
            userAuthState.setExternalId(ExternalUserIdStructWebService.convertExternalUserIdStructJsBeanToBean(jsBean.getExternalId()));
            userAuthState.setStatus(jsBean.getStatus());
            userAuthState.setFirstAuthTime(jsBean.getFirstAuthTime());
            userAuthState.setLastAuthTime(jsBean.getLastAuthTime());
            userAuthState.setExpirationTime(jsBean.getExpirationTime());
            userAuthState.setCreatedTime(jsBean.getCreatedTime());
            userAuthState.setModifiedTime(jsBean.getModifiedTime());
        }
        return userAuthState;
    }

}
