package com.queryclient.rf.proxy;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ServiceProxyFactory
{
    private static final Logger log = Logger.getLogger(ServiceProxyFactory.class.getName());

    private UserServiceProxy userService = null;
    private UserPasswordServiceProxy userPasswordService = null;
    private ExternalUserAuthServiceProxy externalUserAuthService = null;
    private UserAuthStateServiceProxy userAuthStateService = null;
    private DataServiceServiceProxy dataServiceService = null;
    private ServiceEndpointServiceProxy serviceEndpointService = null;
    private QuerySessionServiceProxy querySessionService = null;
    private QueryRecordServiceProxy queryRecordService = null;
    private DummyEntityServiceProxy dummyEntityService = null;
    private ServiceInfoServiceProxy serviceInfoService = null;
    private FiveTenServiceProxy fiveTenService = null;

    protected ServiceProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ServiceProxyFactoryHolder
    {
        private static final ServiceProxyFactory INSTANCE = new ServiceProxyFactory();
    }

    // Singleton method
    public static ServiceProxyFactory getInstance()
    {
        return ServiceProxyFactoryHolder.INSTANCE;
    }

    public UserServiceProxy getUserServiceProxy()
    {
        if(userService == null) {
            userService = new UserServiceProxy();
        }
        return userService;
    }

    public UserPasswordServiceProxy getUserPasswordServiceProxy()
    {
        if(userPasswordService == null) {
            userPasswordService = new UserPasswordServiceProxy();
        }
        return userPasswordService;
    }

    public ExternalUserAuthServiceProxy getExternalUserAuthServiceProxy()
    {
        if(externalUserAuthService == null) {
            externalUserAuthService = new ExternalUserAuthServiceProxy();
        }
        return externalUserAuthService;
    }

    public UserAuthStateServiceProxy getUserAuthStateServiceProxy()
    {
        if(userAuthStateService == null) {
            userAuthStateService = new UserAuthStateServiceProxy();
        }
        return userAuthStateService;
    }

    public DataServiceServiceProxy getDataServiceServiceProxy()
    {
        if(dataServiceService == null) {
            dataServiceService = new DataServiceServiceProxy();
        }
        return dataServiceService;
    }

    public ServiceEndpointServiceProxy getServiceEndpointServiceProxy()
    {
        if(serviceEndpointService == null) {
            serviceEndpointService = new ServiceEndpointServiceProxy();
        }
        return serviceEndpointService;
    }

    public QuerySessionServiceProxy getQuerySessionServiceProxy()
    {
        if(querySessionService == null) {
            querySessionService = new QuerySessionServiceProxy();
        }
        return querySessionService;
    }

    public QueryRecordServiceProxy getQueryRecordServiceProxy()
    {
        if(queryRecordService == null) {
            queryRecordService = new QueryRecordServiceProxy();
        }
        return queryRecordService;
    }

    public DummyEntityServiceProxy getDummyEntityServiceProxy()
    {
        if(dummyEntityService == null) {
            dummyEntityService = new DummyEntityServiceProxy();
        }
        return dummyEntityService;
    }

    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new ServiceInfoServiceProxy();
        }
        return serviceInfoService;
    }

    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenService == null) {
            fiveTenService = new FiveTenServiceProxy();
        }
        return fiveTenService;
    }

}
