package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ConsumerKeySecretPairJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.ConsumerKeySecretPairWebService;


// MockConsumerKeySecretPairWebService is a mock object.
// It can be used as a base class to mock ConsumerKeySecretPairWebService objects.
public abstract class MockConsumerKeySecretPairWebService extends ConsumerKeySecretPairWebService  // implements ConsumerKeySecretPairService
{
    private static final Logger log = Logger.getLogger(MockConsumerKeySecretPairWebService.class.getName());
     

}
