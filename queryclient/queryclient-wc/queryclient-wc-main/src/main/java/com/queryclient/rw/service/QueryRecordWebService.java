package com.queryclient.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.fe.bean.QueryRecordJsBean;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class QueryRecordWebService // implements QueryRecordService
{
    private static final Logger log = Logger.getLogger(QueryRecordWebService.class.getName());
     
    // Af service interface.
    private QueryRecordService mService = null;

    public QueryRecordWebService()
    {
        this(ServiceProxyFactory.getInstance().getQueryRecordServiceProxy());
    }
    public QueryRecordWebService(QueryRecordService service)
    {
        mService = service;
    }
    
    protected QueryRecordService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getQueryRecordServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(QueryRecordService service)
    {
        mService = service;
    }
    
    
    public QueryRecordJsBean getQueryRecord(String guid) throws WebException
    {
        try {
            QueryRecord queryRecord = getServiceProxy().getQueryRecord(guid);
            QueryRecordJsBean bean = convertQueryRecordToJsBean(queryRecord);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getQueryRecord(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getQueryRecord(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QueryRecordJsBean> getQueryRecords(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QueryRecordJsBean> jsBeans = new ArrayList<QueryRecordJsBean>();
            List<QueryRecord> queryRecords = getServiceProxy().getQueryRecords(guids);
            if(queryRecords != null) {
                for(QueryRecord queryRecord : queryRecords) {
                    jsBeans.add(convertQueryRecordToJsBean(queryRecord));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QueryRecordJsBean> getAllQueryRecords() throws WebException
    {
        return getAllQueryRecords(null, null, null);
    }

    // @Deprecated
    public List<QueryRecordJsBean> getAllQueryRecords(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllQueryRecords(ordering, offset, count, null);
    }

    public List<QueryRecordJsBean> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<QueryRecordJsBean> jsBeans = new ArrayList<QueryRecordJsBean>();
            List<QueryRecord> queryRecords = getServiceProxy().getAllQueryRecords(ordering, offset, count, forwardCursor);
            if(queryRecords != null) {
                for(QueryRecord queryRecord : queryRecords) {
                    jsBeans.add(convertQueryRecordToJsBean(queryRecord));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<QueryRecordJsBean> findQueryRecords(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findQueryRecords(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<QueryRecordJsBean> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<QueryRecordJsBean> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<QueryRecordJsBean> jsBeans = new ArrayList<QueryRecordJsBean>();
            List<QueryRecord> queryRecords = getServiceProxy().findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(queryRecords != null) {
                for(QueryRecord queryRecord : queryRecords) {
                    jsBeans.add(convertQueryRecordToJsBean(queryRecord));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createQueryRecord(String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStructJsBean referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws WebException
    {
        try {
            return getServiceProxy().createQueryRecord(querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, extra, note, scheduledTime, processedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createQueryRecord(QueryRecordJsBean jsBean) throws WebException
    {
        try {
            QueryRecord queryRecord = convertQueryRecordJsBeanToBean(jsBean);
            return getServiceProxy().createQueryRecord(queryRecord);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public QueryRecordJsBean constructQueryRecord(QueryRecordJsBean jsBean) throws WebException
    {
        try {
            QueryRecord queryRecord = convertQueryRecordJsBeanToBean(jsBean);
            queryRecord = getServiceProxy().constructQueryRecord(queryRecord);
            jsBean = convertQueryRecordToJsBean(queryRecord);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStructJsBean referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws WebException
    {
        try {
            return getServiceProxy().updateQueryRecord(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, extra, note, scheduledTime, processedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateQueryRecord(QueryRecordJsBean jsBean) throws WebException
    {
        try {
            QueryRecord queryRecord = convertQueryRecordJsBeanToBean(jsBean);
            return getServiceProxy().updateQueryRecord(queryRecord);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public QueryRecordJsBean refreshQueryRecord(QueryRecordJsBean jsBean) throws WebException
    {
        try {
            QueryRecord queryRecord = convertQueryRecordJsBeanToBean(jsBean);
            queryRecord = getServiceProxy().refreshQueryRecord(queryRecord);
            jsBean = convertQueryRecordToJsBean(queryRecord);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteQueryRecord(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteQueryRecord(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteQueryRecord(QueryRecordJsBean jsBean) throws WebException
    {
        try {
            QueryRecord queryRecord = convertQueryRecordJsBeanToBean(jsBean);
            return getServiceProxy().deleteQueryRecord(queryRecord);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteQueryRecords(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteQueryRecords(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static QueryRecordJsBean convertQueryRecordToJsBean(QueryRecord queryRecord)
    {
        QueryRecordJsBean jsBean = null;
        if(queryRecord != null) {
            jsBean = new QueryRecordJsBean();
            jsBean.setGuid(queryRecord.getGuid());
            jsBean.setQuerySession(queryRecord.getQuerySession());
            jsBean.setQueryId(queryRecord.getQueryId());
            jsBean.setDataService(queryRecord.getDataService());
            jsBean.setServiceUrl(queryRecord.getServiceUrl());
            jsBean.setDelayed(queryRecord.isDelayed());
            jsBean.setQuery(queryRecord.getQuery());
            jsBean.setInputFormat(queryRecord.getInputFormat());
            jsBean.setInputFile(queryRecord.getInputFile());
            jsBean.setInputContent(queryRecord.getInputContent());
            jsBean.setTargetOutputFormat(queryRecord.getTargetOutputFormat());
            jsBean.setOutputFormat(queryRecord.getOutputFormat());
            jsBean.setOutputFile(queryRecord.getOutputFile());
            jsBean.setOutputContent(queryRecord.getOutputContent());
            jsBean.setResponseCode(queryRecord.getResponseCode());
            jsBean.setResult(queryRecord.getResult());
            jsBean.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructToJsBean(queryRecord.getReferrerInfo()));
            jsBean.setStatus(queryRecord.getStatus());
            jsBean.setExtra(queryRecord.getExtra());
            jsBean.setNote(queryRecord.getNote());
            jsBean.setScheduledTime(queryRecord.getScheduledTime());
            jsBean.setProcessedTime(queryRecord.getProcessedTime());
            jsBean.setCreatedTime(queryRecord.getCreatedTime());
            jsBean.setModifiedTime(queryRecord.getModifiedTime());
        }
        return jsBean;
    }

    public static QueryRecord convertQueryRecordJsBeanToBean(QueryRecordJsBean jsBean)
    {
        QueryRecordBean queryRecord = null;
        if(jsBean != null) {
            queryRecord = new QueryRecordBean();
            queryRecord.setGuid(jsBean.getGuid());
            queryRecord.setQuerySession(jsBean.getQuerySession());
            queryRecord.setQueryId(jsBean.getQueryId());
            queryRecord.setDataService(jsBean.getDataService());
            queryRecord.setServiceUrl(jsBean.getServiceUrl());
            queryRecord.setDelayed(jsBean.isDelayed());
            queryRecord.setQuery(jsBean.getQuery());
            queryRecord.setInputFormat(jsBean.getInputFormat());
            queryRecord.setInputFile(jsBean.getInputFile());
            queryRecord.setInputContent(jsBean.getInputContent());
            queryRecord.setTargetOutputFormat(jsBean.getTargetOutputFormat());
            queryRecord.setOutputFormat(jsBean.getOutputFormat());
            queryRecord.setOutputFile(jsBean.getOutputFile());
            queryRecord.setOutputContent(jsBean.getOutputContent());
            queryRecord.setResponseCode(jsBean.getResponseCode());
            queryRecord.setResult(jsBean.getResult());
            queryRecord.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            queryRecord.setStatus(jsBean.getStatus());
            queryRecord.setExtra(jsBean.getExtra());
            queryRecord.setNote(jsBean.getNote());
            queryRecord.setScheduledTime(jsBean.getScheduledTime());
            queryRecord.setProcessedTime(jsBean.getProcessedTime());
            queryRecord.setCreatedTime(jsBean.getCreatedTime());
            queryRecord.setModifiedTime(jsBean.getModifiedTime());
        }
        return queryRecord;
    }

}
