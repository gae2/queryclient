package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.PagerStateStruct;
import com.queryclient.af.bean.PagerStateStructBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.PagerStateStructJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.PagerStateStructWebService;


// MockPagerStateStructWebService is a mock object.
// It can be used as a base class to mock PagerStateStructWebService objects.
public abstract class MockPagerStateStructWebService extends PagerStateStructWebService  // implements PagerStateStructService
{
    private static final Logger log = Logger.getLogger(MockPagerStateStructWebService.class.getName());
     

}
