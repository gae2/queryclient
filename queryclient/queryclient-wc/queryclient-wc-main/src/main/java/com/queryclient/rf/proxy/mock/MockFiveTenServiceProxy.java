package com.queryclient.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.FiveTen;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.FiveTenStub;
import com.queryclient.ws.stub.FiveTenListStub;
import com.queryclient.af.bean.FiveTenBean;
import com.queryclient.af.service.FiveTenService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.rf.config.Config;
import com.queryclient.rf.proxy.FiveTenServiceProxy;


// MockFiveTenServiceProxy is a decorator.
// It can be used as a base class to mock FiveTenService objects.
public abstract class MockFiveTenServiceProxy extends FiveTenServiceProxy implements FiveTenService
{
    private static final Logger log = Logger.getLogger(MockFiveTenServiceProxy.class.getName());

    // MockFiveTenServiceProxy uses the decorator design pattern.
    private FiveTenService decoratedProxy;

    public MockFiveTenServiceProxy(FiveTenService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected FiveTenService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(FiveTenService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        return decoratedProxy.getFiveTen(guid);
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        return decoratedProxy.getFiveTen(guid, field);
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        return decoratedProxy.getFiveTens(guids);
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return getAllFiveTens(null, null, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFiveTens(ordering, offset, count, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllFiveTens(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFiveTenKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllFiveTenKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findFiveTens(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findFiveTenKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException
    {
        return decoratedProxy.createFiveTen(counter, requesterIpAddress);
    }

    @Override
    public String createFiveTen(FiveTen bean) throws BaseException
    {
        return decoratedProxy.createFiveTen(bean);
    }

    @Override
    public FiveTen constructFiveTen(FiveTen bean) throws BaseException
    {
        return decoratedProxy.constructFiveTen(bean);
    }

    @Override
    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException
    {
        return decoratedProxy.updateFiveTen(guid, counter, requesterIpAddress);
    }

    @Override
    public Boolean updateFiveTen(FiveTen bean) throws BaseException
    {
        return decoratedProxy.updateFiveTen(bean);
    }

    @Override
    public FiveTen refreshFiveTen(FiveTen bean) throws BaseException
    {
        return decoratedProxy.refreshFiveTen(bean);
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        return decoratedProxy.deleteFiveTen(guid);
    }

    @Override
    public Boolean deleteFiveTen(FiveTen bean) throws BaseException
    {
        return decoratedProxy.deleteFiveTen(bean);
    }

    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteFiveTens(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createFiveTens(List<FiveTen> fiveTens) throws BaseException
    {
        return decoratedProxy.createFiveTens(fiveTens);
    }

    // TBD
    //@Override
    //public Boolean updateFiveTens(List<FiveTen> fiveTens) throws BaseException
    //{
    //}

}
