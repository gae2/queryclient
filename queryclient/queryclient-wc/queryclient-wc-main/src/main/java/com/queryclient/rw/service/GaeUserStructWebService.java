package com.queryclient.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.af.bean.GaeUserStructBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.GaeUserStructJsBean;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class GaeUserStructWebService // implements GaeUserStructService
{
    private static final Logger log = Logger.getLogger(GaeUserStructWebService.class.getName());
     
    public static GaeUserStructJsBean convertGaeUserStructToJsBean(GaeUserStruct gaeUserStruct)
    {
        GaeUserStructJsBean jsBean = null;
        if(gaeUserStruct != null) {
            jsBean = new GaeUserStructJsBean();
            jsBean.setAuthDomain(gaeUserStruct.getAuthDomain());
            jsBean.setFederatedIdentity(gaeUserStruct.getFederatedIdentity());
            jsBean.setNickname(gaeUserStruct.getNickname());
            jsBean.setUserId(gaeUserStruct.getUserId());
            jsBean.setEmail(gaeUserStruct.getEmail());
            jsBean.setNote(gaeUserStruct.getNote());
        }
        return jsBean;
    }

    public static GaeUserStruct convertGaeUserStructJsBeanToBean(GaeUserStructJsBean jsBean)
    {
        GaeUserStructBean gaeUserStruct = null;
        if(jsBean != null) {
            gaeUserStruct = new GaeUserStructBean();
            gaeUserStruct.setAuthDomain(jsBean.getAuthDomain());
            gaeUserStruct.setFederatedIdentity(jsBean.getFederatedIdentity());
            gaeUserStruct.setNickname(jsBean.getNickname());
            gaeUserStruct.setUserId(jsBean.getUserId());
            gaeUserStruct.setEmail(jsBean.getEmail());
            gaeUserStruct.setNote(jsBean.getNote());
        }
        return gaeUserStruct;
    }

}
