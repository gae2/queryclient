package com.queryclient.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ServiceInfo;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.ServiceInfoStub;
import com.queryclient.ws.stub.ServiceInfoListStub;
import com.queryclient.af.bean.ServiceInfoBean;
import com.queryclient.af.service.ServiceInfoService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.rf.config.Config;
import com.queryclient.rf.proxy.ServiceInfoServiceProxy;


// MockServiceInfoServiceProxy is a decorator.
// It can be used as a base class to mock ServiceInfoService objects.
public abstract class MockServiceInfoServiceProxy extends ServiceInfoServiceProxy implements ServiceInfoService
{
    private static final Logger log = Logger.getLogger(MockServiceInfoServiceProxy.class.getName());

    // MockServiceInfoServiceProxy uses the decorator design pattern.
    private ServiceInfoService decoratedProxy;

    public MockServiceInfoServiceProxy(ServiceInfoService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected ServiceInfoService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(ServiceInfoService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public ServiceInfo getServiceInfo(String guid) throws BaseException
    {
        return decoratedProxy.getServiceInfo(guid);
    }

    @Override
    public Object getServiceInfo(String guid, String field) throws BaseException
    {
        return decoratedProxy.getServiceInfo(guid, field);
    }

    @Override
    public List<ServiceInfo> getServiceInfos(List<String> guids) throws BaseException
    {
        return decoratedProxy.getServiceInfos(guids);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos() throws BaseException
    {
        return getAllServiceInfos(null, null, null);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceInfos(ordering, offset, count, null);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllServiceInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllServiceInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findServiceInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findServiceInfos(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findServiceInfoKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createServiceInfo(String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        return decoratedProxy.createServiceInfo(title, content, type, status, scheduledTime);
    }

    @Override
    public String createServiceInfo(ServiceInfo bean) throws BaseException
    {
        return decoratedProxy.createServiceInfo(bean);
    }

    @Override
    public ServiceInfo constructServiceInfo(ServiceInfo bean) throws BaseException
    {
        return decoratedProxy.constructServiceInfo(bean);
    }

    @Override
    public Boolean updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        return decoratedProxy.updateServiceInfo(guid, title, content, type, status, scheduledTime);
    }

    @Override
    public Boolean updateServiceInfo(ServiceInfo bean) throws BaseException
    {
        return decoratedProxy.updateServiceInfo(bean);
    }

    @Override
    public ServiceInfo refreshServiceInfo(ServiceInfo bean) throws BaseException
    {
        return decoratedProxy.refreshServiceInfo(bean);
    }

    @Override
    public Boolean deleteServiceInfo(String guid) throws BaseException
    {
        return decoratedProxy.deleteServiceInfo(guid);
    }

    @Override
    public Boolean deleteServiceInfo(ServiceInfo bean) throws BaseException
    {
        return decoratedProxy.deleteServiceInfo(bean);
    }

    @Override
    public Long deleteServiceInfos(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteServiceInfos(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createServiceInfos(List<ServiceInfo> serviceInfos) throws BaseException
    {
        return decoratedProxy.createServiceInfos(serviceInfos);
    }

    // TBD
    //@Override
    //public Boolean updateServiceInfos(List<ServiceInfo> serviceInfos) throws BaseException
    //{
    //}

}
