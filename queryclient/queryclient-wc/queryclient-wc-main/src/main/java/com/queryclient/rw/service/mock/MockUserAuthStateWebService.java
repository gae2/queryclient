package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.UserAuthState;
import com.queryclient.af.bean.UserAuthStateBean;
import com.queryclient.af.service.UserAuthStateService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.GaeAppStructJsBean;
import com.queryclient.fe.bean.ExternalUserIdStructJsBean;
import com.queryclient.fe.bean.UserAuthStateJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.UserAuthStateWebService;


// MockUserAuthStateWebService is a mock object.
// It can be used as a base class to mock UserAuthStateWebService objects.
public abstract class MockUserAuthStateWebService extends UserAuthStateWebService  // implements UserAuthStateService
{
    private static final Logger log = Logger.getLogger(MockUserAuthStateWebService.class.getName());
     
    // Af service interface.
    private UserAuthStateWebService mService = null;

    public MockUserAuthStateWebService()
    {
        this(MockServiceProxyFactory.getInstance().getUserAuthStateServiceProxy());
    }
    public MockUserAuthStateWebService(UserAuthStateService service)
    {
        super(service);
    }


}
