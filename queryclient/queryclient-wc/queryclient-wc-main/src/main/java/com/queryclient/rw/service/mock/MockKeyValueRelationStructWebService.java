package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.KeyValueRelationStruct;
import com.queryclient.af.bean.KeyValueRelationStructBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.KeyValueRelationStructJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.KeyValueRelationStructWebService;


// MockKeyValueRelationStructWebService is a mock object.
// It can be used as a base class to mock KeyValueRelationStructWebService objects.
public abstract class MockKeyValueRelationStructWebService extends KeyValueRelationStructWebService  // implements KeyValueRelationStructService
{
    private static final Logger log = Logger.getLogger(MockKeyValueRelationStructWebService.class.getName());
     

}
