package com.queryclient.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ConsumerKeySecretPairJsBean;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ConsumerKeySecretPairWebService // implements ConsumerKeySecretPairService
{
    private static final Logger log = Logger.getLogger(ConsumerKeySecretPairWebService.class.getName());
     
    public static ConsumerKeySecretPairJsBean convertConsumerKeySecretPairToJsBean(ConsumerKeySecretPair consumerKeySecretPair)
    {
        ConsumerKeySecretPairJsBean jsBean = null;
        if(consumerKeySecretPair != null) {
            jsBean = new ConsumerKeySecretPairJsBean();
            jsBean.setConsumerKey(consumerKeySecretPair.getConsumerKey());
            jsBean.setConsumerSecret(consumerKeySecretPair.getConsumerSecret());
        }
        return jsBean;
    }

    public static ConsumerKeySecretPair convertConsumerKeySecretPairJsBeanToBean(ConsumerKeySecretPairJsBean jsBean)
    {
        ConsumerKeySecretPairBean consumerKeySecretPair = null;
        if(jsBean != null) {
            consumerKeySecretPair = new ConsumerKeySecretPairBean();
            consumerKeySecretPair.setConsumerKey(jsBean.getConsumerKey());
            consumerKeySecretPair.setConsumerSecret(jsBean.getConsumerSecret());
        }
        return consumerKeySecretPair;
    }

}
