package com.queryclient.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ServiceInfo;
import com.queryclient.af.bean.ServiceInfoBean;
import com.queryclient.af.service.ServiceInfoService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ServiceInfoJsBean;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ServiceInfoWebService // implements ServiceInfoService
{
    private static final Logger log = Logger.getLogger(ServiceInfoWebService.class.getName());
     
    // Af service interface.
    private ServiceInfoService mService = null;

    public ServiceInfoWebService()
    {
        this(ServiceProxyFactory.getInstance().getServiceInfoServiceProxy());
    }
    public ServiceInfoWebService(ServiceInfoService service)
    {
        mService = service;
    }
    
    protected ServiceInfoService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getServiceInfoServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(ServiceInfoService service)
    {
        mService = service;
    }
    
    
    public ServiceInfoJsBean getServiceInfo(String guid) throws WebException
    {
        try {
            ServiceInfo serviceInfo = getServiceProxy().getServiceInfo(guid);
            ServiceInfoJsBean bean = convertServiceInfoToJsBean(serviceInfo);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getServiceInfo(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getServiceInfo(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ServiceInfoJsBean> getServiceInfos(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ServiceInfoJsBean> jsBeans = new ArrayList<ServiceInfoJsBean>();
            List<ServiceInfo> serviceInfos = getServiceProxy().getServiceInfos(guids);
            if(serviceInfos != null) {
                for(ServiceInfo serviceInfo : serviceInfos) {
                    jsBeans.add(convertServiceInfoToJsBean(serviceInfo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ServiceInfoJsBean> getAllServiceInfos() throws WebException
    {
        return getAllServiceInfos(null, null, null);
    }

    // @Deprecated
    public List<ServiceInfoJsBean> getAllServiceInfos(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllServiceInfos(ordering, offset, count, null);
    }

    public List<ServiceInfoJsBean> getAllServiceInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<ServiceInfoJsBean> jsBeans = new ArrayList<ServiceInfoJsBean>();
            List<ServiceInfo> serviceInfos = getServiceProxy().getAllServiceInfos(ordering, offset, count, forwardCursor);
            if(serviceInfos != null) {
                for(ServiceInfo serviceInfo : serviceInfos) {
                    jsBeans.add(convertServiceInfoToJsBean(serviceInfo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllServiceInfoKeys(ordering, offset, count, null);
    }

    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllServiceInfoKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<ServiceInfoJsBean> findServiceInfos(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findServiceInfos(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<ServiceInfoJsBean> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<ServiceInfoJsBean> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<ServiceInfoJsBean> jsBeans = new ArrayList<ServiceInfoJsBean>();
            List<ServiceInfo> serviceInfos = getServiceProxy().findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(serviceInfos != null) {
                for(ServiceInfo serviceInfo : serviceInfos) {
                    jsBeans.add(convertServiceInfoToJsBean(serviceInfo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createServiceInfo(String title, String content, String type, String status, Long scheduledTime) throws WebException
    {
        try {
            return getServiceProxy().createServiceInfo(title, content, type, status, scheduledTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createServiceInfo(ServiceInfoJsBean jsBean) throws WebException
    {
        try {
            ServiceInfo serviceInfo = convertServiceInfoJsBeanToBean(jsBean);
            return getServiceProxy().createServiceInfo(serviceInfo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ServiceInfoJsBean constructServiceInfo(ServiceInfoJsBean jsBean) throws WebException
    {
        try {
            ServiceInfo serviceInfo = convertServiceInfoJsBeanToBean(jsBean);
            serviceInfo = getServiceProxy().constructServiceInfo(serviceInfo);
            jsBean = convertServiceInfoToJsBean(serviceInfo);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime) throws WebException
    {
        try {
            return getServiceProxy().updateServiceInfo(guid, title, content, type, status, scheduledTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateServiceInfo(ServiceInfoJsBean jsBean) throws WebException
    {
        try {
            ServiceInfo serviceInfo = convertServiceInfoJsBeanToBean(jsBean);
            return getServiceProxy().updateServiceInfo(serviceInfo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ServiceInfoJsBean refreshServiceInfo(ServiceInfoJsBean jsBean) throws WebException
    {
        try {
            ServiceInfo serviceInfo = convertServiceInfoJsBeanToBean(jsBean);
            serviceInfo = getServiceProxy().refreshServiceInfo(serviceInfo);
            jsBean = convertServiceInfoToJsBean(serviceInfo);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteServiceInfo(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteServiceInfo(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteServiceInfo(ServiceInfoJsBean jsBean) throws WebException
    {
        try {
            ServiceInfo serviceInfo = convertServiceInfoJsBeanToBean(jsBean);
            return getServiceProxy().deleteServiceInfo(serviceInfo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteServiceInfos(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteServiceInfos(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static ServiceInfoJsBean convertServiceInfoToJsBean(ServiceInfo serviceInfo)
    {
        ServiceInfoJsBean jsBean = null;
        if(serviceInfo != null) {
            jsBean = new ServiceInfoJsBean();
            jsBean.setGuid(serviceInfo.getGuid());
            jsBean.setTitle(serviceInfo.getTitle());
            jsBean.setContent(serviceInfo.getContent());
            jsBean.setType(serviceInfo.getType());
            jsBean.setStatus(serviceInfo.getStatus());
            jsBean.setScheduledTime(serviceInfo.getScheduledTime());
            jsBean.setCreatedTime(serviceInfo.getCreatedTime());
            jsBean.setModifiedTime(serviceInfo.getModifiedTime());
        }
        return jsBean;
    }

    public static ServiceInfo convertServiceInfoJsBeanToBean(ServiceInfoJsBean jsBean)
    {
        ServiceInfoBean serviceInfo = null;
        if(jsBean != null) {
            serviceInfo = new ServiceInfoBean();
            serviceInfo.setGuid(jsBean.getGuid());
            serviceInfo.setTitle(jsBean.getTitle());
            serviceInfo.setContent(jsBean.getContent());
            serviceInfo.setType(jsBean.getType());
            serviceInfo.setStatus(jsBean.getStatus());
            serviceInfo.setScheduledTime(jsBean.getScheduledTime());
            serviceInfo.setCreatedTime(jsBean.getCreatedTime());
            serviceInfo.setModifiedTime(jsBean.getModifiedTime());
        }
        return serviceInfo;
    }

}
