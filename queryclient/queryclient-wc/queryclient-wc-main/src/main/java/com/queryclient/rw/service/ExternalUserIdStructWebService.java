package com.queryclient.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ExternalUserIdStructJsBean;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ExternalUserIdStructWebService // implements ExternalUserIdStructService
{
    private static final Logger log = Logger.getLogger(ExternalUserIdStructWebService.class.getName());
     
    public static ExternalUserIdStructJsBean convertExternalUserIdStructToJsBean(ExternalUserIdStruct externalUserIdStruct)
    {
        ExternalUserIdStructJsBean jsBean = null;
        if(externalUserIdStruct != null) {
            jsBean = new ExternalUserIdStructJsBean();
            jsBean.setUuid(externalUserIdStruct.getUuid());
            jsBean.setId(externalUserIdStruct.getId());
            jsBean.setName(externalUserIdStruct.getName());
            jsBean.setEmail(externalUserIdStruct.getEmail());
            jsBean.setUsername(externalUserIdStruct.getUsername());
            jsBean.setOpenId(externalUserIdStruct.getOpenId());
            jsBean.setNote(externalUserIdStruct.getNote());
        }
        return jsBean;
    }

    public static ExternalUserIdStruct convertExternalUserIdStructJsBeanToBean(ExternalUserIdStructJsBean jsBean)
    {
        ExternalUserIdStructBean externalUserIdStruct = null;
        if(jsBean != null) {
            externalUserIdStruct = new ExternalUserIdStructBean();
            externalUserIdStruct.setUuid(jsBean.getUuid());
            externalUserIdStruct.setId(jsBean.getId());
            externalUserIdStruct.setName(jsBean.getName());
            externalUserIdStruct.setEmail(jsBean.getEmail());
            externalUserIdStruct.setUsername(jsBean.getUsername());
            externalUserIdStruct.setOpenId(jsBean.getOpenId());
            externalUserIdStruct.setNote(jsBean.getNote());
        }
        return externalUserIdStruct;
    }

}
