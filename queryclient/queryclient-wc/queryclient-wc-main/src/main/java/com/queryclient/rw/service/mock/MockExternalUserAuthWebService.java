package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.ws.ExternalUserAuth;
import com.queryclient.af.bean.ExternalUserAuthBean;
import com.queryclient.af.service.ExternalUserAuthService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.GaeAppStructJsBean;
import com.queryclient.fe.bean.ExternalUserIdStructJsBean;
import com.queryclient.fe.bean.ExternalUserAuthJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.ExternalUserAuthWebService;


// MockExternalUserAuthWebService is a mock object.
// It can be used as a base class to mock ExternalUserAuthWebService objects.
public abstract class MockExternalUserAuthWebService extends ExternalUserAuthWebService  // implements ExternalUserAuthService
{
    private static final Logger log = Logger.getLogger(MockExternalUserAuthWebService.class.getName());
     
    // Af service interface.
    private ExternalUserAuthWebService mService = null;

    public MockExternalUserAuthWebService()
    {
        this(MockServiceProxyFactory.getInstance().getExternalUserAuthServiceProxy());
    }
    public MockExternalUserAuthWebService(ExternalUserAuthService service)
    {
        super(service);
    }


}
