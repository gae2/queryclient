package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.FiveTen;
import com.queryclient.af.bean.FiveTenBean;
import com.queryclient.af.service.FiveTenService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.FiveTenJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.FiveTenWebService;


// MockFiveTenWebService is a mock object.
// It can be used as a base class to mock FiveTenWebService objects.
public abstract class MockFiveTenWebService extends FiveTenWebService  // implements FiveTenService
{
    private static final Logger log = Logger.getLogger(MockFiveTenWebService.class.getName());
     
    // Af service interface.
    private FiveTenWebService mService = null;

    public MockFiveTenWebService()
    {
        this(MockServiceProxyFactory.getInstance().getFiveTenServiceProxy());
    }
    public MockFiveTenWebService(FiveTenService service)
    {
        super(service);
    }


}
