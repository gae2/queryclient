package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.ReferrerInfoStructWebService;


// MockReferrerInfoStructWebService is a mock object.
// It can be used as a base class to mock ReferrerInfoStructWebService objects.
public abstract class MockReferrerInfoStructWebService extends ReferrerInfoStructWebService  // implements ReferrerInfoStructService
{
    private static final Logger log = Logger.getLogger(MockReferrerInfoStructWebService.class.getName());
     

}
