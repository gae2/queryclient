package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.fe.bean.QueryRecordJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.QueryRecordWebService;


// MockQueryRecordWebService is a mock object.
// It can be used as a base class to mock QueryRecordWebService objects.
public abstract class MockQueryRecordWebService extends QueryRecordWebService  // implements QueryRecordService
{
    private static final Logger log = Logger.getLogger(MockQueryRecordWebService.class.getName());
     
    // Af service interface.
    private QueryRecordWebService mService = null;

    public MockQueryRecordWebService()
    {
        this(MockServiceProxyFactory.getInstance().getQueryRecordServiceProxy());
    }
    public MockQueryRecordWebService(QueryRecordService service)
    {
        super(service);
    }


}
