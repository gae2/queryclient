package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.User;
import com.queryclient.af.bean.UserBean;
import com.queryclient.af.service.UserService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.GaeAppStructJsBean;
import com.queryclient.fe.bean.GaeUserStructJsBean;
import com.queryclient.fe.bean.UserJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.UserWebService;


// MockUserWebService is a mock object.
// It can be used as a base class to mock UserWebService objects.
public abstract class MockUserWebService extends UserWebService  // implements UserService
{
    private static final Logger log = Logger.getLogger(MockUserWebService.class.getName());
     
    // Af service interface.
    private UserWebService mService = null;

    public MockUserWebService()
    {
        this(MockServiceProxyFactory.getInstance().getUserServiceProxy());
    }
    public MockUserWebService(UserService service)
    {
        super(service);
    }


}
