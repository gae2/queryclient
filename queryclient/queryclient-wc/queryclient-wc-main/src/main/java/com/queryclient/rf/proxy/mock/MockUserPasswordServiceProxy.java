package com.queryclient.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.UserPassword;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.GaeAppStructStub;
import com.queryclient.ws.stub.GaeAppStructListStub;
import com.queryclient.ws.stub.UserPasswordStub;
import com.queryclient.ws.stub.UserPasswordListStub;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.UserPasswordBean;
import com.queryclient.af.service.UserPasswordService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.rf.config.Config;
import com.queryclient.rf.proxy.UserPasswordServiceProxy;


// MockUserPasswordServiceProxy is a decorator.
// It can be used as a base class to mock UserPasswordService objects.
public abstract class MockUserPasswordServiceProxy extends UserPasswordServiceProxy implements UserPasswordService
{
    private static final Logger log = Logger.getLogger(MockUserPasswordServiceProxy.class.getName());

    // MockUserPasswordServiceProxy uses the decorator design pattern.
    private UserPasswordService decoratedProxy;

    public MockUserPasswordServiceProxy(UserPasswordService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected UserPasswordService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(UserPasswordService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public UserPassword getUserPassword(String guid) throws BaseException
    {
        return decoratedProxy.getUserPassword(guid);
    }

    @Override
    public Object getUserPassword(String guid, String field) throws BaseException
    {
        return decoratedProxy.getUserPassword(guid, field);
    }

    @Override
    public List<UserPassword> getUserPasswords(List<String> guids) throws BaseException
    {
        return decoratedProxy.getUserPasswords(guids);
    }

    @Override
    public List<UserPassword> getAllUserPasswords() throws BaseException
    {
        return getAllUserPasswords(null, null, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswords(ordering, offset, count, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUserPasswords(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserPasswordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUserPasswordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUserPasswords(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUserPasswordKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserPassword(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        return decoratedProxy.createUserPassword(managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

    @Override
    public String createUserPassword(UserPassword bean) throws BaseException
    {
        return decoratedProxy.createUserPassword(bean);
    }

    @Override
    public UserPassword constructUserPassword(UserPassword bean) throws BaseException
    {
        return decoratedProxy.constructUserPassword(bean);
    }

    @Override
    public Boolean updateUserPassword(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String admin, String user, String username, String email, String openId, String plainPassword, String hashedPassword, String salt, String hashMethod, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        return decoratedProxy.updateUserPassword(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, admin, user, username, email, openId, plainPassword, hashedPassword, salt, hashMethod, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

    @Override
    public Boolean updateUserPassword(UserPassword bean) throws BaseException
    {
        return decoratedProxy.updateUserPassword(bean);
    }

    @Override
    public UserPassword refreshUserPassword(UserPassword bean) throws BaseException
    {
        return decoratedProxy.refreshUserPassword(bean);
    }

    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        return decoratedProxy.deleteUserPassword(guid);
    }

    @Override
    public Boolean deleteUserPassword(UserPassword bean) throws BaseException
    {
        return decoratedProxy.deleteUserPassword(bean);
    }

    @Override
    public Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteUserPasswords(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUserPasswords(List<UserPassword> userPasswords) throws BaseException
    {
        return decoratedProxy.createUserPasswords(userPasswords);
    }

    // TBD
    //@Override
    //public Boolean updateUserPasswords(List<UserPassword> userPasswords) throws BaseException
    //{
    //}

}
