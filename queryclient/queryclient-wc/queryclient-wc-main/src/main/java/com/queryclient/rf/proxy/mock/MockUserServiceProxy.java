package com.queryclient.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.ws.User;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.GaeAppStructStub;
import com.queryclient.ws.stub.GaeAppStructListStub;
import com.queryclient.ws.stub.GaeUserStructStub;
import com.queryclient.ws.stub.GaeUserStructListStub;
import com.queryclient.ws.stub.UserStub;
import com.queryclient.ws.stub.UserListStub;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.af.bean.GaeUserStructBean;
import com.queryclient.af.bean.UserBean;
import com.queryclient.af.service.UserService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.rf.config.Config;
import com.queryclient.rf.proxy.UserServiceProxy;


// MockUserServiceProxy is a decorator.
// It can be used as a base class to mock UserService objects.
public abstract class MockUserServiceProxy extends UserServiceProxy implements UserService
{
    private static final Logger log = Logger.getLogger(MockUserServiceProxy.class.getName());

    // MockUserServiceProxy uses the decorator design pattern.
    private UserService decoratedProxy;

    public MockUserServiceProxy(UserService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected UserService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(UserService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public User getUser(String guid) throws BaseException
    {
        return decoratedProxy.getUser(guid);
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        return decoratedProxy.getUser(guid, field);
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        return decoratedProxy.getUsers(guids);
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUsers(ordering, offset, count, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUsers(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUserKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUsers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUsers(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUserKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String address, String location, String ipAddress, String referer, Boolean obsolete, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return decoratedProxy.createUser(managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, address, location, ipAddress, referer, obsolete, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public String createUser(User bean) throws BaseException
    {
        return decoratedProxy.createUser(bean);
    }

    @Override
    public User constructUser(User bean) throws BaseException
    {
        return decoratedProxy.constructUser(bean);
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String address, String location, String ipAddress, String referer, Boolean obsolete, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return decoratedProxy.updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, address, location, ipAddress, referer, obsolete, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public Boolean updateUser(User bean) throws BaseException
    {
        return decoratedProxy.updateUser(bean);
    }

    @Override
    public User refreshUser(User bean) throws BaseException
    {
        return decoratedProxy.refreshUser(bean);
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        return decoratedProxy.deleteUser(guid);
    }

    @Override
    public Boolean deleteUser(User bean) throws BaseException
    {
        return decoratedProxy.deleteUser(bean);
    }

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteUsers(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUsers(List<User> users) throws BaseException
    {
        return decoratedProxy.createUsers(users);
    }

    // TBD
    //@Override
    //public Boolean updateUsers(List<User> users) throws BaseException
    //{
    //}

}
