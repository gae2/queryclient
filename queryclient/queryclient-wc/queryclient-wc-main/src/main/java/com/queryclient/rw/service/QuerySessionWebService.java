package com.queryclient.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.fe.bean.QuerySessionJsBean;
import com.queryclient.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class QuerySessionWebService // implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(QuerySessionWebService.class.getName());
     
    // Af service interface.
    private QuerySessionService mService = null;

    public QuerySessionWebService()
    {
        this(ServiceProxyFactory.getInstance().getQuerySessionServiceProxy());
    }
    public QuerySessionWebService(QuerySessionService service)
    {
        mService = service;
    }
    
    protected QuerySessionService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getQuerySessionServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(QuerySessionService service)
    {
        mService = service;
    }
    
    
    public QuerySessionJsBean getQuerySession(String guid) throws WebException
    {
        try {
            QuerySession querySession = getServiceProxy().getQuerySession(guid);
            QuerySessionJsBean bean = convertQuerySessionToJsBean(querySession);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getQuerySession(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getQuerySession(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QuerySessionJsBean> getQuerySessions(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QuerySessionJsBean> jsBeans = new ArrayList<QuerySessionJsBean>();
            List<QuerySession> querySessions = getServiceProxy().getQuerySessions(guids);
            if(querySessions != null) {
                for(QuerySession querySession : querySessions) {
                    jsBeans.add(convertQuerySessionToJsBean(querySession));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QuerySessionJsBean> getAllQuerySessions() throws WebException
    {
        return getAllQuerySessions(null, null, null);
    }

    // @Deprecated
    public List<QuerySessionJsBean> getAllQuerySessions(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllQuerySessions(ordering, offset, count, null);
    }

    public List<QuerySessionJsBean> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<QuerySessionJsBean> jsBeans = new ArrayList<QuerySessionJsBean>();
            List<QuerySession> querySessions = getServiceProxy().getAllQuerySessions(ordering, offset, count, forwardCursor);
            if(querySessions != null) {
                for(QuerySession querySession : querySessions) {
                    jsBeans.add(convertQuerySessionToJsBean(querySession));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<QuerySessionJsBean> findQuerySessions(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<QuerySessionJsBean> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<QuerySessionJsBean> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<QuerySessionJsBean> jsBeans = new ArrayList<QuerySessionJsBean>();
            List<QuerySession> querySessions = getServiceProxy().findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(querySessions != null) {
                for(QuerySession querySession : querySessions) {
                    jsBeans.add(convertQuerySessionToJsBean(querySession));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStructJsBean referrerInfo, String status, String note) throws WebException
    {
        try {
            return getServiceProxy().createQuerySession(user, dataService, serviceUrl, inputFormat, outputFormat, ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createQuerySession(QuerySessionJsBean jsBean) throws WebException
    {
        try {
            QuerySession querySession = convertQuerySessionJsBeanToBean(jsBean);
            return getServiceProxy().createQuerySession(querySession);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public QuerySessionJsBean constructQuerySession(QuerySessionJsBean jsBean) throws WebException
    {
        try {
            QuerySession querySession = convertQuerySessionJsBeanToBean(jsBean);
            querySession = getServiceProxy().constructQuerySession(querySession);
            jsBean = convertQuerySessionToJsBean(querySession);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStructJsBean referrerInfo, String status, String note) throws WebException
    {
        try {
            return getServiceProxy().updateQuerySession(guid, user, dataService, serviceUrl, inputFormat, outputFormat, ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateQuerySession(QuerySessionJsBean jsBean) throws WebException
    {
        try {
            QuerySession querySession = convertQuerySessionJsBeanToBean(jsBean);
            return getServiceProxy().updateQuerySession(querySession);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public QuerySessionJsBean refreshQuerySession(QuerySessionJsBean jsBean) throws WebException
    {
        try {
            QuerySession querySession = convertQuerySessionJsBeanToBean(jsBean);
            querySession = getServiceProxy().refreshQuerySession(querySession);
            jsBean = convertQuerySessionToJsBean(querySession);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteQuerySession(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteQuerySession(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteQuerySession(QuerySessionJsBean jsBean) throws WebException
    {
        try {
            QuerySession querySession = convertQuerySessionJsBeanToBean(jsBean);
            return getServiceProxy().deleteQuerySession(querySession);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteQuerySessions(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteQuerySessions(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static QuerySessionJsBean convertQuerySessionToJsBean(QuerySession querySession)
    {
        QuerySessionJsBean jsBean = null;
        if(querySession != null) {
            jsBean = new QuerySessionJsBean();
            jsBean.setGuid(querySession.getGuid());
            jsBean.setUser(querySession.getUser());
            jsBean.setDataService(querySession.getDataService());
            jsBean.setServiceUrl(querySession.getServiceUrl());
            jsBean.setInputFormat(querySession.getInputFormat());
            jsBean.setOutputFormat(querySession.getOutputFormat());
            jsBean.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructToJsBean(querySession.getReferrerInfo()));
            jsBean.setStatus(querySession.getStatus());
            jsBean.setNote(querySession.getNote());
            jsBean.setCreatedTime(querySession.getCreatedTime());
            jsBean.setModifiedTime(querySession.getModifiedTime());
        }
        return jsBean;
    }

    public static QuerySession convertQuerySessionJsBeanToBean(QuerySessionJsBean jsBean)
    {
        QuerySessionBean querySession = null;
        if(jsBean != null) {
            querySession = new QuerySessionBean();
            querySession.setGuid(jsBean.getGuid());
            querySession.setUser(jsBean.getUser());
            querySession.setDataService(jsBean.getDataService());
            querySession.setServiceUrl(jsBean.getServiceUrl());
            querySession.setInputFormat(jsBean.getInputFormat());
            querySession.setOutputFormat(jsBean.getOutputFormat());
            querySession.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            querySession.setStatus(jsBean.getStatus());
            querySession.setNote(jsBean.getNote());
            querySession.setCreatedTime(jsBean.getCreatedTime());
            querySession.setModifiedTime(jsBean.getModifiedTime());
        }
        return querySession;
    }

}
