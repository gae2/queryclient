package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ExternalServiceApiKeyStruct;
import com.queryclient.af.bean.ExternalServiceApiKeyStructBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ExternalServiceApiKeyStructJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.ExternalServiceApiKeyStructWebService;


// MockExternalServiceApiKeyStructWebService is a mock object.
// It can be used as a base class to mock ExternalServiceApiKeyStructWebService objects.
public abstract class MockExternalServiceApiKeyStructWebService extends ExternalServiceApiKeyStructWebService  // implements ExternalServiceApiKeyStructService
{
    private static final Logger log = Logger.getLogger(MockExternalServiceApiKeyStructWebService.class.getName());
     

}
