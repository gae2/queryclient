package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ReferrerInfoStructJsBean;
import com.queryclient.fe.bean.QuerySessionJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.QuerySessionWebService;


// MockQuerySessionWebService is a mock object.
// It can be used as a base class to mock QuerySessionWebService objects.
public abstract class MockQuerySessionWebService extends QuerySessionWebService  // implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(MockQuerySessionWebService.class.getName());
     
    // Af service interface.
    private QuerySessionWebService mService = null;

    public MockQuerySessionWebService()
    {
        this(MockServiceProxyFactory.getInstance().getQuerySessionServiceProxy());
    }
    public MockQuerySessionWebService(QuerySessionService service)
    {
        super(service);
    }


}
