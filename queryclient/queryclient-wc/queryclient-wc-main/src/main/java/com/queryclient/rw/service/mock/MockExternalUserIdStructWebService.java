package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ExternalUserIdStruct;
import com.queryclient.af.bean.ExternalUserIdStructBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ExternalUserIdStructJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.ExternalUserIdStructWebService;


// MockExternalUserIdStructWebService is a mock object.
// It can be used as a base class to mock ExternalUserIdStructWebService objects.
public abstract class MockExternalUserIdStructWebService extends ExternalUserIdStructWebService  // implements ExternalUserIdStructService
{
    private static final Logger log = Logger.getLogger(MockExternalUserIdStructWebService.class.getName());
     

}
