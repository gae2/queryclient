package com.queryclient.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QueryRecord;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.ws.stub.ReferrerInfoStructListStub;
import com.queryclient.ws.stub.QueryRecordStub;
import com.queryclient.ws.stub.QueryRecordListStub;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QueryRecordBean;
import com.queryclient.af.service.QueryRecordService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.rf.config.Config;
import com.queryclient.rf.proxy.QueryRecordServiceProxy;


// MockQueryRecordServiceProxy is a decorator.
// It can be used as a base class to mock QueryRecordService objects.
public abstract class MockQueryRecordServiceProxy extends QueryRecordServiceProxy implements QueryRecordService
{
    private static final Logger log = Logger.getLogger(MockQueryRecordServiceProxy.class.getName());

    // MockQueryRecordServiceProxy uses the decorator design pattern.
    private QueryRecordService decoratedProxy;

    public MockQueryRecordServiceProxy(QueryRecordService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected QueryRecordService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(QueryRecordService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public QueryRecord getQueryRecord(String guid) throws BaseException
    {
        return decoratedProxy.getQueryRecord(guid);
    }

    @Override
    public Object getQueryRecord(String guid, String field) throws BaseException
    {
        return decoratedProxy.getQueryRecord(guid, field);
    }

    @Override
    public List<QueryRecord> getQueryRecords(List<String> guids) throws BaseException
    {
        return decoratedProxy.getQueryRecords(guids);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords() throws BaseException
    {
        return getAllQueryRecords(null, null, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecords(ordering, offset, count, null);
    }

    @Override
    public List<QueryRecord> getAllQueryRecords(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllQueryRecords(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQueryRecordKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQueryRecordKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllQueryRecordKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecords(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QueryRecord> findQueryRecords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findQueryRecords(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQueryRecordKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQueryRecordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findQueryRecordKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createQueryRecord(String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        return decoratedProxy.createQueryRecord(querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime);
    }

    @Override
    public String createQueryRecord(QueryRecord bean) throws BaseException
    {
        return decoratedProxy.createQueryRecord(bean);
    }

    @Override
    public QueryRecord constructQueryRecord(QueryRecord bean) throws BaseException
    {
        return decoratedProxy.constructQueryRecord(bean);
    }

    @Override
    public Boolean updateQueryRecord(String guid, String querySession, Integer queryId, String dataService, String serviceUrl, Boolean delayed, String query, String inputFormat, String inputFile, String inputContent, String targetOutputFormat, String outputFormat, String outputFile, String outputContent, Integer responseCode, String result, ReferrerInfoStruct referrerInfo, String status, String extra, String note, Long scheduledTime, Long processedTime) throws BaseException
    {
        return decoratedProxy.updateQueryRecord(guid, querySession, queryId, dataService, serviceUrl, delayed, query, inputFormat, inputFile, inputContent, targetOutputFormat, outputFormat, outputFile, outputContent, responseCode, result, referrerInfo, status, extra, note, scheduledTime, processedTime);
    }

    @Override
    public Boolean updateQueryRecord(QueryRecord bean) throws BaseException
    {
        return decoratedProxy.updateQueryRecord(bean);
    }

    @Override
    public QueryRecord refreshQueryRecord(QueryRecord bean) throws BaseException
    {
        return decoratedProxy.refreshQueryRecord(bean);
    }

    @Override
    public Boolean deleteQueryRecord(String guid) throws BaseException
    {
        return decoratedProxy.deleteQueryRecord(guid);
    }

    @Override
    public Boolean deleteQueryRecord(QueryRecord bean) throws BaseException
    {
        return decoratedProxy.deleteQueryRecord(bean);
    }

    @Override
    public Long deleteQueryRecords(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteQueryRecords(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    {
        return decoratedProxy.createQueryRecords(queryRecords);
    }

    // TBD
    //@Override
    //public Boolean updateQueryRecords(List<QueryRecord> queryRecords) throws BaseException
    //{
    //}

}
