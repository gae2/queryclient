package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.DummyEntity;
import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.DummyEntityJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.DummyEntityWebService;


// MockDummyEntityWebService is a mock object.
// It can be used as a base class to mock DummyEntityWebService objects.
public abstract class MockDummyEntityWebService extends DummyEntityWebService  // implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(MockDummyEntityWebService.class.getName());
     
    // Af service interface.
    private DummyEntityWebService mService = null;

    public MockDummyEntityWebService()
    {
        this(MockServiceProxyFactory.getInstance().getDummyEntityServiceProxy());
    }
    public MockDummyEntityWebService(DummyEntityService service)
    {
        super(service);
    }


}
