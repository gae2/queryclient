package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeAppStruct;
import com.queryclient.af.bean.GaeAppStructBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.GaeAppStructJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.GaeAppStructWebService;


// MockGaeAppStructWebService is a mock object.
// It can be used as a base class to mock GaeAppStructWebService objects.
public abstract class MockGaeAppStructWebService extends GaeAppStructWebService  // implements GaeAppStructService
{
    private static final Logger log = Logger.getLogger(MockGaeAppStructWebService.class.getName());
     

}
