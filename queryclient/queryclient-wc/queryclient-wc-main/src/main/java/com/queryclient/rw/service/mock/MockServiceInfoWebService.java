package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ServiceInfo;
import com.queryclient.af.bean.ServiceInfoBean;
import com.queryclient.af.service.ServiceInfoService;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.ServiceInfoJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.ServiceInfoWebService;


// MockServiceInfoWebService is a mock object.
// It can be used as a base class to mock ServiceInfoWebService objects.
public abstract class MockServiceInfoWebService extends ServiceInfoWebService  // implements ServiceInfoService
{
    private static final Logger log = Logger.getLogger(MockServiceInfoWebService.class.getName());
     
    // Af service interface.
    private ServiceInfoWebService mService = null;

    public MockServiceInfoWebService()
    {
        this(MockServiceProxyFactory.getInstance().getServiceInfoServiceProxy());
    }
    public MockServiceInfoWebService(ServiceInfoService service)
    {
        super(service);
    }


}
