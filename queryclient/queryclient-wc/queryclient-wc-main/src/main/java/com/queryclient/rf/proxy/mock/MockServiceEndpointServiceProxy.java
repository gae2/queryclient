package com.queryclient.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ConsumerKeySecretPair;
import com.queryclient.ws.ServiceEndpoint;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairStub;
import com.queryclient.ws.stub.ConsumerKeySecretPairListStub;
import com.queryclient.ws.stub.ServiceEndpointStub;
import com.queryclient.ws.stub.ServiceEndpointListStub;
import com.queryclient.af.bean.ConsumerKeySecretPairBean;
import com.queryclient.af.bean.ServiceEndpointBean;
import com.queryclient.af.service.ServiceEndpointService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.rf.config.Config;
import com.queryclient.rf.proxy.ServiceEndpointServiceProxy;


// MockServiceEndpointServiceProxy is a decorator.
// It can be used as a base class to mock ServiceEndpointService objects.
public abstract class MockServiceEndpointServiceProxy extends ServiceEndpointServiceProxy implements ServiceEndpointService
{
    private static final Logger log = Logger.getLogger(MockServiceEndpointServiceProxy.class.getName());

    // MockServiceEndpointServiceProxy uses the decorator design pattern.
    private ServiceEndpointService decoratedProxy;

    public MockServiceEndpointServiceProxy(ServiceEndpointService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected ServiceEndpointService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(ServiceEndpointService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public ServiceEndpoint getServiceEndpoint(String guid) throws BaseException
    {
        return decoratedProxy.getServiceEndpoint(guid);
    }

    @Override
    public Object getServiceEndpoint(String guid, String field) throws BaseException
    {
        return decoratedProxy.getServiceEndpoint(guid, field);
    }

    @Override
    public List<ServiceEndpoint> getServiceEndpoints(List<String> guids) throws BaseException
    {
        return decoratedProxy.getServiceEndpoints(guids);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints() throws BaseException
    {
        return getAllServiceEndpoints(null, null, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpoints(ordering, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> getAllServiceEndpoints(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllServiceEndpoints(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceEndpointKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceEndpointKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllServiceEndpointKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpoints(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ServiceEndpoint> findServiceEndpoints(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findServiceEndpoints(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceEndpointKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceEndpointKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findServiceEndpointKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createServiceEndpoint(String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        return decoratedProxy.createServiceEndpoint(user, dataService, serviceName, serviceUrl, authRequired, authCredential, status);
    }

    @Override
    public String createServiceEndpoint(ServiceEndpoint bean) throws BaseException
    {
        return decoratedProxy.createServiceEndpoint(bean);
    }

    @Override
    public ServiceEndpoint constructServiceEndpoint(ServiceEndpoint bean) throws BaseException
    {
        return decoratedProxy.constructServiceEndpoint(bean);
    }

    @Override
    public Boolean updateServiceEndpoint(String guid, String user, String dataService, String serviceName, String serviceUrl, Boolean authRequired, ConsumerKeySecretPair authCredential, String status) throws BaseException
    {
        return decoratedProxy.updateServiceEndpoint(guid, user, dataService, serviceName, serviceUrl, authRequired, authCredential, status);
    }

    @Override
    public Boolean updateServiceEndpoint(ServiceEndpoint bean) throws BaseException
    {
        return decoratedProxy.updateServiceEndpoint(bean);
    }

    @Override
    public ServiceEndpoint refreshServiceEndpoint(ServiceEndpoint bean) throws BaseException
    {
        return decoratedProxy.refreshServiceEndpoint(bean);
    }

    @Override
    public Boolean deleteServiceEndpoint(String guid) throws BaseException
    {
        return decoratedProxy.deleteServiceEndpoint(guid);
    }

    @Override
    public Boolean deleteServiceEndpoint(ServiceEndpoint bean) throws BaseException
    {
        return decoratedProxy.deleteServiceEndpoint(bean);
    }

    @Override
    public Long deleteServiceEndpoints(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteServiceEndpoints(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException
    {
        return decoratedProxy.createServiceEndpoints(serviceEndpoints);
    }

    // TBD
    //@Override
    //public Boolean updateServiceEndpoints(List<ServiceEndpoint> serviceEndpoints) throws BaseException
    //{
    //}

}
