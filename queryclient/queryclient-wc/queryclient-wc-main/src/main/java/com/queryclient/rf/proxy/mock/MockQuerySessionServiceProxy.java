package com.queryclient.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.ReferrerInfoStruct;
import com.queryclient.ws.QuerySession;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.ReferrerInfoStructStub;
import com.queryclient.ws.stub.ReferrerInfoStructListStub;
import com.queryclient.ws.stub.QuerySessionStub;
import com.queryclient.ws.stub.QuerySessionListStub;
import com.queryclient.af.bean.ReferrerInfoStructBean;
import com.queryclient.af.bean.QuerySessionBean;
import com.queryclient.af.service.QuerySessionService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.rf.config.Config;
import com.queryclient.rf.proxy.QuerySessionServiceProxy;


// MockQuerySessionServiceProxy is a decorator.
// It can be used as a base class to mock QuerySessionService objects.
public abstract class MockQuerySessionServiceProxy extends QuerySessionServiceProxy implements QuerySessionService
{
    private static final Logger log = Logger.getLogger(MockQuerySessionServiceProxy.class.getName());

    // MockQuerySessionServiceProxy uses the decorator design pattern.
    private QuerySessionService decoratedProxy;

    public MockQuerySessionServiceProxy(QuerySessionService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected QuerySessionService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(QuerySessionService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public QuerySession getQuerySession(String guid) throws BaseException
    {
        return decoratedProxy.getQuerySession(guid);
    }

    @Override
    public Object getQuerySession(String guid, String field) throws BaseException
    {
        return decoratedProxy.getQuerySession(guid, field);
    }

    @Override
    public List<QuerySession> getQuerySessions(List<String> guids) throws BaseException
    {
        return decoratedProxy.getQuerySessions(guids);
    }

    @Override
    public List<QuerySession> getAllQuerySessions() throws BaseException
    {
        return getAllQuerySessions(null, null, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessions(ordering, offset, count, null);
    }

    @Override
    public List<QuerySession> getAllQuerySessions(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllQuerySessions(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllQuerySessionKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllQuerySessionKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllQuerySessionKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessions(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<QuerySession> findQuerySessions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findQuerySessions(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findQuerySessionKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findQuerySessionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findQuerySessionKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createQuerySession(String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        return decoratedProxy.createQuerySession(user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note);
    }

    @Override
    public String createQuerySession(QuerySession bean) throws BaseException
    {
        return decoratedProxy.createQuerySession(bean);
    }

    @Override
    public QuerySession constructQuerySession(QuerySession bean) throws BaseException
    {
        return decoratedProxy.constructQuerySession(bean);
    }

    @Override
    public Boolean updateQuerySession(String guid, String user, String dataService, String serviceUrl, String inputFormat, String outputFormat, ReferrerInfoStruct referrerInfo, String status, String note) throws BaseException
    {
        return decoratedProxy.updateQuerySession(guid, user, dataService, serviceUrl, inputFormat, outputFormat, referrerInfo, status, note);
    }

    @Override
    public Boolean updateQuerySession(QuerySession bean) throws BaseException
    {
        return decoratedProxy.updateQuerySession(bean);
    }

    @Override
    public QuerySession refreshQuerySession(QuerySession bean) throws BaseException
    {
        return decoratedProxy.refreshQuerySession(bean);
    }

    @Override
    public Boolean deleteQuerySession(String guid) throws BaseException
    {
        return decoratedProxy.deleteQuerySession(guid);
    }

    @Override
    public Boolean deleteQuerySession(QuerySession bean) throws BaseException
    {
        return decoratedProxy.deleteQuerySession(bean);
    }

    @Override
    public Long deleteQuerySessions(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteQuerySessions(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createQuerySessions(List<QuerySession> querySessions) throws BaseException
    {
        return decoratedProxy.createQuerySessions(querySessions);
    }

    // TBD
    //@Override
    //public Boolean updateQuerySessions(List<QuerySession> querySessions) throws BaseException
    //{
    //}

}
