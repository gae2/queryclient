package com.queryclient.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.queryclient.ws.BaseException;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.GaeUserStruct;
import com.queryclient.af.bean.GaeUserStructBean;
import com.queryclient.fe.WebException;
import com.queryclient.fe.bean.GaeUserStructJsBean;
import com.queryclient.rf.proxy.mock.MockServiceProxyFactory;
import com.queryclient.rw.service.GaeUserStructWebService;


// MockGaeUserStructWebService is a mock object.
// It can be used as a base class to mock GaeUserStructWebService objects.
public abstract class MockGaeUserStructWebService extends GaeUserStructWebService  // implements GaeUserStructService
{
    private static final Logger log = Logger.getLogger(MockGaeUserStructWebService.class.getName());
     

}
