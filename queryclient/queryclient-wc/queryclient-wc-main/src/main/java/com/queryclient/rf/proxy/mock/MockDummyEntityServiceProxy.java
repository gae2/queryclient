package com.queryclient.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.queryclient.ws.BaseException;
import com.queryclient.ws.exception.NotImplementedException;
import com.queryclient.ws.core.StatusCode;
import com.queryclient.ws.core.StringCursor;
import com.queryclient.ws.DummyEntity;
import com.queryclient.ws.stub.ErrorStub;
import com.queryclient.ws.stub.KeyListStub;
import com.queryclient.ws.stub.DummyEntityStub;
import com.queryclient.ws.stub.DummyEntityListStub;
import com.queryclient.af.bean.DummyEntityBean;
import com.queryclient.af.service.DummyEntityService;
import com.queryclient.af.util.MarshalHelper;
import com.queryclient.af.util.StringUtil;
import com.queryclient.rf.auth.TwoLeggedOAuthClientUtil;
import com.queryclient.rf.config.Config;
import com.queryclient.rf.proxy.DummyEntityServiceProxy;


// MockDummyEntityServiceProxy is a decorator.
// It can be used as a base class to mock DummyEntityService objects.
public abstract class MockDummyEntityServiceProxy extends DummyEntityServiceProxy implements DummyEntityService
{
    private static final Logger log = Logger.getLogger(MockDummyEntityServiceProxy.class.getName());

    // MockDummyEntityServiceProxy uses the decorator design pattern.
    private DummyEntityService decoratedProxy;

    public MockDummyEntityServiceProxy(DummyEntityService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected DummyEntityService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(DummyEntityService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public DummyEntity getDummyEntity(String guid) throws BaseException
    {
        return decoratedProxy.getDummyEntity(guid);
    }

    @Override
    public Object getDummyEntity(String guid, String field) throws BaseException
    {
        return decoratedProxy.getDummyEntity(guid, field);
    }

    @Override
    public List<DummyEntity> getDummyEntities(List<String> guids) throws BaseException
    {
        return decoratedProxy.getDummyEntities(guids);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities() throws BaseException
    {
        return getAllDummyEntities(null, null, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntities(ordering, offset, count, null);
    }

    @Override
    public List<DummyEntity> getAllDummyEntities(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDummyEntities(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDummyEntityKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDummyEntityKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDummyEntityKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntities(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DummyEntity> findDummyEntities(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDummyEntities(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDummyEntityKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDummyEntityKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDummyEntityKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDummyEntity(String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        return decoratedProxy.createDummyEntity(user, name, content, maxLength, expired, status);
    }

    @Override
    public String createDummyEntity(DummyEntity bean) throws BaseException
    {
        return decoratedProxy.createDummyEntity(bean);
    }

    @Override
    public DummyEntity constructDummyEntity(DummyEntity bean) throws BaseException
    {
        return decoratedProxy.constructDummyEntity(bean);
    }

    @Override
    public Boolean updateDummyEntity(String guid, String user, String name, String content, Integer maxLength, Boolean expired, String status) throws BaseException
    {
        return decoratedProxy.updateDummyEntity(guid, user, name, content, maxLength, expired, status);
    }

    @Override
    public Boolean updateDummyEntity(DummyEntity bean) throws BaseException
    {
        return decoratedProxy.updateDummyEntity(bean);
    }

    @Override
    public DummyEntity refreshDummyEntity(DummyEntity bean) throws BaseException
    {
        return decoratedProxy.refreshDummyEntity(bean);
    }

    @Override
    public Boolean deleteDummyEntity(String guid) throws BaseException
    {
        return decoratedProxy.deleteDummyEntity(guid);
    }

    @Override
    public Boolean deleteDummyEntity(DummyEntity bean) throws BaseException
    {
        return decoratedProxy.deleteDummyEntity(bean);
    }

    @Override
    public Long deleteDummyEntities(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteDummyEntities(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    {
        return decoratedProxy.createDummyEntities(dummyEntities);
    }

    // TBD
    //@Override
    //public Boolean updateDummyEntities(List<DummyEntity> dummyEntities) throws BaseException
    //{
    //}

}
